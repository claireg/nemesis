# -*- coding: utf-8 -*-
"""
Warnings
--------
    Ce fichier est utilisé lors de l'appel `python setup.py sdist ...` mais AUSSI lors du pip install.
"""
import os
import sys
import codecs
import re
import glob
from setuptools import setup, find_packages

here = os.path.dirname(__file__)

name = "nemesis"
packages = find_packages(where=".")
meta_path = os.path.join(".", "nemesis", "__init__.py")
keywords = ["matplotlib", "qt5", "c++", "CEA"]
classifiers = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Developers",
    "Natural Language :: English",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.7",
    "Topic :: Software Development :: Libraries :: Python Modules",
]
install_requires = []
extra_require = {"docs": [
    "sphinx",
],
    "tests": [
        "pytest",
    ],
}
extra_require["dev"] = extra_require["tests"] + extra_require["docs"]


###############################################################################

class InstallHelper(object):
    current_workdir = os.path.abspath(os.path.dirname(__file__))
    meta_file = ''

    @classmethod
    def package_data(cls):
        cls.meta_file = cls.read(meta_path)

    @classmethod
    def read(cls, *parts):
        """
        Build an absolute path from *parts* and and return the contents of the
        resulting file.  Assume UTF-8 encoding.
        """
        with codecs.open(os.path.join(cls.current_workdir, *parts), "rb", "utf-8") as f:
            return f.read()

    @classmethod
    def find_meta(cls, meta):
        """
        Extract __*meta*__ from cls.meta_file.
        """
        meta_match = re.search(
            r"^__{meta}__ = ['\"]([^'\"]*)['\"]".format(meta=meta),
            cls.meta_file, re.M
        )
        if meta_match:
            return meta_match.group(1)
        raise RuntimeError("Unable to find __{meta}__ string.".format(meta=meta))


def setup_package():
    """
    Configuration du package nemesis
    """
    helper = InstallHelper()
    helper.package_data()
    __version__ = helper.find_meta("version")

    uri = helper.find_meta("uri")
    long_description = helper.read("README.md")
    # Tous les headers y compris ceux à configurer
    headers = glob.glob('./include/*.h') + glob.glob('./include/*.h.in') + glob.glob('./include/*.hpp')

    metadata = dict(
        name=name,
        version=__version__,
        author=helper.find_meta("author"),
        author_email=helper.find_meta("email"),
        description=helper.find_meta("description"),
        long_description=long_description,
        long_description_content_type="text/markdown",
        url=uri,
        packages=packages,
        classifiers=classifiers,
        # Active la prise en compte du fichier MANIFEST.in
        include_package_data=True,
        # options pas dans le tuto "Python Packaging User Guide"
        package_dir={"": "."},
        maintainer=helper.find_meta("author"),
        maintainer_email=helper.find_meta("email"),
        license=helper.find_meta("license"),
        platforms='any',
        keywords=keywords,
        python_requires='>=3.7',

        # matplotlib has C/C++ extensions, so it's not zip safe.
        # Telling setuptools this prevents it from doing an automatic
        # check for zip safety.
        zip_safe=False,

        # List third-party Python packages that we require
        install_requires=install_requires,
        extras_require=extra_require,
    )

    # ne sera exécuté que lors du python setup.py install_headers
    metadata['headers'] = headers
    print('##### HEADERS = {0}'.format(';'.join(headers)))
    setup(**metadata)


if __name__ == '__main__':
    version_msg_error = ['ERREUR : nemesis est compatible avec python 3.6 et au delà uniquement.',
                         '\n\tPython trouvé : {0}.{1}.{2}'.format(sys.version_info.major, sys.version_info.minor,
                                                                  sys.version_info.micro),
                         '\t({0})'.format(sys.executable)]
    if not (sys.version_info.major == 3 and sys.version_info.minor > 5):
        sys.exit(''.join(version_msg_error))

    setup_package()
