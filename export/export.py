# -*- coding: utf-8 -*-

"""
Script d'exportation des sources de nemesis.

Les fichiers/répertoires/chaînes de caractèrse à exclure sont listés dans les fichiers _EXCLUDE.

Procédure d'export de `nemesis`:

1. cloner le projet depuis le *bare repository*
2. Activer un environnement python basé sur python3.7
3. Exécuter les commandes suivantes ::

        cd project/export
        python export.py


"""

import sys
import os
import glob
import shutil
import warnings
import subprocess

_exclude_filename = '_EXCLUDE'
_intern_dirname = 'intern'
_starting_block = '>>> _EXCLUDE'
_ending_block = '<<< _EXCLUDE'

__VERBOSE = True
FS_ENCODING = sys.getfilesystemencoding()


def get_file_mimetype(filepath: str):
    # À utiliser avec parcimonie car très coûteux en temps
    # Mieux faut essayer de l'ouvrir en mode text pour tester si un fichier est de type text
    # le module mimetypes de python 3.7.0 ne reconnaît pas les fichiers .md, .cpp, ...
    cmd = f'file --mime-type {filepath}'
    try:
        output = subprocess.check_output(cmd.split())
        output = output.decode(FS_ENCODING).strip()
    except subprocess.CalledProcessError as e:
        print(f'get_file_mimetype : {e}')
        return None
    else:
        tmp, mime = output.split(':')
        return mime.strip()


def is_text_file(filepath: str) -> bool:
    mime = get_file_mimetype(filepath)
    if mime.startswith('text'):
        return True
    return False


def list_exclude_files(show: bool = False):
    exclude_files = glob.glob(f'../*/{_exclude_filename}')
    main_exclude = os.path.join('..', _exclude_filename)
    if os.path.exists(main_exclude):
        exclude_files.append(main_exclude)
    if show:
        print(f'Liste des répertoires contenant un fichier {_exclude_filename} : {exclude_files}\n')
    return exclude_files


def list_intern_dir(show: bool = False):
    intern_dirs = []
    for root, dirs, files in os.walk('..'):
        if _intern_dirname in dirs:
            intern_dirs.append(root)
    if show:
        print(f'Liste des répertoires contenant un répertoire {_intern_dirname} : {intern_dirs}\n')
    return intern_dirs


def remove_dir_or_file(filepath: str, exclude_file: str = ''):
    realpath = os.path.realpath(filepath)
    if os.path.exists(filepath):
        if os.path.isdir(filepath):
            shutil.rmtree(realpath)
        elif os.path.isfile(filepath):
            os.remove(realpath)
        else:
            raise FileNotFoundError(f"{filepath} n'est ni un fichier ni un répertoire. "
                                    f"\t\t(in {exclude_file}).\nLigne mal formée ?")
    else:
        warnings.warn(f"{filepath} n'existe pas \t\t(in {exclude_file}).\nLigne mal formée ?")


def remove_dirs_or_files(dirpath: str, line: str, exclude_file: str):
    p = os.path.join(dirpath, line)
    if '*' not in p:
        remove_dir_or_file(p, exclude_file)
    else:
        files_or_dirs = glob.glob(p)
        if len(files_or_dirs) == 0:
            raise NotImplementedError(f'Cas non prévu {exclude_file}: {line}\nLigne mal formée ?')
        for obj in files_or_dirs:
            remove_dir_or_file(obj, exclude_file)
    if __VERBOSE:
        print(f'\tSuppression répertoires ou fichiers {dirpath} ({line})')


def get_comment_char(filepath: str):
    ext = os.path.splitext(filepath)[1]
    if ext in ('.py', '.sh', '.bash'):
        return '#'
    elif ext in ('.cpp', '.cxx', '.h', '.hpp'):
        return '//'
    else:
        raise NotImplementedError(f'Extension de fichier non prévu : {ext} ({filepath})')


def bad_comment_char(filepath: str, tmp_filepath: str, num: int, line: str):
    os.rename(tmp_filepath, filepath)
    raise RuntimeError(f'Bloc mal formé : caratère de commentaire erroné : {filepath}:{num}:\t{line}')


def remove_blocks(dirpath: str, ex_line: str, exclude_file: str):
    values = ex_line.split(':')
    if len(values) != 2:
        raise RuntimeError(f'Ligne mal formée in {exclude_file} : {ex_line}.')

    filepath = os.path.join(dirpath, values[0])
    if not os.path.exists(filepath):
        raise FileNotFoundError(f"{filepath} n'existe pas")

    comment_char = get_comment_char(filepath)
    basename = os.path.basename(filepath)
    filename, ext = os.path.splitext(basename)
    old_path = os.path.join(os.path.dirname(filepath), f'tmp_{filename}{ext}')
    os.rename(filepath, old_path)
    with open(old_path, mode='r', encoding='utf-8') as f_origin:
        with open(filepath, mode='w', encoding='utf-8') as f_new:
            in_block = False
            n_starts = 0
            n_ends = 0
            for i, line in enumerate(f_origin):
                if _starting_block in line:
                    if comment_char in line:
                        in_block = True
                        n_starts += 1
                    else:
                        bad_comment_char(filepath, old_path, num=i, line=line)
                elif _ending_block in line:
                    if comment_char in line:
                        in_block = False
                        n_ends += 1
                    else:
                        bad_comment_char(filepath, old_path, num=i, line=line)
                else:
                    if not in_block:
                        f_new.write(line)

    if n_starts != n_ends:
        os.rename(old_path, filepath)
        raise RuntimeError(f'Fichier mal formé {filepath}\nLe nombre de début de blocs '
                           f'ne correspond pas au nombre de fin de bloc')
    os.remove(old_path)
    if __VERBOSE:
        print(f'\tBloc remplacé dans {dirpath} ({ex_line})')


def replace_in_file(filepath: str, old: str, new: str):
    if _exclude_filename in filepath:
        return None

    if os.path.exists(filepath):
        try:
            with open(filepath, mode='r', encoding='utf-8') as f:
                new_text = f.read().replace(old, new)

            with open(filepath, mode='w', encoding='utf-8') as f:
                f.write(new_text)
        except UnicodeDecodeError:
            # Test brutal mais rapide pour tester si un fichier est de type text ou binaire
            #   open(f, mode='r') pour un fichier binaire lève une exception
            pass
    else:
        warnings.warn(f"{filepath} n'existe pas.\nBug ?")


def recursive_replace_in_file(filepath: str, old: str, new: str, deep=1):
    # filepath is a directory
    for root, dirs, files in os.walk(filepath):
        for de in dirs:
            fp = os.path.join(root, de)
            recursive_replace_in_file(fp, old, new, deep+1)
        for fe in files:
            fp = os.path.join(root, fe)
            if 'Catch2-master' not in fp and '_static' not in fp:
                replace_in_file(fp, old, new)
                # Beaucoup plus lent, mais plus propre
                # if is_text_file(fp):
                #     replace_in_file(fp, old, new)
            else:
                pass


def replace_strings_or_blocks(dirpath: str, line: str, exclude_file: str):
    if line.endswith(':'):
        relative_path, s_intern, s_open, empty = line.split(':')
        # relative_path = values[0]
        # values = values[1:-1]
        fp = os.path.join(dirpath, relative_path)
        if '*' in fp:
            if __VERBOSE:
                print(f'\tChaine à remplacer dans {dirpath} : {s_intern} -> {s_open}')
            files_or_dirs = glob.glob(fp)
            for e in files_or_dirs:
                if os.path.isdir(e):
                    recursive_replace_in_file(e, s_intern, s_open)
                elif os.path.isfile(e):
                    replace_in_file(e, s_intern, s_open)
        else:
            if __VERBOSE:
                print(f'\tChaine à remplacer dans {fp} : {s_intern} -> {s_open}')
            replace_in_file(fp, s_intern, s_open)
    else:
        if 'blocks' in line:
            remove_blocks(dirpath, line, exclude_file)
        else:
            raise NotImplementedError(f'Cas non prévu : {exclude_file} : {line}\nLigne mal formée ?')


def parse_exclude_file(exclude_file: str):
    dirpath = os.path.dirname(exclude_file)
    with open(exclude_file, mode='r', encoding='utf-8') as f:
        if __VERBOSE:
            print(f'# ANALYSE de {exclude_file}')
        for line in f:
            line = line.strip()
            if line:
                if ':' in line:
                    replace_strings_or_blocks(dirpath, line, exclude_file)
                else:
                    remove_dirs_or_files(dirpath, line, exclude_file)
    if __VERBOSE:
        print()


if __name__ == '__main__':
    excludes = list_exclude_files(show=True)
    for entry in excludes:
        parse_exclude_file(entry)
        remove_dir_or_file(entry, '')
