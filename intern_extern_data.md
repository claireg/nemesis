# Liste des séparations internes / externes à faire

En plus des sources dans les répertoires `intern`, il existe un fichier `_EXCLUDE` qui liste
les répertoires, fichiers, chaînes et blocs de code à exclure lors d'un export.
**Le répertoire `intern` doit être listé dans le fichier `_EXCLUDE`.**.

Les fichiers `_EXCLUDE` sont dans les répertoires principaux du projet, et non dans les sous-répertoirs (en gros,
un fichier par thématique du projet).

Format à exclure  :

* Exclusion d'un répertoire :
    
        toto/
        toto

* Exclusion d'un fichier :

        nom_fichier.extension

* Suppression de chaînes dans un fichier :

        nom.ext:a_remplacer_par:chaine_remplacement:

* Suppression de chaînes dans tous les fichiers du répertoire courant et des sous-répertoires :

        *:a_remplacer:chaine_remplacement:

* Suppression de blocs de code 

        nom.ext:blocks

    * En Python : un bloc = 
        
            # >>> _EXCLUDE
            code 
            sur 
            plusieurs 
            lignes
            # <<< _EXCLUDE
     
    * En C++ : un bloc =
    
            // >>> _EXCLUDE
            code
            sur 
            plusieurs
            lignes
            // <<< _EXCLUDE
            
## docs

### api

#### api_readdata

`@FIXME` :  Toujours d'actualités ?

Les docstrings ainsi que le code de certaines méthodes de `psydatapool.py` ou `gaiadatapool.py` contiennent des 
références à des données qui devraient être internes.


