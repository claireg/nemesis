# -*- coding: utf-8 -*-

import sys
import argparse
import importlib
import warnings

# noinspection PyUnresolvedReferences,PyUnresolvedReferences
from distutils.version import LooseVersion
# avec yolk : from yolk.yolklib import Distributions
# avec yolk3k
from yolk.yolklib import get_distributions, get_packages

bad_modules = []

exe = sys.executable


def __print_bad_modules():
    global bad_modules
    if len(bad_modules) > 0:
        print()
        warnings.warn(f'Those modules are missing: {", ".join(bad_modules)}')
        print()


def __check_module(name, version=None):
    global bad_modules
    try:
        current_module = importlib.import_module(name)
    except ImportError:
        bad_modules.append(name)
    else:
        print(f'* {name} : {current_module.__version__}')
        if version is not None:
            if LooseVersion(current_module.__version__) < LooseVersion(version):
                print(f'Nemesis Check Installation : {name} too old. Need : {version}')


def __check_module_no_version(name):
    """
    Pour les modules n'ayant pas de variable __version__ tel que PyQt5 ...
    """
    global bad_modules
    try:
        importlib.import_module(name)
    except ImportError:
        bad_modules.append(name)
    else:
        print(f'* {name} : No version')


def __tuple2str(tuple_):
    return '.'.join(str(i) for i in tuple_)


def check_python_version():
    python_2 = (2, 7, 13)
    python_3 = (3, 6, 4)
    too_old_message = f'Version Python trop vieille\n\tConseillée : >= {__tuple2str(python_2)} ou >= ' \
                      f'{__tuple2str(python_3)}'
    if sys.version_info.major == 2:
        if sys.version_info < python_2:
            raise (Exception(too_old_message))
    elif sys.version_info.major == 3:
        if sys.version_info < (3, 6, 4):
            raise (Exception(too_old_message))
    else:
        raise (Exception(too_old_message))


def check_qt5agg():
    # On sait que l'import fonctionne
    import matplotlib as mpl
    mpl.use('Qt5Agg')
    try:
        import matplotlib.pyplot as plt
    except ImportError as e:
        print(e)
        raise Exception('Nemesis Check Installation : import matplotlib.pyplot failed.')
    else:
        print('* import matplotlib.pyplot succeeded.')
        try:
            plt.plot(range(10), 'go--')
            # Pour ne pas voir la fenêtre
            plt.show(block=False)
        except Exception as e:
            print(e)
            raise Exception('Nemesis Check Installation : plt.plot or plt.show failed')
        else:
            print('* show plot with Qt5Agg backend succeeded.')


def check():
    global exe
    print(f"##### Vérification de l'environnement {exe}")
    check_python_version()
    __check_module('yolk', '0.4.3')
    __check_module('numpy', '1.17.2')
    __check_module('scipy', '1.3.1')
    __check_module('matplotlib', '3.1.1')
    __check_module('recommonmark', '0.5.0')
    __check_module('sphinx', '2.2.0')
    __check_module('pylint', '2.4.3')
    __check_module('pytest', '5.1.1')
    __check_module('psutil', '5.5.1')
    __check_module('yaml', '5.1')
    __check_module('memory_profiler', '0.47')
    __check_module('graphviz', '0.8.3')
    # non présent dans les install spack
    __check_module('snakeviz', '0.4.2')
    __print_bad_modules()

    try:
        import PySide2
    except ImportError:
        __check_module_no_version('PyQt5')
    else:
        __check_module('PySide2', '5.9.0a1')
    check_qt5agg()

    try:
        import pytest
    except ImportError as e:
        print(e)
        raise Exception('Nemesis Check Installation : import pytest failed.')
    else:
        print('* import pytest succeeded.')

    print()


def matplotlib_backend():
    import matplotlib
    print(f'##### Backend matplotlib par défaut : {matplotlib.get_backend()}\n')


def print_packages():
    # pkg de type EggInfoDistribution
    virtual_env_packages = get_packages(show='all')
    print('##### {exe} : Liste des packages disponibles ({len(virtual_env_packages)} packages)')
    print('\n'.join([f'* {p}' for p in virtual_env_packages]))
    print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Vérification de l'installation d'un environnement virtuel.")
    parser.add_argument("-l", "--list", action="store_true", dest="list", help="Liste les packages installés")
    parser.add_argument("-c", "--check", action="store_true", dest="check", default=True,
                        help="Vérification du chargement des packages principaux nécessaires à Nemesis")

    # Gestion des arguments de lancement
    args, other_args = parser.parse_known_args()

    check()
    matplotlib_backend()
    if args.list is True:
        print_packages()
