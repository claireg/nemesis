# -*- coding: utf-8 -*-

import os
import sys
import argparse
import importlib
# noinspection PyUnresolvedReferences,PyUnresolvedReferences
from distutils.version import LooseVersion
from yolk.yolklib import Distributions


def build_env_dictionary(pip_list_file):
    """
    Construit le dictionnaire des packages d'un environnement en se basant sur le contenu du fichier issu de la commande
    `pip list`.

    Parameters
    ----------
    pip_list_file : str
        (os.PathLike) Fichier produit par `pip list > mon_fichier`

    Returns
    -------
    dict
    """
    with open(pip_list_file) as f:
        for i in range(2):
            # les deux premières lignes sont inutiles
            f.readline()
        i = 0
        result = dict()
        for line in iter(f.readline, ''):
            pkg, version = line.split()
            # print(f'{i} : {pkg}\t{version}')
            result[pkg] = version
            i += 1
        return result


def compare(d1, d2):
    keys_1 = d1.keys()
    keys_2 = d2.keys()
    common_keys = [key for key in keys_1 if key in keys_2]
    only_d1 = [key for key in keys_1 if key not in keys_2]
    only_d2 = [key for key in keys_2 if key not in keys_1]
    str_only_d1 = ', '.join(only_d1)
    str_only_d2 = ', '.join(only_d2)
    print(f'* Only in 1 : {str_only_d1}')
    print(f'* Only in 2 : {str_only_d2}')
    print('* Common')
    for k in common_keys:
        v1, v2 = d1[k], d2[k]
        if v1 == v2:
            v = '–'
        else:
            v = f'{v1} vs {v2}'
        print(f'\t* {k} : \t{v}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Comparaison de deux environnements virtuels.")
    parser.add_argument("-v1", "--env_1", dest='v1', action="store", type=str, help="Path to `pip_list_file` n°1")
    parser.add_argument("-v2", "--env_2", dest='v2', action="store", type=str, help="Path to `pip_list_file` n°2")

    # Gestion des arguments de lancement
    args, other_args = parser.parse_known_args()

    dict_v1 = build_env_dictionary(args.v1)
    dict_v2 = build_env_dictionary(args.v2)

    compare(dict_v1, dict_v2)
