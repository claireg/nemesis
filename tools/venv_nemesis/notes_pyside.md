# Notes sur la compilation / installation de Qt for Python

Les noms du package est PySide2.
Les versions de PySide2 sont les numéros de version de Qt associée : 
* `pyside-setup-everywhere-src-5.11.0.tar.xz`
* `pyside2-setup-5.9.zip`
* `pyside2-setup-5.9.tgz`

## Modifications des sources

Les fichiers de compilation ont été modifiés à 2 endroits.
| Fichier      | Ligne | Commentaire                                      |
|:------------ | -----:| ------------------------------------------------ |
| `./sources/shiboken2/CMakeLists.txt` | 433 | Pour que la lib shiboken soit liée à la lib python (1) |
| `./sources/pyside2/CMakeLists.txt` | 229 | Ajout spécification version ABI (2) |
| `./setup.py` | 262 | Ajout version 3.7 de python |

* _(1)_ Cela posait problème dans un seul cas : lorsque `nemesis` était chargé via un `dlopen` dans Gaia. On a modifié 
 le comportement par défaut qui refusait de lier la lib shiboken2 à la lib python. Ça fonctionne dans 95% des cas mais 
 pas quand on se lie à Gaia.
* _(2)_ `CMAKE_CXX_FLAGS` : `_GLIBCXX_USE_CXX11_ABI=0`. On espère ainsi régler les pbms de version d'abi et de c++ une 
fois le package installé.


**J'ai refait un package `pyside2-setup-5.9.tgz` qui contient toutes les modifications.**

_Nota Bene_ : /!\ Dans `pyside2-setup-5.9.zip` original, le répertoire `sources/pyside2-tools` est vide ...

### PySide2 et PyCharm

_A priori_, le `ipython` de pycharm ne fonctionne qu'avec `PyQt5`. 

## Stratégie

* Compilation (build) / Création wheel à partir du build
* Installation

## Tentatives de Compilation et installation

Différentes tentatives effectuées.
**Le script `installNemesis.sh` fonctionne en automatique pour l'installation de _PySide2_.**
Les tentatives sont gardées pour mémoire.

### L'environnement

```bash

    GNU=/path/vers/gcc49/gcc/6.4
    GCC64_LIB=${GNU}/lib
    GCC64_LIB64=${GNU}/lib64

    LLVM_BASE=/path/vers/gcc6/llvm/6.0.0
    LLVM_LIB=${LLVM_BASE}/lib

    ABI="-D_GLIBCXX_USE_CXX11_ABI=0"
    LDFLAGS_BASE="-L${GCC64_LIB} -L${GCC64_LIB64}"

    QT_BASE=/path/vers/gcc/qt/5.9.1
    QMAKE=${QT_BASE}/bin/qmake

    export CC=${GNU}/bin/gcc
    export CXX=${GNU}/bin/g++
    export FC=${GNU}/bin/gfortran
    export F77=${FC}
    export F90=${FC}
    export CFLAGS=${ABI}
    export CXXFLAGS=${ABI}
    export LDFLAGS=${LDFLAGS_BASE}
    # pour cmake pyside-everywhere
    export LLVM_INSTALL_DIR=${LLVM_BASE}
    # pour cmake pyside-everywhere
    export QT_SRC_DIR=${QT_BASE}

    export LD_LIBRARY_PATH=${LLVM_LIB}:${QT_BASE}/lib/:${GCC64_LIB}:${GCC64_LIB64}:${LD_LIBRARY_PATH}
    export PATH=${QT_BASE}/bin/:$PATH
```

### Activation de l'environnement virtuel

```bash
    . /path/vers/Nemesis_version/bin/activate
```

### Compilation

```bash
    python setup.py build --qmake=${QMAKE} --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --jobs=16
```

## Création wheel

Cette commande effectue aussi la compilation ...

```bash
    python setup.py bdist_wheel --qmake=${QMAKE} --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --jobs=16
```

Rq: On devrait pouvoir faire le build et le fichier wheel en une seul commande

**ATTENTION** aucun fichier .whl n'est généré ...

## Installation

En théorie :

```bash
    pip install fichier.whl
```

## Pour tester

```bash
export LD_LIBRARY_PATH=/tmp/MyProducts/pyside2-setup-5.9/Nemesis_0.43_install/py3.6-qt5.9.1-64bit-release/lib:$LD_LIBRARY_PATH
```

```python
import sys
sys.path.append('/tmp/MyProducts/pyside2-setup-5.9/Nemesis_0.43_install/py3.6-qt5.9.1-64bit-release/lib/python3.6/site-packages')

from PySide2 import QtCore, QtGui, QtWidgets, __version__

import matplotlib as mpl
mpl.use('Qt5Agg')
import matplotlib.pyplot as plt
```

## Test pour ne pas avoir de LD_LIBRARY_PATH à faire

```bash
    GNU=/path/vers/gcc49/gcc/6.4
    GCC64_LIB=${GNU}/lib
    GCC64_LIB64=${GNU}/lib64

    LLVM_BASE=/path/vers/gcc6/llvm/6.0.0
    LLVM_LIB=${LLVM_BASE}/lib
    
    # LDFLAGS_BASE="-L${GNULIB} -L${GNULIB64}/lib64 -Wl,--rpath=${GNULIB} -Wl,--rpath=${GNULIB64}"
    # Pour le moment 
    LDFLAGS_BASE="-L${GNULIB} -L${GNULIB64}/lib64"
    ABI="-D_GLIBCXX_USE_CXX11_ABI=0"

    QT_BASE=/path/vers/gcc/qt/5.9.1
    QMAKE=${QT_BASE}/bin/qmake

    export CC=${GNU}/bin/gcc
    export CXX=${GNU}/bin/g++
    export FC=${GNU}/bin/gfortran
    export F77=${FC}
    export F90=${FC}
    export CFLAGS=${ABI}
    export CXXFLAGS=${ABI}
    export LDFLAGS=${LDFLAGS_BASE}
    # pour cmake pyside-everywhere
    export LLVM_INSTALL_DIR=${LLVM_BASE}
    # pour cmake pyside-everywhere
    export QT_SRC_DIR=${QT_BASE}

    export LD_LIBRARY_PATH=${LLVM_LIB}:${QT_BASE}/lib/:${GCC64_LIB}:${GCC64_LIB64}:${LD_LIBRARY_PATH}
    export PATH=${QT_BASE}/bin/:$PATH

    . ~/Devel/Products/Python/install/python/3.6.4/Nemesis_0.4/bin/activate
    
    python setup.py bdist_wheel --qmake=${QMAKE} --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --jobs=16 --standalone
```
    
## ``PySide-1.2.4``  Autre test 1

```bash
    cp ~/Devel/Products/Python/packages/PySide-1.2.4.tar.gz .
    tar xf PySide-1.2.4.tar.gz 
    cd PySide-1.2.4

    GNU=/path/vers/gcc49/gcc/6.4
    GCC64_LIB=${GNU}/lib
    GCC64_LIB64=${GNU}/lib64
    LLVM_BASE=/path/vers/gcc6/llvm/6.0.0
    LLVM_LIB=${LLVM_BASE}/lib

    LDLAGS_BASE="-L${GNULIB} -L${GNULIB64}/lib64"
    ABI="-D_GLIBCXX_USE_CXX11_ABI=0"
    QT_BASE=/path/vers/gcc/qt/5.9.1
    QMAKE=${QT_BASE}/bin/qmake

    export CC=${GNU}/bin/gcc
    export CXX=${GNU}/bin/g++
    export FC=${GNU}/bin/gfortran
    export F77=${FC}
    export F90=${FC}
    export CFLAGS=${ABI}
    export CXXFLAGS=${ABI}
    export LDFLAGS=${LDFLAGS_BASE}
    # pour cmake pyside-everywhere
    export LLVM_INSTALL_DIR=${LLVM_BASE}
    # pour cmake pyside-everywhere
    export QT_SRC_DIR=${QT_BASE}
    export LD_LIBRARY_PATH=${LLVM_LIB}:${QT_BASE}/lib/:${GCC64_LIB}:${GCC64_LIB64}:${LD_LIBRARY_PATH}
    export PATH=${QT_BASE}/bin/:$PATH
    . ~/Devel/Products/Python/install/python/3.6.4/Nemesis_0.4/bin/activate
    python setup.py bdist_wheel --qmake=${QMAKE} --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --jobs=16 --standalone
    pip install dist/PySide-1.2.4-cp36-cp36m-linux_x86_64.whl 
```

Problème LD_LIBRARY_PATH au lancement pour gcc64
Qt en qt 4 ...


## ``PySide-1.2.4`` Autre test 2

```bash
    cp ~/Devel/Products/
    Python/packages/PySide-1.2.4.tar.gz .
    tar xf PySide-1.2.4.tar.gz 
    cd PySide-1.2.4

    GNU=/path/vers/gcc49/gcc/6.4
    GCC64_LIB=${GNU}/lib
    GCC64_LIB64=${GNU}/lib64
    LLVM_BASE=/path/vers/gcc6/llvm/6.0.0
    LLVM_LIB=${LLVM_BASE}/lib
    LDLAGS_BASE="-L${GNULIB} -L${GNULIB64}/lib64 -Wl,--rpath=${GNULIB} -Wl,--rpath=${GNULIB64}"
    ABI="-D_GLIBCXX_USE_CXX11_ABI=0"
    QT_BASE=/path/vers/gcc/qt/5.9.1
    QMAKE=${QT_BASE}/bin/qmake
    export CC=${GNU}/bin/gcc
    export CXX=${GNU}/bin/g++
    export FC=${GNU}/bin/gfortran
    export F77=${FC}
    export F90=${FC}
    export CFLAGS=${ABI}
    export CXXFLAGS=${ABI}
    export LDFLAGS=${LDFLAGS_BASE}
    # pour cmake pyside-everywhere
    export LLVM_INSTALL_DIR=${LLVM_BASE}
    # pour cmake pyside-everywhere
    export QT_SRC_DIR=${QT_BASE}
    export LD_LIBRARY_PATH=${LLVM_LIB}:${QT_BASE}/lib/:${GCC64_LIB}:${GCC64_LIB64}:${LD_LIBRARY_PATH}
    export PATH=${QT_BASE}/bin/:$PATH
    export QTDIR=${QT_BASE}
    export QTINC=${QT_BASE}/include
    export QTLIB=${QT_BASE}/lib

    . ~/Devel/Products/Python/install/python/3.6.4/Nemesis_0.4/bin/activate
    python setup.py bdist_wheel --qmake=${QMAKE} --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --jobs=16 --standalone
```

Marche toujours pas ...

## ``pyside2-setup-5.9`` : Autre test 3 

Il faut d'abord récupérer le répertoire sources/pyside2-tools dans une autre version de pyside2 (ici une version 5.11).
Le fichier ``pyside2-setup-5.9.tgz`` de ``~/Devel/Products/Python/packages/`` a un répertoire ``pyside2-tools`` correct.

Doc pour installation à partir des sources : ``…/pyside2-setup-5.9/docs/building``.

```bash
    GNU=/path/vers/gcc49/gcc/6.4
    GCC64_LIB=${GNU}/lib
    GCC64_LIB64=${GNU}/lib64
    LLVM_BASE=/path/vers/gcc6/llvm/6.0.0
    LLVM_LIB=${LLVM_BASE}/lib
    LDFLAGS_BASE="-L${GNULIB} -L${GNULIB64}/lib64 -Wl,--rpath=${GNULIB} -Wl,--rpath=${GNULIB64}"
    ABI="-D_GLIBCXX_USE_CXX11_ABI=0"
    QT_BASE=/path/vers/gcc/qt/5.9.1
    QMAKE=${QT_BASE}/bin/qmake
    export CC=${GNU}/bin/gcc
    export CXX=${GNU}/bin/g++
    export FC=${GNU}/bin/gfortran
    export F77=${FC}
    export F90=${FC}
    export CFLAGS=${ABI}
    export CXXFLAGS=${ABI}
    export LDFLAGS=${LDFLAGS_BASE}
    # pour cmake pyside-everywhere
    export LLVM_INSTALL_DIR=${LLVM_BASE}
    # pour cmake pyside-everywhere
    export QT_SRC_DIR=${QT_BASE}
    export LD_LIBRARY_PATH=${LLVM_LIB}:${QT_BASE}/lib/:${GCC64_LIB}:${GCC64_LIB64}:${LD_LIBRARY_PATH}
    export PATH=${QT_BASE}/bin/:$PATH
    export QTDIR=${QT_BASE}
    export QTINC=${QT_BASE}/include
    export QTLIB=${QT_BASE}/lib

    . ~/Devel/Products/Python/install/python/3.6.4/Nemesis_0.4/bin/activate
    
    python setup.py bdist_wheel --qmake=${QMAKE} --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --jobs=16 --standalone

    ls dist
    pip install dist/PySide2-5.9.0a1-5.9.1-cp36-cp36m-linux_x86_64.whl 
```

Si ça plante : 
```bash
    export Shiboken_DIR=/tmp/MyProducts/pyside2-setup-5.9/Nemesis_0.43_install/py3.6-qt5.9.1-64bit-release/lib/cmake/Shiboken2-5.9.0
```

**ÇA MARCHE !**

_NB_ du 7 décembre : en fait ça ne marchait pas dans ipython (pbm de link vers la lib du compilateur spécifique).

## Test du 7 décembre : 0.0

```bash
export CXX="/path/vers/gcc49/gcc/6.4/bin/g++"
export FC="/path/vers/gcc49/gcc/6.4/bin/gfortran"
export CFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export CC="/path/vers/gcc49/gcc/6.4/bin/gcc"
export CXX="/path/vers/gcc49/gcc/6.4/bin/g++"
export FC="/path/vers/gcc49/gcc/6.4/bin/gfortran"
export CFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export LDFLAGS="-Wl,--rpath=/path/vers/gcc49/gcc/6.4/lib64 -L/path/vers/gcc49/gcc/6.4/lib64 -L~/Devel/Products/python/3.7.0/python/lib -Wl,--rpath=~/Devel/Products/python/3.7.0/python/lib -Wl,--rpath=~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/lib -L~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/lib  -Wl,--rpath=/path/vers/gcc/qt/5.9.1/lib -L/path/vers/gcc/qt/5.9.1/lib"
export PATH=/path/vers/gcc6/llvm/6.0.0/bin:~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/bin:/usr/local/sr/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:/ccc/products/libre/bin:/ccc/products/nec/bin

nem7

python3 setup.py install --jobs=16 --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --qmake=/path/vers/gcc/qt/5.9.1/bin/qmake
```

Problème path lib C++

## Test du 7 décembre : 0.1

Même environnement que _V7-10h_

Modification de la commande `setup.py`:

```bash
python3 setup.py bdist_wheel --jobs=16 --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --qmake=/path/vers/gcc/qt/5.9.1/bin/qmake --standalone
pip install dist/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl 
```

Problème path lib C++

## Test du 7 décembre : 0.2

Même environnement que _V7-11h_

Modification de la commande `pip`:

```bash
pip install dist/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl --global-option=build_ext --global-option="-R/path/vers/gcc49/gcc/6.4/lib64"
```

Dans le même esprit
```bash
pip install --global-option="--rpath=/path/vers/gcc49/gcc/6.4/lib64" dist/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl
```

Dans le même esprit 2
```bash
pip install --install-option="--rpath=/path/vers/gcc49/gcc/6.4/lib64" dist/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl
```

Dans le même esprit 3
```bash
pip install --install-option="-R/path/vers/gcc49/gcc/6.4/lib64" dist/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl
```

Problème path lib C++

## Test du 7 décembre : 0.3

```bash
export CXX="/path/vers/gcc49/gcc/6.4/bin/g++"
export FC="/path/vers/gcc49/gcc/6.4/bin/gfortran"
export CFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export CC="/path/vers/gcc49/gcc/6.4/bin/gcc"
export CXX="/path/vers/gcc49/gcc/6.4/bin/g++"
export FC="/path/vers/gcc49/gcc/6.4/bin/gfortran"
export CFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
export LDFLAGS="-Wl,--rpath=/path/vers/gcc49/gcc/6.4/lib64,--enable-new-dtags -Wl,--rpath=~/Devel/Products/python/3.7.0/python/lib -Wl,--rpath=~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/lib -Wl,--rpath=/path/vers/gcc/qt/5.9.1/lib"
export PATH=/path/vers/gcc6/llvm/6.0.0/bin:~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/bin:/usr/local/sr/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:/ccc/products/libre/bin:/ccc/products/nec/bin

. /path/vers/venv/bin/activate

python3 setup.py install --jobs=16 --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --qmake=/path/vers/gcc/qt/5.9.1/bin/qmake
# OU suivant résultat test V7-11h
python3 setup.py bdist_wheel --jobs=16 --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --qmake=/path/vers/gcc/qt/5.9.1/bin/qmake --standalone
pip install dist/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl 
```

`setup.py install` : Problème path lib C++

## Test du 7 décembre : 1.0

Environnement comme _V7-13h_
```bash
python3 setup.py install --jobs=16 --cmake=/path/vers/gcc/cmake/3.11.4/bin/cmake --qmake=/path/vers/gcc/qt/5.9.1/bin/qmake --rpath=/path/vers/gcc49/gcc/6.4/lib64:~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/lib:/path/vers/gcc/qt/5.9.1/lib:~/Devel/Products/python/3.7.0/Nemesis_0.1_gcc_64/lib/python3.7/site-packages/PySide2
```

En fait, `--rpath=` : ${GCC64_LIB64}:${VIRTUAL_ENV_PYTHON_LIBRARIES}:${QTLIB}:${PYTHON_SITEPACKAGES}/PySide2

**ÇA FONCTIONNE ENFIN !** (le 7 décembre 2018)