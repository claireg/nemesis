#!/bin/bash
${CMDDBG}

VERBOSE=1
umask 002
debug=0
show_usage=0

. ./scripts/messages.sh

# À lancer depuis ${PATH_SOURCES}/3.7.0

### -- Parsing des arguments
while [ "$#" != 0 ] ; do
    case "$1" in
    -h)
        show_usage=1
        ;;
    --help)
        show_usage=1
        ;;
    --dbg|--debug)
        debug=1
        ;;
    esac
    shift
done

# INSTALL_BASE=$(ccc_home -t)/python
# INSTALL_BASE=$(ccc_home)/Devel/Products/python
INSTALL_BASE=/tmp/MyProducts/python
# INSTALL_BASE=/path/vers/gcc6/python

PYTHON_SRC_TAR=Python-3.7.0.tar.xz
PYTHON_SRC_ORIGIN=$(echo ${PYTHON_SRC_TAR} | cut -d'.' -f1,2,3)
# PYTHON_VERSION = 3.7.0
PYTHON_VERSION=$(echo ${PYTHON_SRC_TAR} | cut -d'-' -f2 | cut -d'.' -f1,2,3)
PYTHON_VERSION_XY=$(echo ${PYTHON_VERSION} | cut -d'.' -f1,2)
CCC_OS=$(ccc_os)
INSTALL_PYTHON_VERSION=${INSTALL_BASE}/${PYTHON_VERSION}/python

PYTHON_SRC_BASE=${PYTHON_SRC_ORIGIN}-${CCC_OS}

## -- Fonctions
usage ()
{
    message_cat "Environnement compilation et installation de Python ${PYTHON_VERSION}"
    message_kv "CC" "${CC}"
    message_kv "CXXFLAGS" "${CXXFLAGS}"
    message_kv "LDFLAGS" "${LDFLAGS}"
    message_kv "INSTALL" "${INSTALL_PYTHON_VERSION}"
    if [[ ${debug} -eq 1 ]]
    then
        message_kv "DEBUG" "${DBG_OPT}"
    fi
}

create_path_names ()
{
    default_path="$1"

    if [[ -d ${default_path} ]]
    then
        echo "Cette version de Python existe déjà (${INSTALL_PYTHON_VERSION})."
        echo " Voulez-vous choisir un autre nom — sera ajouté à ${PYTHON_VERSION} ? oui (défaut) / non"
        read tmp_answer
        answer=${tmp_answer,,}
        if [[ ${answer} = 'oui' ]]
        then
            echo " Nouveau Nom (e.g. ABI0) ? "
            read name_answer
            if [[ -d ${name_answer} ]]
            then
                echo "Le répertoire existe déjà, on sort"
                exit 1
            fi
            PYTHON_VERSION=${PYTHON_VERSION}-${name_answer}
            PYTHON_SRC_BASE=${PYTHON_SRC_ORIGIN}-${name_answer}-${CCC_OS}
            INSTALL_PYTHON_VERSION=${INSTALL_BASE}/${PYTHON_VERSION}/python
            return 1
        fi
    fi
    return 0
}


## -- Debug
DBG_OPT=
if [[ ${debug} -eq 1 ]]
then
    DBG_OPT="--with-pydebug"
fi


## -- Construction du nom du répertoire d'installation
if [[ ${debug} -eq 0 ]]
then
    default="${INSTALL_BASE}/${PYTHON_VERSION}/python"
    create_path_names ${default}
else
    default="${INSTALL_BASE}/${PYTHON_VERSION}-DBG/python"
    create_path_names ${default}
    if [[ $? -eq 0 ]]
    then
        PYTHON_VERSION=${PYTHON_VERSION}-DBG
        PYTHON_SRC_BASE=${PYTHON_SRC_ORIGIN}-DBG-${CCC_OS}
        INSTALL_PYTHON_VERSION=${default}
    fi
fi

## -- Configuration environnement de build
if [[ ! -d ${PYTHON_SRC_BASE} ]]
then
    message_cat "tar xf des sources Python"
    tar xf ${PYTHON_SRC_TAR}
    mv ${PYTHON_SRC_ORIGIN} ${PYTHON_SRC_BASE}
fi
cd ${PYTHON_SRC_BASE}

export GNUPATH=/path/vers/gcc49/gcc/6.4
export CC=${GNUPATH}/bin/gcc
export CXX=${GNUPATH}/bin/g++
export FC=${GNUPATH}/bin/gfortran
export F77=${FC}
export F90=${FC}
GNULIB=${GNUPATH}/lib
GNULIB64=${GNUPATH}/lib64
ABI="-D_GLIBCXX_USE_CXX11_ABI=0"
export CXXFLAGS=${ABI}
export LD_LIBRARY_PATH=${GNULIB}:${GNULIB64}:${LD_LIBRARY_PATH}

LDFLAGS_BASE="-L${GNULIB} -L${GNULIB64}/lib64 -Wl,--rpath=${INSTALL_PYTHON_VERSION}/lib -Wl,--rpath=${GNULIB} -Wl,--rpath=${GNULIB64}"
export LDFLAGS=${LDFLAGS_BASE}
export PATH=${GNUPATH}:${PATH}

## -- Usage
if [[ ${show_usage} -eq 1 ]]
then
    usage
    exit 1
fi

usage
yes_no_question " Environnement d'installation correcte" "oui"
if [[ $? -eq 1 ]]
then
    message_cat "Environnement incorrect. On sort."
    exit 1
fi


## -- Configuration
./configure \
    --prefix=${INSTALL_PYTHON_VERSION} \
    --enable-optimizations \
    --enable-shared \
    --with-ensurepip=yes \
    ${DBG_OPT} \
    CC=${GNUPATH}/bin/gcc \
    CXX=${GNUPATH}/bin/g++ \
    CXXFLAGS="${ABI}" \
    LDFLAGS="${LDFLAGS}"

## -- Compilation et installation
make -j12
make install

# Recopie du module snake dans la distribution python qui vient d'être créée
# cd -
# PYTHON_SITEPACKAGES=${INSTALL_PYTHON_VERSION}/lib/python${PYTHON_VERSION_XY}/site-packages
# cp -rf ./packagesANL/snake ${PYTHON_SITEPACKAGES}/

