#!/bin/bash
$CMDDBG

. ./messages.sh

## -- version
PYTHON_VERSION_XY=3.7
PYTHON_VERSION_XYZ=${PYTHON_VERSION_XY}.0
VIRTUALENV_VERSION_XY=16.3
VIRTUALENV_VERSION_XYZ=${VIRTUALENV_VERSION_XY}.0

## - Chemins des sources et environnement machine

# @Warning : si installation ailleurs, il suffit de changer les variables suivante par les bonnes valeurs
#    * TARGET
#    * ROOT
#    * ROOT_SOURCES
#    * PYTHON_VIRTUALENV_INSTALL_PREFIX_BASE
TARGET=$(ccc_os -G)
#ROOT=$(ccc_home -t)/python
ROOT=$(ccc_home)/Devel/Products/python
#ROOT=/path/vers/gcc6/python
ROOT_SOURCES=$(ccc_home)/Devel/Products/Python


## -- Environnement Python
# pour gcc44
# PYTHON_VIRTUALENV_INSTALL_PREFIX_BASE=/path/vers/gcc/python
# python_installations=$(ls /path/vers/gcc/python | tr '\n' ' ')
# pour gcc 64
PYTHON_VIRTUALENV_INSTALL_PREFIX_BASE=${ROOT}
python_installations=$(ls ${PYTHON_VIRTUALENV_INSTALL_PREFIX_BASE} | tr '\n' ' ')
echo " Noms des installations python existantes : ${python_installations}"
echo "   Défaut = ${PYTHON_VERSION_XYZ}"
echo "   Nom de l'installation choisit ? (<entrée> pour laisser le défaut)"
read answer
if [[ ${answer} = '' ]]
then
    PYTHON_VIRTUALENV_INSTALL_PREFIX=${PYTHON_VIRTUALENV_INSTALL_PREFIX_BASE}/${PYTHON_VERSION_XYZ}
else
    PYTHON_VIRTUALENV_INSTALL_PREFIX=${PYTHON_VIRTUALENV_INSTALL_PREFIX_BASE}/${answer}
fi
PYTHON_INSTALL_PREFIX=${PYTHON_VIRTUALENV_INSTALL_PREFIX}/python
PYTHON_EXEC=${PYTHON_INSTALL_PREFIX}/bin/python${PYTHON_VERSION_XY}

## -- Environnement de l'environnement virtuel qui sera créé : ${ENVIRONMENT_NAME}
# PYTHON_PACKAGES_PREFIX=${ROOT_SOURCES}/${PYTHON_VERSION_XYZ}
PYTHON_PACKAGES_SOURCES=${ROOT_SOURCES}/packages
WHEEL_PACKAGES_SOURCES=${ROOT_SOURCES}/WHEELS_FOR_PYTHON3

PIP_INSTALL_OPTIONS="--no-index --no-cache-dir"

# EXEC_VIRTUALENV=`ccc_home -Dcalc opendist`/sources/Python/${PYTHON_VERSION_XYZ}/virtualenv-${VIRTUALENV_VERSION_XYZ}/virtualenv.py
EXEC_VIRTUALENV=${ROOT_SOURCES}/${PYTHON_VERSION_XYZ}/virtualenv-${VIRTUALENV_VERSION_XYZ}/virtualenv.py

PATH_INSTALL=${PYTHON_VIRTUALENV_INSTALL_PREFIX}/${ENVIRONMENT_FULLNAME}
VIRTUAL_ENV_PYTHON_LIBRARIES=${PATH_INSTALL}/lib
PYTHON_SITEPACKAGES=${PATH_INSTALL}/lib/python${PYTHON_VERSION_XY}/site-packages

BUILD_DIR=${PYTHON_PACKAGES_SOURCES}/../_build/${TARGET}
if [[ ! -d ${BUILD_DIR} ]]
then
    echo "Création répertoire de build"
    mkdir -p ${BUILD_DIR}
fi

go_nogo ()
{
    path_install="$1"
    if [[ -d "${path_install}" ]]
    then
        vname=$(basename ${path_install})
        message_cat "L'environnement virtuel ${vname} est déjà installé."
        yes_no_question "  Voulez-vous l'effacer" "non"
        if [[ $? -eq 1 ]]
        then
            rm -rf ${PATH_INSTALL} 2> /dev/null
        fi
    fi

    usage
    yes_no_question "  Environnement virtuel correct" "oui"
    if [[ $? -eq 1 ]]
    then
        message_kv "Adapter le script installNemesis.sh si nécessaire."
        exit 1
    fi

    # Vérification de l'environnement système
    sys_usage
    yes_no_question "  Environnement système correct" "oui"
    if [[ $? -eq 1 ]]
    then
        message_cat "Options sytème possibles :"
        message_kv "* pour compilateur gcc : --gnu_64"
        message_kv "* pour compilateur intel 17 : --intel_17"
        exit 1
    fi
}
