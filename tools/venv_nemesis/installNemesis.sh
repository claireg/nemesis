 #!/bin/bash
${CMDDBG}

#
# Script d'installation de l'environnement virtuel Nemesis 
#  Le script est 
#    - valable sur TOUTES les plateformes
#    - versionné à l'aide de git 
#  Tout ce fait à partir de path_sources, Python (ccc_home -Dcalc opendist ou $(ccc_home)/Devel/Products/<py_version>)
#

umask 002
VERBOSE=1
compiler=gcc_64
compilers_nickname="g64"
show_usage=0

# Chemins immuables
LLVM_BASE=/path/vers/gcc6/llvm/6.0.0
LLVM_LIB=${LLVM_BASE}/lib

QT_BASE=/path/vers/gcc/qt/5.9.1
export QMAKE=${QT_BASE}/bin/qmake
echo ${QMAKE}

### -- Parsing des arguments
while [ "$#" != 0 ] ; do
    case "$1" in
    -h)
        show_usage=1
        ;;
    --help)
        show_usage=1
        ;;
    --quiet)
        VERBOSE=0
        ;;
    --gcc_64)
        compiler=gun_64
        compilers_nickname="g64"
        ;;
    --intel_17)
        compiler=intel_17
        compilers_nickname="i17"
        ;;
    esac
    shift
done

## -- Environnement virtuel
ENVIRONMENT_NAME=Nemesis
ENVIRONMENT_SHORT_NAME=Nem
ENVIRONMENT_VERSION=0.2
ENVIRONMENT_FULLNAME="${ENVIRONMENT_NAME}_${ENVIRONMENT_VERSION}_${compiler}"
ENVIRONMENT_PROMPT="${ENVIRONMENT_SHORT_NAME}-${ENVIRONMENT_VERSION}-${compilers_nickname}"

### -- Configuration environnement d'exécution
if [[ ${compiler} = "gcc_64" ]]
then
    ABI="-D_GLIBCXX_USE_CXX11_ABI=0"
    GNU=/path/vers/gcc49/gcc/6.4
    GCC64_LIB=${GNU}/lib
    GCC64_LIB64=${GNU}/lib64

    export CC=${GNU}/bin/gcc
    export CXX=${GNU}/bin/g++
    export FC=${GNU}/bin/gfortran
    export F77=${FC}
    export F90=${FC}
    export CFLAGS=${ABI}
    export CXXFLAGS=${ABI}
    export LD_LIBRARY_PATH=${GCC64_LIB}:${GCC64_LIB64}:${LLVM_LIB}:${LD_LIBRARY_PATH}
    #export LDFLAGS="-L${GCC64_LIB} -L${GCC64_LIB64}/lib64 -Wl,--rpath=${GCC64_LIB64}"
    export LDFLAGS="-Wl,--rpath=${GCC64_LIB64} -L${GCC64_LIB64}"
elif [[ ${compiler} = "intel_17" ]]
then
    module load intel/17.0.4.196
    export CC=${CCC_C}
    export CXX=${CCC_CXX}
    export FC=${CCC_F77}
    export F90=${CCC_F77}
    export F77=${CCC_F77}
    export CFLAGS="${CCC_CFLAGS} ${CCC_CXXFLAGS}"
    export CXXFLAGS="${CCC_CFLAGS} ${CCC_CXXFLAGS}"
    # pas de modification de LD_LIBRARY_PATH, le module load le fait
    export LDFLAGS="-L${CXX_INTEL_LIBDIR} -Wl,--rpath=${CXX_INTEL_LIBDIR} -lirc -lsvml -limf "
else
    message_cat "  Compilateur non-reconnu. Choix parmi --gcc_64, intel_17"
fi

### -- Inclusion script bash pour version python, chemin des sources et chemin d'installation
# @NOTE le script messages.sh est inclus dans setEnvironment.sh
. ./setEnvironment.sh

PYTHON_LIBDIR=${PYTHON_INSTALL_PREFIX}/lib

export PATH=${LLVM_BASE}/bin:${QT_BASE}/bin/:$PATH

export QT_SRC_DIR=${QT_BASE}
export QTDIR=${QT_BASE}
export QTINC=${QT_BASE}/include
export QTLIB=${QT_BASE}/lib

# Adaptation de LD_LIBRARY_PATH et LDFLAGS suivant chemin installation environnement virtuel, et python
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${QTLIB}:${PYTHON_LIBDIR}
PYQT_LDFLAGS="${LDFLAGS} -L${PYTHON_LIBDIR} -Wl,--rpath=${PYTHON_LIBDIR}"
PYQT_LDFLAGS="${PYQT_LDFLAGS} -Wl,--rpath=${VIRTUAL_ENV_PYTHON_LIBRARIES} -L${VIRTUAL_ENV_PYTHON_LIBRARIES} "
PYQT_LDFLAGS="${PYQT_LDFLAGS} -Wl,--rpath=${QTLIB} -L${QTLIB} "



## Divers
usage ()
{
    message_cat "Environnement virtuel"
    message_kv "ENVIRONMENT_FULLNAME" "${ENVIRONMENT_FULLNAME}"
    message_kv "ENVIRONMENT_PROMPT" "${ENVIRONMENT_PROMPT}"
    #message_kv "PYTHON_PACKAGES_PREFIX" "${PYTHON_PACKAGES_PREFIX}"
    message_kv "PYTHON_PACKAGES_SOURCES" "${PYTHON_PACKAGES_SOURCES}"
    message_kv "VIRTUAL_ENV_PYTHON_LIBRARIES" "${VIRTUAL_ENV_PYTHON_LIBRARIES}"

    message_cat "Python"
    message_kv "PYTHON_VIRTUALENV_INSTALL_PREFIX" "${PYTHON_VIRTUALENV_INSTALL_PREFIX}"
    message_kv "PYTHON_INSTALL_PREFIX" "${PYTHON_INSTALL_PREFIX}"
    message_kv "PYTHON_SITEPACKAGES" "${PYTHON_SITEPACKAGES}"
    message_kv "EXEC_VIRTUALENV" "${EXEC_VIRTUALENV}"
    message_cat "Installation de ${ENVIRONMENT_FULLNAME}"
    message_kv "PATH_INSTALL" "${PATH_INSTALL}"
    message_kv "PYTHON_EXEC" "${PYTHON_EXEC}"

    message_cat "Compilation"
    message_kv "BUILD_DIR" "${BUILD_DIR}"
}

sys_usage ()
{
    message_cat "Environnement système"
    message_kv "CC" "${CC}"
    message_kv "CXX" "${CXX}"
    message_kv "FC" "${FC}"
    message_kv "CFLAGS" "${CFLAGS}"
    message_kv "CXXFLAGS" "${CXXFLAGS}"
    message_kv "LDFLAGS" "${LDFLAGS}"
    message_kv "PYQT_LDFLAGS" "${PYQT_LDFLAGS}"
    message_kv "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
    message_kv "PATH" "${PATH}"
}

sys_usage_export ()
{
    message_kv_export "CC" "${CC}"
    message_kv_export "CXX" "${CXX}"
    message_kv_export "FC" "${FC}"
    message_kv_export "CFLAGS" "${CFLAGS}"
    message_kv_export "CXXFLAGS" "${CXXFLAGS}"
    message_kv_export "LDFLAGS" "${LDFLAGS}"
    message_kv_export "LD_LIBRARY_PATH" "${LD_LIBRARY_PATH}"
    message_kv_export "PATH" "${PATH}"
} 

if [[ ${show_usage} -eq 1 ]]
then
    sys_usage
    usage
    exit 1
fi

### -- Création environnement virtuel: si existe déjà, demande confirmation à l'utilisateur
create_init_user_virtualenv ()
{
    # virtualenv 16.0.0 installe setuptools 39.1.0
    go_nogo ${PATH_INSTALL}

#     echo "@@@@@ ${PYTHON_EXEC} ${EXEC_VIRTUALENV} --no-site-packages --python=${PYTHON_EXEC} --prompt=\"${ENVIRONMENT_PROMPT}\" ${PATH_INSTALL}"
#     echo "@@@@@ . ${PATH_INSTALL}/bin/activate"

    export LD_LIBRARY_PATH=${PYTHON_INSTALL_PREFIX}/lib:${LD_LIBRARY_PATH}
    if [[ ! -f ${PATH_INSTALL}/bin/activate ]]
    then
        ${PYTHON_EXEC} ${EXEC_VIRTUALENV} --no-site-packages --python=${PYTHON_EXEC} --prompt="${ENVIRONMENT_PROMPT}" ${PATH_INSTALL}
    fi
  
    message_cat "Activation de ${PATH_INSTALL}"
    . ${PATH_INSTALL}/bin/activate

    # -f "file" exists and is a regular file
    # -e "file" exists
    # -L "file" exists and is a symbolic link
    PYTHON_LIBRARY_LINK=libpython${PYTHON_VERSION_XY}m.so
    VIRTUAL_ENV_LIBDIR=${PATH_INSTALL}/lib
    if [[ ! -L ${VIRTUAL_ENV_LIBDIR}/${PYTHON_LIBRARY_LINK} ]]
    then
        cd ${VIRTUAL_ENV_LIBDIR}
        ln -s ${PYTHON_LIBDIR}/${PYTHON_LIBRARY_LINK} ${VIRTUAL_ENV_LIBDIR}/${PYTHON_LIBRARY_LINK}
        # ln -s TARGET DIRECTORY
        ln -s ${VIRTUAL_ENV_LIBDIR}/${PYTHON_LIBRARY_LINK} ${VIRTUAL_ENV_LIBDIR}/${PYTHON_LIBRARY_LINK}.1.0
        # Dans ${PATH_INSTALL}/lib
        #  libpython3.7.so -> libpython3.7m.so.1.0
        #  libpython3.7m.so.1.0 -> ~/Devel/Products/Python/install/python/3.7.0/python/lib/libpython3.7m.so
        cd -
    fi
    chmod g+s ${PATH_INSTALL}/lib/python3.7/site-packages
}

## ---- fonctions pour installer les différents packages

### -- Installation yolk : OBLIGATOIRE pour le script check_installation (vérification de l'environnement construit)
install_yolk ()
{
    message_pkg "Yolk"
    pip install -f file://${PYTHON_PACKAGES_SOURCES} yolk ${PIP_INSTALL_OPTIONS}
    message_pkg_end $? "yolk"
}


### -- Installation PySide2
install_pyside()
{
    # Voir notes_pyside.md.
    # ATTENTION
    #   * Il ne faut pas invoquer ${PYTHON_EXEC} mais python3
    #   * À bien prendre la version de package modifié (pour fonctionnement dans Gaia)
    PYSIDE_VERSION=5.9.0
    PYSIDE_NAME="pyside2-setup-5.9"
    PYSIDE_INSTALL=${PYTHON_SITEPACKAGES}
    if [[ ! -d ${PYSIDE_INSTALL}/PySide2 ]]
    then
        message_pkg "module PySide"

        QT_VERSION_XYZ=`${QMAKE} --version |& tail -n1 | cut -d' ' -f4`
        QT_VERSION_XY=`echo ${QT_VERSION_XYZ} | cut -d'.' -f1,2`

        PYSIDE_FILE_NAME=${PYSIDE_NAME}.zip
        PYSIDE_FILE=${PYTHON_PACKAGES_SOURCES}/${PYSIDE_FILE_NAME}
                
        if [[ $(ccc_os) == "RedHat-7-x86_64" ]]
        then
            PYSIDE_BUILD_BASE=/tmp/MyProducts
            if [[ ! -d ${PYSIDE_BUILD_BASE} ]] ; then
                mkdir -p ${PYSIDE_BUILD_BASE}
            fi
            CMAKE="/path/vers/gcc/cmake/3.11.4/bin/cmake"
        else
            PYSIDE_BUILD_BASE=$(ccc_home -Dcalc -w)
            CMAKE="/path/vers/gcc48/cmake/3.12.1/bin/cmake"
        fi
        PYSIDE_BUILD=${PYSIDE_BUILD_BASE}/${PYSIDE_NAME}

        if [[ -d ${PYSIDE_BUILD} ]]
        then
            rm -fr ${PYSIDE_BUILD}
        fi
        umask 002 && \
        cd ${PYSIDE_BUILD_BASE} && \
        sg opendist -c "unzip ${PYSIDE_FILE}"
        cd ${PYSIDE_BUILD}

        PYSIDE_RPATH=--rpath="${GCC64_LIB64}:${VIRTUAL_ENV_PYTHON_LIBRARIES}:${QTLIB}:${PYTHON_SITEPACKAGES}/PySide2"
        PYSIDE_OPTIONS="--jobs=16 --qmake=${QMAKE} --cmake=${CMAKE} ${PYSIDE_RPATH}"
#        echo ""
#        echo "##################################################"
#        printf "### Pour installer pyside2 : \n"
#        printf "\t* Ouvrir un terminal\n"
#        printf "\t* Aller dans le répertoire des sources\n"
#        message_cmd "cd ${PYSIDE_BUILD_BASE}/${PYSIDE_NAME}"
#        printf "\t* Exporter les variables suivantes\n\n"
#        sys_usage_export
#        printf "\n"
#        printf "\t* Activer l'environnement virtuel\n"
#        message_cmd ". ${PATH_INSTALL}/bin/activate"
#        printf "\t* Exécuter la commande suivante.\n"
#        message_cmd "python3 setup.py install ${PYSIDE_OPTIONS}"

        # Ne s'exécute pas dans le shell si à la place de python3, on met ${PYTHON_EXEC}
        # Si pbm de quota : sg opendist -c python3 setup.py install ${PYSIDE_OPTIONS}
        sg opendist -c "python3 setup.py install ${PYSIDE_OPTIONS}"

    fi
    message_pkg_end $? "PySide"
}

install_pyside_wheel ()
{
    PYSIDE_VERSION=5.9.0
    PYSIDE_NAME="pyside2-setup-5.9"
    PYSIDE_INSTALL=${PYTHON_SITEPACKAGES}
    if [[ ! -d ${PYSIDE_INSTALL}/PySide2 ]]
    then
        message_pkg "module PySide Wheel"
        pip install --no-index ${WHEEL_PACKAGES_SOURCES}/PySide2-5.9.0a1-5.9.1-cp37-cp37m-linux_x86_64.whl
    fi
    message_pkg_end $? "PySide Wheel"
}

install_sip ()
{
    SIP_VERSION=4.19.12
    SIP_NAME="sip-${SIP_VERSION}"
    SIP_INSTALL=${PYTHON_SITEPACKAGES}
    SIP_FILE=${PYTHON_PACKAGES_SOURCES}/${SIP_NAME}.tar.gz
    SIP_BUILD=${BUILD_DIR}/${SIP_NAME}
    if [[ ! -d ${SIP_INSTALL}/PyQt5 ]]
    then
        SIP_OPTIONS=" --sip-module PyQt5.sip \
            --destdir ${SIP_INSTALL} \
            --bindir ${PATH_INSTALL}/bin \
            --incdir ${PATH_INSTALL}/include \
            --platform linux-g++-64"

        message_pkg "SIP"
        if [[ -d ${SIP_BUILD} ]]
        then
            rm -rf ${SIP_BUILD}
        fi
        umask 002 && \
        cd ${BUILD_DIR} && \
        tar xf ${SIP_FILE}
        cd ${SIP_NAME}

        ${PYTHON_EXEC} configure.py ${SIP_OPTIONS} \
                    CC=${GNUPATH}/bin/gcc \
                    CXX=${GNUPATH}/bin/g++ \
                    CXXFLAGS="${CXXFLAGS} -I${PYTHON_INSTALL_PREFIX}/include/python${PYTHON_VERSION_XY} -Wl,--rpath=${GCC64_LIB64} ${PYQT_LDFLAGS}"
                    LDFLAGS="${PYQT_LDFLAGS}"
        make -j16 && \
        make install
    fi
    message_pkg_end $? "SIP"
}


### -- Installation PyQt5
install_pyqt ()
{
    PYQT_VERSION=5.11.2
    PYQT_NAME="PyQt5_gpl-${PYQT_VERSION}"
    PYQT_INSTALL=${PYTHON_SITEPACKAGES}
    if [[ ! -d ${PYQT_INSTALL}/PyQt5 ]]
    then
        install_sip

        PYQT_FILE=${PYTHON_PACKAGES_SOURCES}/${PYQT_NAME}.tar.gz
        PYQT_BUILD=${BUILD_DIR}/${PYQT_NAME}

        # SIP_INSTALL est une variable globale, donc on peut l'utiliser

        PYQT_OPTIONS=" --qmake=${QMAKE} \
            --destdir=${PYQT_INSTALL} \
            --sipdir=${SIP_INSTALL} \
            --sip-incdir=${PATH_INSTALL}/include
            --bindir=${PATH_INSTALL}/bin
            --assume-shared --confirm-license --no-dist-info --verbose --qsci-api"

        # Indispensable sinon ne trouve pas .../site-package/PyQt5 ...
        export PYTHONPATH=${PYTHON_SITEPACKAGES}:$PYTHONPATH

        message_pkg "PyQt5"

        if [[ -d ${PYQT_BUILD} ]]
        then
            rm -rf ${PYQT_BUILD}
        fi
        umask 002 && \
        cd ${BUILD_DIR} && \
        tar xf ${PYQT_FILE}
        cd ${PYQT_BUILD}

        ${PYTHON_EXEC} configure.py ${PYQT_OPTIONS} \
            CC=${GNUPATH}/bin/gcc \
            CXX=${GNUPATH}/bin/g++ \
            LDFLAGS="${tmp_LDFLAGS}"
        echo "-- Après PyQt configure.py"
        make -j16 && \
        make install
    fi
    message_pkg_end $? "PyQt5"

    # La compilation et l'installation de PyQt fonctionne mais manque un symbole …
    # ImportError: .../Nemesis_0.4/lib/python3.6/site-packages/PyQt5/QtCore.so: undefined symbol: _ZN16QMetaEnumBuilder11setIsScopedEb
    # Est-ce parce que j'essaye de compiler un PyQt pour Qt 5.11.2 et que je force un Qt 5.9.1 ???
}



### -- Installation matplotlib
install_matplotlib ()
{
    message_pkg "modules matplotlib"

    # La compilation avec la mkl 18.0.0.128 ne fonctionne pas
    # module load mkl
    module load mkl/17.0.4.196

    NUMPY_PIP_COMPILER_RPATH=""
    if [[ ${compiler} = 'intel_17' ]]
    then
        NUMPY_PIP_COMPILER_RPATH=--global-option="-R${C_INTEL_LIBDIR}"
    fi
    NUMPY_PIP_MKL_RPATH=--global-option="-R${MKL_LIBDIR}"
    NUMPY_PIP_OPT="--global-option=build_ext ${NUMPY_PIP_MKL_RPATH} ${NUMPY_PIP_COMPILER_RPATH}"

    SCIPY_PIP_PARALLEL_BUILD="-j 8"
    SCIPY_PIP_OPT="--global-option=build --global-option=${SCIPY_PIP_PARALLEL_BUILD}"
    LDFLAGS_SCIPY="${LDFLAGS} -shared"
    export LDFLAGS="${LDFLAGS_SCIPY} ${LDFLAGS}"
    # pip install -f file://${PYTHON_PACKAGES_SOURCES} numpy>=1.14.0 ${PIP_INSTALL_OPTIONS} ${NUMPY_PIP_OPT} && \
    # Test faux si un refait entièrement l'environnement.
    # @FIXME @TODO
    if [[ ! -d ${PYTHON_SITEPACKAGES}/numpy ]]
    then
        pip install ${PYTHON_PACKAGES_SOURCES}/numpy-1.15.4.zip -v ${PIP_INSTALL_OPTIONS} ${NUMPY_PIP_OPT}
    fi
    pip install -f file://${PYTHON_PACKAGES_SOURCES} backports.ssl_match_hostname ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} tornado>=5.0 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} pyparsing ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} Cython==0.28.4 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} scipy==1.1.0 ${PIP_INSTALL_OPTIONS} ${NUMPY_PIP_OPT} && \
    # pip install -f file://${PYTHON_PACKAGES_SOURCES} matplotlib==3.0.2 ${PIP_INSTALL_OPTIONS}
    pip install -f file://${PYTHON_PACKAGES_SOURCES} kiwisolver ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} python-dateutil ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} cycler ${PIP_INSTALL_OPTIONS} && \
    pip install --no-index ${WHEEL_PACKAGES_SOURCES}/matplotlib-3.0.2-cp37-cp37m-linux_x86_64.whl

	# Si l'installation plante, il se peut que cela indique un pbm avec setuptools et en fait le problème est sur un autre package.
	# Il faut installer le package à la main, après avoir activer le nouvel environnement virtuel, en essayant une des deux solutions suivantes :
	#  	1. pip install -f file:///ccc/S/dsku/vega/voluserS/hal1/home/s9/guilbaud/Devel/Products/Python/packages mon_package --no-index --no-cache-dir
	#	2. Détare dans /tmp, puis python setup.py
	#	ATTENTION à l'environnement (CC, CXX, …)
    message_pkg_end $? "modules matplotlib. Est-ce que la mkl est bien chargée ? Sinon installer à la main pour voir le package manquant"
}


### -- Installation Sphinx
# @WARNING : obligation d'utiliser PYTHON_PACKAGES_SOURCES car sinon trop de niveau de liens symboliques
install_sphinx ()
{
    # Notes : les modules pour markdown dans cette fonction sont compatibles avec Sphinx
    # rst.linker, utilisé par plusieurs packages pour la documentation
    message_pkg "modules Sphinx"
    
    pip install --pre -f file://${PYTHON_PACKAGES_SOURCES} pytest-runner ${PIP_INSTALL_OPTIONS}  && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} CommonMark ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} Pygments ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} MarkupSafe ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} Jinja2 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} docutils ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} Pillow>=5.0.0 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} sphinx-gallery ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} latexmk.py ${PIP_INSTALL_OPTIONS} && \
    pip install --pre -f file://${PYTHON_PACKAGES_SOURCES} Sphinx==1.8.3 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} recommonmark==0.5.0 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} sphinx_rtd_theme ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} colorlog ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} python-docs-theme ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} ${PYTHON_PACKAGES_SOURCES}/hieroglyph-master.zip ${PIP_INSTALL_OPTIONS} && \
    # @HANDMADE : utile pour la documentation générale. Non utilisé par le projet
    #pip install -f file://${PYTHON_PACKAGES_SOURCES} ${PYTHON_PACKAGES_SOURCES}/python-packaging-user-guide-master.zip ${PIP_INSTALL_OPTIONS} && \
    # @HANDMADE : à faire après installation de tous les packages. Utile pour ???
    # pip install -f file://${PYTHON_PACKAGES_SOURCES} rst.linker ${PIP_INSTALL_OPTIONS} && \
    # pip install ${PYTHON_PACKAGES_SOURCES}/numpydoc-master.zip ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} numpydoc ${PIP_INSTALL_OPTIONS} && \
    # à la demande de la doc de numpy-1.15.4
    pip install -f file://${PYTHON_PACKAGES_SOURCES} sphinxcontrib-trio ${PIP_INSTALL_OPTIONS}

    message_pkg_end $? "Sphinx"
}

### -- Installation pylint
install_pylint ()
{
    message_pkg "modules pylint"
    
    pip install -f file://${PYTHON_PACKAGES_SOURCES} lazy-object-proxy ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} wrapt ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} backports.functools_lru_cache ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} configparser ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} singledispatch ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} mccabe ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} astroid>=1.6.5 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} pylint==1.9.2 ${PIP_INSTALL_OPTIONS}
    
    message_pkg_end $? "pylint"
}

### -- Installation IPython
install_ipython ()
{
    message_pkg "modules ipython"

    # colorspacious est utile pour générer la doc matplotlib
    
    pip install -f file://${PYTHON_PACKAGES_SOURCES} setuptools==40.7.2 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} jedi>=0.12.1 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} decorator>=4.3.0 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} pickleshare>=0.7.4 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} simplegeneric>=0.8.1 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} traitlets>=4.3.2 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} colorspacious --no-index --no-cache-dir
    pip install -f file://${PYTHON_PACKAGES_SOURCES} prompt_toolkit>=1.0.15 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} python-dateutil==2.6.1 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} jupyter_client ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} ipykernel>=4.8.2 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} ipython ${PIP_INSTALL_OPTIONS} && \
    # Pour IPython/Jupyter dans Pycharm
    pip install -f file://${PYTHON_PACKAGES_SOURCES} sympy ${PIP_INSTALL_OPTIONS}
    
    message_pkg_end $? "ipython"
}


install_tests_420 ()
{
    # Installation de attrs, pluggy et pytest à la main (ce mettre dans /tmp, detarrer les sources, activer l'environnement virtuel
    #   python setup.py build
    #   python setup.py install
    # NB : l'erreur peut-être sur la version de setuptools, mais si on installe le pkg à la main, on remarque que l'erreur
    #   est ailleurs.
    message_pkg "modules tests"
    pip install -f file://${PYTHON_PACKAGES_SOURCES} nose ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} py ${PIP_INSTALL_OPTIONS} && \
    # @HANDMADE : À faire à la main
    # pip install --no-index -v -f file://${PYTHON_PACKAGES_SOURCES} --log /tmp/MyProducts/attrs.log attrs && \
    pip install --no-index ${WHEEL_PACKAGES_SOURCES}/attrs-18.1.0-py2.py3-none-any.whl && \
    # attention, pluggy nécessite atomicwrites qui nécessite more-itertools ...
    pip install -f file://${PYTHON_PACKAGES_SOURCES} atomicwrites ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} more-itertools ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} pluggy==0.8.1 ${PIP_INSTALL_OPTIONS} && \
    # @HANDMADE : À faire à la main ?
    pip install -f file://${PYTHON_PACKAGES_SOURCES} pluggy==0.8.1 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} pytest==4.2.0 ${PIP_INSTALL_OPTIONS}
    message_pkg_end $? "modules tests"
}


install_tests ()
{
	# Trois packages (attrs, pluggy et pytest) posent problème à l'installation. Pour réussir leur installation, 2 solutions
	#	1. Installation manuelle  à la main 
	#		Se mettre dans /tmp, detarrer les sources, activer l'environnement virtuel
    #   		python setup.py build
    #   		python setup.py install
    # 	2. Création de wheels pour ne plus rien avoir à faire à la main
    #		python setup.py bdist_wheel
    # 		cp dist/* ${WHEEL_PACKAGES_SOURCES}
	#		# Ensuite dans le script installNemesis.sh
	#		pip install --no-index ${WHEEL_PACKAGES_SOURCES}/XXX-py2.py3-none-any-whl
	# 
    # NB : l'erreur peut-être sur la version de setuptools, mais si on installe le pkg à la main, on remarque que l'erreur
    #   est ailleurs.
    # La séquence suivante fonctionne sans intervention humaine
    message_pkg "modules tests (pytest 3.8.2)"
    pip install -f file://${PYTHON_PACKAGES_SOURCES} nose ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} py ${PIP_INSTALL_OPTIONS} && \
    pip install --no-index ${WHEEL_PACKAGES_SOURCES}/attrs-18.1.0-py2.py3-none-any.whl && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} atomicwrites ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} more-itertools ${PIP_INSTALL_OPTIONS} && \
    pip install --no-index ${WHEEL_PACKAGES_SOURCES}/pluggy-0.8.1-py2.py3-none-any.whl && \
    pip install --no-index ${WHEEL_PACKAGES_SOURCES}/pytest-3.8.2-py2.py3-none-any.whl
    message_pkg_end $? "modules tests"
}


install_profiling ()
{
    message_pkg "modules profiling"
    pip install -f file://${PYTHON_PACKAGES_SOURCES} psutil ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} memory_profiler ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} graphviz ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} snakeviz ${PIP_INSTALL_OPTIONS}
    message_pkg_end $? "modules profiling"
}


install_dep ()
{
    # utiliser dans beaucoup de packages - permet d'être sûr d'avoir la même version partout
    message_pkg "six, setuptools_scm"
    pip install -f file://${PYTHON_PACKAGES_SOURCES} six>=1.11.0 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} packaging ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} appdirs ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} setuptools>=40.7.2 ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} setuptools_scm>=3.0.6 ${PIP_INSTALL_OPTIONS} 
    message_pkg_end $? "six, setuptools_scm"
}

install_gdbgui ()
{
    # gdbgui a besoin de gevent, mais si il le trouve dans ${PYTHON_PACKAGES_SOURCES},
    #  l'installation se passe très bien sans intervention manuelle
    message_pkg "gdbgui"
    pip install ${PYTHON_PACKAGES_SOURCES}/pygdbmi-master.zip ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} greenlet ${PIP_INSTALL_OPTIONS} && \
    pip install -f file://${PYTHON_PACKAGES_SOURCES} cffi ${PIP_INSTALL_OPTIONS} && \
    pip install --no-index -f file://${PYTHON_PACKAGES_SOURCES} ${WHEEL_PACKAGES_SOURCES}/gdbgui-0.13.0.0-py3-none-any.whl 
    message_pkg_end $? "gdbgui"
}

### -- Installations des différents packages

create_init_user_virtualenv # OK

install_dep # OK
install_yolk # OK
install_pyside
if [[ ${compiler} = "intel_17" ]]
then
	install_pyqt # OK (manque lib à l'exécution ...)
fi
# Ne fonctionne pas en intel 17 car le llvm sur lequel repose pyside2 a été compiler pour du gcc6
# if [[ ${compiler} = "gcc_64" ]]
# then
	#install_pyside_wheel # OK
# fi

install_matplotlib 
install_sphinx # OK
install_pylint # OK
install_ipython # OK
install_profiling # OK
install_tests # OK avec wheels
if [[ $(ccc_os) == "RedHat-7-x86_64" ]]
then
    install_gdbgui # OK
fi

message_pkg "Fin installation environnement ${ENVIRONMENT_FULLNAME}"

# Pour créer les wheels : 
# python setup.py bdist_wheel
# pip install dist/XXXX.whl

exit 0
