#!/usr/bin/env bash


for dlib in $(find . -name "*.so");
do
    abi=$(readelf --file-header ${dlib} | grep -Hin "Version ABI" | tr -d ' ' | cut -d':' -f3- | cut -d':' -f2);
    if [[ ${abi} = '1' ]]
    then
        echo "WRONG ABI in ${dlib}"
    fi
done
