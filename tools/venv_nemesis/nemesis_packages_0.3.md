##### Vérification de l'environnement virtuel Nemesis_0.3
	~/Devel/Products/Python/install/3.7.0
* yolk : 0.4.3
* numpy : 1.14.5
* scipy : 1.1.0
* matplotlib : 2.2.2
* recommonmark : 0.4.0
* sphinx : 1.7.5
* pylint : 1.9.2
* nose : 1.3.7
* pytest : 3.4.1
* psutil : 5.4.6
* snakeviz : 0.4.2
* memory_profiler : 0.52.0
* graphviz : 0.8.4

##### Backend matplotlib par défaut : TkAgg

##### Nemesis_0.3 : Liste des packages disponibles (91 packages)
	~/Devel/Products/Python/install/3.7.0
* yolk 0.4.3
* wrapt 1.10.11
* wheel 0.31.1
* Werkzeug 0.11.15
* wcwidth 0.1.7
* urllib3 1.23
* traitlets 4.3.2
* tornado 5.0.2
* sphinxcontrib-websupport 1.1.0
* Sphinx 1.7.5
* sphinx-rtd-theme 0.4.0
* sphinx-gallery 0.2.0
* snowballstemmer 1.2.1
* snakeviz 0.4.2
* six 1.11.0
* singledispatch 3.4.0.3
* simplegeneric 0.8.1
* setuptools 28.8.0
* setuptools-scm 2.1.0
* scipy 1.1.0
* requests 2.19.1
* recommonmark 0.4.0
* pyzmq 17.0.0
* pytz 2018.5
* python-socketio 2.0.0
* python-engineio 2.2.0
* python-dateutil 2.6.1
* pytest 3.4.1
* pytest-runner 4.2
* PySide2 5.9.0a1
* pyparsing 2.2.0
* pylint 1.9.2
* Pygments 2.2.0
* pygdbmi 0.8.3.0
* py 1.5.4
* ptyprocess 0.5.2
* psutil 5.4.6
* prompt-toolkit 1.0.15
* pluggy 0.6.0
* pip 10.0.1
* Pillow 5.2.0
* pickleshare 0.7.4
* pexpect 4.6.0
* parso 0.3.1
* packaging 17.1
* numpydoc 0.8.0.dev0
* numpy 1.14.5
* nose 1.3.7
* memory-profiler 0.52.0
* mccabe 0.6.1
* matplotlib 2.2.2
* matplotlib-colorbar 0.3.5
* MarkupSafe 1.0
* lazy-object-proxy 1.3.1
* latexmk.py 0.4
* kiwisolver 1.0.1
* jupyter-core 4.4.0
* jupyter-client 5.2.3
* Jinja2 2.10
* jedi 0.12.1
* itsdangerous 0.24
* isort 4.2.15
* ipython 6.2.1
* ipython-genutils 0.2.0
* ipykernel 4.8.2
* imagesize 1.0.0
* idna 2.7
* greenlet 0.4.14
* graphviz 0.8.4
* gevent 1.3.5
* gdbgui 0.13.0.0
* Flask 0.12.2
* Flask-SocketIO 2.9.6
* Flask-Compress 1.4.0
* docutils 0.14
* decorator 4.3.0
* Cython 0.28.4
* cycler 0.10.0
* configparser 3.5.0
* CommonMark 0.5.4
* colorspacious 1.1.2
* click 6.7
* chardet 3.0.4
* certifi 2018.4.16
* backports.ssl-match-hostname 3.5.0.1
* backports.functools-lru-cache 1.5
* Babel 2.6.0
* attrs 18.1.0
* astroid 1.6.5
* appdirs 1.4.3
* alabaster 0.7.11

