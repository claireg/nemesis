#!/bin/bash

MSG_0="\033[32m"
MSG_1="\033[34m"
MSG_ERROR="\033[1;45;41m"
MSG_END="\033[00m"


## -- fonctions pour les affichages sur la sortie standard
message_cat ()
{
  if [[ ${VERBOSE} -eq 1 ]]
  then
    catName="$1"
    echo -e ""
    echo -e "${MSG_0}++ ${catName}${MSG_END}"
  fi
}

message_kv ()
{
  if [[ ${VERBOSE} -eq 1 ]]
  then
    msgKey="$1"
    msgValue="$2"
    echo -e "${MSG_1}${msgKey}${MSG_END} = ${msgValue}"
  fi
}

message_kv_export ()
{
  if [[ ${VERBOSE} -eq 1 ]]
  then
    msgKey="$1"
    msgValue="$2"
    echo -e "${MSG_0}export ${msgKey}=\"${msgValue}\"${MSG_END}"
  fi
}

message_pkg ()
{
  if [[ ${VERBOSE} -eq 1 ]]
  then
    pkgName="$1"
    echo -e ""
    echo -e "${MSG_0}################################################################################"
    echo -e "### Installation ${pkgName}${MSG_END}"
  fi
}

message_pkg_end ()
{
  ret=$1
  msg="$2"
  if [[ ${ret} != 0 ]]
  then
    echo -e "${MSG_ERROR}Erreur lors de l'installation de ${msg} => on sort${MSG_END}"
    exit 1
  fi
}

message_cmd ()
{
    msg="$1"
    echo -e "        ${MSG_0}${msg}${MSG_END}"
    echo -e ""
}

yes_no_question ()
{
    question="$1"
    tmp_default="$2"

    default=${tmp_default,,}
    if [[ ${default} = 'oui' ]]
    then
        default_text="oui (défaut) / non"
    else
        default_text="oui / non (défaut)"
    fi

    message_kv "${question} ? ${default_text}"
    read tmp_answer
    answer=${tmp_answer,,}
    if [[ ${answer} = "" ]]
    then
        answer=${default}
    fi

    if [[ ${answer} = ${default} ]]
    then
        # la réponse est la réponse par défaut (donc attendue)
        return 0
    else
        # la réponse n'est pas la réponse par défaut
        return 1
    fi
}