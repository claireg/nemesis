#!/bin/bash

script_format="../extern/git-cmake-format-master/git-cmake-format.py"
bin_git="/usr/bin/git"
bin_clang_format="/path/vers/gcc6/llvm/6.0.0/bin/clang-format"

if [[ -z ${VIRTUAL_ENV} ]]
then
	. ~/Devel/Products/Python/install/3.7.0/Nemesis_0.3/bin/activate
fi


python3 -d ${script_format} --cmake ${bin_git} ${bin_clang_format} -style=file -ignore=Catch2-master
