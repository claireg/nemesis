#!/usr/bin/env bash

# SC en bash équivalent aux PEP en python

# Remplacement de for file in $(ls -1) ; do ... ; done – SC2045
shopt -s globstar
for file in ./* ; do
  echo "* ${file}"
done

# Remplacement de $(ls -1 ${e}) — SC2012
files=$(find tools/* -maxdepth 1)
echo "FILES=#${files}#"