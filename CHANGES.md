# Version 0.1.6

lundi 5 octobre 2020 à 13:30 par Claire Guilbaud

## Logs

BLD modifications suite update CMake et recette Spack

# Version 0.1.4

lundi 21 septembre 2020 à 10:36 par Claire Guilbaud

## Logs

* ENH Logs Python
* ENH Lecteur / Affichage / Extraction fichiers Psyché
* MAINT Refactoring à pls endroits pour plus de cohérence
* @DOC, @TST

# Version 0.1.1

jeudi 19 décembre 2019 à 09:45 par Claire Guilbaud

## Logs 

ENH Passage à CMake/Spack
* @ DOC
* Simplification compilation exemples et tests C++ (`CMake`)
* Changement version Python, matplotlib, numpy, Sphinx et pytest

# Version 0.1.1

jeudi 19 décembre 2019 à 09:45 par Claire Guilbaud

## Logs 

ENH Passage à CMake/Spack
* @ DOC
* Simplification compilation exemples et tests C++ (`CMake`)
* Changement version Python, matplotlib, numpy, Sphinx et pytest

# Version 0.1.1

jeudi 27 juin 2019 à 14:35 par Claire Guilbaud

## Logs 

DEV gcc 8.3 + ENH lecture .psy + BUG ajustement boîte d'axes

* Nouvelles fonctions pour lire de façon générique les fichiers .psy
* Exposant des labels des axes en bout d'axes pour des valeurs comprises
entre 10^-5 et 10^5
* Ajustement de la boîte d'axes : il faut utiliser ``switch_axes``
pour ne pas avoir à le gérer à la main


# Version 0.1.0

lundi 17 juin 2019 à 13:58 par Claire Guilbaud

## Logs 

MAINT réorganisation projet en vue passage en opensource
* @TST
* @DATA
* @DOCS
* EXM


# Version 0.0.8

mardi 28 mai 2019 à 15:47 par Claire Guilbaud

## Logs 

ENH zlim, view_init + MAINT réorg doc, en-têtes C++

ENH
* Ajout fonction C++ `view_init(elevation, azimuth)` pour axes 3D
* Ajout fonction C++ `zlim()` et `zlim(bottom, top)` pour axes 3D


# Version 0.0.7

lundi 20 mai 2019 à 11:51 par Claire Guilbaud

## Logs 

ENH transparence + ENH divers

ENH transparence
* Possibilité de modifier la transparence d'une ou plusieurs collections
d'une boite d'axes

ENH PseudoLogMathTextSciFormatter et LogLogMathTextSciFormatter
* Pour que lorsqu'on balade le curseur dans la boite d'axes
les valeurs soient lisibles

ENH ajustement subplots boîtes axes 3D

ENH zlabel

@TST
@DOC

# Version 0.0.6

lundi 13 mai 2019 à 11:33 par Claire Guilbaud

## Logs 

BUG : raised_contourf zscale en log (z avec des 0.0)

BUG
* Ajout argument `masked_value=` pour raised_contourf pour «supprimer»
les valeurs qui ont dû mal à passer en log. ATTENTION, les valeurs
<= masked_value sont remplacées par masked_value, et non masquées.
* Comportement maintenant cohérent entre `raised_contourf`
et `*_contour*`

# Version 0.0.5

lundi 06 mai 2019 à 16:50 par Claire Guilbaud

## Logs 

BUG raised_contourf avec zscale='log' / MAINT refactoring module pyplotcea

BUG
* Si zscale='log', z = log10(z) et normalisation linéaire valeur / couleur

MAINT
* Simplification code pyplotcea
* Les méthodes internes ne sont plus visibles

* @DOC
* @EXM
* @TST

# Version 0.0.4

vendredi 26 avril 2019 à 15:58 par Claire Guilbaud

## Logs 

MAINT refactoring `PyPlotCEA.*_*contour*`

* Homégénéisation des paramètres des fonctions `PyPlotCEA.*_*contour*`
* @TST
* @DOC

# Version 0.0.3.1

mardi 23 avril 2019 à 15:08 par Claire Guilbaud

## Logs 

ENH / MAINT / REL : fct gén readdata / post_actions

ENH : fonction générique readdata
MAINT : user a accès à des fct auparavant masquées
REL : modif procédures de déploiement suite version 0.0.3.0

# Version 0.0.3

vendredi 19 avril 2019 à 11:15 par Claire Guilbaud

## Logs 

ENH log pour le z : traitement spécial

ENH
* possibilité de passer en log les axes 3D
* `raised_conturf` : Si `zscale = 'log'` : traitement spéciale pour les données s'étalant sur trop de décades
* répercution de ce traitement pour `logaritmic_contourf` et `logaritmic_tricontourf`
* @DOC

# Version 0.0.2

jeudi 04 avril 2019 à 10:56 par Claire Guilbaud

## Logs 

ENH graphiques 3D / MAINT / BUG

* MAINT : Correction segv tests C++ (bug `add_site` et `print_sys_path`)
* ENH affichage de graphiques 3D
* @DOC : existe un glossaire

# Version 0.0.1i (0.0.1.9)

jeudi 21 févr. 2019 à 09:46 par Claire Guilbaud

## Logs 

ENH nuages de points et multi-afficheurs

* ENH
    * Affichage nuage de points : wrapping de `pyplot.scatter`
    * Mode multi-afficheurs (Création, activation et destruction)
    * `logarithmic_[tri]contour[f]` : masquage des valeurs <= 0 dans z 
* @TST
* @DOC

# Version 0.0.1h (0.0.1.8)

mercredi 13 févr. 2019 à 10:42 par Claire Guilbaud

## Logs 

ENH isolignes et cartes

* MAINT : Isolignes : plus d'iso de tracer entre deux couleurs sur une carte
* ENH obtention des niveaux d'une carte ou des isolignes après un appel à `*contour[f]*`
    * Données stockées dans un `numpy.array` en Python, et dans un vector<double> en C++.
* @DOC

# Version 0.0.1g (0.0.1.7)

lundi 04 févr. 2019 à 10:00 par Claire Guilbaud

## Logs 

BUG terminaison en erreurs lors du `PyFinalizeEx`

* @DOC pres generale : ajout slides pour les perf

# Version 0.0.1f (0.0.1.6)

mercredi 30 janv. 2019 à 10:43 par Claire Guilbaud

## Logs 

BUGS corrections bugs autour des isolignes

* BUGS : Isolignes
    * Ticks de la colorbar si la liste des niveaux contient des valeurs <= 0.
    * Correction application np.log10 sur des valeurs inconnues (qui peuvent être <= 0.)
* @DOC

# Version 0.0.1e (0.0.1.5)

jeudi 24 janv. 2019 à 11:30 par Claire Guilbaud

## Logs 

ENH contour avec triangulation implicite
    
La triangulation est faite par matplotlib en utilisant la triangulation de Delaunay.
* Python : ecriture méthodes `*_tricontour*` (appels de `matplotlib.tricontour*`)
* C++ : wrapping de méthodes python `*_tricontour*`
* @DOC (méthodes ajoutées et ToDo)
* @TST C++
* @TST Python

# Version 0.0.1d

jeudi 10 janv. 2019 à 15:27 par Claire Guilbaud

## Logs 

MAINT nv env python + ENH modif titre fenetre
    
* ENH : possibilité de modifier le titre de la fenêtre graphique via
  `set_window_title`.
* MAINT : nouvel environnement python dans opendist en vue distribution
  pour tous.


# Version 0.0.1c

lundi 07 janv. 2019 à 10:44 par Claire Guilbaud

## Logs 

DEV Iso-Lignes

Développement nouvelle fonctionnalité pour l'affichage d'iso-lignes (colorées ou non) pour un mapping linéaire ou logarithmique
des données

MAJ Cartes de couleurs

Possibilités d'utiliser une forme générique afin de pouvoir définir l'ensemble des arguments autorisés en Python
Renommage de certaines fonctions (warning affiché à l'utilisation)

# Version 0.0.1b

mardi 20 nov. 2018 à 15:03 par Claire Guilbaud

## Logs 

CONF nemesis-0.0.1 : /path/vers/share/scripts/nemesis-0.0.1

DEV : Carte de couleurs («3D Plan»)

Développement nouvelles fonctionnalités pour l'affichage de cartes de couleurs avec mapping linéaire ou logarithmique 
des données.

* Si mapping logarithmique et les données contiennent des valeurs nulles (ou quasi nulles). Ces valeurs sont masquées :
    * permet de ne pas avoir de message d'erreurs
    * la légende de couleurs de la carte est correcte

# Version 0.0.1a

mardi 20 nov. 2018 à 15:00 par Claire Guilbaud

## Logs 



# Version 0.0.0

mardi 25 sept. 2018 à 14:21 par Claire Guilbaud

## Logs 

CONF nemesis-0.0.0 : /path/vers/share/scripts/nemesis-0.0.0"

TST : mise en place tests C++ / @BUG check ptr fct nemesis
    
* La mise en place des tests a permise de trouver des coquilles dans
  matplotlibcpp, mais aussi un soucis de paramètre non passé à
  matplotlib



