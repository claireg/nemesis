cmake_minimum_required(VERSION 3.11)

if(${CMAKE_VERSION} VERSION_LESS 3.12)
    cmake_policy(VERSION ${CMAKE_VERSION_MAJOR}.${CMAKE_VERSION_MINOR})
endif()

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

include(NemesisUtilities)
include(NemesisVersionConfig)

# Pour qu'à l'installation le python à utiliser soit fixé
# Pour la distribution / création de version, il faut que
# * SET_PYTHON_ENV soit à OFF
# * que ENV{PATH} soit égal à "/THE/PATH/VERS/ENV/PYTHON/bin:$ENV{PATH}"
# En effet, la recette Spack modifie les ces deux variables / chemins.
#set(SET_PYTHON_ENV OFF)
#set(SET_PYTHON_ENV ON)
#if(${SET_PYTHON_ENV})
#    set(ENV{PATH} "/THE/PATH/VERS/ENV/PYTHON/bin:$ENV{PATH}")
#endif()

# CMAKE_PREFIX_PATH est null dans un environnement virtuel classique
if(${USE_VIRTUAL_ENV})
    message(STATUS "### -> use_virtual_env")
    set(ENV{PATH} "$ENV{VIRTUAL_ENV}/bin:$ENV{PATH}")
    message(STATUS "VIRTUAL_ENV = $ENV{VIRTUAL_ENV}")
    message(STATUS "PATH = $ENV{PATH}")
endif()

# -----------------------------------------------
# -- Python
set(PYTHON_VERSION "3.8")
find_package (Python3 ${PYTHON_VERSION} EXACT
    COMPONENTS Interpreter Development NumPy)
# Valable pour un environnement python dans un python, dans un environnement virtuel ou dans un env spack,
set(SITE_PACKAGES "${Python3_SITELIB}")
# --- Moche
# set(NUMPY_INCLUDE_DIR "${SITE_PACKAGES}/numpy/core/include/numpy")
# --- Fonctionne mais compliqué pour rien
# _set_from_python(NUMPY_INCLUDE_DIR      "import numpy.distutils.misc_util; print(numpy.distutils.misc_util.get_numpy_include_dirs())")
# --- Fonctionne et est élégant si au moins python 3.8
# utilisation de Python3_NumPy_INCLUDE_DIRS (voir doc cmake FindPython)
# set(NUMPY_INCLUDE_DIR "${Python3_NumPy_INCLUDE_DIRS}")
# pour récupérer le site_packages :
#  os.path.abspath(os.path.join(matplotlib.get_data_path(), '..', '..'))
message(STATUS "#######################################################")
status(Python3_INCLUDE_DIRS)
status(Python3_LIBRARIES)
status(Python3_LIBRARY_DIRS)
status(Python3_SITELIB)
status(Python3_NumPy_INCLUDE_DIRS)
status(Python3_NumPy_FOUND)
message(STATUS " ... Variables maison à ne plus utiliser")
message(STATUS "     * SITE_PACKAGES -> Python3_SITELIB")
message(STATUS "     * NUMPY_INCLUDE_DIR -> Python3_NumPy_INCLUDE_DIRS")
message(STATUS "#######################################################")

# -----------------------------------------------
get_filename_component(Nemesis_ROOT_DIR "${CMAKE_CURRENT_LIST_DIR}/../.." ABSOLUTE)

# -----------------------------------------------
# Gestion RPATH pour lib.so (notamment avec le bon python et les bonnes lib gcc)
# ces lignes ne sont utiles que si le compilateur ou gcc ne sont pas installés dans des paths classiques
# et qu'ils n'ont pas été installé par spack !
# selon les recommendations spack
set(CMAKE_MACOSX_RPATH  1)
set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# les miens
set(CMAKE_SKIP_RPATH FALSE)
set(CMAKE_SKIP_INSTALL_RPATH FALSE)

list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
endif("${isSystemDir}" STREQUAL "-1")

# -----------------------------------------------
# Flags Compilateur ou Projet pour les exe/lib utilisant nemesis
set(Nemesis_COMPILE_FEATURES cxx_std_17)
# set(Nemesis_COMPILE_OPTIONS -fconcepts)

# Les headers
set(tmp_headers "matplotlibcpp_cea.hpp;matplotlibcpp.hpp;matplotlibcpp_opensource.hpp;utilities.h")
set(Nemesis_HEADERS "")
foreach(header ${tmp_headers})
    list(APPEND Nemesis_HEADERS ${Nemesis_ROOT_DIR}/include/${header})
endforeach()
set(Nemesis_HEADERS_NO_PATH "nemesiscppconfig.h")

# Pour le debug : -DDEBUG seulement en mode debug
# $<CONFIG:Debug> : 1 si la config est Debug
# $<1:DEBUG> : ajoute -DDEBUG à la ligne de compilation
set(Nemesis_COMPILE_DEFINITIONS WITH_VIRTUALENV)
list(APPEND Nemesis_COMPILE_DEFINITIONS USE_VARIADIC_TEMPLATES_ARGS)
list(APPEND Nemesis_COMPILE_DEFINITIONS CEA_ADDON)
list(APPEND Nemesis_COMPILE_DEFINITIONS $<$<CONFIG:Debug>:DEBUG>)

# les -I
set(Nemesis_INCLUDE_DIRS $<INSTALL_INTERFACE:include>)
list(APPEND Nemesis_INCLUDE_DIRS $<BUILD_INTERFACE:${Nemesis_ROOT_DIR}/include>)
list(APPEND Nemesis_INCLUDE_DIRS ${Python3_INCLUDE_DIRS})
list(APPEND Nemesis_INCLUDE_DIRS ${Python3_NumPy_INCLUDE_DIRS})
# list(APPEND Nemesis_INCLUDE_DIRS ${NUMPY_INCLUDE_DIR})

# les -L
set(Nemesis_LIBRARIES ${Python3_LIBRARIES})
list(APPEND Nemesis_LIBRARIES -lstdc++fs)
