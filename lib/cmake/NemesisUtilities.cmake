
# -----------------------------------------------
# Fonctions

# Affichage de toutes les variables (dans le cache cmake) avec leurs valeurs
function(show_all_variables)
    message(STATUS "\n*** dump start cmake variables ***")
    get_cmake_property (_all_var_names VARIABLES)
    foreach(_var_name ${_all_var_names})
        message(STATUS "${_var_name}=${${_var_name}}")
    endforeach()
    message(STATUS "*** dump end ***\n")
endfunction(show_all_variables)

# Affichage de toutes les variables contenant la sous-chaine search
function(show_variables search)
    message(STATUS "\n** dump #${search}# cmake variables ***")
    get_cmake_property(_all_var_names VARIABLES)
    foreach(_var_name ${_all_var_names})
        string(FIND ${_var_name} "${search}" pos_found)
        if(NOT pos_found STREQUAL -1)
            message(STATUS "${_var_name}=${${_var_name}}")
        endif()
    endforeach()
    message(STATUS "** dump end ***\n")
endfunction(show_variables)

# Affichage simplifié de variable
function(status msg)
    message(STATUS "### ${msg} : ${${msg}}")
endfunction()

function(warning msg)
    message(WARNING "\n### ${msg} : ${${msg}}\n")
endfunction()

# Code de J. Galby pour récupérer des infos via des commandes python
# usage: _set_from_python(VAR "python_code")
macro(_set_from_python outvar python_code)
    if(NOT DEFINED ${outvar})
        execute_process(
                COMMAND ${Python3_EXECUTABLE} -c "${python_code}"
                OUTPUT_VARIABLE ${outvar}
                OUTPUT_STRIP_TRAILING_WHITESPACE)
        if(${outvar} STREQUAL "")
            message(FATAL_ERROR "Failed to detect ${outvar} from command: ${Python_EXECUTABLE} -c ${python_code}")
        endif()
    endif()
endmacro()
