# Obtention de Qt pour les exemples C++ / Qt de Nemesis

# Dit à cmake de générer une règle pour exécuter rcc sur les fichiers *.qrc
set(CMAKE_AUTORCC ON)
# les fichiers générés par le moc le sont dans bin. Dit à CMake de toujours inclure les en-têtes de ce dossier
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(SPACK_ROOT "$ENV{SPACK_ROOT}")
if(NOT EXISTS ${SPACK_ROOT})
    # portable cea
    set(Qt5_DIR /opt/Qt5.14.2/5.14.2/gcc_64/lib/cmake/Qt5)
endif()
find_package(Qt5 COMPONENTS Widgets REQUIRED)

set(QT_LIBRARIES Qt5::Widgets)
