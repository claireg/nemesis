//
// Created by guilbaud on 20/06/12.
//
// #ifdef __cplusplus
#include <iterator>
#include <utility>

#include "matplotlibcpp.hpp"

namespace plt = matplotlibcpp;

// API C
//
// Existe
// * (matplotlibcpp_cea) plt::detail::get_array(const unsigned long n, const Numeric* x)
// * (matplotlibcpp_cea, utilisé dans l'exm use_c_array) plt::named_plot(name, PyObject* xarray, PyObject* yarray, const string& format="")
// Besoins :
// * backend, ion, ioff, figure, show, pause, close
// * named_plot(name, x, y, format) :
// * plot(x, y, format)

namespace detail {
template <typename Numeric>
Numeric* get_array_from_vector(const std::vector<Numeric>& v) {
    return v.data();
}
template <typename Numeric>
const std::vector<Numeric>& get_vector_from_array(const Numeric* arr) {
    // std::vector<double> local_vector(arr, arr+n); // faut ajouter const unsigned long n
    return std::move(std::vector<double>(std::begin(arr), std::end(arr)));
}
} // namespace detail

void apic_backend(const char* name) { plt::backend(name); }
void apic_ion() { plt::ion(); }
void apic_ioff() { plt::ioff(); }
void apic_clf() { plt::clf(); }
long apic_figure(long number = -1) { return plt::figure(number); }
void apic_show() { plt::show(true); }
void apic_shownb() { plt::show(false); }
void apic_pause(double interval) { plt::pause(interval); }
void apic_close() { plt::close(); }

bool apic_plot(unsigned long n, double* x, double* y) {
    PyObject* xx = plt::detail::get_array(n, x);
    PyObject* yy = plt::detail::get_array(n, y);

    return plt::plot(xx, yy);
}
bool apic_plotf(unsigned long n, double* x, double* y, const char * format) {
    PyObject* xx = plt::detail::get_array(n, x);
    PyObject* yy = plt::detail::get_array(n, y);

    return plt::plot(xx, yy, format);
}

bool apic_named_plot(const char* label, unsigned long n, double* x, double* y) {
    PyObject* xx = plt::detail::get_array(n, x);
    PyObject* yy = plt::detail::get_array(n, y);

    return plt::named_plot(label, xx, yy);
}

bool apic_named_plotf(const char* label, unsigned long n, double* x, double* y, const char* format) {
    PyObject* xx = plt::detail::get_array(n, x);
    PyObject* yy = plt::detail::get_array(n, y);

    return plt::named_plot(label, xx, yy, format);
}
//#endif // __cplusplus