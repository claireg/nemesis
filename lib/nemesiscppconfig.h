// the configured options and settings for the current project.
#pragma once

#define VIRTUAL_ENV_SITE_PACKAGES "/home/guilbaudc/Devel/products/python_and_co/3.8.3/cersei383/lib/python3.8/site-packages"
#define NEMESIS_SITE_PACKAGES "/home/guilbaudc/Devel/projects/wnemesis/lib/.."

#define NEMESIS_VERSION_MAJOR 0
#define NEMESIS_VERSION_MINOR 0
#define NEMESIS_VERSION_PATCH 0
#define NEMESIS_VERSION_TWEAK 33

