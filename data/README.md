# Données brutes du projet

**ATTENTION, le fichier `carte_support_structure_compose.psy` est vérolé.**

Ce sont de données pour les tests et les exemples.
Les données sont brutes issues des outils représentatifs : Gaia, Psyché.

En général, à chaque fichier de données correspond une image png. Ces images servent à la comparaison visuel entre
le rendu matplotlib et le rendu originel.

## Gaia

Fichiers XML (données physiques, pas de mise en page).


## Psyché

Fichiers PSY (données physiques avec mise en page), et parfois TXT.
Les fichiers TXT servent pour le debug de la lecture des fichiers complexes. Ils contiennent le résultat de
l'appel de `ReadPsyData.print_result(psy_object)`.

