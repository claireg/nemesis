#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err60-cpp"
//
// Created by guilbaud on 18/03/19.
//

#ifndef PROJECT_MATPLOTLIBCPP_CEA_HPP
#define PROJECT_MATPLOTLIBCPP_CEA_HPP

#include "matplotlibcpp_opensource.hpp"

namespace matplotlibcpp {

void deprecated_warning(const std::string &func, const std::string &instead)
{
    std::cerr << "/!\\ WARNING : DEPRECATED FUNCTION : " << func << '\n';
    std::cerr << "\tPlease use this function instead : " << instead << '\n';
}

// ----- scatter : spécialisations
template <typename Numeric>
bool scatter(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::string& marker = "o",
        const std::string& color = "black", double size = 36.0)
{
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "c", PyString_FromString(color.c_str()));
    PyDict_SetItemString(kwargs, "s", PyFloat_FromDouble(size));
    PyDict_SetItemString(kwargs, "marker", PyString_FromString(marker.c_str()));

    return detail::scatter(x, y, kwargs);
}

// ---------------------------------------------
// Nemesis' Quiver
//

// ---------- 2D arguments
namespace detail {
template <typename Numeric>
inline bool nquiver(const std::vector<std::vector<Numeric>>& x, const std::vector<std::vector<Numeric>>& y,
        const std::vector<std::vector<Numeric>>& u, const std::vector<std::vector<Numeric>>& w,
        PyObject* kwargs)
{
    assert(x.size() == y.size() && x.size() == u.size() && x.size() == w.size());

    detail::_interpreter::get();

    PyObject* xarray = detail::get_2darray(x);
    PyObject* yarray = detail::get_2darray(y);
    PyObject* uarray = detail::get_2darray(u);
    PyObject* warray = detail::get_2darray(w);

    PyObject* args = PyTuple_New(4);
    PyTuple_SetItem(args, 0, xarray);
    PyTuple_SetItem(args, 1, yarray);
    PyTuple_SetItem(args, 2, uarray);
    PyTuple_SetItem(args, 3, warray);

    PyObject* res = PyObject_Call(
            detail::_interpreter::get().s_nemesis_nquiver, args, kwargs);

    Py_DECREF(kwargs);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to nquiver() with 2D arguments failed.");
    }

    Py_DECREF(res);
    return res;
}
} // namespace detail

#ifdef USE_VARIADIC_TEMPLATES_ARGS
template <typename Numeric, class... Args>
inline bool nquiver(const std::vector<std::vector<Numeric>>& x, const std::vector<std::vector<Numeric>>& y,
        const std::vector<std::vector<Numeric>>& u, const std::vector<std::vector<Numeric>>& w,
        const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    return detail::nquiver(x, y, u, w, kwargs);
}
#endif

template <typename Numeric>
inline bool nquiver(const std::vector<std::vector<Numeric>>& x, const std::vector<std::vector<Numeric>>& y,
        const std::vector<std::vector<Numeric>>& u, const std::vector<std::vector<Numeric>>& w)
{
    PyObject* kwargs =  PyDict_New();
    return detail::nquiver(x, y, u, w, kwargs);
}

// ---------- 1D arguments
namespace detail {
template <typename Numeric>
inline bool nquiver(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
        const std::vector<Numeric>& u, const std::vector<Numeric>& w,
       PyObject* kwargs)
{
    assert(x.size() == y.size() && x.size() == u.size() && x.size() == w.size());

    detail::_interpreter::get();

    PyObject* xarray = detail::get_array(x);
    PyObject* yarray = detail::get_array(y);
    PyObject* uarray = detail::get_array(u);
    PyObject* warray = detail::get_array(w);

    PyObject* args = PyTuple_New(4);
    PyTuple_SetItem(args, 0, xarray);
    PyTuple_SetItem(args, 1, yarray);
    PyTuple_SetItem(args, 2, uarray);
    PyTuple_SetItem(args, 3, warray);

    PyObject* res = PyObject_Call(
            detail::_interpreter::get().s_nemesis_nquiver, args, kwargs);

    Py_DECREF(kwargs);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to nquiver() with 1D arguments failed.");
    }

    Py_DECREF(res);
    return res;
}
} // namespace nquiver

#ifdef USE_VARIADIC_TEMPLATES_ARGS
template <typename Numeric, class... Args>
inline bool nquiver(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
        const std::vector<Numeric>& u, const std::vector<Numeric>& w,
        const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    return detail::nquiver(x, y, u, w, kwargs);
}
#endif

template <typename Numeric>
inline bool nquiver(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
        const std::vector<Numeric>& u, const std::vector<Numeric>& w)
{
    PyObject* kwargs = PyDict_New();
    return detail::nquiver(x, y, u, w, kwargs);
}

// ---------------------------------------------
// Nemesis' Streamplot
//

// ---------- 2D arguments
namespace detail {
template <typename Numeric>
inline bool nstreamplot(const std::vector<std::vector<Numeric>>& x, const std::vector<std::vector<Numeric>>& y,
        const std::vector<std::vector<Numeric>>& u, const std::vector<std::vector<Numeric>>& w,
        PyObject* kwargs)
{
    assert(x.size() == y.size() && x.size() == u.size() && x.size() == w.size());

    detail::_interpreter::get();

    PyObject* xarray = detail::get_2darray(x);
    PyObject* yarray = detail::get_2darray(y);
    PyObject* uarray = detail::get_2darray(u);
    PyObject* warray = detail::get_2darray(w);

    PyObject* args = PyTuple_New(4);
    PyTuple_SetItem(args, 0, xarray);
    PyTuple_SetItem(args, 1, yarray);
    PyTuple_SetItem(args, 2, uarray);
    PyTuple_SetItem(args, 3, warray);

    PyObject* res = PyObject_Call(
            detail::_interpreter::get().s_nemesis_nstreamplot, args, kwargs);

    Py_DECREF(kwargs);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to nstreamplot() with 2D arguments failed.");
    }

    Py_DECREF(res);
    return res;
}
} // namespace detail

#ifdef USE_VARIADIC_TEMPLATES_ARGS
template <typename Numeric, class... Args>
inline bool nstreamplot(const std::vector<std::vector<Numeric>>& x, const std::vector<std::vector<Numeric>>& y,
        const std::vector<std::vector<Numeric>>& u, const std::vector<std::vector<Numeric>>& w,
        const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    return detail::nstreamplot(x, y, u, w, kwargs);
}
#endif

template <typename Numeric>
inline bool nstreamplot(const std::vector<std::vector<Numeric>>& x, const std::vector<std::vector<Numeric>>& y,
        const std::vector<std::vector<Numeric>>& u, const std::vector<std::vector<Numeric>>& w)
{
    PyObject* kwargs = PyDict_New();
    return detail::nstreamplot(x, y, u, w, kwargs);
}

// ---------- 1D arguments
namespace detail {
template <typename Numeric>
inline bool nstreamplot(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
        const std::vector<Numeric>& u, const std::vector<Numeric>& w,
        PyObject* kwargs)
{
    assert(x.size() == y.size() && x.size() == u.size() && x.size() == w.size());

    detail::_interpreter::get();

    PyObject* xarray = detail::get_array(x);
    PyObject* yarray = detail::get_array(y);
    PyObject* uarray = detail::get_array(u);
    PyObject* warray = detail::get_array(w);

    PyObject* args = PyTuple_New(4);
    PyTuple_SetItem(args, 0, xarray);
    PyTuple_SetItem(args, 1, yarray);
    PyTuple_SetItem(args, 2, uarray);
    PyTuple_SetItem(args, 3, warray);

    PyObject* res = PyObject_Call(
            detail::_interpreter::get().s_nemesis_nstreamplot, args, kwargs);

    Py_DECREF(kwargs);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to nstreamplot() with 1D arguments failed.");
    }

    Py_DECREF(res);
    return res;
}
} // namespace detail

#ifdef USE_VARIADIC_TEMPLATES_ARGS
template <typename Numeric, class... Args>
inline bool nstreamplot(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
        const std::vector<Numeric>& u, const std::vector<Numeric>& w,
        const unsigned long nx, const unsigned long ny,
        const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    PyDict_SetItemString(kwargs, "nx", PyLong_FromLong(nx));
    PyDict_SetItemString(kwargs, "ny", PyLong_FromLong(ny));
    return detail::nstreamplot(x, y, u, w, kwargs);
}
#endif

template <typename Numeric>
inline bool nstreamplot(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
        const std::vector<Numeric>& u, const std::vector<Numeric>& w,
        const unsigned long nx, const unsigned long ny)
{
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "nx", PyLong_FromLong(nx));
    PyDict_SetItemString(kwargs, "ny", PyLong_FromLong(ny));
    return detail::nstreamplot(x, y, u, w, kwargs);
}

namespace detail {

#ifndef WITHOUT_NUMPY
template <typename Numeric>
PyObject* get_array(const unsigned long n, const Numeric* v)
{
    detail::_interpreter::get(); //interpreter needs to be initialized for the numpy commands to work
    NPY_TYPES type = select_npy_type<Numeric>::type;
    npy_intp vsize = n;
    if (type == NPY_NOTYPE) {
        std::vector<double> vd(static_cast<unsigned long>(vsize));
        std::copy(v, v + vsize, vd.begin());
        PyObject* varray = PyArray_SimpleNewFromData(1, &vsize, NPY_DOUBLE, (void*)(vd.data()));
        return varray;
    }

    PyObject* varray = PyArray_SimpleNewFromData(1, &vsize, type, (void*)(v));
    return varray;
}

#else // fallback if we don't have numpy: copy every element of the given vector

template <typename Numeric>
PyObject* get_array(const unsigned long n, const Numeric* v)
{
    detail::_interpreter::get();

    PyObject* list = PyList_New(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        PyList_SetItem(list, i, PyFloat_FromDouble(v[i]));
    }
    return list;
}

#endif // WITHOUT_NUMPY

} // namespace detail

// ----------
// NEMESIS
//
inline void nemesis_params()
{
    detail::_interpreter::get();

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_params, detail::_interpreter::get().s_python_empty_tuple);
    Py_DECREF(detail::_interpreter::get().s_python_empty_tuple);
    if (!res){
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.nemesis_params() failed.");
    }
    Py_DECREF(res);
}

inline void set_cmap(const std::string &cmap_name)
{
    detail::_interpreter::get();

    if(!cmap_name.empty()) {
        PyObject* str_cmap_name = PyString_FromString(cmap_name.c_str());
        PyObject* args = PyTuple_New(1);
        PyTuple_SetItem(args, 0, str_cmap_name);

        PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_cmap, args);
        Py_DECREF(args);

        if (!res){
            PyErr_Print();
            throw std::runtime_error("Call to PyPlotCEA.set_cmap() failed.");
        }
        Py_DECREF(res);
    }
}

inline void set_titles(const std::string &title, const std::string& sub_title, const std::string& third_title)
{
    detail::_interpreter::get();

    PyObject* str_title = PyString_FromString(title.c_str());
    PyObject* str_sub_title = PyString_FromString(sub_title.c_str());
    PyObject* str_third_title = PyString_FromString(third_title.c_str());

    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, str_title);
    PyTuple_SetItem(args, 1, str_sub_title);
    PyTuple_SetItem(args, 2, str_third_title);

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_titles, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.set_titles() failed.");
    }
    Py_DECREF(res);
}

inline void set_titles(const std::string& title, double x0, double y0, long fs0,
                       const std::string& sub_title, double x1, double y1, long fs1,
                       const std::string& third_title, double x2, double y2, long fs2)
{
    detail::_interpreter::get();

    PyObject* str_title = PyString_FromString(title.c_str());
    PyObject* str_sub_title = PyString_FromString(sub_title.c_str());
    PyObject* str_third_title = PyString_FromString(third_title.c_str());

    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, str_title);
    PyTuple_SetItem(args, 1, str_sub_title);
    PyTuple_SetItem(args, 2, str_third_title);

    PyObject* kwargs = PyDict_New();

    PyObject* title_pos = PyTuple_New(2);
    PyTuple_SetItem(title_pos, 0, PyFloat_FromDouble(x0));
    PyTuple_SetItem(title_pos, 1, PyFloat_FromDouble(y0));
    PyDict_SetItemString(kwargs, "title_bbl_pos", title_pos);
    PyDict_SetItemString(kwargs, "title_bbl_font_size", PyLong_FromLong(fs0));

    PyObject* sub_title_pos = PyTuple_New(2);
    PyTuple_SetItem(sub_title_pos, 0, PyFloat_FromDouble(x1));
    PyTuple_SetItem(sub_title_pos, 1, PyFloat_FromDouble(y1));
    PyDict_SetItemString(kwargs, "sub_title_pos", sub_title_pos);
    PyDict_SetItemString(kwargs, "sub_title_font_size", PyLong_FromLong(fs1));

    PyObject* third_title_pos = PyTuple_New(2);
    PyTuple_SetItem(third_title_pos, 0, PyFloat_FromDouble(x2));
    PyTuple_SetItem(third_title_pos, 1, PyFloat_FromDouble(y2));
    PyDict_SetItemString(kwargs, "title_mat_pos", third_title_pos);
    PyDict_SetItemString(kwargs, "title_mat_font_size", PyLong_FromLong(fs2));

    PyObject* res = PyObject_Call(detail::_interpreter::get().s_nemesis_set_titles, args, kwargs);

    Py_DECREF(args);
    Py_DECREF(kwargs);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.set_titles(...with pos and font size...) failed.");
    }
    Py_DECREF(res);
}

template <typename Numeric>
inline void set_figure_size(Numeric width, Numeric height)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(2);
    PyTuple_SetItem(args, 0, PyFloat_FromDouble(width));
    PyTuple_SetItem(args, 1, PyFloat_FromDouble(height));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_figure_size, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.figure_size() failed.");
    }
    Py_DECREF(res);
}

template <typename Numeric>
inline void set_figure_xy(Numeric x, Numeric y, Numeric width, Numeric height)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(4);
    PyTuple_SetItem(args, 0, PyFloat_FromDouble(x));
    PyTuple_SetItem(args, 1, PyFloat_FromDouble(y));
    PyTuple_SetItem(args, 2, PyFloat_FromDouble(width));
    PyTuple_SetItem(args, 3, PyFloat_FromDouble(height));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_figure_xy, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.figure_xy() failed.");
    }
    Py_DECREF(res);
}

inline void set_window_title(const std::string& title)
{
    detail::_interpreter::get();

    if(!title.empty()) {
        PyObject* str_title = PyString_FromString(title.c_str());
        PyObject* args = PyTuple_New(1);
        PyTuple_SetItem(args, 0, str_title);

        PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_window_title, args);
        Py_DECREF(args);

        if (!res){
            PyErr_Print();
            throw std::runtime_error("Call to PyPlotCEA.set_window_title() failed.");
        }
        Py_DECREF(res);
    }
}

template <typename Numeric>
inline void set_linewidth(Numeric linewidth)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, PyFloat_FromDouble(linewidth));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_linewidth, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.set_linewidth() failed.");
    }
    Py_DECREF(res);
}

inline void set_log_scale(bool xscale, bool yscale)
{
    detail::_interpreter::get();

    PyObject* py_xscale = xscale ? Py_True : Py_False;
    PyObject* py_yscale = yscale ? Py_True : Py_False;

    PyObject* args = PyTuple_New(2);
    PyTuple_SetItem(args, 0, py_xscale);
    PyTuple_SetItem(args, 1, py_yscale);

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_log_scale, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.set_log_scale() failed.");
    }
    Py_DECREF(res);
}

inline void under_axes_legend()
{
    detail::_interpreter::get();

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_under_axes_legend, detail::_interpreter::get().s_python_empty_tuple);
    Py_DECREF(detail::_interpreter::get().s_python_empty_tuple);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.under_axes_legend() failed.");
    }
    Py_DECREF(res);
}

template <typename Numeric>
inline void networks_legend(const std::vector<std::string>& labels, const std::vector<Numeric>& v_idx0)
{
    detail::_interpreter::get();

    PyObject* list_labels = PyList_New(labels.size());
    for (size_t i = 0; i < labels.size(); ++i) {
        PyList_SetItem(list_labels, i, PyString_FromString(labels.at(i).c_str()));
    }

    PyObject* list_idx0 = PyList_New(v_idx0.size());
    for (size_t i = 0; i < v_idx0.size(); ++i) {
        PyList_SetItem(list_idx0, i, PyLong_FromLong(v_idx0.at(i)));
    }

    PyObject* args = PyTuple_New(2);
    PyTuple_SetItem(args, 0, list_labels);
    PyTuple_SetItem(args, 1, list_idx0);

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_networks_legend, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.networks_legend() failed.");
    }
    Py_DECREF(res);
}

inline void highlight_curve(long num, double width)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(2);
    PyTuple_SetItem(args, 0, PyLong_FromLong(num));
    PyTuple_SetItem(args, 1, PyFloat_FromDouble(width));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_highlight_curve, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.highlight_curve() failed.");
    }
    Py_DECREF(res);
}

template <typename Numeric>
inline void set_linewidth_to_all(Numeric linewidth)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, PyFloat_FromDouble(linewidth));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_linewidth_to_all, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.set_linewidth_to_all() failed.");
    }
    Py_DECREF(res);
}

inline void hide_curve(long num)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, PyLong_FromLong(num));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_hide_curve, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.hide_curve() failed.");
    }
    Py_DECREF(res);
}

inline void show_curve(long num)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, PyLong_FromLong(num));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_show_curve, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.show_curve() failed.");
    }
    Py_DECREF(res);
}

inline void show_all_curves()
{
    detail::_interpreter::get();

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_show_all_curves, detail::_interpreter::get().s_python_empty_tuple);
    Py_DECREF(detail::_interpreter::get().s_python_empty_tuple);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.show_all_curves() failed.");
    }
    Py_DECREF(res);
}

inline void set_label(long num, const std::string& label)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(2);
    PyTuple_SetItem(args, 0, PyLong_FromLong(num));
    PyTuple_SetItem(args, 1, PyString_FromString(label.c_str()));

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_label, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.set_label() failed.");
    }
    Py_DECREF(res);
}

// ----------
// PLOT
//

bool plot(PyObject* xarray, PyObject* yarray, const std::string& s = "")
{
    // On espère que les 2 objets ont bien la même taille !
    detail::_interpreter::get();

    PyObject* pystring = PyString_FromString(s.c_str());

    PyObject* plot_args = PyTuple_New(3);
    PyTuple_SetItem(plot_args, 0, xarray);
    PyTuple_SetItem(plot_args, 1, yarray);
    PyTuple_SetItem(plot_args, 2, pystring);

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_python_function_plot, plot_args);

    Py_DECREF(plot_args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to plot(*x, *y, s) failed.");
    }

    Py_DECREF(res);
    return res;
}

bool named_plot(const std::string& name, PyObject* yarray, const std::string& format = "")
{
    detail::_interpreter::get();

    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "label", PyString_FromString(name.c_str()));

    PyObject* pystring = PyString_FromString(format.c_str());

    PyObject* plot_args = PyTuple_New(2);
    PyTuple_SetItem(plot_args, 0, yarray);
    PyTuple_SetItem(plot_args, 1, pystring);

    PyObject* res = PyObject_Call(detail::_interpreter::get().s_python_function_plot, plot_args, kwargs);

    Py_DECREF(kwargs);
    Py_DECREF(plot_args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to named_plot(name, *x, *y, format) failed.");
    }

    Py_DECREF(res);
    return res;
}

bool named_plot(const std::string& name, PyObject* xarray, PyObject* yarray, const std::string& format = "")
{
    detail::_interpreter::get();

    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "label", PyString_FromString(name.c_str()));

    PyObject* pystring = PyString_FromString(format.c_str());

    PyObject* plot_args = PyTuple_New(3);
    PyTuple_SetItem(plot_args, 0, xarray);
    PyTuple_SetItem(plot_args, 1, yarray);
    PyTuple_SetItem(plot_args, 2, pystring);

    PyObject* res = PyObject_Call(detail::_interpreter::get().s_python_function_plot, plot_args, kwargs);

    Py_DECREF(kwargs);
    Py_DECREF(plot_args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to named_plot(name, *x, *y, format) failed.");
    }

    Py_DECREF(res);
    return res;
}

// ----------
// MISC - CEA
//

// Not @TRANS repo matplotlibcpp :
inline void figure() {
    detail::_interpreter::get();

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_python_function_figure, detail::_interpreter::get().s_python_empty_tuple);
    Py_DECREF(detail::_interpreter::get().s_python_empty_tuple);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to figure() failed.");
    }

     Py_DECREF(res);
}

// Not @TRANS repo matplotlibcpp : there is now a `long figure(long number=-1)` fct incompatible with this fct
// `long figure(long number=-1)` must be remove
template <typename Identity>
inline long figure(Identity number)
{
    detail::_interpreter::get();

    // Identity est de type int ou string
    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, get_pyobject_from(number));

    PyObject* res = nullptr;
    res = PyObject_CallObject(detail::_interpreter::get().s_python_function_figure, args);

    Py_DECREF(args);

    if(!res) {
        PyErr_Print();
        throw std::runtime_error("Call to pyplot.figure(number) failed.");
    }

    PyObject* num = PyObject_GetAttrString(res, "number");
    if (!num) throw std::runtime_error("Could not get number attribute of figure object");
    long figureNumber = PyLong_AsLong(num);

    Py_DECREF(num);
    Py_DECREF(res);

    return figureNumber;
}

inline void load(const std::string& filename)
{
    detail::_interpreter::get();

    PyObject* pyfilename = PyString_FromString(filename.c_str());

    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, pyfilename);

    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_load_image, args);
    Py_DECREF(args);
    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to save() failed.");
    }

    Py_DECREF(res);
}


// ----------
// CONTOURF
//
namespace detail {
template <typename Numeric>
inline void mapping_contourf(bool log_map, const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y, \
                        const std::vector<Numeric>&z, PyObject* kwargs)
{
    assert(x.size()*y.size() == z.size() || (x.size() == y.size() && x.size() == z.size()));

    detail::_interpreter::get();

    // using numpy arrays
    PyObject* x_array = detail::get_array(x);
    PyObject* y_array = detail::get_array(y);
    PyObject* z_array = detail::get_array(z);

    // construct positional args
    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, x_array);
    PyTuple_SetItem(args, 1, y_array);
    PyTuple_SetItem(args, 2, z_array);

    if( x.size() == y.size() && x.size() == z.size()) {
        PyDict_SetItemString(kwargs, "nx", PyLong_FromLong(x.size()));
        PyDict_SetItemString(kwargs, "ny", PyLong_FromLong(y.size()));
    }
    if(!s.empty()) {
        PyDict_SetItemString(kwargs, "label", PyString_FromString(s.c_str()));
    }

    PyObject* res = nullptr;
    if( log_map ) {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_logarithmic_contourf, args, kwargs);
    }
    else {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_linear_contourf, args, kwargs);
    }
    Py_DECREF(args);
    Py_DECREF(kwargs);

    if(!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.s_nemesis_*_contourf() failed.");
    }

    Py_DECREF(res);
}
} // namespace detail

// ----- named_linear_contourf : forme générique
template <typename Numeric, class... Args>
inline void named_linear_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                  const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::mapping_contourf(false, s, x, y, z, kwargs);
}

// ----- named_linear_contourf : spécialisations
template <typename Numeric>
inline void named_linear_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                  const std::vector<Numeric>&z, long n_levels = 10, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_contourf(false, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_linear_mapping_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                         const std::vector<Numeric>&z, long n_levels = 10, const bool draw_iso=false)
{
    deprecated_warning(__PRETTY_FUNCTION__, "named_linear_contourf");
    named_linear_contourf(s, x, y, z, n_levels, draw_iso);
}

// ----- named_logarithmic_contourf : forme générique
template <typename Numeric, class... Args>
inline void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                       const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::mapping_contourf(true, s, x, y, z, kwargs);
}

// ----- named_logarithmic_contourf : spécialisations
template <typename Numeric>
inline void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                       const std::vector<Numeric>&z, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_iso_pts", PyLong_FromLong(0));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_contourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_mapping_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                              const std::vector<Numeric>&z, const bool draw_iso=false)
{
    deprecated_warning(__PRETTY_FUNCTION__, "named_logarithmic_contourf");
    named_logarithmic_contourf(s, x, y, z, draw_iso);
}

template <typename Numeric>
inline void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                       const std::vector<Numeric>&z, long n_iso_pts, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_iso_pts", PyLong_FromLong(n_iso_pts));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_contourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_mapping_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                              const std::vector<Numeric>&z, long n_iso_pts, const bool draw_iso=false)
{
    deprecated_warning(__PRETTY_FUNCTION__, "named_logarithmic_contourf");
    named_logarithmic_contourf(s, x, y, z, n_iso_pts, draw_iso);
}

template <typename Numeric>
inline void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                       const std::vector<Numeric>&z, const Numeric masked_value, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_contourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_mapping_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                              const std::vector<Numeric>&z, const Numeric masked_value, const bool draw_iso=false)
{
    deprecated_warning(__PRETTY_FUNCTION__, "named_logarithmic_contourf");
    named_logarithmic_contourf(s, x, y, z, masked_value, draw_iso);
}

template <typename Numeric>
inline void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                       const std::vector<Numeric>&z, const Numeric masked_value, long n_iso_pts,
                                       const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_iso_pts", PyLong_FromLong(n_iso_pts));
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_contourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_mapping_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                              const std::vector<Numeric>&z, const Numeric masked_value, long n_iso_pts,
                                              const bool draw_iso=false)
{
    deprecated_warning(__PRETTY_FUNCTION__, "named_logarithmic_contourf");
    named_logarithmic_contourf(s, x, y, z, masked_value, n_iso_pts, draw_iso);
}

// ----------
// CONTOUR : Isolines
//

namespace detail {
template <typename Numeric>
inline void contour(bool log_map, const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y, \
                    const std::vector<Numeric>&z, PyObject* kwargs)
{
    assert(x.size()*y.size() == z.size() || (x.size() == y.size() && x.size() == z.size()));

    detail::_interpreter::get();

    // using numpy arrays
    PyObject* x_array = detail::get_array(x);
    PyObject* y_array = detail::get_array(y);
    PyObject* z_array = detail::get_array(z);

    // construct positional args
    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, x_array);
    PyTuple_SetItem(args, 1, y_array);
    PyTuple_SetItem(args, 2, z_array);

    if( x.size() == y.size() && x.size() == z.size()) {
        PyDict_SetItemString(kwargs, "nx", PyLong_FromLong(x.size()));
        PyDict_SetItemString(kwargs, "ny", PyLong_FromLong(y.size()));
    }
    if(!s.empty()) {
        PyDict_SetItemString(kwargs, "label", PyString_FromString(s.c_str()));
    }

    PyObject* res = nullptr;
    if( log_map ) {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_logarithmic_contour, args, kwargs);
    }
    else {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_linear_contour, args, kwargs);
    }
    Py_DECREF(args);
    Py_DECREF(kwargs);

    if(!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.s_nemesis_*_contour() failed.");
    }

    Py_DECREF(res);
}
} // namespace detail

// ----- named_linear_contour : forme générique
template <typename Numeric, class... Args>
inline void named_linear_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                 const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::contour(false, s, x, y, z, kwargs);
}

// ----- named_linear_contour : spécialisations
template <typename Numeric>
inline void named_linear_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                 const std::vector<Numeric>&z, long n_levels = 10)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));

    detail::contour(false, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_linear_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                 const std::vector<Numeric>&z, const std::vector<Numeric>& levels)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "levels", detail::get_array(levels));

    detail::contour(false, s, x, y, z, kwargs);
}

// ----- named_logarithmic_contour : forme générique
template <typename Numeric, class... Args>
inline void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                      const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::contour(true, s, x, y, z, kwargs);
}

// ----- named_logarithmic_contour : spécialisations
template <typename Numeric>
inline void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                      const std::vector<Numeric>&z, const Numeric masked_value, long n_levels = 10)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));

    detail::contour(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                      const std::vector<Numeric>& z, const Numeric masked_value,
                                      const std::vector<Numeric>& levels)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "levels", detail::get_array(levels));
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));

    detail::contour(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                      const std::vector<Numeric>&z, long n_levels = 10)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));

    detail::contour(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                      const std::vector<Numeric>&z, const std::vector<Numeric> levels)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "levels", detail::get_array(levels));

    detail::contour(true, s, x, y, z, kwargs);
}

// ----------
// TRICONTOURF
//
namespace detail {
template <typename Numeric>
inline void mapping_tricontourf(bool log_map, const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y, \
                        const std::vector<Numeric>&z, PyObject* kwargs)
{
    assert(x.size() == y.size() && x.size() == z.size());

    detail::_interpreter::get();

    // using numpy arrays
    PyObject* x_array = detail::get_array(x);
    PyObject* y_array = detail::get_array(y);
    PyObject* z_array = detail::get_array(z);

    // construct positional args
    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, x_array);
    PyTuple_SetItem(args, 1, y_array);
    PyTuple_SetItem(args, 2, z_array);

    if(!s.empty()) {
        PyDict_SetItemString(kwargs, "label", PyString_FromString(s.c_str()));
    }

    PyObject* res = nullptr;
    if( log_map ) {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_logarithmic_tricontourf, args, kwargs);
    }
    else {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_linear_tricontourf, args, kwargs);
    }
    Py_DECREF(args);
    Py_DECREF(kwargs);

    if(!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.s_nemesis_*_mapping_contour() failed.");
    }

    Py_DECREF(res);
}
} // namespace detail

// ----- named_linear_tricontourf : forme générique
template <typename Numeric, class... Args>
inline void named_linear_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                     const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::mapping_tricontourf(false, s, x, y, z, kwargs);
}

// ----- named_linear_tricontourf : spécialisations
template <typename Numeric>
inline void named_linear_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                     const std::vector<Numeric>&z, long n_levels = 10, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_tricontourf(false, s, x, y, z, kwargs);
}

// ----- named_logarithmic_contourf : forme générique
template <typename Numeric, class... Args>
inline void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                          const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::mapping_tricontourf(true, s, x, y, z, kwargs);
}

// ----- named_logarithmic_tricontourf : spécialisations
template <typename Numeric>
inline void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                          const std::vector<Numeric>&z, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_iso_pts", PyLong_FromLong(0));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_tricontourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                          const std::vector<Numeric>&z, long n_iso_pts, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_iso_pts", PyLong_FromLong(n_iso_pts));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_tricontourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                          const std::vector<Numeric>&z, const Numeric masked_value, const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_tricontourf(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                          const std::vector<Numeric>&z, const Numeric masked_value, long n_iso_pts,
                                          const bool draw_iso=false)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_iso_pts", PyLong_FromLong(n_iso_pts));
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));
    PyDict_SetItemString(kwargs, "draw_iso", (draw_iso == true) ? Py_True : Py_False);

    detail::mapping_tricontourf(true, s, x, y, z, kwargs);
}

// ----------
// TRICONTOUR : Isolines
//
namespace detail {
template <typename Numeric>
inline void tricontour(bool log_map, const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y, \
                    const std::vector<Numeric>&z, PyObject* kwargs)
{
    assert(x.size() == y.size() && x.size() == z.size());

    detail::_interpreter::get();

    // using numpy arrays
    PyObject* x_array = detail::get_array(x);
    PyObject* y_array = detail::get_array(y);
    PyObject* z_array = detail::get_array(z);

    // construct positional args
    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, x_array);
    PyTuple_SetItem(args, 1, y_array);
    PyTuple_SetItem(args, 2, z_array);

    if(!s.empty()) {
        PyDict_SetItemString(kwargs, "label", PyString_FromString(s.c_str()));
    }

    PyObject* res = nullptr;
    if( log_map ) {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_logarithmic_tricontour, args, kwargs);
    }
    else {
        res = PyObject_Call(detail::_interpreter::get().s_nemesis_linear_tricontour, args, kwargs);
    }
    Py_DECREF(args);
    Py_DECREF(kwargs);

    if(!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.s_nemesis_*_tricontour() failed.");
    }

    Py_DECREF(res);
}
} // namespace detail

// ----- named_linear_tricontour : forme générique
template <typename Numeric, class... Args>
inline void named_linear_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                    const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::tricontour(false, s, x, y, z, kwargs);
}

// ----- named_linear_tricontour : spécialisations
template <typename Numeric>
inline void named_linear_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                    const std::vector<Numeric>&z, long n_levels = 10)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));

    detail::tricontour(false, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_linear_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                    const std::vector<Numeric>&z, const std::vector<Numeric>& levels)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "levels", detail::get_array(levels));

    detail::tricontour(false, s, x, y, z, kwargs);
}

// ----- named_logarithmic_tricontour : forme générique
template <typename Numeric, class... Args>
inline void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                         const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::tricontour(true, s, x, y, z, kwargs);
}

// ----- named_logarithmic_tricontour : spécialisations
template <typename Numeric>
inline void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                         const std::vector<Numeric>&z, const Numeric masked_value, long n_levels = 10)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));

    detail::tricontour(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                         const std::vector<Numeric>& z, const Numeric masked_value,
                                         const std::vector<Numeric>& levels)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "levels", detail::get_array(levels));
    PyDict_SetItemString(kwargs, "masked_value", PyFloat_FromDouble(masked_value));

    detail::tricontour(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                         const std::vector<Numeric>&z, long n_levels = 10)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "n_levels", PyLong_FromLong(n_levels));

    detail::tricontour(true, s, x, y, z, kwargs);
}

template <typename Numeric>
inline void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                         const std::vector<Numeric>&z, const std::vector<Numeric> levels)
{
    // construction keywords
    PyObject* kwargs = PyDict_New();
    PyDict_SetItemString(kwargs, "levels", detail::get_array(levels));

    detail::tricontour(true, s, x, y, z, kwargs);
}

// ----------
// GET_CONTOUR[F] …
//
template <typename Numeric>
inline std::vector<Numeric> pyobject_to_vector(PyObject* po)
{
    detail::_interpreter::get();

    // Ne traite que les tableaux 1D
    if(!PyArray_Check(po)) {
        Py_DECREF(po);
        throw std::runtime_error("pyobject_to_vector : PyObject is not an array.");
    }

    auto *np_arr = reinterpret_cast<PyArrayObject*>(po);
    if(PyArray_NDIM(np_arr) != 1) {
        Py_DECREF(po);
        throw std::runtime_error("pyobject_to_vector : PyArrayObject is not of dimension 1");
    }

    auto len = PyArray_SHAPE(np_arr)[0];
    auto * c_arr = reinterpret_cast<Numeric*>(PyArray_DATA(np_arr));

    std::vector<Numeric> arr(static_cast<unsigned long>(len));
    arr.assign(c_arr, c_arr+len);

    Py_DECREF(po);
    return arr;
}

inline std::vector<double> get_contourf_levels()
{
    PyObject* args = PyTuple_New(0);
    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_function_get_contourf_levels, args);

    Py_DECREF(args);
    if(!res) {
        throw std::runtime_error("Call to get_contourf_levels() failed.");
    }

    return pyobject_to_vector<double>(res);
}

inline std::vector<double> get_contourf_layers()
{
    PyObject* args = PyTuple_New(0);
    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_function_get_contourf_layers, args);

    Py_DECREF(args);
    if(!res) {
        throw std::runtime_error("Call to get_contourf_layers() failed.");
    }

    return pyobject_to_vector<double>(res);
}

inline std::vector<double> get_contour_levels()
{
    PyObject* args = PyTuple_New(0);
    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_function_get_contour_levels, args);

    Py_DECREF(args);
    if(!res) {
        throw std::runtime_error("Call to get_contour_levels() failed.");
    }

    return pyobject_to_vector<double>(res);
}

//inline QWidget* get_canvas()
//{
//    PyObject* args = PyTuple_New(0);
//    PyObject* res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_function_get_canvas, args);
//
//    Py_DECREF(args);
//    if(!res) {
//        throw std::runtime_error("Call to get_canvas() failed.");
//    }
//
//    Py_INCREF(res);
//    return reinterpret_cast<QWidget*>(res);
//}

// ----------
// PLOT3D[F] …
//
namespace detail {
template <typename Numeric>
inline void plot3D(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::vector<Numeric>& z,
                    const std::string& format, const std::string& zdir, PyObject* kwargs)
{
    load_3d_modules();

    assert(x.size() == y.size());
    assert(x.size() == z.size() || z.size() == 1);

    detail::_interpreter::get();

    // using numpy arrays
    PyObject* xarray = detail::get_array(x);
    PyObject* yarray = detail::get_array(y);
    PyObject* zarray = detail::get_array(z);

    PyObject* pyformat = PyString_FromString(format.c_str());

    // construct positional args
    PyObject* args = PyTuple_New(4);
    PyTuple_SetItem(args, 0, xarray);
    PyTuple_SetItem(args, 1, yarray);
    PyTuple_SetItem(args, 2, zarray);
    PyTuple_SetItem(args, 3, pyformat);

    // Build up the kw args.
    PyDict_SetItemString(kwargs, "zdir", PyString_FromString(zdir.c_str()));

    PyObject *axis = switch_axes("3d");
    Py_INCREF(axis);

    PyObject *plot3d = PyObject_GetAttrString(axis, "plot3D");
    if(!plot3d) {
        Py_DECREF(args);
        Py_DECREF(axis);
        PyErr_Print();
        throw std::runtime_error("No plot3D");
    }
    Py_INCREF(plot3d);
    // À changer par PyObject_Call quand on aura des kwargs
    PyObject* res = PyObject_Call(plot3d, args, kwargs);
    Py_DECREF(plot3d);
    Py_DECREF(args);
    Py_DECREF(kwargs);
    Py_DECREF(axis);

    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to plot3D failed.");
    }

    Py_DECREF(res);

}
} // namespace detail

// -- plot3D forme générique
template <typename Numeric, class... Args>
inline void plot3D(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::vector<Numeric>& z,
            const std::string& format, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::plot3D(x, y, z, format, "z", kwargs);
}

template <typename Numeric, class... Args>
inline void plot3D(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::vector<Numeric>& z,
            const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    auto format = std::string("");
    detail::plot3D(x, y, z, format, "z", kwargs);
}

// -- plot3D spécialisations
template <typename Numeric>
inline void plot3D(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::vector<Numeric>& z)
{
    PyObject *kwargs = PyDict_New();
     auto format = std::string("");
    detail::plot3D(x, y, z, format, "z", kwargs);
}

template <typename Numeric>
inline void plot3D(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::vector<Numeric>& z,
                    const std::string& format)
{
    PyObject *kwargs = PyDict_New();
    detail::plot3D(x, y, z, format, "z", kwargs);
}

// ----------
// PLOT_SURFACE …
//

// --- plot_surface forme générique
template <typename Numeric, class... Args>
inline void plot_surface(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                            const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::plot_surface(x, y, z, kwargs);
}

// ----------
// RAISED_CONTOURF …
//
namespace detail {
template <typename Numeric>
inline void raised_contourf(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::vector<Numeric>&z,
        PyObject* kwargs)
{
    assert(x.size()*y.size() == z.size() || (x.size() == y.size() && x.size() == z.size()));

    detail::_interpreter::get();

    // using numpy arrays
    PyObject* x_array = detail::get_array(x);
    PyObject* y_array = detail::get_array(y);
    PyObject* z_array = detail::get_array(z);

    // construct positional args
    PyObject* args = PyTuple_New(3);
    PyTuple_SetItem(args, 0, x_array);
    PyTuple_SetItem(args, 1, y_array);
    PyTuple_SetItem(args, 2, z_array);

    if( x.size() == y.size() && x.size() == z.size()) {
        PyDict_SetItemString(kwargs, "nx", PyLong_FromLong(x.size()));
        PyDict_SetItemString(kwargs, "ny", PyLong_FromLong(y.size()));
    }

    // switch_axes est fait en python
    PyObject* res = nullptr;
    res = PyObject_Call(detail::_interpreter::get().s_nemesis_raised_contourf, args, kwargs);
    Py_DECREF(args);
    Py_DECREF(kwargs);

    if(!res) {
        PyErr_Print();
        throw std::runtime_error("Call to PyPlotCEA.*_raised_contourf() failed.");
    }

    Py_DECREF(res);
}
} // namespace detail

// --- raised_contour forme générique
template <typename Numeric, class... Args>
inline void raised_contourf(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                            const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    detail::raised_contourf(x, y, z, kwargs);
}

template <typename Numeric, class... Args>
inline void named_raised_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                 const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)
{
    PyObject* kwargs = analyze_keywords(keywords);
    if(!s.empty()) {
        PyDict_SetItemString(kwargs, "label", PyString_FromString(s.c_str()));
    }
    detail::raised_contourf(x, y, z, kwargs);
}

// --- raised_contourf spécialisations
template <typename Numeric>
inline void raised_contourf(const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                            const std::vector<Numeric>&z)
{
    PyObject *kwargs = PyDict_New();
    detail::raised_contourf(x, y, z, kwargs);
}

template <typename Numeric>
inline void named_raised_contourf(const std::string& s, const std::vector<Numeric>& x, const std::vector<Numeric>& y,
                                 const std::vector<Numeric>&z)
{
    PyObject *kwargs = PyDict_New();
    if(!s.empty()) {
        PyDict_SetItemString(kwargs, "label", PyString_FromString(s.c_str()));
    }
    detail::raised_contourf(x, y, z, kwargs);
}

inline void zlabel(const std::string& str)
{
    detail::_interpreter::get();

    PyObject* pystr = PyString_FromString(str.c_str());
    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, pystr);

    PyObject* res = nullptr;
    res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_zlabel, args);
    Py_DECREF(args);

    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to zlabel() failed.");
    }

    Py_DECREF(res);
}

inline void set_alpha(const double alpha)
{
    detail::_interpreter::get();

    PyObject* args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, PyFloat_FromDouble(alpha));

    PyObject* res = nullptr;
    res = PyObject_CallObject(detail::_interpreter::get().s_nemesis_set_alpha, args);
    Py_DECREF(args);

    if (!res) {
        PyErr_Print();
        throw std::runtime_error("Call to set_alpha() failed.");
    }

    Py_DECREF(res);
}

} // end namespace matplotlibcpp
#endif //PROJECT_MATPLOTLIBCPP_CEA_HPP

#pragma clang diagnostic pop