//
// Created by guilbaud on 20/06/11.
//

#ifndef PROJECT_MATPLOTLIBCPP_CEA_API_C_HPP
#define PROJECT_MATPLOTLIBCPP_CEA_API_C_HPP

#ifndef __cplusplus
#include <stdbool.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

void apic_backend(const char* name);
void apic_ion();
void apic_ioff();
void apic_clf();
long apic_figure(long number);
void apic_show();
void apic_shownb();
void apic_pause(double interval);
void apic_close();

bool apic_plot(unsigned long n, double* x, double* y);
bool apic_plotf(unsigned long n, double* x, double* y, const char* format);
bool apic_named_plot(const char* label, unsigned long n, double* x, double* y);
bool apic_named_plotf(const char* label, unsigned long n, double* x, double* y, const char* format);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // PROJECT_MATPLOTLIBCPP_CEA_API_C_HPPt