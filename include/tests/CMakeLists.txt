# Depuis ce répertoire :
# *ATTENTION* il faut avoir activer l'environnement virtuel désiré. 
# ./configure et suivre les instructions
# cmake -S . -B /tmp --build doit aussi fonctionner

cmake_minimum_required(VERSION 3.15)

project(TestsNemesis
        DESCRIPTION "Tests de matplotlibcpp et matplotlibcpp-cea, ie nemesis-cpp"
        LANGUAGES CXX)

# pour pip
if(${USE_VIRTUEL_ENV})
    set(ENV{PATH} "$ENV{VIRTUAL_ENV}/bin:$ENV{PATH}")
endif()
# pour spack sans vue avec option spack_env de configure

# if(DEFINED ENV{SPACK_SITE_PACKAGES})
#    message(STATUS "SPACK_SITE_PACKAGES environment variable defined")
#    set(SITE_PACKAGES "$ENV{SPACK_SITE_PACKAGES}")
#    message("SPACK_SITE_PACKAGES:" $ENV{SPACK_SITE_PACKAGES})
# endif()

# -----------------------------------------------
# Pour trouver NemesisConfig.cmake, fixer soit Nemesis_DIR
#if(NOT Nemesis_DIR)
#    set(Nemesis_DIR ${PROJECT_SOURCE_DIR}/../../lib/cmake)
#endif()
set(Nemesis_DIR ${PROJECT_SOURCE_DIR}/../../lib/cmake)
message(STATUS "Nemesis_DIR = ${Nemesis_DIR}")
find_package(Nemesis REQUIRED CONFIG)

set(PROJECT_VERSION_MAJOR  ${Nemesis_VERSION_MAJOR})
set(PROJECT_VERSION_MINOR ${Nemesis_VERSION_MINOR})
set(PROJECT_VERSION_PATCH ${Nemesis_VERSION_PATCH})
set(PROJECT_VERSION_TWEAK 99)
set(PROJECT_VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}.${PROJECT_VERSION_TWEAK})

# -----------------------------------------------
# -- CMake's variables
# ATTENTION les variables déjà existante dans le cache de CMake
#   ne sont pas modifiables depuis ici mais depuis la ligne de commande.
set(CMAKE_VERBOSE_MAKEFILE ON CACHE BOOL "Verbose Makefile" FORCE)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# -----------------------------------------------
# -- Catch
set(CATCH_MAIN ON)
option(BUILD_TESTING "Build nemesis-cpp testing" ${CATCH_MAIN}) 

# Prepare "Catch" library for other executables
add_library(Catch INTERFACE)
target_include_directories(Catch INTERFACE ${PROJECT_SOURCE_DIR})

# -----------------------------------------------
# -- Le projet (i.e. les tests C++)
set(PRJ_FILES "./tu_main.cpp;./tu_matplotlibcpp.cpp;./tu_utilities.cpp")
foreach(header ${Nemesis_HEADERS})
    list(APPEND PRJ_FILES ${header})
endforeach()
foreach(header ${Nemesis_HEADERS_NO_PATH})
    list(APPEND PRJ_FILES ${header})
endforeach()
message(STATUS "PRJ_FILES = ${PRJ_FILES}")
set(TU_EXE "TU${PROJECT_NAME}")

# show_all_variables()

# -- project configuration
set(CFG_NAME "nemesiscppconfig.h")
# set(CFG_DONE "${CMAKE_BINARY_DIR}/${CFG_NAME}")
set(CFG_IN_INSTALL_DIR "${Nemesis_DIR}/../../include/${CFG_NAME}")
set(CFG_IN_BUILD_DIR "${CMAKE_BINARY_DIR}/${CFG_NAME}")
# Si le fichier existe, c'est qu'on est dans un répertoire d'installation
# Sinon c'est qu'on est lors d'un build (dans un répertoire de développement)
if(NOT EXISTS ${CFG_IN_INSTALL_DIR})
    if(NOT EXISTS "${CFG_IN_BUILD_DIR}")
        message(STATUS "/!\\ Fichier ${CFG_NAME} inexistant dans \n"
                "\t* ${CFG_IN_BUILD_DIR} \n"
                "\t* ${CFG_IN_INSTALL_DIR}\n\t-> il faut en créer un.")
        set(NEMESIS_PKG_DIR "${PROJECT_SOURCE_DIR}/../../")
        set(NEMESIS_SITE_PACKAGES "${NEMESIS_PKG_DIR}")
        # Remplacé lors de l'installation de nemesis. Inutile en dev
        set(SPACK_DEPENDENCIES_SITELIB "")
        # Utile si dans configure, utilisation de l'option --spack_env
        if(DEFINED ENV{SPACK_SITE_PACKAGES})
            set(SITE_PACKAGES "$ENV{SPACK_SITE_PACKAGES}")
        else()
            message(STATUS "Python3_SITELIB = ${Python3_SITELIB}")
            set(SITE_PACKAGES "${Python3_SITELIB}")
        endif()
        configure_file (
                "${NEMESIS_PKG_DIR}/include/${CFG_NAME}.in"
                "${CMAKE_BINARY_DIR}/${CFG_NAME}"
        )
    endif()
endif()

# -----------------------------------------------
# -- rpath ...

# On devrait tester que le compliateur n'est pas dans un path système
# plutôt que de tests qu'on est bien passer par une toolchain
if(${USE_CUSTOM_COMPILER})
    execute_process(COMMAND bash "-c" "LANG=C ${CMAKE_CXX_COMPILER} -print-search-dirs | sed -n 's/libraries: =//gp' | tr -s ':' ';'"
    OUTPUT_VARIABLE GNU_LIBRARIES_SEARCH_DIR)

    find_library(STDCPP_LIB stdc++ PATHS ${GNU_LIBRARIES_SEARCH_DIR})
    get_filename_component(_COMPILER_LIB_DIR ${STDCPP_LIB} DIRECTORY)
    set(COMPILER_RPATH_FLAGS "-Wl,-rpath,${_COMPILER_LIB_DIR}")
endif()
# -----------------------------------------------
# -- build executable
if(${BUILD_TESTING})
    add_executable(${TU_EXE} ${PRJ_FILES})
    add_test(NAME nemesiscpp_tests
            COMMAND ${TU_EXE})

    target_compile_features(${TU_EXE}
        PRIVATE
            ${Nemesis_COMPILE_FEATURES}
    )
    target_compile_options(${TU_EXE}
        PRIVATE
            ${Nemesis_COMPILE_OPTIONS}
    )
    target_compile_definitions(${TU_EXE}
        PRIVATE
            CATCH_UNIT_TESTS
            ${Nemesis_COMPILE_DEFINITIONS}
    )
    target_include_directories(${TU_EXE}
        PRIVATE
            ${Nemesis_INCLUDE_DIRS}
            ${CMAKE_BINARY_DIR}
    )
    target_link_libraries(${TU_EXE}
        PRIVATE
            ${Nemesis_LIBRARIES}
    )
    target_link_options(${TU_EXE}
        PRIVATE
            ${COMPILER_RPATH_FLAGS}
    )
endif()
