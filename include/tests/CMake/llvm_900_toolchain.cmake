# this one is important
set(CMAKE_SYSTEM_NAME Linux)

set(LLVM_VERSION_VERSION 9.0.0)
set(LLVM_ROOT /home/guilbaudc/beurk-pkg/opt/spack/linux-ubuntu16.04-skylake/gcc-8.2.0/llvm-9.0.0-rp6xusi3bgk4cghosepkpiqjmti2yuvj)

# specify the cross compiler
set(CMAKE_C_COMPILER "${LLVM_ROOT}/bin/clang")
set(CMAKE_CXX_COMPILER "${LLVM_ROOT}/bin/clang++")

set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_SKIP_RPATH FALSE)

# where is the target environment
#set(CMAKE_FIND_ROOT_PATH  /home/alex/src/ecos/install )
set(CMAKE_FIND_ROOT_PATH "${LLVM_ROOT}")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)
