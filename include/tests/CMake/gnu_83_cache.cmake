set(GNU_VERSION 8.3)

set(GNU_ROOT /usr)
message(WARNING "\n\t/!\\ GNU_ROOT = ${GNU_ROOT}")


set(CMAKE_C_COMPILER "${GNU_ROOT}/bin/gcc" CACHE INTERNAL "")
set(CMAKE_CXX_COMPILER "${GNU_ROOT}/bin/g++" CACHE INTERNAL "")
set(COMPILER_LIBRARIES "${GNU_ROOT}/lib64" CACHE INTERNAL "")

# -- get from https://gitlab.kitware.com/cmake/community/wikis/doc/cmake/RPATH-handling
# use, i.e. don't skip the full RPATH for the build tree
set(CMAKE_SKIP_BUILD_RPATH  FALSE CACHE INTERNAL "")

# when building, don't use the install RPATH already
# (but later on when installing)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE CACHE INTERNAL "")

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib;${COMPILER_LIBRARIES}" CACHE INTERNAL "")
set(CMAKE_BUILD_RPATH "${CMAKE_INSTALL_PREFIX}/lib;${COMPILER_LIBRARIES}" CACHE INTERNAL "")
# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE CACHE INTERNAL "")

# the RPATH to be used when installing, but only if it's not a system directory
list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
endif("${isSystemDir}" STREQUAL "-1")



