# this one is important
set(CMAKE_SYSTEM_NAME Linux)

set(GNU_VERSION 8.3)
set(GNU_ROOT /path/vers/gcc/${GNU_VERSION} CACHE PATH "chemin vers installation gnu" FORCE)

set(CMAKE_FIND_ROOT_PATH "${GNU_ROOT}")
# specify the cross compiler
set(CMAKE_C_COMPILER "${GNU_ROOT}/bin/gcc")
set(CMAKE_CXX_COMPILER "${GNU_ROOT}/bin/g++")

set(USE_CUSTOM_COMPILER ON CACHE BOOL "utilisation d'une toolchain" FORCE)
