# Tests de la partie C++ du projet nemesis

Pour lancer les tests :

1. Activer l'environnement python Nemesis de son choix.
    En général, il s'agit de l'environnement utilisé dans l'espace de production ou celui de son espace de développement.
    
2. Configurer le projet `tests`

        cd <workspace>/include/tests
        ./configure

3. Compiler le projet

        cd cmake-build-debug
        make

4. Lancer l'exécutable généré

        ./TUTestsNemesis

## Notes

* Tous les tests sont en `Catch`, avec les fichiers de tests en dehors des sources.
* Pour `matplotlibcpp`, on teste que l'appel des fonctions de ce *namespace* ne lève pas d'exception lors de l'exécution.
    Il faut donc faire en sorte que tout appel à l'api C de Python ou à matplotlib lève bien une exception en cas de 
    soucis.

## État courant

Il y a actuellement 185 tests (donc 2 passent en `failed as expected`) réparties en 15 cas (`TEST_CASE`).