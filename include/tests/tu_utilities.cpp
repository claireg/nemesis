///
/// tu_utilities.cpp
///
/// \brief  Tests unitaires pour le header utilities
/// \date   15/05/2018
/// \author Claire Guilbaud

#ifdef CATCH_UNIT_TESTS
#include "catch.hpp"

#include <string>
#include <vector>

#include "utilities.h"

namespace nem = nem_utils;
using Catch::Matchers::EndsWith;

TEST_CASE("split string", "[utilities][string]") {
    using namespace std::string_literals;
    std::vector<std::wstring> words;
    std::wstring line = L"a b c d"s;
    nem::split_string(words, line.c_str());

    std::wstring a = L"a"s;
    std::wstring b = L"b"s;
    std::wstring c = L"c"s;
    std::wstring d = L"d"s;
    REQUIRE(words[0] == a);
    REQUIRE(words[1] == b);
    REQUIRE(words[2] == c);
    REQUIRE(words[3] == d);
}

TEST_CASE("print vector", "[utilities][print]") {
    using namespace std::string_literals;
    // @todo: need  vector<string>, vector<int>
    std::vector<int> tab_int(4, 1);
    std::vector<std::vector<std::string>> tab_tab_string(2);

    SECTION("print simple vector") {
        REQUIRE_NOTHROW(nem::print_vector(tab_int));
    }

    SECTION("print vector of vector") {
        unsigned long intern_size = 2;
        for (int i = 0; i < tab_tab_string.size(); ++i) {
            tab_tab_string[i].resize(intern_size);
            for (int j = 0; j < static_cast<unsigned long>(intern_size); ++j) {
                tab_tab_string[i][j] = std::to_string(i * 10 + j);
            }
        }
        REQUIRE_NOTHROW(nem::print_vector_vector(tab_tab_string));
    }

    SECTION("highlight print vector") {
        auto msg = L"Tableau d'entiers"s;
        REQUIRE_NOTHROW(nem::highlight_print_vector(tab_int, msg, 40));
        REQUIRE_NOTHROW(nem::highlight_print_vector(tab_int, msg));
        REQUIRE_NOTHROW(nem::highlight_print_vector(tab_int));
    }
}

TEST_CASE("filepath exists", "[utilities][filesystem]") {
    fs::path this_file(__FILE__);

    REQUIRE_NOTHROW(nem::exists(this_file));
}

TEST_CASE("logger timer", "[utilities][timer]") {
    auto filename = "gaia_section_efficace.txt";
    REQUIRE_NOTHROW(nem::logger_timer("tu logger timer with return: 1", nem::create_reference_path)(filename));
    REQUIRE_NOTHROW(nem::logger_timer("tu logger timer with return: 2", nem::reference_dir)());
}

TEST_CASE("logger timer no return", "[!mayfail][utilities][timer]") {
//    auto filename = "gaia_section_efficace.txt";
//    REQUIRE_NOTHROW(nem::logger_timer_func("tu logger timer no return: 1", nem::create_reference_path, filename));
//    REQUIRE_NOTHROW(nem::logger_timer_func("tu logger timer no return: 2", nem::reference_dir));
    FAIL("Known failed : Jamais utilisé pour le moment — donc fonctionne peut-être pas en C++: SIGSEGV - Segmentation violation signal");
}

TEST_CASE("centered string", "[utilities][string]") {
    std::string s("aa");
    std::ostringstream expected;
    expected << " " << s << " ";
    std::ostringstream tested;
    tested << std::setw(4) << nem::centered(s) << " ";
    REQUIRE(expected.str() == tested.str());
}
#endif