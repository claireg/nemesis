# Environnements Python avec Spack

Pour refaire des environnements Python avec Spack pour `nemesis`.

## Caractéristiques des environnements

Environnement `doris` :
* matplotlib 3.1.1
* PySide2 5.11.3

Environnement `cersei` :
* matplotlib 3.2.1
* PySide2 5.13.1

## Installation avec Spack

### Recettes et tarballs

Dans un premier temps, il faut installer les tarballs et les recettes dans Spack. 
Le recette de py-nemesis est à mettre dans recettes internes, les autres sont à mettre dans les recettes externes 
(celle pour les produits externes – si vous n'avez pas cette notion, mettez-les dans les recettes internes).

### Environnement Python pour `nemesis`

Ensuite, il faut installer Python et l'environnement Python (fait avec Spack). 
J'ai tendance à organiser mes environnement python suivant la version de Python (ici une 3.7.4), et j'indique la version 
de gcc dans leurs noms. Ces environnements sont assez complets et permet de générer la doc de `nemesis` et presque 
n'importe quelle doc de package python. 

Pour installer les environnements Python, il faut utiliser les fichiers `spack*.lock` ou `spack*.yaml`.
* Un environnement Spack créé à partir d'un manifest `spack.yaml` est garanti d'avoir les mêmes spacs racine que l'environnement original,
mais peut être concrétisé différement.
* Un environnemnet Spack créé à partir d'un *lockfile* `spack.lock` est garanti d'avoir la même concrétisation des specs que l'environnement
original.

Les fichiers `spack*.lock` et `spack*.yaml` sont dans le répertoire `py_env_spack/python_x.y.z`.

    cd /chemin/repertoire/vers/spack_lock
    # TODO : changer le /path/vers/chemin
    mkdir -p /path/vers/chemin/3.7.4/doris-dev-gcc820
    spack env create --with-view /path/vers/chemin/3.7.4/doris-dev-gcc820 doris-dev-gcc820 spack.lock

Pour vérifier qu'il fonctionne bien :

    spack env activate -p doris-dev-gcc820
    ipython
    > import matplotlib as mpl
    > mpl.use('Qt5Agg')
    > import matplotlib.pyplot as plt
    > plt.plot(range(10))


Un environnement Spack se désactive via la commande `despacktivate` (un alias pour `spack env deactivate`)

### Installation de `nemesis`

Il faut modifier la recette `py-nemesis` : 
* dans la fonction `base_environnement`, variable `env_python_path` pour qu'elle pointe vers ton install d'environnement (`/path/vers/chemin/3.7.4/doris-dev-gcc820`). 

**Je n'ai pas encore trouvé comment récupérer le path de l'environnement activé.** (Ça fait partie de ma *todolist*).

Maintenant, exécuter la commande `spack install py-nemesis` (avec l'environnement activé). Ajouter le variant `+doc` pour avoir la doc.
