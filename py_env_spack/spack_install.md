# Installation de nemesis avec Spack

Dans un premier temps, il faut installer les tarballs et les recettes dans Spack. 
Le recette de py-nemesis est à mettre dans recettes internes, les autres sont à mettre dans les recettes externes 
(celle pour les produits externes – si vous n'avez pas cette notion, mettez-les dans les recettes internes).

Ensuite, il faut installer l'environnement Python (fait avec Spack). 
J'ai tendance à organiser mes environnement python suivant la version de Python (ici une 3.7.4), et j'indique la version 
de gcc dans leurs noms. Cet environnement est assez complet et permet de générer la doc de nemesis et presque 
n'importe quelle doc de package python. Au cas où ça ne fonctionnerait pas, il existe aussi un fichier `spack_light.lock` 
(dans ce cas, l'environement et le nom deviennent doris-gcc820).

    cd /chemin/repertoire/vers/spack_lock
    # TODO : changer le /path/vers/chemin
    mkdir -p /path/vers/chemin/3.7.4/doris-dev-gcc820
    spack env create --with-view /path/vers/chemin/3.7.4/doris-dev-gcc820 doris-dev-gcc820 spack.lock

Pour vérifier qu'il fonctionne bien :

    spack env activate -p doris-dev-gcc820
    ipython
    > import matplotlib as mpl
    > mpl.use('Qt5Agg')
    > import matplotlib.pyplot as plt
    > plt.plot(range(10))


Un environnement Spack se désactive via la commande `despacktivate` (un alias pour `spack env deactivate`)


Il faut modifier la recette `py-nemesis` : 
* dans la fonction `base_environnement`, variable `env_python_path` pour qu'elle pointe vers ton install d'environnement (`/path/vers/chemin/3.7.4/doris-dev-gcc820`). 

Je n'ai pas encore trouvé comment récupérer le path de l'environnement activé.


Maintenant, faire un `spack install py-nemesis` (avec l'environnement activé). Ajouter le variant `+doc` pour avoir la doc.
Les fichiers `spack*.lock` sont dans le répertoire `py_env_spack`.