# -*- coding: utf-8 -*-

import inspect


class ReferenceData(object):
    @classmethod
    def save(cls, class_, startswith_=None):
        for meth in cls.get_methods_from_class(class_, startswith_):
            meth()

    @classmethod
    def get_methods_from_class(cls, class_, startswith_=None):
        results = inspect.getmembers(class_, predicate=inspect.ismethod)
        if startswith_ is None:
            return [obj for (name, obj) in results]
        else:
            # Attention, les méthodes de classes s'appelent '_{class_.__name__}{meth_name}'
            classmethod_name = f'_{class_.__name__}{startswith_}'
            return [obj for (name, obj) in results if name.startswith(startswith_) or name.startswith(classmethod_name)]


class GaiaData(object):
    @classmethod
    def __save_curve_data(cls):
        print('__save_curve_data')

    @classmethod
    def __save_stairs_curve_data(cls):
        print('__save_stairs_curve_data')

    @classmethod
    def __save_section_efficace(cls):
        print('__save_section_efficace')

    @classmethod
    def save(cls):
        ReferenceData.save(cls, '__save')


if __name__ == '__main__':
    GaiaData.save()
