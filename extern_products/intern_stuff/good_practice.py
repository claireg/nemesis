# -*- coding: utf-8 -*-
#!/bin/env python

u"""
Bonnes pratiques pour le projet (suite lecture doc matplotlib).

Note
----

    juste pour voir comment est rendue cette section.

See also
--------
    :ref:`manual matplotlib <matplotlib:howto-faq>`

"""
import numpy as np
import matplotlib.pyplot as plt


class MyPlotter(object):
    """
    Une classe servant de définition de namespace pour plus de lisiblité dans le code.
    """
    @classmethod
    def my_curve_plotter(cls, ax, data1, data2, param_dict):
        """
        A helper function to make a graph

        Parameters
        ----------
        ax : Axes
            The axes to draw to
        data1 : np.ndarray
            The x data
        data2 : np.ndarray
            The y data
        param_dict : dict
            Dictionary of kwargs to pass to ax.plot

        Returns
        -------
        list: list of artists added
        """
        out = ax.plot(data1, data2, **param_dict)
        return out


def plot_2_curves():
    """
    Affichage d'une courbe de 2 manières différentes.
    """
    data1, data2, data3, data4 = np.random.randn(4, 100)
    fig, (ax1, ax2) = plt.subplots(1, 2)
    results = [None, None]
    results[0] = MyPlotter.my_curve_plotter(ax1, data1, data2, {'marker': 'x'})
    results[1] = MyPlotter.my_curve_plotter(ax2, data3, data4, {'marker': 'o'})
    return results


if __name__ == '__main__':
    plot_2_curves()
    plt.show()
