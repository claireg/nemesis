# -*- coding: utf-8 -*-
# #!/bin/env python

u"""
Ensemble des tutoriaux sur pyplot dans la documentation matplotlib.

matplotlib.pyplot : ensemble de commandes qui font que matplotlib agit comme MATLAB.
    * Chaque fonction de pyplot effectue des changements pour une figure (création figure, création d'une zone
        pour tracer, tracer des courbes, …).
    * Différents états sont préservés entre les appels de fonctions (figure et zone courantes pour tracer,
        axes tracés, …)
    * Notion de figure courante et d'axes courants. Toutes les commandes pour tracer s'appliquent sur les axes courants.
    * gca() retourne les axes courants, gcf() retourne la figure courante.
    * Les axes sont la boîte d'axes ainsi que la/les courbes ou autres plots à l'intérieur de la boîte.
    * pour effacer le contenu d'une figure : clf()
    * pour effacer le contenu des axes courant : cla()
    * Si on joue avec les figures :
        * la mémoire requise pour une figure n'est pas complètement released. Ça ne l'est que lorsque la figure est
        fermée via close()
        * Supprimer toutes les références à une figure, et ou utiliser le window manager pour tuer une fenêtre n'est pas
        suffisant car pyplot maintient des références internes jusqu'à ce que close() soit appelé.

Tips:
    * Si une fonction fait appel à plt.show(), un autre appel à plt.show() par la suite ne peut avoir lieu
        que si la première figure est fermée. I.e. plt.show créé une figure (une fenêtre ?)
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


def tuto_4(set_lines):
    print("##### Tuto_4")
    plt.setp(set_lines, color='r', linewidth=2.0, linestyle='-')
    # équivalent à
    # plt.setp(lines, 'color', 'r', 'linewidth', 2.0, 'linestyle', '-')
    plt.show()


def tuto_3(show=False):
    """
    Affiche 3 courbes
    * C1: x=t, y=t
    * C2: x=t, y=t*t
    * C3: x=t, y=t**3
    :return: None
    """
    print("##### Tuto_3")
    t = np.arange(0., 5., 0.3)
    # 'r--' : red dashes, 'bs': blue squares and 'g^': green triangles
    lines = plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
    if show is True:
        plt.show()
    else:
        # Affichage des propriétés graphiques des lignes
        plt.setp(lines)
        tuto_4(lines)


def tuto_2():
    plt.plot([1, 2, 3, 4], [2, 4, 8, 16])
    plt.ylabel('Two ranges')
    plt.axis([0, 6, 0, 20])
    plt.show()


def tuto_1():
    """
    Trace une droite de 4 points dans une figure et l'affiche
    :return: None
    """
    plt.plot([1, 2, 3, 4])
    plt.ylabel('some numbers')
    plt.show()


def f(t):
    return np.exp(-t) * np.cos(2*np.pi * t)


def tuto_5():
    t1 = np.arange(0., 5., 0.1)
    t2 = np.arange(0., 5., 0.02)

    # optional here. will be created by default.
    plt.figure(1)
    # optional here. will be created by default.
    # first integer: numrows, 2nd integer: numcols, 3rd integer: fignum — between 1 and numrows*numcols
    plt.subplot(211)  # identical to plt.subplot(2, 1, 1)
    plt.plot(t1, f(t1), 'bo', t2, f(t2), 'k')

    plt.subplot(212)
    plt.plot(t2, np.cos(2*np.pi*t2), 'r--')

    plt.show()
    # In non-interactive mode, display all figures and block until the figures have been closed.
    # Call plt.close() has non-effect


def tuto_6():
    plt.figure(1)                 # the first figure
    plt.subplot(211)              # the first subplot in the first figure
    plt.plot([1, 2, 3], 'bo-')
    plt.subplot(212)              # the second subplot in the first figure
    plt.plot([4, 5, 6], 'k^')
    plt.xlabel('Œil')
    plt.ylabel('BÉPO')

    plt.figure(2)                 # a second figure
    plt.plot([4, 5, 6])           # creates a subplot(111) by default
    plt.title("Second Plot.")
    plt.text(1, 5, r'$\mu=100, \ \sigma_i=15$')
    plt.yscale('log')
    plt.grid(True)

    plt.figure(3)                 # figure 1 current; subplot(212) still current
    plt.subplot(211)              # make subplot(211) in figure1 current
    plt.title('Easy as 1, 2, 3')  # subplot 211 title
    # Les coordonnées sont ceux dans la boîte (les axes) i.e. data coordinates.
    plt.annotate('local max', xy=(1, 2), xytext=(2, 2.5),
                 arrowprops=dict(facecolor='black', shrink=0.005))
    plt.yscale('symlog')


def tuto_7():
    # ax = plt.subplot2grid((2,2),(0, 0))
    # same as ax = plt.subplot(2,2,1) except that index starts from 0
    plt.subplot2grid((3, 3), (0, 0), colspan=3)
    plt.text(0.5, 0.5, r'ax1')
    plt.subplot2grid((3, 3), (1, 0), colspan=2)
    plt.text(0.5, 0.5, r'ax2')
    plt.subplot2grid((3, 3), (1, 2), rowspan=2)
    plt.text(0.5, 0.5, r'ax3')
    plt.subplot2grid((3, 3), (2, 0))
    plt.text(0.5, 0.5, r'ax4')
    plt.subplot2grid((3, 3), (2, 1))
    plt.text(0.5, 0.5, r'ax5')


def tuto_8():
    # Same as tuto_7 with GridSpec explicitly created
    gs = gridspec.GridSpec(3, 3)
    gs.update(left=0.05, right=0.95, wspace=0.2)
    plt.subplot(gs[0, :])
    plt.text(0.5, 0.5, r'ax1')
    plt.subplot(gs[1, :-1])
    plt.text(0.5, 0.5, r'ax2')
    plt.subplot(gs[1:, -1])
    plt.text(0.5, 0.5, r'ax3')
    plt.subplot(gs[-1, 0])
    plt.text(0.5, 0.5, r'ax4')
    plt.subplot(gs[-1, -2])
    plt.text(0.5, 0.5, r'ax5')


def example_plot(ax, fontsize=12):
    ax.plot([1, 2])
    ax.locator_params(nbins=3)
    ax.set_xlabel('x-label', fontsize=fontsize)
    ax.set_ylabel('y-label', fontsize=fontsize)
    ax.set_title('Title', fontsize=fontsize)


def tuto_9():
    plt.close('all')
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2)
    example_plot(ax1)
    example_plot(ax2)
    example_plot(ax3)
    example_plot(ax4)
    plt.tight_layout()


def tuto_10():
    arr = np.arange(100).reshape((10, 10))
    plt.close('all')
    plt.figure(figsize=(5, 4))
    ax = plt.subplot(111)
    im = ax.imshow(arr, interpolation='none')
    plt.colorbar(im)
    plt.tight_layout()


def tuto_11():
    """
    By default, axes facecolor is white.
    Transparency between several axes or figures could be possible (figures in the same window)
    :return:
    """
    # Same as tuto_8 with tight_layout
    gs = gridspec.GridSpec(3, 3)
    gs.update(left=0.05, right=0.95, wspace=0.2)
    ax1 = plt.subplot(gs[0, :])
    rect1 = ax1.patch
    rect1.set_facecolor('green')
    rect1.set_alpha(0.5)
    plt.text(0.5, 0.5, r'ax1')
    plt.subplot(gs[1, :-1])
    plt.text(0.5, 0.5, r'ax2')
    ax3 = plt.subplot(gs[1:, -1])
    rect3 = ax3.patch
    rect3.set_alpha(0.5)
    plt.text(0.5, 0.5, r'ax3')
    plt.subplot(gs[-1, 0])
    plt.text(0.5, 0.5, r'ax4')
    plt.subplot(gs[-1, -2])
    plt.text(0.5, 0.5, r'ax5')
    gs.tight_layout(plt.gcf())
    rect = plt.gcf().patch
    rect.set_facecolor('blue')


if __name__ == '__main__':
    tuto_7()
    plt.show()
