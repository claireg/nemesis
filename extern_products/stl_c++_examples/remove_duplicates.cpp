// Une solution
void remove_duplicates_0(vector<T>& vec) {
    std::sort(vec.begin(), vec.end());
    vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
}

// Une autre solution
void remove_duplicates_1(vector<T>& vec) {
    unordered_set<T> s(vec.begin(), vec.end());
    vec.assign(s.begin(), s.end());
}

// Encore une autre solution
void remove_duplicates_2(vector<T>& vec) {
    set<T> s;
    for(unsigned int i=0; i<vec.size(); ++i) {
        s.insert(vec[i]);
    }
    vec.assign(s.begin(), s.end());
}