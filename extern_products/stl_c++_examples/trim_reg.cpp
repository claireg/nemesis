// —— Easy removing leading, trailing and extra spaces from a std::string in one line

value = std::regex_replace(value, std::regex("^ +| +$|( ) +"), "$1");

// —— Removing only leading spaces

value.erase(value.begin(), std::find_if(value.begin(), value.end(), std::bind1st(std::not_equal_to<char>(), ' ')));

// or

value = std::regex_replace(value, std::regex("^ +"), "");

// —— Removing only trailing spaces

value.erase(std::find_if(value.rbegin(), value.rend(), std::bind1st(std::not_equal_to<char>(), ' ')).base(), value.end());

// or

value = std::regex_replace(value, std::regex(" +$"), "");

// —— Removing only extra spaces

value = regex_replace(value, std::regex(" +"), " ");
