#include <experimental/any>
#include <type_traits>
#include <typeinfo>

using check1_t = std::experimental::fundamentals_v1::any;
using check2_t = std::experimental::fundamentals_v1::bad_any_cast;

static_assert(std::is_base_of<std::bad_cast, check2_t>::value,
    "bad_any_cast must derive from bad_cast");
