#include <testsuite_hooks.h>
#include <experimental/any>

using std::experimental::any;
using std::experimental::any_cast;

bool should_throw = false;
struct Bad {
    Bad() = default;
    Bad(const Bad&)
    {
        if (should_throw)
            throw 666;
    }
};

struct Bad2 {
    Bad2() = default;
    Bad2(const Bad2&)
    {
        if (should_throw)
            throw 666;
    }
    Bad2(Bad2&&) noexcept {}
};

int del_count = 0;
struct Good {
    Good() = default;
    Good(const Good&) = default;
    Good(Good&&) = default;
    ~Good() { ++del_count; }
};

int main()
{
    any a1 = Good();
    del_count = 0;
    try {
        Bad b;
        any a2 = b;
        should_throw = true;
        a1 = a2;
    } catch (...) {
        auto x = any_cast<Good>(a1);
        VERIFY(del_count == 0);
        VERIFY(!a1.empty());
        any_cast<Good>(a1);
    }
    any a3 = Good();
    del_count = 0;
    try {
        Bad2 b;
        any a4 = b;
        should_throw = true;
        a3 = a4;
    } catch (...) {
        auto x = any_cast<Good>(a1);
        VERIFY(del_count == 0);
        VERIFY(!a1.empty());
        any_cast<Good>(a1);
    }
}
