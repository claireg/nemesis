# Projet nemesis #

Projet d'affichage de courbes, réseaux, cartes, nappes and co. avec matplotlib.

Ce projet a pour but de permettre d'utiliser matplotlib en C++, et de proposer un ensemble de fonctionnalités de
haut-niveau basées sur matplotlib (gestion du backend, légendes des réseaux, personnalisation légendes et axes, ...).

**Il faut choisir la license** :
[Choix de la licence](https://choosealicense.com)

## Minimum requis

### Python

* `python` >= 3.7
* `numpy` >= 1.14.5
* `matplotlib` >= 3.0.2
* `PyQt5` ou `PySide2` en fonction de la version de Qt dont on dispose

Si génération de la doc
* `Sphinx` >= 1.8.3
* `numpydoc` >= 0.8.0
* `Pygments` >= 2.2.0

Si génération de la documentation
* `hieroglyph`

Si tests
* `pytest` >= 3.8.2

### C++

* gcc >= 6.4

## Environnement virtuel Python

Nemesis peut utiliser les environnements virtuels. Il faut alors activer un environnement python avant de lancer un script 
utilisant `nemesis`.
En interne, il existe un environnement python installé via `Spack` (`doris`), sur le portable, il y a plusieurs possibilités :
* `themis` : un environnement virtuel python classique (basé sur `venv` de Python3).
* `doris`, `doris-dev` : environnement `Spack` (le second contient tous les packages pour générer la doc, et développer)
* `doris-and-nemo` : vue `Spack` sans shell d'activation de l'environnement.

Pour l'activer, utiliser l'un des alias défini ou :

    . <path_vers_venv_nemesis-x.y>/bin/activate

Les modules python sont compatibles python >= 3.7 (même si une version 3.6 devrait fonctionner sans soucis).

## Environnement virtuel Python et `home -t`

J'ai testé les installations des environnements virtuels dans mon `home -t`, et à plusieurs reprises des fichiers de
ces installations ont diparu, les rendant inutilisables (*e.g.* impossible de charger `matplotlib.pyplot`, impossible
de lancer ipython, …).
En attendant de comprendre pourquoi, les environnements ont été réinstallés dans mon home.

## Documentation du projet

La documentation est faite à l'aide de Sphinx.

Pour la générer

    cd docs
    make html

La documentation se trouve alors dans le répertoire `_build/html/index.html` du repo local.

### Présentations

Les présentations sont au format `reStructuredText` avec des directives `Sphinx` / `hieroglyph`.

    cd docs/presentations
    ./gen_presentations.sh

## Utilisation de PyCharm / CLion

Les commandes suivantes sont faites au démarrage de la machine, afin d'installer `PyCharm` et `CLion` en local de
la machine (*i.e.* dans `/tmp/MyProducts`)

    cd ~/Devel/Products
    jetbrains_to_tmp.sh

## Installation *classique* (hors Spack)

Si on veut vraiment se passer de `pip` :

* Pour installer un package python dans un environnement python
    
    python setup.py install --record files.txt
    
* Pour le supprimer

    xargs rm -rf < files.txt

Avec pip

    pip install . 
    pip uninstall .
    
On peut aussi faire un `.tar.gz` via

    python setup.py sdist

