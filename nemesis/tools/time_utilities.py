# -*- coding: utf-8 -*-

"""
Ensemble d'utilitaires pour la mesure de temps.

Code basé sur un post du blog saladtomatonion.com intitulé «Mesurer le temps d'exécution de code en python».

"""

import time


class Timer(object):
    """
    Mesure de temps.
    Résultat dans mon_timer.interval

    Implémente le design pattern *context manager* utilisé par le mot clé ``with``.

    Examples
    --------
    Exemple simple::

        with Timer() as timer:
            mon_code()
        print('Total in seconds : {}'.format(timer.interval)
    """
    def __init__(self):
        self.start_time = None
        self.interval = None

    def __enter__(self):
        self.start()
        # __enter__ must return an instance bound with a "as" keyword
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def start(self):
        if hasattr(self, 'interval'):
            del self.interval
        self.start_time = time.time()

    def stop(self):
        if hasattr(self, 'start_time'):
            self.interval = time.time() - self.start_time
            del self.start_time


class LoggerTimer(Timer):
    """
    Mesure de temps avec possibilité de personnaliser le message affiché, ainsi que la destination du message.
    Est utilisable comme un décorateur ou avec le mot clé ``with``.

    Examples
    --------
    Exemples simple::

        with LoggerTimer('Process data'):
            mon_code()

        import logging
        logger = logging.getLogger('loggertimer.test')
        with LoggerTimer('Process data', logger.debug):
            mon_code()

        @LoggerTimer('The function', logger.debug)
        def ma_fonction_a_mesurer(argument):
            mon_code()
    """
    @staticmethod
    def default_logger(msg):
        print(msg)

    def __init__(self, prefix_='', func_=None):
        # Use func_ fi not None else default one
        super().__init__()
        self.__func = func_ or LoggerTimer.default_logger
        # Format the prefix if not None or empty, else use empty string
        self.prefix = '{}'.format(prefix_) if prefix_ else ''

    def stop(self):
        super().stop()
        self.__func('{prefix} {interval}'.format(prefix=self.prefix, interval=self.interval))

    def __call__(self, func_):
        # use self as context manager in a decorated function
        def decorated_func(*args, **kwargs):
            with self:
                return func_(*args, **kwargs)
        return decorated_func
