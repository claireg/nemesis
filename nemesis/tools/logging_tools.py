# -*- coding: utf-8 -*-

import os
import logging
import traceback
import uuid
import inspect
import collections
import typing

import numpy as np

__pid = os.getpid()
__logger = logging.getLogger('autolog')
__ulogger = logging.getLogger('uselog')
__clogger = logging.getLogger('nemesis')
try:
    # Fonctionne car ici handlers[0] est un FileHandler
    __autologdir = os.path.dirname(__logger.handlers[0].baseFilename)
except IndexError as e:
    # Contournement au cas où le pkg python-json-logger n'existe pas, pour que nemesis fonctionne quand même
    print('Le package python-json-logger n existe pas')
    __autologdir = os.path.dirname(os.path.join('/tmp', 'log', 'nemesis', 'autolog.py'))

NeedLogInfo = collections.namedtuple('NeedLogInfo', ['autolog', 'uselog', 'sessionlog'])
__needLog = NeedLogInfo(True, True, True)


def __get_varenv(varenv):
    """
    Retourne true si la variable d'environnement varenv existe, False sinon
    :param varenv:
    :return:
    """
    try:
        r = os.environ[varenv]
    except KeyError:
        # elle n'existe pas
        return False
    else:
        return True


def get_log_varenv() -> typing.NamedTuple:
    return __needLog


def __set_log_varenv():
    """
    Positionne les variables globales concernant les logs.

    Si 'NEMESIS_NO_AUTOLOG' est positionnée, alors uselog et sessionlog désactivés
    :return:    None
    """
    sessionlog = not __get_varenv('NEMESIS_NO_SESSIONLOG')
    uselog = not __get_varenv('NEMESIS_NO_USELOG')
    autolog = not __get_varenv('NEMESIS_NO_AUTOLOG')
    if not autolog:
        uselog = False
        sessionlog = False
    else:
        # uselog et sessionlog ne peuvent être tous les 2 faux
        if not uselog and not sessionlog:
            uselog = True
            sessionlog = True
    return NeedLogInfo(autolog, uselog, sessionlog)


__needLog = __set_log_varenv()


def __uselog_array(value: np.array) -> dict:
    array_info = dict.fromkeys(['shape', 'ndim', 'size'])
    array_info['shape'] = value.shape
    array_info['ndim'] = value.ndim
    array_info['size'] = value.size
    return array_info


def __autolog_array(name, array):
    var = f'{name}_{uuid.uuid4().hex}'
    outfile = os.path.join(__autologdir, f'{var}.npy')
    np.save(outfile, array)
    __logger.info(f"{var} = np.load('{outfile}')")
    return var


def __format_value(value):
    f_value = value
    if isinstance(value, str):
        f_value = f'\'{value}\''
    return f_value


def __format_args(arguments: dict) -> str:
    result = []
    for key, value in arguments.items():
        if key in ['vargs', 'kwargs']:
            # value est un dictionnaire
            if len(value.keys()) > 0:
                for sub_key, sub_value in value.items():
                    result.append(f'{sub_key}={__format_value(sub_value)}')
        else:
            if isinstance(value, np.ndarray):
                var = __autolog_array(key, value)
                result.append(f'{key} = {var}')
            else:
                result.append(f'{key}={__format_value(value)}')
    return ', '.join(result)


def autolog(message: str):
    """Automatically log the current function details.

    Warning
    -------
    Si on décore la fonction, alors, il faudra remonter de 2 crans au lieu d'un (sinon on logge l'appel du
    décorateur ...)
    """
    # readline ne peut-être utiliser que lorsque `nemesis` est utilisé en interactif
    # Get the previous frame in the stack, otherwise it would be this function!!!
    if __needLog.autolog:
        frame = inspect.currentframe().f_back
        if frame is None:
            # Quand appelé depuis le C++, c'est le cas ...
            __clogger.warning(f'autolog : frame is None : {message}')
            __clogger.warning(f'\t{inspect.currentframe()}')
            __clogger.warning(f'\t{inspect.currentframe().f_code.co_name}')
            return
        try:
            module = inspect.getmodule(frame)
            name = []
            if module:
                name.append(module.__name__)
            if 'self' in frame.f_locals:
                name.append(frame.f_locals['self'].__class__.__name__)
            if 'cls' in frame.f_locals:
                name.append(frame.f_locals["cls"].__name__)
            codename = frame.f_code.co_name
            if codename != '<module>':  # top level usually
                name.append(codename)  # function or a method
            fullname = '.'.join(name)
            func = frame.f_code
            #  A named tuple ArgInfo(args, varargs, keywords, locals)
            # varargs : Le nom des arguments *args dans la signature de la fonction
            # keywords : Le nom des arguments **kwargs dans la signature de la fonction
            # locals : la valeur des arguments
            argvalues = inspect.getargvalues(frame)
            func_arguments = dict.fromkeys(key for key in argvalues.args if key not in ['self', 'cls'])
            # Impossible que key n'existe pas
            for key in func_arguments.keys():
                func_arguments[key] = argvalues.locals[key]
            if argvalues.varargs is not None:
                # c'est un tuple. Pour l'instant je n'en ai qu'un seul qui est vide
                # 'args': ('',)
                # @TODO à finir plutard
                varargs = argvalues.locals[argvalues.varargs]
                if len(varargs) > 0:
                    if isinstance(varargs[0], str) and varargs[0]:
                        raise RuntimeError(f'Faut faire quoi avec les varargs ? {varargs}')
                # func_arguments['vargs'][key] = argvalues.locals[key]
            if argvalues.keywords is not None:
                # On les log même si la liste est vide (pour comparer avec ou sans)
                func_arguments['kwargs'] = argvalues.locals[argvalues.keywords]

            # in python format
            if __needLog.sessionlog:
                # current_args = ', '.join([f'{key}={value}' for key, value in func_arguments.items()])
                current_args = __format_args(func_arguments)
                func_info = '' if '<module>' in func.co_name else f'{fullname}({current_args})'
                # func_info =  f'{func.co_name}({current_args})'
                msg = func_info if message == '' else f'# {message}\n{fullname}({current_args})'
                __logger.info(f'{msg}')

            # in json format
            if __needLog.uselog:
                usage_info = dict.fromkeys(['pid', 'func_name', 'func_args'])
                usage_info['pid'] = __pid
                usage_info['func_name'] = fullname
                usage_info['func_args'] = func_arguments
                for key, value in func_arguments.items():
                    if isinstance(value, np.ndarray):
                        func_arguments[key] = __uselog_array(value)
                __ulogger.info('uselog', extra=usage_info)
        finally:
            # Indispensable selon la doc python pour éviter les cycles dans les références
            del frame


def autolog_error(message) -> None:
    """
    Log d'une erreur :
    * la console affiche juste le traceback
    * les logs d'utilisation ont une info restreinte (fichier, numéro de ligne, fonction)
    * les logs de session ont la traceback, mais aussi la pile d'appels
    :param tb:
    :return:
    """
    tb = traceback.format_exc()
    __logger.info('#')
    __logger.info(f'# {message}')
    __logger.info('#')
    __logger.error('# Stack: ')
    # f est de type FrameInfo (un NamedTuple : FrameInfo(frame, filename, lineno, function, code_context, index)
    # la première FrameInfo est la ligne du for
    stack = inspect.stack()[1:]
    if stack is None:
        # Quand appelé depuis le C++, c'est le cas ...
        __clogger.warning(f'autolog_error : stack is None : {message}')
        __clogger.warning(f'\t {inspect.stack()}')
        return
    for f in stack:
        code = None
        if f.code_context is not None:
            code = ''.join([f'#     {e.strip()}' for e in f.code_context])
        msg_2 = f'#   File "{f.filename}", line {f.lineno}'
        __logger.error(msg_2)
        if code is not None:
            __logger.error(code)
    __logger.error('#')
    last = stack[0]
    before_last = stack[-1]
    info = {'last.filename': last.filename, 'last.lineno': last.lineno, 'last.function': last.function,
            'blast.filename': before_last.filename, 'blast.lineno': before_last.lineno,
            'blast.function': before_last.function}
    __ulogger.error('autolog_error', extra=info)
    msg_4 = '\n'.join([f'# {line}' for line in tb.split('\n')])
    __logger.error(msg_4)
    __clogger.error(message)
    __clogger.error(tb)
