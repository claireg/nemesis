# -*- coding: utf-8 -*-

"""
Divers d'utilitaires.

"""
from typing import Dict, Set, Any


def compare_dict(d1: Dict[str, Any], d2: Dict[str, Any]) -> (Set[str], Set[str], Set[str]):
    """
    Comparaison de deux dictionnaires.

    Affiche les clés qui sont uniquement dans le premier, plus les clés uniquement dans le second,
    et pour les clés communes, affiche les différences entre les valeurs.

    Parameters
    ----------
    d1  : Dict[str, Any]
        Premier dictionnaire
    d2  : Dict[str, Any]
        Deuxième dictionnaire

    Returns
    -------
    (Set[str], Set[str], Set[str])
        * clés uniquement dans le premier dictionnaire
        * clés uniquement dans le second dictionnaire
        * clés communes aux deux dictionnaires
    """
    keys_1 = d1.keys()
    keys_2 = d2.keys()
    common_keys = keys_1 & keys_2
    only_d1 = [key for key in keys_1 if key not in keys_2]
    only_d2 = [key for key in keys_2 if key not in keys_1]
    str_only_d1 = ', '.join(only_d1)
    str_only_d2 = ', '.join(only_d2)
    print(f'* Only in 1 : {str_only_d1}')
    print(f'* Only in 2 : {str_only_d2}')
    print('* Common : ')
    for k in common_keys:
        v1, v2 = d1[k], d2[k]
        if v1 == v2:
            v = '–'
        else:
            v = f'{v1} vs {v2}'
        print(f'\t* {k} : \t{v}')
    return set(only_d1), set(only_d2), common_keys
