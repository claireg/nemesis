# -*- coding: utf-8 -*-

"""
Ensemble d'outils système pour le projet.

Notes
-----
L'import de ce package doit être comme suit::

    import nemesis.tools as spock

"""

from .time_utilities import (
    Timer,
    LoggerTimer,
)

from .decorator import (
    trace,
)

from .some_utilities import (
    compare_dict,
)

from .logging_tools import (
    autolog,
    autolog_error,
    get_log_varenv,
    NeedLogInfo,
)
