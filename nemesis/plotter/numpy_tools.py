# -*- coding: utf-8 -*-

import warnings
import logging
import numpy as np
import math
import matplotlib.cbook as cbook
import matplotlib.tri as tri
import nemesis.tools as nem_tools

class NumpyHelper(object):
    """
    Fonctionnalités liées à `numpy` (manipulation de tableaux, maillages, ...)
    """
    @classmethod
    def remove_less_equal_zero(cls, levels: np.array) -> np.array:
        """
        Suppression des valeurs nulles d'un tableau.

        Parameters
        ----------
        levels : np.array (1D) [tuple, list]
            Tableau à éditer

        Returns
        -------
        np.array
            Un tableau avec les items <= zéro supprimés ou le tableau d'entrée si il n'avait pas de zéros
        """
        nz = np.count_nonzero(levels <= 0.)
        if nz != len(levels):
            if isinstance(levels, (list, tuple)):
                levels = np.array(levels)
            valid_indices = levels > 0.
            levels = levels[np.nonzero(valid_indices)]
        return levels

    @classmethod
    def remove_equal(cls, value: float, levels: np.array) -> np.array:
        """
        Suppression des valeurs égales à `value` d'un tableau

        Parameters
        ----------
        value   : float
            Valeur à supprimer
        levels     : np.array (1D) [tuple, list]
            Tableau à éditer

        Returns
        -------
        np.array
            Un tableau avec les items <= zéro supprimés ou le tableau d'entrée si il n'avait pas de zéros
        """
        nz = np.count_nonzero(levels != value)
        if nz != len(levels):
            if isinstance(levels, (list, tuple)):
                levels = np.array(levels)
            valid_indices = levels != value
            levels = levels[np.nonzero(valid_indices)]
        return levels

    @classmethod
    def mask_less_equal_value(cls, value: float, z: np.array) -> np.array:
        """
        Masquage des valeurs <= `value` dans le tableau z.

        Parameters
        ----------
        value   : float
            Valeur à supprimer
        z       : np.array
            Tableau à traiter

        Returns
        -------
        np.array
            Tableau avec les valeurs <= value masquées
        """
        z_min = float(z.min())
        if z_min <= 0.:
            z = np.ma.masked_where(z <= value, z, copy=False)
            # @FIXME à mettre en info, pas en warning
            # warnings.warn(f'Mask: Les valeurs de z <= {value} ont été masquées')
        return z

    @classmethod
    def mask_less_equal_zero(cls, z: np.array) -> np.array:
        """
        Masquage des valeurs <= 0 dans le tableau z.

        Parameters
        ----------
        z       : np.array
            Tableau à traiter

        Returns
        -------
        np.array
            Tableau avec les valeurs <= 0 masquées
        """
        return cls.mask_less_equal_value(0., z)

    @classmethod
    def reduce_decades_for_z(cls, z: np.array, n_dec: int = 9, max_dec: int = 100) -> (int, np.array):
        """
        Masquage des valeurs <= nouveau min dans le tableau z pour que les données ne s'étalent pas plus que sur n_dec
        lorsque initialement elles étaient sur max_dec.

        Nouveau_min = z.max() - n_dec decades

        Parameters
        ----------
        z       : np.array
            Tableau à traiter
        n_dec   : int
            Nombre de décades souhaitées entre min et max. Attention, si on met 10, et que nouveau_min = 1.e-10,
             matplotlib aura tendance à n'en faire que 7 (question d'arrondi)
        max_dec : int
            Nombre de décades maximales à partir duquel déclencher le masquage des données.

        Returns
        -------
        (int, np.array)
            0 : le nombre de décades entre le mie et max d'origine
            1 : le tableau avec des données masquées ou non
        """
        # On supprime les valeurs <= z_min calculé pour être sûr diminuer le nombre de décades affichées
        # z.all() is np.ma.masked : cas où toutes les valeurs ont été précédemment masquées (<= 0., masked_value = )
        z = np.ma.masked_where(z <= 0., z, copy=False)
        z_max = z.max()
        vmax = math.log(z_max) / math.log(10)
        vmin = math.log(z.min()) / math.log(10)
        # Masquage des données si les données s'étalent sur plus de 100 décades
        # On réduit les données pour qu'il n'y ait que n_dec décades entre le min et le max (9 par défaut)
        if not math.isnan(vmax) and not math.isnan(vmin):
            numdec = math.floor(vmax) - math.ceil(vmin)
            if numdec > max_dec:
                log_vmin = math.floor(math.log10(z_max)) - n_dec
                z_min = 10 ** log_vmin
                z = np.ma.masked_where(z <= z_min, z, copy=False)
                # @FIXME à mettre en info, pas en warning
                # s = '%1.6e' % z_min
                # warnings.warn(f'Réduction du nombre de décades : '
                #               f' Les valeurs de z <= {s} ont été masquées')
            return numdec, z
        else:
            return -1, z

    @classmethod
    def reduce_decades_for_triang(cls, triang: tri.Triangulation, z: np.array, n_dec: int = 9,
                                  max_dec: int = 100) -> (int, tri.Triangulation):
        """
        Masquage des valeurs <= nouveau min dans le tableau z pour que les données ne s'étalent pas plus que sur n_dec
        lorsque initialement elles étaient sur max_dec.

        Nouveau_min = z.max() - n_dec decades

        Parameters
        ----------
        triang  : tri.Triangulation
            Triangulation des données à afficher (Triangulation obtenue à partir des abscisses et des ordonnées)
        z       : np.array
            Tableau à traiter
        n_dec   : int
            Nombre de décades souhaitées entre min et max. Attention, si on met 10, et que nouveau_min = 1.e-10,
             matplotlib aura tendance à n'en faire que 7 (question d'arrondi)
        max_dec : int
            Nombre de décades maximales à partir duquel déclencher le masquage des données.

        Returns
        -------
        (int, np.array)
            0 : le nombre de décades entre le mie et max d'origine
            1 : le tableau avec des données masquées ou non
        """

        # On masque les triangles dont les valeurs <= z_min calculé pour être sûr diminuer le nombre de
        # décades affichées
        # z.all() is np.ma.masked : cas où toutes les valeurs ont été précédemment masquées (<= 0., masked_value = )
        z_max = z.max()
        vmax = math.log(z_max) / math.log(10)
        vmin = math.log(z.min()) / math.log(10)
        # Masquage des triangles pour lesquels les données s'étalent sur plus de max_dec (100 par défaut) décades
        # On réduit les «données» pour qu'il n'y ait que n_dec décades entre le min et le max (9 par défaut)
        if not math.isnan(vmax) and not math.isnan(vmin):
            numdec = math.floor(vmax) - math.ceil(vmin)
            if numdec > max_dec:
                log_vmin = math.floor(math.log10(z_max)) - n_dec
                z_min = 10 ** log_vmin
                is_bad = np.less_equal(z, 0.) | np.less_equal(z, z_min)
                triang.set_mask(np.all(np.where(is_bad[triang.triangles], True, False), axis=1))
                # @FIXME à mettre en info, pas en warning
                # s = '%1.6e' % z_min
                # warnings.warn(f'Réduction du nombre de décades : '
                #               f' Les triangles dont les valeurs de z <= {s} ont été masqués')
            return numdec, triang
        else:
            return -1, triang

    @classmethod
    def compute_new_zmin(cls, z: np.array, n_dec: int = 9, max_dec: int = 100) -> (bool, float):
        z_max = z.max()
        z_min = z.min()
        try:
            vmax = math.log(z_max) / math.log(10)
            vmin = math.log(z_min) / math.log(10)
        except ValueError as e:
            nem_tools.autolog_error(f'\tErreur calcul vmin / vmax : {e}')
            return False, z_min
        else:
            # Masquage des données si les données s'étalent sur plus de 100 décades
            # On réduit les données pour qu'il n'y ait que n_dec décades entre le min et le max (9 par défaut)
            if not math.isnan(vmax) and not math.isnan(vmin):
                numdec = math.floor(vmax) - math.ceil(vmin)
                if numdec > max_dec:
                    log_vmin = math.floor(math.log10(z_max)) - n_dec
                    return True, 10 ** log_vmin
            return False, z_min

    @classmethod
    def check_mesh(cls, x_coord: np.array, y_coord: np.array, z_coord: np.array, **kwargs) \
            -> (np.array, np.array, np.array):
        """
        Cohérence des paramètres d'un maillage orthonormé.

​       Vérification de la cohérence d'un maillage dans les *_contour*.

        Parameters
        ----------
        x_coord   : np.array
            (1D ou 2D). Valeurs des abscisses des points de la carte
        y_coord   : np.array
            (1D ou 2D). Valeurs des ordonnées des points de la carte
        z_coord   : np.array
            (1D ou 2D). Valeurs de colorations

​       Other Parameters
​       ----------------
​       nx : int
​           Nombre de points en abscisses de la grille.
​           À préciser seulement si x, y, z sont 1D et ont la même taille.
​       ny : int
​           Nombre de points en ordonnées de la grille.
​           À préciser seulement si x, y, z sont 1D et ont la même taille.
​
​       Returns
​       -------
​       x, y, z : np.array de dimension 2
​           Retourne des tableaux 2D représentant la géométrie complète de la grille structurée, ainsi
​           que les valeurs aux nœuds.
​       """
        if not (x_coord.ndim == y_coord.ndim == z_coord.ndim == 2):
            if x_coord.size == y_coord.size == z_coord.size:
                # kwargs doit contenir nx et ny
                g_kwargs = cbook.normalize_kwargs(kwargs, required=('nx', 'ny'), allowed=('nx', 'ny'))
                nx, ny = g_kwargs['nx'], g_kwargs['ny']
                x = x_coord.reshape(nx, ny)
                y = y_coord.reshape(nx, ny)
                z = z_coord.reshape(nx, ny)
            elif x_coord.size * y_coord.size == z_coord.size:
                # Ne pas inverser dans z_coord.reshape les valeurs : meshgrid inverse (voir doc numpy.meshgrid)
                x, y = np.meshgrid(x_coord, y_coord)
                z = z_coord.reshape(y_coord.size, x_coord.size)
            else:
                raise RuntimeError(f'Check_mesh : Données incohérentes. '
                                   f'z.size != y.size * x.size : no mesh can be built.')
            return x, y, z
        else:
            if x_coord.ndim == y_coord.ndim == z_coord.ndim:
                if x_coord.size == y_coord.size == z_coord.size:
                    return x_coord, y_coord, z_coord
            raise RuntimeError(f'Check_mesh : Données 2D incohérentes. '
                               f'z.size != y.size != y.size : no mesh can be built.')

    @classmethod
    def check_trimesh(cls, x_coord: np.array, y_coord: np.array, z_coord: np.array, **kwargs) \
            -> (np.array, np.array, np.array):
        """
        Cohérence des paramètres d'un maillage triangulaire.

        Vérification de la cohérence d'un maillage dans les *_tricontour*. Le maillage sera obtenu par triangulation
        de Delaunay.

        Parameters
        ----------
        x_coord   : np.array (1D)
            Valeurs des abscisses des points de la carte
        y_coord   : np.array (1D)
            Valeurs des ordonnées des points de la carte
        z_coord   : np.array (1D)
            Valeurs de colorations

        Raises
        ------
            RuntimeError si les données ne sont pas cohérentes et compatibles avec affichage d'un tricontourf.
        """
        if not ((x_coord.ndim == y_coord.ndim == z_coord.ndim == 1) and (x_coord.size == y_coord.size == z_coord.size)):
            raise RuntimeError(f'Check_trimesh : Données incohérentes. Les tableaux x_coord, y_coord et z_coord '
                               f'doivent répondre aux critères suivants\n\t'
                               f'* ils doivent être à une dimension\n\t'
                               f'* ils doivent avoir la même taille')
        return x_coord, y_coord, z_coord
