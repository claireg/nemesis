# -*- coding: utf-8 -*-

import numpy as np

import matplotlib.ticker as mticker
import matplotlib.cbook as cbook


class SameLengthMathTextSciFormatter(mticker.Formatter):
    """
    Formatter log pour que les labels aient toujours la même longueur. Attention x = 10 ** x.
    """
    def __init__(self, fmt: str = '%1.2e') -> None:
        self.fmt = fmt

    def __call__(self, x: float, pos: int = None) -> str:
        x = 10 ** x
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            # s = r'%s{\times}%s' % (significand, exponent)
            s = r'%s{\times}%s' % (significand, exponent)
        else:
            s = r'%s%s' % (significand, exponent)
        return '${}$'.format(s)


class PseudoLogMathTextSciFormatter(mticker.Formatter):
    """
    Équivalent à matplotlib.ticker.LogFormatterSciNotation mais avec x = 10 ** x.
    """
    def __init__(self, fmt: str = '%1.2e') -> None:
        self.fmt = fmt

    def __call__(self, x: float, pos: int = None) -> str:
        x = 10 ** x
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            if str(significand) == '1.00':
                s = r'%s' % exponent
            else:
                s = r'%s{\times}%s' % (significand, exponent)
        else:
            if exponent == '':
                s = '10^{0}'
            else:
                s = r'%s%s' % (significand, exponent)
        return '${}$'.format(s)

    def format_data(self, value: float) -> float:
        return cbook.strip_math(self.__call__(10 ** value))

    def format_data_short(self, value: float) -> str:
        """
        Return a short formatted string representation of a number.

        Utiliser pour afficher les coordonnées de la souris dans la boîte d'axes.
        """
        return '%-12g' % (10 ** value)


class LogLogMathTextSciFormatter(mticker.Formatter):
    """
    Équivalent à matplotlib.ticker.LogFormatterSciNotation mais avec x = log10(x).
    """
    def __init__(self, fmt: str = '%1.2e') -> None:
        self.fmt = fmt

    def __call__(self, x: float, pos: int = None) -> str:
        x = np.log10(x)
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            if str(significand) == '1.00':
                s = r'%s' % exponent
            else:
                s = r'%s{\times}%s' % (significand, exponent)
        else:
            if exponent == '':
                s = '10^{0}'
            else:
                s = r'%s%s' % (significand, exponent)
        return "${}$".format(s)

    def format_data(self, value: float) -> float:
        return cbook.strip_math(self.__call__(np.log10(value)))

    def format_data_short(self, value: float) -> str:
        """
        Return a short formatted string representation of a number.

        Utiliser pour afficher les coordonnées de la souris dans la boîte d'axes.
        """
        return '%-12g' % (np.log10(value))
