# -*- coding: utf-8 -*-

"""
These are classes to support contour plotting and labelling for nemesis.PyPlotCEA.
"""

import warnings
from typing import Union

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.cm as cm
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
import matplotlib.contour as mcontour
import matplotlib.colorbar as mcolorbar
import matplotlib.axes as maxes
import matplotlib.cbook as cbook

import nemesis.plotter.numpy_tools as nempy_tools


def isocolorbar(mappable: mcontour.QuadContourSet = None, cax: maxes.Axes = None, ax: maxes.Axes = None,
                **kwargs) -> Union[mcolorbar.Colorbar, None]:
    """
    Affiche une colorbar pour uun QuadContourSet (retour de pyplot.contour)

    Parameters
    ----------
    mappable : QuadContourSet
        l'objet à partir duquel la colorbar va être créé.
    cax     : axes.Axes
        Axes into which the colorbar will be drawn.
    ax      : axes.Axes
        Parent axes from which space for a new colorbar axes will be stolen. If a list of axes is given they will
        all be resized to make room for the colorbar axes.

    Other Parameters
    ----------------
    **kwargs : dict
        Tous les paramètres possible de :py:func:`~matplotlib.pyplot.colorbar`

    Returns
    -------
        :class:`~matplotlib.colorbar.Colorbar` instance
    """
    if mappable is None:
        mappable = plt.gci()
        if mappable is None:
            raise RuntimeError('No mappable was found to use for colorbar '
                               'creation. First define a mappable such as '
                               'an image (with imshow) or a contour set ('
                               'with contourf).')
    if ax is None:
        ax = plt.gca()

    n = len(mappable.levels)
    # Contournement «bug» (?) si aucun contour n'est trouvé par plt.contour, alors il peut y avoir quand
    #   même un niveau (et un layers) avec une valeur abérante (2e-315)
    if n == 0 or (n == 1 and mappable.levels[0] == mappable.layers[0]):
        # @FIXME à mettre en info, pas en warning
        # warnings.warn('No levels found, so no colorbar needed.')
        return None

    ret = plt.gcf().colorbar(mappable, cax=cax, ax=ax, **kwargs)
    width = 100 / n
    if width < 1:
        width = 1
    elif width > 20:
        width = 20
    # changement largeur des traits des iso
    ret.lines[0].set_linewidth(width)
    # `cbar.ax.set_frame_on(False)` fonctionnerait si ce n'était pas un rectangle qui était utilisé pour faire
    #    le contour de l'échelle proprement dit.
    # suppression des major ticks
    ret.ax.tick_params(size=0)
    # suppression des minor_ticks
    ret.minorticks_off()
    # changement largeur du rectangle qui entoure la cbar
    ret.outline.set_linewidth(0)
    return ret


class BaseContourHelper(object):
    """
    Ensemble de fonctionnalités de base pour afficher un [tri]contour[f].
    """
    def __init__(self, x: np.array, y: np.array, z: np.array, other_aliases: dict = None, **kwargs: object) -> None:
        self.common_aliases = {'n_levels': ('nlevels', 'nl'), 'levels': ('l',),
                               'masked_value': ('mv',),
                               'draw_colorbar': ('dcb', 'dc'),
                               'cmap': ('cm',), 'label': ('lb',), 'linewidths': ('lw',),
                               'cax': ('current_axes', 'ax'),
                               'colors': ('c',), }
        self.cont_kwargs = {}
        self.cs = None
        self.levels = None
        self.x = x
        self.y = y
        self.z = z
        self.triang = None

        if other_aliases is not None:
            self.common_aliases.update(**other_aliases)

        self.is_log = kwargs.pop('is_log', False)
        self.is_tri = kwargs.pop('is_tri', False)
        if not self.is_tri:
            self.common_aliases.update(nx=('n_x',), ny=('n_y',))
        if self.is_log:
            self.common_aliases.update(n_decades=('nd', 'n_dec',), max_decades=('md', 'max_dec',))
        self.norm_kwargs = cbook.normalize_kwargs(kwargs, self.common_aliases)

    def check_data(self) -> None:
        if self.is_tri:
            self._check_trimesh()
        else:
            self._check_mesh()
        if self.masked_value is not None:
            self.z = nempy_tools.NumpyHelper.mask_less_equal_value(self.masked_value, self.z)

    def log_transform_data(self) -> None:
        if not self.is_tri:
            self._update_z_for_log()
        else:
            self._update_triang_for_log()
        self._compute_levels_for_log()
        self.cont_kwargs['norm'] = mcolors.LogNorm()

    def _process_args(self) -> None:
        self.current_axes = self.norm_kwargs.pop('cax', plt.gca())
        self.n_dec = self.norm_kwargs.pop('n_decades', 9)
        self.max_dec = self.norm_kwargs.pop('max_decades', 100)
        self.label = self.norm_kwargs.pop('label', None)
        self.masked_value = self.norm_kwargs.pop('masked_value', None)
        self.draw_colorbar = self.norm_kwargs.pop('draw_colorbar', True)

        self._levels_process_args()

    def _levels_process_args(self) -> None:
        """
        Analyse des paramètres de kwargs pour les niveaux.
        """
        if (('n_levels' in self.norm_kwargs) and ('levels' in self.norm_kwargs)) or ('levels' in self.norm_kwargs):
            self.levels = self.norm_kwargs.pop('levels')
            if 'n_levels' in self.norm_kwargs:
                self.norm_kwargs.pop('n_levels')
        else:
            # En C++, le nombre de niveaux (intervals) est 10 (n+1 contour lines)
            self.levels = self.norm_kwargs.pop('n_levels', 10)

    def _check_mesh(self) -> None:
        self.x, self.y, self.z = nempy_tools.NumpyHelper.check_mesh(self.x, self.y, self.z, **self.norm_kwargs)

    def _check_trimesh(self) -> None:
        self.x, self.y, self.z = nempy_tools.NumpyHelper.check_trimesh(self.x, self.y, self.z, **self.norm_kwargs)

    def _update_z_for_log(self) -> None:
        # le masquage se fait dans reduce_decades_for_z
        # ATTENTION si l'utilisateur a ajouté le paramètre masked_value dans l'appel à *_contour*
        #   alors le tableau est déjà un tableau masqué
        # self.z = nempy_tools.NumpyHelper.mask_less_equal_zero(self.z)
        numdec, self.z = nempy_tools.NumpyHelper.reduce_decades_for_z(self.z, self.n_dec, self.max_dec)

    def _update_triang_for_log(self) -> None:
        # le masquage des triangles dont les valeurs <= 0, est fait dans la fonction
        numdec, self.triang = nempy_tools.NumpyHelper.reduce_decades_for_triang(self.triang, self.z, self.n_dec, self.max_dec)

    def _compute_levels_for_log(self) -> None:
        # Si l'utilisateur passe des valeurs erronnées, ``contour(…)`` s'en sort très bien, mais pas ``colorbar(…)``
        if isinstance(self.levels, (list, tuple, np.ndarray)) is True:
            if not isinstance(self.levels, np.ndarray):
                self.levels = np.array(self.levels)
            levels_min = float(self.levels.min())
            if levels_min <= 0.:
                warnings.warn(f'{self.__class__} : Log scale:  Les valeurs de z <= 0 ont été masquées')
                self.levels = nempy_tools.NumpyHelper.remove_less_equal_zero(self.levels)


class ContourHelper(BaseContourHelper):
    """
    Ensemble de fonctionnalités pour afficher un [tri]contour.
    """
    def __init__(self, x: np.array, y: np.array, z: np.array, **kwargs: object) -> None:
        super().__init__(x, y, z, **kwargs)
        if self.is_tri:
            self.triang = tri.Triangulation(self.x, self.y)
        self.format = None

        self._process_args()

    def check_data(self) -> None:
        super().check_data()

    def log_transform_data(self) -> None:
        super().log_transform_data()
        self.format = mticker.LogFormatterSciNotation(minor_thresholds=(np.inf, np.inf))

    def contour(self) -> mcontour.ContourSet:
        if self.is_tri:
            # self.cs = self.current_axes.tricontour(self.x, self.y, self.z, levels=self.levels, **self.cont_kwargs)
            self.cs = self.current_axes.tricontour(self.triang, self.z, levels=self.levels, **self.cont_kwargs)
        else:
            self.cs = self.current_axes.contour(self.x, self.y, self.z, levels=self.levels, **self.cont_kwargs)
        return self.cs

    def _process_args(self) -> None:
        super()._process_args()
        local_kwargs = self._ccmap_process_args()
        line_kwargs = {'linewidths': self.norm_kwargs.pop('linewidths', 0.5), 'linestyles': 'solid', }
        self.cont_kwargs.update(line_kwargs)
        self.cont_kwargs.update(local_kwargs)

    def _ccmap_process_args(self) -> dict:
        """
        Analyse des paramètres de kwargs pour la colormap ou la couleur

        Returns
        -------
        dict
            Le dictionnaire contenant colors ou cmap,
        """
        color_found = False
        local_kwargs = {}
        if 'colors' in self.norm_kwargs and 'cmap' in self.norm_kwargs:
            raise ValueError("Il faut donner un couleur (colors=) OU une carte de couleurs (cmap=) mais PAS les deux.")
        elif 'colors' in self.norm_kwargs:
            color_found = True
            local_kwargs['colors'] = self.norm_kwargs.pop('colors')
        elif 'cmap' in self.norm_kwargs:
            color_found = True
            local_kwargs['cmap'] = self.norm_kwargs.pop('cmap')
        if color_found is False:
            local_kwargs['cmap'] = mpl.cm.get_cmap()
        return local_kwargs

    def colorbar(self) -> None:
        c_shrink = self._get_shrink(len(self.cs.levels))
        if self.draw_colorbar:
            cb = isocolorbar(self.cs, ax=self.current_axes, shrink=c_shrink, drawedges=False,
                             format=self.format, **self.norm_kwargs)
            if cb is not None:
                if self.label is not None and len(self.label) != 0:
                    cb.set_label(self.label)

    @staticmethod
    def _get_shrink(n: int) -> float:
        """
        Calcul le shrink à appliquer à l'échelle de couleurs des iso-lignes.

        Parameters
        ----------
        n : int
            Nombre de niveaux (iso-lignes ou nombre de couleurs)

        Returns
        -------
        float
            Facteur à appliquer à l'échelle de couleurs.
        """
        shrink = 1.0
        if n < 10:
            shrink = 0.3
        if n < 20:
            shrink = 0.5
        elif n < 50:
            shrink = 0.75
        elif n < 75:
            shrink = 0.85
        return shrink


class ContourFilledHelper(BaseContourHelper):
    """
    Ensemble de fonctionnalités pour afficher un [tri]contourf.
    """
    def __init__(self, x: np.array, y: np.array, z: np.array, **kwargs: object) -> None:
        other_aliases = {'draw_isocolorbar': ('dicb', 'dic',),
                         'draw_iso': ('di',), 'n_iso_pts': ('nip', 'n_ip',), }
        super().__init__(x, y, z, other_aliases, **kwargs)
        if self.is_tri:
            self.triang = tri.Triangulation(self.x, self.y)
        self.iso_kwargs = {}
        self.cs_iso = None
        self.iso_levels = None

        self._process_args()

    def check_data(self) -> None:
        super().check_data()

    def log_transform_data(self) -> None:
        super().log_transform_data()
        self.iso_kwargs['norm'] = mcolors.LogNorm()

    def contourf(self) -> mcontour.ContourSet:
        if self.is_tri:
            # self.cs = self.current_axes.tricontourf(self.x, self.y, self.z, self.levels, **self.cont_kwargs)
            self.cs = self.current_axes.tricontourf(self.triang, self.z, self.levels, **self.cont_kwargs)
        else:
            self.cs = self.current_axes.contourf(self.x, self.y, self.z, self.levels, **self.cont_kwargs)
        return self.cs

    def contour(self) -> mcontour.ContourSet:
        if self.is_tri:
            # self.cs_iso = self.current_axes.tricontour(self.x, self.y, self.z, levels=self.iso_levels,
            #                                            **self.iso_kwargs)
            self.cs_iso = self.current_axes.tricontour(self.triang, self.z, levels=self.iso_levels,
                                                       **self.iso_kwargs)
        else:
            self.cs_iso = self.current_axes.contour(self.x, self.y, self.z, levels=self.iso_levels,
                                                    **self.iso_kwargs)
        return self.cs_iso

    def colorbar(self) -> None:
        cb = plt.gcf().colorbar(self.cs, ax=self.current_axes, **self.norm_kwargs)
        if cb is not None:
            if self.label is not None and len(self.label) != 0:
                cb.set_label(self.label)
            if self.cs_iso is not None and self.draw_iso_in_colorbar:
                cb.add_lines(self.cs_iso, erase=False)

    def _process_args(self) -> None:
        super()._process_args()
        self.cont_kwargs['cmap'] = self.norm_kwargs.pop('cmap', mpl.cm.get_cmap())
        self._iso_process_args()

    def _iso_process_args(self) -> None:
        width = self.norm_kwargs.pop('linewidths', 0.5)
        style = 'solid'
        color = self.norm_kwargs.pop('colors', 'black')
        self.iso_kwargs = {'linewidths': width, 'linestyles': style, 'colors': color}
        self.draw_iso = self.norm_kwargs.pop('draw_iso', False)
        self.draw_iso_in_colorbar = self.norm_kwargs.pop('draw_iso_colorbar', False)
        self.n_iso_pts = self.norm_kwargs.pop('n_iso_pts', 0)

    def prepare_iso_levels(self, cs_levels) -> None:
        if self.is_log:
            self.levels = nempy_tools.NumpyHelper.remove_equal(0., cs_levels)
            iso_exp = self._compute_iso_levels(np.log10(self.levels))
            self.iso_levels = np.power(10, iso_exp)
        else:
            self.iso_levels = self._compute_iso_levels(cs_levels)

    def _compute_iso_levels(self, standard_levels: np.array) -> np.array:
        """
        Calcul des valeurs pour les isolignes intermédiaires lors d'un mapping logarithmique valeur / couleur.

        Parameters
        ----------
        standard_levels : np.array or list
            Tableau des lignes de niveaux de la coloration

        Returns
        -------
        np.array
            Tableau des valeurs pour les iso-lignes

        Notes
        -----
        Chaque niveau fait apparaître n_iso_pts sous-niveaux
        Ce qui donne comme valeurs :
         min -> np00 | np01 | ... | np0i -> sl1 -> np10 | np11 | ... | np1i -> sl2 -> ... -> max
        """
        iso_levels = np.zeros(standard_levels.size + self.n_iso_pts * (standard_levels.size - 1))
        local_min = standard_levels[0]
        local_max = standard_levels[1]
        iso_idx = 0
        idx = 1
        while idx < standard_levels.size:
            local_levels = np.linspace(local_min, local_max, self.n_iso_pts + 1, endpoint=False)
            for e in local_levels:
                iso_levels[iso_idx] = e
                iso_idx += 1
            idx += 1
            # Pour protéger le dernier pas de la boucle
            if idx < standard_levels.size:
                local_min = standard_levels[idx - 1]
                local_max = standard_levels[idx]
        # On ajoute la dernière valeur
        iso_levels[-1] = standard_levels[-1]
        np.insert(iso_levels, -1, standard_levels[-1])
        # Si les niveaux d'origine sont croissants, alors les niveaux finaux le sont aussi
        return iso_levels
