# -*- coding: utf-8 -*-

import os
import warnings
import logging
from typing import Dict, Union, Optional, List, Iterable

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.contour as mcontour
import matplotlib.axes as maxes
import matplotlib.colorbar as mcolorbar
import matplotlib.ticker as mticker
import matplotlib.projections as mprojections
import matplotlib.collections as collections
from mpl_toolkits import mplot3d
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import nemesis.plotter.contour as nem_contour
import nemesis.plotter.misc as nem_misc
import nemesis.plotter.mplot3d_extension as nem_3d
import nemesis.tools as nem_tools

_log = logging.getLogger('nemesis')


# noinspection PyTypeChecker
class PyPlotCEA(object):
    """
    Une classe servant de définition de namespace pour plus de lisiblité dans le code.

    Définition des rcParams de matplotlib utilisé par Gaia.

    Notes
    -----
    * Liste des fonctions dont l'appel doit se faire avant tout autre appel de fonctions matplotlib / pyplot / nemesis :
        * ``matplotlib.use`` (la toute toute première)
        * ``nemesis_params``
        * ``set_cmap``
        * ``set_figure_size``
        * ``set_linewidth``
    """
    __last_filled_contourset: Union[None, mcontour.ContourSet] = None
    __last_contourset: Union[None, mcontour.ContourSet] = None
    __switch_subplotpars: Dict[str, nem_misc.NemSubplotPars] = {}

    @classmethod
    def nemesis_params(cls) -> None:
        """
        Modification des marges des axes.

        Permet de ne pas avoir les 5% autour des données. Les ticks sont sur les axes.
        Évite de faire appel aux limites sur les axes en permanence
        """
        try:
            nem_tools.autolog('')
            mpl.rc('figure', max_open_warning=50)
            mpl.rc('axes', autolimit_mode='round_numbers', xmargin=0, ymargin=0)
            mpl.rc('axes.formatter', limits=(-5, 5), use_mathtext=True)
            mpl.rc('axes', grid=True)
            mpl.rc('path', simplify=True)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_cmap(cls, cmap: str) -> None:
        """
        Modification de la palette (colormap) par défaut.

        Parameters
        ----------
        cmap    : str
            Le nom d'une palette de couleur (appelée colormap dans matplotlib)

        Notes
        -----
        Colormap reference dans la doc matplotlib.
        Liste des palettes «Séquentielle à perception uniform» :
        * viridis (défaut)
        * plasma
        * inferno
        * magma
        * cividis
        * twilight (cyclique)
        * twilight_shifted (cyclique)
        """
        try:
            nem_tools.autolog('')
            if cmap is not None:
                mpl.rc('image', cmap=cmap)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_figure_size(cls, width: int, height: int) -> None:
        """
        Modification de la taille de la fenêtre d'affichage.

        Parameters
        ----------
        width   : int
            Largeur en pixels de la fenêtre
        height  : int
            Hauteur en pixels de la fenêtre

        Notes
        -------
        * L'appel doit se faire avant le premier appel à une méthode matplotlib.
        """
        try:
            nem_tools.autolog('')
            dpi = mpl.rcParams['figure.dpi']
            mpl.rc('figure', figsize=(width / dpi, height / dpi))
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_linewidth(cls, width: float) -> None:
        """
        Modification de l'épaisseur des lignes.

        Parameters
        ----------
        width : float
            Épaisseur de la ligne

        Notes
        -------
        * L'appel doit se faire avant le premier appel à une méthode matplotlib
        """
        try:
            nem_tools.autolog('')
            mpl.rc('lines', linewidth=width)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_figure_xy(cls, x: int, y: int, width: int, height: int) -> None:
        """
        Modification de la taille de la fenêtre d'affichage.

        Parameters
        ----------
        x       : int
            Abscisses de la Figure sur le display
        y       : int
            Ordonnées de la Figure sur le display
        width   : int
            Largeur en pixels de la fenêtre
        height  : int
            Hauteur en pixels de la fenêtre

        Warnings
        --------
        * L'appel doit se faire avant le premier appel à une méthode matplotlib.
        * L'appel à plt.get_current_fig_manager().window plante après wrapping via API C (en backend TkAgg)
        """
        try:
            nem_tools.autolog('')
            # Le réglage de la position de la fenêtre est dépendant du backend utilisé (et du window manager)
            if mpl.get_backend() in ('Qt5Agg', 'Qt4Agg'):
                plt.get_current_fig_manager().window.setGeometry(x, y, width, height)
                # plt.get_current_fig_manager().window)) est de type matplotlib.backends.backend_qt5.MainWindow
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')
        # window = plt.get_current_fig_manager().window
        # if mpl.get_backend() == 'TkAgg':
        #     # wm_geometry identique à geometry ?
        #     cls.window.geometry("{w}x{h}+{x}+{y}".format(w=width, h=height, x=x, y=y))
        #     print('Appel à wm_geometry')
        # # Pour mémoire
        # elif mpl.get_backend() in ('Qt5Agg', 'Qt4Agg'):
        #     print('Backend Qt*Agg')
        #     cls.window.setGeometry(x_,y_,width_, height_)
        # elif mpl.get_backend() in ('WXAgg', 'WX'):
        #     print('Backend WX*')
        #     cls.window.SetPosition(x_, y_) 

    @classmethod
    def set_window_title(cls, title: str) -> None:
        """
        Modification du titre de la fenêtre (Qt)

        Parameters
        ----------
        title   : str
            Titre de la fenêtre

        Warnings
        --------
        * Propre au backend Qt5Agg
        """
        try:
            nem_tools.autolog('')
            if mpl.get_backend() in 'Qt5Agg':
                plt.get_current_fig_manager().window.setWindowTitle(title)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_log_scale(cls, x_log: bool, y_log: bool) -> None:
        """
        Passage en log d'un ou plusieurs axes. PERMET de customiser les axes (nb ticks en co.)

        Parameters
        ----------
        x_log    : bool
            Si True, passage en log des abscisses, sinon axe linéaire.
        y_log    : bool
            Si True, passage en log des abscisses, sinon axe linéaire.
        """
        try:
            nem_tools.autolog('')
            use_specific_subticks = ''
            if x_log is True:
                plt.xscale("log")
                use_specific_subticks += 'x'
            else:
                plt.xscale("linear")
            if y_log is True:
                plt.yscale("log")
                use_specific_subticks += 'y'
            else:
                plt.yscale("linear")
            # Pose des problèmes avec la grille (opacité, moyenne de rosseland, axe des X. Y ok)
            # Pour l'instant, on débranche
            # if len(use_specific_subticks) > 0:
            #   cls.set_subticks_log_axes(use_specific_subticks)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    # noinspection PyTypeChecker
    @classmethod
    def set_subticks_log_axes(cls, axis: str) -> None:
        """
        Personnalisation des axes (nb ticks, subticks, intervalles, ...)

        Parameters
        ----------
        axis : str
            Nom de l'axe. Possibilité : ['x', 'y']
        """
        try:
            nem_tools.autolog('')
            loc_min_subs = np.arange(2, 10) * 0.1
            loc_maj = mticker.LogLocator(base=10., subs=(1.,), numticks=20)
            loc_min = mticker.LogLocator(base=10., subs=loc_min_subs, numticks=20)
            current_axis = plt.gca()
            if 'x' in axis:
                current_axis.xaxis.set_major_locator(loc_maj)
                current_axis.xaxis.set_minor_locator(loc_min)
                current_axis.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
            if 'y' in axis:
                current_axis.yaxis.set_major_locator(loc_maj)
                current_axis.yaxis.set_minor_locator(loc_min)
                current_axis.yaxis.set_minor_formatter(mpl.ticker.NullFormatter())
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_titles(cls, title_bbl: str, sub_title: str, title_mat: str, **kwargs) -> None:
        """
        Affichage des titres comme dans Gaia.

        Parameters
        ----------
        title_bbl : str
            Titre de la bibliothèque.
        sub_title : str
            Sous-titre.
        title_mat : str
            Titre du matériau

        Other Parameters
        ----------------
        title_bbl_pos : tuple[double] (of size 2)
            Position abscisse et ordonnée de title_bbl_pos.
        title_bbl_font_size : int
            Taille de la police utilisée pour afficher title_bbl
        sub_title_pos : tuple[double] (of size 2)
            Position abscisses et ordonnées de sub_title
        sub_title_font_size : int
            Taille de la police utilisée pour afficher sub_title
        title_mat_pos : tuple[double] (of size 2)
            Position abscisses et ordonnées de title_mat.
            Si x == -1, la position est calculée suivant la longueur de title_mat :
            1. - 0.012 * len(title_mat) - 0.1
        title_mat_font_size : int
            Taille de la police utilisée pour afficher title_mat

        Notes
        -----
        * Les valeurs des positions sont comprises entre 0. et 1. (bornes incluses)
        """
        try:
            nem_tools.autolog('')
            # Transformation des options courtes en options longues
            set_titles_aliases = {'title_bbl_pos': ('tpos', 'title_pos'), 'sub_title_pos': ('stpos', 'stitle_pos'),
                                  'title_mat_pos': ('tmpos', 'tmat_pos'),
                                  'title_bbl_font_size': ('tsize', 'title_size'),
                                  'sub_title_font_size': ('stsize', 'st_size'),
                                  'title_mat_font_size': ('tmsize', 'tm_size')}
            g_kwargs = cbook.normalize_kwargs(kwargs, set_titles_aliases)
            title_bbl_pos = g_kwargs.pop('title_bbl_pos', (0.1, 0.95))
            sub_title_pos = g_kwargs.pop('sub_title_pos', (0.05, 0.9))
            title_mat_pos = g_kwargs.pop('title_mat_pos', (-1, 0.95))
            title_bbl_font_size = g_kwargs.pop('title_bbl_font_size', 14)
            sub_title_font_size = g_kwargs.pop('sub_title_font_size', 10)
            title_mat_font_size = g_kwargs.pop('title_mat_font_size', 14)

            plt.figtext(title_bbl_pos[0], title_bbl_pos[1], title_bbl, fontsize=title_bbl_font_size)
            plt.figtext(sub_title_pos[0], sub_title_pos[1], sub_title, fontsize=sub_title_font_size)
            x_title_mat = title_mat_pos[1]
            if title_mat_pos[0] == -1:
                x_title_mat = 1. - 0.012 * len(title_mat) - 0.1
            plt.figtext(x_title_mat, title_mat_pos[1], title_mat, fontsize=title_mat_font_size)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def get_n_curves(cls) -> int:
        """
        Obtention du nombre de lignes (courbes) dans la boîte d'axes courants.

        Returns
        -------
        int
            Le nombre de lignes de pyplot.gca()
        """
        nem_tools.autolog('')
        return len(plt.gca().lines)

    @classmethod
    def switch_axes(cls, mode='rectilinear') -> Union[maxes.Axes, Axes3D]:
        """
        Création (si nécessaire) d'une boîte d'axes utilisant la projection `mode`.

        Parameters
        ----------
        mode : str
            Projection souhaitée parmi "3d", "aitoff", "hammer", "lambert", "mollweide", "polar", "rectilinear".

        Returns
        -------
        Une nouvelle boîte d'axes

        Notes
        -----
        * "3d" n'est disponible que si l'instruction suivante a été effectuée :
            ``from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import``.
        * Si l'on souhaite ne pas avoir à gérer à la main les ajustements des axes, il faut explicitement
        appelé switch_axes('rectilinear') quand on repasse en 2D (plt.plot change seul la boite d'axes mais
        ne change pas les ajustements des axes en fonction du type de projection.
        """
        try:
            nem_tools.autolog('')
            cax = plt.gca()
            names = mprojections.get_projection_names()
            if mode not in names:
                # Ou une exception ??
                warnings.warn(f'Type de projection inconnue ({mode}) : possibilité {names}')
                return cax
            if cax.name != mode:
                fig = plt.gcf()
                fig.delaxes(cax)
                # Sauvegarde des anciennes valeurs
                if cax.name not in cls.__switch_subplotpars:
                    cls.__switch_subplotpars[cax.name] = nem_misc.NemSubplotPars(left=fig.subplotpars.left,
                                                                                 right=fig.subplotpars.right,
                                                                                 bottom=fig.subplotpars.bottom,
                                                                                 top=fig.subplotpars.top,
                                                                                 wspace=fig.subplotpars.wspace,
                                                                                 hspace=fig.subplotpars.hspace)
                cax = plt.gca(projection=mode)
                if mode == '3d':
                    if mode not in cls.__switch_subplotpars:
                        cls.__switch_subplotpars[mode] = nem_misc.NemSubplotPars(left=0.050, right=0.950,
                                                                                 bottom=0.050, top=0.920,
                                                                                 wspace=fig.subplotpars.wspace,
                                                                                 hspace=fig.subplotpars.hspace)
                adj = cls.__switch_subplotpars[mode]
                fig.subplots_adjust(left=adj.left, bottom=adj.bottom, right=adj.right, top=adj.top)
                # rcParams[figure.subplot.*] ne semblent jamais changer ... Pourquoi ?
            return cax
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def is_3d(cls) -> bool:
        """
        Indique si la boîte d'axes courante est 3D.

        Returns
        -------
        True si la boîte d'axes courante a pour nom '3d', False sinon.
        """
        try:
            nem_tools.autolog('')
            cax = plt.gca()
            if cax.name == '3d':
                return True
            return False
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def zlabel(cls, zlabel: str = None, fontdict: dict = None, labelpad: Optional[float] = None, **kwargs) -> str:
        """

        Returns
        -------

        """
        try:
            nem_tools.autolog('')
            if cls.is_3d():
                if zlabel is not None:
                    return plt.gca().set_zlabel(zlabel, fontdict=fontdict, labelpad=labelpad, **kwargs)
                else:
                    return plt.gca().get_zlabel()
            return ''
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def under_axes_legend(cls) -> None:
        """
        Affichage de la légende des courbes sous la boîte d'axes.
        """
        try:
            # si les labels des courbes ne sont pas trop long, ncol=5 est bien (en plus si il y en a
            # moins de 5, le nombre de colonnes est égales au nombre de courbes
            # bbx_1 = (0., 0., 1., 0.102)
            nem_tools.autolog('')
            bbx_2 = (0.002, 0.002, 0.995, 0.102)
            plt.legend(bbox_to_anchor=bbx_2, bbox_transform=plt.gcf().transFigure, ncol=2,
                       mode="expand", borderaxespad=0., loc='lower center')
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def networks_legend(cls, labels: List[str], idx0: List[int], loc: str = 'lower center') -> None:
        """
        Création d'une légende de réseaux.
        Il y a un label par réseau. Un LineCollection par réseau, représentatif des 3 premières courbes.

        Parameters
        ----------
        labels  : list[str]
            Liste des labels pour chaque réseau
        idx0    : list[int]
            Liste des indices de la première courbe de chaque réseau
        loc     : str
            Localisation de la légende (autorisée : 'upper right', 'upper left', 'upper center',
            'lower right', 'lower left', 'lower center')
        """
        try:
            nem_tools.autolog('')
            llc = nem_misc.create_legend_line_collections(idx0)
        except RuntimeError as e:
            logger = logging.getLogger('root')
            logger.warning(f'Impossible de créer la légende de réseaux : {e}')
            import traceback
            nem_tools.autolog_error('RuntimeError')
            return None
        else:
            anchor, pbcb = nem_misc.move_axes_net_legend(plt.gca(), loc=loc)
            # Si loc='best', alors cela peut aller à l'encontre de ce qu'a demandé l'utilisateur :
            #   demandée : 'lower center', positionnée avec 'best' : 'lower right'
            plt.legend(llc, labels, handler_map={type(llc[0]): nem_misc.HandlerDashedLines()}, handleheight=3,
                       bbox_transform=plt.gcf().transFigure, loc=loc,
                       bbox_to_anchor=pbcb, ncol=len(idx0))

    @classmethod
    def highlight_curve(cls, num: int, width: float = None) -> None:
        """
        Mise en valeur de la `num`-ème courbe affichée.

        Parameters
        ----------
        num     : int
            Indice de la courbe à mettre en valeur
        width   : float
            Nouvelle épaisseur de la courbe. Si None, l'épaisseur sera alors le double de l'épaisseur actuelle.
        """
        try:
            nem_tools.autolog('')
            current_line = nem_misc.check_curve(num)
            if width is None:
                current_line.set_linewidth(2 * current_line.get_linewidth())
            else:
                current_line.set_linewidth(width)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def hide_curve(cls, num: int) -> None:
        """
        Masquage de la `num`-ième courbe affichée.

        On joue sur l'alpha de la couleur pour masquer la courbe.

        Parameters
        ----------
        num : int
            Indice de la courbe à mettre en valeur

        See Also
        --------
            show_curve
        """
        try:
            nem_tools.autolog('')
            current_line = nem_misc.check_curve(num)
            current_line.set_alpha(0.)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def show_curve(cls, num: int) -> None:
        """
        La `num`-ième courbe affichée devient invisible.

        On joue sur l'alpha de la couleur pour afficher la courbe.

        Parameters
        ----------
        num : int
            Indice de la courbe à mettre en valeur

        See Also
        --------
            hide_curve
        """
        try:
            nem_tools.autolog('')
            current_line = nem_misc.check_curve(num)
            current_line.set_alpha(1.)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def show_all_curves(cls) -> None:
        """
        Toutes les courbes affichées sont visibles.

        On joue sur l'alpha de la couleur pour afficher la courbe.

        See Also
        --------
            hide_curve
        """
        try:
            nem_tools.autolog('')
            for line in plt.gca().lines:
                line.set_alpha(1.)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_linewidth_to_all(cls, width: float) -> None:
        """
        Modification de l'épaisseur de chaque courbe affichée.

        Parameters
        ----------
        width   : float
            Nouvelle épaisseur
        """
        try:
            nem_tools.autolog('')
            for line in plt.gca().lines:
                line.set_linewidth(width)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_label(cls, num: int, label: str = None) -> None:
        """
        Modification du label d'une courbe déjà affichée.

        Parameters
        ----------
        num     : int
            Indice de la courbe dont le label doit être modifiée.
        label   : str
            Nouveau label
        """
        try:
            nem_tools.autolog('')
            current_line = nem_misc.check_curve(num)
        except RuntimeError as e:
            print(f'set_label : {e}')
            import traceback
            nem_tools.autolog_error('RuntimeError')
            pass
        else:
            if label is None:
                label = str(id(current_line))
            current_line.set_label(label)

    @classmethod
    def grid(cls, cax: maxes.Axes) -> None:
        """
        Affichage de la grille pour la boîte d'axes cax.

        La grille est noir, transparente (à 0.3), et avec des lignes continues.

        Parameters
        ----------
        cax : axes.Axes
            les axes dans lesquels la grille va être affichée
        """
        try:
            # Log inutile car appelée depuis des méthodes de haut-niveau
            # nem_tools.autolog('')
            cax.grid(color='black', linestyle='-', alpha=0.3)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def __generic_contourf(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Tracé d'une carte colorée.

        Code valable pour une association valeur / couleur linéaire ou logarithmique et pour un «maillage» rectilinéaire
        ou triangulaire.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        is_log                          : bool
            True si l'association valeur / couleur est logarithmique, False sinon.
        is_tri                          : bool
            True si x, y décrit un maillage géométrique triangulaire. False sinon.
        n_levels                        : int
             Nombre de niveaux de couleurs (par défaut choisit automatiquement par matplotlib) – (n_levels intervals).
        levels [ou l]                   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value                    : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        draw_iso                        : bool
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        n_iso_pts                       : int
            Nombre d'isolignes insérées entre chaque niveau de la carte colorée. Ignoré si ``draw_iso`` est ``False``.
        draw_colorbar                   : bool
            Si ``True`` affiche la colorbar associée à la carte.
        linewidths                      : float
            Épaisseur des iso-lignes (si ``draw_iso`` est ``True``)
        cmap [ou cm]                : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        label [ou lb]                   : str
            Légende de l'échelle de couleurs des iso-lignes.
        cax [ou ax ou current_axes]     : axes.Axes
            Axes dans lequel la carte va être tracée.
        nx                              : int
            **Si is_grid == True** : Nombre de points en abscisses de la grille.
        ny                              : int
            **Si is_grid == True** : Nombre de points en ordonnées de la grille.
        n_decades                       : int
            **Si is_log == True** : Nombre de décades dans les données souhaitées
        max_decades                     : int
            **Si is_log == True** : Nombre de décades à partir duquel des données vont être masquées

        See Also
        --------
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            cobj = nem_contour.ContourFilledHelper(x, y, z, **kwargs)
            cobj.check_data()
            if cobj.is_log:
                cobj.log_transform_data()

            cls.__last_filled_contourset = cobj.contourf()

            if cobj.draw_iso:
                cobj.prepare_iso_levels(cls.__last_filled_contourset.levels)
                cls.__last_filled_contourset = cobj.contour()

            cls.grid(cax=cobj.current_axes)
            if cobj.draw_colorbar:
                cobj.colorbar()
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def logarithmic_contourf(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Affichage d'une carte avec une association valeur / couleur logarithmique.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels        : int
             Nombre de niveaux de couleurs (par défaut choisit automatiquement par matplotlib) – (n_levels intervals).
        levels [ou l]   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value    : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées).
        n_decades       : int
            Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
        max_decades     : int
            Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
        draw_iso        : bool
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        n_iso_pts       : int
            Nombre d'isolignes insérées entre chaque niveau de la carte colorée. Ignoré si ``draw_iso`` est ``False``.
        draw_colorbar   : bool
            Si ``True`` affiche la colorbar associée à la carte.
        linewidths      : float
            Épaisseur des iso-lignes (si ``draw_iso`` est ``True``)
        cmap [ou cm]    : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        cax [ou ax ou current_axes]    : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]   : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**
        nx              : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**
        ny              : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**

        Notes
        -----
        ``x``, ``y`` and ``z`` ont la même forme (1D ou 2D).
        Ce qui donne :

            * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
            * Si ils sont 1D, deux possibilités :

                * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
                * x.size * y.size = z.size : une grille sera créer pour mapper les données

        See Also
        --------
        .logarithmic_contour
        .linear_contour
        .logarithmic_tricontour
        .linear_tricontour
        .linear_contourf
        .logarithmic_tricontourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=True, is_tri=False)
            cls.__generic_contourf(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def linear_contourf(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Affichage d'une carte structurée régulière avec une association valeur / couleur linéaire.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels                        : int
             Nombre de niveaux de couleurs (par défaut choisit automatiquement par matplotlib) – (n_levels intervals).
        levels [ou l]                   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value                    : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        draw_iso                        : bool
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        n_iso_pts                       : int
            Nombre d'isolignes insérées entre chaque niveau de la carte colorée. Ignoré si ``draw_iso`` est ``False``.
        draw_colorbar                   : bool
            Si ``True`` affiche la colorbar associée à la carte.
        linewidths                      : float
            Épaisseur des iso-lignes (si ``draw_iso`` est ``True``)
        cmap [ou cm]                    : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        cax [ou ax ou current_axes]     : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]                   : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**
        nx                              : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**
        ny                              : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**

        Notes
        -----
        ``x``, ``y`` and ``z`` ont la même forme (1D ou 2D).
        Ce qui donne :

            * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
            * Si ils sont 1D, deux possibilités :

                * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
                * x.size * y.size = z.size : une grille sera créer pour mapper les données

        See Also
        --------
        .logarithmic_contour
        .linear_contour
        .logarithmic_tricontour
        .linear_tricontour
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_tricontourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=False, is_tri=False)
            cls.__generic_contourf(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def logarithmic_tricontourf(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Affichage d'une carte avec une association valeur / couleur logarithmique.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels                    : int
             Nombre de niveaux de couleurs (par défaut choisit automatiquement par matplotlib) – (n_levels intervals).
        levels [ou l]               : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value                : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées).
        n_decades                   : int
            Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
        max_decades                 : int
            Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
        draw_iso                    : bool
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        n_iso_pts                   : int
            Nombre d'isolignes insérées entre chaque niveau de la carte colorée. Ignoré si ``draw_iso`` est ``False``.
        draw_colorbar               : bool
            Si ``True`` affiche la colorbar associée à la carte.
        linewidths                  : float
            Épaisseur des iso-lignes (si ``draw_iso`` est ``True``)
        cmap [ou cm]                : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        cax [ou ax ou current_axes] : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]               : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**

        See Also
        --------
        .logarithmic_contour
        .linear_contour
        .logarithmic_tricontour
        .linear_tricontour
        .logarithmic_contourf
        .linear_contourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=True, is_tri=True)
            cls.__generic_contourf(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def linear_tricontourf(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Affichage d'une carte non-structurée avec une association valeur / couleur linéaire.

        La carte est définie par les points (décrits via x et y). Le maillage est calculé par matplotlib via une
        triangulation de Delaunay.

        Parameters
        ----------
        x   : np.array (1D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels        : int
             Nombre de niveaux de couleurs (par défaut choisit automatiquement par matplotlib) – (n_levels intervals).
        levels [ou l]   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value                    : double
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        draw_iso        : bool
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        draw_colorbar   : bool
            Si ``True`` affiche la colorbar associée à la carte.
        linewidths      : float
            Épaisseur des iso-lignes (si ``draw_iso`` est ``True``)
        cmap [ou cm]                : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        cax [ou ax ou current_axes] : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]                   : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**

        See Also
        --------
        .logarithmic_contour
        .linear_contour
        .logarithmic_tricontour
        .linear_tricontour
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=False, is_tri=True)
            cls.__generic_contourf(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def __generic_contour(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Tracé d'isolignes.

        Code valable pour une association valeur / couleur linéaire ou logarithmique et pour un «maillage» rectilinéaire
        ou triangulaire.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        is_log                          : bool
            True si l'association valeur / couleur est logarithmique, False sinon.
        is_tri                          : bool
            True si x, y décrit un maillage géométrique triangulaire. False sinon.
        n_levels [ou nlevels ou nl]     : int
            Nombre d'iso-lignes voulues (10 par défaut).
        levels [ou l]                   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
        masked_value                    : double
            Valeur de comparaison pour masquer les données.
        colors [ou c]                   : str ou matplotlib.colors
            Couleur (au sens matplotlib) des iso-lignes.
        cmap [ou cm]                    : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        linewidths [ou lw]              : float
            Épaisseur de toutes les iso-lignes.
        draw_colorbar [ou dcb ou dc]    : bool
            Si ``True`` affiche la colorbar associée à la carte.
        label [ou lb]                   : str
            Légende de l'échelle de couleurs des iso-lignes.
        cax [ou ax ou current_axes]     : axes.Axes
            Axes dans lequel la carte va être tracée.
        nx                              : int
            **Si is_grid == True** : Nombre de points en abscisses de la grille.
        ny                              : int
            **Si is_grid == True** : Nombre de points en ordonnées de la grille.
        n_decades                       : int
            **Si is_log == True** : Nombre de décades dans les données souhaitées
        max_decades                     : int
            **Si is_log == True** : Nombre de décades à partir duquel des données vont être masquées

        See Also
        --------
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            cobj = nem_contour.ContourHelper(x, y, z, **kwargs)
            cobj.check_data()
            if cobj.is_log:
                cobj.log_transform_data()

            cls.__last_contourset = cobj.contour()

            cls.grid(cax=cobj.current_axes)
            cobj.colorbar()
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def logarithmic_contour(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste des iso-valeurs.

        On prendra la liste de iso-valeurs si les 2 sont précisés, ou le nombre d'iso-lignes si rien n'est
        précisé.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels [ou nlevels ou nl]     : int
            Nombre d'iso-lignes voulues (10 par défaut).
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        levels [ou l]                   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value                    : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées).
        n_decades                       : int
            Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
        max_decades                     : int
            Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
        colors [ou c]                   : str ou matplotlib.colors
            N'importe quelle couleur matplotlib. Les iso-lignes auront toutes cette couleur.
        cmap [ou cm]                    : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        linewidths [ou lw]              : float
            Épaisseur de toutes les iso-lignes.
        draw_colorbar [ou dcb ou dc]    : bool
            Si ``True`` affiche la colorbar associée à la carte.
        cax [ou ax ou current_axes]     : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]                   : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**
        nx                              : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**
        ny                              : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**

        Notes
        -----
        ``x``, ``y`` and ``z`` ont la même forme (1D ou 2D).
        Ce qui donne :

            * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
            * Si ils sont 1D, deux possibilités :

                * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
                * x.size * y.size = z.size : une grille sera créer pour mapper les données

        See Also
        --------
        .linear_contour
        .logarithmic_tricontour
        .linear_tricontour
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_contourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=True, is_tri=False)
            cls.__generic_contour(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def linear_contour(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste des iso-valeurs.

        On prendra la liste de iso-valeurs si les 2 sont précisés, ou le nombre d'iso-lignes si rien n'est
        précisé.

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels [ou nlevels ou nl]  : int
            Nombre d'iso-lignes voulues (10 par défaut).
        levels [ou l]               : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
        masked_value                : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        colors [ou c]               : str ou matplotlib.colors
            N'importe quelle couleur matplotlib. Les iso-lignes auront toutes cette couleur.
        cmap [ou cm]                : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        linewidths [ou lw]          : float
            Épaisseur de toutes les iso-lignes.
        draw_colorbar [ou dcb ou dc]: bool
            Si ``True`` affiche la colorbar associée à la carte.
        cax [ou ax ou current_axes] : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]               : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**
        nx                          : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**
        ny                          : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**

        Notes
        -----
        ``x``, ``y`` and ``z`` ont la même forme (1D ou 2D).
        Ce qui donne :

            * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
            * Si ils sont 1D, deux possibilités :

                * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
                * x.size * y.size = z.size : une grille sera créer pour mapper les données

        See Also
        --------
        .logarithmic_contour
        .logarithmic_tricontour
        .linear_tricontour
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_contourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=False, is_tri=False)
            cls.__generic_contour(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def logarithmic_tricontour(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste des iso-valeurs.

        On prendra la liste de iso-valeurs si les 2 sont précisés, ou le nombre d'iso-lignes si rien n'est
        précisé.

        Parameters
        ----------
        x   : np.array (1D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels [ou nlevels ou nl]     : int
            Nombre d'iso-lignes voulues (10 par défaut).
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        levels [ou l]                   : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
            Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
        masked_value                    : double
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées).
        n_decades                       : int
            Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
        max_decades                     : int
            Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
        colors [ou c]                   : str ou matplotlib.colors
            N'importe quelle couleur matplotlib. Les iso-lignes auront toutes cette couleur.
        cmap [ou cm]                    : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        linewidths [ou lw]              : float
            Épaisseur de toutes les iso-lignes.
        draw_colorbar [ou dcb ou dc]    : bool
            Si ``True`` affiche la colorbar associée à la carte.
        cax [ou ax ou current_axes]     : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]                   : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**

        See Also
        --------
        .logarithmic_contour
        .linear_contour
        .linear_tricontour
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_contourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=True, is_tri=True)
            cls.__generic_contour(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def linear_tricontour(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste des iso-valeurs.

        On prendra la liste de iso-valeurs si les 2 sont précisés, ou le nombre d'iso-lignes si rien n'est
        précisé.

        Parameters
        ----------
        x   : np.array (1D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D)
            Valeurs de colorations

        Other Parameters
        ----------------
        n_levels [ou nlevels ou nl]  : int
            Nombre d'iso-lignes voulues (10 par défaut).
        levels [ou l]               : list
            Liste de valeurs pour lesquelles tracer une iso-ligne.
        masked_value                : float
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        colors [ou c]               : str ou matplotlib.colors
            N'importe quelle couleur matplotlib. Les iso-lignes auront toutes cette couleur.
        cmap [ou cm]                : str ou matplotlib.colors.Colormap
            La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
        linewidths [ou lw]          : float
            Épaisseur de toutes les iso-lignes.
        draw_colorbar [ou dcb ou dc]: bool
            Si ``True`` affiche la colorbar associée à la carte.
        cax [ou ax ou current_axes] : axes.Axes
            Axes dans lequel la carte va être tracée.
            **À ne pas utiliser depuis le binding C++.**
        label [ou lb]               : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**

        See Also
        --------
        .logarithmic_contour
        .linear_contour
        .logarithmic_tricontour
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_contourf
        .linear_tricontourf
        matplotlib.pyplot.contour
        matplotlib.pyplot.tricontour
        matplotlib.pyplot.contourf
        matplotlib.pyplot.tricontourf
        """
        try:
            nem_tools.autolog('')
            kwargs.update(is_log=False, is_tri=True)
            cls.__generic_contour(x, y, z, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def load_image(cls, file_name: str) -> bool:
        """
        Chargement d'une image au format png.

        Parameters
        ----------
        file_name : str
            (os.PathLike) Chemin complet vers l'image à charger.

        Returns
        -------
        bool
            False si le fichier n'existe pas (warning affiché), True sinon.
        """
        try:
            nem_tools.autolog('')
            if not os.path.exists(file_name):
                warnings.warn(f'File {file_name} not found', RuntimeWarning)
                return False
            img = plt.imread(file_name)
            plt.imshow(img)
            plt.gca().axis('off')
            return True
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def isocolorbar(cls, mappable: mcontour.QuadContourSet = None, cax: maxes.Axes = None, ax: maxes.Axes = None,
                    **kwargs) -> Union[mcolorbar.Colorbar, None]:
        try:
            return nem_contour.isocolorbar(mappable, cax, ax, **kwargs)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def get_contourf_levels(cls) -> np.array:
        """
        Obtention des niveaux de couleurs de la dernière carte affichée via une des méthodes en *contourf*.

        La carte est de type :py:attr:`matplotlib.contour.QuadContourSet`
        ou :py:attr:`matplotlib.contour.TriContourSet`.

        Returns
        -------
        `numpy.ndarray` instance
            Les valeurs des niveaux. Équivalent `matplotlib.contour.ContourSet.levels`

        Raises
        ------
        RuntimeError si aucune carte n'a été dessinée.

        See Also
        --------
        .get_contourf_layers
        .get_contour_levels
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_tricontourf
        .linear_tricontourf
        """
        try:
            nem_tools.autolog('')
            if cls.__last_filled_contourset is not None:
                return cls.__last_filled_contourset.levels
            else:
                raise RuntimeError(f'{cls.__name__}.get_contourf_levels : Aucune carte dessinée pour le moment.')
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def get_contourf_layers(cls) -> np.array:
        """
        Obtention des *layers* de la dernière carte affichée via via une des méthodes en *contourf*.

        La carte est de type :py:attr:`matplotlib.contour.QuadContourSet` ou
        :py:attr:`matplotlib.contour.TriContourSet`.

        Un *layer* est à mi-chemin entre les niveaux (voir _process_colors())

        Returns
        -------
        `numpy.ndarray`
            Les valeurs des *layers*. Équivalent `matplotlib.contour.ContourSet.layers`

        Raises
        ------
        RuntimeError si aucune carte n'a été dessinée.

        See Also
        --------
        .get_contourf_levels
        .get_contour_levels
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_tricontourf
        .linear_tricontourf
        """
        try:
            nem_tools.autolog('')
            if cls.__last_filled_contourset is not None:
                return cls.__last_filled_contourset.layers
            else:
                raise RuntimeError(f'{cls.__name__}.get_contourf_layers : Aucune carte dessinée pour le moment.')
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def get_contour_levels(cls) -> np.array:
        """
        Obtention des valeurs des dernières isolignes affichées via une des méthodes en *contour*.

        La carte est de type :py:attr:`matplotlib.contour.QuadContourSet` ou
        :py:attr:`matplotlib.contour.TriContourSet`.

        Returns
        -------
        `numpy.ndarray`
            Les valeurs des niveaux. Équivalent `matplotlib.contour.ContourSet.levels`

        Raises
        ------
        RuntimeError si aucune isoligne n'a été dessinée.

        Notes
        -----
        * Les *layers* sont identiques aux niveaux (*levels*) pour les iso-lignes.

        See Also
        --------
        .get_contourf_levels
        .get_contourf_layers
        .logarithmic_contourf
        .linear_contourf
        .logarithmic_tricontourf
        .linear_tricontourf
        """
        try:
            nem_tools.autolog('')
            if cls.__last_contourset is not None:
                if isinstance(cls.__last_contourset.levels, (tuple, list)):
                    return np.array(cls.__last_contourset.levels)
                return cls.__last_contourset.levels
            else:
                raise RuntimeError(f'{cls.__name__}.get_contour_levels : Aucune iso-ligne dessinée pour le moment.')
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    # @classmethod
    # def get_canvas(cls) -> mpl.backends.backend_qt5agg.FigureCanvasQTAgg:
    #     if mpl.get_backend() in 'Qt5Agg':
    #         fig_manager = plt.get_current_fig_manager()
    #         # return fig_manager.canvas
    #         return fig_manager.window.takeCentralWidget()
    #     return None

    @classmethod
    def raised_contourf(cls, x: np.array, y: np.array, z: np.array, **kwargs) -> None:
        """
        Affichage d'une surface 3D.

        Version ``nemesis`` de :func:`~mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface`

        Parameters
        ----------
        x   : np.array (1D or 2D)
            Valeurs des abscisses des points de la carte
        y   : np.array (1D or 2D)
            Valeurs des ordonnées des points de la carte
        z   : np.array (1D or 2D)
            Valeurs de colorations

        Other Parameters
        ----------------
        xscale                      : str (in 'log', 'lin')
            Indique l'échelle pour l'axe des X
            Contournement d'un bug connu sur les axes 3D : `ax.set_xscale` and co sont buggés (non prise en
            charge de la transformation des données).
        yscale                      : str (in 'log', 'lin')
            Indique l'échelle pour l'axe des Y
            Contournement d'un bug connu sur les axes 3D : `ax.set_xscale` and co sont buggés (non prise en
            charge de la transformation des données).
        zscale                      : str (in 'log', 'lin')
            Indique l'échelle pour l'axe des Z
        draw_colorbar               : bool
            Si ``True`` affiche la colorbar associée à la surface (nappe).
            Défaut : ``False``
        n_decades                   : int
            Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
        max_decades                 : int
            Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
        cmap [ou cm]                : str ou matplotlib.colors.Colormap ou "current".
            La palette (*colormap*) utilisée pour colorer les faces de la surface.
            Si ``"current"``, utilisation de la colormap par défaut (``mpl.cm.get_cmap()``) ;
            Si non défini : couleur unique pour toutes les faces.
        cax [ou ax ou current_axes] : axes.Axes
            axes dans lesquels dessiner
        label [ou lb]               : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**
        nx                          : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**
        ny                          : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**

        Tous les autres arguments sont passés à la méthode ``mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface``.

        Notes
        -----
        ``x``, ``y`` et ``z`` ont la même forme (1D ou 2D).
        Ce qui donne :

            * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
            * Si ils sont 1D, deux possibilités :

                * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
                * x.size * y.size = z.size : une grille sera créer pour mapper les données

        ``xscale``, ``yscale`` et ``zscale`` : Contournement d'un bug connu sur les axes 3D : `ax.set_xscale` and co
        sont buggés (non prise en charge de la transformation des données).

        See Also
        --------
        mpl_toolkits.mplot3d.Axes3D.plot_surface
        """
        try:
            nem_tools.autolog('')
            cobj = nem_3d.BaseSurface3DHelper(x, y, z, **kwargs)
            cobj.log_transform()
            cobj.check_data()
            # création si on n'est pas déjà en 3D ; choix des formatters des labels des axes
            cobj.set_axis(cls.switch_axes('3d'))

            __surface = cobj.plot_surface()
            # art3d.Poly3DCollection

            cls.grid(cax=cobj.axis)
            cobj.axis.view_init(5, 25)  # view_init(elevation, azimuth)
            cobj.colorbar()
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def set_alpha(cls, alpha: float, index: Union[None, int, Iterable[int]] = None,
                  mpl_type: Union[None, str, collections.Collection] = None) -> None:
        """
        Modification de la transparence d'une ou plusieurs collections pour un type donné.

        * Si `index=None`, alors seule la dernière collection est modifiée.
        * Si `index=[]` ou `()`, alors toutes les collections sont modifiées
        * Si `mpl_type=None`, alors le type de collections est `mplot3d.art3d.Poly3DCollection`

        Parameters
        ----------
        alpha   : float
            Nouvelle transparence
        index   : None ou int ou list ou tuple
            Indices des collections à modifier.
        mpl_type : str ou type `collections` de matplotlib
            Type de collections à modifier.

        Returns
        -------
        None

        Notes
        -----
        Si `mpl_type` est une chaîne de caractères, elle doit être du type 'collections.TypeVoulu` (e.g.
        'collections.PathCollection')
        """
        try:
            nem_tools.autolog('')
            type_3d = False
            if mpl_type is None:
                mpl_type = mplot3d.art3d.Poly3DCollection
                type_3d = True
            else:
                if isinstance(mpl_type, str):
                    mpl_type = eval(mpl_type)

            col = []
            if type_3d and not cls.is_3d():
                raise RuntimeError(f'{__name__}. : Type demandé incompatible avec la boîte d axes actuelle \n\t'
                                   f'Type = {str(mpl_type)}\tBoîte non 3d')

            for entry in plt.gca().collections:
                if isinstance(entry, mpl_type):
                    col.append(entry)

            if index is None:
                col[-1].set_alpha(alpha)
            elif isinstance(index, (list, tuple)):
                if len(index) == 0:
                    # Tous les objets
                    for entry in col:
                        entry.set_alpha(alpha)
                else:
                    for idx in index:
                        col[idx].set_alpha(alpha)
            else:
                col[index].set_alpha(alpha)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def nquiver(cls, x: np.array, y: np.array, u: np.array, v: np.array, **kwargs) -> None:
        """
        Affichage d'un champs de vecteurs.

        Version ``nemesis`` de :func:`~matplotlib.pyplot.quiver`

        Parameters
        ----------
        x   : 1D ou 2D np.array
            Valeurs des abscisses des points
        y   : 1D ou 2D np.array
            Valeurs des ordonnées des points
        u   : 1D ou 2D np.array
            Valeurs de abscisses des vecteurs
        v   : 1D ou 2D np.array
            Valeurs de ordonnées des vecteurs

        Other Parameters
        ----------------
        title           : str
            Titre du graphique (vide par défaut)
        default_title  : bool
            Affiche ou nom le titre pas défaut (défaut=True)
        label           : str
            Label de l'échelle de couleurs (vide par défaut)
        draw_colorbar   : bool
            Affichage ou non de la colorbar (défaut : True)
        draw_quiverkey  : bool
            Affichague ou non de la *quiverkey* – vecteur unité (défaut : True)

        Tous les autres arguments sont passés à la méthode ``matplotlib.pyplot.quiver``.

        Notes
        -----
        ``x`` et ``y`` doivent être de dimension identique (1D ou 2D), et de même forme.
        ``u`` et ``v`` doivent être de dimension identique (1D ou 2D), et de même forme.

        Si ``x`` et ``y`` sont 1D :

            * ils décrivent la base de la grille support (``x`` est de taille ``nx``, ``y`` est de taille ``ny``)
            * ils décrivent la grille support complète (``x`` et ``y`` sont de taille ``nx * ny``)

        Combinaisons possibles :

            * x, y, u et v : 2D
            * x, y, u, et v : 1D
            * x, y, u, et v : 1D
            * x, y : 1D et u, v : 2D
            * x, y : 1D (base de la grille support) et u, v : 2D
            * x, y : 1D (base de la grille support) et u, v : 1D : ne plante pas, mais résultat erroné

        See Also
        --------
        .nstreamplot
        matplotlib.pyplot.quiver
        matplotlib.pyplot.streamplot
        """
        try:
            common_aliases = {'units': ('u',), 'angles': ('ang', 'ag',),
                              'scale': ('sc',), 'scale_units': ('scu',), 'width': ('w',),
                              'headwidth': ('hw',), 'headlength': ('hl',), 'headaxislength': ('hal',),
                              'minshaft': ('minsh',), 'minlength': ('minlg', 'milg',), 'pivot': ('pv',),
                              'color': ('c',),
                              'title': ('title',), 'default_title': ('dt', 'd_ttl',),'draw_colorbar': ('dcb', 'dc',), 'label': ('lb',),
                              'draw_quiverkey': ('dqk', 'dk',)}

            # -- vérification des dimensions des données
            # X et Y doivent être de même dimension : 1D ou 2D
            if not ((x.ndim == y.ndim == 1) or (x.ndim == y.ndim == 2)):
                raise RuntimeError('X et Y doivent être de même dimension (dim = 1 ou dim = 2)')
            # construction du dictionnaire
            quiver_args = cbook.normalize_kwargs(kwargs, common_aliases)
            user_title = quiver_args.pop('title', None)
            draw_colorbar = quiver_args.pop('draw_colorbar', True)
            draw_quiverkey = quiver_args.pop('draw_quiverkey', True)
            lb = quiver_args.pop('label', None)
            draw_default_title = quiver_args.pop('default_title', True)

            with_color_mag = True
            if 'color' in quiver_args.keys():
                with_color_mag = False
                draw_colorbar = False
            # calcul de la magnitude
            mag = np.hypot(u, v)
            # normalisation des composantes
            s_min = mag.min()
            s_max = mag.max()
            mag_norm = s_max - s_min
            u /= mag_norm
            v /= mag_norm
            # affichage du champs de vecteurs
            if with_color_mag:
                q = plt.quiver(x, y, u, v, mag, **quiver_args)
            else:
                q = plt.quiver(x, y, u, v, **quiver_args)
            # modification du titre
            minmax_title = f'Norme : [{s_min:.4e}, {s_max:.4e}]'

            if user_title:
                plt.suptitle(user_title)
                if draw_default_title:
                    plt.title(minmax_title)
            else:
                if draw_default_title:
                    plt.suptitle(minmax_title)
            if draw_colorbar:
                cb_args = {}
                if lb is not None:
                    cb_args['label'] = lb
                plt.gcf().colorbar(q, label=lb)
            if draw_quiverkey:
                plt.gca().quiverkey(q, 0.8, 0.9, 1, 'unité', labelpos='E', coordinates='figure')
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')

    @classmethod
    def nstreamplot(cls, x: np.array, y: np.array, u: np.array, v: np.array, **kwargs) -> None:
        """
        Affichage d'un champs de vecteurs.

        Version ``nemesis`` de :func:`~matplotlib.pyplot.quiver`

        Parameters
        ----------
        x   : 1D ou 2D np.array
            Valeurs des abscisses des points. Fourniture des abscisses des points d'une grille complète ou partielle.
        y   : 1D ou 2D np.array
            Valeurs des ordonnées des points. Fourniture des abscisses des points d'une grille complète ou partielle.
        u   : 1D ou 2D np.array
            Valeurs de abscisses des vecteurs
        v   : 1D ou 2D np.array
            Valeurs de ordonnées des vecteurs

        Other Parameters
        ----------------
        title           : str
            Titre du graphique (vide par défaut)
        draw_colorbar   : bool
            Affichage ou non de la colorbar (défaut : True)
        lw_mag          : bool
            Utilisation de la norme du vecteur comme largeur des lignes
        nx                          : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
        ny                          : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.


        Tous les autres arguments sont passés à la méthode ``matplotlib.pyplot.streamplot``.

        Notes
        -----
        ``x`` et ``y`` doivent être de dimension identique (1D ou 2D), et de même forme.
        ``u`` et ``v`` doivent être de dimension identique (1D ou 2D), et de même forme.

        Si ``x``, ``y``, ``u`` et ``v`` sont 1D et de même taille, alors ``nx`` et ``ny`` doivent être indiqués

        Si ``x`` et ``y`` sont 1D :

            * ils décrivent la base de la grille support (``x`` est de taille ``nx``, ``y`` est de taille ``ny``)
            * ils décrivent la grille support complète (``x`` et ``y`` sont de taille ``nx * ny``)

        Combinaisons possibles :

            * x, y, u et v : 2D
            * x, y : 2D, u, v : 1D
            * x, y : 1D et u, v : 2D
            * x, y : 1D (base de la grille support) et u, v : 2D
            * x, y : 1D (base de la grille support) et u, v : 1D
            * x, y, u, et v : 1D. Précisez ``nx`` et ``ny``


        See Also
        --------
        .nquiver
        matplotlib.pyplot.streamplot
        matplotlib.pyplot.quiver
        """
        try:
            common_aliases = {'density': ('d',), 'linewidth': ('lw', 'linewidths'), 'color': ('c',),
                              'cmap': ('cm',), 'norm': ('no',), 'arrowsize': ('asize', 'as',),
                              'arrowstyle': ('astyle', 'ast',), 'minlength': ('minlg', 'milg',),
                              'zorder': ('zo',), 'start_points': ('spoints', 'spts',),
                              'maxlength': ('maxlg', 'malg',), 'integration_direction': ('i_dir', 'int_dir'),
                              'title': ('title',), 'lw_mag': ('lwm',), 'draw_colorbar': ('dcb', 'dc',)}

            # --- Vérification des dimensions des données et de leurs cohérences
            #   plt.streamplot est moins permissif que plt.quiver
            if x.ndim == y.ndim == u.ndim == v.ndim == 1 and x.size == y.size == u.size == v.size:
                common_aliases.update(nx=('n_x',), ny=('n_y',))

            # construction du dictionnaire
            strm_args = cbook.normalize_kwargs(kwargs, common_aliases)
            user_title = strm_args.pop('title', None)
            with_lw_mag = strm_args.pop('lw_mag', False)
            draw_colorbar = strm_args.pop('draw_colorbar', True)

            if x.ndim == y.ndim == 2:
                if u.ndim == v.ndim == 2:
                    if not (x.shape == y.shape == u.shape == v.shape):
                        msg = f'{x.shape}\t{y.shape}\t{u.shape}\t{v.shape}'
                        raise RuntimeError(f"X, Y, U et V doivent avoir la même forme : {msg}")
                elif u.ndim == v.ndim == 1:
                    # Cas X et Y 2D, U et V 1D
                    if not (u.shape == v.shape and x.shape == y.shape and u.shape[0] == x.size):
                        print(f'u.shape[0] == v.shape[0] : {u.shape[0] == v.shape[0]}')
                        print(f'u.shape[0] == x.shape[0] : {u.shape[0] == x.size}')
                        print(f'x.shape == y.shape : {x.shape == y.shape}')
                        msg_uv = f'Forme de U ou V invalide : U:{u.shape} == V:{v.shape}' \
                            if not (u.shape == v.shape) else ''
                        msg_xy = f'Forme de X ou Y invalide : X:{x.shape} == Y:{y.shape})' \
                            if not (x.shape == y.shape) else ''
                        msg_xyuv = f'Forme de U ou V incompatible avec forme de X ou Y : ' \
                                   f'U:{u.shape} == V:{v.shape} U0:{u.shape[0]} == X0:{x.size}' \
                            if not (u.shape[0] == x.size) else ''
                        raise RuntimeError(
                            f"Cas X et Y 2D, U et V 1D\n\tCombinaison des formes des tableaux invalide\n"
                            f"{msg_xy} {msg_uv} {msg_xyuv}")
                    else:
                        u = u.reshape(x.shape)
                        v = v.reshape(y.shape)
                else:
                    raise RuntimeError(f"U et V doivent être de même dimension : 1 ou 2 : {u.ndim}, {v.ndim}")
            elif x.ndim == y.ndim == 1:
                if u.ndim == u.ndim == 2:
                    if not (u.shape == v.shape and x.shape == y.shape):
                        msg = f'{x.shape}\t{y.shape}\t{u.shape}\t{v.shape}'
                        raise RuntimeError(f"X, Y, U et V doivent avoir la même forme : {msg}")
                    else:
                        # Cas X et Y 1D complet, U et V 2D
                        if x.shape[0] == u.size:
                            x = x.reshape(u.shape)
                            y = y.reshape(u.shape)
                        # Cas X et Y 1D base de la grille, U et V 2D -> rien à faire
                        elif not (x.shape[0] == u.shape[0]):
                            msg = f'X0:{x.shape[0]} == U0:{u.shape[0]}'
                            raise RuntimeError(f"Combinaison des formes invalide : {msg}")
                elif u.ndim == v.ndim == 1:
                    # Cas X et Y 1D base de la grille, U et V 1D
                    if x.size * y.size == u.size and u.size == v.size:
                        new_shape = (x.size, y.size)
                        u = u.reshape(new_shape)
                        v = v.reshape(new_shape)
                    # Cas X et Y 1D complet, U et V 1D : nx et ny doivent exister
                    elif x.size == y.size == u.size == v.size:
                        nx, ny = strm_args.pop('nx', None), strm_args.pop('ny', None)
                        if nx and ny and x.size == nx * ny:
                            new_shape = (nx, ny)
                            x = x.reshape(new_shape)
                            y = y.reshape(new_shape)
                            u = u.reshape(new_shape)
                            v = v.reshape(new_shape)
                        else:
                            if not (nx and ny):
                                raise RuntimeError('X, Y, U et V 1D et de même taille : nx et ny doivent être préciser')
                            else:
                                raise RuntimeError(
                                    f"X, Y, U et V doivent être de taille nx*ny ({nx * ny} – X: {x.size})")
                else:
                    raise RuntimeError(f"U et V doivent être de même dimension : 1 ou 2 : {u.ndim}, {v.ndim}")
            else:
                raise RuntimeError(f"X et Y doivent être de même dimension : 1 ou 2 : {x.ndim}, {y.ndim}")

            # calcul de la magnitude
            mag = np.hypot(u, v)
            # normalisation des composantes
            s_min = mag.min()
            s_max = mag.max()
            mag_norm = s_max - s_min
            u /= mag_norm
            v /= mag_norm
            # création tableaux des épaisseurs
            if with_lw_mag:
                lw = 5 * mag / mag.max()
                if 'linewidth' not in strm_args.keys():
                    strm_args['linewidth'] = lw
            # affichage du streamplot
            if 'color' not in strm_args.keys():
                strm = plt.streamplot(x, y, u, v, color=mag, **strm_args)
            else:
                if strm_args['color'] is None:
                    draw_colorbar = False
                strm = plt.streamplot(x, y, u, v, **strm_args)
            if draw_colorbar:
                plt.gcf().colorbar(strm.lines)
            # modification du titre
            minmax_title = f'Norme : [{s_min:.4e}, {s_max:.4e}]'
            if user_title:
                plt.suptitle(user_title)
                plt.title(minmax_title)
            else:
                plt.suptitle(minmax_title)
        except RuntimeError:
            nem_tools.autolog_error('RuntimeError')
