# -*- coding: utf-8 -*-

import warnings

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as mcm
import matplotlib.cbook as cbook
import matplotlib.colors as mcolors
import matplotlib.ticker as mticker
import mpl_toolkits.mplot3d as mplot3d
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import nemesis.plotter.numpy_tools as nempy_tools
import nemesis.plotter.ticker as nem_ticker
import nemesis.tools as nem_tools


def move_parameters(keys: tuple, **kwargs: dict) -> (dict, dict):
    """
    Déplacement des paires de ``kwargs`` dont la clé est dans la liste ``keys`` vers un dictionnaire «local».

    Parameters
    ----------
    keys        : tuple
        Liste des clés qui doivent être déplacées.
    **kwargs    : dict
        Dictionnaire à analyser

    Returns
    -------
    dict, dict
        Le dictionnaire «local»
        le dictionnaire des paramètres restant à analyser.
    """
    local_kwargs = {}
    if len(keys) > 0:
        for key in keys:
            if key in kwargs:
                local_kwargs[key] = kwargs.pop(key)
    else:
        for key, value in kwargs.items():
            local_kwargs[key] = value
    return local_kwargs, kwargs


class BaseSurface3DHelper(object):
    """
    Ensemble de fonctionnalités pour afficher une surface 3D – 3D Nappe
    """
    def __init__(self, x: np.array, y: np.array, z: np.array, other_aliases: dict = None, **kwargs: object):
        """
        Initialisation des paramètres de classes et analyse des paramètres passés par l'utilisateur.
        Parameters
        ----------
        x   : np.array
            Tableau des abscisses
        y   : np.array
            Tableau des ordonnées
        z   : np.array
            Tableau des cotes
        other_aliases : dict
            Paramètres supplémentaires – ne sert pas pour l'instant

        Other Parameters
        ----------------
        xscale                      : str (in 'log', 'lin')
            Indique l'échelle pour l'axe des X
            Contournement d'un bug connu sur les axes 3D : `ax.set_xscale` and co sont buggés (non prise en
            charge de la transformation des données).
        yscale                      : str (in 'log', 'lin')
            Indique l'échelle pour l'axe des Y
            Contournement d'un bug connu sur les axes 3D : `ax.set_xscale` and co sont buggés (non prise en
            charge de la transformation des données).
        zscale                      : str (in 'log', 'lin')
            Indique l'échelle pour l'axe des Z
        draw_colorbar               : bool
            Si ``True`` affiche la colorbar associée à la surface (nappe).
            Défaut : ``False``
        n_decades                   : int
            Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
        max_decades                 : int
            Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
        cmap [ou cm]                : str ou matplotlib.colors.Colormap ou "current".
            La palette (*colormap*) utilisée pour colorer les faces de la surface.
            Si ``"current"``, utilisation de la colormap par défaut (``mpl.cm.get_cmap()``) ;
            Si non défini : couleur unique pour toutes les faces.
        cax [ou ax ou current_axes] : axes.Axes
            axes dans lesquels dessiner
        label [ou lb]               : str
            Légende de l'échelle de couleurs des iso-lignes.
            **À ne pas utiliser depuis le binding C++.**
        nx                          : int
            Nombre de points en abscisses de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**
        ny                          : int
            Nombre de points en ordonnées de la grille.
            À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
            **À ne pas utiliser depuis le binding C++.**

        Tous les autres arguments sont passés à la méthode ``mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface``.

        Notes
        -----
        ``x``, ``y`` et ``z`` ont la même forme (1D ou 2D).
        Ce qui donne :

            * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
            * Si ils sont 1D, deux possibilités :

                * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
                * x.size * y.size = z.size : une grille sera créer pour mapper les données

        ``xscale``, ``yscale`` et ``zscale`` : Contournement d'un bug connu sur les axes 3D : `ax.set_xscale` and co
        sont buggés (non prise en charge de la transformation des données).

        See Also
        --------
        mpl_toolkits.mplot3d.Axes3D.plot_surface
        """
        self.common_aliases = {'nx': ('n_x',), 'ny': ('n_y',),
                               'masked_value': ('mv',),
                               'cmap': ('cm',), 'label': ('lb',),
                               'xscale': ('xs',), 'yscale': ('ys',), 'zscale': ('zs',),
                               'mapping': ('map', ),
                               'draw_colorbar': ('dcb', 'dc'),
                               'cax': ('current_axes', 'ax')}
        self.surface_kwargs = {}
        self.colorbar_kwargs = {}
        self.surface = None
        self.x = x
        self.y = y
        self.z = z
        self.scale = {'x': 'lin', 'y': 'lin', 'z': 'lin'}
        self.n_decades = 15
        self.max_decades = 100
        self.draw_colorbar = True
        self.label = None
        self.mapping = None
        self.axis = None

        if other_aliases is not None:
            self.common_aliases.update(**other_aliases)

        self.norm_kwargs = cbook.normalize_kwargs(kwargs, self.common_aliases)
        self._process_args()
        self._cmap_process_args()
        self._surface_process_args()

    def _process_args(self) -> None:
        self.scale['x'] = self.norm_kwargs.pop('xscale', self.scale['x'])
        self.scale['y'] = self.norm_kwargs.pop('yscale', self.scale['y'])
        self.scale['z'] = self.norm_kwargs.pop('zscale', self.scale['z'])
        self.masked_value = self.norm_kwargs.pop('masked_value', None)
        self.n_decades = self.norm_kwargs.pop('n_decades', self.n_decades)
        self.max_decades = self.norm_kwargs.pop('max_decades', self.max_decades)
        self.draw_colorbar = self.norm_kwargs.pop('draw_colorbar', self.draw_colorbar)
        self.label = self.norm_kwargs.pop('label', self.label)
        self.mapping = self.norm_kwargs.pop('mapping', self.mapping)
        self.axis = self.norm_kwargs.pop('cax', self.axis)

    def _cmap_process_args(self) -> None:
        """
        Analyse des paramètres de ``self.norm_args`` pour la colormap.
        """
        if 'cmap' in self.norm_kwargs:
            cmap_obj = self.norm_kwargs.pop('cmap')
            if isinstance(cmap_obj, str) is True:
                if cmap_obj.lower() == 'current':
                    self.surface_kwargs['cmap'] = mcm.get_cmap()
                else:
                    self.surface_kwargs['cmap'] = cmap_obj
        else:
            # Si pas de cmap, pas de coloration, donc pas besoin de colorbar
            self.draw_colorbar = False

    def _surface_process_args(self) -> None:
        # À ce stade, normalement les paramètres pour la colorbar ou interne à cette classe ont été «pris»
        #   dans self.norm_kwargs. Tout ce qui reste est passé à plot_surface.
        # surface_keys = ['ccount', 'rcount', 'rstride', 'cstride', 'vmin', 'vmax', 'shade']
        local_kwargs, self.norm_kwargs = move_parameters((), **self.norm_kwargs)
        self.surface_kwargs.update(local_kwargs)

    def _check_log_axes(self, name: str) -> str:
        if self.scale[name] is not None:
            if isinstance(self.scale[name], (str,)):
                if self.scale[name] not in ('lin', 'log'):
                    warnings.warn(f'Échelle pour {name} invalide ("lin" ou "log" valides). '
                                  f'Trouvé : {self.scale[name]})')
        else:
            self.scale[name] = 'lin'
        return self.scale[name]

    def check_data(self) -> None:
        # Utilisation g_kwargs['nx'] et g_kwargs['ny'] si présents
        # Création d'un «maillage» numpy si nécessaire
        self.x, self.y, self.z = nempy_tools.NumpyHelper.check_mesh(self.x, self.y, self.z, **self.norm_kwargs)

    def _log_transform_zaxis(self) -> np.array:
        """
        Transformation du tableau z si nécessaire.

        Il se peut que le minimum soit modifier si le nombre de décades est trop grand par rapport à ce que veut
        l'utilisateur.

        Returns
        -------
        np.array
            Valeurs du tableau transformé si nécessaire
        """
        if self.masked_value is not None:
            self.z = np.where(self.z < self.masked_value, self.masked_value, self.z)
            # @FIXME à mettre en info, pas en warning
            # warnings.warn(f'Les valeurs de z < {self.masked_value} ont été remplacées par {self.masked_value}')
        lg = self._check_log_axes('z')
        # _log.debug(f'_log_transform_zaxis : lg = {lg}')
        if lg == 'log':
            old_z_min = self.z.min()
            is_new, z_min = nempy_tools.NumpyHelper.compute_new_zmin(self.z, self.n_decades, self.max_decades)
            # _log.debug(f'\tRetour de compute_new_zmin : {is_new}, {z_min} ')
            if is_new is True:
                # _log.debug('\tNouveau min')
                self.z = np.where(self.z < z_min, z_min, self.z)
                # @FIXME à mettre en info, pas en warning
                # warnings.warn(f'Les valeurs de z < {z_min} ont été remplacées par {z_min} '
                #               f'(Ancien minimum : {old_z_min})')
            # else:
            # _log.debug('\tAncien min')
            return np.log10(self.z)
        return self.z

    def _log_transform_axis(self, array: np.array, scale: str) -> np.array:
        """
        Analyse des arguments concernant un passage en log d'un axe.

        Si l'axe correspondant à param est demandé en log (via une clé dans kwargs), alors cette méthode retourne
        (True, les données passées en log10, le dictionnaire allégé de la clé param).

        Parameters
        ----------
        array       : np.array
            Tableau des données pour l'axe ``param``
        scale       : str
            'xscale' ou 'yscale' ou 'zscale'

        Returns
        -------
        np.array
            Valeurs du tableau transformé si nécessaire
        """
        lg = self._check_log_axes(scale)
        if lg == 'log':
            return np.log10(array)
        return array

    def _set_axes_formatter(self) -> None:
        if self.scale['x'] == 'log':
            self.axis.xaxis.set_major_formatter(nem_ticker.PseudoLogMathTextSciFormatter("%1.2e"))
        if self.scale['y'] == 'log':
            self.axis.yaxis.set_major_formatter(nem_ticker.PseudoLogMathTextSciFormatter("%1.2e"))
        self._update_kwargs_for_mapping()

    def log_transform(self) -> None:
        self.x = self._log_transform_axis(self.x, 'x')
        self.y = self._log_transform_axis(self.y, 'y')
        self.z = self._log_transform_zaxis()

    def set_axis(self, axis) -> None:
        self.axis = self.axis if self.axis is not None else axis
        self._set_axes_formatter()

    def _update_kwargs_for_mapping(self) -> None:
        if self.mapping == 'log':
            # aucune transformation de z (à part peut-être z < z_min)
            # attention : peut écraser le paramètre norm ajouté par l'utilisateur
            self.surface_kwargs['norm'] = mcolors.LogNorm()
            # formatage des labels de l'axe Z — comme z inchangés, on ne touche pas au format par défaut
            # current_axes.zaxis.set_major_formatter(cls.__log_formatter)
            # self.axis.zaxis.set_major_formatter(mticker.LogFormatterSciNotation(minor_thresholds=(np.inf, np.inf)))
            # formattage des ticks de la colorbar : norm=LogNorm(), donc formattage en log de la colorbar
            self.colorbar_kwargs['format'] = mticker.LogFormatterSciNotation(minor_thresholds=(np.inf, np.inf))
        else:
            if self.scale['z'] == 'log':
                # z = np.log10(z)
                # formattage des ticks de la colorbar
                self.colorbar_kwargs['format'] = nem_ticker.PseudoLogMathTextSciFormatter("%1.2e")
                # formatage des labels de l'axe Z
                self.axis.zaxis.set_major_formatter(nem_ticker.PseudoLogMathTextSciFormatter("%1.2e"))

    def plot_surface(self) -> mplot3d.art3d.Poly3DCollection:
        if 'zorder' not in self.surface_kwargs:
            self.surface_kwargs['zorder'] = 0
        self.surface = self.axis.plot_surface(self.x, self.y, self.z, **self.surface_kwargs)
        return self.surface

    def colorbar(self) -> None:
        if self.draw_colorbar:
            cb = plt.gcf().colorbar(self.surface, ax=self.axis, **self.colorbar_kwargs)
            if cb is not None:
                if self.label is not None and len(self.label) != 0:
                    cb.set_label(self.label)
