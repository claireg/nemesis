# -*- coding: utf-8 -*-
import warnings
from dataclasses import dataclass

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLineCollection
from matplotlib.lines import Line2D
import matplotlib.collections as mcol
import matplotlib.lines as mlines
import matplotlib.axes as maxes
import matplotlib.transforms as mtransforms


class HandlerDashedLines(HandlerLineCollection):
    """
    Custom Handler for LineCollection instances.
    """

    def create_artists(self, legend, orig_handle,
                       xdescent, ydescent, width, height, fontsize, trans):
        # figure out how many lines there are
        numlines = len(orig_handle.get_segments())
        xdata, xdata_marker = self.get_xdata(legend, xdescent, ydescent,
                                             width, height, fontsize)
        leglines = []
        # divide the vertical space where the lines will go
        # into equal parts based on the number of lines
        if isinstance(xdata, list):
            xdata = np.array(xdata)
        ydata = (height / (numlines + 1)) * np.ones(xdata.shape, float)
        # for each line, create the line at the proper location
        # and set the dash pattern
        for i_line in range(numlines):
            legline = Line2D(xdata, ydata * (numlines - i_line) - ydescent)
            self.update_prop(legline, orig_handle, legend)
            # set color, dash pattern, and linewidth to that
            # of the lines in linecollection
            try:
                color = orig_handle.get_colors()[i_line]
            except IndexError:
                color = orig_handle.get_colors()[0]
            try:
                dashes = orig_handle.get_dashes()[i_line]
            except IndexError:
                dashes = orig_handle.get_dashes()[0]
            try:
                lw = orig_handle.get_linewidths()[i_line]
            except IndexError:
                lw = orig_handle.get_linewidths()[0]
            if dashes[1] is not None:
                print(dashes[0])
                print(dashes[1])
                legline.set_dashes(dashes[1])
            legline.set_color(color)
            legline.set_transform(trans)
            legline.set_linewidth(lw)
            leglines.append(legline)
        return leglines


@dataclass
class NemSubplotPars:
    left: float
    right: float
    bottom: float
    top: float
    wspace: float
    hspace: float


def check_curve(num: int) -> mlines.Line2D:
    """
    Vérifie si la `num`-ième courbe existe.

    Parameters
    ----------
    num : int
        Indice de la courbe à vérifier

    Returns
    -------
        mpl.lines.Line2D si la courbe existe, sinon lève une exception.

    Raises
    ------
        RuntimeError si la courbe n'existe pas.
    """
    all_lines = plt.gca().lines
    if num >= len(all_lines):
        raise RuntimeError(f'{__name__}.__check_curve : courbe inexistante \n\t'
                           f'Demandé : {num}\tPossible : {len(all_lines)}')
    return all_lines[num]


def create_legend_line_collections(idx0: list) -> list:
    """
    Création d'autant de collections de lignes qu'il y a de réseaux

    Parameters
    ----------
    idx0 : list[int]
        Indice de la première courbe de chaque réseau

    Returns
    -------
        list de mpl.collections.LineCollection
    """
    # make list of one line -- doesn't matter what the coordinates are
    line = [[(0, 0)]]
    # set up the proxy artist

    # all_lines = plt.gca().lines
    all_lines = plt.gcf().gca().lines
    if len(all_lines) == 0:
        raise RuntimeError("Aucune courbe ou réseau affiché.")

    n_networks = len(idx0)
    line_collections = [None] * n_networks
    n_curves = [0] * n_networks
    end = len(idx0) - 1
    if n_networks == 1:
        n_curves[0] = len(all_lines)
    else:
        # calcul du nombre de courbes par réseau
        for i, e in enumerate(idx0):
            if i == 0:
                n_curves[i] = idx0[i + 1]
            elif i == end:
                n_curves[i] = len(all_lines) - e
            else:
                n_curves[i] = idx0[i + 1] - e
    # Pour chaque réseau, création d'une LineCollection représentant le réseau (style et couleurs)
    for net in range(n_networks):
        start = idx0[net]
        n_lines_in_col = 3 if n_curves[net] > 2 else n_curves[net]
        net_styles = [None] * n_lines_in_col
        net_colors = [None] * n_lines_in_col
        for i in range(n_lines_in_col):
            current_line = all_lines[start + i]
            net_styles[i] = current_line.get_linestyle()
            net_colors[i] = current_line.get_color()
        line_collections[net] = mcol.LineCollection(n_lines_in_col * line, linestyles=net_styles, colors=net_colors)
    return line_collections


def move_axes_net_legend(parent: maxes.Axes, **kwargs) -> ((float, float), mtransforms.Bbox):
    """
    Déplacement des axes pour la légende de réseau.

    Parameters
    ----------
    parent : axes.Axes
        Boîte d'axes à redimenssioner

    Other Parameters
    ----------------
    loc : str
        La localisation de la légende.
    fraction
    shrink
    aspec
    pad

    Returns
    -------
    (float, float), Bbox

    Warnings
    --------
    Fonctionne si la position est lower center
    """
    loc = kwargs.pop('loc', 'lower center')
    if loc not in ('upper right', 'upper left', 'upper center', 'lower right', 'lower left', 'lower center'):
        warnings.warn(f'{loc} : Unauthorized legend location for networks. \'lower\' center will be used.')
        loc = 'lower center'
    fraction = kwargs.pop('fraction', 0.1)
    shrink = kwargs.pop('shrink', 1.0)
    # aspect = kwargs.pop('aspect', 20)
    # pb = transforms.PBox(parent.get_position())
    pb = parent.get_position(original=True).frozen()
    if 'lower' in loc:
        pad = kwargs.pop('pad', 0.15)  # 0.05 ok si pas de labels de tracer
        pbcb, pbx, pb1 = pb.splity(fraction, fraction + pad)
        pbcb = pbcb.shrunk(shrink, 1.0).anchored('C', pbcb)
        # aspect = 1.0 / aspect
        anchor = kwargs.pop('anchor', (0.25, 1.0))
        panchor = kwargs.pop('panchor', (0.25, 0.0))
    else:
        pad = kwargs.pop('pad', 0.05)
        x1 = 1.0 - fraction
        pb1, pbx, pbcb = pb.splity(x1 - pad, x1)
        pbcb = pbcb.shrunk(1.0, shrink).anchored('C', pbcb)
        anchor = kwargs.pop('anchor', (0.0, 0.25))
        panchor = kwargs.pop('panchor', (1.0, 0.25))
    parent.set_position(pb1)
    parent.set_anchor(panchor)
    return anchor, pbcb
