# Package nemesis

Package principal du projet

Contient différents sous-paquets tels que les tests ou la lecture des données.

# Tests de la partie Python du projet nemesis

Copier-coller de la *docstrings* du module `run_tests.py`.

En ligne de commande, depuis le répertoire `nemesis`, les tests passent:

    python3 run_tests.py
    # ou
    pytest

Possibilité de lancer les tests depuis le répertoire `tests` pour un seul module:

    pytest test_plotter.py

Possibilité de lancer UN test depuis le répertoire de `tests`:

    pytest [-v --color=yes --junitxml=./local.xml] test_read_data.py::test_read_psy_file_onde
    # pytest module_à_tester.py::fonction_a_tester

### Notes

* La comparaison d'images fonctionne (utilisation decorator `matplotlib`).
* La comparaison de dictionnaire pour objets Psyché fonctionne (fonction dans `testing/__init__.py`)
* Les tests en doctest sont pris en compte par `pytest` (même lancé depuis `pycharm`).
* Un fichier au format `junit` est créé (+ modification xml pour ajout xslt si hors Jenkins)
* 2 fichiers de config `py.test` :
    * ``nemesis/pytest.ini``
    * ``testing/conftest.py``

### Warnings

Pour la comparaison de chemin vers un fichier ou répertoire, il faut utiliser `os.path.samefile(f1, f2)` car
selon la façon de lancer le test, le chemin peut différer mais pointer sur le même fichier.