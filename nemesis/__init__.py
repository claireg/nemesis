# -*- coding: utf-8 -*-

"""
Package nemesis.


.. literalinclude:: ../../nemesis/README.md

"""
import logging.config
import os
import json
import datetime
import sys

__version__ = "0.1.6"

__title__ = "nemesis"
__description__ = "CEA plotting package based on matplotlib."

__uri__ = "file://.../share/docs/nemesis.html"
__doc__ = __description__ + " <" + __uri__ + ">"

__author__ = "Claire Guilbaud"
__email__ = "claire.guilbaud@cea.fr"

__license__ = "???MIT???"
__copyright__ = "Copyright (c) 2020 CEA"

_called_from_test = False


def setup_logging(default_path='logging_ini.json', default_level=logging.INFO, env_key='LOG_CFG'):
    """ Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    else:
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), path)
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        # Au cas où les répertoires dans lesquels les logs vont être écrits n'existeraient pas
        for h, v in config['handlers'].items():
            if 'filename' in v:
                fname = os.path.dirname(v['filename'])
                os.makedirs(fname, exist_ok=True)
                # suppression des fichiers .npy de la session précédente
                for file in os.listdir(fname):
                    if file.endswith(".npy"):
                        os.remove(os.path.join(fname, file))
        try:
            logging.config.dictConfig(config)
        except ValueError as e:
            # Contournement au cas où le pkg python-json-logger n'existe pas, pour que nemesis fonctionne quand même
            print(e)
            logging.basicConfig(level=default_level)
    else:
        logging.basicConfig(level=default_level)


def process_name(pid):
    import subprocess
    try:
        # cmd = f'ps -p {pid} -o comm='
        cmd = f'ps -p {pid} -o args='
        output = subprocess.check_output(cmd.split())
        output = output.decode(sys.getfilesystemencoding()).strip()
    except subprocess.CalledProcessError as e:
        logger = logging.getLogger('console')
        logger.warning(e)
        output = ''
    return output


def information():
    import getpass
    # ne pas utiliser args ou process comme clé car clés déjà utilisées dans le système de logging
    info = dict.fromkeys(['date', 'pinfo', 'user', 'python_version', 'platform'])
    info['date'] = datetime.datetime.now().isoformat()
    info['pinfo'] = dict.fromkeys(['caller', 'argv', 'pid', 'ppid'])
    if len(sys.argv) > 0:
        info['pinfo']['caller'] = 'python'
        info['pinfo']['argv'] = sys.argv
    else:
        pid_name = process_name(os.getpid())
        info['pinfo']['caller'] = 'c++'
        info['pinfo']['args'] = pid_name
    info['pinfo']['pid'] = os.getpid()
    info['pinfo']['ppid'] = os.getppid()
    info['user'] = dict.fromkeys(['name', 'resuid'])
    info['user']['name'] = getpass.getuser()
    info['user']['resuid'] = os.getresuid()
    info['python_version'] = sys.version_info
    if not sys.platform.startswith('linux'):
        info['platform'] = sys.platform
    else:
        import platform
        info['platform'] = platform.uname()._asdict()
    return info


def initial_log():
    info = information()
    ulogger = logging.getLogger('uselog')
    ulogger.info('nemesis.__init__', extra=info)
    autologger = logging.getLogger('autolog')
    autologger.info('#')
    autologger.info('# Some information:')
    autologger.info(f'#   Date: {info["date"]}')
    autologger.info(f'#   Process:')
    autologger.info(f'#     Called from {info["pinfo"]["caller"]}: {info["pinfo"]["argv"]}')
    autologger.info(f'#     Pid: {info["pinfo"]["pid"]}\tPpid: {info["pinfo"]["ppid"]}')
    autologger.info(f'#   User:')
    autologger.info(f'#     Resuid: {info["user"]["resuid"]}')  # ne pas utiliser {getpass.getuser()} ?
    autologger.info(f'#     Name: {info["user"]["name"]}')
    v = '.'.join([f'{value}' for value in info['python_version']])
    autologger.info(f'#   Python version: {v}')
    # logger.info(f'# implementation: {sys.implementation}\n')
    if isinstance(info['platform'], dict):
        autologger.info('#   Platform: ')
        uname = info['platform']
        str_uname = '\n'.join([f'#     {k}: {v}' for k, v in uname.items()])
        autologger.info(str_uname)
    else:
        autologger.info(f'#   Platform: {sys.platform}')
    autologger.info('#')
    autologger.info('import nemesis\n')


setup_logging()
# Pour tester que le logger 'nemesis' fonctionne — affichage sur la console
# logger = logging.getLogger(__name__)
# __name__ = nemesis
# logger.info("Et c'est parti !")
initial_log()
