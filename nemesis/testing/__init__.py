# -*- coding: utf-8 -*-

"""
todo
----

* compare_dict devrait être un ``pytest_assertrepr_compare`` hook
    (hook)[/path/vers/install/shaare/python/pytest-3.4.1/assert.html#defining-your-own-assertion-comparison]

"""
import os
import locale
import warnings
import sys
from io import StringIO
from typing import Any, Dict, NoReturn, Union

import numpy as np
from numpy.testing import (
    assert_equal,
    assert_array_equal,
)
import matplotlib as mpl
from matplotlib.cbook import MatplotlibDeprecationWarning
import nemesis.readdata as nem_rd

import pickle


pickle_extension = "pickle"
npz_extension = "npz"


def get_objects_test_dir(f_name_: str):
    """
    Retourne le nom complet du répertoire contenant les fichiers de référence.

    Si ce répertoire n'exist pas, il est créé.

    Parameters
    ----------
    f_name_ : str
        (os.PathLike) Nom complet du module (__file__ depuis le module appelant)

    Returns
    -------
    str
        Nom du répertoire de test.
    """
    t_name_py = os.path.basename(f_name_)
    t_name, t_name_ext = os.path.splitext(t_name_py)
    test_dir = os.path.normpath(
        os.path.join(os.path.dirname(__file__), '..', 'tests', 'baseline_objects', t_name))
    if not os.path.exists(test_dir):
        os.mkdir(test_dir)
    return test_dir


def get_file_test(filename_: str) -> str:
    return os.path.join(get_objects_test_dir(filename_))


def skip_home(i_dir_: str) -> str:
    i_home = i_dir_.find('home')
    return i_dir_[i_home:]


def pickle_object(filename_: str, object_: Any) -> None:
    pickle.dump(object_, open(filename_, 'wb'), pickle.HIGHEST_PROTOCOL)


def load_pickled(dirname_: str, testname_: str, object_: Any) -> Dict[str, Any]:
    pick_file = create_name_for_pickle(dirname_, testname_)
    try:
        result = pickle.load(open(pick_file, 'rb'))
    except FileNotFoundError:
        print('Creation {}'.format(pick_file))
        # create ref data automatically at first call
        pickle_object(pick_file, object_)
        result = pickle.load(open(pick_file, 'rb'))
    return result


def __create_name_for_extension(test_dir_: str, name_: str, extension_: str) -> str:
    """
    Création d'un nom de fichier pour un test de nom `name_.extension_`

    Parameters
    ----------
    test_dir_ : os.PathLike object
        Répertoire du test
    name_ : str
        Nom du test (e.g. toto)
    extension_ : str
        Nom de l'extension

    Returns
    -------
    os.PathLike object
        Nom complet d'un fichier correspondant au test
        (e.g. …/tests/baseline_objects/nom_module_test/toto.pck)
    """
    name = f'{name_}.{extension_}'
    return os.path.join(test_dir_, name)


def create_name_for_pickle(test_dir_: str, name_: str) -> str:
    """
    Création d'un nom de fichier pickle pour un test de nom `name_`

    Parameters
    ----------
    test_dir_ : os.PathLike object
        Répertoire du test
    name_ : str
        Nom du test (e.g. toto)

    Returns
    -------
    os.PathLike object
        Nom complet d'un fichier correspondant au test
        (e.g. …/tests/baseline_objects/nom_module_test/toto.pck)
    """
    return __create_name_for_extension(test_dir_, name_, pickle_extension)


def create_name_for_npz(test_dir_: str, name_: str) -> str:
    """
    Création d'un nom de fichier npz (multiarray numpy) pour un test de nom `name_`

    Parameters
    ----------
    test_dir_ : os.PathLike object
        Répertoire du test
    name_ : str
        Nom du test (e.g. toto)

    Returns
    -------
    os.PathLike object
        Nom complet d'un fichier correspondant au test
    """
    return __create_name_for_extension(test_dir_, name_, npz_extension)


def compare_psy_dict(ref_dict_, tst_dict_):
    for (tst_key, tst_value) in tst_dict_.items():
        ref_value = ref_dict_.get(tst_key, None)
        if ref_value is None:
            raise Exception('Key {k} in test_data but not in ref_data'.format(k=tst_key))
        if tst_key == 'filepath':
            # on vérifie que c'est le même fichier — le path peut différé.
            print(f'\tTst : {tst_value}\tRef : {ref_value}')
            # if os.path.samefile(tst_value, ref_value) is not True:
            # Suite anonymisation des paths, on ne peut plus utiliser os.path.samefile
            if tst_value != ref_value:
                cmp_data = "{td}\t!= {rd}".format(td=tst_value, rd=ref_value)
                raise Exception('Key {k} : file not the same\n\t({diff})'.format(k=tst_key, diff=cmp_data))
        elif isinstance(tst_value, dict):
            compare_psy_dict(ref_value, tst_value)
        elif isinstance(tst_value, list):
            if isinstance(ref_value, (nem_rd.PsyObject, nem_rd.PsyEnum)):
                # Cas des lignes tutu = [R_2]
                if tst_value[0] != ref_value:
                    cmp_data = "{td}\t!= {rd}".format(td=tst_value, rd=ref_value)
                    raise Exception('Key {k} : test_data != ref_data\n\t({diff})'.format(k=tst_key, diff=cmp_data))
            else:
                for tst_entry, ref_entry in zip(tst_value, ref_value):
                    if isinstance(tst_entry, dict):
                        compare_psy_dict(ref_entry, tst_entry)
        elif isinstance(tst_value, np.ndarray):
            if tst_value.all() != ref_value.all():
                if tst_value.size < 20:
                    cmp_data = "\n".join(['{t}\t{r}'.format(t=t, r=r) for t, r in zip(tst_value, ref_value)])
                else:
                    cmp_data = 'Size : {t}\t{r}'.format(t=tst_value.size, r=tst_value.size)
                raise Exception('Key {k} : test_data != ref_data (np.ndarray)\n\t({diff})'.format(k=tst_key,
                                                                                                  diff=cmp_data))
        elif isinstance(tst_value, (nem_rd.PsyObject, nem_rd.PsyEnum)):
            if tst_value != ref_value:
                cmp_data = "{td}\t!= {rd}".format(td=tst_value, rd=ref_value)
                raise Exception('Key {k} : test_data != ref_data \n\t({diff})'.format(k=tst_key, diff=cmp_data))
        else:
            try:
                if ref_value != tst_value:
                    cmp_data = "{td}\t!= {rd}".format(td=tst_value, rd=ref_value)
                    raise Exception('Key {k} (!= failed): test_data != ref_data \n\t({diff})'.format(k=tst_key,
                                                                                                     diff=cmp_data))
            except ValueError as e:
                print(e)
                print(type(ref_value))
    return True


def compare_gaia_dict(ref_dict_, tst_dict_):
    """Comparaison dictionnaire fichier XML Gaia.

    N'est utile que pour la clé 'filepath'.
    """
    for (tst_key, tst_value) in tst_dict_.items():
        ref_value = ref_dict_.get(tst_key, None)
        if ref_value is None:
            raise Exception('Key {k} in test_data but not in ref_data'.format(k=tst_key))
        if tst_key == 'filepath':
            # on vérifie que c'est le même fichier — le path peut différé.
            if os.path.samefile(tst_value, ref_value) is not True:
                cmp_data = "{td}\t!= {rd}".format(td=tst_value, rd=ref_value)
                raise Exception('Key {k} : file not the same\n\t({diff})'.format(k=tst_key, diff=cmp_data))
        elif isinstance(tst_value, dict):
            compare_gaia_dict(ref_value, tst_value)
        elif isinstance(tst_value, list):
            for tst_entry, ref_entry in zip(tst_value, ref_value):
                if isinstance(tst_entry, dict):
                    compare_gaia_dict(ref_entry, tst_entry)
        elif isinstance(tst_value, np.ndarray):
            if tst_value.all() != ref_value.all():
                if tst_value.size < 20:
                    cmp_data = "\n".join(['{t}\t{r}'.format(t=t, r=r) for t, r in zip(tst_value, ref_value)])
                else:
                    cmp_data = 'Size : {t}\t{r}'.format(t=tst_value.size, r=tst_value.size)
                raise Exception('Key {k} : test_data != ref_data (np.ndarray)\n\t({diff})'.format(k=tst_key,
                                                                                                  diff=cmp_data))
        else:
            try:
                if ref_value != tst_value:
                    cmp_data = "{td}\t!= {rd}".format(td=tst_value, rd=ref_value)
                    raise Exception('Key {k} (!= failed): test_data != ref_data \n\t({diff})'.format(k=tst_key,
                                                                                                     diff=cmp_data))
            except ValueError as e:
                print(e)
                print(type(ref_value))
    return True


def compare_psy_curve_data(ref_data_, tst_data_) -> bool:
    assert ref_data_.xlabel == tst_data_.xlabel
    assert ref_data_.xunit == tst_data_.xunit
    assert ref_data_.ylabel == tst_data_.ylabel
    assert ref_data_.yunit == tst_data_.yunit
    assert ref_data_.zlabel == tst_data_.zlabel
    assert ref_data_.zunit == tst_data_.zunit
    assert ref_data_.mark == tst_data_.mark
    assert ref_data_.height == tst_data_.height
    assert ref_data_.name == tst_data_.name
    assert_array_equal(ref_data_.colors, tst_data_.colors)
    assert_array_equal(ref_data_.data, tst_data_.data)
    return True


def run_test_read_psy_file_base(tst_dir_: str, tst_name_: str, filename_: str, intern: bool = False):
    """
    Comparaision des données lues dans un fichier .psy avec sa référence.

    Parameters
    ----------
    tst_dir_ : os.PathLike object
        Répertoire du test

    tst_name_ : str
        Nom du test

    filename_ : str
        nom du fichier à lire (il doit être dans <workdir>/data

    intern : bool
        Indique si les données sont internes. False par défaut.

    Returns
    -------
    bool
        True si le test est passé, false sinon

    """
    data_path = os.path.join(nem_rd.get_data_dir(), filename_)
    if intern:
        data_path = os.path.join(nem_rd.get_intern_data_dir(), filename_)
    tst_obj = nem_rd.ReadPsyFile.read_psy_file(data_path)
    ref_obj = load_pickled(tst_dir_, tst_name_, tst_obj)

    # print(f'##### {tst_name_} ({tst_dir_})')
    # print(f'\tData_Path : {data_path}')
    result = compare_psy_dict(ref_obj, tst_obj)

    assert result is True


def run_test_read_gaia_file_base(tst_dir_: str, tst_name_: str, filename_: str, intern: bool = False):
    """
    Comparaison des données lues dans un fichier XML Gaia avec sa référence.

    Parameters
    ----------
    tst_dir_ : os.PathLike object
        Répertoire du test

    tst_name_ : str
        Nom du test

    filename_ : string
        nom du fichier à lire (il doit être dans <workdir>/data[/intern]

    intern : bool
        Indique si les données sont internes. False par défaut.

    Returns
    -------
    bool
        True si le test est passé, false sinon
    """
    data_path = os.path.join(nem_rd.get_data_dir(), filename_)
    if intern:
        data_path = os.path.join(nem_rd.get_intern_data_dir(), filename_)
    tst_obj = nem_rd.ReadGaiaFile.read_gaia_file(data_path)

    ref_obj = load_pickled(tst_dir_, tst_name_, tst_obj)

    result = compare_gaia_dict(ref_obj, tst_obj)

    assert result is True


##########
#
# Code copié depuis numpy-1.15.4/numpy/core/tests/test_print.py
#
##########
def _test_redirected_print(obj, ref, fct=None):
    file_ref = StringIO()
    file_tst = StringIO()
    stdout = sys.stdout
    try:
        sys.stdout = file_tst
        if fct is None:
            print(obj)
        else:
            fct(obj)
        sys.stdout = file_ref
        print(ref)
    finally:
        sys.stdout = stdout

    assert_equal(file_tst.getvalue(), file_ref.getvalue(),
                 err_msg='print failed')

##########
#
# Code copié depuis matplotlib-3.0.2/lib/matplotlib/testing/__init__py
#
##########


def is_called_from_pytest():
    """Returns whether the call was done from pytest"""
    return getattr(mpl, '_called_from_pytest', False)


def set_font_settings_for_testing():
    mpl.rcParams['font.family'] = 'DejaVu Sans'
    mpl.rcParams['text.hinting'] = False
    mpl.rcParams['text.hinting_factor'] = 8


def set_reproducibility_for_testing():
    mpl.rcParams['svg.hashsalt'] = 'matplotlib'


def setup():
    # The baseline images are created in this locale, so we should use
    # it during all of the tests.

    try:
        locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    except locale.Error:
        try:
            locale.setlocale(locale.LC_ALL, 'English_United States.1252')
        except locale.Error:
            warnings.warn(
                "Could not set locale to English/United States. or fr_FR"
                "Some date-related tests may fail.")

    mpl.use('Agg', force=True, warn=False)  # use Agg backend for these tests

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", MatplotlibDeprecationWarning)
        mpl.rcdefaults()  # Start with all defaults

    # These settings *must* be hardcoded for running the comparison tests and
    # are not necessarily the default values as specified in rcsetup.py.
    set_font_settings_for_testing()
    set_reproducibility_for_testing()
