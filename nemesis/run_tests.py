# -*- coding: utf-8 -*-

import pytest
import os
import sys
import subprocess
import tempfile
import shutil
from typing import NoReturn

"""
En ligne de commande, depuis le répertoire `nemesis`, les tests passent::

    python3 run_tests.py
    # ou
    pytest

Possibilité de lancer les tests depuis le répertoire `tests` pour un seul module::

    pytest test_prototyping.py

Notes
-----
* La comparaison d'images fonctionne (utilisation decorator `matplotlib`).
* La comparaison de dictionnaire pour objets Psyché fonctionne (fonction dans `testing/__init__.py`)
* Les tests en doctest sont pris en compte par `pytest` (même lancé depuis `pycharm`).
* Un fichier au format `junit` est créé (+ modification xml pour ajout xslt si hors Jenkins)
* 2 fichiers de config `py.test` :
    * ``nemesis/pytest.ini``
    * ``testing/conftest.py``

Warnings
--------
Pour la comparaison de chemin vers un fichier ou répertoire, il faut utiliser `os.path.samefile(f1, f2)` car
selon la façon de lancer le test, le chemin peut différer mais pointer sur le même fichier.

"""

FS_ENCODING = sys.getfilesystemencoding()
XSL_NEEDED = False


def get_home_temp() -> str:
    """
    Retourne le répertoire temporaire.

    Returns
    -------
    str
        (os.PathLike) le chemin
    """
    # Pas besoin de remplacer cmd car cmd existe au TGCC et en cas d'échec, il y a un rep de positionner
    cmd = 'ccc_home -t'
    try:
        output = subprocess.check_output(cmd.split())
        output = output.decode(FS_ENCODING).strip()
    except FileNotFoundError:
        # pas sur réseau CEA
        output = '/tmp'
    return output


def generate_output_path() -> str:
    """
    Retourne le répertoire dans lequel le fichier XML junit va être créé.

    Returns
    -------
    str
        (os.PathLike) Chemin vers un répertoire
    """
    try:
        workspace = os.environ['WORKSPACE']
    except KeyError:
        workspace = get_home_temp()
        globals()['XSL_NEEDED'] = True
    return os.path.join(workspace, 'nemesis_tests', 'results.xml')


def add_xsl_header_in_file(xml_file: str) -> None:
    """
    Modification du xml généré pour indiquer la feuille de style.
    Copie le fichier xsl utilisé.

    Parameters
    ----------
    xml_file : str
        (os.PathLike) le fichier XML généré par py.test (au format JUnit).
    """
    xsl_file = "nemesis_junit.xsl"
    output_dir = os.path.dirname(xml_file)
    header = '<?xml-stylesheet href="{}" type="text/xsl"?>'.format(xsl_file)
    to_replace = '<?xml version="1.0" encoding="utf-8"?>'
    tmp = tempfile.NamedTemporaryFile(mode='w', delete=False, dir=output_dir)
    with open(xml_file, 'r') as f:
        for i, line in enumerate(f.readlines()):
            line = line.replace(to_replace, header, 1)
            tmp.write(line + '\n')
    tmp.close()
    try:
        os.rename(tmp.name, xml_file)
    except OSError:
        print('Le répertoire temporaire ne se trouve pas le même filesystem que le fichier originel')
        pass
    # copie explicite fichier xsl, un lien ne suffit pas
    src = os.path.join(os.path.dirname(__file__), 'tests', xsl_file)
    dst = os.path.join(output_dir, xsl_file)
    # copie explicite, un lien ne suffit pas
    shutil.copyfile(src, dst)


if __name__ == '__main__':
    output_filename = generate_output_path()
    opt_junit = '--junitxml={}'.format(output_filename)
    print(f'### Output dans {output_filename}')
    pytest.main(['-v', opt_junit, '--color=yes'])
    if XSL_NEEDED is True:
        add_xsl_header_in_file(output_filename)
