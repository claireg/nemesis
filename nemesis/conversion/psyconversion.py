# -*- coding: utf-8 -*-

import os
from typing import Union, List
import itertools

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import (
    markers,
    colors,
    ticker,
    tri,
    cm,
)
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from mpl_toolkits.mplot3d.art3d import Line3DCollection

import nemesis as nem
import nemesis.readdata as nem_rd
from nemesis.plotter import PyPlotCEA as nplt

output_formats = ['png', 'eps', 'jpg', 'pdf', 'ps', 'raw', 'svg', 'tif']


class PsyConversion(object):
    mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    mpl_markers = markers.MarkerStyle.markers.keys()

    def __init__(self, infile: str, to_format=output_formats[0], where=None, conversion=False):
        self.infile = infile
        self.outputs = []
        self.psy_dict = nem_rd.ReadPsyFile.read_psy_file(infile)
        self.analyzer = nem_rd.AnalyzePsyDict(self.psy_dict)
        self.convert = {'format': to_format, 'outdir': where}
        self.result = {}
        for page_name in self.analyzer.psy_page_name():
            graphic_name = self.analyzer.psy_page_graphic_name(page_name)
            if len(graphic_name) > 1:
                raise RuntimeError('ATTENTION, plusieurs représentations dans une même page.\n'
                                   'Seule la première est traitée')
            repr_type = self.analyzer.psy_representation_type(graphic_name[0])
            if repr_type is None:
                raise RuntimeError('ERREUR : représentation inconnue')

            self.result[page_name] = dict.fromkeys(['repr', 'data'])
            current = self.result[page_name]
            if repr_type == 'StandardCurve':
                # le type de la représentation est le nom de la méthode matplotlib.pyplot pour afficher les données
                current['repr'] = 'plot'
                # @fixme il faudrait une liste, car peut y avoir plusieurs représentations par page
                current['data'] = self.flat_curves(page_name)
            elif repr_type == 'DepthCurve':
                current['repr'] = 'plot3d'
                current['data'] = self.depth_curves(page_name)
            elif repr_type == 'RaiseMap':
                scene_type = self.analyzer.psy_scene_type(page_name)
                if scene_type == 'DEPTH':
                    current['repr'] = 'plot_surface'
                    current['data'] = self.raise_map(page_name)
                else:
                    # @FIXME @TST pas de fichier .psy pour tester ...
                    current['repr'] = 'contourf'
                    current['data'] = self.flat_map(page_name)
                    # cls.flat_map(psy_file, page_name)
            elif repr_type == 'UnstructuredRaiseMap':
                current['repr'] = 'plot_trisurf'
                current['data'] = self.unstruct_raise_map(page_name)
            elif repr_type == 'SetOfCurve':
                current['repr'] = 'plot'
                current['data'] = self.set_of_curves(page_name)
            elif repr_type == 'VectorField':
                current['repr'] = 'quiver'
                current['data'] = self.vector_fields(page_name)
            else:
                raise RuntimeWarning(f'ERREUR : type de représentation non prévue ({repr_type})')

            # obligatoirement ici car un fichier par page (une page = une figure)
            # ou alors il faut avoir une liste des figures créées
            if conversion is True:
                self.do_convert(page_name)

    def do_convert(self, page_name) -> None:
        # Un fichier par page
        # Format nécessitant une extension : pgf (xelatex)
        # Format raw : image noire
        if self.convert['format'] not in output_formats:
            choices = '\n'.join(output_formats)
            raise RuntimeError(f'ERREUR format de sortie inconnu.\n Choix parmi {choices}')
        if self.convert['outdir'] is None:
            self.convert['outdir'] = os.path.dirname(os.path.abspath(self.infile)),
        elif not os.path.exists(self.convert['outdir']):
            os.mkdir(self.convert['outdir'])
        common_in_out = os.path.basename(self.infile)
        outfile = os.path.join(self.convert['outdir'], f'{common_in_out}_{page_name}.{self.convert["format"]}')
        # print(f'\t=> Ecriture du fichier : {outfile}')
        plt.savefig(outfile, format=self.convert['format'], pad_inches=.0, bbox_inches='tight')
        self.outputs.append(outfile)

    @staticmethod
    def do_draw() -> None:
        # Affichage de toutes les pages en une seule fois
        print(f'From pytest : {nem._called_from_test}')
        if not nem._called_from_test:
            plt.show()

    def do_extract(self) -> dict:
        return self.result

    def data_collection(self, page_name: str, rep: str) -> List[np.array]:
        """
        Code commun à flat_curves, set_of_curves et vector_fields

        Parameters
        ----------
        page_name
            Nom de la page psyche.
        rep
            'curves' pour 'psyche.StandardCurve' ou 'networks' pour 'psyche.SetOfCurve')
            ou 'quiver' pour 'psyche.VectorField'
        """
        data = self.analyzer.psy_function_data(page_name)
        scale_0, scale_1, scale_2 = self.analyzer.psy_axes_scaling(page_name)
        titles = self.analyzer.psy_text(page_name)
        legend, legend_title = self.analyzer.psy_legend(page_name)
        if rep == 'curves':
            PsyPlot.curves(data, titles, scale_0, scale_1, legend, legend_title)
        elif rep == 'networks':
            PsyPlot.networks(data, titles, scale_0, scale_1, legend, legend_title)
        elif rep == 'quiver':
            scene_type = self.analyzer.psy_scene_type(page_name)
            if scene_type == 'DEPTH':
                print('Tracé de champs de vecteurs 3D non encore implémenté ...')
            else:
                PsyPlot.quiver(data, titles, scale_0, scale_1)
        elif rep == 'curves3d':
            if data[0].height is True:
                PsyPlot.curves3d(data, titles, scale_0, scale_1, scale_2, legend, legend_title)
            else:
                PsyPlot.curves(data, titles, scale_0, scale_1, legend, legend_title)
        else:
            raise RuntimeError(f'Représentation non prise en compte : {rep}')
        return [d.data for d in data]

    def flat_curves(self, page_name: str) -> List[np.array]:
        """
        Cas 'StandardCurve' : Tracé d'une courbe 2D.

        Parameters
        ----------
        page_name :
            Nom de la page psyche.
        Returns
        -------
            Liste de np.array représentant les données brutes
        """
        return self.data_collection(page_name, 'curves')

    def set_of_curves(self, page_name: str) -> List[np.array]:
        """
        Cas  'SetOfCurve' : Tracé d'une réseau de courbes.

        Parameters
        ----------
        page_name :
            Nom de la page psyche.
        """
        return self.data_collection(page_name, 'networks')

    def vector_fields(self, page_name: str) -> List[np.array]:
        """
        Cas 'VectorField' : Tracé d'un champ de vecteurs.

        Parameters
        ----------
        page_name :
            Nom de la page psyche.
        """
        return self.data_collection(page_name, 'quiver')

    def depth_curves(self, page_name: str) -> List[np.array]:
        """
        Cas 'DepthCurve' : Tracé d'une courbe en 3D.

        Parameters
        ----------
        page_name :
            Nom de la page psyche.
        """
        return self.data_collection(page_name, 'curves3d')

    def raise_map(self, page_name: str) -> List[np.array]:
        """
        cas 'RaiseMap DEPTH' : Tracé d'une nappe avec des données structurées. (fonction  colorFunction)

        Parameters
        ----------
        page_name :
            Nom de la page psyche.
        """
        # Recuperation des data
        raise_map_name = self.analyzer.psy_raisemap_name()
        colormap_name = self.analyzer.psy_colormap_name()
        x_tab, y_tab, z_tab, nb_col = self.analyzer.psy_map_data(raise_map_name, colormap_name)
        x_lbl, x_unit, y_lbl, y_unit, z_lbl, z_unit = self.analyzer.psy_axes_label(page_name)

        xscale, yscale, zscale = self.analyzer.psy_axes_scaling(page_name)

        # Recuperation des textes
        titles = self.analyzer.psy_text(page_name)

        # Recuperation de la legende graphique
        legend, legend_title = self.analyzer.psy_legend(page_name)
        legend_title = ' / '.join(legend_title)
        legend = ' / '.join(legend)
        nplt.set_titles(legend_title, legend, '')

        PsyPlot.raisemap(titles,
                         x_tab, x_lbl, x_unit, xscale,
                         y_tab, y_lbl, y_unit, yscale,
                         z_tab, z_lbl, z_unit, zscale)

        return [x_tab, y_tab, z_tab]

    def flat_map(self, page_name: str) -> List[np.array]:
        """
        cas 'RaiseMap FLAT' : Tracé d'une carte avec des données structurées. (fonction  colorFunction)

        Parameters
        ----------
        page_name :
            Nom de la page psyche.

        Returns
        -------

        """
        # @fixme à tester
        # Recuperation des data
        # ---------------------
        raise_map_name = self.analyzer.psy_raisemap_name()
        colormap_name = self.analyzer.psy_colormap_name()
        x_tab, y_tab, z_tab, nb_col = self.analyzer.psy_map_data(raise_map_name,
                                                                 colormap_name)
        # x_lbl, x_unit, y_lbl, y_unit, z_lbl, z_unit = psyU.psy_axes_label()
        axes_lbl = self.analyzer.psy_axes_label(page_name)

        xscale, yscale, zscale = self.analyzer.psy_axes_scaling(page_name)

        # Recuperation des textes
        titles = self.analyzer.psy_text(page_name)

        # Recuperation de la legende graphique
        legend, legend_title = self.analyzer.psy_legend(page_name)
        legend_title = ' / '.join(legend_title)
        legend = ' / '.join(legend)
        nplt.set_titles(legend_title, legend, '')

        PsyPlot.contour(titles,
                        x_tab, axes_lbl[0], axes_lbl[1], xscale,
                        y_tab, axes_lbl[2], axes_lbl[3], yscale,
                        z_tab, axes_lbl[4], axes_lbl[5])
        return [x_tab, y_tab, z_tab]

    def unstruct_raise_map(self, page_name: str) -> List[np.array]:
        """
        cas 'UnstructuredRaiseMap' : Tracé d'une carte avec des données non structurées.

        Parameters
        ----------
        page_name :
            Nom de la page psyché

        Returns
        -------

        """
        # Recuperation des data
        # x=[0] y=[1] z=[3] c=[4] t=[5]
        data = self.analyzer.psy_function_data(page_name)

        xscale, yscale, zscale = self.analyzer.psy_axes_scaling(page_name)

        # Recuperation des textes
        titles = self.analyzer.psy_text(page_name)

        # Recuperation de la legende graphique
        legend, legend_title = self.analyzer.psy_legend(page_name)
        legend_title = ' / '.join(legend_title)
        legend = ' / '.join(legend)
        nplt.set_titles(legend_title, legend, '')

        PsyPlot.unstructraisemap(data, titles,
                                 xscale, yscale, zscale)

        return [d.data for d in data]


class PsyPlot(object):
    @classmethod
    def curves(cls, data: List[nem_rd.PsyCurveData],
               titles: List[nem_rd.PsyStdTextData],
               xscale: nem_rd.PsyEnum, yscale: nem_rd.PsyEnum,
               legend: List[str], title_legend: str) -> None:
        """
        Tracé de courbes en 2D

        Parameters
        ----------
        data :
            Données à tracer
        titles :
            Titres du graphique
        xscale :
        yscale :
            Représentation en Lin ou Log pour chacune des directions
        legend :
            Items de la légende graphique
        title_legend :
            Titre de la légende graphique

        Returns
        -------

        """
        n_subplt = len(data)
        fig, axs = plt.subplots(1, n_subplt, figsize=(10, 6))

        for i, current in enumerate(data):
            if n_subplt == 1:
                axe = axs
            else:
                axe = axs[i]
            axe.ticklabel_format(axis='both', style='scientific', scilimits=(-1, 2))
            it_marker = itertools.cycle(PsyConversion.mpl_markers)
            for j, net in enumerate(current.data):
                x_tab, y_tab = net[0], net[1]
                if current.mark is True:
                    axe.plot(x_tab, y_tab, marker=next(it_marker))
                else:
                    axe.plot(x_tab, y_tab)

            # Traces des axes
            cls.custom_axes2d(axe, '',
                              current.xlabel, current.xunit, xscale,
                              current.ylabel, current.yunit, yscale)

            # Traces de la legende
            if len(legend) > 0:
                axe.legend(legend, title=title_legend)

            # Traces des titres
            cls.titles(fig, titles)

            axe.grid()

    @classmethod
    def networks(cls, data: List[nem_rd.PsyCurveData],
                 titles: List[nem_rd.PsyStdTextData],
                 xscale: nem_rd.PsyEnum, yscale: nem_rd.PsyEnum,
                 legend: List[str], legend_title: str) -> None:
        """
        Tracé d'un ou plusieurs réseaux de courbes 2D

        Parameters
        ----------
        data :
            Données à tracer
        titles :
            Titres du graphique
        xscale :
        yscale :
            Représentation en Lin ou Log pour chacune des directions
        legend :
            Items de la légende graphique
        legend_title :
            Titre de la légende graphique
        """
        n_subplt = len(data)
        fig, axs = plt.subplots(1, n_subplt, figsize=(10, 6))

        for i, current in enumerate(data):
            if n_subplt == 1:
                axe = axs
            else:
                axe = axs[i]
            nb_reseau = len(current.data)
            legendcolor = []
            color_seg_base = [colors.to_rgba(c) for c in PsyConversion.mpl_colors]
            idx = []
            j = 0
            for net, c in zip(current.data, itertools.cycle(color_seg_base)):
                x_tab, y_tab = net[0], net[1]
                if nb_reseau == 1:
                    for cx, cy in zip(x_tab, y_tab):
                        axe.plot(cx, cy, marker=PsyConversion.mpl_markers[0])
                else:
                    legendcolor.append(c)
                    idx.append(j)
                    plot_args = []
                    for cx, cy in zip(x_tab, y_tab):
                        plot_args.append(cx)
                        plot_args.append(cy)
                    axe.plot(*plot_args, color=c)
                    j += len(x_tab)

            # Traces de la legende
            if nb_reseau > 1:
                labels = [e.strip() for e in legend]
                nplt.networks_legend(labels, idx)
            else:
                if len(legend) > 0:
                    axe.legend(legend, title=legend_title)

            # Traces des axes
            cls.custom_axes2d(axe, '',
                              current.xlabel, current.xunit, xscale,
                              current.ylabel, current.yunit, yscale)

            # Traces des titres
            cls.titles(fig, titles)

            axe.grid()

    @classmethod
    def curves3d(cls, data: List[nem_rd.PsyCurveData],
                 titles: List[nem_rd.PsyStdTextData],
                 xscale: nem_rd.PsyEnum, yscale: nem_rd.PsyEnum, zscale: nem_rd.PsyEnum,
                 legend: List[str], title_legend: str) -> None:
        """
        Tracé de courbes en 3D

        Parameters
        ----------
        data :
            Données à tracer
        titles :
            Titres du graphique
        xscale :
            Type de l'échelle axe des x
        yscale :
            Type de l'échelle axe des y
        zscale :
           Type de l'échelle axe des z
        legend :
            Items de la légende graphique
        title_legend :
            Titre de la légende graphique
        """
        # Traces des axes
        fig = plt.figure(figsize=(10, 6))

        max_colors = len(plt.rcParams['axes.prop_cycle'])
        it_marker = itertools.cycle(PsyConversion.mpl_markers)

        for ij, current in enumerate(data):
            axe = fig.add_subplot(1, ij + 1, ij + 1, projection='3d')
            # Nombre de courbes indépendantes vs nombre de points (ici nombre d'abscisses) dans chaque courbe
            # print(f'DepthCurves :\n\t* Taille globale : {len(current.data)}\n'
            #       f'\t* Taille interne : {len(current.data[0][0])}\n'
            #       f'\t* Nombre de couleurs : {len(current.colors)}')
            # print(f'Taille current.colors : {len(current.colors)}')
            if len(current.data[0][0]) == 2:
                # Uniquement pour les courbes qui n'ont que 2 points
                # @FIXME à généraliser (attention au test du if)
                # Construction des segments - cas des hérissons
                # current.data[ii] : contient 3 tableaux :
                #   * 0 : X
                #   * 1 : Y
                #   * 2 : Z
                # Pour chacun des tableaux, n (ici 2) points tq
                #   * x0 x1 ... xn
                segments = []
                segments_color = []
                x_min, y_min, z_min = np.inf, np.inf, np.inf
                x_max, y_max, z_max = -np.inf, -np.inf, -np.inf
                # Cas si 2 points
                last_color = 'C0'
                for ii in range(len(current.data)):
                    points = current.data[ii]
                    x0, y0, z0 = points[0][0], points[1][0], points[2][0]
                    x1, y1, z1 = points[0][1], points[1][1], points[2][1]
                    xmi = min(x0, x1)
                    if x_min > xmi:
                        x_min = xmi
                    xma = max(x0, x1)
                    if x_max < xma:
                        x_max = xma
                    ymi = min(y0, y1)
                    if y_min > ymi:
                        y_min = ymi
                    yma = max(y0, y1)
                    if y_max < yma:
                        y_max = yma
                    zmi = min(z0, z1)
                    if z_min > zmi:
                        z_min = zmi
                    zma = max(z0, z1)
                    if z_max < zma:
                        z_max = zma
                    segments.append([(x0, y0, z0), (x1, y1, z1)])
                    try:
                        num_color = (current.colors[ii] - 1) % max_colors
                        last_color = f'C{num_color}'
                        segments_color.append(last_color)
                    except IndexError as e:
                        # Fichier vérolé (lecture ou écriture ? coquilles3_herissons et coquilles2_herissons
                        segments_color.append(last_color)
                line_seg = Line3DCollection(segments)
                line_seg.set_color(segments_color)
                axe.add_collection3d(line_seg)
                axe.set_xlim(x_min, x_max)
                axe.set_ylim(y_min, y_max)
                axe.set_zlim(z_min, z_max)
            else:
                for coords, c_tab in zip(current.data, current.colors):
                    x_tab, y_tab, z_tab = coords[0], coords[1], coords[2]
                    marker = '' if data[0].mark is not True else next(it_marker)
                    num_color = (c_tab - 1) % max_colors
                    axe.plot(x_tab, y_tab, z_tab, f'C{num_color}{marker}')

            cls.custom_axes3d(axe,
                              current.xlabel, current.xunit, xscale,
                              current.ylabel, current.yunit, yscale,
                              current.zlabel, current.zunit, zscale)

            if len(legend) > 0:
                axe.legend(legend, title=title_legend)

            cls.titles(fig, titles)

    @classmethod
    def quiver(cls, data: List[nem_rd.PsyCurveData],
               titles: List[nem_rd.PsyStdTextData],
               xscale: nem_rd.PsyEnum, yscale: nem_rd.PsyEnum) -> None:
        """
        Tracé d'un champ de vecteurs 2D

        Parameters
        ----------
        data :
            Données à tracer
        titles :
            Titres du graphique
        xscale :
            Représentation en Lin ou Log pour les abscisses
        yscale :
            Représentation en Lin ou Log pour les ordonnées
        """
        n_subplt = len(data)
        fig, axs = plt.subplots(1, n_subplt, figsize=(10, 6))

        for i, current in enumerate(data):
            if n_subplt == 1:
                axe = axs
            else:
                axe = axs[i]
            # Peut-il y avoir plusieurs vectorfields dans une même scène psyché ?
            # Structuré de stockage d'un vectorfield :
            # * 1 champ de vecteurs est stockés dans 1 PsyCurveData (pourquoi ?)
            # * 6 éléments : x, y, u, v, c (couleurs)
            # Infos :
            #   * len(current.data) == 1
            #   * len(current.data[0]) == 6   type(current.data[0]) == tuple
            if len(current.data) != 1:
                raise RuntimeWarning('ATTENTION plusieurs champs de vecteurs, seul le premier est pris en compte')
            x_tab = current.data[0][0]
            y_tab = current.data[0][1]
            u_tab = current.data[0][2]
            v_tab = current.data[0][3]

            lb = nem_rd.get_axis_label(current.zlabel[0], current.zunit[0])
            # On veut la même chose que dans Psyché alors on désactive les titres par défaut
            nquiver_args = {'default_title': False, 'draw_quiverkey': False, 'label': lb}
            nplt.nquiver(x_tab, y_tab, u_tab, v_tab, **nquiver_args)

            cls.custom_axes2d(axe, '',
                              current.xlabel, current.xunit, xscale,
                              current.ylabel, current.yunit, yscale)

            cls.titles(fig, titles)
            axe.grid()

    @classmethod
    def raisemap(cls, titles: List[nem_rd.PsyStdTextData],
                 x_tab: np.array, x_lbl: str, x_unit: str, xscale: nem_rd.PsyEnum,
                 y_tab: np.array, y_lbl: str, y_unit: str, yscale: nem_rd.PsyEnum,
                 z_tab: np.array, z_lbl: str, z_unit: str, zscale: nem_rd.PsyEnum) -> None:
        """
        Tracer d'une nappe de couleur en structuré

        Parameters
        ----------
        titles :
            Titres du graphique
        x_tab :
        x_lbl :
        x_unit :
            Données, Label et Unité de l'axe X
        xscale :
            Représentation en Lin ou Log pour la direction X
        y_tab :
        y_lbl :
        y_unit :
            Données, Label et Unité de l'axe y
        yscale :
            Représentation en Lin ou Log pour la direction Y
        z_tab :
        z_lbl :
        z_unit :
            Données, Label et Unité de l'axe Z
        zscale :
            Représentation en Lin ou Log pour la direction Z
        """
        axe = plt.gcf().add_subplot(1, 1, 1, projection='3d')

        axe.plot_surface(x_tab, y_tab, z_tab, cmap=cm.get_cmap())
        cls.custom_axes3d(axe,
                          x_lbl, x_unit, xscale,
                          y_lbl, y_unit, yscale,
                          z_lbl, z_unit, zscale)
        cls.titles(plt.gcf(), titles)

    @classmethod
    def unstructraisemap(cls, data: List[nem_rd.PsyCurveData],
                         titles: List[nem_rd.PsyStdTextData],
                         xscale: nem_rd.PsyEnum, yscale: nem_rd.PsyEnum, zscale: nem_rd.PsyEnum) -> None:
        """
        Tracer d'une nappe de couleur en non structuré

        Parameters
        ----------
        data :
            Données associées aux 3 directions
        titles :
            Les titre du graphique
        xscale :
        yscale :
        zscale :
            Représentation en Lin ou Log pour chacunne des directions
        """
        # @fixme à tester + seule la première raisemap est pris en compte, faire des subplot
        #   Quand on aura des données
        # Traces des axes
        # ---------------
        fig = plt.figure(figsize=(10, 6))
        axe = fig.gca(projection='3d')

        cls.custom_axes3d(axe,
                          data[0].xlabel, data[0].xunit, xscale,
                          data[0].ylabel, data[0].yunit, yscale,
                          data[0].zlabel, data[0].zunit, zscale)

        # Trace des surfaces
        # ------------------
        # nb_surf = len(data[0].data)
        for i in range(1):
            x_tab = data[0].data[i][0]
            y_tab = data[0].data[i][1]
            z_tab = data[0].data[i][2]
            c_tab = data[0].data[i][3]
            c_min, c_max = np.min(c_tab), np.max(c_tab)

            surf_triangles = tri.Triangulation(x_tab, y_tab)
            plot_kwargs = {'triangles': surf_triangles.triangles}
            if c_min != c_max:
                plot_kwargs['cmap'] = cm.get_cmap()

            axe.plot_trisurf(x_tab, y_tab, z_tab, **plot_kwargs)

        # Traces des titres
        cls.titles(fig, titles)

    @classmethod
    def contour(cls, titles: List[nem_rd.PsyStdTextData],
                x_tab: np.array, x_lbl: str, x_unit: str, x_scale: nem_rd.PsyEnum,
                y_tab: np.array, y_lbl: str, y_unit: str, y_scale: nem_rd.PsyEnum,
                z_tab: np.array, z_lbl: str, z_unit: str
                ) -> None:
        """
        Tracé en 2D d'une nappe (Carte)

        Parameters
        ----------
        titles :
            Titres du graphique
        x_tab :
        x_lbl :
        x_unit :
            Données, Label et Unité de l'axe X
        x_scale :
            Représentation en Lin ou Log pour la direction X
        y_tab :
        y_lbl :
        y_unit :
            Données, Label et Unité de l'axe y
        y_scale :
            Représentation en Lin ou Log pour la direction Y
        z_tab :
        z_lbl :
        z_unit :
            Données, Label et Unité de l'axe Z

        Returns
        -------

        """
        # @fixme à tester
        cs1 = plt.contourf(x_tab, y_tab, z_tab, locator=ticker.LogLocator(), cmap=mpl.cm.get_cmap())
        cls.custom_axes2d(plt.gca(), '',
                          x_lbl, x_unit, x_scale,
                          y_lbl, y_unit, y_scale)
        fig = plt.gcf()
        titre = z_lbl
        if len(z_unit) > 0:
            titre = '{} ({})'.format(z_lbl, z_unit)
        fig.colorbar(cs1, label=titre)

        # Traces des titres
        # -----------------
        cls.titles(fig, titles)

        plt.gca().grid()

    @classmethod
    def custom_axes2d(cls, axes: plt.Axes, titre: str,
                      xlabel: str, xunit: str, xscale: nem_rd.PsyEnum,
                      ylabel: str, yunit: str, yscale: nem_rd.PsyEnum) -> None:
        """
        Tracer d'un axe 2D

        Parameters
        ----------
        axes :
            Axe a afficher
        titre :
            Titre général
        xlabel :
            Label de l'axe X
        xunit :
            Unité de l'axe X
        xscale :
            Axe X Lin ou Log
        ylabel :
            Label de l'axe Y
        yunit :
            Unité de l'axe Y
        yscale :
            Axe Y Lin ou Log

        Returns
        -------

        """
        xlbl = nem_rd.get_axis_label(xlabel, xunit)
        ylbl = nem_rd.get_axis_label(ylabel, yunit)
        mpl_xscale = nem_rd.get_axis_scale(xscale)
        mpl_yscale = nem_rd.get_axis_scale(yscale)
        axes.set(xlabel=xlbl, ylabel=ylbl, title=titre, xscale=mpl_xscale, yscale=mpl_yscale)

    @classmethod
    def custom_axes3d(cls,
                      axes: Axes3D,
                      xlabel: str, xunit: str, xscale: nem_rd.PsyEnum,
                      ylabel: str, yunit: str, yscale: nem_rd.PsyEnum,
                      zlabel: str, zunit: str, zscale: nem_rd.PsyEnum) -> None:
        """
        Tracer d'un axe 3D

        Parameters
        ----------
        axes : pyplot.axes
            Axe a afficher
        xlabel :
            Label de l'axe X
        ylabel :
            Label de l'axe Y
        zlabel :
            Label de l'axe Z
        xunit :
            Unité de l'axe X
        yunit :
            Unité de l'axe Y
        zunit :
            Unité de l'axe Z
        xscale :
            Échelle de l'axe X ('LIN' ou 'LOG')
        yscale :
            Échelle de l'axe Y ('LIN' ou 'LOG')
        zscale :
            Échelle de l'axe Z ('LIN' ou 'LOG')
        """
        xlbl = nem_rd.get_axis_label(xlabel, xunit)
        ylbl = nem_rd.get_axis_label(ylabel, yunit)
        zlbl = nem_rd.get_axis_label(zlabel, zunit)
        mpl_xscale = nem_rd.get_axis_scale(xscale)
        mpl_yscale = nem_rd.get_axis_scale(yscale)
        mpl_zscale = nem_rd.get_axis_scale(zscale)

        axes.set(xlabel=xlbl, ylabel=ylbl, zlabel=zlbl, xscale=mpl_xscale, yscale=mpl_yscale, zscale=mpl_zscale)

    @classmethod
    def titles(cls, figure: plt.Figure, titles: List[nem_rd.PsyStdTextData]) -> None:
        """
        Affichage des titres du graphique tels qu'ils sont dans le fichier d'origine

        Parameters
        ----------
        figure :
            La figure MatPlotLib à mettre à jour
        titles :
            Liste des labels à afficher
        """
        nb_titles = len(titles)
        if nb_titles > 1:
            figure.suptitle(titles[0].title, x=titles[0].posx, y=titles[0].posy)
            for i in range(nb_titles - 1):
                figure.text(titles[i + 1].posx, titles[i + 1].posy, titles[i + 1].title,
                            fontsize='large', horizontalalignment=titles[i + 1].alignment)
        else:
            for i in range(nb_titles):
                figure.text(titles[i].posx, titles[i].posy, titles[i].title,
                            fontsize='large', horizontalalignment=titles[i].alignment)


def psy_draw(psyfile: str) -> None:
    obj = PsyConversion(psyfile)
    obj.do_draw()


def psy_extract(psyfile: str) -> dict:
    obj = PsyConversion(psyfile)
    return obj.do_extract()


def psy_convert(psyfile: str, where: str = None, to: str = None) -> List[str]:
    obj = PsyConversion(psyfile, to_format=to, where=where, conversion=True)
    return obj.outputs


def psy_compose(psyfile: str, draw: bool = True, extract: bool = True, conversion: bool = False,
                where: str = None, to: str = None) -> Union[dict, None, List[str]]:
    conv_args = {}
    if conversion is True:
        conv_args['conversion'] = True
        conv_args['where'] = where
        conv_args['to_format'] = to
    obj = PsyConversion(psyfile, **conv_args)
    if draw:
        obj.do_draw()
    if extract:
        return obj.do_extract()
    if conversion:
        return obj.outputs