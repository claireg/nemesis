# -*- coding: utf-8 -*-

"""
Conversion format de fichier maison vers `matplotlib`

Les fichiers lus sont
* au format Psyché.
* au format Gaia XML.

Notes
-----
L'import de ce package doit être comme suit::

    import nemesis.conversion as nem_cv
"""

from .psyconversion import (
    psy_draw,
    psy_extract,
    psy_compose,
    psy_convert,
    output_formats,
)