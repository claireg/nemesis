# -*- coding: utf-8 -*-

"""
Module pour comparer différents tracés de matplotlib par rapport à une image.

Sert à prototyper les fonctionnalités à mettre dans plotter (en comparant le résultat obtenu avec une image de
référence).
"""
import os

import matplotlib.pyplot as plt
from matplotlib.image import AxesImage, imread
import matplotlib.axes as maxes
import matplotlib.contour as mcontour
import matplotlib.collections as mcollections
import matplotlib.figure as mfigure

from matplotlib.ticker import (
    MaxNLocator,
    FixedLocator
)
import matplotlib.colors as colors

import nemesis.readdata as nem_rd


# noinspection PyShadowingNames,PyShadowingNames,PyShadowingNames,PyShadowingNames,
# noinspection PyShadowingNames,PyShadowingNames,PyShadowingNames
class PrototypePyplot(object):
    """
    Une classe servant de définition de namespace pour plus de lisiblité dans le code.
    """
    @classmethod
    def scale_log_axis(cls, ax_: maxes.Axes, title_: str = None) -> None:
        ax_.set_xscale("log", nonposx='clip')
        ax_.set_yscale("log", nonposy='clip')
        ax_.grid(c='k', ls='-', alpha=0.3)
        if title_ is not None:
            ax_.set_title(title_)

    @classmethod
    def scale_axis(cls, ax_: maxes.Axes, title_: str = None) -> None:
        ax_.set_xscale("linear", nonposx='clip')
        ax_.set_yscale("linear", nonposy='clip')
        ax_.grid(c='k', ls='-', alpha=0.3)
        if title_ is not None:
            ax_.set_title(title_)

    @classmethod
    def scale_ylog_axis(cls, ax_: maxes.Axes, title_: str = None) -> None:
        ax_.set_xscale("linear", nonposx='clip')
        ax_.set_yscale("log", nonposy='clip')
        ax_.grid(c='k', ls='-', alpha=0.3)
        if title_ is not None:
            ax_.set_title(title_)

    @classmethod
    def log_contour(cls, ax_: maxes.Axes, x_, y_, z_, levels_) -> mcontour.ContourSet:
        cls.scale_log_axis(ax_)
        out = ax_.contour(x_, y_, z_, levels_, colors='black', linewidths=0.5)
        return out

    @classmethod
    def log_pcolormesh(cls, ax_: maxes.Axes, x_, y_, z_, **kwargs) -> mcollections.QuadMesh:
        cls.scale_log_axis(ax_, 'pcolormesh')
        out = ax_.pcolormesh(x_, y_, z_, **kwargs)
        return out

    @classmethod
    def log_contourf(cls, ax_: maxes.Axes, x_, y_, z_, levels, **kwargs) -> mcontour.ContourSet:
        cls.scale_log_axis(ax_, 'contourf')
        out = ax_.contourf(x_, y_, z_, levels, **kwargs)
        return out

    @classmethod
    def log_pcolor(cls, ax_: maxes.Axes, x_, y_, z_, **kwargs) -> mcollections.Collection:
        cls.scale_log_axis(ax_, 'pcolor')
        out = ax_.pcolor(x_, y_, z_, **kwargs)
        return out

    @classmethod
    def log_imshow(cls, ax_: maxes.Axes, x_, y_, z_, **kwargs) -> AxesImage:
        cls.scale_log_axis(ax_, 'imshow')
        out = ax_.imshow(z_, extent=[x_.min(), x_.max(), y_.min(), y_.max()], **kwargs)
        return out

    @classmethod
    def compare_map_plot(cls, x_, y_, z_, n_colors_: int = None) -> mfigure.Figure:
        cmap = plt.get_cmap('viridis')
        # levels = MaxNLocator(nbins=n_colors).tick_values(z.min(), z.max())
        # levels = MaxNLocator(nbins=n_colors_ + 1).tick_values(z_.min(), z_.max())
        # levels = FixedLocator([0., 2.2, 6.3, 10.3, 14.4, 18.5, 22.5, 26.6, 28.]).tick_values(z_.min(), z_.max())
        levels = MaxNLocator().tick_values(z_.min(), z_.max())
        norm = colors.BoundaryNorm(levels, ncolors=cmap.N)

        fig, axs = plt.subplots(3, 2, figsize=(20, 12))

        ax_pcm = axs[0, 0]
        out_pcm = cls.log_pcolormesh(ax_pcm, x_, y_, z_, cmap=cmap, norm=norm)
        cls.log_contour(ax_pcm, x_, y_, z_, levels)
        fig.colorbar(out_pcm, ax=ax_pcm)

        ax_cf = axs[0, 1]
        out_cf = cls.log_contourf(ax_cf, x_, y_, z_, levels)
        cls.log_contour(ax_cf, x_, y_, z_, levels)
        fig.colorbar(out_cf, ax=ax_cf)

        ax_pc = axs[1, 0]
        out_pc = cls.log_pcolor(ax_pc, x_, y_, z_, cmap=cmap, norm=norm)
        cls.log_contour(ax_pc, x_, y_, z_, levels)
        fig.colorbar(out_pc, ax=ax_pc)

        ax_im = axs[1, 1]
        out_im = cls.log_imshow(ax_im, x_, y_, z_, cmap=cmap, norm=norm)
        cls.log_contour(ax_im, x_, y_, z_, levels)
        fig.colorbar(out_im, ax=ax_im)

        # @INTERN : Only with intern data
        # ax_im = plt.subplot2grid((3, 2), (2, 0), colspan=2)
        # img_name = os.path.join(nem_rd.get_intern_data_dir(), 'carte_linux_inside.png')
        # img = imread(img_name)
        # ax_im.imshow(img, aspect='equal')
        # ax_im.set_title('reference')

        return fig

    @classmethod
    def compare_psy_mpl(cls, x_, y_, z_, n_colors_: int = None) -> mfigure.Figure:
        # cmap = plt.get_cmap('viridis')
        # levels = MaxNLocator(nbins=n_colors).tick_values(z.min(), z.max())
        levels = FixedLocator([0., 2.2, 6.3, 10.3, 14.4, 18.5, 22.5, 26.6, 28.]).tick_values(z_.min(), z_.max())
        # norm = colors.BoundaryNorm(levels, ncolors=cmap.N)

        fig, axs = plt.subplots(2, 1, figsize=(20, 12))

        ax_cf = axs[0]
        out_cf = cls.log_contourf(ax_cf, x_, y_, z_, levels)
        cls.log_contour(ax_cf, x_, y_, z_, levels)
        fig.colorbar(out_cf, ax=ax_cf)

        # @INTERN : Only with intern data
        # ax_im = axs[1]
        # img_name = os.path.join(nem_rd.get_intern_data_dir(), 'carte_linux_inside.png')
        # img = imread(img_name)
        # ax_im.imshow(img, aspect='equal')

        return fig

    @classmethod
    def compare_psy_network(cls, x_, ly_) -> mfigure.Figure:
        fig, axs = plt.subplots(1, 2, figsize=(20, 12))

        ax_cf = axs[0]
        cls.scale_log_axis(ax_cf, 'lpm_rosseland_rx')
        for y in ly_:
            ax_cf.plot(x_, y)
        ax_cf.set_xlim(x_.min(), x_.max())

        # @INTERN : Only with intern data
        # ax_im = axs[1]
        # img_name = os.path.join(nem_rd.get_intern_data_dir(), 'lpm_rosseland_rx.png')
        # img = imread(img_name)
        # ax_im.imshow(img, aspect='equal')

        return fig

    @classmethod
    def compare_gaia_curve_or_network(cls, ly_) -> mfigure.Figure:
        fig, axs = plt.subplots(1, 2, figsize=(20, 12))

        ax_cf = axs[0]
        cls.scale_axis(ax_cf, 'curve')
        # Pour avoir f(MasseVolumique) = Toutes les grandeurs sauf Pression
        # x = ly_[1]
        # for i, y in enumerate(ly_):
        #     if i != 1 and i != 7:
        #         ax_cf.plot(x, y)
        # ax_cf.set_xlim(x.min(), x.max())
        # Pour avoir f(MasseVolumique) = Pression
        x = ly_[1]
        y = ly_[7]
        ax_cf.plot(x, y)
        ax_cf.set_xlim(x.min(), x.max())
        ax_cf.set_ylim(y.min(), y.max())

        ax_im = axs[1]
        img_name = os.path.join(nem_rd.get_data_dir(), 'curve.png')
        img = imread(img_name)
        ax_im.imshow(img, aspect='equal')

        return fig

    @classmethod
    def compare_gaia_stairs_curve(cls, x_, y_) -> mfigure.Figure:
        fig, axs = plt.subplots(1, 2, figsize=(20, 12))

        ax_cf = axs[0]
        cls.scale_log_axis(ax_cf, 'section efficace')
        ax_cf.plot(x_, y_, 'o-')
        # pour les sections efficaces
        ax_cf.set_xlim(0.1, 100)
        ax_cf.set_ylim(0.1, 10)

        ax_im = axs[1]
        img_name = os.path.join(nem_rd.get_data_dir(), 'courbe_en_escalier.png')
        img = imread(img_name)
        ax_im.imshow(img, aspect='equal')

        return fig

    @classmethod
    def compare_gaia_section_efficace(cls, x_, y_) -> mfigure.Figure:
        fig, axs = plt.subplots(1, 2, figsize=(20, 12))

        ax_cf = axs[0]
        cls.scale_log_axis(ax_cf, 'section efficace')
        ax_cf.plot(x_, y_)
        # pour les sections efficaces
        ax_cf.set_xlim(0.1, 100)
        ax_cf.set_ylim(0.1, 10)

        ax_im = axs[1]
        img_name = os.path.join(nem_rd.get_data_dir(), 'courbe_en_escalier.png')
        img = imread(img_name)
        ax_im.imshow(img, aspect='equal')

        return fig

    @classmethod
    def gaia_network_2(cls, x_, ly_) -> mfigure.Figure:
        args: list = []
        for y in ly_:
            args.append(x_)
            args.append(y)

        fig, axs = plt.subplots(1, 1, figsize=(20, 12))
        axs.loglog(*args)
        axs.set_xlim(x_.min(), x_.max())

        return fig


if __name__ == '__main__':
    # x, y, z, n_colors = nem_rd.psy_map_2d_data()
    # fig = PrototypePyplot.compare_map_plot(x, y, z, n_colors)
    # # fig = PrototypePyplot.compare_psy_mpl(x, y, z, n_colors)

    # x, y = nem_rd.curves_rx_data()
    # fig = PrototypePyplot.compare_psy_network(x, y)

    x, y = nem_rd.gaia_section_efficace()
    fig = PrototypePyplot.compare_gaia_section_efficace(x, y)

    # y = nem_rd.gaia_curve_data()
    # fig = PrototypePyplot.compare_gaia_curve_or_network(y)

    # fig.tight_layout()

    # plt.savefig('/cea/S/home/s9/guilbaud/Devel/Work/WNemesis/nemesis/tests/baseline_images/test_curves_rx_plot.png')
    # plt.show()

    # x1, x2, ly = nem_rd.gaia_network_2()
    # fig = PrototypePyplot.gaia_network_2(x2, ly)
    fig.tight_layout()
    plt.show()
