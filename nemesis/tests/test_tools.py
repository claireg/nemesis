# -*- coding: utf-8 -*-
from typing import Dict, Tuple, NewType, Union

import nemesis.tools


KwargsAliases = NewType('KwargsAliases', Dict[str, Union[Tuple[str], Tuple[str, str]]])


def get_dict() -> Tuple[KwargsAliases, KwargsAliases]:
    d1 = {'cmap': ('cm',), 'colors': ('c',), 'draw_colorbar': ('dcb', 'dc'), 'label': ('lb',), 'levels': ('l',),
          'linewidths': ('lw',), 'masked_value': ('mv',), 'n_levels': ('nlevels', 'nl'),
          'nx': ('n_x',), 'ny': ('n_y',)}
    d2 = {'cax': ('current_axes', 'ax'), 'cmap': ('cm',), 'colors': ('c',), 'draw_colorbar': ('dcb', 'dc'),
          'label': ('lb',), 'levels': ('l',), 'linewidths': ('lw',), 'masked_value': ('mv',),
          'n_levels': ('nlevels', 'nl')}
    return d1, d2


def test_compare_dict() -> None:
    ref_only_1 = {'nx', 'ny'}
    ref_only_2 = {'cax'}
    ref_common = {'cmap', 'colors', 'draw_colorbar', 'label', 'levels', 'linewidths', 'masked_value', 'n_levels'}
    d1, d2 = get_dict()
    only_1, only_2, common = nemesis.tools.compare_dict(d1, d2)
    assert only_1 == ref_only_1
    assert only_2 == ref_only_2
    assert common == ref_common
