# -*- coding: utf-8 -*-

"""
Tests pour le module ``pyplotcea`` des textes (labels des axes, titre échelle de couleurs, ...)

"""
import pytest
import numpy as np
from numpy.testing import (
    assert_array_almost_equal
)
from matplotlib import pyplot as plt
import matplotlib.ticker as mticker
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd

waves_x, waves_y, waves_z = nem_rd.waves(15, 15)
waves_x_lin = np.array([10., 15., 20., 25., 30., 35., 40., 45., ])
waves_y_lin = np.array([10., 15., 20., 25., 30., ])
waves_z_lin = np.array([-3000., -2000., -1000., 0., 1000., 2000., 3000., ])
waves_x_log = np.array([1., 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, ])
waves_y_log = np.array([1., 1.1, 1.2, 1.3, 1.4, 1.5, ])
waves_z_log = np.array([-12., -10., -8., -6., -4., -2., 0., 2., 4., ])

waves_raised_testdata = [
    ('lin_lin_lin', [(waves_x_lin, waves_y_lin, waves_z_lin),
                     (mticker.ScalarFormatter, mticker.ScalarFormatter, mticker.ScalarFormatter)]),
    ('log_lin_lin', [(waves_x_log, waves_y_lin, waves_z_lin),
                     (nem_plt.PseudoLogMathTextSciFormatter, mticker.ScalarFormatter, mticker.ScalarFormatter)]),
    ('log_log_lin', [(waves_x_log, waves_y_log, waves_z_lin),
                     (nem_plt.PseudoLogMathTextSciFormatter,
                      nem_plt.PseudoLogMathTextSciFormatter,
                      mticker.ScalarFormatter)]),
    ('log_log_log', [(waves_x_log, waves_y_log, waves_z_log),
                     (nem_plt.PseudoLogMathTextSciFormatter,
                      nem_plt.PseudoLogMathTextSciFormatter,
                      nem_plt.PseudoLogMathTextSciFormatter)]),
]


@pytest.mark.parametrize("scales,expected", waves_raised_testdata)
def test_raised_contourf_text(scales, expected):
    xscale, yscale, zscale = scales.split('_')
    if zscale == 'log':
        add_kwargs = {'masked_value': 1.e-300}
    else:
        add_kwargs = {}
    nem_plt.PyPlotCEA.raised_contourf(waves_x, waves_y, waves_z,
                                      xscale=xscale, yscale=yscale, zscale=zscale,
                                      cmap='current', **add_kwargs)
    cax = plt.gca()
    x_ticks = cax.get_xticks()
    y_ticks = cax.get_yticks()
    z_ticks = cax.get_zticks()
    ref_x_ticks = expected[0][0]
    ref_y_ticks = expected[0][1]
    ref_z_ticks = expected[0][2]
    assert_array_almost_equal(x_ticks, ref_x_ticks)
    assert_array_almost_equal(y_ticks, ref_y_ticks)
    assert_array_almost_equal(z_ticks, ref_z_ticks)
    x_formatter = expected[1][0]
    y_formatter = expected[1][1]
    z_formatter = expected[1][2]
    assert isinstance(cax.xaxis.get_major_formatter(), x_formatter) is True
    assert isinstance(cax.yaxis.get_major_formatter(), y_formatter) is True
    assert isinstance(cax.zaxis.get_major_formatter(), z_formatter) is True


def test_zlabel():
    nem_plt.PyPlotCEA.raised_contourf(waves_x, waves_y, waves_z, cmap='current')
    label_ref = 'Cotes'
    nem_plt.PyPlotCEA.zlabel(label_ref)
    label = nem_plt.PyPlotCEA.zlabel()
    assert label == label_ref


def test_pseudologformatter():
    value = 0.001
    ref_math = '$10^{0}$'
    ref_data = '1.01x10^1'
    # en mpl 3.0.2 : '1.01times10^1'. Modification de matplotlib.cbook.strip_math en mpl 3.1.1
    ref_short = '1.00231     '
    pseudo = nem_plt.PseudoLogMathTextSciFormatter()
    assert pseudo(value) == ref_math
    assert pseudo.format_data(value) == ref_data
    assert pseudo.format_data_short(value) == ref_short
