# -*- coding: utf-8 -*-

import pytest
import os
import pickle
import numpy as np
from numpy.testing import (
    assert_array_equal
)
import nemesis.testing as nem_tst
import nemesis.readdata as nem_rd

TEST_DIR = nem_tst.get_objects_test_dir(__file__)


def test_path_anonymizer_0() -> None:
    filepath = '/home/to/somewhere/scratch.txt'
    ref = os.path.join(nem_rd.anonym_path, 'scratch.txt')
    result = nem_rd.path_anonymizer(filepath)
    assert (result == ref)


def test_path_anonymizer_1() -> None:
    filepath = '/home/to/some/data/intern/scratch.txt'
    ref = os.path.join(nem_rd.anonym_path, 'data', 'intern', 'scratch.txt')
    result = nem_rd.path_anonymizer(filepath)
    assert (result == ref)


def test_read_psy_file_onde() -> None:
    tst_name = 'read_psy_file_onde'
    psy_name = 'onde.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_read_psy_file_courbe3d() -> None:
    tst_name = 'read_psy_file_courbe3d'
    psy_name = 'courbe3d.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_read_psy_file_plusieurs_courbes() -> None:
    tst_name = 'read_psy_file_plusieurs_courbes'
    psy_name = 'plusieurs_courbes.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_read_psy_file_plusieurs_reseaux() -> None:
    tst_name = 'read_psy_file_plusieurs_reseaux'
    psy_name = 'plusieurs_reseaux.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_read_psy_file_vecteurs() -> None:
    tst_name = 'read_psy_file_vecteurs'
    psy_name = 'vecteurs.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_read_psy_file_herisson() -> None:
    tst_name = 'read_psy_file_herisson'
    psy_name = 'herisson.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_read_gaia_file_curve() -> None:
    tst_name = 'read_gaia_file_curve'
    gaia_name = 'curve.xml'
    nem_tst.run_test_read_gaia_file_base(TEST_DIR, tst_name, gaia_name)


def test_read_gaia_file_courbe_en_escalier() -> None:
    tst_name = 'read_gaia_file_courbe_en_escalier'
    gaia_name = 'courbe_en_escalier.xml'
    nem_tst.run_test_read_gaia_file_base(TEST_DIR, tst_name, gaia_name)


def test_gaia_points_data() -> None:
    test_name = nem_tst.create_name_for_npz(TEST_DIR, 'test_gaia_points_data')
    [y_ref, ] = nem_rd.gaia_points_data(nem_rd.get_data_dir(), 'curve.xml')

    # création si les données de référence du test n'existe pas -> à commenter ensuite
    # np.savez_compressed(test_name, y=y_ref)

    npz_file = np.load(test_name)
    y = npz_file['y']
    assert_array_equal(y_ref, y)


def test_gaia_one_mesh1d_data() -> None:
    test_name = nem_tst.create_name_for_npz(TEST_DIR, 'test_gaia_one_mesh1d_data')
    x_ref, y_ref = nem_rd.gaia_one_mesh1d_data(nem_rd.get_data_dir(), 'courbe_en_escalier.xml')

    # création si les données de référence du test n'existe pas -> à commenter ensuite
    # np.savez_compressed(test_name, x=x_ref, y=y_ref)

    npz_file = np.load(test_name)
    x = npz_file['x']
    y = npz_file['y']

    assert_array_equal(x_ref, x)
    assert_array_equal(y_ref, y)


def test_psy_hedgehog() -> None:
    test_name = nem_tst.create_name_for_npz(TEST_DIR, 'test_psy_hedgehog')
    psy_hh = nem_rd.psy_hedgehog()
    hh_ref = [h.data for h in psy_hh]

    # création si les données de référence du test n'existe pas -> à commenter ensuite
    # np.savez_compressed(test_name, hedgehog=hh_ref)

    npz_file = np.load(test_name)
    hh = npz_file['hedgehog']

    for h_ref, h in zip(hh_ref, hh):
        assert_array_equal(h_ref, h)


def quiver_streamplot_data(bname) -> (np.array, np.array, np.array, np.array):
    data_path = os.path.join(nem_rd.get_data_dir(), bname, f'data_{bname}.pickle')
    data = pickle.load(open(data_path, 'rb'))
    return data['x'], data['y'], data['u'], data['v']


def test_pickle_data_1() -> None:
    ref_shapes = (32, 32)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_1')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_2() -> None:
    ref_shapes = (90, 90)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_2')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_3() -> None:
    ref_shapes = (17, 32)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_3')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_4() -> None:
    ref_shapes = (300, )
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_4')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_5() -> None:
    ref_shapes = (17, 32)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_5')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_6() -> None:
    ref_shapes = (40, 40)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_6')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_7() -> None:
    ref_shapes = (17, 32)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_7')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_8() -> None:
    ref_shapes = (32, 32)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_8')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_9() -> None:
    ref_shapes = (15, 32)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_9')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_pickle_data_10() -> None:
    ref_shapes = (40, 40)
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_10')
    assert ref_shapes == x_qs.shape
    assert ref_shapes == y_qs.shape
    assert ref_shapes == u_qs.shape
    assert ref_shapes == v_qs.shape


def test_psy_coquilles() -> None:
    tst_name = 'read_psy_file_coquilles'
    psy_name = 'coquilles.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_psy_coquilles2() -> None:
    tst_name = 'read_psy_file_coquilles2'
    psy_name = 'coquilles2_herissons.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_psy_coquilles3() -> None:
    tst_name = 'read_psy_file_coquilles3'
    psy_name = 'coquilles3_herissons.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def test_psy_multipages() -> None:
    tst_name = 'read_psy_file_multipages'
    psy_name = 'multipages_herisson_coquilles.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)

