# -*- coding: utf-8 -*-

"""
Tests comparaison images
"""

from matplotlib import pyplot as plt

from matplotlib.testing.decorators import image_comparison


@image_comparison(baseline_images=['line_dashes'], remove_text=True, extensions=['png'])
def test_line_dashes() -> None:
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.plot(range(10), linestyle=(0, (3, 3)), lw=5)
