# -*- coding: utf-8 -*-

"""
Tests pour le module ``pyplotcea``.

Les méthodes de la classe MatplotlibConfiguration sont appelables en C++ via ``matplotlibcpp``.
"""

import os
import pickle

import pytest

import numpy as np
from matplotlib import collections
from matplotlib.testing.decorators import image_comparison
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors

import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd


@image_comparison(baseline_images=['test_nemesis_rc_parameters'], remove_text=True, extensions=['png'])
def test_nemesis_rc_parameters() -> None:
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_linewidth(2)
    nem_plt.PyPlotCEA.set_figure_size(800, 600)
    n = 10
    x = np.arange(n)
    y = np.linspace(2, 2, n)
    plt.plot(x, y)


@image_comparison(baseline_images=['test_set_log_scale_0'], remove_text=True, extensions=['png'])
def test_set_log_scale_0() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.sin(x))
    nem_plt.PyPlotCEA.set_log_scale(False, True)


@image_comparison(baseline_images=['test_set_log_scale_1'], remove_text=True, extensions=['png'])
def test_set_log_scale_1() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.sin(x))
    nem_plt.PyPlotCEA.set_log_scale(True, False)


@image_comparison(baseline_images=['test_set_log_scale_2'], remove_text=True, extensions=['png'])
def test_set_log_scale_2() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.sin(x))
    nem_plt.PyPlotCEA.set_log_scale(True, True)


@image_comparison(baseline_images=['test_set_subticks_log_axes_0'], remove_text=True, extensions=['png'])
def test_set_subticks_log_axes_0() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x))
    plt.yscale('log')
    nem_plt.PyPlotCEA.set_subticks_log_axes('x')


@image_comparison(baseline_images=['test_set_subticks_log_axes_1'], remove_text=True, extensions=['png'])
def test_set_subticks_log_axes_1() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x))
    plt.xscale('log')
    nem_plt.PyPlotCEA.set_subticks_log_axes('y')


@image_comparison(baseline_images=['test_set_subticks_log_axes_2'], remove_text=True, extensions=['png'])
def test_set_subticks_log_axes_2() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x))
    plt.xscale('log')
    plt.yscale('log')
    nem_plt.PyPlotCEA.set_subticks_log_axes('xy')


@image_comparison(baseline_images=['test_set_titles'], extensions=['png'], tol=0.07)
def test_set_titles() -> None:
    nem_plt.PyPlotCEA.set_titles("Bibliothèque", "Sous-titre", "Matériau")
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.tan(x), '.')


def test_get_n_curves() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), x, np.sin(x), x, np.arctan(x))
    assert nem_plt.PyPlotCEA.get_n_curves() == 3


@image_comparison(baseline_images=['test_under_axes_legend'], extensions=['png'])
def test_under_axes_legend() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    plt.plot(x, np.arctan(x), label='arctan(x)')
    nem_plt.PyPlotCEA.under_axes_legend()


@image_comparison(baseline_images=['test_networks_legend'], extensions=['png'])
def test_networks_legend() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    plt.plot(x, np.arctan(x), '--', label='arctan(x)')
    plt.plot(x, x, '--', label='x')
    nem_plt.PyPlotCEA.networks_legend(['cos / sin', 'arccos, arcsin'], [0, 2])


@image_comparison(baseline_images=['test_networks_legend_2'], extensions=['png'])
def test_networks_legend_2() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    plt.plot(x, np.arctan(x), '--', label='arctan(x)')
    plt.plot(x, x, '--', label='x')
    nem_plt.PyPlotCEA.networks_legend(['cos / sin', ], [0, ])


@image_comparison(baseline_images=['test_highlight_curve'], remove_text=True, extensions=['png'])
def test_highlight_curve() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    nem_plt.PyPlotCEA.highlight_curve(1)


@image_comparison(baseline_images=['test_hide_curve'], remove_text=True, extensions=['png'])
def test_hide_curve() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    nem_plt.PyPlotCEA.hide_curve(1)


@image_comparison(baseline_images=['test_show_curve'], remove_text=True, extensions=['png'])
def test_show_curve() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    nem_plt.PyPlotCEA.hide_curve(0)
    nem_plt.PyPlotCEA.show_curve(0)


@image_comparison(baseline_images=['test_show_all_curves'], remove_text=True, extensions=['png'])
def test_show_all_curves() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    plt.plot(x, np.arctan(x), '--', label='arctan(x)')
    plt.plot(x, x, '--', label='x')
    for i in range(nem_plt.PyPlotCEA.get_n_curves()):
        nem_plt.PyPlotCEA.hide_curve(i)
    nem_plt.PyPlotCEA.show_all_curves()


@image_comparison(baseline_images=['test_set_linewidth_to_all'], remove_text=True, extensions=['png'])
def test_set_linewidth_to_all() -> None:
    x = np.linspace(-np.pi, np.pi, 201)
    plt.plot(x, np.cos(x), label='cos(x)')
    plt.plot(x, np.sin(x), label='sin(x)')
    plt.plot(x, np.arctan(x), '--', label='arctan(x)')
    plt.plot(x, x, '--', label='x')
    nem_plt.PyPlotCEA.set_linewidth_to_all(3.)


def tricontour_data() -> (np.array, np.array, np.array):
    n_angles = 48
    n_radii = 8
    min_radius = 0.25
    radii = np.linspace(min_radius, 0.95, n_radii)

    angles = np.linspace(0, 2 * np.pi, n_angles, endpoint=False)
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)
    angles[:, 1::2] += np.pi / n_angles

    x = (radii * np.cos(angles)).flatten()
    y = (radii * np.sin(angles)).flatten()
    z = np.fabs(np.cos(radii) * np.cos(3 * angles)).flatten()

    return x, y, z


@image_comparison(baseline_images=['test_log_triangulation_0_0'], remove_text=True, extensions=['png'])
def test_log_triangulation_0_0() -> None:
    x, y, z = tricontour_data()
    nem_plt.PyPlotCEA.logarithmic_tricontourf(x, y, z, n_levels=8, masked_value=0., draw_iso=True)


@image_comparison(baseline_images=['test_log_triangulation_0_1'], remove_text=True, extensions=['png'])
def test_log_triangulation_0_1() -> None:
    x, y, z = tricontour_data()
    nem_plt.PyPlotCEA.logarithmic_tricontourf(x, y, z, n_levels=8, masked_value=0.)


@image_comparison(baseline_images=['test_log_triangulation_0_2'], remove_text=True, extensions=['png'])
def test_log_triangulation_0_2() -> None:
    x, y, z = tricontour_data()
    nem_plt.PyPlotCEA.logarithmic_tricontour(x, y, z, n_levels=8, masked_value=0., linewidths=2)


@image_comparison(baseline_images=['test_log_triangulation_1_0'], remove_text=True, extensions=['png'])
def test_log_triangulation_1_0() -> None:
    x, y, z = tricontour_data()
    z = np.power(10, z * 16)
    nem_plt.PyPlotCEA.logarithmic_tricontourf(x, y, z, n_decades=2, max_decades=5)


@image_comparison(baseline_images=['test_log_triangulation_1_1'], remove_text=True, extensions=['png'])
def test_log_triangulation_1_1() -> None:
    x, y, z = tricontour_data()
    z = np.power(10, z * 16)
    nem_plt.PyPlotCEA.logarithmic_tricontour(x, y, z, n_decades=2, max_decades=5)


def contour_data() -> (np.array, np.array, np.array):
    x, y, z = nem_rd.gaia_isolines_levels()
    return x, y, z


def test_get_contours_logarithmic_contourf() -> None:
    ref_levels = np.array([1.e-322, 1.e-300, 1.e-278, 1.e-256, 1.e-234, 1.e-212, 1.e-190, 1.e-168, 1.e-146, 1.e-124,
                           1.e-102, 1.e-080, 1.e-058, 1.e-036, 1.e-014, 1.e+008, 1.e+030], dtype=float)
    ref_layers = np.array([5.e-301, 5.e-267, 5.e-233, 5.e-199, 5.e-165, 5.e-131, 5.e-097, 5.e-063, 5.e-029,
                           5.e+005, 5.e+039], dtype=float)
    ref_iso_levels = np.array([1.e-09, 1.e-07, 1.e-05, 1.e-03, 1.e-01, 1.e+01], dtype=float)
    x, y, z = contour_data()
    nem_plt.PyPlotCEA.logarithmic_contourf(x, y, z, masked_value=1.e-300)
    levels = nem_plt.PyPlotCEA.get_contourf_levels()
    layers = nem_plt.PyPlotCEA.get_contourf_layers()
    nem_plt.PyPlotCEA.logarithmic_contour(x, y, z, levels=ref_iso_levels)
    iso_levels = nem_plt.PyPlotCEA.get_contour_levels()
    assert ref_levels.all() == levels.all()
    assert ref_layers.all() == layers.all()
    assert ref_iso_levels.all() == iso_levels.all()


def test_get_contours_linear_contourf() -> None:
    ref_levels = np.array([0., 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1.05, 1.2, 1.35, 1.5], dtype=float)
    ref_layers = np.array([0.075, 0.225, 0.375, 0.525, 0.675, 0.825, 0.975, 1.125, 1.275, 1.425], dtype=float)
    ref_iso_levels = np.array([0., 0.5, 1., 1.5], dtype=float)
    x, y, z = contour_data()
    nem_plt.PyPlotCEA.linear_contourf(x, y, z, masked_value=1.e-300)
    levels = nem_plt.PyPlotCEA.get_contourf_levels()
    layers = nem_plt.PyPlotCEA.get_contourf_layers()
    nem_plt.PyPlotCEA.linear_contour(x, y, z, n_levels=2)
    iso_levels = nem_plt.PyPlotCEA.get_contour_levels()
    assert ref_levels.all() == levels.all()
    assert ref_layers.all() == layers.all()
    assert ref_iso_levels.all() == iso_levels.all()


def test_get_contours_logarithmic_tricontourf() -> None:
    ref_levels = np.array([1.e-19, 1.e-17, 1.e-15, 1.e-13, 1.e-11, 1.e-09, 1.e-07, 1.e-05, 1.e-03, 1.e-01, 1.e+01,
                           1.e+03], dtype=float)
    ref_layers = np.array([5.e-301, 5.e-267, 5.e-233, 5.e-199, 5.e-165, 5.e-131, 5.e-097, 5.e-063, 5.e-029, 5.e+005,
                           5.e+039], dtype=float)
    ref_iso_levels = np.array([1.e-16, 1.e-14, 1.e-12, 1.e-10, 1.e-08, 1.e-06, 1.e-04, 1.e-02, 1.e+00], dtype=float)
    x, y, z = tricontour_data()
    nem_plt.PyPlotCEA.logarithmic_tricontourf(x, y, z, masked_value=1.e-300)
    levels = nem_plt.PyPlotCEA.get_contourf_levels()
    layers = nem_plt.PyPlotCEA.get_contourf_layers()
    nem_plt.PyPlotCEA.logarithmic_tricontour(x, y, z, levels=ref_iso_levels)
    iso_levels = nem_plt.PyPlotCEA.get_contour_levels()
    assert ref_levels.all() == levels.all()
    assert ref_layers.all() == layers.all()
    assert ref_iso_levels.all() == iso_levels.all()


def test_get_contours_linear_tricontourf() -> None:
    ref_levels = np.array([0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.], dtype=float)
    ref_layers = np.array([0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95], dtype=float)
    ref_iso_levels = np.array([0., 0.4, 0.8, 1.2], dtype=float)
    x, y, z = tricontour_data()
    nem_plt.PyPlotCEA.linear_tricontourf(x, y, z, masked_value=1.e-300)
    levels = nem_plt.PyPlotCEA.get_contourf_levels()
    layers = nem_plt.PyPlotCEA.get_contourf_layers()
    nem_plt.PyPlotCEA.linear_tricontour(x, y, z, n_levels=2)
    iso_levels = nem_plt.PyPlotCEA.get_contour_levels()
    print(f'iso_levels = {iso_levels}')
    assert ref_levels.all() == levels.all()
    assert ref_layers.all() == layers.all()
    assert ref_iso_levels.all() == iso_levels.all()


@image_comparison(baseline_images=['test_iso_log_mapping_0'], extensions=['png'])
def test_iso_log_mapping_0() -> None:
    x, y, z = contour_data()
    nem_plt.PyPlotCEA.logarithmic_contourf(x, y, z, label='carte',
                                           draw_iso=False, masked_value=1e-300, n_decades=9, max_decades=100)
    iso_levels = nem_plt.PyPlotCEA.get_contourf_levels()
    nem_plt.PyPlotCEA.logarithmic_contour(x, y, z, levels=iso_levels, label='iso', colors='black', draw_colorbar=False)


@image_comparison(baseline_images=['test_isocolorbar_lin_0'], extensions=['png'])
def test_isocolorbar_lin_0() -> None:
    levels = np.array([0., 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1.05, 1.2, 1.35, 1.5], dtype=float)
    x, y, z = contour_data()
    cs = plt.contour(x, y, z, levels)
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, drawedges=False, )
    cb.set_label('')


@image_comparison(baseline_images=['test_isocolorbar_lin_1'], extensions=['png'])
def test_isocolorbar_lin_1() -> None:
    x, y, z = contour_data()
    cs = plt.contour(x, y, z, 10)
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, drawedges=False, )
    cb.set_label('')


@image_comparison(baseline_images=['test_isocolorbar_log_tri_0'], extensions=['png'])
def test_isocolorbar_log_tri_0() -> None:
    levels = np.array([1.e-19, 1.e-17, 1.e-15, 1.e-13, 1.e-11, 1.e-09, 1.e-07, 1.e-05, 1.e-03, 1.e-01, 1.e+01,
                       1.e+03], dtype=float)
    x, y, z = tricontour_data()
    cs = plt.tricontour(x, y, z, levels)
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, drawedges=False, )
    cb.set_label('Isocolorbar')


@image_comparison(baseline_images=['test_isocolorbar_log_tri_1'], extensions=['png'])
def test_isocolorbar_log_tri_1() -> None:
    x, y, z = tricontour_data()
    cs = plt.tricontour(x, y, z, 8)
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, drawedges=False, )
    cb.set_label('')


@image_comparison(baseline_images=['test_isocolorbar_lin_tri_0'], extensions=['png'])
def test_isocolorbar_lin_tri_0() -> None:
    levels = np.array([0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.], dtype=float)
    x, y, z = tricontour_data()
    cs = plt.tricontour(x, y, z, levels)
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, drawedges=False, )
    cb.set_label('')


@image_comparison(baseline_images=['test_isocolorbar_lin_tri_1'], extensions=['png'])
def test_isocolorbar_lin_tri_1() -> None:
    x, y, z = tricontour_data()
    cs = plt.tricontour(x, y, z, 5)
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, drawedges=False, )
    cb.set_label('')


@image_comparison(baseline_images=['test_raised_contourf_7'], remove_text=True, extensions=['png'])
def test_raised_contourf_7() -> None:
    x, y, z = nem_rd.waves(20, 20)
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, xscale='log', yscale='log', cmap='current',
                                      label='$x^2 * cos(y) + y^2*sin(x)$')
    plt.gca().view_init(25, -65)


@image_comparison(baseline_images=['test_raised_contourf_8'], remove_text=True, extensions=['png'])
def test_raised_contourf_8() -> None:
    x, y, z = nem_rd.waves(30, 30)
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current',
                                      label='$x^2 * cos(y) + y^2*sin(x)$')
    plt.gca().view_init(25, -65)


def wavelet_data() -> (np.array, np.array, np.array, np.array, np.array, np.array):
    x, y, z = nem_rd.wavelet(30, 30)
    px, py = x, y
    pz = z[0::x.size]
    return x, y, z, px, py, pz


@image_comparison(baseline_images=['test_set_alpha_default'], remove_text=True, extensions=['png'])
def test_set_alpha_default() -> None:
    x, y, z, px, py, pz = wavelet_data()
    ax = nem_plt.PyPlotCEA.switch_axes('3d')
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', edgecolor='none')
    ax.plot3D(px, py, pz, 'r-')
    plt.gca().view_init(88, -54)
    nem_plt.PyPlotCEA.set_alpha(0.7)


@image_comparison(baseline_images=['test_set_alpha_path_collection_obj'], remove_text=True, extensions=['png'])
def test_set_alpha_path_collection_obj() -> None:
    x, y, z, px, py, pz = wavelet_data()
    nem_plt.PyPlotCEA.linear_contourf(x, y, z)
    nem_plt.PyPlotCEA.set_alpha(0.5, [], collections.PathCollection)


@image_comparison(baseline_images=['test_set_alpha_path_collection_str'], remove_text=True, extensions=['png'])
def test_set_alpha_path_collection_str() -> None:
    x, y, z, px, py, pz = wavelet_data()
    nem_plt.PyPlotCEA.linear_contourf(x, y, z)
    nem_plt.PyPlotCEA.set_alpha(0.5, [0, 2], 'collections.PathCollection')


def need_reshape_data() -> (np.array, np.array, np.array):
    x_c = np.array([-3.0, -1.5, 1.5, 3.0])
    y_c = np.array([-2.0, -1.0, 0.0, 1.0, 2.0])
    z_c = np.array([3.0, 0.1, 0.1, 3.0,
                    0.1, 0.2, 0.2, 0.1,
                    0.1, 3.0, 3.0, 0.1,
                    0.1, 0.2, 0.2, 0.1,
                    3.0, 0.1, 0.1, 3.0])
    return x_c, y_c, z_c


@image_comparison(baseline_images=['test_cpp_reshape_contourf'], remove_text=True, extensions=['png'])
def test_cpp_reshape_contourf() -> None:
    x_c, y_c, z_c = need_reshape_data()
    nem_plt.PyPlotCEA.set_cmap('cividis')
    nem_plt.PyPlotCEA.linear_contourf(x_c, y_c, z_c, label='Linear', draw_colorbar=False)


@image_comparison(baseline_images=['test_cpp_reshape_raised_contourf'], remove_text=True, extensions=['png'])
def test_cpp_reshape_raised_contourf() -> None:
    x_c, y_c, z_c = need_reshape_data()
    nem_plt.PyPlotCEA.raised_contourf(x_c, y_c, z_c, cmap='viridis', draw_colorbar=False)
    plt.gca().view_init(20, 30)


@image_comparison(baseline_images=['test_plot_hedgehogs'], extensions=['png'])
def test_plot_hedgehogs() -> None:
    color_seg_base = [mcolors.to_rgba(c) for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]
    hh_data = nem_rd.psy_hedgehog()
    ax = nem_plt.PyPlotCEA.switch_axes('3d')
    for hh in hh_data:
        for arr, c in zip(hh.data, hh.colors):
            ax.plot3D(*arr, color=color_seg_base[c])
    ax.view_init(azim=165, elev=15)


def quiver_streamplot_data(bname) -> (np.array, np.array, np.array, np.array):
    data_path = os.path.join(nem_rd.get_data_dir(), bname, f'data_{bname}.pickle')
    data = pickle.load(open(data_path, 'rb'))
    return data['x'], data['y'], data['u'], data['v']


@image_comparison(baseline_images=['test_quiver_data_1_1'], extensions=['png'], remove_text=True)
def test_quiver_data_1_1() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_1')
    nem_plt.PyPlotCEA.nquiver(x_qs, y_qs, u_qs, v_qs)


@image_comparison(baseline_images=['test_quiver_data_1_2'], extensions=['png'], remove_text=True)
def test_quiver_data_1_2() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_1')
    nem_plt.PyPlotCEA.nquiver(x_qs, y_qs, u_qs, v_qs, title='THE TITLE', width=0.006)


@image_comparison(baseline_images=['test_quiver_data_1_3'], extensions=['png'], remove_text=True)
def test_quiver_data_1_3() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_1')
    nem_plt.PyPlotCEA.nquiver(x_qs, y_qs, u_qs, v_qs, draw_colorbar=False)


@image_comparison(baseline_images=['test_streamplot_data_2_1'], extensions=['png'], remove_text=True)
def test_streamplot_data_2_1() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_2')
    nem_plt.PyPlotCEA.nstreamplot(x_qs, y_qs, u_qs, v_qs)


@image_comparison(baseline_images=['test_streamplot_data_2_2'], extensions=['png'], remove_text=True)
def test_streamplot_data_2_2() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_2')
    nem_plt.PyPlotCEA.nstreamplot(x_qs, y_qs, u_qs, v_qs, lw_mag=True)


@image_comparison(baseline_images=['test_streamplot_data_2_3'], extensions=['png'], remove_text=True)
def test_streamplot_data_2_3() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_2')
    nem_plt.PyPlotCEA.nstreamplot(x_qs, y_qs, u_qs, v_qs, color=None)


@image_comparison(baseline_images=['test_streamplot_data_10_1'], extensions=['png'], remove_text=True)
def test_streamplot_data_10_1() -> None:
    x_qs, y_qs, u_qs, v_qs = quiver_streamplot_data('data_10')
    nem_plt.PyPlotCEA.nstreamplot(x_qs, y_qs, u_qs, v_qs, draw_colorbar=False, title='')


xd8, yd8, ud8, vd8 = quiver_streamplot_data('data_8')
xd8_flatten, yd8_flatten, ud8_flatten, vd8_flatten = xd8.flatten(), yd8.flatten(), ud8.flatten(), vd8.flatten()
xd8_small = xd8[0]
yd8_small = yd8[:, 0]


quiver_xd8_testdata = [
    ('x, y, uf, vf', [(xd8, yd8, ud8_flatten, vd8_flatten)]),
    ('xf, yf, uf, vf', [(xd8_flatten, yd8_flatten, ud8_flatten, vd8_flatten)]),
    ('xf, yf, u, v', [(xd8_flatten, yd8_flatten, ud8, vd8)],),
    ('xfs, yfs, u, v', [(xd8_small, yd8_small, ud8, vd8)],),
    ('x, y, uf, vf', [(xd8, yd8, ud8_flatten, vd8_flatten)])
]

streamplot_xd8_testdata = [
    ('x, y, uf, vf', [(xd8, yd8, ud8_flatten, vd8_flatten)]),
    ('xfs, yfs, u, v', [(xd8_small, yd8_small, ud8, vd8)],),
    ('xfs, yfs, uf, vf', [(xd8_small, yd8_small, ud8_flatten, vd8_flatten)],),
    ('xf, yf, u, v', [(xd8_flatten, yd8_flatten, ud8, vd8)])
]


@pytest.mark.parametrize("arrays", quiver_xd8_testdata)
def test_quiver_data_8_combination(arrays) -> None:
    name, [(x_qs, y_qs, u_qs, v_qs)] = arrays
    try:
        nem_plt.PyPlotCEA.nquiver(x_qs, y_qs, u_qs, v_qs)
    except Exception as e:
        pytest.fail(f'Echec nquiver combinaison {name}', pytrace=True)


@pytest.mark.parametrize("arrays", streamplot_xd8_testdata)
def test_streamplot_data_8_combination(arrays) -> None:
    name, [(x_qs, y_qs, u_qs, v_qs)] = arrays
    try:
        nem_plt.PyPlotCEA.nstreamplot(x_qs, y_qs, u_qs, v_qs)
    except Exception as e:
        pytest.fail(f'Echec nstreamplot combinaison {name}', pytrace=True)


def test_streamplot_data_8_combination_1d() -> None:
    try:
        nem_plt.PyPlotCEA.nstreamplot(xd8_flatten, yd8_flatten, ud8_flatten, vd8_flatten,
                                      nx=xd8.shape[0], ny=xd8.shape[1])
    except Exception as e:
        pytest.fail("Echec : nsteamplot 1D avec nx,ny ", pytrace=True)
