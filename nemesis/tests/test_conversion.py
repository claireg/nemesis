# -*- coding: utf-8 -*-

"""Tests pour le subpackage ``conversion``.

* Pas de tests pour ``psy_draw`` qui ne fait qu'un ``plt.show()`` de plus que ``psy_extract``.
* La comparaison de dictionnaire est fait dans le module test_read_data
"""

import os
import tempfile
import shutil
import warnings

import pytest

from matplotlib.testing.compare import compare_images

import nemesis.conversion as nem_cv
import nemesis.testing as nem_tst

DATA_DIR = os.path.join(os.path.dirname(__file__), '..', '..', 'data')
TEST_DIR = nem_tst.get_objects_test_dir(__file__)

psy_files_testdata = [
    ("coquilles.psy", 0.01),
    ("herisson.psy", 0.01),
    ("onde.psy", 0.01),
    ("coquilles3_herissons.psy", 0.01),
    ("plusieurs_courbes.psy", 0.01),
    ("vecteurs.psy", 0.01),
    ("multipages_herisson_coquilles.psy", 0.01),
    ("courbe3d.psy", 0.01),
    ("plusieurs_reseaux.psy", 0.01),
    ("coquilles2_herissons.psy", 0.01),
]
# carte_support_structure_compose.psy : fichier mal formé


@pytest.mark.parametrize("fic,tol", psy_files_testdata)
def test_psy_convert(fic, tol) -> None:
    bfic, ext = os.path.split(fic)
    tmp_dir = tempfile.TemporaryDirectory(prefix=bfic)
    filename = os.path.join(DATA_DIR, fic)
    actuals = nem_cv.psy_convert(filename, tmp_dir.name, 'png')
    results = []
    for actual in actuals:
        actual_path, actual_name = os.path.split(actual)
        expected = os.path.join(TEST_DIR, actual_name)
        # premier passage du test, stockage de l'image résultat
        if not os.path.exists(expected):
            shutil.copyfile(actual, expected)
        result = compare_images(expected, actual, tol=0.01)
        results.append(True if result is None else False)
    assert all(results) is True

