# -*- coding: utf-8 -*-

import os
import numpy as np
from numpy.testing import (
    assert_array_equal
)
import nemesis.testing as nem_tst
import nemesis.readdata as nem_rd

TEST_DIR = nem_tst.get_objects_test_dir(__file__)

__psy_dict_herisson = None
__psy_dict_onde = None
__psy_dict_vec = None


def get_psy_dict_from_file(filename: str) -> dict:
    psy_data = os.path.join(nem_rd.get_data_dir(), filename)
    if os.path.exists(psy_data):
        return nem_rd.ReadPsyFile.read_psy_file(psy_data)
    else:
        raise RuntimeWarning(f'Fichier {psy_data} inexistant')


def get_herisson_dict():
    global __psy_dict_herisson
    if __psy_dict_herisson is None:
        __psy_dict_herisson = get_psy_dict_from_file('herisson.psy')
    return __psy_dict_herisson


def get_onde_dict():
    global __psy_dict_onde
    if __psy_dict_onde is None:
        __psy_dict_onde = get_psy_dict_from_file('onde.psy')
    return __psy_dict_onde


def get_vec_dict():
    global __psy_dict_vec
    if __psy_dict_vec is None:
        __psy_dict_vec = get_psy_dict_from_file('vecteurs.psy')
    return __psy_dict_vec


def test_psyu_herisson_base_0() -> None:
    psy_dict = get_herisson_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    page_names = analyzer.psy_page_name()
    assert len(page_names) == 1
    assert page_names[0] == 'GEOMETRIE3D_4'

    object_names = analyzer.psy_page('Page', 'GEOMETRIE3D_4')
    assert str(object_names[0]) == '[GEOMETRIE3D_4_scene, GEOMETRIE3D_4_title, GEOMETRIE3D_4_comment]'
    assert object_names[1] == ['StandardText', 'Scene', 'ColorLegend']

    frame_names = analyzer.psy_page_frame_name('GEOMETRIE3D_4')
    assert str(frame_names) == '[GEOMETRIE3D_4_scene, GEOMETRIE3D_4_title, GEOMETRIE3D_4_comment]'

    collection_names = analyzer.psy_page_collection_name('GEOMETRIE3D_4')
    assert collection_names == 'GEOMETRIE3D_4'


def test_psyu_herisson_base_1() -> None:
    psy_dict = get_herisson_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    collection_names = analyzer.psy_page_collection_name('GEOMETRIE3D_4')
    assert collection_names == 'GEOMETRIE3D_4'

    # @FIXME pour l'instant collection_names est une string et non une liste de string
    graphics, next_types = analyzer.psy_collection('Collection', collection_names)
    assert len(graphics) == 283
    assert nem_rd.PsyObject('GEOMETRIE3D_4_Crb3D_901') in graphics
    assert next_types == ['DepthCurve', 'StandardCurve', 'SetOfCurve', 'VectorField',
                          'RaiseMap', 'UnstructuredRaiseMap']

    rep_typ = analyzer.psy_representation_type('GEOMETRIE3D_4_Crb3D_902')
    assert rep_typ == 'DepthCurve'

    graphic_names = analyzer.psy_page_graphic_name('GEOMETRIE3D_4')
    assert graphic_names == ['GEOMETRIE3D_4_Crb3D_901']

    scene_type = analyzer.psy_scene_type('GEOMETRIE3D_4')
    assert scene_type == 'DEPTH'

    # comment on récupère le nom de la scène ?
    object_names_2 = analyzer.psy_scene('Scene', 'GEOMETRIE3D_4_scene')
    assert object_names_2[0] == nem_rd.PsyEnum('GEOMETRIE3D_4')

    texts = analyzer.psy_text('GEOMETRIE3D_4')
    assert len(texts) == 2
    assert texts[0] == nem_rd.PsyStdTextData(title='Géométrie3D', posx=0.5, posy=0.96, alignment='center',
                                             name=nem_rd.PsyObject('GEOMETRIE3D_4_title'))
    assert texts[1] == nem_rd.PsyStdTextData(title='herisson.exp', posx=0.5, posy=0.0, alignment='center',
                                             name=nem_rd.PsyObject('GEOMETRIE3D_4_comment'))


def test_psyu_herisson_base_2() -> None:
    psy_dict = get_herisson_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    scene_labels = analyzer.psy_axes_label('GEOMETRIE3D_4')
    assert scene_labels == ('X', 'mm', 'Y', 'mm', 'Z', 'mm')

    scene_scales = analyzer.psy_axes_scaling('GEOMETRIE3D_4')
    linear = nem_rd.PsyEnum('LIN')
    assert scene_scales == (linear, linear, linear)

    xlb, xu, fl, fu = analyzer.psy_curve_axes_label('GEOMETRIE3D_4_Crb3D_1183')
    assert xlb == ['X', 'Y']
    assert xu == ['mm', 'mm']
    assert fl == 'Z'
    assert fu == 'mm'


def test_psyu_herisson_data_0() -> None:
    test_name = 'test_psyu_herisson_data_0'
    psy_dict = get_herisson_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    depth_curve = analyzer.psy_depth_curve('DepthCurve', 'GEOMETRIE3D_4_Crb3D_901')
    assert len(depth_curve) == 6
    assert depth_curve[0:3] == (5, False, True)
    assert depth_curve[3].all() == np.array([0., 103.764]).all()
    assert depth_curve[4].all() == np.array([0., 0.13185]).all()
    assert depth_curve[5].all() == np.array([0., 50.3624]).all()

    function_1183 = analyzer.psy_function('Function', 'GEOMETRIE3D_4_Crb3D_1183')
    assert len(function_1183) == 3
    assert function_1183[0].all() == np.array([0., -24.4187]).all()
    assert function_1183[1].all() == np.array([0., 5.42157]).all()
    assert function_1183[2].all() == np.array([0., 1.15536]).all()

    ref_data_1183 = analyzer.psy_function_data('GEOMETRIE3D_4')
    data = nem_tst.load_pickled(TEST_DIR, test_name, ref_data_1183)
    for d_ref, d in zip(ref_data_1183, data):
        nem_tst.compare_psy_curve_data(d_ref, d)


def test_psyu_networks() -> None:
    test_name = 'test_psyu_networks'
    psy_dict = get_psy_dict_from_file('plusieurs_reseaux.psy')
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)

    legend = analyzer.psy_legend('pg5_a1')
    assert len(legend) == 2
    # les espaces après R1 sont dans le fichier .psy d'origine
    assert legend[0] == ['R1      ', 'R2', 'R3']

    networks = analyzer.get_psy_object('SetOfCurve', 'res5_R1')
    # ref_data = (x, y)
    c, ref_data = analyzer.psy_net_curve('SetOfCurve', networks)

    assert c == 1
    data = nem_tst.load_pickled(TEST_DIR, test_name, ref_data)
    for d_ref, d in zip(ref_data, data):
        assert_array_equal(d_ref, d)


def test_psyu_curves() -> None:
    test_name = 'test_psyu_curves'
    psy_dict = get_psy_dict_from_file('plusieurs_courbes.psy')
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    page_names = analyzer.psy_page_name()

    legend = analyzer.psy_legend(page_names[0])
    assert len(legend) == 2
    # les espaces après R1 sont dans le fichier .psy d'origine
    assert legend[0] == ['C1      ', 'C2', 'C3']

    curves = analyzer.get_psy_object('StandardCurve', 'crb4_C1')
    # ref_data = (x, y)
    c, m, ref_data = analyzer.psy_std_curve('StandardCurve', curves)

    assert c == 1
    assert m is False
    data = nem_tst.load_pickled(TEST_DIR, test_name, ref_data)
    for d_ref, d in zip(ref_data, data):
        assert_array_equal(d_ref, d)


def test_psyu_vec_cmap() -> None:
    psy_dict = get_vec_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    colormap = analyzer.psy_colormap_name()
    assert colormap == 'DefaultColorMap'


def test_psyu_vec_data() -> None:
    test_name = 'test_psyu_vec_data'
    psy_dict = get_vec_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    vecfield = analyzer.get_psy_object('VectorField', 'FLD_02')
    # ref_data = x, y, u, v, h, c
    ref_data = analyzer.psy_vector_field('VectorField', vecfield)

    data = nem_tst.load_pickled(TEST_DIR, test_name, ref_data)
    for d_ref, d in zip(ref_data, data):
        assert_array_equal(d_ref, d)


def test_psyu_onde_name() -> None:
    psy_dict = get_onde_dict()
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    name = analyzer.psy_raisemap_name()
    assert name == 'Onde'


def test_psyu_onde_data() -> None:
    test_name = 'test_psyu_onde_data'
    analyzer = nem_rd.AnalyzePsyDict(get_onde_dict())
    name = analyzer.psy_raisemap_name()
    colormap_name = analyzer.psy_colormap_name()
    # ref_data = [x_tab, y_tab, z_tab, nb_col]
    ref_data = analyzer.psy_map_data(name, colormap_name)
    assert ref_data[-1] == 240

    data = nem_tst.load_pickled(TEST_DIR, test_name, ref_data)
    for d_ref, d in zip(ref_data[:-1], data[:-1]):
        assert_array_equal(d_ref, d)


def test_psyu_coquilles2_0() -> None:
    test_name = 'test_psyu_coquilles2_0'
    psy_dict = get_psy_dict_from_file('coquilles2_herissons.psy')
    analyzer = nem_rd.AnalyzePsyDict(psy_dict)
    rmap = analyzer.get_psy_object('UnstructuredRaiseMap', 'GEOMETRIE3D_3_SHAPE_1751')
    # data_ref : [x_ref, y_ref, h_ref, c_ref, t_ref]
    ref_data = analyzer.psy_unstruct_map('UnstructuredRaiseMap', rmap)

    data = nem_tst.load_pickled(TEST_DIR, test_name, ref_data)
    for d_ref, d in zip(ref_data, data):
        assert_array_equal(d_ref, d)
