# -*- coding: utf-8 -*-

"""Packages pour les tests.

Les tests sont écrits pour py.test, doctest.

Il existe un script ``run_tests.py`` pour lancer la non-régression.

Warnings
--------
* Les tests simples (appel fct suivi d'un print) sont fait en doctest
* Les plus complexes sont faits avec pytest (et l'extension pytest de matplotlib)
* Les tests nécessitants une comparaison d'image sont fait avec la fixture matplotlib


Note
----
* Lancemet des tests à la main via un module python::

    cd <workdir>/nemesis
    python3 ./run_tests.py

* Pour avoir les tests les plus lents (```pytest```)::

    pytest --durations=3

* Pour savoir quels sont les tests collectés::

    pytest --collect-only

* Pour avoir un sortie junit::

    pytest --junitxml=/tmp/MyProducts/nemesis_tests

* [obsolète] Pour avoir les doctests avec pytest::

    pytest --doctest-modules

* Plus simplement : tests pytest et doctest (avec pytest.ini dans rep `nemesis`)::

    pytest

* [obsolète] Pour lancer les tests en doctest à la main::

    python -m doctest utilities.py [-v]

* [obsolète] Pour lancer les tests ```pytest``` du répertoire *tests* à la main::

    cd <workdir>/nemesis/tests
    pytest

"""

import os


# Check that the test directories exist
if not os.path.exists(os.path.join(
        os.path.dirname(__file__), 'baseline_images')):
    raise IOError(
        'The baseline image directory does not exist. '
        'This is most likely because the test data is not installed. '
        'You may need to install matplotlib from source to get the '
        'test data.')
