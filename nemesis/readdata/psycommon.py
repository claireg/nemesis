# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import List, TypeVar
import warnings

import numpy as np


levels = ['ScreenViewer', 'Page']

psy_object_generic = {
    'Data': ['ListFloatData', 'ArrayFloatData', 'ListIntegerData', 'ArrayIntegerData', 'StringData'],
    'Value': ['FloatValue', 'IntegerValue', 'StringValue'],
    'Support': ['ScatteredSupport', 'StructuredSupport', 'UnstructuredSupport'],
    'Representation': ['StandardCurve', 'SetOfCurve', 'DepthCurve', 'FlatMap', 'RaiseMap', 'PointField',
                       'VectorField',
                       'UnstructuredRaiseMap', 'FlatHistogram', 'DepthHistogram', 'Image']
}

hierarchy = {'ScreenViewer': [{'authorizedPage': ['Page']}],
             'Page': [{'frame': ['StandardText', 'Scene', 'ColorLegend']}],
             'StandardText': [{'textContents': []}, {'frameAnchorX': []}, {'frameAnchorY': []}],
             'ColorLegend': [{'legendTitle': []}],
             'Scene': [{'sceneContents': ['Collection']}],
             'Collection': [{'graphic': ['DepthCurve', 'StandardCurve', 'SetOfCurve', 'VectorField', 'RaiseMap',
                                         'UnstructuredRaiseMap']}],
             'DepthCurve': [{'heightFunction': ['Function']}],
             'StandardCurve': [{'heightFunction': ['Function']}],
             'SetOfCurve': [{'heightFunction': ['Function']}],
             'RaiseMap': [{'heightFunction': ['Function']}, {'colorFunction': ['Function']}],
             'UnstructuredRaiseMap': [{'heightFunction': ['Function']}, {'colorFunction': ['Function']}],
             'VectorField': [{'vectorFunction': ['Function']}, {'heightFunction': ['Function']},
                             {'colorFunction': ['Function']}],
             'Function': [{'geometricalSupport': ['ScatteredSupport', 'StructuredSupport',
                                                  'UnstructuredSupport']},
                          {'physicalValue': ['FloatValue']}],
             'ScatteredSupport': [{'coordinateValues': ['FloatValue']}],
             'StructuredSupport': [{'coordinateValues': ['FloatValue']}],
             'UnstructuredSupport': [{'coordinateValues': ['FloatValue']}, {'topology': ['NodeTopology']}],
             'NodeTopology': [{'cells': ['ArrayIntegerData']}],
             'FloatValue': [{'data': ['ListFloatData', 'ArrayFloatData']}],
             'ListFloatData': [{'data.values': []}],
             'ArrayFloatData': [{'data.values': []}],
             'IntegerValue': [{'data': ['ListIntegerData', 'ArrayIntegerData']}],
             'ListIntegerData': [{'data.values': []}],
             'ArrayIntegerData': [{'data.values': []}],
             }


class PsyEnum(object):
    """
    Classe représentant un enum définit dans Psyché.

    Attributes
    ----------
    name : string
        Nom de l'enum (e.g. DISABLE, ABSOLUTE, MIN, SOLID, …).

    """

    def __init__(self, name: str) -> None:
        self.__name = name

    def __repr__(self) -> str:
        return f'{self.__name}'

    def __eq__(self, other) -> bool:
        return self.__name == other.name

    @property
    def name(self) -> str:
        """Nom de l'enum (e.g. DISABLE, ABSOLUTE, MIN, SOLID, …)."""
        return self.__name


class PsyObject(object):
    """
    Classe représentant un objet définit dans Psyché.

    Attributes
    ----------
    name : string
        Nom de l'objet (e.g. Function, RaiseMap, Page, …).

    """

    def __init__(self, name: str) -> None:
        self.__name = name

    def __repr__(self) -> str:
        return f'{self.__name}'

    def __eq__(self, other) -> bool:
        return self.__name == other.name

    @property
    def name(self) -> str:
        """Nom de l'objet Psyché (e.g. Function, RaiseMap, Page, …)."""
        return self.__name


@dataclass
class PsyCurveData:
    """Classe pour stocker les données d'une ou plusieurs courbes, réseaux, hérissons.
    Toutes données affichables via pyplot.plot ou mpl_toolkits.mplot3d.Axes3D.plot3D."""
    xlabel: str
    xunit: str
    ylabel: str
    yunit: str
    zlabel: str
    zunit: str
    data: List[np.array]
    colors: List[int]
    mark: bool
    height: bool
    name: str


@dataclass
class PsyStdTextData:
    """
    Classe pour stocker les données d'un ou plusieurs Texte de graphique.
    """
    title: str
    posx: float
    posy: float
    alignment: str
    name: str


NemPsyTypes = TypeVar('NemPsyTypes', str, PsyEnum, PsyObject)


def psy_alignment(inlabel: str) -> str:
    """
    Conversion de la valeur d'un attribut frameAnchorRef de Psyche
    vers son equivalent pour matplotlib

    Parameters
    ----------
    inlabel :
        Mot-cle Psyché définissant l'ancrage du texte dans l'image.

    Returns
    -------
    str :
        Correspondance de ce mot-clé pour matplotlib.

    """
    if inlabel in ('CENTER', 'LEFT', 'RIGHT'):
        return inlabel.lower()
    elif inlabel in ('BOTTOM_MIDDLE', 'TOP_MIDDLE'):
        return 'center'
    elif inlabel in ('MIDDLE_LEFT', 'BOTTOM_LEFT'):
        return 'left'
    elif inlabel in ('MIDDLE_RIGHT', 'BOTTOM_RIGHT'):
        return 'right'
    else:
        warnings.warn(f'frameAnchorRef : mot-clé : {inlabel} non prévu.')
        return 'right'


def get_axis_scale(scale):
    """Retourne l'échelle d'un axe selon `matplotlib`

    Parameters
    ----------
    scale:
        Échelle de l'axe en dénomination Pysché

    Returns
    -------
        Échelle selon la dénomination `matplotlib`
    """
    return 'linear' if scale == PsyEnum('LIN') else 'log'


def get_axis_label(label: str, lb_unit: str) -> str:
    """Retourne le label d'un axe.

    Parameters
    ----------
    label:
        label de base
    lb_unit:
        unité de l'axe

    Returns
    -------
        le label complet (avec les unités si il y en a)

    Notes
    -----
        La méthode peut sembler inutile mais elle permet d'unifier les
        labels des axes (code à un seul endroit)
    """
    return f'{label} ({lb_unit})' if lb_unit != '' else label