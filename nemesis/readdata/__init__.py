# -*- coding: utf-8 -*-

"""
Lecture de différents types de données.

Les fichiers lus sont
* au format Psyché.
* au format Gaia XML.

Dans tous les cas, les tableaux de réels ou entiers sont des tableauy numpy.

Notes
-----
L'import de ce package doit être comme suit::

    import nemesis.readdata as nem_rd
"""
import numpy as np
from typing import List

from .utilities import (
    narray_prop,
    get_data_dir,
    get_ref_data_dir,
    path_anonymizer,
    anonym_path,
    compute_section_efficace,
)

from .psyreader import (
    ReadPsyFile,
)

from .psycommon import (
    levels,
    psy_object_generic,
    hierarchy,
    PsyObject,
    PsyEnum,
    PsyCurveData,
    PsyStdTextData,
    NemPsyTypes,
    psy_alignment,
    get_axis_scale,
    get_axis_label,
)

from .gaiareader import (
    ReadGaiaFile,
)

from .psyutilities import (
    psy_map_data,
    get_curves_from_psy_file,
    AnalyzePsyDict,
)

from .psydatapool import (
    print_psy_several_curves,
    print_psy_several_networks,
    print_psy_vectors,
    print_psy_wave,
    print_psy_curve_3d,
    psy_hedgehog,
)

from .gaia_utilities import (
    gaia_points_data,
    gaia_one_mesh1d_data,
    gaia_network_data,
    gaia_map_data,
)
from .gaiadatapool import (
    gaia_curve_data,
    gaia_stairs_curve_data,
    gaia_section_efficace,
    gaia_isolines_levels,
)


def waves(nx: int = 50, ny: int = 50) -> (np.array, np.array, np.array):
    """
    Création d'ondes 3D

    Parameters
    ----------
    nx : int
        Nombre de points selon les abscisses (défaut 50)
    ny : int
        Nombre de points selon les ordonnées (défaut 50)

    Returns
    -------
    np.array, np.array, np.array
        Abscisses, ordonnées, Cotes (x.size * y.size)

    Notes
    -----
    * x : linéaire entre 10 et 45
    * y : linéaire entre 10 et 30
    * $z = x^2*cos(y) + y^2*sin(x)$
    """
    x = np.linspace(10, 45, num=nx)
    y = np.linspace(10, 30, num=ny)
    z = np.zeros(x.size * y.size)
    iz = 0
    for yy in x:
        for xx in y:
            z[iz] = xx*xx*np.cos(yy) + yy*yy*np.sin(xx)
            iz += 1
    return x, y, z


def wavelet(nx: int = 50, ny: int = 50) -> (np.array, np.array, np.array):
    """
    Création d'une ondelette 3D

    Parameters
    ----------
    nx : int
        Nombre de points selon les abscisses (défaut 50)
    ny : int
        Nombre de points selon les ordonnées (défaut 50)

    Returns
    -------
    np.array, np.array, np.array
        Abscisses, ordonnées, Cotes (x.size * y.size)

    Notes
    -----
    * x : linéaire entre -5 et 5
    * y : linéaire entre -5 et 5
    * z = sin(hypot(x, y))
    """
    x = np.linspace(-5, 5, num=nx)
    y = np.linspace(-5, 5, num=ny)
    z = np.zeros(x.size * y.size)
    # z = np.sin(np.hypot(x, y))
    iz = 0
    for yy in y:
        for xx in x:
            z[iz] = np.sin(np.hypot(xx, yy))
            iz += 1
    return x, y, z


def wavelets_2d(n: int = 50) -> List[np.array]:
    """Création de m ondelettes 2D.

    Chaque ondelette est une «tranche» de l'ondelette 3D (`wavelet`).
    Le nombre d'ondelettes est proportionnelle au nombre de points dans l'ondelette 3D.

    Parameters
    ----------
    n: int
        Nombre de points d'abscisses

    Returns
    -------
    List[np.array]
        Liste des ondelettes 2D.
    """
    x, y, z = wavelet(n, n)
    mid = x.size // 2
    start = x.size // 3
    zz = []
    for i in range(start, mid):
        zz.append(z[i::x.size])
    return zz


def parametric(n: int = 100) -> (np.array, np.array, np.array):
    """
    Création d'une courbe paramétrique en 3D.

    Parameters
    ----------
    n : int
        Nombre de points (défaut = 100)

    Returns
    -------
    np.array, np.array, np.array
        Abscisses, Ordonnées et Cotes

    Notes
    -----
    f(theta, r) = (x, y, z)
    * theta : linéaire entre -4 pi et 4 pi
    * z : linéaire entre -2 et 2
    * $r = z^2$
    * x = r * sin(theta)
    * y = r * cos(theta)
    """
    theta = np.linspace(-4 * np.pi, 4 * np.pi, n)
    z = np.linspace(-2, 2, n)
    r = z**2 + 1
    # print(theta)
    # print(r)
    # print(z)
    x = r * np.sin(theta)
    y = r * np.cos(theta)
    return x, y, z
