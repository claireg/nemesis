# -*- coding: utf-8 -*-

"""
Lecture d'un fichier .xml produit par Gaia.

Accès aux données lues.

"""
from typing import Union, List
import pprint
import logging

import numpy as np

import xml.etree.ElementTree as ElT

import nemesis.tools as nem_tools


class ReadGaiaFile(object):
    """
    Lecture d'un fichier de données exporté par Gaia.

    Si EnsembleDePoints, alors
        n EnsembleDePoints.Dimension
        1 EnsemebleDePoints.Data

    Si Maillage1D, alors
        1 Data

    Si Champ, alors
        1 Support1D
        1 Data

    Notes
    -----
    Les clés des dictionnaires sont les noms des tags (élément). Les attributs d'un tag sont mis, sous forme
    de dictionnaire, dans le champ `attributes`. Le contenu est mis dans le champ `values` (qui n'existe pas
    si le tag n'a pas de contenu).

    """
    @classmethod
    def print_result(cls, gaia_object: dict) -> None:
        """
        Affichage de l'arbre XML lu.

        Parameters
        ----------
        gaia_object : dict
            dictionnaire correspondant au fichier lu.
        """
        # for (key, value) in gaia_object.items():
        #     if isinstance(value, dict):
        #         print(f'# {key}')
        #         for (key_intern, value_intern) in value.items():
        #             print(f'{key_intern}\t{value_intern}')
        #     elif isinstance(value, list):
        #         for e in value:
        #             print(f'\t{e}')
        pprint.pprint(gaia_object)

    @classmethod
    def __convert_reals(cls, str_values: str) -> Union[List, np.array]:
        """
        Conversion d'une suite de nombres réels en tableau numpy.

        Parameters
        ----------
        str_values : str

        Returns
        -------
        np.ndarray
            tableau numpy
        """
        tmp_values = [entry.strip() for entry in str_values.strip().split(' ')]
        tmp_values = [entry for entry in tmp_values if len(entry) > 0]
        try:
            first_value = eval(tmp_values[0])
            if len(tmp_values) > 0 and isinstance(first_value, float):
                return np.array([eval(v) for v in tmp_values])
            return tmp_values
        except NameError:
            raise TypeError('_convert_reals : une seule valeur ou premier valeur non-rééelle.')

    @classmethod
    def __convert_values(cls, str_values: str) -> Union[None, List, np.array]:
        try:
            return cls.__convert_reals(str_values)
        except TypeError as e:
            nem_tools.autolog_error(f'Problème lors de la conversion des valeurs : {e}')
            return None

    @classmethod
    def __read_ensemble_de_points(cls, root_: ElT) -> dict:
        """
        Lecture et conversion du tag EnsembleDePoints.

        ``EnsembleDePoints``
        * Attibuts :
            * ``Nom``
            * ``NombreDeDimensions`` (n)
        * Sous-tags (sous-éléments)
            * *n* ``EnsembleDePoints.Dimension``
                * Attributs :
                    * Numéro
                    * Grandeur
                    * Unité
            * 1 ``EnsembleDePoints.Data``.
                * Attributs :
                    * NombreDeValeurs
                * Contenu :
                    * les valeurs (NombreDeDimensions * NombreDeValeurs)

        Parameters
        ----------
        root_ : xml.etree.ElementTree.Element
            Element de type EnsembleDePoints

        Returns
        -------
        dict
            Les données convertis si nécessaire (i.e. pas que des string).

        Warnings
        --------
        Possibilité de brancher la lecture du tag EnsembleDePoints dans ReadGaiaFile.__read_common_tag
        """
        result = dict(attributes=root_.attrib.copy())
        r_attrib = result['attributes']
        for (key, value) in r_attrib.items():
            if 'Nombre' in key or 'Numero' in key:
                r_attrib[key] = eval(value)
        result['EnsembleDePoints.Dimension'] = root_.findall('EnsembleDePoints.Dimension')
        e_dim = result['EnsembleDePoints.Dimension']
        for i, element in enumerate(e_dim):
            e_dim[i] = dict(element.attrib.copy())
            for (key_dim, value_dim) in element.items():
                if 'Nombre' in key_dim or 'Numero' in key_dim:
                    e_dim[i][key_dim] = eval(value_dim)
        e_data = root_.find('EnsembleDePoints.Data')
        r_data = result['EnsembleDePoints.Data'] = dict(attributes=e_data.attrib.copy())
        for (key_data_attr, value_data_attr) in r_data.items():
            for (key_data, value_data) in value_data_attr.items():
                if 'Nombre' in key_data or 'Numero' in key_data:
                    value_data_attr[key_data] = eval(value_data)
        n_val = result['EnsembleDePoints.Data']['attributes']['NombreDeValeurs']
        n_dim = result['attributes']['NombreDeDimensions']
        rough_data = cls.__convert_reals(e_data.text)
        # Les données sont ordonnées comme en Fortran et non comme en C
        r_data['values'] = rough_data.reshape((n_dim, n_val), order='F')
        return result

    @classmethod
    def __read_common_tag(cls, root_: ElT) -> dict:
        """
        Lecture et conversion du tag Maillage1D, Champ.

        ``Maillage1D``
        * Attibuts :
            * ``Nom``
            * ``Grandeur``
            * ``Unite``
        * Sous-tags (sous-éléments)
            * 1 ``Data``.
                * Attributs :
                    * NombreDeValeurs
                * Contenu :
                    * les valeurs (NombreDeDimensions * NombreDeValeurs)

        ``Champ``
        * Attibuts :
            * ``Nom``
            * ``Grandeur``
            * ``Unite``
        * Sous-tags (sous-éléments)
            * 1 ``Support1D``
                * Attributs :
                    * Nom
                    * Grandeur
                    * Définition
            * 1 ``Data``.
                * Attributs :
                    * NombreDeValeurs
                * Contenu :
                    * les valeurs (NombreDeDimensions * NombreDeValeurs)
            OU
            * 1 ``SupportRectilineaire``
                * Attributs : NombreDeDimensions = ndim
                * ndim ``SupportRectilineaire.Dimension``
                    * Attributs :
                        * Numero
                        * Nom
                        * Grandeur
                        * Definition

        Parameters
        ----------
        root_ : xml.etree.ElementTree.Element
            Element de type Maillage1D, Champ, …

        Returns
        -------
        dict
            Les données convertis si nécessaire (i.e. pas que des string).
        """
        result = dict(attributes=root_.attrib.copy())
        r_attrib = result['attributes']
        for (key, value) in r_attrib.items():
            if 'Nombre' in key or 'Numero' in key:
                r_attrib[key] = eval(value)
        for child in root_:
            result[child.tag] = dict(attributes=child.attrib.copy())
            # Conversion des valeurs des attributs du tag
            e_tag = result[child.tag]['attributes']
            for (key, value) in e_tag.items():
                if 'Nombre' in key or 'Numero' in key:
                    e_tag[key] = eval(value)
            # Conversion du contenu du tag si il y en a un — le strip est important car contient parfois juste '\n\t'
            if child.text:
                # child.text peut ne contenir que des \n et \t, du coup, le split de __convert_reals ne fonctionne pas
                stripped_child_text = child.text.strip()
                if stripped_child_text:
                    result[child.tag]['values'] = cls.__convert_reals(stripped_child_text)
        return result

    @classmethod
    def read_gaia_file(cls, name: str) -> dict:
        """
        Données dans le fichier name

        Parameters
        ----------
        name : str
            os.PathLike

        Returns
        -------
        dict
        """
        file_tree = ElT.parse(name)
        root = file_tree.getroot()
        root_result = {}
        for child in root:
            if child.tag not in root_result:
                root_result[child.tag] = []
            current_block = root_result[child.tag]
            if child.tag == 'EnsembleDePoints':
                current_block.append(cls.__read_ensemble_de_points(child))
            elif child.tag == 'Maillage1D':
                current_block.append(cls.__read_common_tag(child))
            elif child.tag == 'Champ':
                current_block.append(cls.__read_common_tag(child))
        return root_result
