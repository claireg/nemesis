# -*- coding: utf-8 -*-

"""
Chargement de données depuis un fichier .psy

courbe3d.psy
"""

import os
from typing import List

from .utilities import get_data_dir
from .psyreader import ReadPsyFile
from .psycommon import PsyCurveData
from .psyutilities import get_curves_from_psy_file


def print_psy_several_curves() -> None:
    data_path = os.path.join(get_data_dir(), 'plusieurs_courbes.psy')
    psy_file = ReadPsyFile.read_psy_file(data_path)
    print('### several_curves')
    ReadPsyFile.print_result(psy_file)


def print_psy_several_networks() -> None:
    data_path = os.path.join(get_data_dir(), 'plusieurs_reseaux.psy')
    psy_file = ReadPsyFile.read_psy_file(data_path)
    print('### several_networks')
    ReadPsyFile.print_result(psy_file)


def print_psy_vectors() -> None:
    data_path = os.path.join(get_data_dir(), 'vecteurs.psy')
    psy_file = ReadPsyFile.read_psy_file(data_path)
    print('### vectors')
    ReadPsyFile.print_result(psy_file)


def print_psy_wave() -> None:
    data_path = os.path.join(get_data_dir(), 'onde.psy')
    psy_file = ReadPsyFile.read_psy_file(data_path)
    print('### onde')
    ReadPsyFile.print_result(psy_file)


def print_psy_curve_3d() -> None:
    data_path = os.path.join(get_data_dir(), 'courbe3d.psy')
    psy_file = ReadPsyFile.read_psy_file(data_path)
    print('### courbe3d')
    ReadPsyFile.print_result(psy_file)


def psy_hedgehog() -> List[PsyCurveData]:
    """
    Lecture du fichier de données herisson.psy.

    Returns
    -------
    List[PsyCurveData]
        PsyCurve (dataclass)
            .data List[np.array]
                Un tableau par dimension (X, Y [, Z])
            .colors List[int]
                Un tableau des indices des couleurs
            .name str
                Nom de la collection ayant permis d'extraire les données

    """
    data_path = os.path.join(get_data_dir(), 'herisson.psy')
    return get_curves_from_psy_file(data_path)
