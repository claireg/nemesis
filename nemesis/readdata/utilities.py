# -*- coding: utf-8 -*-

"""
Ensemble de fonctionnalités pour la lecture de données.

"""

import os
import numpy as np

anonym_path = os.path.join('path', 'to')


# noinspection SpellCheckingInspection
def narray_prop(narray: np.array) -> str:
    """
        Retourne de la taille et la forme d'un tableau numpy.

        Parameters
        ----------
        narray : numpy.ndarray
            le tableau pour lequel on souhaite les prop.

        Returns
        -------
        str
            "narray.size\tnarray.shape"

        Examples
        --------
        >>> t1 = np.arange(5)
        >>> print(narray_prop(t1)) # doctest: +NORMALIZE_WHITESPACE
        5   (5,)
        >>> t2 = np.arange(4).reshape(2, 2)
        >>> print(narray_prop(t2)) # doctest: +NORMALIZE_WHITESPACE
        4    (2, 2)
        """
    return '{size_}\t{shape}'.format(size_=narray.size, shape=narray.shape)


def get_data_dir() -> str:
    """
    Retourne le répertoire contenant les données ouvertes du projet.

    Returns
    -------
    path
        (os.PathLike) chemin vers nemesis/data (i.e. ../data en chemin normalisé absolu)
    """
    # Le test est fait avec pytest pour ne pas tester les montages disques
    current_dir = os.path.dirname(__file__)
    return os.path.normpath(os.path.join(current_dir, '..', '..', 'data'))


def get_intern_data_dir() -> str:
    """
    Retourne le répertoire contenant les données non-ouvertes du projet.

    Returns
    -------
    path
        (os.PathLike) chemin vers nemesis/data (i.e. ../data/intern en chemin normalisé absolu)
    """
    # Le test est fait avec pytest pour ne pas tester les montages disques
    current_dir = os.path.dirname(__file__)
    return os.path.normpath(os.path.join(current_dir, '..', '..', 'data', 'intern'))


def get_ref_data_dir() -> str:
    """
    Retourne le répertoire contenant les données de reference de tests du projet.

    Returns
    -------
    path
        (os.PathLike) chemin vers nemesis/data (i.e. ../data/reference_data en chemin normalisé absolu)
    """
    # Le test est fait avec pytest pour ne pas tester les montages disques
    current_dir = os.path.dirname(__file__)
    return os.path.normpath(os.path.join(current_dir, '..', '..', 'data', 'intern', 'reference_data'))


def path_anonymizer(filepath: str) -> str:
    """
    Anonymisation d'un chemin (pour ne pas stocker la structure des montages CEA)

    Parameters
    ----------
    filepath    : str
        (os.PathLike) Chemin à transformer

    Returns
    -------
    str (os.PathLike)
        'path/to/nom_fichier.ext' si le dernier terme est nom_fichier.ext et que le fichier désiqné n'est pas
        dans les données du projet Nemesis, sinon 'path/to/data[/intern]/nom_fichier.ext'
    """
    if 'data' in filepath:
        idata = filepath.find('data')
        return os.path.join(anonym_path, filepath[idata:])
    else:
        basename = os.path.basename(filepath)
        return os.path.join(anonym_path, basename)


def compute_section_efficace(x_origin: np.array, y_origin: np.array) -> (np.array, np.array):
    new_size = x_origin.size + (x_origin.size // 2)
    x_new = np.empty(new_size)
    y_new = np.empty(new_size)
    for i in range(x_new.size):
        i_origin = i // 2
        if i % 2 == 0:
            x_new[i] = x_origin[i_origin]
            y_new[i] = y_origin[i_origin]
        else:
            x_new[i] = x_origin[i_origin+1]
            y_new[i] = y_new[i-1]
    return x_new, y_new
