# -*- coding: utf-8 -*-

"""
Lecture d'un fichier .psy.

Accès aux données lues.

"""
import os
import re
from typing import List, Any

import numpy as np
from .utilities import path_anonymizer
from .psycommon import (
    psy_object_generic,
    PsyEnum,
    PsyObject,
)

import nemesis.tools as nem_tools


class ReadPsyFile(object):
    """
    Lecture d'un fichier Psyché au format *psy*.

    """

    __current_block_attributes = None

    @classmethod
    def __print_psy_type_dictionnary(cls, di: dict, level: int = 0) -> None:
        sep = "\t" * level
        for (key, value) in di.items():
            if isinstance(value, dict):
                sep_title = '# ' if level == 0 else ''
                print('\t\t\t{sep}{sep_title}{key}'.format(sep=sep, sep_title=sep_title, key=key))
                cls.__print_psy_type_dictionnary(value, level + 1)
            else:
                type_ = str(type(value))
                if isinstance(value, np.ndarray):
                    if value.size > 10:
                        n_values = value.size
                        value_begin = [str(e) for e in value[:3]]
                        i_end = value.size - 3
                        value_end = [str(e) for e in value[i_end:]]
                        value = '[{begin}, ..., {end}]'.format(begin=', '.join(value_begin),
                                                               end=', '.join(value_end))
                        print('\t\t\t{sep}{key} ({size}): {value}\t{type_}'.format(sep=sep, key=key,
                                                                                   size=n_values, value=value,
                                                                                   type_=type_))
                    else:
                        print('\t\t\t{sep}{key}: {value}\t{type_}'.format(sep=sep, key=key, value=value,
                                                                          type_=type_))
                else:
                    if isinstance(value, list):
                        if len(value) == 0:
                            type_ += ' <empty>'
                        else:
                            type_ += ' of ' + str(type(value[0]))
                    print('\t\t\t{sep}{key}: {value}\t{type_}'.format(sep=sep, key=key, value=value,
                                                                      type_=type_))

    @classmethod
    def __print_psy_type(cls, psy_type_object: dict) -> None:
        print('\t\t# {}'.format(psy_type_object['name']))
        cls.__print_psy_type_dictionnary(psy_type_object['attributes'])

    @classmethod
    def print_result(cls, psy_object: dict) -> None:
        """
        Affichage complet du dictionnaire lu (clés + valeurs).

        Appel récursif.

        Parameters
        ----------
        psy_object: dict
            dictionnaire correspondant au fichier psyché lu.
        """
        for (key, value) in psy_object.items():
            if key not in ['name', 'filepath']:
                # Liste de dictionnaires pour toutes les clés qui ne s'appelent pas 'name' dans
                #  le dictionnaire de premier niveau
                print('\t# {key}'.format(key=key))
                for entry in value:
                    cls.__print_psy_type(entry)
            else:
                if key == 'name':
                    print('# Nom Fichier : {name}'.format(name=value))
                else:
                    print('# Chemin Fichier : {name}'.format(name=value))

    @classmethod
    def print_short_result(cls, psy_object: dict, with_chemin: bool = False) -> None:
        """
        Affichage réduit du dictionnaire lu (juste les noms des clés, et leur type psyché).

        Parameters
        ----------
        psy_object : dict
            Dictionnaire correspondant au fichier psyché lu.

        with_chemin:
            Indique si le chemin complet du fichier sera imprimé en plus du nom du fichier.
        """
        for (key, value) in psy_object.items():
            if key not in ['name', 'filepath']:
                # Liste de dictionnaires pour toutes les clés qui ne s'appelent pas 'name' ou 'filepath' dans
                #  le dictionnaire de premier niveau
                # print('\t# {key}'.format(key=key))
                for entry in value:
                    current_name = entry.get('name', '')
                    print('\t\t# {key}: {name}'.format(key=key, name=current_name))
            else:
                if key == 'name':
                    print('# Nom du fichier : {name}'.format(name=value))
                elif with_chemin:
                    print('# Chemin Fichier : {name}'.format(name=value))

    @classmethod
    def __convert_values(cls, str_values: str) -> Any:
        """
        Conversion des données lues dans le fichier psyché en type Python.

        Parameters
        ----------
        str_values : str
            Données sous formes de string à convertir en type Python ou numpy

        Returns
        -------
        object
            ``int``, ``float``, ``list``, ``numpy.ndarray``, ``PsyEnum``, ``PsyObject``, … : objet python
            correspondant à ``str_values``.
        """
        # Call only for attributes value
        try:
            values = eval(str_values)
            if isinstance(values, list):
                if len(values) > 0 and isinstance(values[0], float):
                    return np.array(values)
            return values
        except NameError:
            # Le r est important pour dire que c'est une raw-string, sinon
            # Python affiche un warning : «Invalid escape sequence»
            pattern = re.compile(r"\[*\]")
            if str_values.isupper() and pattern.search(str_values) is None:
                return PsyEnum(str_values)
            else:
                # Can be [Onde_x, Onde_y] : a list of PsyObject
                if str_values.startswith('[') and str_values.endswith(']'):
                    n_comma = str_values.find(',')
                    short_str_values = str_values[1:-1]
                    if n_comma == -1:
                        return [PsyObject(short_str_values.strip())]
                    else:
                        entries = short_str_values.split(',')
                        return [PsyObject(entry.strip()) for entry in entries]
                else:
                    return PsyObject(str_values)

    @classmethod
    def __get_sub_current_block(cls, current_block: dict, key: str) -> Any:
        if key not in current_block:
            current_block[key] = dict()
        return current_block[key]

    @classmethod
    def __insert_sub_attribute(cls, current_block: dict, key: str, values: str) -> None:
        sub_keys = key.split('.')
        if isinstance(sub_keys, list):
            current_key = sub_keys[0]
            sub_current_block = cls.__get_sub_current_block(current_block, current_key)
            if len(sub_keys) > 2:
                new_sub_keys = key[key.find('.') + 1:]
                cls.__insert_sub_attribute(sub_current_block, new_sub_keys, values)
            else:
                sub_current_block = cls.__get_sub_current_block(current_block, current_key)
                current_key = sub_keys[1]
                sub_current_block[current_key] = cls.__convert_values(values)
        else:
            current_key = sub_keys
            current_block[current_key] = cls.__convert_values(values)

    @classmethod
    def __insert_in_psy_file_dictionary(cls, current_block: dict, key: str, values: str) -> None:
        if '.' in key:
            cls.__insert_sub_attribute(current_block, key, values)
        else:
            current_block[key] = cls.__convert_values(values)

    @classmethod
    def read_psy_file(cls, name: str) -> dict:
        """Création d'un dictionnaire stockant les données lues dans un fichier .psy.
        Le dictionnaire retourné est de la forme ::

            {
                'name' = <string_nom_du_fichier_lu_sans_point_psy>,
                'psy_type_0' =
                    [
                        {
                            'name' = <string_nom_objet>,
                            'attributes' = { … }
                        },
                        {
                            'name' = <string_nom_objet>,
                            'attributes' = { … }
                        },
                        …
                        ],
                'psy_type_1' =
                    [
                        {
                            'name' = <string_nom_objet>,
                            'attributes' = { … }
                        },
                        {
                            'name' = <string_nom_objet>,
                            'attributes' = { … }
                        },
                        …
                        ]
            }

        Parameters
        ----------
        name: str
            nom complet du fichier à ouvrir.

        Returns
        -------
        dict
            Dictionnaire représentant les données Psyché.

        """
        psy_name = os.path.basename(name).split('.')[0]
        filepath = path_anonymizer(name)
        psy_object = {'name': psy_name, 'filepath': filepath}
        if not os.path.isfile(name):
            raise RuntimeError(f'ERREUR le fichier {name} n existe pas')
        with open(name, 'r', encoding='utf-8') as f:
            block_type = None
            begin_block = False
            previous_line = None
            multi_line = False
            multi_line_values = None
            multi_line_key = None
            current_block = None
            try:
                for i, line in enumerate(f):
                    if not line.isspace():
                        line = line.strip()
                        # Block detection
                        if '{' in line:
                            begin_block = True
                        if '}' in line:
                            block_name = line.split('}')[1].strip()
                            current_block['name'] = block_name
                            block_type = None
                            begin_block = False
                            current_block = None
                            __current_block_attributes = None
                        # Block management
                        if begin_block is True:
                            if block_type is None:
                                # a block begins
                                block_type = previous_line.strip()
                                if block_type in psy_object:
                                    psy_object[block_type].append(dict(name=None, attributes=dict()))
                                else:
                                    psy_object[block_type] = [dict(name=None, attributes=dict())]
                                current_block = psy_object[block_type][-1]
                                __current_block_attributes = current_block.get('attributes')
                            elif '=' in line:
                                # attribute declares on the current line — line could have several '='
                                key, values = line.split('=', maxsplit=1)
                                key = key.strip()
                                values = values.strip()
                                if '[' in values and ']' in values:
                                    cls.__insert_in_psy_file_dictionary(__current_block_attributes, key, values)
                                elif '[' in values and ']' not in values:
                                    # begin [
                                    multi_line = True
                                    multi_line_key = key
                                    multi_line_values = values
                                else:
                                    cls.__insert_in_psy_file_dictionary(__current_block_attributes, key, values)
                            elif multi_line is True:
                                # a multi-line attribute
                                if ']' in line:
                                    # end [
                                    multi_line = False
                                    multi_line_values += line
                                    cls.__insert_in_psy_file_dictionary(__current_block_attributes, multi_line_key,
                                                                        multi_line_values)
                                else:
                                    # in []
                                    multi_line_values += line
                        previous_line = line
            except UnicodeDecodeError as e:
                nem_tools.autolog_error('Erreur décodage caratère dans le fichier {name} : ln {i} : {line}')
                raise RuntimeError(f'{e}')

        return psy_object

    @classmethod
    def get_object_generic(cls, psy_name: str) -> List[str]:
        """
        Retourne la liste des objets dont le type est passé en argument.

        Parameters
        ----------
        psy_name :
            Nom de l'objet générique.

        Returns
        -------
        List[str] :
            La liste des objets de ce type.
        """
        return psy_object_generic.get(psy_name)
