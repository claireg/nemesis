# -*- coding: utf-8 -*-

"""
Chargement de données depuis un fichier .xml

"""

import os
import numpy as np
from typing import NewType, Tuple, List

from .gaiareader import ReadGaiaFile

NpArray = NewType('NpArray', np.array)


def gaia_points_data(data_path: str, filename: str) -> List[NpArray]:
    """
    Lecture d'un ensemble de points.

    Parameters
    ----------
    data_path  : str
        (os.pathLike) Répertoire dans lequel se trouve le fichier filename
    filename   : str
        Nom du fichier à lire

    Returns
    -------
    [np.array]  Valeurs des ensembles de points trouvés

    """
    gaia_file = ReadGaiaFile.read_gaia_file(os.path.join(data_path, filename))
    yi = []
    for i in range(len(gaia_file["EnsembleDePoints"])):
        yi.append(gaia_file['EnsembleDePoints'][i]['EnsembleDePoints.Data']['values'])
    return yi


def gaia_one_mesh1d_data(data_path: str, filename: str) -> Tuple[NpArray, NpArray]:
    """
    Lecture d'un ``Maillage1D`` et d'un ``Champ``

    Parameters
    ----------
    data_path   : str
        (os.pathLike) Répertoire dans lequel se trouve le fichier filename
    filename    : str
        Nom du fichier à lire

    Returns
    -------
    (np.array, np.array)
        0 : premier maillage1d (tableau 2D)
        1 : table de taille m1.size*(m2.size-1) ou m1.size*m2.size (les champs)
    """
    gaia_file = ReadGaiaFile.read_gaia_file(os.path.join(data_path, filename))
    maillage1d = gaia_file['Maillage1D'][0]
    champ = gaia_file['Champ'][0]
    x = maillage1d['Data']['values']
    y = champ['Data']['values']
    if champ['Support1D']['attributes']['Definition'] == 'MAILLES':
        # Il faut ajouter un point à y
        np.append(y, y[-1])
    return x, y


def gaia_network_data(data_path: str, filename: str) -> (np.array, np.array, [np.array]):
    """
    Lecture de deux ``Maillage1D`` et de n ``Champ``

    Parameters
    ----------
    data_path   : str
        (os.pathLike) Répertoire dans lequel se trouve le fichier filename
    filename    : str
        Nom du fichier à lire

    Returns
    -------
    (np.array, np.array, [np.array])
        0 : premier maillage1d (tableau 2D)
        1 : deuxième maillage1d (tableau 2D)
        2 : liste de tables de taille m1.size*(m2.size-1) ou m1.size*m2.size (les champs)
    """
    gaia_file = ReadGaiaFile.read_gaia_file(os.path.join(data_path, filename))
    m1 = gaia_file['Maillage1D'][0]
    m2 = gaia_file['Maillage1D'][1]
    x1 = m1['Data']['values']
    x2 = m2['Data']['values']
    n_x1 = m1['Data']['attributes']['NombreDeValeurs']
    n_x2 = m2['Data']['attributes']['NombreDeValeurs']
    yi = []
    for i in range(len(gaia_file["Champ"])):
        champ = gaia_file['Champ'][i]
        tmp_y = champ['Data']['values']
        yi.append(tmp_y.reshape((n_x1, n_x2), order='F'))
    return x1, x2, yi


def gaia_map_data(data_path: str, filename: str = None) -> (np.array, np.array, np.array):
    """
    Lecteur d'un fichier de données représentant une map (3D Plan)

    Parameters
    ----------
    data_path        : str
        (os.PathLike)   Chemin vers le fichier
    filename    : str
        Nom du fichier à lire

    Returns
    -------
    3-uplets de numpy.ndarray
        0 : premier maillage1d (tableau 2D)
        1 : deuxième maillage1d (tableau 2D)
        2 : table de taille m1.size*(m2.size-1) ou m1.size*m2.size
    """
    if filename is not None:
        full_path = os.path.join(data_path, filename)
    else:
        full_path = data_path
    print(f'## Nom du fichier à lire : {full_path}')
    gaia_file = ReadGaiaFile.read_gaia_file(full_path)
    m1 = gaia_file['Maillage1D'][0]
    m2 = gaia_file['Maillage1D'][1]
    m3 = gaia_file['Champ'][0]
    x = m1['Data']['values']
    y = m2['Data']['values']
    n_x = m1['Data']['attributes']['NombreDeValeurs']
    n_y = m2['Data']['attributes']['NombreDeValeurs']
    n_z = m3['Data']['attributes']['NombreDeValeurs']
    # Cas où il y a des données aux nœuds, et d'autres aux mailles, il faut couper les données
    if n_z == (n_x * (n_y - 1)):
        y = y[:-1]
    z = m3['Data']['values']
    yi, xi = np.meshgrid(x, y)
    z = z.reshape(xi.shape, order='F')
    # Il ne faut pas manipuler les données juste après la lecture, l'utilisateur doit être conscient
    # lorsque ses données sont manipulées
    # zi = np.ma.masked_where(z <= 1.e-300, z)
    return xi, yi, z
