# -*- coding: utf-8 -*-

"""
Chargement de données depuis un fichier .xml

"""

import numpy as np
from typing import NewType, Tuple, List

from .utilities import (
    get_data_dir,
    get_ref_data_dir,
    compute_section_efficace
)
from .gaia_utilities import (
    gaia_points_data,
    gaia_one_mesh1d_data,
    gaia_map_data
)

NpArray = NewType('NpArray', np.array)


def gaia_curve_data() -> List[NpArray]:
    """
    Lecture des données du fichier Gaia curve.xml.

    Réseau de courbes ?

    Returns
    -------
    np.ndarray
        de dimension 2 (19*10)

    Notes
    -----
    * Peut-être faut-il changer le nom de la fonction.
    """
    [y, ] = gaia_points_data(get_data_dir(), 'curve.xml')
    return y


def gaia_stairs_curve_data() -> Tuple[NpArray, NpArray]:
    """
    Lecture des donnbes du fichier Gaia courbe_en_escalier.xml.

    Ici il y a qu'un seul champ et un seul maillage1d

    Returns
    -------
    np.ndarray
        Les abscisses
    np.ndarray
        Les ordonnées
    """
    x, y = gaia_one_mesh1d_data(get_data_dir(), 'courbe_en_escalier.xml')
    return x, y


def gaia_section_efficace() -> (np.array, np.array):
    """
    Lecture des données du fichier Gaia courbe_en_escalier.xml, et conversion en «marche d'escalier».

    Returns
    -------
    np.ndarray
        Les abscisses
    np.ndarray
        Les ordonnées
    """
    x_origin, y_origin = gaia_stairs_curve_data()
    return compute_section_efficace(x_origin, y_origin)


def gaia_isolines_levels() -> (np.array, np.array, np.array):
    """
    Deux ``Maillage1D`` et un ``Champ``.

    Permet de tester (carte + iso + colorbar) en demandant un niveau à 0. alors qu'on veut un mapping log

    Returns
    -------
    numpy.ndarray tuple (3 items)
        0 : premier maillage1d (54 : tableau 2D)
        1 : deuxième maillage1d (1671 : tableau 2D)
        2 : table de taille m1.size*(m2.size-1) (90180)

    Warnings
    --------

    Le second maillage a été tronqué car le premier a des données aux mailles,
    et le second des données aux nœuds.

    Ce 3ème tableau contient des données "nulles" (1.e-300) auxquelles on ne peut
    appliquer de log10, il faut donc les masquer si on veut afficher ses données.

    """
    xi, yi, zi = gaia_map_data(get_data_dir(), 'spectre_planck.xml')
    return xi, yi, zi

