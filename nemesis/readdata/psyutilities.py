# -*- coding: utf-8 -*-

"""
Utilitaires pour le chargement de données .psy.
"""

import os
from typing import Tuple, NewType, List, Union
import warnings

import numpy as np

from .utilities import get_data_dir, get_intern_data_dir
from .psyreader import ReadPsyFile
from .psycommon import (
    levels,
    hierarchy,
    PsyEnum,
    PsyObject,
    PsyCurveData,
    PsyStdTextData,
    NemPsyTypes,
    psy_alignment,
    psy_object_generic,
)

NpArray = NewType('NpArray', np.array)


class AnalyzePsyDict(object):
    def __init__(self, psy_dict):
        self.psy_dict = psy_dict

    @staticmethod
    def get_token_data(psy_name: str, index: int, subindex: int) -> (str, str):
        """
        Dans la hiérarchie des objets Psyché, retourne l'attribut de celui qui est passé en argument et la liste des
        suivants de même niveau.

        Parameters
        ----------
        psy_name :
            Nom du type de l'objet dans la hiérarchie.
        index :
            ??
        subindex :
            ??

        Returns
        -------
        str, str :
            Nom de l'attribut suivant dans la hiérarchie et la liste des types suivants.
        """
        str_name = psy_name
        if isinstance(psy_name, (PsyEnum, PsyObject)):
            str_name = psy_name.name
        token = hierarchy[str_name][index]
        for i, item in enumerate(token.items()):
            if i == subindex:
                return item[0], item[1]

    @staticmethod
    def get_number_of_tokens(psy_name: str) -> int:
        """
        Retourne le nombre d'objets existant dans le type passé en argument.

        Parameters
        ----------
        psy_name
            Nom de l'objet générique.

        Returns
        -------
        int :
            Le nombre d'objets de ce type.
        """
        str_name = psy_name
        if isinstance(psy_name, (PsyEnum, PsyObject)):
            str_name = psy_name.name
        return len(hierarchy[str_name])

    def get_top_level(self) -> str:
        for level in levels:
            if level in self.psy_dict:
                return level
        filename = self.psy_dict['filepath']
        raise RuntimeError(f'Fichier mal formé : pas d objets ScreenViewer, Page, Text, Scene ou Legend '
                           f'dans {filename}')

    def get_psy_object(self, psy_type: NemPsyTypes, name: str) -> Union[dict, None]:
        """
        Retourne les propriétés de l'objet de type psy_type et de nom name.

        Parameters
        ----------
        psy_type :
            Type de l'objet recherché
        name : str
            Nom de l'objet recherché

        Returns
        -------
        dict ou None
            Les propriétés (attributs) de l'objet trouvé. None si rien trouvé.
        """
        str_name = name
        if isinstance(name, (PsyEnum, PsyObject)):
            str_name = name.name
        # print('\n# Recherche: \t{type_}: {name}'.format(name=str_name, type_=psy_type))
        type_object_list = self.psy_dict.get(psy_type, None)
        # type_object_list est obligatoirement une liste ou None
        if type_object_list is not None:
            if isinstance(type_object_list, list):
                for psy_obj in type_object_list:
                    # print('\n\nN={n}\nT={t}'.format(n=str_name, t=psy_obj['name']))
                    if psy_obj['name'] == str_name:
                        return psy_obj['attributes']
            else:
                type_ = type(type_object_list)
                print(f'\t-> La valeur associée à la clé n est pas une liste (type={type_})')
                print(f'\tObjet recherché : {name} ({psy_type})')
                return None
        else:
            # L'objet de type `psy_type` et de nom `name` n'existe pas
            # Les print ne sont utiles que pour le debug ou la mise en place du reader
            # print(f'\t-> Objet inconnu : {name} ({psy_type})')
            # self.print_short_result(psy_dict)
            return None

    def get_type_object(self, psy_name: NemPsyTypes) -> Union[str, None]:
        """
        Retourne le type psyché d'un objet dont le nom est passé en argument.

        Parameters
        ----------
        psy_name :
            Nom Psyché de l'objet

        Returns
        -------
        str ou None :
            Type de l'objet recherché ou None si rien trouvé.
        """
        # Il peut y avoir plusieurs objets de même nom, la fonction s'arrête sur le premier qui n'est pas
        # obligatoirement le bon.
        for key, value in self.psy_dict.items():
            if isinstance(value, list):
                # element is a dictionary
                for element in value:
                    if 'name' in element.keys():
                        e_name = element['name']
                        if isinstance(psy_name, PsyObject):
                            if e_name == psy_name.name:
                                return key
                        else:
                            if e_name == psy_name:
                                return key

        filename = self.psy_dict['filepath']
        warnings.warn(f'Objet inexistant dans {filename}')
        return None

    def psy_collection(self, psy_collection_name: str, psy_name: str) -> (List[str], List[str]):
        """
        Retourne la liste des noms des objets graphiques appartenant à une collection donnée en argument.

        Parameters
        ----------
        psy_collection_name :
            Nom Psyché du type d'objet (ici 'Collection').
        psy_name :
            Nom de l'objet.

        Returns
        -------
        List[str], List[str] :
            Liste des objets contenu dans la collection et liste des types suivants.
        """
        collection = self.get_psy_object(psy_collection_name, psy_name)
        # next_psy_type = ['DepthCurve', 'StandardCurve']
        attr_graphic, next_psy_type = self.get_token_data(psy_collection_name, 0, 0)
        return collection['graphic'], next_psy_type

    def get_curves_from_collection(self, page_name: str, collection_name: str, psy_name: str) -> \
            PsyCurveData:
        """
        Récupère les données associées à une fonction graphique definie dans un fichier psyché

        Parameters
        ----------
        page_name :
            Nom de la page psyché.
        collection_name :
            Nom de la collection.
        psy_name :
            Nom de la fonction graphique.

        Returns
        -------
        PsyCurveData :
            Tableau des données associées (Fonction et support).
        """
        if page_name == '':
            if len(self.psy_dict['Page']) == 1:
                page_name = self.psy_dict['Page'][0]['name']
            else:
                raise RuntimeError('Plusieurs pages dans le fichier, il faut préciser son nom.')
        graphics, next_psy_type = self.psy_collection(collection_name, psy_name)
        # ICI : Liste de DepthCurve dans le cas du hérisson
        # 1 DepthCurve -> Function = 1 ScatteredSupport + 1 FloatValue (-> ListFloatData)
        p_data = PsyCurveData(xlabel='', xunit='', ylabel='', yunit='', zlabel='', zunit='',
                              data=[], colors=[], mark=False, height=False, name=psy_name)

        for graphic_type in next_psy_type:
            for elt in graphics:
                curve = self.get_psy_object(graphic_type, elt)
                if curve is not None:
                    if graphic_type == 'DepthCurve' and curve is not None:
                        color, p_data.mark, p_data.height, x_tab, y_tab, z_tab =\
                            self.psy_depth_curve(graphic_type, elt)
                        x_lbl, x_unit, p_data.zlabel, p_data.zunit = self.psy_curve_axes_label(elt)

                        p_data.xlabel = x_lbl[0]
                        p_data.xunit = x_unit[0]
                        p_data.ylabel = x_lbl[1]
                        p_data.yunit = x_unit[1]
                        p_data.data.append((x_tab, y_tab, z_tab))
                        p_data.colors.append(color)
                    elif graphic_type == 'StandardCurve' and curve is not None:
                        color, p_data.mark, (x_tab, y_tab) = self.psy_std_curve(graphic_type, curve)
                        x_lbl, x_unit, p_data.ylabel, p_data.yunit = self.psy_curve_axes_label(elt)
                        p_data.xlabel = x_lbl[0]
                        p_data.xunit = x_unit[0]
                        p_data.data.append((x_tab, y_tab))
                        p_data.colors.append(color)
                    elif graphic_type == 'SetOfCurve' and curve is not None:
                        color, (x_tab, y_tab) = self.psy_net_curve(graphic_type, curve)
                        x_lbl, x_unit, p_data.ylabel, p_data.yunit = self.psy_curve_axes_label(elt)
                        p_data.xlabel = x_lbl[0]
                        p_data.xunit = x_unit[0]
                        p_data.data.append((x_tab, y_tab))
                        p_data.colors.append(color)
                    elif graphic_type == 'VectorField' and curve is not None:
                        (x_tab, y_tab, u_tab, v_tab, h_tab, c_tab) = self.psy_vector_field(graphic_type, curve)
                        x_lbl, x_unit, p_data.zlabel, p_data.zunit = self.psy_curve_axes_label(curve['vectorFunction'])

                        p_data.data.append((x_tab, y_tab, u_tab, v_tab, h_tab, c_tab))
                        p_data.xlabel = x_lbl[0]
                        p_data.xunit = x_unit[0]
                        p_data.ylabel = x_lbl[1]
                        p_data.yunit = x_unit[1]
                    elif graphic_type == 'UnstructuredRaiseMap' and curve is not None:
                        (x_tab, y_tab, h_tab, c_tab, t_tab) = self.psy_unstruct_map(graphic_type, curve)
                        p_data.xlabel, p_data.xunit, p_data.ylabel, p_data.yunit, p_data.zlabel, p_data.zunit = \
                            self.psy_axes_label(page_name)
                        p_data.data.append((x_tab, y_tab, h_tab, c_tab, t_tab))
                    elif graphic_type == 'RaiseMap' and curve is not None:
                        (x_tab, y_tab, h_tab, c_tab) = self.psy_struct_map(graphic_type, curve)
                        p_data.xlabel, p_data.xunit, p_data.ylabel, p_data.yunit, p_data.zlabel, p_data.zunit = \
                            self.psy_axes_label(page_name)

                        p_data.data.append((x_tab, y_tab, h_tab, c_tab))
                    else:
                        print(f'/!\\ Type {graphic_type} non encore pris en compte')

        return p_data

    def psy_curve_axes_label(self, curve_name: str) -> (List[str], List[str], str, str):
        """
        Retourne les labels et unités de l'abcisse et de l'ordonnée de la courbe dont le nom est passé en argument

        Parameters
        ----------
        curve_name :
            Nom psyche de la courbe.

        Returns
        -------
        List[str], List[str], str, str :
            Label et Unités des axes de la courbe.
        """
        fonction = self.get_psy_object('Function', curve_name)
        flotvalue = self.get_psy_object('FloatValue', fonction['physicalValue'].name)
        xlabel = []
        xunit = []
        flabel = flotvalue['titleValue']
        funit = flotvalue['unit']['name']

        sup_list = ReadPsyFile.get_object_generic('Support')
        support_name = fonction['geometricalSupport'].name
        for sup_type in sup_list:
            sup_value = self.get_psy_object(sup_type, support_name)
            if sup_value is not None:
                for i in range(sup_value['componentNbr']):
                    sup_name = sup_value['coordinateValues'][i]
                    flotvalue = self.get_psy_object('FloatValue', sup_name)
                    xlabel.append(flotvalue['titleValue'])
                    xunit.append(flotvalue['unit']['name'])
                break

        return xlabel, xunit, flabel, funit

    def psy_legend(self, page_name: str) -> (List[str], str):
        """
        Retourne la liste des items de la legende ainsi que son titre général

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        List[str], str :
            Items et titre de la legende.
        """
        legend = []
        title = []
        frames, next_psy_type = self.psy_page('Page', page_name)
        for frame in frames:
            graphic_legend = self.get_psy_object('GraphicLegend', frame)
            if graphic_legend is not None:
                legend = graphic_legend['userItems']
                title = graphic_legend['legendTitle']
                break
        return legend, title

    def psy_struct_map(self, psy_std_name: str, mapdict: dict) -> \
            (np.array, np.array, np.array, np.array):
        """
        Retourne les données attachées à une représentation de type carte structurée.

        Parameters
        ----------
        psy_std_name :
            Type psyche de la représentation : RaiseMap
        mapdict :
            Dictionnaire associé à cet objet.

        Returns
        -------
        np.array, np.array, np.array, np.array :
            Tableau X, Y , élévation et coloration.

        """
        nb_token = self.get_number_of_tokens(psy_std_name)
        x_tab = y_tab = h_tab = c_tab = []
        for i in range(nb_token):
            attr_function, next_psy_type = self.get_token_data(psy_std_name, i, 0)
            psy_function_name = next_psy_type[0]
            function_name_obj = mapdict[attr_function]
            function_name = function_name_obj.name
            a1_tab, a2_tab, a3_tab = self.psy_function(psy_function_name, function_name)

            if attr_function == 'heightFunction':
                x_tab, y_tab = np.meshgrid(a1_tab, a2_tab)
                h_tab = a3_tab
            elif attr_function == 'colorFunction':
                c_tab = a3_tab

        return x_tab, y_tab, h_tab, c_tab

    def psy_unstruct_map(self, psy_std_name: str, mapdict: dict) -> \
            (np.array, np.array, np.array, np.array):
        """
        Retourne les données attachées à une représentation de type carte non structurée.

        Parameters
        ----------
        psy_std_name :
            Type psyche de la représentation : UnstructuredRaiseMap
        mapdict :
            Dictionnaire associé à cet objet.

        Returns
        -------
        np.array, np.array, np.array, np.array :
            Tableau X, Y , topologie, élévation et coloration.
        """
        nb_token = self.get_number_of_tokens(psy_std_name)
        x_tab = y_tab = h_tab = c_tab = t_tab = []
        for i in range(nb_token):
            attr_function, next_psy_type = self.get_token_data(psy_std_name, i, 0)
            psy_function_name = next_psy_type[0]
            function_name_obj = mapdict[attr_function]
            function_name = function_name_obj.name
            a1_tab, a2_tab, a3_tab, a4_tab = self.psy_function(psy_function_name, function_name)
            if attr_function == 'heightFunction':
                x_tab = a1_tab
                y_tab = a2_tab
                t_tab = a3_tab
                h_tab = a4_tab
            elif attr_function == 'colorFunction':
                c_tab = a4_tab
        return x_tab, y_tab, h_tab, c_tab, t_tab

    def psy_vector_field(self, psy_std_name, vec_field: dict) -> \
            (np.array, np.array, np.array, np.array, np.array):
        """
        Retourne les données attachées à une représentation de type champ de vecteur.

        Parameters
        ----------
        psy_std_name :
            Type psyche de la représentation : VectorField
        vec_field :
            Dictionnaire associé à cet objet.

        Returns
        -------
        np.array, np.array, np.array, np.array, np.array :
            Données associées au champ de vecteur X, Y U V élévation et couleur.
        """
        nb_token = self.get_number_of_tokens(psy_std_name)
        x_tab = y_tab = u_tab = v_tab = h_tab = c_tab = []
        for i in range(nb_token):
            attr_function, next_psy_type = self.get_token_data(psy_std_name, i, 0)
            psy_function_name = next_psy_type[0]
            function_name_obj = vec_field[attr_function]
            function_name = function_name_obj.name
            a1_tab, a2_tab, a3_tab = self.psy_function(psy_function_name, function_name)
            if attr_function == 'vectorFunction':
                x_tab = a1_tab
                y_tab = a2_tab
                u_tab = a3_tab[0::2]
                v_tab = a3_tab[1::2]
            elif attr_function == 'heightFunction':
                h_tab = a3_tab
            elif attr_function == 'colorFunction':
                c_tab = a3_tab
        return x_tab, y_tab, u_tab, v_tab, h_tab, c_tab

    def psy_net_curve(self, psy_std_name, net_curve: dict) -> (int, (np.array, np.array)):
        """
        Retourne les données attachées à une représentation de type réseaux de courbes.

        Parameters
        ----------
        psy_std_name :
            Type psyche de la représentation :
        net_curve :
            Dictionnaire associé à cet objet.

        Returns
        -------
        int, (np.array, np.array) :
            Retourne la couleur associée au réseau et les tableaux des données X et Y.
        """
        attr_function, next_psy_type = self.get_token_data(psy_std_name, 0, 0)
        psy_function_name = next_psy_type[0]

        function_name_obj = net_curve[attr_function]
        function_name = function_name_obj[0].name

        color = net_curve['lineColor']['indexList'][0]
        coords = self.psy_function(psy_function_name, function_name)

        return color, coords

    def psy_std_curve(self, psy_std_name, std_curve: dict) -> (int, bool, np.array, np.array):
        """
        Retourne les données attachées à une représentation de type courbe.

        Parameters
        ----------
        psy_std_name :
            Type psyche de la représentation :
        std_curve :
            Dictionnaire associé à cet objet.

        Returns
        -------
        int, bool, np.array, np.array :
            Retourne la couleur, marqueur et les tableaux des données X et Y.
        """
        attr_function, next_psy_type = self.get_token_data(psy_std_name, 0, 0)
        psy_function_name = next_psy_type[0]

        function_name = std_curve[attr_function]

        color = std_curve['curveLine']['color']['index']
        markdrawing = std_curve['markDrawing']
        marker = True
        if markdrawing.name == 'DISABLE':
            marker = False

        coords = self.psy_function(psy_function_name, function_name)

        return color, marker, coords

    def psy_depth_curve(self, psy_depth_name, psy_name: str) -> \
            Tuple[int, bool, bool, np.array, np.array, np.array]:
        """
        Récupération des données pour tracer d'une courbe 3D

        Parameters
        ----------
        psy_depth_name :
            Nom de l'objet graphique.
        psy_name :
            Nom de la fonction graphique.

        Returns
        -------
        color :
        marker :
            Tracé avec/sans marqueurs
        height :
            Tracé en 2D/3D
        x, y, z :
            Tableau des données associées (Fonction et support).
        """
        depth_curve = self.get_psy_object(psy_depth_name, psy_name)
        attr_function, next_psy_type = self.get_token_data(psy_depth_name, 0, 0)
        psy_function_name = next_psy_type[0]

        function_name = depth_curve[attr_function]

        color = depth_curve['curveLine']['color']['index']
        heightdrawing = depth_curve['heightDrawing']
        height = True
        if heightdrawing.name == 'DISABLE':
            height = False
        markdrawing = depth_curve['markDrawing']
        marker = True
        if markdrawing.name == 'DISABLE':
            marker = False

        coords = self.psy_function(psy_function_name, function_name)

        return color, marker, height, coords[0], coords[1], coords[2]

    def psy_map_data(self, raise_map_name: str, colormap_name: str) \
            -> Tuple[np.array, np.array, np.array, int]:
        """
        Lecture des données du fichier psy plan_simple.psy.
                                onde.psy Ok
                                carte_support_structure_compose.psy Pb dim z_tab

        Parameters
        ----------
        raise_map_name :
            Nom de l'objet RaiseMap dans le fichier filename
        colormap_name :
            Nom de l'objet ColorMap dans le fichier filename

        Returns
        -------
        np.ndarray
            données pour abscisses — x_tab
        np.ndarray
            données pour ordonnées — y_tab
        np.ndarray
            données pour z_tab (coloration)
        int
            nombre de couleurs à utiliser
        """
        # ReadPsyFile.print_result(psy_file)
        # récupération RaiseMap.heightFunction (Function) : GaiaV2_graphic_1_h
        #  heightFunction.geometricalSupport (StructuredSupport)
        #   geometricalSupport.coordinateValues = [ GaiaV2_graphic_1_x (FloatValue),
        #                                           GaiaV2_graphic_1_y (FloatValue) ]
        #     coordinateValues.data : ListFloatData (GaiaV2_graphic_1_x et GaiaV2_graphic_1_y)
        raise_map = self.get_psy_object('RaiseMap', raise_map_name)
        hf_name = raise_map['colorFunction']
        height_function = self.get_psy_object('Function', hf_name)
        gs_name = height_function['geometricalSupport']
        pv_name = height_function['physicalValue']

        geometrical_support = self.get_psy_object('StructuredSupport', gs_name)
        cv_names = geometrical_support['coordinateValues']

        need_triangulation = False

        x_value = self.get_psy_object('FloatValue', cv_names[0])
        x_name = x_value['data']
        x_coord_obj = self.get_psy_object('ListFloatData', x_name)
        if x_coord_obj is None:
            need_triangulation = True
            x_coord_obj = self.get_psy_object('ArrayFloatData', x_name)
        x_coord = x_coord_obj['data']['values']

        y_value = self.get_psy_object('FloatValue', cv_names[1])
        y_name = y_value['data']
        y_coord_obj = self.get_psy_object('ListFloatData', y_name)
        if y_coord_obj is None:
            need_triangulation = True
            y_coord_obj = self.get_psy_object('ArrayFloatData', y_name)
        y_coord = y_coord_obj['data']['values']

        physical_value = self.get_psy_object('FloatValue', pv_name)
        dpv_name = physical_value['data']
        z_coord_obj = self.get_psy_object('ArrayFloatData', dpv_name)
        z_coord = z_coord_obj['data']['values']

        if need_triangulation is False:
            x_tab, y_tab = np.meshgrid(x_coord, y_coord)
            # il faut remettre en forme z_coord pour qu'il est la même forme
            # et dimension que x_coord et y_coord
            z_tab: np.array = z_coord.reshape(y_coord.size, x_coord.size)
        else:
            x_tab, y_tab, z_tab = x_coord, y_coord, z_coord

        # sep = 'psy_map_data ––––––––––––––––––––'
        # print(f'{sep}\nX : {x_coord.size}\t{x_coord.shape}\n{x_coord}\n{sep}\n')
        # print(f'{sep}\nY : {y_coord.size}\t{y_coord.shape}\n{y_coord}\n{sep}\n')
        # print(f'{sep}\nZ : {z_coord.size}\t{z_coord.shape}\n{z_coord}\n{sep}\n')
        # print('size xcoord = ',x_coord.size, ' ycoord = ',y_coord.size, ' zcoord =',z_coord.size )
        # print('dim  xcoord = ',x_coord.ndim, ' ycoord = ',y_coord.ndim, ' zcoord =',z_coord.ndim )

        cm_obj = self.get_psy_object('ColorMap', colormap_name)

        return x_tab, y_tab, z_tab, cm_obj['colorNbr']

    def psy_axes_label(self, page_name: str) -> Tuple[str, str, str, str, str, str]:
        """
        Récupère les labels et unités associés aux axes.

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        Tuple[str, str, str, str, str, str]
            Labels et unités de chacuns des axes.
        """
        # @FIXME ne prend en compte que la première scène, et part du principe que la scène est la 1ere frame
        frames = self.psy_page_frame_name(page_name)
        # frames = scene, titre, commentaires, ...
        # On suppose que la scène est la première ...
        scene_name = frames[0]
        scene = self.get_psy_object('Scene', scene_name)
        if scene is None:
            raise RuntimeError('La scène n est pas la première frame déclarée')
        x_tab = scene['xScale']
        y_tab = scene['yScale']
        z_tab = scene['zScale']

        return (x_tab['title'], x_tab['unit'], y_tab['title'], y_tab['unit'],
                z_tab['title'], z_tab['unit'])

    def psy_axes_scaling(self, page_name: str) -> Tuple[PsyEnum, PsyEnum, PsyEnum]:
        """
        Récupère le mode de répartition des données sur les axes : Lin ou Log

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        Tuple[PsyEnum, PsyEnum, PsyEnum]
            Mode de répartition des données sur chacunes des directions.
        """
        # @FIXME ne prend en compte que la première scène, et part du principe que la scène est la 1ere frame
        frames = self.psy_page_frame_name(page_name)
        scene_name = frames[0]
        scene = self.get_psy_object('Scene', scene_name)
        if scene is None:
            raise RuntimeError('La scène n est pas la première frame déclarée')
        return scene['xScale']['type'], scene['yScale']['type'], scene['zScale']['type']

    def psy_function_data(self, page_name: str) -> List[PsyCurveData]:
        """
        Retourne les données associées à une 'function' psyché (support et data)
        @FIXME À garder ? fonction  qui dit qu'elle n'a pas d'objets de ce type de ce nom
        (ce qui est normal en soit ...)
        * StandardCurve (GEOMETRIE3D_4_Crb3D_901)
        * SetOfCurve    (GEOMETRIE3D_4_Crb3D_901)
        * VectorField   (GEOMETRIE3D_4_Crb3D_901)
        * RaiseMap      (GEOMETRIE3D_4_Crb3D_901)
        * UnstructuredRaiseMap (GEOMETRIE3D_4_Crb3D_901)

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        List[PsyCurveData] :
            Données d'une fonction psyché.

        """
        full_data = []
        frames, next_psy_type = self.psy_page('Page', page_name)
        for frame in frames:
            frame_type = self.get_type_object(frame)
            if frame_type == 'Scene':
                collection, next_psy_type = self.psy_scene(frame_type, frame)
                for contents_type in next_psy_type:
                    if contents_type == 'Collection':
                        p_data = self.get_curves_from_collection(page_name, contents_type, collection)
                        full_data.append(p_data)
        return full_data

    def psy_data_from_value(self, psy_type_name: str, psy_name: str, search_for: str) -> np.array:
        """
        Retourne le tableau des valeurs contenues dans un objet psyché de type `ArrayFloatData` ou `ArrayIntegerData`

        Parameters
        ----------
        psy_type_name :
            Type Psyché du tableau.
        psy_name :
            Nom Psyché du Tableau.
        search_for :
            'ArrayFloatData' ou 'ArrayIntegerData'

        Returns
        -------
        nb.array :
            Tableau des valeurs
        """
        # À partir d'un FloatValue de nom psy_name,
        # retourne les valeurs du ListFloatData ou ArrayFloatData correspondant
        value = self.get_psy_object(psy_type_name, psy_name)
        attr_v, value_v = self.get_token_data(psy_type_name, 0, 0)
        data_name = value[attr_v]
        data_size = []
        psy_data_name = None
        list_data = None
        # Pour l'instant, que deux types de psyobject dans ListFloatData
        for psy_data_name in value_v:
            list_data = self.get_psy_object(psy_data_name, data_name)
            if list_data is not None:
                if psy_data_name == search_for:
                    data_size = list_data['data']['array']['sizes']
                break
        # list_data = self.get_psy_object(psy_dict, psy_data_name, data_name)
        attr_lfd, value_lfd = self.get_token_data(psy_data_name, 0, 0)
        if '.' not in attr_lfd:
            raise RuntimeError(f'ERREUR : {attr_lfd}\t({psy_data_name}, {data_name} – {psy_type_name, psy_name}')
        first, last = attr_lfd.split('.')

        if len(data_size) > 1:
            return list_data[first][last].reshape(data_size[1], data_size[0])
        return np.array(list_data[first][last])

    def psy_data_from_float_value(self, psy_type_name: str, psy_name: str) -> np.array:
        """
        Retourne le tableau des valeurs contenues dans un objet psyché de type "FloatValue"

        Parameters
        ----------
        psy_type_name :
            Type Psyché du tableau.
        psy_name :
            Nom Psyché du Tableau.

        Returns
        -------
        nb.array :
            Tableau des valeurs
        """
        return self.psy_data_from_value(psy_type_name, psy_name, 'ArrayFloatData')

    def psy_data_from_int_value(self, psy_type_name: str, psy_name: str) -> np.array:
        """
        Retourne le tableau des valeurs contenues dans un objet psyché de type "ArrayIntegerData"

        Parameters
        ----------
        psy_type_name :
            Type Psyché du tableau.
        psy_name :
            Nom Psyché du Tableau.

        Returns
        -------
        nb.array :
            Tableau des valeurs
        """
        return self.psy_data_from_value(psy_type_name, psy_name, 'ArrayIntegerData')

    def psy_function(self, psy_function_name: str, function_name: str) -> (np.array, np.array, np.array):
        """
        Récupère les données associées a une *function* psyché

        Parameters
        ----------
        psy_function_name :
            Nom du type de fonction recherchée (ici 'Function')
        function_name :
            Nom psyché de la fonction recherchée.

        Returns
        -------
        np.array, np.array, np.array :
            Tableaux des données de la fonction (supports et fonction)
        """
        function = self.get_psy_object(psy_function_name, function_name)
        attr_support, value_support = self.get_token_data(psy_function_name, 0, 0)

        # pour differencier StructuredSupport de ScatteredSupport
        psy_support_name = ''
        support_name = function[attr_support]
        for i in value_support:
            support = self.get_psy_object(i, support_name)
            if support is not None:
                psy_support_name = i
                break

        attr_physical, value_physical = self.get_token_data(psy_function_name, 1, 0)
        psy_physical_name = value_physical[0]
        physical_name = function[attr_physical]

        support = self.get_psy_object(psy_support_name, support_name)

        nb_token = self.get_number_of_tokens(psy_support_name)
        vdata = []
        for i in range(nb_token):
            attr_coord, value_coord = self.get_token_data(psy_support_name, i, 0)
            coord_name = support[attr_coord]
            if isinstance(coord_name, list):
                # 'coordinateValues'  C'est une liste [X, Y]
                # data = [x: np.array, y: np.array, z: np.array]
                if value_coord[0] == 'FloatValue':
                    for coor in coord_name:
                        vtab = self.psy_data_from_float_value('FloatValue', coor)
                        vdata.append(vtab)
                elif value_coord[0] == 'IntegerValue':
                    for coor in coord_name:
                        vtab = self.psy_data_from_int_value('IntegerValue', coor)
                        vdata.append(vtab)
            else:
                # 'topologie'
                if value_coord[0] == 'NodeTopology':
                    vtab = self.psy_data_from_int_value(value_coord[0], coord_name)
                    vdata.append(vtab)

        vdata.append(self.psy_data_from_float_value(psy_physical_name, physical_name))

        return vdata

    def psy_page(self, psy_page_name: str, psy_name: str = None) -> (List[str], List[str]):
        """
        Retourne la liste des noms des objets appartenant à une page donnée en argument.
        @FIXME Faut-il changer le nom de la méthode

        Parameters
        ----------
        psy_page_name :
            Nom Psyché du type d'objet (ici 'Page').
        psy_name :
            Nom de l'objet.

        Returns
        -------
        List[str], List[str]:
            Liste des objets contenu dans la page et liste des types suivants.
        """
        if len(self.psy_dict[psy_page_name]) == 1 and psy_name is None:
            # Cas du «top» de l'arbre des objets Psyché
            psy_name = self.psy_dict[psy_page_name][0]['name']
            page = self.get_psy_object(psy_page_name, psy_name)
        else:
            page = self.get_psy_object(psy_page_name, psy_name)
        attr_frame, next_psy_type = self.get_token_data(psy_page_name, 0, 0)
        # print(f'Page : {page}\n\tAttr frame : {attr_frame}\tnext_psy_type : {next_psy_type}')
        return page[attr_frame], next_psy_type

    def psy_page_name(self) -> (List[str]):
        """
        Récupère une liste des noms de pages contenues dans le fichier psyché

        Returns
        -------
        List[str] :
            Liste des noms des pages.

        """
        return [item['name'] for item in self.psy_dict['Page']]

    def psy_page_frame_name(self, page_name: str) -> (List[str]):
        """
        Retourne la liste des noms des 'frame' contenues dans la page psyché passée en argument

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        List[str] :
            Liste des noms des 'frame'.
        """
        return self.psy_page('Page', page_name)[0]

    def psy_page_collection_name(self, page_name: str) -> str:
        """
        Retourne la liste des noms des 'collection' contenues dans la page psyché passée en argument
        @FIXME ne retourne que la collection de la dernière scène
        Est utilisé dans psy_page_graphic_name (qui est utilisé dans psy2svg - déclenche un RunTimeError si pls)

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        str :
            Liste des noms des 'collection'.

        """
        frames = self.psy_page_frame_name(page_name)

        collection_name = ''
        for fra in frames:
            scene = self.get_psy_object('Scene', fra)
            if scene is not None:
                collection_name = scene['sceneContents'].name

        return collection_name

    def psy_page_graphic_name(self, page_name: str) -> (List[str]):
        """
        Retourne la liste des noms des objets graphiques contenus dans la page psyché passée en argument.
        Objets graphiques au sens type de representation : StandardCurve, SetOfCurve, DepthCurve, FlatMap,
        RaiseMap, PointField, VectorField, UnstructuredRaiseMap, FlatHistogram, DepthHistogram, Image
        @FIXME : prendre en compte plusieurs collections

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        List[str] :
            Liste des noms des objets graphiques.
        """
        collection_name = self.psy_page_collection_name(page_name)
        collection = self.get_psy_object('Collection', collection_name)
        graphics = []
        if collection is not None:
            graphics.append(collection['graphic'][0].name)

        return graphics

    def psy_scene_type(self, page_name: str) -> str:
        """
        Determine le type de la 'scene' d'une page d'un fichier psyché
        @FIXME ne prend pas en compte les pages avec plusieurs scènes

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        str :
            Type de la scene : DEPTH ou FLAT.
        """
        scene_name = self.psy_page_frame_name(page_name)[0]
        scene = self.get_psy_object('Scene', scene_name)
        return scene['sceneType'].name

    def psy_scene(self, psy_scene_name: str, psy_name: str) -> (List[str], str):
        """
        Retourne la liste des noms des objets  appartenant à une scene donnée en argument.
        @FIXME à supprimer ? utiliser dans psy_function_data (et dans bc d'autres de psy2svg)

        Parameters
        ----------
        psy_scene_name :
            Nom Psyché du type d'objet (ici scene).
        psy_name :
            Nom de l'objet.

        Returns
        -------
        List[str], str :
            Liste des objets contenu dans la scene et liste des types suivants.

        """
        scene = self.get_psy_object(psy_scene_name, psy_name)
        attr_scene, next_psy_type = self.get_token_data(psy_scene_name, 0, 0)
        return scene[attr_scene], next_psy_type

    def psy_raisemap_name(self) -> str:
        """
        Récupère le nom Psyché de la raisemap

        Returns
        -------
        str :
            Nom de l'objet recherché dans le fichier.
        """
        # @FIXME ne considère que la 1ère raisemap
        try:
            return self.psy_dict['RaiseMap'][0]['name']
        except KeyError as e:
            return ''

    def psy_colormap_name(self) -> str:
        """
        Récupère le nom Psyché de la color map

        Returns
        -------
        str :
            Nom de l'objet recherché dans le fichier.
        """
        # @FIXME ne considère que la 1ère colormap
        try:
            return self.psy_dict['ColorMap'][0]['name']
        except KeyError as e:
            return ''

    def psy_representation_type(self, graphic_name: str) -> str:
        """
        Retourne le mode de représentation associé à un objet graphic identifié par son nom

        Parameters
        ----------
        graphic_name :
            Nom du graphique recherché.

        Returns
        -------
        str :
            Nom du type de représentation.
        """
        for rep in psy_object_generic['Representation']:
            try:
                for item in self.psy_dict[rep]:
                    if item.get('name') == graphic_name:
                        return rep
            except KeyError as e:
                continue

    def psy_text(self, page_name: str) -> List[PsyStdTextData]:
        """
        Retourne la liste des textes contenus dans le fichier étudié

        Parameters
        ----------
        page_name :
            Nom de la page psyché.

        Returns
        -------
        List[PsyStdTextData] :
            Liste des objets de type PsyStdTextData contenus dans la page.
        """
        full_data = []
        frames, next_psy_type = self.psy_page('Page', page_name)
        for frame in frames:
            frame_type = self.get_type_object(frame)
            if frame_type == 'StandardText':
                std_txt = self.get_psy_object(frame_type, frame)
                align = psy_alignment(std_txt['frameAnchorRef'].name)
                p_data = PsyStdTextData(title=std_txt['textContents'],
                                        posx=float(std_txt['frameAnchorX']),
                                        posy=float(std_txt['frameAnchorY']),
                                        alignment=align,
                                        name=frame)
                full_data.append(p_data)
        return full_data


def get_curves_from_psy_file(name: str) -> List[PsyCurveData]:
    """
    @FIXME Vérifier si ne fait pas doublons avec `ReadPsyFile.psy_*_curve` (normalement ... à vérifier)
    :param name:
    :return:
    """
    psy_dict = ReadPsyFile.read_psy_file(name)
    analyzer = AnalyzePsyDict(psy_dict)
    top = analyzer.get_top_level()
    full_data = []
    if top == 'Page':
        frames, next_psy_type = analyzer.psy_page(top, None)
        for frame in frames:
            frame_type = analyzer.get_type_object(frame)
            if frame_type == 'Scene':
                collection, next_psy_type = analyzer.psy_scene(frame_type, frame)
                for contents_type in next_psy_type:
                    if contents_type == 'Collection':
                        hh = analyzer.get_curves_from_collection('', contents_type, collection)
                        full_data.append(hh)
    return full_data


def psy_map_data(filename: str, raise_map_name: str, colormap_name: str, intern: bool = False) \
        -> Tuple[NpArray, NpArray, NpArray, int]:
    """
    Lecture des données du fichier psy plan_simple.psy.

    Parameters
    ----------
    filename : str
        (os.pathLike) Nom du fichier contenante les données (avec son extension .psy)
    raise_map_name : str
        Nom de l'objet RaiseMap dans le fichier filename
    colormap_name : str
        Nom de l'objet ColorMap dans le fichier filename
    intern : bool
        True si les données à lire sont internes

    Returns
    -------
    np.ndarray
        données pour abscisses — x
    np.ndarray
        données pour ordonnées — y
    np.ndarray
        données pour z (coloration)
    int
        nombre de couleurs à utiliser
    """
    if intern is True:
        data_path = os.path.join(get_intern_data_dir(), filename)
    else:
        data_path = os.path.join(get_data_dir(), filename)
    psy_dict = ReadPsyFile.read_psy_file(data_path)
    analyzer = AnalyzePsyDict(psy_dict)
    # ReadPsyFile.print_result(psy_file)
    # récupération RaiseMap.heightFunction (Function) : GaiaV2_graphic_1_h
    #  heightFunction.geometricalSupport (StructuredSupport)
    #   geometricalSupport.coordinateValues = [ GaiaV2_graphic_1_x (FloatValue),  GaiaV2_graphic_1_y (FloatValue) ]
    #     coordinateValues.data : ListFloatData (GaiaV2_graphic_1_x et GaiaV2_graphic_1_y)
    raise_map = analyzer.get_psy_object('RaiseMap', raise_map_name)
    hf_name = raise_map['colorFunction']
    height_function = analyzer.get_psy_object('Function', hf_name)
    gs_name = height_function['geometricalSupport']
    pv_name = height_function['physicalValue']

    geometrical_support = analyzer.get_psy_object('StructuredSupport', gs_name)
    cv_names = geometrical_support['coordinateValues']

    need_triangulation = False

    x_value = analyzer.get_psy_object('FloatValue', cv_names[0])
    x_name = x_value['data']
    x_coord_obj = analyzer.get_psy_object('ListFloatData', x_name)
    if x_coord_obj is None:
        need_triangulation = True
        x_coord_obj = analyzer.get_psy_object('ArrayFloatData', x_name)
    x_coord = x_coord_obj['data']['values']

    y_value = analyzer.get_psy_object('FloatValue', cv_names[1])
    y_name = y_value['data']
    y_coord_obj = analyzer.get_psy_object('ListFloatData', y_name)
    if y_coord_obj is None:
        need_triangulation = True
        y_coord_obj = analyzer.get_psy_object('ArrayFloatData', y_name)
    y_coord = y_coord_obj['data']['values']

    physical_value = analyzer.get_psy_object('FloatValue', pv_name)
    dpv_name = physical_value['data']
    z_coord_obj = analyzer.get_psy_object('ArrayFloatData', dpv_name)
    z_coord = z_coord_obj['data']['values']
    # sep = '––––––––––––––––––––'
    # print(f'{sep}\nX : {x_coord.size}\t{x_coord.shape}\n{x_coord}\n{sep}\n')
    # print(f'{sep}\nY : {y_coord.size}\t{y_coord.shape}\n{y_coord}\n{sep}\n')

    if need_triangulation is False:
        x, y = np.meshgrid(x_coord, y_coord)
        # il faut remettre en forme z_coord pour qu'il est la même forme et dimension que x_coord et y_coord
        z: np.array = z_coord.reshape(y_coord.size, x_coord.size)
    else:
        x, y, z = x_coord, y_coord, z_coord

    cm_obj = analyzer.get_psy_object('ColorMap', colormap_name)

    return x, y, z, cm_obj['colorNbr']
