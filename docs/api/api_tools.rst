====================
 ``tools`` module
====================

.. currentmodule:: nemesis.tools

.. automodule:: nemesis.tools

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst
    :nosignatures:

    Timer
    LoggerTimer
    trace
    compare_dict
