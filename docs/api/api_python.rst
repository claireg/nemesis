.. _api_python:

===============================
Interface Python de ``nemesis``
===============================

.. only:: builder_html

    :Release: |version|
    :Date: |today|

.. toctree::
    :maxdepth: 1

    api_nemesis.rst
    api_readdata.rst
    api_plotter.rst
    api_tests.rst
    api_tools.rst



