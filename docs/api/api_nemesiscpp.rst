.. _api_nemesiscpp:

C++ ``nemesis`` interface
==========================

Documentation du header ``src/utilities.h`` et ``src/nemesiscppconfig.h``.


Variables globales
------------------

.. c:macro:: VIRTUAL_ENV_SITE_PACKAGES

    Chemin vers le répertoire `site-packages` de l'environnement virtuel à utiliser.

.. c:macro:: NEMESIS_SITE_PACKAGES

    Chemin vers le package `nemesis`.

.. c:macro:: NEMESIS_VERSION_MAJOR

    Version majeure de nemesis.

.. c:macro:: NEMESIS_VERSION_MINOR

    Version mineure de nemesis.

.. c:macro:: NEMESIS_VERSION_PATCH

    Version patch de nemesis.

.. c:macro:: NEMESIS_VERSION_TWEAK

    Version de configuration de nemesis.


.. note::

    Le résultat de ``NEMESIS_VERSION_MAJOR.NEMESIS_VERSION_MINOR.NEMESIS_VERSION_PATCH`` donne la même version que dans ``nemesis/__init__.py`` pour la variable ``version``.


Namespace ``nem_utils``
-----------------------

.. cpp:namespace:: nem_utils

.. cpp:function::   template <typename WStrContainer> \
                    void split_string(WStrContainer& results_, const wchar_t* tosplit_)

    Séparation de la chaîne ``tosplit`` en n sous-chaînes.

    :param wchar_t* tosplit: Chaîne initiale contenant des espaces.
    :param WStrContainer results_: Conteneur pour stocker le résultat de la division.
    :type: WStrContainer : n'import quel type de conteneur C++ ayant la méthode ``push_back`` (contiendra des ``wstring``).

.. cpp:function::   template <typename T> \
                    void print_vector(std::vector<T>& tab_, const std::wstring& msg_ = "")

    Affichage sur la sortie standard d'un tableau avec un en-tête si précisé lors de l'appel. ``T`` doit être un type affichable.

    :param vector<T>& tab_: tableau à afficher.
    :param wstring& msg_: en-tête (chaîne vide par défaut ``L""``).

.. cpp:function::   template <typename T> \
                    void print_vector_vector(std::vector<std::vector<T>>& resultats)

    Affichage sur la sortie standard d'un tableau de tableau. ``T`` doit être un type affichable.

        :param vector<vector<T>>& resultats: tableau à afficher.

.. cpp:class:: template <typename charT, typename traits = std::char_traits<charT>> center_helper

    Aide à l'alignement centré d'une chaîne de caractères.

.. cpp:function:: center_helper::center_helper(std::basic_string<charT, traits> str)

    Constructeur par défaut de la classe nem_utils::center_helper.

.. cpp:function::   template <typename a, typename b> \
                    std::basic_ostream\<a, b>& operator\<\<(std::basic_ostream\<a, b>& s, const center_helper\<a, b>& c)

    Surcharge du l'opérateur ``<<``.

.. cpp:function::   template <typename charT, typename traits> \
                    basic_ostream\<charT, traits>& operator\<\<(basic_ostream\<charT, traits>& s, const center_helper\<charT, traits>& c)

    Fonction pour centrer un chaîne de caractère dans un flux.

    **Exemple d'utilisation**

    ``ostringstream tested << setw(4) << nem::centered(s) << " ";``

    :param sbasic_ostream<charT, traits>& operator<<(basic_ostream<charT, traits>& s: le flux d'entrée.
    :param center_helper<charT, traits>& c: la chaîne à centrer
    :return: le flux dans lequel la chaîne a été centrée.
    :rtype: :cpp:expr:`basic_ostream<charT, traits>&`

.. cpp:function:: constexpr std::tuple<int, int, int, int> version_info()

    Création d'un tuple représentant la version de nemesis.

    :return: la version de nemesis sous forme de quadruplet.
    :rtype: :cpp:expr:`tuple<int, int, int, int>`

.. cpp:function:: std::string version()

    Retourne la version de nemesis sous forme de chaîne de caractères.

    :return: la version de nemesis sous forme de chaîne de caractères.
    :rtype: :cpp:expr:`std::string`

.. cpp:function:: void cartridge()

    Affichage sur la sortie du cartouche du projet nemesis.


#### Seulement si ``__cplusplus >= 201402L``

**En gros, si on est en C++14 (gcc 6.4 l'est par défaut).**

Les fonctions ne sont pas accessibles sinon car elles utilisent la nouvelle *feature* encore expérimentale :
le namespace ``std::experimental::filesystem``.

.. cpp:function:: auto reference_dir()

    Retourne le chemin vers le répertoire contenant les données de référence (pour les tests).

.. cpp:function:: auto create_reference_path(const fs::path& filename_)

    Création d'un chemin complet vers un fichier dans le répertoire des données de référence.

    :param path filename_: nom ou chemin vers un fichier. ``path`` est du type :cpp:expr:`std::experimental::filesystem`.

.. cpp:function:: void exists(const fs::path& filepath_)

    Lève une exception si le fichier n'existe pas. ``filepath_`` est du type :cpp:expr:`std::experimental::filesystem`.

    :throw runtime_error: si le fichier n'existe pas.


Les fonctions suivantes ne sont pas accessibles car elles utilisent les arguments template :cpp:expr:`Args&&...`.
Elles permettent de mesurer les temps d'exécution de fonctions C++ selon leurs arguments d'entrée et leur type de retour.
Elles se basent toutes sur :cpp:expr:`std::chrono`, avec comme échelle la milliseconde par défaut.

.. cpp:function::   template <class TDuration = std::chrono::milliseconds> \
                    void logger_timer_message(const std::string& msg, auto interval)

    Affiche sure la sortie standard le temps (en millisecondes par défaut) correspondant à la durée passée  en argument.

    :param string msg: Message à afficher (en général l'utilisateur met le nom de la fonction mesurée).
    :param auto interval: une durée (d'un type convertible en :cpp:expr:`std::chrono::duration_cast<TDuration>`).

.. cpp:function::   template <class TFunc, class TDuration = std::chrono::milliseconds> \
                    auto logger_timer(const std::string& msg, TFunc&& func)

    Exécute et Mesure le temps d'exécution de la fonction :cpp:expr:`func`, et affiche sur la sortie standard le temps mesuré.

    **Restriction d'utilisation**

    Ne traite que les fonctions ``T func(Args...)``. Le type de retour de la fonction ne peut être :cpp:expr:`void`.

    **Exemples d'utilisation**

    ``nem::logger_timer("gaia_section_efficace", ex_1)(pause); // int ex_1(int) { ... }``
    ``nem::logger_timer("gaia_section_efficace", ex_2)(); // int ex_2() {...}``

    :param string msg: Le message à afficher sur la sortie standard.
    :param TFunc&& func: La fonction dont le temps d'exécution doit être mesuré.
    :return: le résultat de l'appel à :cpp:expr:`func`.

.. cpp:function::   template <class TFunc, class TDuration = std::chrono::milliseconds, class... Args> \
                    typename std::enable_if<std::is_constructible<TFunc, Args&&...>::value>::type \
                    logger_timer_func_return_args(const std::string& msg, TFunc&& func, Args&&... args)

    :param string msg: Message à afficher (en général l'utilisateur met le nom de la fonction mesurée).
    :param TFunc&& func: La fonction dont le temps d'exécution doit être mesuré.
    :param Args&&... args: arguments de la fonction à appeler.
    :return: l'objet / référence / ... retourner normalement par l'appel direct à :cpp:expr:`func`.

    **Restriction d'utilisation**

    Utilisable seulement si le type de retour de l'appel peut être construit.

.. cpp:function::   template <class TFunc, class TDuration = std::chrono::milliseconds, class... Args> \
                    auto logger_timer_func(const std::string& msg, TFunc&& func, Args&&... args)

    Traite tous les signatures de fonctions mais ne retourne jamais rien ! car quid du type de result dans le cas
    où la fonction retourne :cpp:expr:`void`.

    **Exemples d'utilisation**

    ``nem::logger_timer_func("gaia_curve_data", ex_2, pause) // int ex_2(int) { ... }``

    :param string msg: Message à afficher (en général l'utilisateur met le nom de la fonction mesurée).
    :param TFunc&& func: La fonction dont le temps d'exécution doit être mesuré.
    :param Args&&... args: arguments de la fonction à appeler.
    :return: Le type de retour de :cpp:expr:`func(args...)`.

    Le prototype complet de la fonction (que Sphinx ne comprend pas – la partie ``-> decltype(func(args...))`` :

    ``template <class TFunc, class TDuration = std::chrono::milliseconds, class... Args>``
    ``auto logger_timer_func(const std::string& msg, TFunc&& func, Args&&... args) -> decltype(func(args...))``











