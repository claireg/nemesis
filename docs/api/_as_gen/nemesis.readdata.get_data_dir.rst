nemesis.readdata.get\_data\_dir
===============================


.. currentmodule:: nemesis.readdata

.. autofunction:: get_data_dir



.. raw:: html

    <div class="clearer"></div>

