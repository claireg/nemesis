====================
 ``readdata`` module
====================

.. currentmodule:: nemesis.readdata

.. automodule:: nemesis.readdata
   :no-members:
   :no-inherited-members:

Classes
-------

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst
    :nosignatures:

    ReadGaiaFile
    ReadPsyFile
    PsyObject
    PsyEnum


Fonctions
---------

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst

    narray_prop
    get_data_dir
    compute_section_efficace
    gaia_points_data
    gaia_one_mesh1d_data
    gaia_network_data
    gaia_map_data
    gaia_curve_data
    gaia_stairs_curve_data
    gaia_section_efficace
    gaia_network_0
    gaia_network_1
    gaia_network_2
    gaia_map_with_zeros
    gaia_isolines_levels
    gaia_isolines_levels_2
    psy_map_data
    psy_map_2d_data
    print_psy_several_curves
    print_psy_several_networks
    print_psy_vectors
    print_psy_wave
    print_psy_curve_3d
    print_psy_network_0
    print_psy_network_1
    print_psy_network_2
    print_psy_network_3
    print_psy_network_4
    print_psy_network_5