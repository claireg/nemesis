.. _api_cpp:

============================
Interface C++ de ``nemesis``
============================

.. only:: builder_html

    :Release: |version|
    :Date: |today|


L'interface C++ se compose de 2 parties :

* Une partie miroir de :py:mod:`matplotlib:matplotlib.pyplot` composée de :
    * :ref:`api_matplotlibcpp`
    * ``nemesis`` (:py:mod:`nemesis.plotter.PyPlotCEA`) en Python
* Une partie de gestion de l'environnement python et d'utilitaires :ref:`api_nemesiscpp`.

.. admonition::     Les nouveautés
    :class: admonition tip

    * :cpp:func:`matplotlibcpp::view_init` (Rubrique Utilitaire, gestion fenêtre et axes)
    * :cpp:func:`matplotlibcpp::zlim` (Rubrique Personnalisation des axes et des courbes)



Fonctions de ``matplotlib`` et assimilées
=========================================

Toutes ces fonctions sont réunies dans l'espace de nom ``matplotlibcpp``.

Courbes et réseaux
------------------

* :cpp:func:`matplotlibcpp::plot`
* :cpp:func:`matplotlibcpp::named_plot`

* :cpp:func:`matplotlibcpp::semilogx`
* :cpp:func:`matplotlibcpp::named_semilogx`
* :cpp:func:`matplotlibcpp::semilogy`
* :cpp:func:`matplotlibcpp::named_semilogy`
* :cpp:func:`matplotlibcpp::loglog`

Cartes
------

.. rubric:: **Propre à** ``nemesis``

* :cpp:func:`matplotlibcpp::named_logarithmic_contourf`
* :cpp:func:`matplotlibcpp::named_linear_contourf`
* :cpp:func:`matplotlibcpp::named_logarithmic_contour`
* :cpp:func:`matplotlibcpp::named_linear_contour`
* :cpp:func:`matplotlibcpp::named_logarithmic_tricontourf`
* :cpp:func:`matplotlibcpp::named_linear_tricontourf`
* :cpp:func:`matplotlibcpp::named_logarithmic_tricontour`
* :cpp:func:`matplotlibcpp::named_linear_tricontour`
* :cpp:func:`matplotlibcpp::get_contourf_levels`
* :cpp:func:`matplotlibcpp::get_contourf_layers`
* :cpp:func:`matplotlibcpp::get_contour_levels`
* :cpp:func:`matplotlibcpp::set_cmap`

Graphiques
----------

* :cpp:func:`matplotlibcpp::stem`
* :cpp:func:`matplotlibcpp::fill_between`
* :cpp:func:`matplotlibcpp::hist`
* :cpp:func:`matplotlibcpp::bar`
* :cpp:func:`matplotlibcpp::errorbar`
* :cpp:func:`matplotlibcpp::scatter`
* :cpp:func:`matplotlibcpp::quiver`

Graphiques 3D
--------------

* :cpp:func:`matplotlibcpp::plot_surface`
* :cpp:func:`matplotlibcpp::plot3D`
* :cpp:func:`matplotlibcpp::load_3d_modules`
* :cpp:func:`matplotlibcpp::is_3d`
* :cpp:func:`matplotlibcpp::raised_contourf`
* :cpp:func:`matplotlibcpp::named_raised_contourf`

Annotations et textes
---------------------

* :cpp:func:`matplotlibcpp::suptitle`
* :cpp:func:`matplotlibcpp::title`
* :cpp:func:`matplotlibcpp::text`
* :cpp:func:`matplotlibcpp::ylabel`
* :cpp:func:`matplotlibcpp::xlabel`
* :cpp:func:`matplotlibcpp::xticks`
* :cpp:func:`matplotlibcpp::yticks`

* :cpp:func:`matplotlibcpp::annotate`
* :cpp:func:`matplotlibcpp::legend`

.. rubric:: **Propre à** ``nemesis``

* :cpp:func:`matplotlibcpp::set_titles`
* :cpp:func:`matplotlibcpp::under_axes_legend`
* :cpp:func:`matplotlibcpp::networks_legend`

Personnalisation des axes et des courbes
----------------------------------------

* :cpp:func:`matplotlibcpp::ylim`
* :cpp:func:`matplotlibcpp::xlim`
* :cpp:func:`matplotlibcpp::zlim` (uniquement pour des axes 3D)

.. rubric:: **Propre à** ``nemesis``

* :cpp:func:`matplotlibcpp::set_log_scale`

* :cpp:func:`matplotlibcpp::set_linewidth`
* :cpp:func:`matplotlibcpp::set_linewidth_to_all`

* :cpp:func:`matplotlibcpp::highlight_curve`

* :cpp:func:`matplotlibcpp::show_curve`
* :cpp:func:`matplotlibcpp::show_all_curves`
* :cpp:func:`matplotlibcpp::hide_curve`

Utilitaires
-----------

.. rubric:: Manipulation de tableaux

* :cpp:func:`matplotlibcpp::get_array`
* :cpp:func:`pyobject_to_vector`

.. rubric:: Transformation type C++ en PyObject

* :cpp:func:`matplotlibcpp::get_pyobject_from`


.. rubric:: Gestion fenêtre et axes

* :cpp:func:`matplotlibcpp::view_init`  (uniquement pour des axes 3D)
* :cpp:func:`matplotlibcpp::subplot`
* :cpp:func:`matplotlibcpp::subplots_adjust`
* :cpp:func:`matplotlibcpp::figure`
* :cpp:func:`matplotlibcpp::axis`
* :cpp:func:`matplotlibcpp::tight_layout`
* :cpp:func:`matplotlibcpp::grid`

.. rubric:: **Propre à** ``nemesis``

* :cpp:func:`matplotlibcpp::nemesis_params`
* :cpp:func:`matplotlibcpp::set_figure_size`
* :cpp:func:`matplotlibcpp::set_figure_xy`
* :cpp:func:`matplotlibcpp::set_window_title`

.. rubric:: Affichage and co.

* :cpp:func:`matplotlibcpp::show`
* :cpp:func:`matplotlibcpp::draw`
* :cpp:func:`matplotlibcpp::clf`
* :cpp:func:`matplotlibcpp::cla`
* :cpp:func:`matplotlibcpp::switch_axes`

* :cpp:func:`matplotlibcpp::pause`

* :cpp:func:`matplotlibcpp::close`

* :cpp:func:`matplotlibcpp::save`

.. rubric:: Divers

* :cpp:func:`matplotlibcpp::backend`

* :cpp:func:`matplotlibcpp::xkcd`

* :cpp:func:`matplotlibcpp::ion`
* :cpp:func:`matplotlibcpp::ioff`

* :cpp:func:`matplotlibcpp::ginput`


Divers
------

* :cpp:func:`matplotlibcpp::detail::get`
* :cpp:class:`matplotlibcpp::detail::_interpreter::_interpreter`
* :cpp:func:`matplotlibcpp::detail::_interpreter::~_interpreter`

.. rubric:: **Propre à** ``nemesis``

* :cpp:func:`matplotlibcpp::detail::exists`
* :cpp:func:`matplotlibcpp::detail::add_site`
* :cpp:func:`matplotlibcpp::detail::print_sys_path`


Vision globale exhaustive
-------------------------

.. toctree::
    :maxdepth: 2

    api_matplotlibcpp.rst

Fonctions utilitaire et de gestion de l'environnement Python
============================================================

Vision globale exhaustive
-------------------------

.. toctree::
    :maxdepth: 2

    api_nemesiscpp.rst
