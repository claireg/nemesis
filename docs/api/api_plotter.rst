===================
 ``plotter`` module
===================

.. currentmodule:: nemesis.plotter

.. automodule:: nemesis.plotter
   :no-members:
   :no-inherited-members:

Classes
-------

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst
    :nosignatures:

    PyPlotCEA
    numpy_tools.NumpyHelper
    ticker.SameLengthMathTextSciFormatter
    ticker.PseudoLogMathTextSciFormatter
    ticker.LogLogMathTextSciFormatter

Fonctions
---------

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst
    :nosignatures:

    contour.isocolorbar

Dev : Classes
-------------

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst
    :nosignatures:

    PrototypePyplot
    contour.BaseContourHelper
    contour.ContourHelper
    contour.ContourFilledHelper
    misc.HandlerDashedLines
    mplot3d_extension.BaseSurface3DHelper

Dev : Fonctions
---------------

.. autosummary::
    :toctree: _as_gen/
    :template: autosummary.rst
    :nosignatures:

    misc.check_curve
    misc.create_legend_line_collections
    misc.move_axes_net_legend
    mplot3d_extension.move_parameters