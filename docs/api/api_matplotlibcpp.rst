.. _api_matplotlibcpp:

``matplotlibcpp`` : C++ ``matplotlib`` interface
================================================

Toute l'interface est contenu dans un seul fichier : ``src/matplotlibcpp.hpp``.

Si l'appel à une fonction ajouté par le CEA dans ce ``namespace`` échoue, une exception est levée (``std::runtime_error``),
et le message d'erreur est affiché sur la sortie standard (exécution de ``PyErr_Print();`` avant de lever l'exception).

Namespace ``matplotlibcpp``
---------------------------

.. cpp:namespace:: matplotlibcpp



Courbes et réseaux
``````````````````

.. rubric:: plot
.. cpp:function::   template <typename Numeric> \
                    bool plot(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::map<std::string, std::string>& keywords)

    Trace une courbe ``y`` = ``x``.

    :param double x: abscisses de la courbe
    :param double y: ordonnées de la courbe
    :param map<string, string> keywords: dictionnaire des options à passer à la fonction python.

    Équivalent à ::

        matplotlib.pyplot.plot(x, y, **keywords)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool plot(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Trace une courbe ``y`` = ``x`` avec les propriétés spécifiées dans s:
    ``s = '[color][marker][line]'``. Chacune des entrées est optionnelle (à défaut on utilisera celle du cycle en cours).

    Équivalent à ::

        matplotlib.pyplot.plot(x, y, s).

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   template <typename Numeric> \
                    bool plot(const std::vector<Numeric>& y, const std::string& format = "")

    Équivalent à ::

        matplotlib.pyplot.plot(y, format).

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   template <typename A, typename B, typename... Args> \
                    bool plot(const A& a, const B& b, const std::string& format, Args... args)

    Appel récursif à la méthode python ``matplotlib.pyplot.plot(...)``.

    Très pratique pour afficher en un seul appel plusieurs courbes. Plus rapide que n appels à ``matplotlib.pyplot.plot(...)``.

    Équivalent::

        matplotlib.pyplot.plot(x1, y1, s1, x2, y2, s2, ...)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   bool plot(const std::vector<double>& x, const std::vector<double>& y, const std::string& format = "")

    Utile pour un appel simple en C++ (avec des *initializer lists*) : ``plot( {1,2,3,4}, {1,2,3,4} )``.

     Équivalent à ::
        matplotlib.pyplot.plot(x, y, format)`

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   bool plot(const std::vector<double>& y, const std::string& format = "")

    Utile pour un appel simple en C++ (avec des *initializer lists*) : ``plot( {1,2,3,4} )``.

    Équivalent à ::

        matplotlib.pyplot.plot(y, format)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   bool plot(const std::vector<double>& x, const std::vector<double>& y, const std::map<std::string, std::string>& keywords)

    Utile pour un appel simple en C++ (avec des *initializer lists*) : ``plot( {1,2,3,4}, {1,2,3,4}, keywords )``.
    Équivalent à ::

        matplotlib.pyplot.plot(x, y, **keywords)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function:: bool plot(PyObject* xarray, PyObject* yarray, const std::map<std::string, std::string>& keywords)

    Appel la méthode ``matplotlib.pyplot.plot`` à partir de ``PyObject`` issus de l'appel à ``matplotlibcpp.get_array``.

    :param PyObject* xarray: Tableau ``numpy`` des abscisses.
    :param PyObject* yarray: Tableau ``numpy`` des ordonnées.
    :param map<string, string> keywords: Les arguments sous forme de clé acceptés par la méthode ``matplotlib.pyplot.plot``.

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   bool plot(PyObject* xarray, PyObject* yarray, const std::string& s = "")

    Appel la méthode ``matplotlib.pyplot.plot`` à partir de ``PyObject`` issus de l'appel à ``matplotlibcpp.get_array``.

    :param PyObject* xarray: Tableau ``numpy`` des abscisses.
    :param PyObject* yarray: Tableau ``numpy`` des ordonnées.
    :param string s: Format de la courbe (couleur, type des marqueurs, …)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   template <typename Numeric> \
                    bool named_plot(const std::string& name, const std::vector<Numeric>& y, const std::string& format = "")

    Équivalent à ::

        matplotlib.pyplot.plot(y, format, label=name)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   template <typename Numeric> \
                    bool named_plot(const std::string& name, const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::string& format = "")

    Équivalent à ::

        matplotlib.pyplot.plot(x, y, format, label=name, )

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   bool named_plot(const std::string& name, PyObject* yarray, const std::string& format = "")

    Appel la méthode ``matplotlib.pyplot.plot`` avec l'argument ``label=`` à partir de ``PyObject*`` issus de l'appel à ``matplotlibcpp.get_array``.

    :param string name: Label de la courbe.
    :param PyObject* yarray: Tableau ``numpy`` des ordonnées.
    :param string s: Format de la courbe (couleur, type des marqueurs, …)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. cpp:function::   bool named_plot(const std::string& name, PyObject* xarray, PyObject* yarray, const std::string& format = "")

    Appel la méthode ``matplotlib.pyplot.plot`` avec l'argument ``label=`` à partir de ``PyObject`` issus de l'appel à ``matplotlibcpp.get_array``.

    :param string name: Label de la courbe.
    :param PyObject* xarray: Tableau ``numpy`` des abscisses.
    :param PyObject* yarray: Tableau ``numpy`` des ordonnées.
    :param string s: Format de la courbe (couleur, type des marqueurs, …)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.plot`

.. rubric:: semilogx

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool semilogx(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.semilogx(x, y, s)``.

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.semilogx`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool named_semilogx(const std::string& name, const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.semilogx(x, y, s, label=name)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.semilogx`

.. rubric:: semilogy
.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool semilogy(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.semilogy(x, y, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.semilogy`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool named_semilogy(const std::string& name, const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.semilogy(x, y, s, label=name)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.semilogy`


.. rubric:: loglog
.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool loglog(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.loglog(x, y, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.loglog`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool named_loglog(const std::string& name, const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

     Équivalent à ::

        matplotlib.pyplot.loglog(x, y, s, label=name)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.loglog`



Cartes
``````
.. rubric:: Mapping logarithmique

Le maillage est décrit une grille cartésienne régulière (au sens numpy :py:func:`numpy.meshgrid`).

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique.
    Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"masked_value", double``
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        * ``"draw_iso", bool``
            Si ``true`` trace les iso-lignes intermédiaires (changement de couleurs).
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la carte.
        * ``"n_iso_pts", int``
            Nombre d'isolignes insérées entre chaque niveau de la carte colorée. Ignoré si ``draw_iso`` est ``false``.
        * ``"linewidths", float``
            Épaisseur des iso-lignes (si ``draw_iso`` vaut ``true``).
        * ``"max_decades", int``
            Si le nombre de decades entre z.min() et z.max est supérieur à ce nombre, alors le minimum des données est modifié (`n_decades` est pris en compte).
        * ``"n_decades", int``
            Nombre de decades souhaité en dessous de z.max()

    Exemples d'appel à cette méthode ::

        plt::named_logarithmic_contourf("label", x, y, z,
            make_tuple(
                "masked_valued", 0.,
                "draw_iso", false
               ));


    Appel la méthode python ``PyPlotCEA.logarithmic_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est une grille
    cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.logarithmic_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, long n_iso_pts, const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est une grille
    cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_iso_pts: Nombre d'iso-lignes entre 2 niveaux de coloration
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.logarithmic_contourf(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, const Numeric masked_value, \
                        const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est une grille
    cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const Numeric masked_value, long n_iso_pts, const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est une grille
    cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param long n_iso_pts: Nombre d'iso-lignes entre 2 niveaux de coloration
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.logarithmic_contourf(…)``.

    .. seealso::

         :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. rubric:: Mapping linéaire

Le maillage est décrit une grille cartésienne régulière (au sens numpy :py:func:`numpy.meshgrid`).

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_linear_contourf(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique.
    Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"n_levels", int``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"draw_iso", bool``
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        * ``"draw_colorbar", bool``
            Si ``True`` affiche la colorbar associée à la carte.
        * ``"linewidths", float``
            Épaisseur des iso-lignes

    Exemple d'appel à cette méthode ::

                plt::named_linear_contourf("linear", x, y, z,
                    make_tuple(
                                "draw_iso", true,
                                "n_levels", 22
                                ));

    Appel la méthode python ``PyPlotCEA.logarithmic_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. cpp:function::   template <typename Numeric> \
                    void named_linear_contourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, long n_levels = 10, \
                        const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur linéaire. Le maillage est une grille cartésienne
    régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_levels: Nombre de niveaux de couleurs à utiliser pour le mapping
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.linear_contourf(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

Isolignes
`````````

.. rubric:: mapping logarithmique

Le maillage est décrit une grille cartésienne régulière (au sens numpy :py:func:`numpy.meshgrid`).

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                    const std::tuple<Args...>& keywords)

    **Version générique** pour le tracer d'iso-lignes pour une répartition log. Il faut indiquer le nombre d'iso-lignes
    (n_levels) ou la liste des iso-valeurs. On prendra la liste de iso-valeurs si les 2 paramètres sont précisés, ou le
    nombre d'iso-lignes si rien n'est précisé. Le maillage est une grille cartésienne régulière décrite de manière
    implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"n_levels", int``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"levels", vector<double>``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"masked_value", double``
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        * ``"colors", "black"``
            Les iso-lignes auront toutes cette couleur.
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la carte.
        * ``"linewidths", float``
            Épaisseur des iso-lignes

    Appel la méthode python ``PyPlotCEA.logarithmic_contour(…)``.

    Exemple d'appel à cette méthode ::

        plt::named_logarithmic_contour("label", x, y, z,
            make_tuple(
                "masked_valued", 0.,
                "levels", levels,
                 "colors", "black",
                 "draw_colorbar", false
        ));

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const Numeric masked_value, long n_levels = 10)

    Tracer d'iso-lignes. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param long n_levels: Nombre d'iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const Numeric masked_value, const std::vector<Numeric>& levels)

    Tracer d'iso-lignes. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param vector<Numeric> levels: Les valeurs (niveaux) des iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        long n_levels = 10)

    Tracer d'iso-lignes. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_levels: Nombre d'iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_contour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const std::vector<Numeric>& levels)

    Tracer d'iso-lignes. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param vector<Numeric> levels: Les valeurs (niveaux) des iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. rubric:: mapping linéaire

Le maillage est décrit une grille cartésienne régulière (au sens numpy :py:func:`numpy.meshgrid`).

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_linear_contour(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                    const std::tuple<Args...>& keywords)

    **Version générique** pour le tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste
    des iso-valeurs. On prendra la liste de iso-valeurs si les 2 paramètres sont précisés, ou le nombre d'iso-lignes
    si rien n'est précisé. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string name: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"n_levels", int``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"levels", vector<double>``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"colors", "black"``
            Les iso-lignes auront toutes cette couleur.
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la carte.
        * ``"linewidths", float``
            Épaisseur des iso-lignes

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    Exemple d'appel à cette méthode ::

        plt::named_linear_contour("label", x, y, z,
            make_tuple(
                "levels", levels,
                "colors", "black",
                "draw_colorbar", true
        ));

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. cpp:function::   template <typename Numeric> \
                    void named_linear_contour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, long n_levels = 10)

    Tracer d'iso-lignes. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :param string name: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_levels: Nombre d'iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`

.. cpp:function::   template <typename Numeric> \
                    void named_linear_contour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, const std::vector<Numeric>& levels)

    Tracer d'iso-lignes. Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :param string name: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param vector<Numeric> levels: Les valeurs (niveaux) des iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_contour`

        :py:func:`matplotlib:matplotlib.pyplot.contourf`

        :py:func:`matplotlib:matplotlib.pyplot.contour`


.. rubric:: Mapping logarithmique avec triangulation

Le maillage est construit par triangulation de Delaunay à partir de la fourniture de la liste de tous les points. Voir
:py:func:`matplotlib.tri.Triangulation`.

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le
    maillage est construit par triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"masked_value", double``
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        * ``"draw_iso", bool``
            Si ``true`` trace les iso-lignes intermédiaires (changement de couleurs).
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la carte.
        * ``"n_iso_pts", int``
            Nombre d'isolignes insérées entre chaque niveau de la carte colorée. Ignoré si ``draw_iso`` est ``false``.
        * ``"linewidths", float``
            Épaisseur des iso-lignes (si ``draw_iso`` vaut ``true``).
        * ``"max_decades", int``
            Si le nombre de decades entre z.min() et z.max est supérieur à ce nombre, alors le minimum des données est modifié (`n_decades` est pris en compte).
        * ``"n_decades", int``
            Nombre de decades souhaité en dessous de z.max()

    Exemples d'appel à cette méthode ::

        plt::named_logarithmic_tricontourf("label", x, y, z,
            make_tuple(
                "masked_valued", 0.,
                "draw_iso", false
               ));


    Appel la méthode python ``PyPlotCEA.logarithmic_tricontourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`


.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.logarithmic_tricontourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, long n_iso_pts, const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_iso_pts: Nombre d'iso-lignes entre 2 niveaux de coloration
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.logarithmic_tricontourf(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`


.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, const Numeric masked_value, \
                        const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`


.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const Numeric masked_value, long n_iso_pts, const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param long n_iso_pts: Nombre d'iso-lignes entre 2 niveaux de coloration
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.logarithmic_tricontourf(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`


.. rubric:: Mapping linéaire avec triangulation

Le maillage est construit par triangulation de Delaunay à partir de la fourniture de la liste de tous les points. Voir
:py:func:`matplotlib.tri.Triangulation`.

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_linear_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique.
    Le maillage est construit par triangulation de Delaunay à partir de la fourniture de la liste de points
    (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"n_levels", int``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"draw_iso", bool``
            Si ``True`` trace les iso-lignes intermédiaires (changement de couleurs).
        * ``"draw_colorbar", bool``
            Si ``True`` affiche la colorbar associée à la carte.
        * ``"linewidths", float``
            Épaisseur des iso-lignes

    Exemple d'appel à cette méthode ::

                plt::named_linear_tricontourf("linear", x, y, z,
                    make_tuple(
                                "draw_iso", true,
                                "n_levels", 22
                                ));

    Appel la méthode python ``PyPlotCEA.logarithmic_tricontourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`


.. cpp:function::   template <typename Numeric> \
                    void named_linear_tricontourf(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, long n_levels = 10, \
                        const bool draw_iso=false)

    Affichage d'une carte de couleurs avec un mapping couleur / valeur linéaire. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_levels: Nombre de niveaux de couleurs à utiliser pour le mapping
    :param bool draw_iso: Affiche ou non des iso-lignes entre les changements de couleurs.

    Appel la méthode python ``PyPlotCEA.linear_tricontourf(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

Isolignes
`````````

.. rubric:: mapping logarithmique avec triangulation

Le maillage est construit par triangulation de Delaunay à partir de la fourniture de la liste de tous les points. Voir
:py:func:`matplotlib.tri.Triangulation`.

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                    const std::tuple<Args...>& keywords)

    **Version générique** pour le tracer d'iso-lignes pour une répartition log. Il faut indiquer le nombre d'iso-lignes
    (n_levels) ou la liste des iso-valeurs. On prendra la liste de iso-valeurs si les 2 paramètres sont précisés, ou le
    nombre d'iso-lignessi rien n'est précisé. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"n_levels", int``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"levels", vector<double>``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"masked_value", double``
            Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées)
        * ``"colors", "black"``
            Les iso-lignes auront toutes cette couleur.
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la carte.
        * ``"linewidths", float``
            Épaisseur des iso-lignes

    Appel la méthode python ``PyPlotCEA.logarithmic_tricontour(…)``.

    Exemple d'appel à cette méthode ::

        plt::named_logarithmic_tricontour("label", x, y, z,
            make_tuple(
                "masked_valued", 0.,
                "levels", levels,
                 "colors", "black",
                 "draw_colorbar", false
        ));

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.logarithmic_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const Numeric masked_value, long n_levels = 10)

    Tracer d'iso-lignes. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param long n_levels: Nombre d'iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_tricontour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const Numeric masked_value, const std::vector<Numeric>& levels)

    Tracer d'iso-lignes. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param Numeric masked_value: Valeur à masquer (e.g. en général 0. pour un mapping logarithmique)
    :param vector<Numeric> levels: Les valeurs (niveaux) des iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_contour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        long n_levels = 10)

    Tracer d'iso-lignes. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_levels: Nombre d'iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_tricontour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. cpp:function::   template <typename Numeric> \
                    void named_logarithmic_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                        const std::vector<Numeric>& levels)

    Tracer d'iso-lignes. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string s: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param vector<Numeric> levels: Les valeurs (niveaux) des iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_tricontour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. rubric:: mapping linéaire avec triangulation

Le maillage est construit par triangulation de Delaunay à partir de la fourniture de la liste de tous les points. Voir
:py:func:`matplotlib.tri.Triangulation`.

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_linear_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                    const std::vector<Numeric>& y, const std::vector<Numeric>&z, \
                    const std::tuple<Args...>& keywords)

    **Version générique** pour le tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste
    des iso-valeurs. On prendra la liste de iso-valeurs si les 2 paramètres sont précisés, ou le nombre d'iso-lignes
    si rien n'est précisé. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string name: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"n_levels", int``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"levels", vector<double>``
            Nombre de niveaux de couleurs (10 par défaut)
        * ``"colors", "black"``
            Les iso-lignes auront toutes cette couleur.
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la carte.
        * ``"linewidths", float``
            Épaisseur des iso-lignes

    Appel la méthode python ``PyPlotCEA.linear_tricontour(…)``.

    Exemple d'appel à cette méthode ::

        plt::named_linear_tricontour("label", x, y, z,
            make_tuple(
                "levels", levels,
                "colors", "black",
                "draw_colorbar", true
        ));

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`


.. cpp:function::   template <typename Numeric> \
                    void named_linear_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, long n_levels = 10)

    Tracer d'iso-lignes. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :param string name: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param long n_levels: Nombre d'iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_tricontour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

.. cpp:function::   template <typename Numeric> \
                    void named_linear_tricontour(const std::string& s, const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, const std::vector<Numeric>&z, const std::vector<Numeric>& levels)

    Tracer d'iso-lignes. Le maillage est construit par
    triangulation de Delaunay à partir de la fourniture de la liste de points (``x`` et ``y``).

    :param string name: Label de la carte de couleurs (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param vector<Numeric> levels: Les valeurs (niveaux) des iso-lignes à tracer.

    Appel la méthode python ``PyPlotCEA.linear_tricontour(…)``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.linear_tricontour`

        :py:func:`matplotlib:matplotlib.pyplot.tricontourf`

        :py:func:`matplotlib:matplotlib.pyplot.tricontour`

Utilitaires cartes et isolignes
```````````````````````````````

.. cpp:function:: std::vector<double> get_contourf_levels()

    Obtention des niveaux de couleurs de la dernière carte affichée via une des méthodes en *contourf*.
    La carte est de type :py:attr:`matplotlib.contour.QuadContourSet` ou :py:attr:`matplotlib.contour.TriContourSet`.

    :return: un tableau C++ de valeurs
    :rtype: :cpp:expr:`vector<Numeric>`

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.get_contourf_levels`

        :py:func:`nemesis.plotter.PyPlotCEA.get_contourf_layers`

.. cpp:function:: std::vector<double> get_contourf_layers()

    Obtention des *layers* de la dernière carte affichée via via une des méthodes en *contourf*.
    La carte est de type :py:attr:`matplotlib.contour.QuadContourSet` ou :py:attr:`matplotlib.contour.TriContourSet`.

    Un *layer* est à mi-chemin entre les niveaux (voir _process_colors())

    :return: un tableau C++ de valeurs
    :rtype: :cpp:expr:`vector<Numeric>`

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.get_contourf_levels`

        :py:func:`nemesis.plotter.PyPlotCEA.get_contour_levels`

.. cpp:function:: std::vector<double> get_contour_levels()

    Obtention des valeurs des dernières isolignes affichées via une des méthodes en *contour*.
    La carte est de type :py:attr:`matplotlib.contour.QuadContourSet` ou :py:attr:`matplotlib.contour.TriContourSet`.

    N.B. : Les *layers* sont identiques aux niveaux (*levels*) pour les isolignes.

    :return: un tableau C++ de valeurs
    :rtype: :cpp:expr:`vector<Numeric>`

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.get_contourf_levels`

        :py:func:`nemesis.plotter.PyPlotCEA.get_contourf_layers`


Graphiques divers
`````````````````

.. cpp:function::   template <typename Numeric> \
                    bool stem(const std::vector<Numeric>& x, const std::vector<Numeric>& y, const std::map<std::string, std::string>& keywords)

    Créer un *stem* plot (trace des lignes verticales pour chaque x à partir d'une ligne de base en y, et place des marqueurs dessus).

    Équivalent à ::

        matplotlib.pyplot.stem(x, y, **keywords)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.stem`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool stem(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.stem(x, y, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.stem`

.. cpp:function::   template <typename Numeric> \
                    bool stem(const std::vector<Numeric>& y, const std::string& format = "")

     Équivalent à ::

        matplotlib.pyplot.stem(y, format)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.stem`


.. cpp:function::   template <typename Numeric> \
                    bool fill_between(const std::vector<Numeric>& x, const std::vector<Numeric>& y1, const std::vector<Numeric>& y2, const std::map<std::string, std::string>& keywords)

    Remplissage de la surface entre 2 courbes horizontales.

    Équivalent à ::

        matplotlib.pyplot.fill_between(x, y1, y2, **keywords)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.fill_between`


.. cpp:function::   template <typename Numeric> \
                    bool hist(const std::vector<Numeric>& y, long bins = 10, std::string color = "b", double alpha = 1.0)

    Trace un histogramme avec ``bins + 1`` arrêtes de couleur ``color`` (noir par défaut) avec la transparence ``alpha`` (opaque par défaut).

    Équivalent à ::

        matplotlib.pyplot.hist(y, bins=..., color=..., alpha=...)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.errorbar`

.. cpp:function::   template <typename Numeric> \
                    bool named_hist(std::string label, const std::vector<Numeric>& y, long bins = 10, std::string color = "b", double alpha = 1.0)

    Appel la méthode python ``matplotlib.pyplot.hist(y, label=label, bins=..., color=..., alpha=...)``.
    Trace un histogramme avec ``bins + 1`` arrêtes de couleur ``color`` (noir par défaut) avec la transparence ``alpha`` (opaque par défaut), et le nomme
    ``label``.

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.hist`

.. cpp:function::   template <typename Numeric> \
                    bool bar(const std::vector<Numeric>& y, const std::string& ec = "black", const std::string& ls = "-", double lw = 1.0, \
                        const std::map<std::string, std::string>& keywords = \
                            std::map<std::string, std::string>())

    Équivalent à ::

        matplotlib.pyplot.bar(x, y, yerr, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.bar`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool errorbar(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::vector<NumericX>& yerr, const std::string& s)

    Équivalent à ::

        matplotlib.pyplot.errorbar(x, y, yerr, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.hist`

.. cpp:function::   template <typename NumericX, typename NumericY> \
                    bool errorbar(const std::vector<NumericX>& x, const std::vector<NumericY>& y, const std::vector<NumericX>& yerr, \
                        const std::map<std::string, std::string> &keywords = \
                            std::map<std::string, std::string>())

    Équivalent à ::

        matplotlib.pyplot.errorbar(x, y, yerr, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.errorbar`

.. cpp:function:: template <typename Numeric> \
                    bool scatter(const std::vector<Numeric>& x, const std::vector<Numeric>& y, \
                        const std::string& marker="o", const std::string color="black", \
                        double size = 36.0)

    Affichage d'un nuage de points.

    Liste (non-exhaustive) des marqueurs possibles (valables aussi pour ``plot``) :

        * "."   Un point
        * ","   Un pixel
        * "o"   Un cercle (le défaut pour ``scatter``)
        * "V"   Un triangle vers le bas
        * "^"   Un triangle vers le haut

    Pour la liste complète des marqueurs, voir :py:mod:`matplotlib:matplotlib.markers`.

    Équivalent à ::

        matplotlib.pyplot.scatter(x, y, marker="o", c="black", s=36.0)


    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.scatter`


.. cpp:function::   template <typename Numeric, class... Args> \
                    void scatter(const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, \
                        const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage de nuage de points.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"s", [double | vector<double>]``
            Taille des marqueurs (1 seul ou 1 par point).
        * ``"c", ["string" | vector<double>]``
            Couleurs des marqueurs (une chaîne ou une valeur par point).
        * ``"marker", ["string" | int]``
            Type du marqueur.
        * ``"cmap", "string"``
            Carte de couleur à utiliser.
        * ``"alpha", double``
            Transparence des marqueurs.

    Exemple d'appel à cette méthode ::

        plt::scatter(x, y,
             make_tuple(
                     "marker", ".",
                     "c", z,
                     "s", 20.,
                     "cmap", "bone",
                     "alpha", 0.5
             ));

    Pour la liste complète des marqueurs, voir :py:mod:`matplotlib:matplotlib.markers`.

    .. seealso::

        :py:func:`matplotlib:matplotlib.pyplot.scatter`

.. cpp:function:: template <typename Numeric> \
                    bool quiver(const std::vector<NumericX>& x, const std::vector<NumericY>& y, \
                        const std::vector<NumericU>& u, const std::vector<NumericW>& w, \
                        const std::map<std::string, std::string>& keywords = \
                            std::map<std::string, std::string>())

    Affichage d'un champs de vecteurs.

    Équivalent à ::

        matplotlib.pyplot.quiver(x, y, u, w)


    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.scatter`


Graphiques 3D
`````````````

.. cpp:function:: template <typename Numeric> \
                    void plot_surface(const std::vector<::std::vector<Numeric>> &x, \
                        const std::vector<::std::vector<Numeric>> &y, \
                        const std::vector<::std::vector<Numeric>> &z, \
                        const std::map<std::string, std::string> &keywords = \
                            std::map<std::string, std::string>())

    Affichage d'une surface (dans un espace 3D)

    Équivalent à ::

        fig = plt.gcf()
        ax = fig.gca(projection='3d')
        ax.plot_surface(x, y, z)


    .. seealso:: :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface`

.. cpp:function:: template <typename Numeric> \
                    void plot3D(const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, \
                        const std::vector<Numeric>& z, \
                        const std::string& zdir="z" )

    Affiche une courbe (dans un espace 3D).
    ``z`` peut ne contenir qu'une valeur ou le même nombre de valeurs que x et y.

    Équivalent à ::

        fig = plt.gcf()
        ax = fig.gca(projection='3d')
        ax.plot3D(x, y, z)

    .. seealso:: :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.plot3D`

.. cpp:function::   template <typename Numeric, class... Args> \
                    void raised_contourff(const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, \
                        const std::vector<Numeric>&z, \
                        const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage d'une surface (contourf 2D dans un espace 3D).
    Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"label", double``
            Label de surface (sera mis comme titre de l'échelle de couleurs).
        * ``"draw_colorbar", bool``
            Si ``true`` affiche la colorbar associée à la surface.
        * ``"cmap", string``
            Palette de couleurs à utiliser. Si ``"current"`` est utilisé alors se sera la palette courante.
        * ``"xscale", 'log'``
            Si on veut l'axe des abscisses en log
        * ``"yscale", 'log'``
            Si on veut l'axe des abscisses en log
        * ``"zscale", 'log'``
            Si on veut l'axe des abscisses en log
        * ``"max_decades", int``
            Si le nombre de decades entre z.min() et z.max est supérieur à ce nombre, alors le minimum des données est modifié (`n_decades` est pris en compte).
        * ``"n_decades", int``
            Nombre de decades souhaité en dessous de z.max()

    Exemples d'appel à cette méthode ::

        plt::raised_contourf(x_c, y_c, z_c, make_tuple(
                                "draw_colorbar", true,
                                "rcount", 10.,
                                "ccount", 10.,
                                "cmap", "current"));

    Appel la méthode python ``PyPlotCEA.raised_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.raised_contourf`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D`

.. cpp:function::   template <typename Numeric> \
                    void raised_contourf(const std::vector<Numeric>& x, \
                            const std::vector<Numeric>& y, \
                            const std::vector<Numeric>&z)


    Affichage d'une carte de couleurs avec un mapping couleur / valeur logarithmique.
    Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??

    Exemples d'appel à cette méthode ::

        plt::raised_contourf(x_c, y_c, z_c);


    Appel la méthode python ``PyPlotCEA.raised_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.raised_contourf`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D`

.. cpp:function::   template <typename Numeric, class... Args> \
                    void named_raised_contourf(const std::string& s, \
                        const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, \
                        const std::vector<Numeric>&z, \
                        const std::tuple<Args...>& keywords)

    **Version générique** pour l'affichage d'une surface (contourf 2D dans un espace 3D).
    Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :tparam class... Args: type du tuple dans lequel seront passés les arguments complémentaires.
    :param string: Label de surface (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??
    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"draw_colorbar", bool``
            Si ``true`` trace l'échelle de couleurs associée à la surface.
        * ``"cmap", string``
            Palette de couleurs à utiliser. Si ``"current"`` est utilisé alors se sera la palette courante.
        * ``"xscale", 'log'``
            Si on veut l'axe des abscisses en log
        * ``"yscale", 'log'``
            Si on veut l'axe des abscisses en log
        * ``"zscale", 'log'``
            Si on veut l'axe des abscisses en log
        * ``"max_decades", int``
            Si le nombre de decades entre z.min() et z.max est supérieur à ce nombre, alors le minimum des données est modifié (`n_decades` est pris en compte).
        * ``"n_decades", int``
            Nombre de decades souhaité en dessous de z.max()

    Exemples d'appel à cette méthode ::

        plt::named_raised_contourf(x_c, y_c, z_c, make_tuple(
                                "draw_colorbar", true,
                                "rcount", 10.,
                                "ccount", 10.,
                                "cmap", "current"));

    Appel la méthode python ``PyPlotCEA.raised_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.raised_contourf`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D`

.. cpp:function::   template <typename Numeric> \
                    void named_raised_contourf(const std::string& s, \
                        const std::vector<Numeric>& x, \
                        const std::vector<Numeric>& y, \
                        const std::vector<Numeric>&z)


    **Version générique** pour l'affichage d'une surface (contourf 2D dans un espace 3D).
    Le maillage est une grille cartésienne régulière décrite de manière implicite : ``x`` x ``y``.

    :tparam Numeric: type des nombres (int, long, float, double, ...)
    :param string: Label de surface (sera mis comme titre de l'échelle de couleurs).
    :param vector<Numeric> x: Tableau des abscisses
    :param vector<Numeric> x: Tableau des ordonnées
    :param vector<Numeric> z: Tableau des ??

    Exemples d'appel à cette méthode ::

        plt::named_raised_contourf(x_c, y_c, z_c);


    Appel la méthode python ``PyPlotCEA.raised_contourf``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.raised_contourf`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.plot_surface`

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D`

.. cpp:function:: void load_3d_modules()

    Importe les modules nécessaires pour afficher dans une boîte 3D.

    .. warning:: La fonction ne doit pas être appelée directement, elle est appelée par les fonctions d'affichages 3D (*e.g.* ``plot_surface`` et ``plot3D``).

.. cpp:function:: bool is_3d()

    Retourne ``true`` si la boîte d'axes courante est du type "3d" ; ``false`` sinon.
    Appel ``pyplot.gca().name`` est retourne le résultat de la comparaison avec ``3d``.


Annotations et textes
`````````````````````

.. cpp:function:: void suptitle(const std::string& titlestr)

    Équivalent à ::

        matplotlib.pyplot.suptitle

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.suptitle`

.. cpp:function::   void title(const std::string& titlestr)

    Équivalent à ::

        matplotlib.pyplot.title(titlestr)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.title`

.. cpp:function::   template<typename Numeric> void text(Numeric x, Numeric y, const std::string& s = "")

    Équivalent à ::

        matplotlib.pyplot.text(x, y, s)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.annotate`

.. cpp:function::   void xlabel(const std::string& str)

    Équivalent à ::

        matplotlib.pyplot.xlabel(str)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.xlabel`

.. cpp:function::   void ylabel(const std::string& str)

    Équivalent à ::

        matplotlib.pyplot.ylabel(str)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.ylabel`

.. cpp:function::   template<typename Numeric> \
                    void xticks(const std::vector<Numeric> &ticks, const std::vector<std::string> &labels = std::vector<std::string>(), \
                        const std::map<std::string, std::string>& keywords = std::map<std::string, std::string>())

    Équivalent à ::

        matplotlib.pyplot.xticks(ticks, labels)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.yticks`

.. cpp:function::   template<typename Numeric> \
                    void yticks(const std::vector<Numeric> &ticks, const std::vector<std::string> &labels = std::vector<std::string>(), \
                        const std::map<std::string, std::string>& keywords = std::map<std::string, std::string>())

    Équivalent à ::

        matplotlib.pyplot.xyicks(ticks, labels)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.xticks`

.. cpp:function::   bool annotate(std::string annotation, double x, double y)

    Annote le point ``(x, y)`` avec le texte ``annotation``. Le point se situe dans la boîte d'axes.

    Équivalent à ::

        matplotlib.pyplot.annotate((annotation,), xy=(x, y))

    :param string annotation: texte de l'annotation.
    :param double x: abscisse du point à annoter
    :param double y: ordonnée du point à annoter.

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.annotate`

.. cpp:function::   void set_titles(std::string title, std::string sub_title, std::strSing third_title)

    Appel la méthode python ``PyPlotCEA.set_titles(title, sub_title, third_title)``.

    :param string title: Titre de la figure.
    :param string sub_title: Sous-titre de la figure.
    :param string third_title: Titre pour les matériaux.

    Par défaut :
    * title sera en position en (0.1, 0.95) en taille 14.
    * sub_title sera en position (0.05, 0.9) en taille 10.
    * third_title sera en position (<calc>, 0.95) en taille 14. ``<calc> = 1. - 0.012 * third_title.size() - 0.1``.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_titles`

.. cpp:function::   void set_titles(std::string title, double x0, double y0, long fs0, \
                        std::string sub_title, double x1, double y1, long fs1, \
                        std::string third_title, double x2, double y2, long fs2)

    :param string title: Titre de la figure.
    :param double x0: Abscisse du titre
    :param double y0: Ordonnée du titre
    :param long fs0: Taille de la police du titre.
    :param string sub_title: Sous-titre de la figure.
    :param double x1: Abscisse du sous-titre.
    :param double y1: Ordonnée du sous-titre.
    :param long fs1: Taille de la police du sous-titre.
    :param string third_title: Titre pour les matériaux.
    :param double x2: Abscisse du troisième titre (Si x2==-1, la position sera calculée suivant la lg de third_title).
    :param double y2: Ordonnée du troisième titre.
    :param long fs2: Taille de la police du troisième titre (défaut: ).

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_titles`

.. cpp:function::   void legend()

    Affiche la légende.

    Équivalent à ::

        matplotlib.pyplot.legend()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.legend`

.. cpp:function::   void under_axes_legend()

    Appel la méthode python ``PyPlotCEA.under_axes_legend()``.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.under_axes_legend`

.. cpp:function::   template <typename Numeric> \
                    void networks_legend(const std::vector<std::string>& labels, const std::vector<Numeric>& v_idx0)

    Appel la méthode python ``PyPlotCEA.networks_legend(labels, v_idx0)``.

    :param vector<string> labels: Tableau des labels des réseaux.
    :param vector<Numeric> v_idx0: Tableau des indices des premières courbes de chaque réseau.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.networks_legend`


Personnalisation des axes et des courbes
````````````````````````````````````````

.. cpp:function::   template <typename Numeric> \
                    void xlim(Numeric left, Numeric right)

    Équivalent à ::

        matplotlib.pyplot.xlim(left, right)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.xlim`

.. cpp:function::   double* xlim()

    Équivalent à ::

        matplotlib.pyplot.xlim()

    Retourne un tableau de double de 2 éléments.

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.xlim`

.. cpp:function::   template <typename Numeric> \
                    void ylim(Numeric left, Numeric right)

    Équivalent à ::

        matplotlib.pyplot.ylim(left, right)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.ylim`

.. cpp:function::   double* ylim()

     Équivalent à ::

        matplotlib.pyplot.ylim()

    Retourne un tableau de double de 2 éléments.

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.ylim`

.. cpp:function::   template <typename Numeric> \
                    void zlim(Numeric bottom, Numeric top)

    Même action que xlim(left, right) et ylim(bottom, top) pour l'axe Z (les cotes).

    .. warning:: **Uniquement pour une boîte d'axes 3D.**

    .. seealso:: :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.set_zlim`

.. cpp:function::   double* zlim()

    Identique à top, bottom = ylim()

    Retourne un tableau de double de 2 éléments.

    .. warning:: **Uniquement pour une boîte d'axes 3D.**

    .. seealso:: :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.get_zlim`

.. cpp:function::   void set_log_scale(bool xscale, bool yscale)

    Appel la méthode python ``PyPlotCEA.set_log_scale(xscale, yscale)``.

    :param bool xscale: Indique si l'axe des abscisses doit être mis en log.
    :param bool yscale: Indique si l'axe des ordonnées doit être mis en log.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_log_scale`

.. cpp:function::   template <typename Numeric> \
                    void set_linewidth(Numeric linewidth)

    Appel la méthode python ``PyPlotCEA.set_linewidth(linewidth)``.

    :param Numeric linewidth: Épaisseur par défaut qui sera appliquée à toutes les lignes affichées après cet appel.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_linewidth`


.. cpp:function::   template <typename Numeric> \
                    void set_linewidth_to_all(Numeric linewidth)

    Appel la méthode python ``PyPlotCEA.set_linewidth_to_all(linewidth)``.

    :param Numeric linewidth: Application de cette épaisseur à toutes les courbes.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_linewidth_to_all`

.. cpp:function::   void highlight_curve(long num, double width)

    Appel la méthode python ``PyPlotCEA.highlight_curve(num, width)``.

    :param long num: Indice de la courbe dont l'épaisseur doit être modifiée.
    :param double width: Nouvelle épaisseur.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.highlight_curve`

.. cpp:function::   void show_curve(long num)

    Appel la méthode python ``PyPlotCEA.show_curve(num)``.

    :param long num: Affichage de la i-ème courbe.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.show_curve`

.. cpp:function::   void show_all_curves()

    Appel la méthode python ``PyPlotCEA.show_all_curves()``.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.show_all_curves`

.. cpp:function::   void hide_curve(long num)

    Appel la méthode python ``PyPlotCEA.hide_curve(num)``.

    :param long num: Masquage de la i-ème courbe.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.hide_curve`


Utilitaires - Manipulation de tableaux
``````````````````````````````````````

.. cpp:function::   template <typename Numeric> \
                    PyObject* get_array(const std::vector<Numeric>& v)

    Convertit un tableau C++ de *Numeric* en objet Python.

    :param vector<Numeric> v: vecteur à convertir en ``PyObject``.
    :return: un tableau ou une liste de double
    :rtype: :py:func:`numpy:numpy.array` or list[double]

.. cpp:function::   template <typename Numeric> \
                    PyObject* get_array(const unsigned int n, const Numeric* v)

    Retourne un tableau ``numpy`` (si ``numpy`` existe) à partir d'un tableau ``C`` . Sinon retourne une liste de ``Numeric``.

    :param uint n: Taille du tableau ``v``.
    :param Numeric* v: Tableau de type ``C``.
    :return: un tableau ``numpy``.
    :rtype: :cpp:expr:`PyObject*`

.. cpp:function::   template <typename Numeric> \
                    std::vector<Numeric> pyobject_to_vector(PyObject* po)

    Conversion d'un ``PyObject`` (qui stocke en fait un ``PyArrayObject`` de ``numpy``) en un ``vector<Numeric>``.

   :tparam Numeric: type des nombres (int, long, float, double, ...)
    :return: un tableau (C++) de valeurs
    :rtype: :cpp:expr:`vector<Numeric>`

.. cpp:function::   PyObject* get_pyobject_from(const int value)

    Conversion d'un ``int`` en un ``PyObject`` représentant un ``long``.

    :return: un PyObject* représentant un ``long``
    :rtype: :cpp:expr:`PyObject*`

.. cpp:function::   PyObject* get_pyobject_from(const char* value)

    Conversion d'un ``char*`` en un ``PyObject`` représentant une chaîne de caractères.

    :return: un PyObject* représentant une chaîne de caractères.
    :rtype: :cpp:expr:`PyObject*`

.. cpp:function::   PyObject* get_pyobject_from(const std::string& value)

    Conversion d'une ``string`` en un ``PyObject`` représentant une chaîne de caractères.

    :return: un PyObject* représentant une chaîne de caractères.
    :rtype: :cpp:expr:`PyObject*`


Utilitaires - Gestion fenêtre et axes
`````````````````````````````````````

.. cpp:function::   void view_init(double elevation, double azimuth)

    Spécification de l'élévation et de l'azimuth des axes.
    ``elevation`` stocke l'angle d'élévation du plan Z, ``azimuth`` stocke l'angle d'azimuth dans le plan (X, Y).

    .. warning:: **Uniquement pour une boîte d'axes 3D.**

    .. seealso:: :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.view_init`


.. cpp:function::   void subplot(long nrows, long ncols, long plot_number)

    Équivalent à ::

        matplotlib.pyplot.subplot(nrows, ncols, plot_number)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.subplot`

.. cpp:function::   void subplots_adjust(const std::map<std::string, double>& keywords = std::map<std::string, double>())

    Équivalent à ::

        matplotlib.pyplot.subplots_adjusts(keywords)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.subplot`

.. cpp:function::   void figure()

    Équivalent à ::

        matplotlib.pyplot.figure()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.figure`

.. cpp:function::   template <typename Identity> \
                    void figure(Identity num)

    Création d'une nouvelle figure (*i.e.* une fenêtre graphique au sens ``matplotlib``).

    Si une figure de nom ``num`` existe déjà, l'appel à cette méthode la rend active. Sinon une nouvelle figure est créée.

    :tparam Identity: type de l'idendifiant de la fenêtre (entier ou chaîne de caractères).
    :param num: Identifiant de la fenêtre à créer ou activer.

        * Si ``num`` est un entier alors le titre de cette fenêtre sera ``Figure <num>``.
        * Si ``num`` est une chaîne de caractères alors le titre de la fenêtre sera ``num``.

    Équivalent à ::

        matplotlib.pyplot.figure(num)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.figure`

.. cpp:function::   template <typename Identity, class... Args> \
                    void figure(Identity num, const std::tuple<Args...>& keywords)

    Création d'une nouvelle figure (*i.e.* une fenêtre graphique au sens ``matplotlib``).

    Si une figure de nom ``num`` existe déjà, l'appel à cette méthode la rend active. Sinon une nouvelle figure est créée.


    :tparam Identity: type de l'idendifiant de la fenêtre (entier ou chaîne de caractères).
    :param num: Identifiant de la fenêtre à créer ou activer

        * Si ``num`` est un entier alors le titre de cette fenêtre sera ``Figure <num>``.
        * Si ``num`` est une chaîne de caractères alors le titre de la fenêtre sera ``num``.

    :param tuple<Args...> keywords: tuple des paramètres complémentaires. Ils sont passés sous forme ``clé, valeur`` :

        * ``"dpi", integer``
            Résolution de la figure (si non fournit le défaut est 100)
        * ``"facecolor", char``
            Couleur du fond (si non fournit le défaut est w (white))
        * ``"edgecolor", char``
            Couleur de la bordure (si non fournit le défaut est w (white))
        * ``"frameon", bool``
            Défaut : True. Si False, la frame de la figure n'est pas dessinée
        * ``"clear", bool``
            Défaut : False. Si True et si la figure existe déjà, alors son contenu sera effacé
        * ``"figsize", vector<int>(2)"`` (width, height)
            Taille de la fenêtre (width, height). Il faut faire explicitement un ``vector<int>`` pour obtenir un tuple d'entiers.

    Équivalent à ::

        matplotlib.pyplot.figure(num)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.figure`

.. cpp:function::   void axis(const std::string& axisstr)

    Équivalent à ::

        matplotlib.pyplot.axis(axisstr)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.axis`

.. cpp:function::   void tight_layout()

    Équivalent à ::

        matplotlib.pyplot.tight_layout()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.tight_layout`

.. cpp:function::   void grid(bool flag)

    Équivalent à ::

        matplotlib.pyplot.grid(flag)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.grid`

.. rubric:: **Si et seulement si CEA_ADDON est défini.**

.. cpp:function::   void nemesis_params(string cmap_name)

    Appel la méthode python ``PyPlotCEA.nemesis_params("viridis")``.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.nemesis_params`

.. cpp:function::   void set_cmap()

    Appel la méthode python ``PyPlotCEA.set_cmap()``.

    .. seealso::

        :py:func:`nemesis.plotter.PyPlotCEA.set_cmap`

        :doc:`Page de référence des colormaps <matplotlib:gallery/color/colormap_reference>`


.. cpp:function::   template <typename Numeric> \
                    void set_figure_xy(Numeric x, Numeric y, Numeric width, Numeric height)

    Appel la méthode python ``PyPlotCEA.set_figure_xy(x, y, width, height)``.

    :param Numeric x: Abscisse de la position de la figure.
    :param Numeric y: Ordonnée de la position de la figure.
    :param Numeric width: Largeur de la figure.
    :param Numeric height: Hauteur de la figure.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_figure_xy`

.. cpp:function::   template <typename Numeric> \
                    void set_figure_size(Numeric width, Numeric height)

    Appel la méthode python ``PyPlotCEA.set_figure_size(width, height)``.

    :param Numeric width: Largeur de la figure.
    :param Numeric height: Hauteur de la figure.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.set_figure_size`

.. cpp:function::   void set_window_title(std::string title)

    Appel la méthode python ``PyPlotCEA.set_window_title(title)``.
    **Ne fonctionne que si le backend 'Qt5Agg' est utilisé.**
    Si vous voulez donner un type, créer explicitement la figure via ``figure('mon titre'), au lieu de laisser
    ``matplotlib`` la créée.

    :param string x: Titre à donner à la fenêtre graphique.



Utilitaires - Affichage and co.
```````````````````````````````

.. cpp:function::   void show(const bool block = true)

    Équivalent à ::

        matplotlib.pyplot.show(block)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.show`

.. cpp:function::   void draw()

    Équivalent à ::

        matplotlib.pyplot.draw()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.draw`

.. cpp:function::   void clf()

    Équivalent à ::

        matplotlib.pyplot.clf()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.clf`

.. cpp:function::   void cla()

    Équivalent à ::

        matplotlib.pyplot.cla()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.cla`

.. cpp:function:: PyObject* switch_axes(const std::string& projection="rectilinear")

    Créer si nécessaire une boite d'axes du type ``projection``. Fonctions appelées par les fonctions d'affichage de
    graphiques 3D, mais l'utilisateur peut l'appeler (en ignorant l'objet retourné) si il souhaite utiliser la fonction
    ``cla()`` – *clear axes* plutôte que ``clf()`` – *clear figure*.

    Les projections possibles sont :

        * "aitoff"
        * "hammer"
        * "lambert"
        * "mollweide"
        * "polar"
        * "rectilinear" (défaut)
        * "3d"

.. cpp:function::   template <typename Numeric> \
                    void pause(Numeric interval)

    Équivalent à ::

        matplotlib.pyplot.pause(inteval)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.pause`

.. cpp:function::   void close()

    Équivalent à ::

        matplotlib.pyplot.close()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.close`

.. cpp:function::   template <typename Identity> \
                    void close(Identity num)

    Fermeture de la fenêtre figure.

    :tparam Identity: int ou str
    :param num: Identifiant de la fenêtre figure à fermer.

    .. note:: L'appel ``close("all")`` va fermer toutes les figures qui ont été créées.

    Équivalent à ::

        matplotlib.pyplot.close()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.close`

.. cpp:function::   void save(const std::string& filename)

    Équivalent à ::

        pylab.savefig(filename)

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.savefig`

.. cpp:function:: void load(const std::string& filename)

    Appel la méthode python ``PyPlotCEA.load_image(mon_image)``.

    :param string filename: Nom de l'image à afficher.

    .. seealso:: :py:func:`nemesis.plotter.PyPlotCEA.load_image`


Utilitaires - Divers
````````````````````

.. cpp:function::   void backend(const std::string& name)

    Modification du *backend* par défaut de matplotlib.

    :param str name: nom du backend (*e.g.* 'Qt5Agg', 'Agg', …)

    .. warning::
        **Doit être appelé avant le premier appel à** ``matplotlib``.

.. cpp:function::   void xkcd()

    Équivalent à ::

        matplotlib.pyplot.xkcd()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.xkcd`

.. cpp:function::   void ion()

    Équivalent à ::

        matplotlib.pyplot.ion()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.ion`

.. cpp:function:: void ioff()

    Équivalent à ::

        matplotlib.pyplot.ioff()

    .. seealso:: :py:func:`matplotlib:matplotlib.pyplot.ioff`

.. cpp:function:: std::vector<std::array<double, 2>> \
                    ginput(const int numClicks = 1, const std::map<std::string, std::string>& keywords = \
                        map<string, string>())

    Permet de récupérer la position x,y lors d'un clic souris.

    Équivalent à ::

        matplotlib.pyplot.ginput(numClicks)



Namespace ``matplotlibcpp.detail``
----------------------------------

.. cpp:namespace-push:: detail

.. cpp:function::   static _interpreter& get()

    L'interpreteur Python. Un seul. Retourne un pointeur sur la classe ``_interpreter``.

.. cpp:class::      _interpreter

.. cpp:function::   _interpreter()

    Constructeur. Appel à ``Py_Initialize()``, ajout des chemins utiles, import de ``numpy``.

    .. seealso:: :c:func:`python:Py_Finalize`

.. cpp:function::   ~__interpreter()

    Destructeur. Appel à ``Py_Finalize()``;

    .. seealso:: :c:func:`Py_Finalize`

.. rubric:: **Si et seulement si CEA_ADDON est défini**


.. cpp:function:: static bool exists(const std::string& pathname)

    Retourne ``True`` si le chemin ``pathname`` existe dans l'arborescence de fichiers.

    :param str pathname: chemin dont l'existence veut être tester (répertoire ou fichier)

.. cpp:function:: static void add_site(const std::string& new_path)

    Ajoute ``new_path`` dans les chemins possibles contenant des modules python.

    :param str new_path: arborescence à ajouter à sys.path pour chercher les packages et modules python.

.. cpp:function:: static void print_sys_path()

    Exécute la commande python ``sys.path``.

    .. seealso:: :py:mod:`sys`

