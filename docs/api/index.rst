.. _api-index:

####################
  The nemesis API
####################

.. only:: builder_html

    :Release: |version|
    :Date: |today|

.. toctree::
    :maxdepth: 2

    api_python
    api_cpp
    ../nemesis_glossary

Documentation utile
===================

Matplotlib
~~~~~~~~~~

* :doc:`toute la documentation <matplotlib:index>`
* les marqueurs de points : :py:mod:`matplotlib:matplotlib.markers`
* :doc:`liste des colormaps <matplotlib:gallery/color/colormap_reference>`

Divers
~~~~~~
* :doc:`doc numpy <numpy:user/index>`

