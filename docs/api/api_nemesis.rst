.. _api_nemesis:

====================
 ``nemesis`` module
====================

.. currentmodule:: nemesis

.. automodule:: nemesis
   :no-members:
   :no-inherited-members:

.. automodule:: nemesis.run_tests

