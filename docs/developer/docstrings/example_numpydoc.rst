.. _example_numpydoc:

Docstrings version numpy
========================


Source
++++++

.. literalinclude:: example_numpydoc.py
   :language: python


Rendu
+++++

.. automodule:: example_numpydoc
    :noindex: