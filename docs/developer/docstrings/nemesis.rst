.. _nemesis_docstring:

========================
nemesis docstring guide
========================

Les *docstrings* de ``nemesis`` sont au format ``numpy``.
Pour la génération dans PyCharm : File > Settings > Tools > Python Integrated Tools, à la rubrique ``docstrings``, choisir ``NumPy``.

Exemple de *docstrings*::

    """
    Tracer d'iso-lignes. Il faut indiquer le nombre d'iso-lignes (n_levels) ou la liste des iso-valeurs.

    On prendra la liste de iso-valeurs si les 2 sont précisés, ou le nombre d'iso-lignes si rien n'est
    précisé.
    Version ``nemesis`` de :func:`~matplotlib.pyplot.contour`


    Parameters
    ----------
    x   : np.array (1D or 2D)
        Valeurs des abscisses des points de la carte
    y   : np.array (1D or 2D)
        Valeurs des ordonnées des points de la carte
    z   : np.array (1D or 2D)
        Valeurs de colorations

    Other Parameters
    ----------------
    n_levels [ou nlevels ou nl]     : int
        Nombre d'iso-lignes voulues (10 par défaut).
        Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
    levels [ou l]                   : list
        Liste de valeurs pour lesquelles tracer une iso-ligne.
        Attention : il faut un des paramètres `n_levels` ou `levels` mais pas les deux.
    masked_value                    : float
        Valeur de comparaison pour masquer les données (les valeurs <= masked_value seront masquées).
    n_decades                       : int
        Nombre de décades des données affichées si initialement il y avait plus de 100 decades entre min et max.
    max_decades                     : int
        Nombre de décades entre le min et le max à partir duquel des données vont être masquées.
    colors [ou c]                   : str ou matplotlib.colors
        N'importe quelle couleur matplotlib. Les iso-lignes auront toutes cette couleur.
    cmap [ou cm]                    : str ou matplotlib.colors.Colormap
        La palette (*colormap*) utilisée pour normaliser les valeurs des données en couleurs RGBA.
    linewidths [ou lw]              : float
        Épaisseur de toutes les iso-lignes.
    draw_colorbar [ou dcb ou dc]    : bool
        Si ``True`` affiche la colorbar associée à la carte.
    cax [ou ax ou current_axes]     : axes.Axes
        Axes dans lequel la carte va être tracée.
        **À ne pas utiliser depuis le binding C++.**
    label [ou lb]                   : str
        Légende de l'échelle de couleurs des iso-lignes.
        **À ne pas utiliser depuis le binding C++.**
    nx                              : int
        Nombre de points en abscisses de la grille.
        À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
        **À ne pas utiliser depuis le binding C++.**
    ny                              : int
        Nombre de points en ordonnées de la grille.
        À ne préciser que si ``x``, ``y``, ``z`` sont 1D et ont la même taille.
        **À ne pas utiliser depuis le binding C++.**

    Notes
    -----
    ``x``, ``y`` and ``z`` ont la même forme (1D ou 2D).
    Ce qui donne :

        * Si ils sont 2D, alors ils ont la même taille pour toutes les dimensions.
        * Si ils sont 1D, deux possibilités :

            * x, y et z ont la même taille, il faut préciser nx=, et ny= (on ne peut pas les déduire des données)
            * x.size * y.size = z.size : une grille sera créer pour mapper les données

    See Also
    --------
    .linear_contour
    .logarithmic_tricontour
    .linear_tricontour
    .logarithmic_contourf
    .linear_contourf
    .logarithmic_contourf
    .linear_tricontourf
    matplotlib.pyplot.contour
    matplotlib.pyplot.tricontour
    matplotlib.pyplot.contourf
    matplotlib.pyplot.tricontourf

    Warnings
    --------

    Premier point : description
    qui peut être sur plusieurs lignes

    Deuxième point : description
    aussi sur plusieurs lignes
    """

.. note::
    * si chaîne de caractères : mettre ``str``
    * si liste de ``type`` attendue, mettre ``list[<type>]``
    * si chemin attendu : mettre ``str`` puis dans le commnentaire ``(os.PathLike) Explication``. Ne marche pas à tous les coups.
    * Une docstring doit contenir
        * Un résumé d'une ligne
        * Une ligne blanche
        * Un résumé plus complet
        * Une autre ligne blanche
        * [Parameters | Other Parameters | Returns | See Also | ...]
    * Dans la rubrique **See Also**
        * Lien vers une fct de la même classe : ``.ma_fonction``
        * Lien vers une fct ``matplotlib`` : ``matplotlib.pyplot.contour``
    * Dans la description / résumé d'une docstring :
        * Pour un objet : ``:py:attr:`matplotlib.contour.QuadContourSet```
        * Pour une fonction : ``:py:func:`~matplotlib.pyplot.colorbar``` (NB: il est possible que ``:py:`` soit inutile)


