.. _format_index:

=============================
Guide formatage documentation
=============================

.. toctree::
    :maxdepth: 2

    format
    nemesis
    example_numpydoc
    example_tests_doc
