.. _intern_docstring_tests:

Essais documentation
====================

.. contents::


Documentation pour les différents tests et essais
-------------------------------------------------

Essai conversion des fichiers .psy

.. currentmodule:: ./example_numpydoc

.. autosummary::

    example_numpydoc

.. automodule:: example_numpydoc
    :members:
    :undoc-members:
    :private-members:
    :special-members:
    :inherited-members:

Mise en pratique des bonnes pratiques
-------------------------------------

Les bonnes pratiques ont été définies dans la documentation
    `matplotlib
    </path/vers/share/python/matplotlib-2.1.2/tutorials/introductory/usage.html>`_


Essais utilisation intersphinx
------------------------------

:py:mod:`matplotlib:matplotlib.pyplot`

:ref:`manual matplotlib <matplotlib:howto-faq>`

