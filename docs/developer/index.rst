.. _dev:

Documentation Développeur
=========================

Les rubriques :

* **CDC** : Cahier des Charges, Dossier de Spécification
* **GL** : Génie Logiciel. Tous ce qui tourne autour du projet (tests, règles de codages, bonnes pratiques, …)
* **Autres**: Documentation de produits tierces utilisés dans ``nemesis``

.. note::

    * ``nemesis`` désigne le projet (Python / C++ / docs / …) mais aussi le module Python.
    * ``Nemesis`` désigne les environnements virtuels Python pour ``nemesis``. **@FIXME ?**
    * Ne pas mettre d'accents dans le mot *nemesis*.

.. toctree::
    :maxdepth: 2

    ../ToDo
    genie_logiciel/index
    projet/index
    divers/index
    docstrings/index
    
