# GL : Installation du projet `nemesis`

L'installation peut se faire soit comme un package Python classique, soit via Spack.
Il faut changer le numéro de version du package `nemesis`.

`setuptools` se base sur les fichiers de la base `git` du projet. Il faut donc commiter les fichiers générés avant
de lancer la commande `python setup.py ...`.

## Création des sources compressées

    git archive --format=tar.gz -v -o ./py-nemesis-X.Y.Z.tar.gz --prefix=py-nemesis-X.Y.Z/ HEAD 

On peut aussi le faire avec `setuptools`, mais le *tarball* contiendra moins de choses

    python setup.py sdist
    
## Déploiement Python classique

Avec `pip`

    pip install nemesis-X.Y.Z.tar.gz --install-option="--install-headers={include_dir}

Directement avec `setuptools`

    python setup.py install
    python setup.py install_headers

## Déploiement Spack

Le déploiement Spack copie aussi les examples et la documentation.
Les *headers* sont copiés dans `path_install/include`.
Les fichiers `NemesisConfig.cmake` et `NemesisVersionConfig.cmake` sont générés lors de l'installation
via la recette `Spack`.

La recette `Spack` est stockée dans le repo `Spack`: Aorès avoir activé un `Spack`, tapez `spack repo list` ou un 
simple `spack edit py-nemesis`.

    spack install [-v -v -v] py-nemesis
    
## Documentation

    # spack find -p py-nemesis
    cd <path_installation>
    cd docs
    make html
    # copie de la documentation où il faut
    cp -R _build/html $NEC_ROOT/share/docs/nemesis-X.Y.Z
    
## Tips

### Pour debugger le script `setup.py`

Permet de debugger la partie `distutils` du `setup.py` :

    export DISTUTILS_DEBUG='ON'
