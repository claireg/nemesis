Title: Besoins pour Gaïa
Date: 2018-02-15

# CDC : Besoins pour Gaïa

## Graphique

* carte (2D et 3D) (2D et 3D : `@DONE` )
* courbe (support 2D et 3D) (2D et 3D : `@DONE`)
* réseau de courbes (2D) — abscisses identiques `@DONE`
* histogramme `@TO_TEST` 
* axes :
  * **axe log** (avec valeurs négatives)
  * représentation log à moins d'une décade alors qu'il en faut plusieurs.
* nuages de points (sur une carte de couleurs) [optionnel: Chrono] `@DONE` 
* isolignes `@DONE` , isolignes annotées `@TO_TEST` 


## Présentation

* multipages `@DONE` (via un multi-figures)
* transparence entre pages : si possible
* zoom interactif (zoom élastique) `@DONE` (`matplotlib` le fait tout seul)
* caractères accentués, exposants, et lettres grecques`@DONE` – utilisation de `'$mu$` (implicite dans `matplotlib` si en utf-8)
* titre`@DONE` 
* légendes `@DONE` 
* légendes de graphiques `@DONE` 
* échelles de couleurs `@DONE` 
* attributs graphiques modifiables (couleurs, avec ou sans point, aspect des points, aspects des axes, lignes, )
* positionnement des titres, légendes et échelles (? avec (x,y) ou (en haut à gauche, …) `@DONE` 

## Application

* client / serveur ?
* mode asservi : si possible
* scriptable (session de log)
* ihm scriptable (action dans l'ihm dans la session)

## API

* attaquable en Python et C/C++ `@DONE`

Remarque : `Cython` permet d'utiliser du C en Python mais ne génère pas du code C/C++ pour du code Python.


## Pour plus tard

* utilisation en batch
* mise en page automatique


## Questions en suspens

* quid licences si utilisation de Qt et PyQt ?


### Questions pour Pierre

* Utilisation de plusieurs boîtes d'axes dans une figure ? — Possible dans `matplotlibcpp` via `subplot`
* Dans une même figure :
    * boîtes d'axes 2D et 3D ? 
    * ou une seule boîte d'axes — possibilité de switch entre les deux ?
* Une figure pour la 2D et une figure pour la 3D ?
* Besoin d'autres projections (type polar ?)

