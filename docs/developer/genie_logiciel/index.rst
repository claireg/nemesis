.. _dev_gl:

Génie Logiciel
==============

Les rubriques :

* **CDC** : Cahier des Charges, Dossier de Spécification
* **GL** : Génie Logiciel. Tous ce qui tourne autour du projet (tests, règles de codages, bonnes pratiques, …)
* **Autres**: Documentation de produits tierces utilisés dans ``nemesis``

.. note::

    * ``nemesis`` désigne le projet (Python / C++ / docs / …) mais aussi le module Python.
    * ``Nemesis`` désigne les environnements virtuels Python pour ``nemesis``. **@FIXME ?**
    * Ne pas mettre d'accents dans le mot *nemesis*.

.. toctree::
    :maxdepth: 2

    GL_NemesisCodingRules
    GL_NemesisGitRules
    GL_NemesisPythonEnvironment
    GL_Profiling
    GL_Logging
    GL_Installation
    GL_Tests
    CDC_Besoins
    CDC_Spec
    GL_NemesisOldPythonEnvironment
