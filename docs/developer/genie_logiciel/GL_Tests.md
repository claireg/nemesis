Title: Tests du projet nemesis
Date: 2018-10-04

# GL : Tests de nemesis

## Partie python du projet

Utilisation de `py.test` (parfois noté `pytest`).

* Lancement des tests (tous)

        cd workspace/nemesis
        python3 run_tests.py
        # ou 
        pytest

* Pour lancer les tests pour un seul module

        cd workspace/nemesis/tests
        pytest test_plotter.py


* Pour lancer UN test depuis le répertoire de `tests`:

        pytest [-v --color=yes --junitxml=./local.xml] test_read_data.py::test_read_psy_file_onde
        # pytest module_à_tester.py::fonction_a_tester
    
Si la variable d'environnement `WORKSPACE` n'existe pas, les résultats sont mis dans `$(ccc_home -t)`.

### Notes sur le fonctionnement

* La comparaison d'images fonctionne (utilisation decorator `matplotlib`).
* La comparaison de tableaux `numpy` se fait à l'aide de la fonction de `numpy.testing` `assert_array_equal`.
    
    
        # Uniquement lors du lancement manuel pour créer les données
        numpy.savez_compressed('path_vers_fichier', a=tab1_ref, b=tab2_ref)
        npz_file = numpy.load('path_vers_fichier')
        a = npz_file['a']
        b = npz_file['b']
        numpy.testing.assert_array_equal(tab1_ref, a)
        numpy.testing.assert_array_equal(tab2_ref, b)

* La comparaison de dictionnaire pour objets Psyché fonctionne (fonction dans `testing/__init__.py`)
* Les tests en doctest sont pris en compte par `pytest` (même lancé depuis `pycharm`).
* Un fichier au format `junit` est créé (+ modification xml pour ajout xslt si hors Jenkins)
* 2 fichiers de config `py.test` :
    * ``nemesis/pytest.ini``
    * ``testing/conftest.py``
* Pour la comparaison de chemin vers un fichier ou répertoire, il faut utiliser `os.path.samefile(f1, f2)` car
selon la façon de lancer le test, le chemin peut différer mais pointer sur le même fichier.
* Il est possible de tester les méthodes qui affichent sur `sys.stdout` en utilisant `_test_redirected_print` du
module `nemesis.testing`. Un exemple est dispo dans `test_read_data.py`

### Ajout d'un nouveau test ou d'un nouveau module

Il faut lancer le(s) nouveau(x) test(s) (soit via le script `run_tests.py`, soit via `pycharm`), puis aller récupérer 
le(s) résultat(s) dans `workspace/nemesis/tests/test_[module][/result_images]`, et le(s) copier dans 
`[baseline_image|baseline_objects]/test_[module]`.

## Partie C++ du projet

Utilisation de `Catch` (sources originelles dans `workspace/Catch2-master`).

**Les tests sont compilés avec l'option `-std=c++1z.`**

Les tests pourraient être dans le code source (fichiers `.cpp`), mais il a été choisi de les mettre à part dans le 
répertoire `workspace/src/tests`.

Pour `matplotlibcpp`, on teste que l'appel des fonctions de ce *namespace* ne lève pas d'exception lors de l'exécution.
Il faut donc faire en sorte que tout appel à l'api C de Python ou à matplotlib lève bien une exception en cas de 
soucis.
    
Par défaut les tests sont activés (option `BUILD_TESTING` à `ON`). 

1. Configuration 

        cd workspace/src/tests
        ./configure

2. Compilation

        cd cmake-build-debug
        make

3. Lancement des tests

        # Exécution simple dans une console
        TUTestsNemesis
        
        # Pour avoir une sortie au format junit pour `Jenkins`
        TUTestsNemesis -r junit


Ces tests font office de tests unitaires et de tests de non-regression. Ils se divisent en 2 parties (le 4 octobre 2018) :
* les tests pour le header `utilities.h` (*namespace* `nem_utils`)
* les tests du header `matplotlibcpp.hpp` (*namespace* `matplotlibcppgit log`)
