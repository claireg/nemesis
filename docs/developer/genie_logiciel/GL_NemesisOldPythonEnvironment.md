Title: Notes Environnement Python
Date: 2018-02-15


# OLD : GL : Python et Environnement Virtuel

## Installation Python

Il faut utiliser le script `installPython.sh`.
Ce script est à lancer depuis le répertoire contenant les sources Python.

    cd <rep_qui_contient_scripts>
    ./scripts/installPython.sh

Notes :
* **La compilation se fait avec le mode optimisé avec au moins un gcc6.4**

### Installation sans passer par le script

#### Extraction des sources

    tar xf Python-3.6.4.tar.xz
    mv Python-3.6.4 Python-3.6.4-`ccc_os`
    cd Python-3.6.4-`ccc_os`

#### Configuration stations (RHEL6 et RHEL7)

    ./configure \
    --prefix=/path/vers/gcc/python/3.6.4/python \
    --enable-optimizations \
    --enable-shared \
    --with-ensurepip=yes \
    --with-pydebug \
    CC=/path/vers/gcc49/gcc/6.4/bin/gcc \
    CXX=/path/vers/gcc49/gcc/6.4/bin/g++ \
    LDFLAGS="-L/path/vers/gcc49/gcc/6.4/lib -L/path/vers/gcc49/gcc/6.4/lib64 -Wl,--rpath=/path/vers/gcc/python/3.6.4/python/lib"

#### Configuration borel

    ./configure \
    --prefix=/path/vers/gcc/python/3.6.4/python \
    --enable-optimizations \
    --enable-shared \
    --with-ensurepip=yes \
    --with-pydebug \
    CC=/path/vers/gcc49/gcc/6.2/bin/gcc \
    CXX=/path/vers/gcc49/gcc/6.2/bin/g++ \
    LDFLAGS="-L/path/vers/gcc49/gcc/6.2/lib -L/path/vers/gcc49/6.2/lib64 -Wl,--rpath=/path/vers/gcc/python/3.6.4/python/lib"

#### Compilation et installation

  make
  make install


## Installation des environnements virtuels

Il faut copier les scripts `bash` dans `<toto>/Python/X.Y.Z/scripts`.
Le script principal d'installation s'occupe d'installer `virtualenv`.

Utiliser les scripts bash présents dans le répertoire `scripts`.

**Recommendations**
* Ne pas générer deux environnements virtuels en même temps pour une même cible car répertoire de compilation identique
* Si création d'un nouveau script pour test ou création nouvelle version, penser à changer la version de l'environnement dans le script.

Pour installer un environnement virtuel :

* Choisir la version de l'environnement virtuel (dans `installNemesis.sh`)
* Vérifier les chemins d'installation de python et de l'environnement virtuel (dans `setEnvironment.sh`)
* Vérifier que c'est bien ce que l'on veut

        ./installNemesis.sh --help

* Installer l'environnement

        ./installNemesis.sh

À noter que certaines installations type `pytest`, ... (celle où il a fallu modifier les sources ou celle avec lesquelles
on a eu des difficulté d'installation) se fond à partir de *wheels*.

## Problèmes rencontrés et solutions

### Installation terminant en erreur : *Disk quota exceeded*

* Vérifier que le *sticky bit* est correctement positionné sur tout le path.
* Au besoin, ne pas supprimer le répertoire de *build* est changé le *sticky bit* et le groupe pour TOUT ce répertoire.
* On peut aussi préfixer les commandes de build et d'installation par `sg opendist -c ...`
    ```bash
    sg opendist -c ...
    ```

* Au cas où ça ne fonctionnerait toujours pas, vérifier que le répertoir `site-packages` 
du nouvel environnement virtuel a bien le *sticky bit* de positionner :
    ```bash
    cd path_venv/lib/python3.7
    chmod g+s site-packages/
    ```

## Package avec sources modifiées

* `PySide2` : pour que shiboken2 ait bien le runpath de la lib python
* `nox-automation` : demander le package `colorlog` dans une veille version. On a juste enlevé le test de la version.
* `numpy` : pour que l'import de `numpy` ne plante pas si il est fait dans une lib statique. Le fichier à modifier est `numpy.core.getlimits.py`, ligne 166

```python
    # CEA : modif pour que l'import fonctionne dans une lib statique en I4R4 ou I4R8
    # _tiny_f128 = exp2(_ld(-16382))
    _tiny_f128 = exp2(_ld(-16372))
```

    
    
* `matplotlib` 3.0.2 : **Le package matplotlib-3.0.2.tar.gz a été recompacté après les modifications suivantes** (pkg du home de Claire)
    * À noter qu'il existe une *wheel* qui installe correctement `freetype` (version 2.9.1)
    * `nemesis` se base sur les fonctionnalités de tests de `matplotlib`. À partir de cette version, 
        il faut activer les tests (*N.B.* Le pkg actuel a été modifié et retaré) :
        * copier `setup.cfg.template` en `setup.cfg`
        * éditer le fichier pour que le ligne suivante ne soit plus commentée
    
                tests = True
                sample_data = True
                toolkits = True
                toolkits_tests = auto

    * Toujours à cause des tests, il faut changer la version de `freetype` utilisée (à moins qu'un jour on installe la nôtre)
        * Dans `setupext.py`, mettre **2.8.0** (au lieu de 2.6.1). La bonne version est données par le lancement des tests par le décorateur
            (`pytest test_log_mapping_1_1`)
        * Ce changement de version est à répercuter dans `lib/matplotlib/__init__.py`.
        
    * Exécuter ensuite les commandes suivantes
```bash
        # activation environnement Python (et compilation ?)
        . path/bin/activate
        # Création du .tar.gz
        python3 setup.py sdist
        # Copie du tar.gz
        cp ./dist/matplotlib-3.0.2.tar.gz path/packages
        # Installation à la main
        # pip install ./dist/matplotlib-3.0.2.tar.gz
        # Sinon installation via le script
```
        
## Vérification de l'environnement

Après avoir activer le nouvel environnement, il faut exécuter le script `check_installation.py`.

    . <path_vers_env>/bin/activate
    python <workdir>/tools/check_installation.py [--pyside|--pyqt]
    
L'option `--pyside` est le défaut. Pour les environnements fait en intel, ajouter l'option `--pyqt`.

Une fois l'environnement vérifié, il faut forcer l'utilisation du backend `qt5agg` pour cette installation de `matplotlib`.
Dans le répertoire d'installation de `matplotlib` (`…/Nemesis_0.1_gcc_64/lib/python3.7/site-packages/matplotlib`),
aller dans `mpl-data`, à la ligne `backend`, mettre `qt5agg` à la place de `tkagg`.

**Cela permettra aux testset aux exemples de bien tester ce qui est utilisé dans Gaia.**


## Connaître la liste des packages d'un environnement

Après avoir activer le nouvel environnement, il faut exécuter le script `check_installation.py` avec l'option `-l`.

    . <path_vers_env>/bin/activate
    python <workdir>/tools/check_installation.py --list
    
## Compilateurs et environnement

Il s'avère que qu'un environnement compilé en gcc 6.4 ne fait pas bon ménage avec un exécutable compilé en intel 17.
La compilation et le link se passent bien, mais à l'exécution, il y a des erreurs de version d'ABI (le compilateur
intel 17 est en effet lié à la bibliothèque `/lib64/libstdc++.so.6`). On a beau rajouté des `-Lchemin_vers_le_bon_path`,
`-Wl,-rpath=chemin_vers_le_bon_path`, ainsi que `-D_GLIBCXX_USE_CXX11_ABI=0`, rien n'y fait).

De plus, via Gaia, le fait que `NemesisDevice` soit une bibliothèque chargé via `dlopen` avec uniquement le flag `RTLD_LAZY`
a aussi un impact (sur `PySide2` et `PyQt5` qui sont liées à d'autres `.so` qui sont chargés automatiquement mais qui 
n'ont pas accès à `libpython3.7m.so`).

Pour l'instant (le 18 septembre 2018), il existe deux environnements virtuel pour `Nemesis`. L'un en gcc 6.4 (avec `PySide2`),
l'autre en intel 17 (avec `PyQt5`). Il faut vérifier que tous les exemples C++ fonctionnent avec les deux environnements.
Les deux environnements fonctionnent en python pur.
Gaia ne fonctionne pas avec l'environnement intel car la bibliothèque `sip` ne trouve pas les symboles de la bibliothèque
`python` ... (Flags du dlopen à changer ? ou recompilation de `PyQt` en ajoutant un `-L` et un `-Wl,--rpath` vers python 
— le répertoire ou la lib ?).

**À tester**

* changer le flag du dlopen dans Gaia ?
    * ajouter `RTLD_LOCAL` (Gaia en intel et environnement python en gcc 6.4)
    * ajouter `RTLD_GLOBAL` (Gaia en intel, environnement en intel) : pas besoin de recompiler `PyQt` et / ou `sip`.
* recompiler `sip` et / ou `PyQt` pour ajouter le `-L` et le `-Wl,--rpath` vers python (chemin ou lib) ?
* Envirronement python : au lieu de mettre un chemin pour l'option `-Wl,--rpath=`, mettre une bibliothèque.


## Divers mais importants

### Numpy

Si problème lors de la compilation de `numpy`, vérifier que le module `mkl` est bien chargé.
Si il y a toujours des erreurs, changer de version de `mkl`.

**Notes :**
* Le 28 mai 2018 : impossible de compiler avec la version `18.0.0.128`.
    * Compilation avec la version `17.0.4.196` OK.
* Les sources de numpy sont patchées à la main pour que l'import de `numpy` ne plante pas si il est fait dans une lib statique.
Le fichier à modifier est `numpy.core.getlimits.py`, ligne 166

```python
    # CEA : modif pour que l'import fonctionne dans une lib statique en I4R4 ou I4R8
    # _tiny_f128 = exp2(_ld(-16382))
    _tiny_f128 = exp2(_ld(-16372))
```

### PySide2

Pour réussir à faire fonctionner Gaia et matplotlib avec PySide2, il a fallu modifier les sources de PySide2.
Le tar.gz des sources s'appelle `pyside2-setup-5.9-cea.tgz`.
Il contient une modification très importante pour fonctionner dans Gaia.
Dans `pyside2-setup-5.9/sources/shiboken2/CMakeList.txt`, le comportement par défaut ne liait pas la lib `shiboken2`
à la lib python car c'était fait automatiquement par les le linker qui utilisait cette lib. Ça fonctionne dans 95% des cas
(utilisation de matplotlib en python, utilisation via un exe C++ qui appelle matplotlib) mais pas dans Gaia
(chargement dynamique d'une lib via un dlopen – mode LAZY uniquement, peut-être qu'avec d'autres options du `dlopen`,
type LOCAL, on s'en serait sorti).

### Matplotlib

Le package a le bon goût de bien prendre en compte le changement de compilateur, le `LD_LIBRARY_PATH` mais ignore la variable
`CXXFLAGS`. En fait, c'est `disutils` (via `disutils.command.build_ext`, méthode `build_extension`), qui le fait. Il 
est dit clairement que si l'on veut passer des arguments au compilateur, il faut les mettre dans la variable *`CFLAGS`*, 
ou l'ajouter à `extra_compile_args` de l'objet extension.

Plutôt que de modifier le fichier `setup.py` de `matplotlib`, on surchage `CFLAGS`

    # Selon les commentaires dans disutils.command.build_ext, méthode build_extension, pour passer des arguments
    # au compilateur il faut le mettre dans la variable CFLAGS (ou l'ajouter à extra_compile_args de l'objet Extension)
    
## Génération documentation

### Principe général

Pour les packages utilisant `Sphinx`.

1. Détarer ou dezipper les sources du package
2. `cd <pkg>/doc`
3. Activation de l'environnement Python adéquat (ici `Nemesis`)
4. `make html` ou `make.py`
4.bis Si pas de fichier `make*`, et si seulement fichier `conf.py` :

        cd rep_contenant_conf_py
        sphinx-build -M html . _build


Si il y a dans le fichier `conf.py` (fichier pour `Sphinx`), il faut que la variable `intersphinx_mapping` ne
contienne pas de `http://…`.

À l'origine (ou si connexion internet)

    intersphinx_mapping = {
        'python': ('https://docs.python.org/3', None),
        'numpy': ('https://docs.scipy.org/doc/numpy/', None),
        'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
        'pandas': ('https://pandas.pydata.org/pandas-docs/stable', None)
    }

Il faut mettre dse chemins relatifs

    intersphinx_mapping = {
        'python': ('/path/vers/share/python/Python-3.7.0', None),
        'numpy': ('/path/vers/share/python/numpy-1.15.4', None),
        'scipy': ('/path/vers/share/python/scipy-1.1.0', None),
        'matplotlib': ('/path/vers/share/python/matplotlib-3.0.2', None),
    }

`intersphinx` permet d'avoir des liens vers de la documentation extérieure dans son projet.

### Documentation matplotlib

En plus de changer la valeur de la variable `intersphinx_mapping`, il faut faire une modif dans `conf.py` pour le calcul du SHA.
Au lieu de faire appel à `git` (vu que là, on n'a pas fait un clone du repo `matplotlib`), faire les modifications suivantes :

    # General substitutions.
    from subprocess import check_output
    # SHA = check_output(['git', 'describe', '--dirty']).decode('utf-8').strip()
    SHA = '3.0.2-cea-dirty'

Avant de générer la doc, il faut modifier l'exemple `misc/load_converter.py`. Après le print, mettre :

    import dateutil
    # …
    data = np.genfromtxt(datafile, delimiter=',', names=True, dtype=None, converters={0: dateutil.parser.parse})
    # remplacer le ax.plot(...) par
    ax.plot(data['Date'], data['High'], '-')
    
Modifier l'exemple suivant `examples/ticks_and_spines/date_index_formatter2.py` comme suit :

```python
import dateutil.parser
from matplotlib import cbook, dates
import matplotlib.pyplot as plt
from matplotlib.ticker import Formatter
import numpy as np

datafile = cbook.get_sample_data('msft.csv', asfileobj=False)
print('loading %s' % datafile)
msft_data = np.genfromtxt(datafile, delimiter=',', names=True,
        converters={0: lambda s: dates.date2num(dateutil.parser.parse(s))})


class MyFormatter(Formatter):
    def __init__(self, dates, fmt='%Y-%m-%d'):
        self.dates = dates
        self.fmt = fmt

    def __call__(self, x, pos=0):
        'Return the label for time x at position pos'
        ind = int(np.round(x))
        if ind >= len(self.dates) or ind < 0:
            return ''

        return dates.num2date(self.dates[ind]).strftime(self.fmt)

formatter = MyFormatter(msft_data['Date'])

fig, ax = plt.subplots()
ax.xaxis.set_major_formatter(formatter)
ax.plot(msft_data['Close'], 'o-')
fig.autofmt_xdate()
plt.show()
```

Modifier `doc/Makefile` pour que les warnings ne soient pas des erreurs (enlever l'option `-W` à `Sphinx`) :

    SPHINXOPTS = 

### Documentation Python

La documentation est généré via un des environnements **Nemesis**.

    . <path_vers_env>/bin/activate
    cd <rep_sources_python_utilisé_pour_compilation>/Doc
    make
    cd /path/vers/gcc/python/3.6.4/python/share
    cp -R <rep_sources_python_utilisé_pour_compilation>/Doc/build/html .


## Remarques sur les dépendances entre packages

Si on veut pouvoir faire du *markdown* dans la documentation `Sphinx`, il faut le package `recommonmark` et donc `commonmark`.

