# GL : Règles codage C++

**Il faut utiliser au maximum les *C++ Core Guidelines* écrites par la communauté.

## Outils de respect des règles de codage

Le formatage du code pour le projet se base sur `clang-format`. Il existe un fichier de configuration pour `clang-format` à la racine du projet nommé `.clang-format`.

Ce fichier de configuration est utilisé dans les 3 cas suivants :

* Avant chaque commit, via le `pre-commit` *hook*.
* Lors de l'utilisation de la commande `git` `clang-format`.
* Lors de l'appel manuel à `clang-format`.

### Le `pre-commit hook`

Le *hook* (Cas (1)) ne reformate pas le code si il trouve du code ne respectant pas les règles.
Il faut le formater soit même, à partir de la racine du projet à l'aide de la commande suivante :

    python ./nemesis-cpp/git-cmake-format-master/git-cmake-format.py --cmake /usr/bin/git /path/vers/gcc6/llvm/6.0.0/bin/clang-format -style=file -ignore=

L'utilitaire `git-cmake-format.py` ne reformate que les fichiers C++ modifiés depuis le dernier `commit`.

### La commande `git`

Si dans votre variable `PATH` vous ajouter le répertoire racine du projet, vous disposez alors de la commande `git` `clang-format` :

    git clang-format

La commande lancera `clang-format` sur les modifications des fichiers courants ou pour un *commit* spécifique.

Pour plus de détails : `git clang-format -h`

### La commande `clang-format`

    /path/vers/gcc6/llvm/6.0.0/bin/clang-format -style=file [-i] [<file> ...]


## Formatage du code


## Classes, namespaces et structures

    namespace mon_namespace {
        …
    }
    
    class MaClasse {
        …
    }
    
    struct MaStruct {
    }

### Nommage

Même règles qu'en Python pour avoir une cohérence à l'intérieur du projet : *CamelCase*.

## Méthodes, fonctions, …

    void ma_fonction(std::vector<T>& tab_)
    {
        …
    }
    
    void ma_methode()
    {
       … 
    }

### Nommage

Même règles qu'en Python pour avoir une cohérence à l'intérieur du projet :
* nom en minuscules
* mots séparés par des `_`

## Paramètres de fonctions et méthodes

### Nommage

Les noms sont en minuscules, et finissent par un `_` pour être facilement reconnaissable dans le code.


## Documentation du code C++

Remarque : utilisation de doxygen pour la documentation C++, avec le style «*c++ comment lines block*».

    ///
    /// text
    ///

### Cartouche d'en-tête

    ///
    /// fichier.h
    ///
    /// \brief  Brève description
    /// \date   20/04/2018
    /// \author Claire Guilbaud
    ///
    
### Documentation variables et variables membres

Documentation sur la même ligne

    int var;    ///< Detailed description after member
    
    int var;    ///< Detailed description after member
                ///< 

### Documentation des fonctions, méthodes, ...

### Documentation des classes 


# GL : Règles codage Python

Suivre la `PEP8` de Python. Ce sont ces règles de codage appliquée par par défaut par `pycharm`.

**Il faut utiliser au maximum la *PEP8* écrites par la communauté.**

## Outils de respect des règles de codage

`Pycharm` vérifie les règles définies dans la **PEP8**.
`pylint` a été installé dans l'environnement virtuel pour le projet, si besoin est.

## Règles propres au projet

### Encodage des caractères

* quand on ouvre un fichier (en lecture ou écriture), le paramètre `encoding=` est obligatoire

```python
    with open(old_filename, encoding=sys.getfilesystemencoding()) as f:
```

* quand on lit un résultat sur la sortie standard, décodage obligatoire (comme c'est des `bytes`)


```python
    output = subprocess.run(['git', 'config', '--get', 'user.name'], capture_output=True)
    real_output = output.stdout.decode(sys.getfilesystemencoding())
```

### Docstrings 

Les *docstrings* sont obligatoires dans chaque méthode, fonctions, classes, packages, scripts accessibles à 
l'utilisateur.

Elles sont très fortement recommandées pour toutes les autres cas.

Une docstring doit obligatoirement contenir :
* Un résumé d'une ligne
* Une ligne blanche précédent un résumé plus complet
* Des précisions (un résumé plus complet)
* Une autre ligne blanche

Exemple :

```python

"""
Un résumé d'une ligne sur la fonctionnalité

Une description plus élaborée. Par exemple, dans cette 
section,
on peut ajouter des détails, des exemples.
Pensez à bien séparer le résumé et la description par
une ligne vide.
"""
```

Il y a trois grandes catégories de docstrings :
* les docstrings pour les classes
* les docstrings pour les packages et modules
* les docstrings pour les fonctions et scripts

Pour les fonctions, ne pas oublier d'indiquer le type d'exception levée si nécessaire.
Exemple de docstring complet : 

```python
# -*- coding: utf-8 -*-

"""
Ensemble d'utilitaires pour la mesure de temps.

Code basé sur un post du blog saladtomatonion.com intitulé «Mesurer le temps d'exécution 
de code en python».

"""
import time


class Timer(object):
    """
    Mesure de temps.

    Implémente le design pattern *context manager* utilisé par le mot clé ``with``.

    Attributes
    ----------
    start_time : float
        Temps en secondes depuis l'epoch.
    interval : float
        Différence de temps en secondes entre l'instant présent et start_time.
    
    Methods
    -------
    start()
        Démarrage du chronomètre
    stop()
        Arrêt du chronomètre
    
    Examples
    --------
    with Timer() as timer:
        mon_code()
    print('Total in seconds : {}'.format(timer.interval)
    """
    def __init__(self):
        """Constructeur.
        
        Returns
        -------
        Un nouvel objet.
        """
        self.start_time = None
        self.interval = None

    def __enter__(self):
        """ 
        Démarrage du chrono pour utilisation avec with ou un décorateur.
        
        Notes
        ------
        __enter__ must return an instance bound with a "as" keyword.
        """
        self.start()
        # __enter__ must return an instance bound with a "as" keyword
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """ 
        Arrêt du chrono pour utilisation avec with ou un décorateur.
        """
        self.stop()

    def start(self):
        """Démarrage du chrono
        """
        if hasattr(self, 'interval'):
            del self.interval
        self.start_time = time.time()

    def stop(self):
        """Arrêt du chrono et stockage de la mesure.
        
        Raises
        ------
        RuntimeError
            Arrive si le chrono n'avait pas été démarré.
        """
        if hasattr(self, 'start_time'):
            self.interval = time.time() - self.start_time
            del self.start_time
        else:
            raise RuntimeError('pas de temps initial.')
```



#### Docstrings et type

* si chaîne de caractères : mettre `str`
* si liste de `type` attendue, mettre `list[<type>]`
* si chemin attendu : mettre `str` puis dans le commnentaire `(os.PathLike) Explication`. Ne marche pas à tous les coups.
* si il y a ambiguité sur des paramètres, variables, utiliser les *type hint* (`Pycharm les propose dans son analyse du code)

### Type hints

**Utiliser** les *type hints* dans la déclaration des fonctions et méthodes, ainsi que pour les variables
 de module. `PyCharm` vérifie les types des paramètres lors des appels des fonctions et méthodes et permet 
 ainsi de détecter des erreurs sans exécuter le code.
**ATTENTION**, pytest vérifie les *type hints* avant de lancer le test, donc si ils sont faux, le test est faux ...

Les types «plus génériques» sont définis dans le module `typing` de Python.
Quelques astuces :

* Si c'est n'import quel type : ``ANY``
* Ne pas mettre les type hints pour les fonctions pas niveaux (logging, decorator pour les traces, timer, ...)
* Pour les chemins : ``str`` (puis en commentaires : os.PathLike)
* Ne pas hésiter à faire des ``NewType``, notamment pour np.array. Ça permet de mettre les type hints en ayant les
tests qui passent toujours

        from typing import TypVar
        import numpy as np
        
        NemPsyTypes = TypeVar('NemPsyTypes', str, PsyEnum, PsyObject)
        NpArray = NewType('NpArray', np.array)

* Mettre les type hints pour les signatures de fonctions et les variables de module


```python
import numpy as np

def ma_fonction(y_: np.array, v_: float, log_: bool) -> np.array:
    if log_:
        print('log')
    
    return y_ * v_
```

Pour ce qui est des types `os.PathLike`, mettre `str` ou alors créer explicitement un objet `os.PathLike`.


### Documentation

* utilisation de docstrings
* utilisation de fichiers annexes en `reStructuredText` ou `Markdown` dans lesquels on trouvera :
    * Informations Génie Logiciel 
        * règles de programmation C++ / Python
        * tips
        * utilisation `git` (workflow du projet, distribution, _)
        * …
    * Le Cahier des Charges
    * Informations diverses sur les performances, les notes sur un sujet
    * Documentation sur les produits externes

