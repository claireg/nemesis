# GL : Versionnage sources

`git` sera le seul gestionnaire de sources utilisé.

**Ces bonnes pratiques sont obligatoires.**

Le repository du projet est (`gitlab`):

    https://gitlab.mon.site.web/mon_groupe/nemesis.git
    # L'ajout du nom permet de s'authentifier avec kerboros, et de ne plus avoir à taper login / mdp à chaque push
    # git clone https://mon_login@gitlab.mon.site.web/nemesis.git 
    # Avant la création d'un groupe, remplacer physical-data par guilbaud

**Il faut suivre le workflow du projet pour avoir un projet cohérent dans lequel on se retrouve facilement.**


## Configuration de git ##

Pour voir la configuration existante : `git config -l`

### Nom et mail ###

Pour tous les repositories (stockage dans `$HOME/.gitconfig`)

    git config --global user.name "Claire Guilbaud"
    git config --global user.email "claire.guilbaud@cea.fr"

Pour le repertoire courant (stockage dans `./.git/.config`) – Prend le dessus sur `$HOME/.gitconfig`

    git config user.name "Claire Guilbaud"
    git config user.email "claire.guilbaud@cea.fr"

### Suprression configuration existant ###

    git config --unset --global user.name

### Ne pas versionner certains fichiers et répertoires ###

Exemple pour fichier temporaire de `vim`

    cat > .gitignore
    *.swp
    git add .gitignore

Possibilité d'avoir un fichier `.gitignore` par répertoire. Si on veut suivre un fichier ignoré par un .gitignore d'un parent, ajouter un `!` en début de ligne

    # cd mypkg ; cat .gitignore
    *.o
    # cd mypkg/vendor_files ; cat .gitignore – driver.o sera suivi.
    !driver.o

### Divers ###

* Spécification de l'outil de comparaison

        # existe aussi meld, diffuse, kdiff, ...
        git config merge.tool kompare

* Coloration

        git config --global color.ui auto

* Choix de l'éditeur pour les messages des commits et des tags

        # Mettez l'éditeur de votre choix
        git config --global core.editor vim

* Création d'alias

        git config --global alias.show-graph 'log --graph --abbrev-commit --pretty=oneline'
        git config --global alias.wdiff "diff --color-words"
        git config --global alias.sshow 'log --pretty=oneline --abbrev-commit'

* La configuration globale se trouve dans `$HOME/.gitconfig`

## Workflow

Chaque développement se fait dans un repo local à partir d'un clone de la branche `dev` du repo distant. Le nom de la branche doit suivre les règles de nommage.

Un fois le développement effectué, **les tests passés, la documentation mise à jour**, il faut créer un branche de `merge` à partir de la branche `dev` à jour (`pull`) par rapport au repo distant. Si la branche s'appelle `feature/toto`, la branche de merge s'appelle `merge/feature/toto`. 

Il faut ensuite effectué un `merge squash` de sa branche de dev dans la branche de merge, et **repasser les tests et regénérée la documentation** avant de faire le commit.
Le message du commit ne doit pas être la suite des commits de la branche de dev, il doit être ré-écrit (Il existe des règles pour le message du commit).

Une fois la branche de merge validée, il faut la merger dans la branche de dev avec un merge.

La branche de dev locale peut maintenant être poussée dans la branche de dev distante.

![nemesis_workflow](../../images/nemesis_workflow.svg)

Sans le blabla, ça donne :

    git checkout dev
    git pull
    git checkout -b feature/toto
    
(développement, tests, doc, commit)+

    git --no-pager log --abbrev-commit dev..HEAD > /tmp/log.txt
    git checkout dev
    git pull
    git checkout -b merge/feature/toto
    git merge --squash feature/toto
    
(tests, correction — commit ?, génération doc)+

    # **message : réécrire le message à partir de /tmp/log.txt **
    git commit 
    git checkout dev
    # Si un seul commit avant, pas de message demandé -> un seul commit dans l'arbre 
    git merge merge/feature/toto 
    git branch -D feature/toto
    git branch -d merge/feature/toto
    git push


## Nommage branches et commit

### Les branches

**Le nom de doit pas contenir de référence à un bug jira (ou autres) car peu explicite.**

* `feature/description` : nouvelle fonctionnalité.
* `bug/description` : correction de bugs.
* `released/X.Y.Z[w]` : préparation de nouvelles versions.
* `hotfix/description` : correction de bugs urgents sur des versions release que donne lieu à une nouvelle release.
* `merge/description` : pour le merge entre plusieurs branches.

Éventuellement `doc/description`.

### Les commits

#### Formatage des sources

**Pour l'instant, plus de formatage des sources.**

Il faudrait que les sources C++ soint formatées avant les commits, mais il ne faut pas formater `matplotlibcpp_opensource.hpp`
car sinon un merge avec le repo de `matplotlibcpp` devient moins simple.

    # À partir du répertoire tools à la racine de l'espace de travail
    ./format_cpp_files.sh
    # Seuls les fichiers modifiés du C++ seront modifiés
    # Rq : Le répertoire Catch2-master est ignoré.

À noter que dans `CLion`, il est possible de formater le code avant de publier les messages.

Les *hooks* `git` ont été désactivés. Mais ils peuvent encore être utilisé manuellement. L'essentiel est d'avoir 
un code cohérent. 
 
À la main, ça donne :

    python ./nemesis-cpp/git-cmake-format-master/git-cmake-format.py --cmake /usr/bin/git /path/vers/gcc6/llvm/6.0.0/bin/clang-format -style=file -ignore=...
 
Voir le document [Règles de codage C++](GL_NemesisCodingRules.md) pour plus de renseignements.

#### Les messages

**`git` interdit les commits sans message**.

Les messages doivent être de la forme (recommandée par `git gui`) :

> **1ere ligne** : <CODE_DEV> décrire en une phrase ce qui a été fait
>
> **2eme ligne** : rien (pour bien séparer le titre du reste)
>
> **lignes suivantes** : description des modifications

En plus de ces recommendations, utiliser le formalisme suivant :

* `@`   update
* `+`   ajout
* `-`   suppression

(Issu de la documentation numpy)

* `API` : an (incompatible) API change
* `BENCH` : changes to the benchmark suite
* `BLD` : change related to building and installing nemesis (examples, module, …)
* `BUG` : bug fix
* `DEP` : deprecate something, or remove a deprecated object
* `DEV` : development tool or utility
* `DOC` : documentation
* `ENH` : enhancement
* `MAINT` : maintenance commit (refactoring, typos, etc.)
* `REV` : revert an earlier commit
* `STY` : style fix (whitespace, PEP8)
* `TST` : addition or modification of tests
* `REL` : related to releasing nemesis

L'intérêt d'un tel formatage est d'avoir un titre qui identifie le commit (facilement retrouvable), et qui s'affiche sur une ligne.

### Fusion de branches

#### Nouvelles fonctionnalités ou correction de bugs

Une fois les développements finis (et **les tests passés avec succès**), il faut *merger* (fusionner) sa branche 
de développement dans la branche `dev` de son *repository* local.

Il faut écrire le commentaire de fusion de la branche de `merge` dans `dev` dans un fichier en partant de :

```bash
# Récupération des logs entre la dev (du workspace) et le HEAD de sa branche de feature
git --no-pager log --abbrev-commit dev..HEAD > /tmp/log.txt
# Ré-écrire proprement le message
```

Il faut essayer de faire en sorte que les lignes fassent moins de 80 caractères. Le message du commit peut être 
rédiger en `markdown` pour avoir une jolie présentation dans `gitlab`.

La branche de developpement est ici appelée `mabranchededev`. Depuis son *local repository*, faire les actions 
suivantes :

``` bash
git checkout dev
git pull origin dev
git checkout -b merge/mabranchededev
git merge --squash mabranchededev
```

`--squash` dit à git de fusionner plusieurs commits en un seul.

Il faut passer l'ensemble des tests et générer la documentation (et les présentations)

    git commit
    
**Le commentaire est celui qui a été écrit à partir des logs**. 
Les fichiers sont alors à l'état *staged*, et donc *tracked*.


    git checkout dev
    git merge merge/mabranchededev # Avant option --no-ff

Si il y a eu un seul commit avant, alors ``git`` ne demandera rien pour finaliser le merge. Si ``git`` demande un 
message, laisser celui qu'il propose et ajouter une phrase sur ce qui a été corrigé.

    git branch -D mabranchededev
    git branch -d merge/mabranchededev
    # Push vers gitlab
    git push
    # Idem pour l'upstream phabricator
    git push phabricator dev


#### Pour de la documentation

Pour la rédaction et mise à jour de documentation, si la branche ne contient pas plus de 3 commits.

Le passage des tests n'est pas obligatoire, à moins que des tests aient été ajoutés dans les docstrings (`doctest`).

La branche de developpement est ici appelée `mabranchededev`.

    git checkout dev
    git merge mabranchededev
    git branch -d mabranchededev
    git push


## Préparation d'une version

Tester lors des premiers déploiements. À tester sur le long terme.

Les tests doivent être passés sur la version dev (`origin/dev`). On ne peut plus les passer quand on crée une version
car les fichiers générés pointent vers des espaces qui n'existent pas encore (type `share/script/nemesis-X.Y.Z`)

**Dans le repo du projet, on garde une trace des fichiers versions déploiement à la *dif*.**

**ATTENTION** il faut activer un environnement virtuel accessible à tous !

* Activer de l'environnement virtuel sur lequel va reposer la version. Exemple :

        . /path/vers/mon/env/spack/bin/activate

### Workspace spécial pour la version

Si on a choisi de faire une version à partir d'un espace de travail «vierge» :

* Cloner l'espace de travail et rapatrier les branches du repo distant dans le repo loacal
    
        cd espace_de_travail
        # En partant de la version Git lab - upstream origin
        git clone https://guilbaud@gitlab.mon.site.web/mon_groupe/nemesis.git  NemesisReleased
        cd NemesisReleased/
        git checkout master
        # git checkout released
        git checkout dev   

* Configurer ce nouvel espace `git`

        git config user.name "Claire Guilbaud"
        git config user.email "claire.guilbaud@cea.fr"

### Workspace de travail classique

Si on a choisi de partir d'un espace de travail existante :

    git checkout dev / master
    git pull

### Si problèmes d'upstreams

`git` indique la commande à taper si on a un problème d'upstream. Elle ressemable à :

    # git remote set-url origin --push --add https://guilbaud@gitlab.mon.site.web/mon_groupe/nemesis.git 

### Préparation de la version

* Préparer les informations suivantes (dans un fichier de scratch ou sur un papier).
    * Le tag de la version `X.Y.Z[w]`, `w` étant une lettre ici. À partir de la version 0.0.2 c'est un chiffre
    * Le numéro de la version `X.Y.Z[.W]`, `W` est un chiffre ici
    * **tag == numéro version**
    * Le commentaire du tag (qq chh de logique par rapport aux précédents tags) : `Version X.Y.Z[w] (X.Y.Z[.W]) – court descriptif`
    * Les commentaires de log pour `CHANGES.md`
    * Le commentaire de fusion dans la branche dev (concis mais complet)

Le mieux est de s'aider des logs entre la master et HEAD :

    git --no-pager log --abbrev-commit master..HEAD > /tmp/log.txt
    
Exemple de fichier de préparation :
```markdown
# Informations générales

* Tag         0.0.4
* Version     0.0.4.0
* Commentaire Version 0.0.4 (0.0.4.0) — MAINT refactoring PyPlotCEA.*_*contour*
* SHA1 `git sshow` ou `git rev-parse HEAD`

# CHANGES.md

MAINT refactoring `PyPlotCEA.*_*contour*`

* Homégénéisation des paramètres des fonctions `PyPlotCEA.*_*contour*`
* @TST
* @DOC


# Commentaires pour le merge

MAINT / TST refactoring `PyPlotCEA.*_*contour*`

MAINT
* Nouveau module `contour` qui contient 3 classes pour faciliter la création
d'un contour (*_*contour*)
* Nouveau module `numpy_tools` : ens de fonctionnalités au dessus de numpy
-> Code homogène entre les différentes fonctions
-> Paramètres homogènes pour les différentes fonctions

TST
* Ajout tests pour les nv fonctions de `readadat`


Divers
* @DOC
* @TODO


# À modifier

## Doc : `GL_NemesisGitRules.md`
* Changer le fichier de préparation pour la version
* Mettre en italique *À Noter que ces fichiers ...*
* Supprimer «Normalement, il n”y a pas de fichers modifiés (sauf si on sauvegarde le contenu de dist un jour)»
* À marquer : Dans le commentaire du tag : ne pas mettre de «`»
* Lors du merge de dev dans master : ajouter que si il n'y a pas de conflit, git demande directement le message de commit
* Dans «remise en mode dev», acte 2 : enlever `cd espace_de_travail`

## `post_actions.sh`

Régler le pbm suivant :

    The HTML pages are in _build/slides.
    Build finished. The HTML slides are in _build/slides.
    mkdir: impossible de créer le répertoire « $/.../share/docs/nemesis-0.0.4/presentations »: Aucun fichier ou dossier de ce type
    cp: impossible de créer le répertoire « /.../share/docs/nemesis-0.0.4/presentations/general_presentation_v0.0.1 »: Aucun fichier ou dossier de ce type
    /.../Work/NemesisReleased/docs/presentations
```

* Se positionner sur la branche `dev` 

        git checkout dev
        git checkout -b X.Y.Z[.W]
        
* Changer le numéro de version du package `nemesis`(dans `nemesis/__init__.py`, ligne `__version__`). 
* Compléter `CHANGES.md`.
* **Commiter les modifications**. Le commentaire sera du type `Préparation version X.Y.Z[.W]`
* Merger la branche `X.Y.Z[.W]` dans `dev`

        git checkout dev
        git merge --squash X.Y.Z[.W] 
        git commit
        # message : celui proposé par git
        git branch -d X.Y.Z[.W]

* Pousser les modifications locales dans le repository (`origin/dev`) si on a une *upstream*

        # Besoin de dire qu'elle est l'upstream
        git push [--set-upstream origin dev]
        
* Tagger la branche *dev* du repo distant. **Ne pas mettre de caractère \` dans le commentaire**.

        git tag -m "Version X.Y.Z[.W] : premier déploiement"  X.Y.Z[w] SHA1_commit
        # À FAIRE AU DERNIER MOMENT : trouver SHA1_commit
        # 		git rev-parse HEAD ou git sshow :
        # Il faut pousser le tag à la main (git push ne le fait pas)
        # git push origin X.Y.Z[w]
        git push origin dev --tags
        # A priori inutile
        # git push phabricator X.Y.Z[w]
        # vérification de l'existence du tag
        git tag -n

* Merger *dev* dans *master*, et pousser ce merge. Le faire à la main, ou demander un *merge-request* dans `gitlab`. 

        git checkout master
        git checkout -b released/X.Y.Z
        git pull origin dev # un fetch + un merge 
        # résolutions des conflits
        # Si il n'y a pas de conflits, qui demande directement le message de commit
        #	-> ne pas faire la commande qui suit
        git commit
        # log = ceux de commit branche feature dans dev avec pour titre `Version X.Y.Z[.W] – Titre`
        # Laisser les conflits résolus pour mémoire
        git push --set-upstream origin master
        # git push

* **Remarque** : un jour il faudrait tester le merge-request de `gitlab` (directement dans gitlab). Puis mettre à jour
la branche master du *workspace* :

        git pull origin master
        git push phabricator master


* À partir de là, on peut supprimer le répertoire `NemesisReleased` ayant servi au déploiement si on en avait créé un.

        
### Remise en mode dév de son espace de travail

Après avoir fait une version, on ré-utilise en général, son répertoire habituel servant d'espace de travail.
Normalement, cet espace est sur la branche *dev* (sans modification). Cette branche doit être mis à jour.
`git stash save` ne sert que si vous avez des modifications dans *dev* (**ce qui est mal**, toutes les modifications
doivent se trouver dans une branche, on ne travaille jamais en direct sur la *dev* !).

* Acte 1 : mise à jour de *dev*
```bash
    # git stash save "WIP: ..."
    git checkout dev
    git pull origin dev
    git checkout master
    git pull origin master
    git checkout dev
    git checkout -b post/X.Y.Z
    # git stash pop
```
* Plus besoin de reconfigurer `nemesiscppconfig.h` car générer lors de la phase configuration de `cmake`.


## Tips : Travailler avec `git` ##

### Travailler sur un fork d'un projet dont la master a changé ...

Avec des pull-requests en attente ...

Pour voir ce qu'il existe sur le repo local :
    
    git branch -a
    git ls-remote
    # la liste des remotes déjà enregistrés
    git remote -v

D'abord, ajouter la repo distant (ici nommée ``upstream-matplotlib``)
 
    git remote add upstream-matplotlibcpp https://github.com/lava/matplotlib-cpp.git
    # vérification de son existence 
    git remote -v

Un ``fetch`` de l'upstream puis le ``rebase`` pour rejouer les commits de la branche distante.

    git fetch upstream-matplotlibcpp
    git rebase upstream-matplotlibcpp/master

Gérer les conflits (``git mergetool`` est intéressant) et ajouter les fichiers modifiés.

    git mergetool
    # choisir diffuse ou kcompare 
    git add ...
 
Et on continue le rebase 

    git rebase --continue

Si la branche actuelle (sur laquelle on vient de faire le rebase) existe sur un serveur distant :

    # `-f`` seulement la première fois que la branche est poussée, juste après le rebase
    git push -f origin merge/var_temp_args


### Affichage des modifications ###

Affichage des différentes modifications

    git log
    git show <n_commit_as_8bce1339bdc38027abb095788c9084a1f40b80b5>
    git show-branch --more=10
    git diff-tree --no-commit-id --name-only -r <commit-ish>
    # Historique sur un laps de temps (entre 2 commits)
    #   --pretty=oneline|short|full : ajustement de la quantité d'informations à afficher
    #   --abbrev-commit : abbrévation des hash IDs
    #   start..end <=> ]start end] 
    git log --pretty=short --abbrev-commit master~3..master^

Affichages des fichiers modifiers pour une serie de commits
(attention les fichiers de la borne inférieur de l'intervalle n'apparaissent pas).

    git show <commit-ish-0>..HEAD --name-only --pretty=""
    git log --since="2019-11-18" --name-only --pretty=""

### Affichage de différences ###

    # Entre commits
    git diff tag_commit_1 tag_commit_2
    # Affiche les modifications non *staged* dans le répertoire de travail
    git diff
    # Affiche les modifications qui sont *staged* et qui feront partie du prochain commit
    git diff --cached
    # Avoir les différences sur un même ligne — nécessite d'avoir déclarer l'alia wdiff
    git wdiff mon_fichier

### Renommage et suppression ###

    # Suppression du repository (index) et du répertoire de travail - ne supprime pas l'historique du fichier dans le repository
    git rm fichier
    git commit -m "Suppression fichier"

    # Renommage
    git mv fichier1 fichier2
    git commit -m "Renommage fichier1 en fichier2"
    # git mv stuff newstuff <=> mv stuff newstuff && git rm stuff && git add newstuff
    # pour voir l'historique complet d'un fichier renommé
    git log --follow newstuff

    # Suppression du fichier de l'index (et non du répertoire de travail) - status staged vers status unstaged
    #    DANGEREUX
    git rm --cached

    # Après un `git rm` : Finalement je voulais en garder un copie
    git checkout HEAD -- data


### Les tags ###

* Création d'un tag annoté

        git tag -m "Tag version 1.0" V1.0 SHA1_commit
        # il faut pousser le tag à la main (git push ne le fait pas)
        git push origin V1.0

* Retrouver les informations liées à un tag

        # pour retrouvé le SHA1 d'un tag -> SHA1_tag
        git rev-parse V1.0
        # Pour retrouvé ce à quoi correspond ce tag
        git cat-file -p SHA1_tag

* Lister les tags

        git tag [--list [pattern]]
        # Avec le commentaire associé
        git tag -n

* Supprimer un tag (si on s'est emmelé les pinceaux ..., sinon le laisser et en faire un nouveau)

        # suppression en locale
        git tag --delete tagname
        # suppression en remote
        git push --delete origin tagname


### Différences entre fichiers, commits

* Différences entre le répertoire de travail et l'index

        git diff

* Différences entre le répertoire de travail et l'index en affichant les mots modifiés colorés

        git wdiff

* Différences entre le répertoire de travail et le commit

        git diff commit

* Différences entre les modifications staged dans l'index et le commit (commit peut être HEAD). `--cached` = `--staged`

        git diff --cached commit

* Différences entre deux commits

        git diff commit1 commit2

    * `-M `: détection des renommages
    * `-w` : différences sans considérer les whitespaces
    * `--stat` : ajout de statistiques
    * `--color` : colorisation de la sortie (une couleur par type de différences)


### Modifications de commits

#### Suppression d'un fichier accidentellement mis à l'état *staged*

    git add foo.c 
    # ah ben non, je voulais pas
    # Obtention de la liste des fichiers à l'état staged
    git ls-files
    # fais en sorte que mon index ressemble à HEAD pour le fichier foo.c
    git reset HEAD foo.c
    # Pour vérifier
    git ls-files

#### Modification du message du dernier commit

    git commit --amend

### Modification de l'auteur du dernier commit

    git commit --amend --author "Bob <kbob@gmail.com>"

#### Marche arrière sur un commit

    # dans master, les commits sont A B C D E F G
    # Commit D a introduit une erreur, il faut l'enlever à G
    git revert master~3
    # les commits dans master sont A B C D E F G D'

#### Modification d'un fichier du dernier commit

    # Le dernier commit contenait le fichier speech.txt
    # Edition de speech.txt
    git add speech.txt
    git commit --amend
    # Possibilité d'éditer le message du dernier commit

### Rebase et cherry-pick

#### Application d'un commit d'une branche vers une autre

    # dans la branche dev, HEAD~2 est un commit qui contient un correctif pour un bug aussi présent dans la branche rel_2.3
    git checkout rel_2.3
    # correctif dans HEAD~2 pour la branche dev -> dev~2
    git cherry-pick dev~2
    # il s'agit bien d'un nouveau commit
    # attention aux conflits potentiels à résoudre

#### Application de plusieurs commits d'une branche vers un autre

    # application commits de dev dans master
    # dans master, les commits sont A B C D ; dans dev (branche créée à partir de B), les commits sont V W X Y Z
    git checkout master
    # git cherry-pick X..Z

#### Marche arrière sur un commit

    # dans master, les commits sont A B C D E F G
    # Commit D a introduit une erreur, il faut l'enlever à G
    git revert master~3
    # les commits dans master sont A B C D E F G D'

#### Modification d'un fichier du dernier commit

    # Le dernier commit contenait le fichier speech.txt
    # Edition de speech.txt
    git add speech.txt
    git commit --amend
    # Possibilité d'éditer le message du dernier commit

#### Rejouer les commits présents dans master mais pas dans dev

    # master : A — B — C – D
    # dev        \ Y — Z
    git checkout dev
    git rebase master
    # équivaut à git rebase master dev
    # dev devient : A — B — C — D — Y' — Z'

#### Rebase locally

    git checkout ma_branche
    git pull --rebase
    git checkout master
    git pull --rebase
    git checkout ma_branch
    git rebase master
    # Résolution des conflits puis `git rebase --continue`
    git push

### Les branches

#### Créer un branche sur un repo distant

* Créer la branche sur un repo local
* Taper la commande suivante :

        git push --set-upstream origin mabranche


#### Pousser une branche locale vers un repo distant

    git push --set-upstream origin enh/update_mirror
    

####  Afficher les branches

    # Lister les branches
    git branch
    # Voir les commits d'une branche
    git show-branch
    # Connaitre les branches de noms bug
    git show-branch 'bug/*'
    #   * signifie commit a été présent dans la branche active
    #   - désigne un merge commit
    #   + signifie présent dans la branche
    #   ! signifie branche active
    #   Si plusieurs signes (+*+) signifie que le commit est présent dans plusieurs branches
    # s'arrête au premier commit commun (--more=num pour en voir plus)

#### Retrouver le commit depuis lequel une branche (new-branch) a été créée à partir d'une autre branche (original-branch)

    git merge-base original-branch new-branch

#### Création d'une branche (ATTENTION ça ne crée que le nom, ça ne devient pas la branche active)

    # À partir de la HEAD de la branche courante, nouvelle branche = prs/pr-1138
    git branch prs/pr-1138
    # À partir d'un autre commit de la branche. starting-commint = nom de branche ou un SHA1
    git branch prs/pr-1138 starting-commit 

#### Rendre une branche active. Si existe modifications non-commitées de l'ancienne branche : git ne fera rien

    # besoin de mettre de côté des commit : *stash* les modifications
    #  ou merger les changements dans une autre branche.
    # Aucune modification dans le working directory
    git checkout ma_branche
    # Modifications à merger dans dev. 
    #   Le merge se fait dans le répertoire de travail. Pas de merge commit sur un branche.
    #   Attention au possible conflit
    git checkout -m dev

#### Création et activation d'une branche en une seul commande

    git checkout -b bug/pr-3
    # forme générique
    git checkout -b new-branch start-point

#### Si suppression accidentelle d'une branche

    git reflog …

#### Suppression d'une branche distante

À faire avec git gui, pas réussi avec les commandes suivantes

    # création tête de branche détachée ayant accès à la branche à supprimer
    git checkout remotes/origin/master
    # suppression de la branche
    git branch -d remotes/origin/doc/readbook


### Les merges

#### Édition de conflits

Lorsque `git merge` annonce des conflits, il faut trouver leurs localisations :

    git diff

Il faut ensuite éditer à la main les conflits, les mettre en état *staged*, et faire le `commit`.

Pour voir le résultats du merge :

    # pour voir le résultat du merge
    git log --graph --pretty=oneline --abbrev-commit

Pour résoudre les conflits, le plus simple semble être de :
1. Ouvrir tous les fichiers conflictuels dans un seul éditeur
2. Résoudre les conflits et sauvegarder le fichier pour tous les fichiers
3. Vérifier qu'il n'y a plus de conflits à coup de `grep <<<<` et `grep >>>>`

#### Localisation des conflits

    git status # ou git ls-files -u
    # Voir les différences selon ma version
    git diff HEAD # équivalent à git diff --ours
    # Voir les différences selon l'autre version
    git diff MERGE_HEAD # équivalent à git diff --theirs
    # voir les conflits différemment (avec les commits)
    #   --merge : seulement les commits liés au conflit
    #   --left-right : affiche < si le commit est de la partie gauche du merge ("our"), ou > sinon (their)
    #   -p : affiche le message et le path associé de chaque commit
    git log --merge --left-right -p
    # Idem pour un seul ficiher
    git log --merge --left-right -p hello
    # Voir tous les états de tous les fichiers (avec les versions 1 – merge à éditer, 2 – our, 3 — their)
    git ls-files -s
    # 100644 ce013625030ba8dba906f756967f9e9ca394464a 1 hello
    # 100644 e63164d9518b1e6caf28f455ac86c8246f78ab70 2 hello
    # 100644 562080a4c6518e1bf67a9f58a32a67bff72d4f00 3 hello
    # Voir tous les fichiers conflits actuels
    git ls-files -u
    # Si on a édité tous les conflits, le numéro de version devient 0
    git ls-files -s
    # 100644 ebc56522386c504db37db907882c9dbd0d05a0f0 0 hello

#### Abandon d'un merge

    # Seulement si le git commit n'a pas été exécuté
    git reset --hard HEAD
    # -> le répertoire de travail et l'index sont dans l'état juste avant le `git merge`
    
    # Si le git commit a été exécuté - ORIG_HEAD = HEAD juste avant le git merge
    git reset --hard ORIG_HEAD
    # -> le répertoire de travail et l'index sont dans l'état juste avant le `git merge`


### Mettre de côté son travail : `stash`

* Sauvegarde du travail (*WIP* = *Work In Progress*)

        git stash save "WIP: message"

* Récupération du travail

        git stash pop

* Connaître la liste des contextes *stashed*

        git stash list

* Voir les différences

        git stash show
        # différences plus précises
        # — par défaut, montre les différences avec stash@{0}
        git stash show -p stash@{1}


### Repositories distants

* Pour récupérer les objets et les metadata liées d'un remote repository

        git fetch

* Idem que `git fetch`, avec un merge des modifications dans le branche locale correspondante (la 1ère étape est `git fetch`).

        git pull

* Transfert des objets et des metadata liées vers un remote repository

        git push

* Affiche la liste des références d'un remote donné (sur un upstream server). Répond à la question "Mise à jour disponible ?".

        # depuis un repo local
        git ls-remote
        # depuis n'import quel répertoire
        git ls-remote ssh://vcs-user@svn-s.mon.site.web/diffusion/SLUG/slug.git
        # ou plus simplement
        git ls-remote origin

* Liste les références du repo courant

        git show-ref


* Création d'un dépôt à partir de repo git existant

        cd /tmp/depot
        git clone --bare ~/rep_contenant_repo_existant nouveau_repo.git
        # Le dernier argument permet de renommer le repo. Par convention, les bare repo ont des noms suffixés par *.git*.
        # --bare, donc pas de remote `origin` dans .git/config.
        # Configuration du remote repo (ici le remote aura pour nom origin)
        git remote add origin path_vers_repo_distant

* Création de *remote tracking-branches*

        # liste toutes les branches
        git branch -a
        # ajout la branche master local au remote repo 
        #     the local repo's notion of origin has been updated based on information brought in from the remote repo
        git remote update
        # la branche sera listée sous le nom remotes/origin/master


## Astuces ##

### Retrouver le contenu d'un fichier connaissant son *hash code* (possibilité de mettre une partie du SHA1 – 3b18e512d)

    git cat-file -p 3b18e512dba79e4c8300dd08aeb37f8e728b8dad

### Voir les noms relatifs de l'historique

    # -> SHA1 de la master
    git rev-parse master
    # Les dix dernières lignes de l'historique
    git show-branch --more=35 | tail -10
    # -- [master~15] Merge branch 'maint'
    # -- [master~3^2^] Merge branch 'maint-1.5.4' into maint
    # +* [master~3^2^2^] wt-status.h: declare global variables as extern
    # -- [master~3^2~2] Merge branch 'maint-1.5.4' into maint
    # -- [master~16] Merge branch 'lt/core-optim'
    # +* [master~16^2] Optimize symlink/directory detection
    # +* [master~17] rev-parse --verify: do not output anything on error
    # +* [master~18] rev-parse: fix using "--default" with "--verify"
    # +* [master~19] rev-parse: add test script for "--verify"
    # +* [master~20] Add svn-compatible "blame" output format to git-svn
    
    # Plus simplement via
    git reflog

### Connaître le nom réél (SHA1 ID) d'un commit relatif

    git rev-parse master~3^2^2^
    # 32efcd91c6505ae28f87c0e9a3e2b3c0115017d8

### Savoir qui a modifié une ligne d'un fichier en dernier (suivant l'état courant du fichier)

    git blame -L 35, mon_fichier

### Afficher les informations sur le dernier commit

    git log -1

### Rechercher dans l'historique toutes les lignes modifiées contenant une chaine donnée (ici include)

`git log -S` <=> *`pickaxe`*

    # Donne toutes les lignes ajoutées ou supprimées contenant include
    git log -Sinclude --pretty=oneline --abrrev-commit mon_fichierF

### Liste des fichiers dans un status donné

    git ls-files --stage


## Informations diverses

### Reset vs. revert vs. checkout

* `git checkout` – Utilisation d'une branche différents : `git checkout` :
  * la branche courant et HEAD ref sont modifiées pour pointer sur le haut de la nouvelle branche.
  * mais voici des *check out a path*:
    * `git checkout -- path/to/file.c` permet de faire un checkout de file.c à partir de l'index
    * `git checkout v2.3 -- some/file.c` permet de faire un checkout de file.c à partir de la révision v2.3.
* `git reset` remet "à zéro" HEAD ref de la branche courante.
* `git revert` ne fonctionne que sur des commits entiers, pas sur des fichiers.


### Branches


### Branches locales

Les branches locales peuvent être nommés autrement que recommandé **si et seulement si** leur nom n'apparaît pas dans un commit ou un merge d'une des branches présentes dans le repository.
Toutes les branches de développement doivent avoir pour parent la branche dev locale, elle-même un lien vers la branche distante.

### Branches repository

Les branches dans le repository du projet :

* Une branche *master*
* Une branche *dev*

Les branches suivantes sont plutôt des branches locales :
* branche(s) *features* (nom = `feature/description`) : nouvelle fonctionnalité
* branche(s) *bugs* (nom = `bug/description`) : correction de bugs
* branche(s) *releases* (nom = `released/X.Y.Z[w]`) : préparation des nouvelles versions
* branche(s) *hotfix* (nom = `hotfix/description`) : correction de bugs urgents sur des versions release qui donne lieu à une nouvelle release

Parent et branches de merges pour les différentes branches

    Nom         Parent      Lieux de merge
    --------------------------------------
    master      None        None
    dev         master      master
    feature     dev         dev
    bug         dev         dev
    hotfix      master      dev et master via rebase
    [opt]doc    dev         dev


Il faut merger sa branche de développement comme préciser à la rubrique «Fusion de branches» avant de faire un `git push`.



