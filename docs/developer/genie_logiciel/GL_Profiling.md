Title: Profiling et performances
Date: 2018-07-03

# GL : Profiling / Performances


## Profiling mémoire


### De façon simple

Il suffit d'utiliser le *decorator* `profile` de `memory_profiler`.
La sortie est alors en mode texte, et les données sont pour chaque ligne de code.

    Line #    Mem usage    Increment   Line Contents
    ================================================
       185     61.0 MiB     61.0 MiB   @profile
       186                             def run_plot(x_, ly_):
       187     85.9 MiB     24.9 MiB       fig, axs = plt.subplots(1, 3, figsize=(15, 12))
       188     86.8 MiB      0.8 MiB       plot_loop(axs[0], x_, ly_)
       189     86.8 MiB      0.0 MiB       axs[0].set_title('plot_loop')
       190     86.8 MiB      0.0 MiB       plt_args = prepare_one(x_, ly_)
       191     87.5 MiB      0.7 MiB       plot_one(axs[1], x_, plt_args)
       192     87.5 MiB      0.0 MiB       axs[1].set_title('plot_one')
       193     88.4 MiB      0.9 MiB       plot_one_with_prepare(axs[2], x_, ly_)
       194     88.4 MiB      0.0 MiB       axs[1].set_title('plot_one_with_prepare')


### Mode fichier

`mprof` comporte 2 phases :
* Génération d'un fichier de données à l'aide de `mprof`  :

        mprof run nemesis/for_intern_debug_purpose/run_perf.py

* Affichage graphique des données générées :

        mprof plot mprofile_20180703092327.dat

`mprof plot` produit le graphique suivant :
![mprof_run_perf](../../images/mprof_run_perf.png)


## Mesures de temps d'exécution

### De façon simple

Le module `tools` de nemesis contient des décorators :
* `Timer` permet de mesurer les temps d'exécution d'une fonction / méthode (temps cumulé) ;
* `LoggerTimer` permet de mesurer les temps d'exécution d'une fonction / méthode (temps cumulé) avec une personnalisation du message affiché ainsi que la destination de l'information (sortie
    standard ou fichier) ;
* `trace` de tracer simplement les appels internes à une fonction / méthode.

Les deux premiers sont utilisables en temps que décorateurs ou à l'aide du mot clé `with`.


### Mode fichier

`cProfile` couplé à snakeviz permet d'obtenir le temps passé dans chacun des appels.

Il y a 2 phases :
* Génération d'un fichier de données à l'aide de `cProfile` :

        python -m cProfile -o run_perf.prof nemesis/for_intern_debug_purpose/run_perf.py

* Affichage graphique des données générées :

        snakeviz run_perf.prof

L'outil `snakeviz` permet une exploration des données. On peut choisir la profondeur de la pile d'appel pour se concentrer
sur les appels de haut niveau.

`snakeviz` produit le graphique suivant (ici pour le module run_perf.py — ligne n°3) :
![mprof_run_perf](../images/snakeviz_run_perf.png)

À noter que pour ce graphique, `run_perf` mesurait aussi les temps d'exécutions de quasiment toutes les méthodes,
et que la méthode `run_plot` était instrumenté par le décorateur `profile`.
