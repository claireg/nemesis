.. _nemesis_logging:

===================
GL : Logs du projet
===================

*logging* = journalisation

.. todo::

    * Logs
        * Mettre les fichiers de logs dans un home utilisateur ... comment faire quand la config est dans un ``json`` ?

.. important::
    * Si le code appelant ``nemesis`` (et l'autolog) est mis dans un bloc ``with`` alors, il n'y a pas de logs de session ...
    * Il n'y a pas de log pour les méthodes ``matplotlib.pyplot`` appelées directement depuis le C++.
    * Le décorateur ``LoggerTimer`` ne fonctionne pas avec les méthodes appelant l'autolog


Généralités sur le logging
==========================

Selon la doc : 

* Un logger par module utilisant ``logging``. Mais c'est vite ingérable pour s'y retrouver.
* Un handler par logger

Selon le site `datahq <https://www.datadoghq.com/blog/python-logging-best-practices/#pythons-logging-module-basics>`_

* Avoir un logger par module n'est pas pratique pour les configurer correctement pour garder une certaine cohérence entre les modules
* Ne pas hésiter à faire un fileConfig (ou un dictConfig) pour les logs : ``Flask`` et ``Django`` font ça.

**Formatter les logs en *json* permet de plus facilement retrouver les infos.**

On peut faire vraiment plein de choses juste avec le module ``logging`` de Python.

Avant tout, il faut décider de ce que l'on veut (et s'y tenir).

On peut aussi avoir un logger avec plusieurs handlers :

* les erreurs sont affichées sur la console
* les messages de niveau inférieur le sont dans un fichier

Chaque handler peut avoir son propre formatter ou utiliser le même.

On peut aussi «Buffering logging messages and outputting them conditionally» : 

* Si on ne veut pas logger toutes les informations de debug si une fonction s'exécute correctement
* Logger les infos de debug si la fonction plante

On peut choisir la façon de nommer des FileHandler (et notamment les ``RotatingFileHandler``). Pas sûr qu'on puisse
faire ça avec une config dans un fichier ou un dictionnaire.

`ici <https://stackoverflow.com/questions/790757/import-json-file-to-couch-db>`_ du code pour transférer ses logs json dans
une base ``CouchDB``.

Il existe des ``TimedRotatingFileHandler``. Ils peuvent être pratiques pour les logs d'utilisation : lorsque le fichier
est changé, on doit pouvoir l'envoyer en mail ou sur une base de données.

Besoins de logging dans ``nemesis``
===================================

Trois types de logs potentiellement utiles pour le projet :

* Logs d'utilisation (pour les développeurs) : attention partie C++ et Python
* Logs de debug (pour les développeurs)
* Logs de session (pour l'utilisateur)

On peut avoir autant de fichier de logs que l'on veut. Dans notre cas :

* Un seul fichier de log pour 3 fonctions ? possibilité de diviser le fichier de log en 3 en fin de session pour dispatcher les infos
* Un fichier «scripting», et un fichier «debug / utilisation» ?
* Trois fichiers ?

Après différents essais, la configuration des logs doit être mise dans un *dictConf* ou un fichier *json* pour plus de souplesse.
Le *fileConf= s'est bien, mais au cas où un filtre pour les logs serait utile un jour, il faudrait la refaire.

Solution retenue
----------------

Les logs d'utilisation et les logs de debugs seront mis dans le même fichier au format *json*.
Les fichiers de logs seront dans un home temporaire utilisateur, puis seront envoyé périodiquement
vers un espace commun (une base de données *CouchDB* qui accepte le *JSON* par exemple).

Toutes les méthodes de ``nemesis.plotter.PyPlotCEA`` attrapent l'exception ``RunTimeError`` pour que ce soit
plus simple à debugger depuis le C++.

.. warning::

    Ne pas mettre de catégorie root sinon uselog s'affiche sur la console
    ```json
        "root": {
        "handlers": ["console"]
        }
    ```

L'appel à ``autolog_error`` logge dans :

* la console (logger 'nemesis')
* fichier session / autolog. Contient aussi la stack et le traceback
* fichier «debug / utilisation». Contient le premier et le dernier appel de la stack

Il est possible de désactiver les logs en définissant une des variables d'environnement suivantes :

* ``'NEMESIS_NO_AUTOLOG'`` pour désactiver les logs d'utilisation et de session
* ``'NEMESIS_NO_SESSIONLOG'`` pour désactiver les logs de session (les plus coûteux)
* ``'NEMESIS_NO_USELOG'`` pour désactiver les logs d'utilisation


Log d'utilisation
=================

* Pour qui ?
    * Le développeur
* Pourquoi ?
    * Permet de connaître les fonctionnalités utilisées
    * Faire des statistiques sur ce que fait l'utilisateur
    * Ne garder des traces que pour les fonctionnalités de haut niveau
    * Logger la taille des données (mais pas leurs valeurs)
* Format ?
    * *json*
* Le message contiendra au moins
    * La date
    * Le niveau du message
    * Un message
    * D'autres paramètres si nécessaire (le *pid* par exemple)

Il y aura un premier log lors de l'import de ``nemesis`` qui contiendra

* Version de python utilisée basé sur ``sys.version_info``
* Des informations sur la plateforme (``sys.plateform`` sous Linux)
* le pid, le ppid (histoire de retrouver les logs d'une même session) (``os.getpid``, ``os.getppid``)
* Des informations sur l'utilisateur  ``getpass.getuser`` / ``os.getresuid`` plutôt ?

Ce sont les méthodes de haut-niveau de ``pyplotcea`` qui seront loggées, ainsi qu'**à terme** les méthodes de haut-niveau
de ``matplotlibcpp``.

.. important::

    Pour l'instant, seules les méthodes de haut-niveau de ``pyplotcea`` sont loggés (via la méthode ``autolog``
    qui s'occupe aussi des logs de sessions. Pour les paramètres volumineux type ``numpy.ndarray``, on logge
    la taille, la forme et la dimension du tableau. Ça permettra d'avoir un ordre d'idées de la taille des
    données manipulées par les utilisateurs.


Log de debug
============

* Pour qui ?
    * Le développeur
* Pourquoi ?
    * Debugger une fonctionnalité
    * Savoir par où les utilisateurs sont passés avant de planter
* Format ?
    * *json*
* Le message contiendra au moins
    * Des informations génériques sur le processus / la machine / ...
    * La *stack*
    * Le *traceback*


A-t-on besoin de logs de debug uniquement quand ça plante ou alors tout le temps ?
Pour le moment, je dirais que que si l'on a besoin de logs de debugs plus complet,
il faudra utiliser le décorateur ``trace`` sur la fonction voulue.

Les logs de debug seront dans le même fichier que les logs d'utilisation.


Log de session
==============

* Pour qui ?
    * l'utilisateur / le developpeur
* Pourquoi ?
    * Pour pouvoir rejouer sa session (pratique pour le développeur qui voudrait debugger)
    * Le code doit être exécutable
    * Doit comporter tous les lignes tapées par l'utilisateur (si en interactif, donc ici il faut ``readline``)
* Format ?
    * Python : ``autolog.py``
    * Exemple

            # en-tête : date, heure, qui, quoi, machine?
            commandepython1
            commandepython2
            # Si erreur : traceback et stack

Les ``numpy.ndarray`` sont stockés dans des fichiers ``npy``, pour que les commandes restent lisibles. Le fichier
de log de session intègre le chargement et le remplacement de ces variables dans les fonctions utilisées.
Pour l'instant ces logs ne sont utiles que pour le développeur pour le debug car ``nemesis`` n'est pratiquement jamais
utilisé en interactif (y'a que les développeurs qui s'y collent, et encore ça dépend des données à afficher).

Il faut logger uniquement les méthodes de haut-niveau de ``nemesis``. Pour l'instant, seules les fonctions
de ``pyplotcea`` ont un intérêt. **Quid du C++ qui appelle directement ``matplotlib`` ?**
Attention à ne pas utiliser ``readline`` pour se créer un historique, mais ``inspect``, histoire que ça fonctionne
quand on lance un script ou qu'on appelle ``nemesis`` depuis du C++.

Log utilisateur à stocker dans le home utilisateur : un fichier par *import* de ``nemesis``.

Le jour où des utilisateurs commenceront à attaquer ``nemesis`` en interactif, on pourra faire alors un historique «classique»
à l'aide de ``readline``. C'est l'un ou l'autre, pas les 2.

.. caution::
    * ``sys.argv`` est vide quand ``nemesis`` est utilisé depuis le C++, utilisation de ``ps`` pour les infos

.. important::
    L'*autolog* (log session + utilisation) est branché dans ``nemesis.pyplotcea``. Ça fonctionne correctement
    pour les tests que j'ai pu faire. Cependant, toutes les méthodes ne passant
    pas par le python de ``nemesis`` n'apparaissent pas, *i.e* les fonctions de ``matplotlibcpp`` appelant directement
    des fonctions de ``matplotlib``.

Performances
============

Mesure de perf avec et sans exécution de la fonction ``autolog``. Ce qui a été mesuré, c'est le temps pour exécuter
toutes les fonctions ci-dessous, avec ou sans l'``autolog``.

Code de test (``examples/python/developer/dev_tests/bug_format_args.py``) ::

    x_array, y_array, z_array = load_data()
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_figure_xy(x=556, y=91, width=800, height=600)
    nem_plt.PyPlotCEA.set_window_title(title='Eastworld')
    nem_plt.PyPlotCEA.set_linewidth(width=1.0)
    nem_plt.PyPlotCEA.logarithmic_contourf(x=x_array, y=y_array, z=z_array,
                                           masked_value=0.0, cmap='viridis', label='$x^2 * cos(y) + y^2*sin(x)$')
    nem_plt.PyPlotCEA.set_titles(title_bbl='TITRE', sub_title='... SOUS TITRE...', title_mat='MAT',
                                 title_bbl_pos=(0.0, 0.95), title_bbl_font_size=20, sub_title_pos=(0.1, 0.9),
                                 sub_title_font_size=12, title_mat_pos=(-1.0, 0.9), title_mat_font_size=16)


Voici les résultats des mesures de temps avec et sans autolog
(``examples/python/developer/tests_and_profiling/perf_autolog_old.py``):

.. image:: ../../images/perf_autolog.png

Pour savoir si un type de log était plus coûteux qu'un autre, il a été fait en sorte de pouvoir débrancher l'un ou
l'autre. Il y a maintenant 4 possibilités :

* sans aucun log
* avec tous les logs
* avec les logs d'utilisation uniquement
* avec les logs de session uniquement

L'exemple précédent, ainsi que le fichier pour analyser les données ont été modifié pour effectuer toutes les mesures.
Voici le résultat :

.. image:: ../../images/perf_autolog_full.png

La légende :

* orange : sans autolog
* bleu : autolog : diff=0.030, ce qui représente 15,83% de temps supplémentaire par rapport à sans autolog
* vert : sessionlog (avec les logs de session) : diff=0.0291 soit 15,33% supplémentaire
* rouge : uselog (avec les logs d'utilisation) : diff=0.026 soit 14,35% supplémentaire

On remarque que les logs de session sont assez coûteux. En effet, chaque tableau numpy est sauvegardé sur disque pour
que le script puisse être rejoué.
En l'état, il serait prudent de désactiver les logs de session quand on ne cherche pas à debugger ou que l'utilisateur
n'en ait pas besoin.

Ici, les temps sont mesurés pour 6 appels de fonctions. Ce sont des temps quasi fixes, hormis pour le logs de session
car nécessite la sauvegarde des tableaux. Le surcoût devrait être acceptable si seuls les logs d'utilisation sont activés.

.. note::
    Comme pour l'instant, le mode développeur prévaut sur le mode utilisateur, les logs complets sont laissés actifs.


Syntaxe des fichiers de logs
============================

Exemple de logs d'utilisation / debug pour une session ::

    {"asctime": "2020-05-07 16:39:54,628", "name": "uselog", "levelname": "INFO", "message": "nemesis.__init__", "date": "2020-05-07T16:39:54.626159", "pinfo": {"caller": "python", "argv": ["/home/guilbaudc/Devel/projects/wnemesis/examples/python/developer/dev_tests/bug_format_args.py"], "pid": 6766, "ppid": 28818}, "user": {"name": "guilbaudc", "resuid": [1010, 1010, 1010]}, "python_version": [3, 7, 4, "final", 0], "platform": {"system": "Linux", "node": "ubuntu", "release": "4.15.0-91-generic", "version": "#92~16.04.1-Ubuntu SMP Fri Feb 28 14:57:22 UTC 2020", "machine": "x86_64", "processor": "x86_64"}}
    {"asctime": "2020-05-07 16:39:54,853", "name": "uselog", "levelname": "INFO", "message": "uselog", "pid": 6766, "func_name": "nemesis.plotter.pyplotcea.nemesis_params", "func_args": {}}
    {"asctime": "2020-05-07 16:39:54,853", "name": "uselog", "levelname": "INFO", "message": "uselog", "pid": 6766, "func_name": "nemesis.plotter.pyplotcea.set_figure_xy", "func_args": {"x": 556, "y": 91, "width": 800, "height": 600}}
    {"asctime": "2020-05-07 16:39:54,853", "name": "uselog", "levelname": "INFO", "message": "uselog", "pid": 6766, "func_name": "nemesis.plotter.pyplotcea.set_window_title", "func_args": {"title": "Eastworld"}}
    {"asctime": "2020-05-07 16:39:54,853", "name": "uselog", "levelname": "INFO", "message": "uselog", "pid": 6766, "func_name": "nemesis.plotter.pyplotcea.set_linewidth", "func_args": {"width": 1.0}}
    {"asctime": "2020-05-07 16:39:54,854", "name": "uselog", "levelname": "INFO", "message": "uselog", "pid": 6766, "func_name": "nemesis.plotter.pyplotcea.logarithmic_contourf", "func_args": {"x": {"shape": [50], "ndim": 1, "size": 50}, "y": {"shape": [50], "ndim": 1, "size": 50}, "z": {"shape": [2500], "ndim": 1, "size": 2500}, "kwargs": {"masked_value": 0.0, "cmap": "viridis", "label": "$x^2 * cos(y) + y^2*sin(x)$"}}}
    {"asctime": "2020-05-07 16:39:55,045", "name": "uselog", "levelname": "INFO", "message": "uselog", "pid": 6766, "func_name": "nemesis.plotter.pyplotcea.set_titles", "func_args": {"title_bbl": "TITRE", "sub_title": "... SOUS TITRE...", "title_mat": "MAT", "kwargs": {"title_bbl_pos": [0.0, 0.95], "title_bbl_font_size": 20, "sub_title_pos": [0.1, 0.9], "sub_title_font_size": 12, "title_mat_pos": [-1.0, 0.9], "title_mat_font_size": 16}}}


Exemple de logs de session pour une autre session ::

    #
    # Some information:
    #   Date: 2020-05-07T16:43:31.216530
    #   Process:
    #     Called from python: ['/home/guilbaudc/Devel/projects/wnemesis/examples/python/developer/dev_tests/eastworld_3d_raised_contourf.py']
    #     Pid: 7078	Ppid: 28818
    #   User:
    #     Resuid: (1010, 1010, 1010)
    #     Name: guilbaudc
    #   Python version: 3.7.4.final.0
    #   Platform:
    #     system: Linux
    #     node: ubuntu
    #     release: 4.15.0-91-generic
    #     version: #92~16.04.1-Ubuntu SMP Fri Feb 28 14:57:22 UTC 2020
    #     machine: x86_64
    #     processor: x86_64
    #
    import nemesis

    x_dd2f89205af14aaf83efcb1ec9e2f6de = np.load('/tmp/log/nemesis/x_dd2f89205af14aaf83efcb1ec9e2f6de.npy')
    y_adef9987b51b43eab89d362500763b43 = np.load('/tmp/log/nemesis/y_adef9987b51b43eab89d362500763b43.npy')
    z_7341c06c018f45ccbbf3c2b0133b0b51 = np.load('/tmp/log/nemesis/z_7341c06c018f45ccbbf3c2b0133b0b51.npy')
    nemesis.plotter.pyplotcea.raised_contourf(x = x_dd2f89205af14aaf83efcb1ec9e2f6de, y = y_adef9987b51b43eab89d362500763b43, z = z_7341c06c018f45ccbbf3c2b0133b0b51, cmap='current', label='$x^2 * cos(y) + y^2*sin(x)$')
    nemesis.plotter.pyplotcea.switch_axes(mode='3d')


Logging de la partie C++
========================

L'appel à ``nemesis.tools.autolog`` et ``nemesis.tools.autolog_error`` ne fonctionne pas depuis le C++. La *frame*
obtenue via ``inspect`` est vide. Des gardes-fous ont été ajoutés pour que le programme ne plante pas si la
*frame* est nulle.

J'ai essayé de logger en C++.

Pour récupérer le nom de la fonction :
* ``__func__`` : norme C++11 : ça fonctionne si mis directement dans la fonction, pas dans une macro
* ``__FUNCTON__`` : C : ça fonctionne directement dans la fonction et dans une macro
* ``__PRETTY_FUNCTION__`` : Gnu compiler : c'est pratiquement ce qu'on veut, mais ça retourne un ``char*``

Les affichages sur la sortie standard pour la fonction ``named_plot`` ::

    __func__ = named_plot
    __FUNCTION__ = named_plot
    __PRETTY_FUNCTION__ = bool matplotlibcpp::named_plot(const string&, const std::vector<Numeric>&, const std::vector<Numeric>&, const string&) [with Numeric = double; std::__cxx11::string = std::__cxx11::basic_string<char>]

J'ai essayé avec un ``std::function``, mais ça veut pas compiler, et je vois pas pourquoi ::

    std::function<bool(const std::string&, const std::vector<Numeric>&, const std::vector<Numeric>&, const std::string&)> current(named_plot);
    std::cout << "Target type : " << current.target_type().name() << '\n';

J'ai aussi essayé avec ``typeinfo.h`` ou ``functionnal`` :
* ``typeid(name).name()``
* ``typeid(x).name()``

Ça donne ::

    Type id name : NSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
    Type id x : St6vectorIdSaIdEE

C'est pas mal, mais on récupère des informations sous forme de *hash*, il faut les *démangler* pour les utiliser.
Du coup, c'est pas portable car ça dépend du compilateur (``#include <cxxabi.h>``)::

    const std::type_info  &ti = typeid(x);
    int status;
    std::cout << ti.name() << " : " << abi::__cxa_demangle(ti.name(), 0, 0, &status) << " : " << status << '\n';

Ça donne quelquechose de quasi exploitable ::

    St6vectorIdSaIdEE : std::vector<double, std::allocator<double> > : 0

On pourrait créer une méthode ``to_autolog`` par type de paramètres possibles (int, string, vector<Numeric>, ...) pour
écrire dans le fichier de logging (celui du Python – *a priori* assez simple de récupérer son chemin complet), et en
utilisant ``__func__`` on s'en sortirait. Par contre, il faudrait gérer proprement les écritures dans les fichiers
de logs (session et utilsation/debug).

Il faudrait ouvrir / fermer le fichier à chaque écriture, et pour les objets complexes dans les logs de session,
il faudrait les écrire à l'aide de l'api C de numpy dans un fichier ``.npy``.
Cela nécessite encore un certain temps de développement.

.. note::
    Ces développements ne sont pas prioritaires, on les met de côté pour le moment.