# GL : Python et Environnement Virtuel

Les environnement python peuvent être créés de deux façons :
* à l'aide de `Spack`.
* à l'aide de `pip` + `venv`,

## `pip` + `venv`

L'utilisation de `pip` est plutôt pour les installations où une connexion internet est disponible.
Mais une fois un environnemt créé, il est possible de télécharger tous les packages pour en
refaire un autre hors connexion :

    # existe option pour ne prender que ce qui est en user-site, 
    # ou dans l'environnement virtuel (pip freeze --help)
    pip freeze > requirements.txt
    pip download -r requirements.txt

Principe :
1. [optionnel] Installer un python 
2. `python3 -m venv /path/to/new/virtual/environment`
3. faire autant de `pip install` qu'il y a besoin ou faire un `pip install -r requirements.txt` 
(existe un fichier dans <workdir_nemesis>/tools)

## Avec `Spack`

### Environnement avec une vue

Si le `Spack` utilisé accepte l'utilisation de commande `spack` dans un script `bash` ET qu'il 
n'a pas de soucis avec les environnements `Spack`, il faut utiliser le script `create.env.py3.bash` 
avec l'option `-s` – doris : PySide2 + vue dans environnement, et si on veut l'option `-d` (mode
développement).

En gros, ça donne :

     spack env create --with-view /path/vers/vue/a/creer nom_environnement
     spack env activate nom_environnement
     spack add python@x.y.z
     spack add py-numpy ^python@x.y.z%gcc@x.y.z
     ....
     spack install

Pour l'utiliser, il suffit de taper

    # activation spack
    spack env activate mon_environnemnt

Attention, au T4 2019, plus il y avait de packages, plus l'ajout d'un package était lent ...

### Simple vue 

Comme plusieurs des installations Spack disponible en interne, ne font pas bon ménage avec
les environnements ou les appels dans des scripts `bash`, on peut faire simplement une vue.
Ce qui revient à faire plusieurs `spack install` (ne pas oublier les dépendances à python et gcc),
et à créer une vue une fois tous les packages voulus installés.

    spack install python@x.y.z %gcc@x.y.z
    spack install py-numpy ^python@x.y.z%gcc@x.y.z
    ...
    spack view symlink /path/vers/vue /spec
    # À faire pour cmake, python, et chacun des packages install«s ci-dessus
    
La vue créée en interne dispose d'un script bash `activate` pour qu'elle puisse être utilisée
comme un environnement virtuel python classique (d'ailleurs le script est à 90% celui généré
avec `venv`).

## Vérification de l'environnement

Après avoir activer le nouvel environnement, il faut exécuter le script `check_installation.py`.

    . <path_vers_env>/bin/activate
    python <workdir>/tools/check_installation.py [--pyside|--pyqt]
    
L'option `--pyside` est le défaut. Pour les environnements fait en intel, ajouter l'option `--pyqt`.

Une fois l'environnement vérifié, il faut forcer l'utilisation du backend `qt5agg` pour cette installation de `matplotlib`.
Dans le répertoire d'installation de `matplotlib` (`…/Nemesis_0.1_gcc_64/lib/python3.7/site-packages/matplotlib`),
aller dans `mpl-data`, à la ligne `backend`, mettre `qt5agg` à la place de `tkagg`.

**Cela permettra aux testset aux exemples de bien tester ce qui est utilisé dans Gaia.**


## Connaître la liste des packages d'un environnement

Après avoir activer le nouvel environnement, il faut exécuter le script `check_installation.py` avec l'option `-l`.

    . <path_vers_env>/bin/activate
    python <workdir>/tools/check_installation.py --list

## Problèmes rencontrés et solutions

### Installation terminant en erreur : *Disk quota exceeded*

* Vérifier que le *sticky bit* est correctement positionné sur tout le path.
* Au besoin, ne pas supprimer le répertoire de *build* est changé le *sticky bit* et le groupe pour TOUT ce répertoire.
* On peut aussi préfixer les commandes de build et d'installation par `sg opendist -c ...`
    ```bash
    sg opendist -c ...
    ```

* Au cas où ça ne fonctionnerait toujours pas, vérifier que le répertoir `site-packages` 
du nouvel environnement virtuel a bien le *sticky bit* de positionner :
    ```bash
    cd path_venv/lib/python3.7
    chmod g+s site-packages/
    ```

## Package avec sources modifiées

* `PySide2` : pour que shiboken2 ait bien le runpath de la lib python. Dans `sources/shiboken2/CMakeList.txt`, 
le comportement par défaut ne lie pas la lib `shiboken2` à la lib python car c'était fait automatiquement par le linker 
qui utilise cette lib ou à l'exécution. Ça fonctionne dans 95% des cas (utilisation de matplotlib en python, 
utilisation via un exe C++ qui appelle matplotlib) mais pas dans Gaia (chargement dynamique d'une lib via un dlopen – 
mode LAZY uniquement.

* `numpy` : pour que l'import de `numpy` ne plante pas si il est fait dans une lib statique. Le fichier à modifier est `numpy.core.getlimits.py`, ligne 166

```python
# CEA : modif pour que l'import fonctionne dans une lib statique en I4R4 ou I4R8
# _tiny_f128 = exp2(_ld(-16382))
_tiny_f128 = exp2(_ld(-16372))
```

## Compilateurs et environnement

**ATTENTION**, si compilation avec un autre compilateur que celui du système, utilisé une toolchain `CMake` correct
(avec les bonnes options pour mettre les `-Wl,-rpath,...` )
   
## Génération documentation

### Principe général

Pour les packages utilisant `Sphinx`.

1. Détarer ou dezipper les sources du package
2. `cd <pkg>/doc`
3. Activation de l'environnement Python adéquat (ici `Nemesis`)
4. `make html` ou `make.py`
4.bis Si pas de fichier `make*`, et si seulement fichier `conf.py` :

        cd rep_contenant_conf_py
        sphinx-build -M html . _build


Si il y a dans le fichier `conf.py` (fichier pour `Sphinx`), il faut que la variable `intersphinx_mapping` ne
contienne pas de `http://…`.

À l'origine (ou si connexion internet)

    intersphinx_mapping = {
        'python': ('https://docs.python.org/3', None),
        'numpy': ('https://docs.scipy.org/doc/numpy/', None),
        'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
        'pandas': ('https://pandas.pydata.org/pandas-docs/stable', None)
    }

Il faut mettre dse chemins relatifs

    intersphinx_mapping = {
        'python': ('/path/vers/share/python/Python-3.7.0', None),
        'numpy': ('/path/vers/share/python/numpy-1.15.4', None),
        'scipy': ('/path/vers/share/python/scipy-1.1.0', None),
        'matplotlib': ('/path/vers/share/python/matplotlib-3.0.2', None),
    }

`intersphinx` permet d'avoir des liens vers de la documentation extérieure dans son projet.

### Documentation matplotlib

En plus de changer la valeur de la variable `intersphinx_mapping`, il faut faire une modif dans `conf.py` pour le calcul du SHA.
Au lieu de faire appel à `git` (vu que là, on n'a pas fait un clone du repo `matplotlib`), faire les modifications suivantes :

    # General substitutions.
    from subprocess import check_output
    # SHA = check_output(['git', 'describe', '--dirty']).decode('utf-8').strip()
    SHA = '3.0.2-cea-dirty'

Avant de générer la doc, il faut modifier l'exemple `misc/load_converter.py`. Après le print, mettre :

    import dateutil
    # …
    data = np.genfromtxt(datafile, delimiter=',', names=True, dtype=None, converters={0: dateutil.parser.parse})
    # remplacer le ax.plot(...) par
    ax.plot(data['Date'], data['High'], '-')
    
Modifier l'exemple suivant `examples/ticks_and_spines/date_index_formatter2.py` comme suit :

```python
import dateutil.parser
from matplotlib import cbook, dates
import matplotlib.pyplot as plt
from matplotlib.ticker import Formatter
import numpy as np

datafile = cbook.get_sample_data('msft.csv', asfileobj=False)
print('loading %s' % datafile)
msft_data = np.genfromtxt(datafile, delimiter=',', names=True,
        converters={0: lambda s: dates.date2num(dateutil.parser.parse(s))})


class MyFormatter(Formatter):
    def __init__(self, dates, fmt='%Y-%m-%d'):
        self.dates = dates
        self.fmt = fmt

    def __call__(self, x, pos=0):
        'Return the label for time x at position pos'
        ind = int(np.round(x))
        if ind >= len(self.dates) or ind < 0:
            return ''

        return dates.num2date(self.dates[ind]).strftime(self.fmt)

formatter = MyFormatter(msft_data['Date'])

fig, ax = plt.subplots()
ax.xaxis.set_major_formatter(formatter)
ax.plot(msft_data['Close'], 'o-')
fig.autofmt_xdate()
plt.show()
```

Modifier `doc/Makefile` pour que les warnings ne soient pas des erreurs (enlever l'option `-W` à `Sphinx`) :

    SPHINXOPTS = 

### Documentation Python

La documentation est généré via un des environnements Python.

    . <path_vers_env>/bin/activate
    cd <rep_sources_python_utilisé_pour_compilation>/Doc
    make
    cd /path/vers/gcc/python/3.6.4/python/share
    cp -R <rep_sources_python_utilisé_pour_compilation>/Doc/build/html .


## Remarques sur les dépendances entre packages

Si on veut pouvoir faire du *markdown* dans la documentation `Sphinx`, il faut le package `recommonmark` et donc `commonmark`.

