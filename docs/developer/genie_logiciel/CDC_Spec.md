# CDC : Specifications techniques


## Tests

### Charge avec large data

* Tester l'affichage de données volumineuses avec `matplotlib` en Python pur.

### API C Python

* Voir si à partir d'une api simple, on peut générer une api C++

### Tests python

* `doctest`. Par défaut dans la distribution Python (utilisé par `numpy`)
* `pytest`. Module en plus (utilisé par `padawan`, `matplotlib`)
* si besoin `unittest` — par défaut dans la distribution Python

#### Lancement des tests

Voir `nemesis/tests/__init__.py`

### Tests C++

Utlisation de `Catch` pour les tests unitaires.


## Techno utilisées

### Plot

* matplotlib
  * numpy
* extensions de matplotlib

### Documentation

Pour générer la documentation du projet, la documentation utilisateurs et la documentation des packages
    python (installés par `pip`)

* sphinx
* sphinx_gallery
* intersphinx
* markdown
* restructuredText

Les docstrings doivent être au format `numpy` (`numpydoc`).

## Visualisation 3D

Dans `matplotlib`, la 3D se trouve dans le toolkit `mpl_toolkits.mplot3d.axes3d.Axes3D`.
Le wrapping C++ de `matplotlib` expose pratiquement que des fonctions de `matplotlib.pyplot` sauf pour une méthode 
`plot_surface` (de `mpl_toolkits.mplot3d.Axes3D`.

On a choisi une solution quasi-entièrement C++ : le wrapping direct des fonctions de `Axes3D`. Les fonctions d'affichage 
 de graphiques sont : 
* `bar3D`
* `contour3D`
* `contourf3D`
* `plot3D`
* `quiver3D`
* `scatter3D`
* `text3D`
* `plot_surface`
* `plot_trisurf`
* `plot_wireframe`
* `tricontour`
* `tricontourf`
* `voxels`

Les fonctions à faire en premier sont 
1. `plot_surface` (existant dans la dernière version de `matplotlibcpp`), 
2. `plot3D`
3. `scatter3D`. 

Les autres seront faites si besoin.

### Avantages de cette solution

* Modifications localisées dans le C++ (principalement)
* Cohérente avec la dernière master de `matplotlibcpp` (pour `plot_surface`)
* On ajoute que ce dont on a besoin
* Extension en python toujours possibles (à mettre en dehors de `matplotlibcpp de référence`)

### Inconvénients

* `matplotlibcpp` devient moins lisible si il n'est pas restructuré
* Dans chaque fonctions appelant `mpl_toolkits.mplot3d.axes3d.Axes3D`, il faut :
    * Vérifier le type de projection des axe , et switcher si nécessaire (ou ne rien faire ?)

### Obligatoire à faire

#### Avant de commencer les développements

* Blinder les tests de NR / unitaires 
* Découper le fichier `matplotlibcpp.hpp` en plusieurs parties (au moins 2 : partie *master*, partie *CEA*)
    * Les tests C++ sont à repasser. Il faut aussi vérifier le bon fonctionnement des exemples, notamment `eastworld`
* Isoler ce qui concerne la 3D dans `plot_surface` dans des fonctions / objets à part pour être réutilisable
* Avoir un environnement compilé avec `--with-pydebug` pour chercher les fuites mémoires
* Vérifier que `matplotlibcpp-master` a tous les exemples qui fonctionnent (pourquoi la version fusionnée plante à la fin?)
* Valgrind sur des exemples simples

#### Après

* Ajouter une fonction `switch_axes` en C++ **et** Python pour passer d'axes rectilinéaires à des axes 3D (Avoir une version
Python permet d'avoir un code "symétrique" entre le C++ et le Python).
* Wrapper les fonctions de `Axes3D` dont on a besoin (si besoin renommer les fcts de `Axes3D` en ajoutant un 3D)

