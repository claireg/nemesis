# Tips Python et matplotlib

## À considérer avant tout nouveau développement

Avant de commencer à coder une fonctionnalité basique (type structure de stockage, filtre, itérateur, ...), vérifier
que la fonctionnalité n'existe pas déjà dans :

* `matplotlib.cbook`
* le *Python Cookbook* disponible à l'adresse suivante [https://code.activestate.com/recipes](https://code.activestate.com/recipes)

## Remise à plat d'une liste de liste, tuples, ...

Utiliser [`matplotlib.cbook.flatten`](file:///path/vers/share/python/matplotlib-2.2.2/api/cbook_api.html?highlight=cbook%20flatten#matplotlib.cbook.flatten):

```python
from matplotlib.cbook import flatten
ma_liste = (('John', ['Hunter']), (1, 23), [[([42, (5, 23)], )]])
print(list(flatten(ma_liste)))
# Out: ['John', 'Hunter', 1, 23, 42, 5, 23]
```

## Pour aggréger des données dans un objet

Si besoin de créer des connexions entre objets de la structure : `matplotlib.cbook.Grouper`

```python
from matplotlib.cbook import Grouper
class Foo(object):
    def __init__(self, s):
        self.s = s
    def __repr__(self):
        return self.s

a, b, c, d, e, f = [Foo(x) for x in 'abcdef']
grp = Grouper()
grp.join(a, b)
grp.join(b, c)
grp.join(d, e)
sorted(map(tuple, grp))
# Out: [(a, b, c), (d, e)]
grp.joined(a, b)
# Out: True
grp.joined(a, c)
# Out: True
grp.joined(a, d)
# Out: False
```

Si le besoin est ju
Si le besoin est juste de regrouper des objets, avant Python 3.7 on aurait utiliser `matplotlib.cbook.Bunch`,
maintenant, il faut utiliser le décorateur `dataclasses.dataclass`

```python
from dataclasses import dataclass

@dataclass
class Point:
    x: float
    y: float
    w: float = 0.0

p = Point(1.5, 2.2)
print(p) # produits "Point(x=1.5, y=2.2, z=0.0)"
```

## Vérification des arguments dans le paramètre kwargs d'une fonction

Il faut utiliser `matplotlib.cbook.normalize_kwargs(...)`.

## Obtenition de la mémoire utilisée par un process

```python
import matplotlib
matplotlib.cbook.report_memory()
```

## Formatage de texte sur un nombre de caractères

```python
import textwrap
s = 'ma jolie phrase super longue pour tester.'

textwrap.wrap(s, 10)
# Out: ['ma jolie phrase', 'super longue', 'pour tester.']
```

## Affichage 3D

Pour activer la 3D (en python), il faut les lignes suivantes 
```python
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
```

Ensuite, il y a plusieurs façons de faire pour afficher du 3D. La méthode retenue consiste à créer explicitement la boîte
d'axes :
```python
ax = pyplot.gca(projection=mode)
ax.plot3D(x, y)
```

### Exemple complet 
```python
import warnings
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.projections as projections
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

def switch_axes(mode='rectilinear'):
    cax = plt.gca()
    names = projections.get_projection_names()
    if mode not in names:
        # Ou une exception ??
        warnings.warn(f'Type de projection inconnue ({mode}) : possibilité {names}')
        return cax
    if cax.name != mode:
        print(f'##### Switch axes {cax.name} to {mode}')
        fig = plt.gcf()
        fig.delaxes(cax)
        return plt.gca(projection=mode)
    return cax

def build_x_y():
    x = np.linspace(0, 1, 100)
    y = np.sin(x * 2 * np.pi) / 2
    return x * 8, y * 8
    
ax = switch_axes('3d')
xs, ys = build_x_y()
print(f'##### Projection : {ax.name}')
ax.plot3D(xs, ys, ys)
```

### Utilisation de `pyplot.plf` avec une projection différente de *rectilinear*

Attention, si on a utilisé une projection différente de la projection par défaut (*i.e.* `rectilinear`), alors quand
on redessine dans la même projection, alors une boîte d'axes est recréée si `pyplot.clf()` a été appelée. Cette fonction
supprime toutes les boîtes d'axes, textes, artistes, etc. qui ont été créées. La boîte d'axes par défaut étant la `rectilinear`,
on *switche* alors.
Pour éviter de recréer une boîte, si le texte, titre and co de la figure ne change pas, il suffit d'appeler `pyplot.cla()`.

Pour ce qui a été mesuré en C++, le surtout de l'appel à `pyplot.clf()` par rapport à `pyplot.cla()` est d'environ 
40 milliseconds pour 3 afichages 3D (deux `plot3D` et un `plot_surface`).