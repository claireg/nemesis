# Tips C++

API c++ du projet `nemesis`.

## Bonnes pratiques à l'utilisation de `matplotlibcpp`

* Le code utilisant `matplotlibcpp` ne doit pas faire apparaître de `Py_DECREF`, sauf cas exceptionnel.
* L'utilisation de l'API C de Python / numpy doit être faite dans `matplotlibcpp` pour que le code utilisant ce 
    *namespace* reste lisible.

## En cas de plantage dans une fonction de `matplotlibcpp`

Si il y a des plantages aléatoires dans certaines fonctions de `matplotlibcpp`, c'est sûrement qu'il y a un charcutage mémoire.
Ça a été le cas pour `set_label`, et `set_window_title` avec des charcutages dans `print_sys_path` et `add_site` !

* Si il y a un charcutage mémoire, c'est en général au prochain appel à `plt::clf()` que l'application plante. Mais ça 
    peut aussi être à un appel à une fonction de matplotlibcpp. **Il peut arriver que l'utilisation de `valgring memcheck`
    ne détecte aucun problème (et ne plante même pas).
* Les fonctions de `matplotlibcpp`, font des `Py_DECREF` sur les objets utilisés comme arguments, **donc sur les objets
    sous-jacents**, notamment les tableaux C numpy, créés par `get_array(int, Numeric*)`. **Il ne faut surtout
    pas faire de `Py_DECREF`sous peine de plantage qq part après dans un appel à `matplotlibcpp`.**
* JAMAIS de `Py_DECREF` dans les cas suivants (`PyErr_Occurred()` ne voit absolument rien même si il y a bien un charcutage mémoire) :
    * sur un objet créé par `PyImport_ImportModule`
    * sur un item d'une liste (`PyObject *item = PyList_GetItem(sys_path, i);`) — voir doc Py_DECREF
    
### Commandes utiles en cas de problème mémoire

* Recherche de *leaks*

        valgrind --leak-check=full --show-reachable=yes mon_application

* Vérification mémoire (bouton à côté du bouton debug de `CLion` — aussi accessible via l'option `-v` du script `GaiaV3`)

        valgrind --tool=memcheck mon_application

## Notes sur la version de C++ de cette composante

Pour le moment, la version 6.4 de gcc est utilisé avec l'option `c++1z` pour avoir accès à certaines *features* du c++17.

Par défaut, gcc 6.4 est en `-std=gun++14`.
Certaines features du c++17 sont accessibles via `-std=c++1z` et/ou `-std=gnu++17`. Ces options sont normalement valables en gcc 6.4 et en gcc 7.2.

Deux features nous intéresse plus particulièrement :
* `any`
* `filesystem`.

En gcc 6.4 : `any` et `filesystem` sont dans experimental, mais `make_any` n'existe pas
En gcc 7.2 : `any` n'est plus experimental et `make_any` existe. `filesystem` est toujours experimental.

### any

Exemple de code avec `any` quand le type d'un objet n'est déterminé qu'au runtime :

	#include <experimental/any>
	#include <experimental/filesystem>
	std::experimental::any readType(std::string type, std::string value) {
		if (type == "int") return std::experimental::make_any<int>(stoi(value));
		else if (type == "float") return std::experimental::make_any<float>(stoi(value));
		else if (type == "string") return std::experimental::make_any<std::string>(stoi(value));
	}

	int main() {
		auto a = readType("int", "154");
		auto b = readType("float", "154.132");
		auto c = readType("string", "bozo");
	}

### filesystem

    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
    fs::path p('/tmp')
    fs::path p = '/tmp;

Les méthodes utiles :

 * `filepath_.native()` : récupération sous forme de string
 * `filepath_.filename()` : nom du fichier
 * `filepath_.parent_path()` :  nom du répertoire parent


## Couleurs sympas

* jaune-vert : #BBBB7E
* bleu ciel foncé : #7FB0E3
* vert herbe : #8AE234
* vieux rose : #C68585
* ocre : #CFA653


## Tests unitaires

Utilisation de Catch (sources originelles dans `workspace/Catch2-master`).

Les tests sont à part.

Pour que les tests soient activés, il faut activer l'option `BUILD_TESTING` lors de la configuration, et ensuite compiler.
Pour lancer les tests :

    # Exécution dans la console
    TUTestsNemesis

    # Pour avoir un sortie format junit pour jenkins
    TUTestsNemesis -r junit
