.. _notes_nemesis_perf:

====================================
Impact personnalisation des axes log
====================================

Mesure de perfs pour quantifier l'impact des customisation des axes en log.

Données :

* ``nem_rd.curves_rx_data()``
    * En C++, lecture de ``reference_data/curves_rx_data.txt``
    * En Python, lecture de ``lpm_rosseland_rx.psy``
* 54 courbes de 47 points
* Pas de légende affichée

En python
=========

Sauf préciser, les temps de chargement des données a été quantifié (faisait partie des fonctions profilées ou mesurées).

Mémoire
-------

*utilisation de ``memory_profiler``*.

Ligne de commande ::

    python -m memory_profiler ./nemesis/for_intern_debug_purpose/profile_mplconfig.py

Ou ::

    mprof run ./nemesis/for_intern_debug_purpose/profile_mplconfig.py
    mprof plot

Avec personnalisation

+--------+------------+-----------+------------------------------------------------------------------+
| Line # | Mem usage  | Increment | Line Contents                                                    |
+========+============+===========+==================================================================+
|    36  |  60.5 MiB  |  60.5 MiB | @profile                                                         |
+--------+------------+-----------+------------------------------------------------------------------+
|    37  |            |           | def run_mplconfig():                                             |
+--------+------------+-----------+------------------------------------------------------------------+
|    38  |   60.5 MiB |   0.0 MiB |      nem_plt.MatplotlibConfiguration.figure_size(WIDTH, HEIGHT)  |
+--------+------------+-----------+------------------------------------------------------------------+
|    39  |   60.5 MiB |   0.0 MiB |      nem_plt.MatplotlibConfiguration.nemesis_params()            |
+--------+------------+-----------+------------------------------------------------------------------+
|    40  |   61.1 MiB |   0.6 MiB |      x, plt_args = gaia_data()                                   |
+--------+------------+-----------+------------------------------------------------------------------+
|    41  |   87.2 MiB |  26.0 MiB |      plt.plot(\*plt_args)                                        |
+--------+------------+-----------+------------------------------------------------------------------+
|    42  |   87.3 MiB |   0.1 MiB |      nem_plt.MatplotlibConfiguration.set_log_scale(True, True)   |
+--------+------------+-----------+------------------------------------------------------------------+
|   43   |   87.3 MiB |   0.0 MiB |      nem_plt.MatplotlibConfiguration.set_titles(BBL, TITLE, MAT) |
+--------+------------+-----------+------------------------------------------------------------------+
|    44  |   87.3 MiB |   0.0 MiB |      plt.xlabel(XLABEL)                                          |
+--------+------------+-----------+------------------------------------------------------------------+
|    45  |   87.3 MiB |   0.0 MiB |      plt.ylabel(YLABEL)                                          |
+--------+------------+-----------+------------------------------------------------------------------+
|    46  |   87.3 MiB |   0.0 MiB |      plt.grid(True)                                              |
+--------+------------+-----------+------------------------------------------------------------------+
|    47  |   87.3 MiB |   0.0 MiB |      plt.draw()                                                  |
+--------+------------+-----------+------------------------------------------------------------------+
|   48   |  108.7 MiB |  21.5 MiB |      plt.pause(2)                                                |
+--------+------------+-----------+------------------------------------------------------------------+
|   49   |  101.5 MiB |  -7.2 MiB |      plt.close()                                                 |
+--------+------------+-----------+------------------------------------------------------------------+


Sans personnalisation

+--------+------------+-----------+------------------------------------------------------------------+
| Line # | Mem usage  | Increment | Line Contents                                                    |
+========+============+===========+==================================================================+
|   105  |   60.4 MiB |  60.4 MiB | @profile                                                         |
+--------+------------+-----------+------------------------------------------------------------------+
|   106  |            |           | def run_plt():                                                   |
+--------+------------+-----------+------------------------------------------------------------------+
|  109   |  60.4 MiB  |   0.0 MiB |      nem_plt.MatplotlibConfiguration.figure_size(WIDTH, HEIGHT)  |
+--------+------------+-----------+------------------------------------------------------------------+
|   110  |  60.4 MiB  |   0.0 MiB |      nem_plt.MatplotlibConfiguration.nemesis_params()            |
+--------+------------+-----------+------------------------------------------------------------------+
|   111  |  60.4 MiB  |   0.0 MiB |      nem_plt.MatplotlibConfiguration.set_line_width(LINE_WIDTH)  |
+--------+------------+-----------+------------------------------------------------------------------+
|   112  |  61.1 MiB  |   0.8 MiB |      x, plt_args = gaia_data()                                   |
+--------+------------+-----------+------------------------------------------------------------------+
|   113  |  87.2 MiB  |  26.0 MiB |      plt.plot(\*plt_args)                                        |
+--------+------------+-----------+------------------------------------------------------------------+
|   114  |  87.3 MiB  |   0.1 MiB |      plt.xscale('log')                                           |
+--------+------------+-----------+------------------------------------------------------------------+
|   115  |  87.3 MiB  |   0.0 MiB |      plt.yscale('log')                                           |
+--------+------------+-----------+------------------------------------------------------------------+
|   117  |  87.3 MiB  |   0.0 MiB |      nem_plt.MatplotlibConfiguration.set_titles(BBL, TITLE, MAT) |
+--------+------------+-----------+------------------------------------------------------------------+
|   118  |  87.3 MiB  |   0.0 MiB |      plt.xlabel(XLABEL)                                          |
+--------+------------+-----------+------------------------------------------------------------------+
|   119  |  87.3 MiB  |   0.0 MiB |      plt.ylabel(YLABEL)                                          |
+--------+------------+-----------+------------------------------------------------------------------+
|   120  |  87.3 MiB  |   0.0 MiB |      plt.grid(True)                                              |
+--------+------------+-----------+------------------------------------------------------------------+
|   121  |  87.3 MiB  |   0.0 MiB |      plt.draw()                                                  |
+--------+------------+-----------+------------------------------------------------------------------+
|   122  | 100.9 MiB  |  13.6 MiB |      plt.pause(2)                                                |
+--------+------------+-----------+------------------------------------------------------------------+
|   123  |  93.7 MiB  |  -7.2 MiB |      plt.close()                                                 |
+--------+------------+-----------+------------------------------------------------------------------+

Temps d'exécution
-----------------

*utilisation de ``cProfile`` et ``LoggerTimer``*.

Ligne de commande avec ``cProfile``::
    python -m cProfile -o mplconfig.prof ./nemesis/for_intern_debug_purpose/profile_mplconfig.py
    snakeviz mplconfig.prof


Pour ``run_mplconfig`` (pour ``run_plt``, cela ne diffère pas à part l'appel à ``set_log_scale`` et ``set_subticks_log_axes``).
Résultats ``snakeviz`` en affichant que ce qui contient *mplconfig* (champ search de la page web). Classement par *tottime*.

+--------+---------+---------+----------+----------+------------------------------------------+
| ncalls | tottime | percall | cumtime	| percall  | filename:lineno(function)                |
+========+=========+=========+==========+==========+==========================================+
|      1 | 5.6e-05 | 5.6e-05 | 0.02882	| 0.02882  | profile_mplconfig.py:108(gaia_data)      |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 5.5e-05 | 5.5e-05 | 6.7e-05 	| 6.7e-05  | profile_mplconfig.py:100(prepare_one)    |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 5.3e-05 | 5.3e-05 | 4.442	| 4.442	   | profile_mplconfig.py:115(run_mplconfig)  |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 3.1e-05 | 3.1e-05 | 5.887	| 5.887	   | profile_mplconfig.py:78(<module>)        |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 2.3e-05 | 2.3e-05 | 0.000124	| 0.000124 | mplconfig.py:135(set_subticks_log_axes)  |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 1.8e-05 | 1.8e-05 | 7.8e-05	| 7.8e-05  | mplconfig.py:4(<module>)                 |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 1.1e-05 | 1.1e-05 | 6.1e-05	| 6.1e-05  | mplconfig.py:33(figure_size)             |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 8e-06   | 8e-06   | 0.00255	| 0.00255  | mplconfig.py:109(set_log_scale)          |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 7e-06   | 7e-06   | 7e-06	| 7e-06	   | mplconfig.py:14(MatplotlibConfiguration) |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 6e-06   | 6e-06   | 0.00037	| 0.00037  | mplconfig.py:150(set_titles)             |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 3e-06   | 3e-06   | 4.7e-05	| 4.7e-05  | mplconfig.py:24(nemesis_params)          |
+--------+---------+---------+----------+----------+------------------------------------------+
|      1 | 2e-06   | 2e-06   | 1.5e-05	| 1.5e-05  | mplconfig.py:52(set_line_width)          |
+--------+---------+---------+----------+----------+------------------------------------------+

En regardant juste les appels à ``run_plt`` et ``run_mplconfig`` :

Pour ``snakeviz`` (``cProfile``)

+--------+---------+---------+---------+---------+-----------------------------------------+
| ncalls | tottime | percall | cumtime | percall | filename:lineno(function)               |
+========+=========+=========+=========+=========+=========================================+
| 1      | 4.9e-05 | 4.9e-05 |	4.528  | 4.528	 | profile_mplconfig.py:123(run_mplconfig) |
+--------+---------+---------+---------+---------+-----------------------------------------+
| –      |         |         |         |         |                                         |
+--------+---------+---------+---------+---------+-----------------------------------------+
| 1      | 4.9e-05 | 4.9e-05 |	3.268  | 3.268	 | profile_mplconfig.py:136(run_plt)       |
+--------+---------+---------+---------+---------+-----------------------------------------+

Pour ``LoggerTimer``

+-------------------+-------------------+
| ``run_mplconfig`` | 4.050039291381836 |
+-------------------+-------------------+
| ``run_plt``       | 2.679107427597046 |
+-------------------+-------------------+

Conclusion
----------

Les fonctions avec chargement des données quantifié utilisent la méthode ``plt.plot(*plt_args)``, tandis que les
fonctions sans chargement des données appellent n fois la méthode ``plt.plot(x, y)``.

* Mémoire
    * Surcoût à l'affichage après la customisation des axes : **+7.9MiB**
    * Les titres ont peu d'impact : bizarrement en rajouter, fait gagner 0.3 MiB sur l'empreinte mémoire !
* Temps d'exécution
    * AVEC chargement des données dans les fonctions éxécutées : Léger surcoût en temps
        * selon cProfile (une mesure) : **+1.126**
        * selon LoggerTimer (une mesure) : **+1.371**
    * SANS chargement des données : Surcoût moindre
        * selon LoggerTimer (7 mesures, puis moyenne) : **+0.844s**.


.. image:: ../../images/Python_results_elapsed_time.png


En C++
======

Seul le temps d'exécution a été quantifié. Les chargements des données n'a pas été mesuré, il a été sorti du code
des fonctions d'affichage.

De plus, au lieu de tracer les courbes, puis de passer les axes en log, pour le mode *plt*, la fonction ``loglog`` a
été utilisé au lieu de ``plot``.

Le Surcoût en temps est d'environ **+0.882s**.

.. image:: ../../images/C++_results_elapsed_time.png


Conclusion
==========

Résumé pour les fonctions n'incluant pas le chargement des données, et faisant appel n fois à ``plt.plot(…)``.

+---------------+-------+--------+
|               | C++   | Python |
+---------------+-------+--------+
| Temps (sec)   | 0.882 | 0.844  |
+---------------+-------+--------+
| Mémoire (MiB) | —     | 6.2    |
+---------------+-------+--------+