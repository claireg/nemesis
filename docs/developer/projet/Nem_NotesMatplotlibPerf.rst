.. _notes_mpl_perf:

.. note::

    Si lenteurs lors de l'affichage d'un réseau de courbes depuis le C++, il faudra créer une `LineCollection`.
    Principe de base :

    * En C++ : `plot_collections(x=, y=[], labels=[])`
    * En python : appel d'une méthode de `mplconfig` (ou autre) qui crée la `LineCollection` (voir `run_perf.plot_collections`)

================================
Notes mesures de perf matplotlib
================================

Les corps des fonctions pour lesquelles on souhaite un temps d'exécution

* Ne doivent pas contenir

    * la lecture des données
    * la mise en forme tableau 1d vers tableau 2d (en c++)
    * l'appel à plt.plot ou plt.draw ou plt.pause

* Doivent contenir

    * l'appel à ``plt.clf()`` => clear figure (sinon recalcul plus compliqué des bornes des axes)

Affichage de réseau de courbes
==============================

En python
---------

À partir d'un ``np.ndarray`` à 2 dimensions (2538 éléments : 54, 47).

Les données sont issues de ``x, ly = nem_rd.curves_rx_data()``.

Temps sans affichage.

+--------------+-------------+---------------+---------------+---------------+
|              | loop        |  one call     | one call prep | collections   +
+==============+=============+===============+===============+===============+
| loglog       | 0.18 à 0.21 | 0.031 à 0.032 | 0.031 à 0.032 |               |
+--------------+-------------+---------------+---------------+---------------+
| plot + scale | 0.19 à 0.21 | 0.032 à 0.034 | 0.032 à 0.034 | 0.012 à 0.015 |
+--------------+-------------+---------------+---------------+---------------+

Code source ``loglog`` :

loop ::

    for y in ly_:
        ax_.loglog(x_, y)
    ax_.set_xlim(x_.min(), x_.max())



one call ::

    ax_.loglog(*plt_args_)
    ax_.set_xlim(x_.min(), x_.max())

one call prep ::

    plt_args = prepare_one(x_, ly_)
    ax_.loglog(*plt_args)
    ax_.set_xlim(x_.min(), x_.max())


Code source ``plot + scale`` :

loop ::

    for y in ly_:
        ax_.plot(x_, y)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")

one call ::

    ax_.plot(*plt_args_)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")

one call prep ::

    plt_args = prepare_one(x_, ly_)
    ax_.plot(*plt_args)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")

Code source de ``prepare_one`` ::

    args = []
    for y in ly_:
        args.append(x_)
        args.append(y)
    return args


collections ::

    # Les couleurs fonctionnent mal ...
    color_seg_base = [colors.to_rgba(c) for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]
    line_seg = collections.LineCollection([list(zip(x, y)) for y in ly_])
    line_seg.set_array(x)
    color_seg = [current_color for (current_color, i) in zip(itertools.cycle(color_seg_base), range(len(ly_)))]
    line_seg.set_color(color_seg)
    ax_.add_collection(line_seg)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")


Affichage de réseau de courbes : comparaison
============================================

Les données sont issues de ``x, ly = nem_rd.curves_rx_data()``.

À partir d'un ``np.ndarray`` à 2 dimensions (662958 éléments : 54, 12277).

Pas de tests plot + scale.

Python
------

+--------------+-------------+---------------+
|              | loop        | one call prep |
+==============+=============+===============+
| python       | 0.20 à 0.41 |  0.07 à 0.09  |
+--------------+-------------+---------------+
| c++          |  > 2000 ms  |  impossible*  |
+--------------+-------------+---------------+

Impossible en C++ :
* ``matplotlibcpp`` boucle pour faire les plots même si on passe les arguments en une seule fois.
* pourrait être possible mais cela aurait un coup mémoire important (création PyObject pour tous les tab à afficher).

Code Python ``loop`` ::

    def cpp_py_cmp_network_2_loglog_loop(x, ly):
        plt.clf()
        for y in ly:
            plt.loglog(x, y)
        plt.xlim(x.min(), x.max())

Code Python ``one_call_prep`` ::

    def cpp_py_cmp_network_2_loglog_one(x, ly):
        plt.clf()
        plt_args = prepare_one(x, ly)
        plt.loglog(*plt_args)
        plt.xlim(x.min(), x.max())

Code C++ ``loop`` ::

    int cpp_py_cmp_network_2_loglog_loop(const std::vector<std::vector<double>>& results, const std::vector<std::vector<double>>& split_z)
    {
        plt::clf();
        for(auto& subvector: split_z) {
            plt::loglog(results[1], subvector);
        }
        auto x_minmax = std::minmax_element(results[1].begin(), results[1].end());
        plt::xlim(*(x_minmax.first), *(x_minmax.second));
        return 0;
    }


À partir des vectors, ``matplotlibcpp`` crée des ``PyArray_SimpleNewFromData``.