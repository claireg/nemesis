# Spack

Installation sur le portable : `~/Devel/projects/spack`.

* (tutorial)[https://spack.readthedocs.io/en/latest/tutorial_basics.html#basics-tutorial]
* (doc)[https://spack.readthedocs.io/en/latest/]
* (doc lisible)[https://spack.io/]

## Installation


    git clone https://github.com/spack/spack
    git checkout releases/v0.12


## Activation

    . share/spack/setup-env.sh

## Les vues

Pratiques pour mettre tout dans un même répertoire, mais pas pratique pour nemesis (pas de possibilité de récupérer
les différentes vues créées, ou alors j'ai pas trouvé comment). De plus, il faudrait mettre nemesis dedans, du coup,
il y aurait une vue par version de Némesis.

## Les environnements

Plus souples que les vues. Il faut activer un environnemnt pour qu'il soit fonctionnel.


### Création

    spack env create  env_python_for_nemesis

Spack répond

==> Updating view at /home/guilbaudc/Devel/projects/spack/var/spack/environments/env_python_for_nemesis/.spack-env/view
==> Created environment 'env_python_for_nemesis' in /home/guilbaudc/Devel/projects/spack/var/spack/environments/env_python_for_nemesis


## Commandes

Liste des packages existants

    spack list 
    spack list py-*

Information sur les versions de packages dans la recette

    spack versions python
    
Informations lorsque plusieurs packages même nom - même version sont installés

    spack find -l -v -N llvm


## Recettes à faire

* snakeviz (optionnel)

## Environnement virtuel python

### maki (en interne)

* python 3.7.2
* yolk : 0.4.3
* numpy : 1.15.4
* scipy : 1.1.0
* matplotlib : 3.0.2
* recommonmark : 0.5.0
* sphinx : 1.8.3
* pylint : 1.9.2
* nose : 1.3.7
* pytest : 3.8.2
* psutil : 5.4.6
* snakeviz : 0.4.2
* memory_profiler : 0.52.0
* graphviz : 0.8.4
* PySide2 : 5.9.0a1

### babylon (portable)

* python 3.7.2
* yolk : 0.4.3
* numpy : 1.16.2
* scipy : 1.2.1
* matplotlib : 3.1.1
* recommonmark : 0.5.0
* sphinx : 2.1.2
* pylint : 2.3.1
* nose : 1.3.7
* pytest : 5.0.1
* psutil : 5.6.3
* snakeviz : 2.0.0
* memory_profiler : 0.55.0
* graphviz : 0.11.1
* PySide2 : 5.12.1


### startrek (portable)

* python 3.7.2
* yolk : 0.4.3
* numpy : 1.16.2
* scipy : 1.2.1
* matplotlib : 3.0.3
* recommonmark : 0.5.0
* sphinx : 1.8.5
* pylint : 2.3.1
* nose : 1.3.7
* pytest : 3.8.2
* psutil : 5.6.3
* snakeviz : 2.0.0
* memory_profiler : 0.55.0
* graphviz : 0.11.1
* PySide2 : 5.12.1

