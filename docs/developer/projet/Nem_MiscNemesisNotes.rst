.. _notes_nemesis_misc:

=================
Notes sur nemesis
=================

Ajustements de la boîte d'axes
==============================

**Valable en Python et en C++.**

``nemesis`` permet de gérer soi-même l'ajustement des boites d'axes d'une figure (via la fonction ``subplots_adjust``–
:py:func:`matplotlib:matplotlib.pyplot.subplots_adjust` ou :cpp:func:`matplotlibcpp::subplots_adjust`).

Cependant, si vous ne souhaitez pas vous en occuper, et que vous passer d'une boîte d'axes 3D à 2D, il faudra
appeler explicitement ``switch_axes`` (en Python :py:func:`nemesis.plotter.PyPlotCEA.switch_axes` ;
en C++ :cpp:func:`matplotlibcpp::switch_axes`) pour passer de la 3D à la 2D::

    nem_plt.PyPlotCEA.switch_axes('rectilinear')

    plt::switch_axes("rectilinear")

Ainsi, les textes affichés via la fonction :py:func:`nemesis.plotter.PyPlotCEA.set_titles` ne seront pas recouverts par
la boîte d'axes.

La position de la légende de réseaux est faite pour les valeurs positionnées par nemesis. Si vous les gérer à la main,
vous pourriez avoir une superposition de cette légende et des axes (titre d'un axe, labels d'un axe, ...).

Quand on change de mode de projection une boîte d'axes (via ``switch_axes``), l'ancienne boîte d'axes est supprimée
et une nouvelle dans le bon mode est créée. Les textes et autres artistes affichés liés à la figure ne sont pas
supprimés. Pour se faire, il faut appeler explicitement la méthode de nettoyage de la *figure* (
en Python :py:func:`matplotlib:matplotlib.pyplot.clf` ; en C++ : :cpp:func:`matplotlibcpp::clf`).

On peut envisager le développement d'une fonction sur le même principe que :py:func:`matplotlib:matplotlib.pyplot.tight_layout`
pour réorganiser les objets affichés pour qu'aucun ne se superpose. Pour l'instant, le besoin n'apparaît pas.

