# Patch d\'un espace de travail vers un autre


## Création de patches

## Sur des commits existants

Création d'autant de fichiers de patches qu'il y a de commits entre dev et la branche courante

    git format-patch dev

Création d'un seul patch

    git format-patch dev --stdout > all-changes.patch

Création de patch entre deux commits

    git diff commitid1 commitid2 > my.patch

Création d'un patch pour un seul commit

    git format-patch -1 commitid --stdout > name.patch


## Sur des fichiers à l'état staged ou en cache

    git diff [--cached] [--staged] [--binary] dev > mon_patch.patch


## Applications des patches :


`git apply` : provides the patch as *unstaged* changes in the current branch.

    git apply 0001-text.patch

Application des patches comme des commits

    git am [--signoff] [-k] 0001-text.patch

* `signoff` : pour conserver l'identité et le message contenu dans le patch
* `k` : pour conserver les flags (les zones entr [] dans le message du commit)


## Pour lire les informations d'un patch créé par git format-patch

### Lister le diffstat sur la sortie standard

    git apply --stat name.patch

### Vérification que le patch est bien applicable

    git apply --check name.patch
