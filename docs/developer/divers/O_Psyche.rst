.. _psyche:

======
Psyché
======

Structure de données Psyché
===========================

.. graphviz::

    digraph Pipeline {
        subgraph cluster0 {
            color=black;
            labelloc=t;
            label="Présentation et Mise en page";
            v -> p_1;
            v -> p_2;
            v [label="Viewer"];
            p_1 [label="Page"];
            p_2 [label="Page"];
            p_2 -> vide_1 [style=dotted];
            vide_1 [label="…", shape=plaintext];
            p_2 -> vide_2 [style=dotted];
            vide_2 [label="…", shape=plaintext];
            p_2 -> vide_3 [style=dotted];
            vide_3 [label="…", shape=plaintext];
            {rank=same; p_1 p_2}
            p_1 -> t;
            t [label="Text"];
            p_1 -> sc;
            sc [label="Scene"];
            p_1 -> l;
            l [label="legend"];
            sc -> c;
            c [label="collection"];
            c -> g_1;
            g_1 [label="Graphic"];
            c -> g_2;
            g_2 [label="Graphic"];
            c -> g_3;
            g_3 [label="Graphic"];
            {rank=same; g_1 g_2 g_3}
        }

        subgraph cluster1 {
            color=black;
            labelloc=b;
            label="Données physiques";
            f_1 -> v_1 [label="Physical"];
            f_1 [label="Function"];
            f_2 [label="Function"];
            f_2 -> vide_4 [style=dotted];
            vide_4 [label="…", shape=plaintext];
            f_2 -> vide_5 [style=dotted];
            vide_5 [label="…", shape=plaintext];
            v_1 [label="Value"];
            v_1 -> d_1 [label="Datas"];
            d_1 [label="Data"];
            v_1 -> d_2 [label="Errors"];
            d_2 [label="Data"];
            f_1 -> s [label="Geometrical"];
            s [label="Support"];
            s -> v_2 [label="Coordinate"];
            s -> v_3
            v_2 [label="Value"];
            v_3 [label="Value"];
            v_2 -> d_3 [label="X"];
            v_3 -> d_4 [label="Y"];
            d_3 [label="Data"];
            d_4 [label="Data"];
            {rank=same; v_1 v_2 v_3}
            {rank=same; d_1 d_2 d_3 d_4}
        }

        g_1 -> f_1;
        g_1 -> f_2;
        label="Chaîne de visualisation";
    }


Structure pour un ``graphic``
-----------------------------

**Un graphique = une fonction**

Une fonction (``Fonction``) a

* des valeurs physiques (``Value``) qui comportent

    * un titre
    * des unités
    * ...

* des données géométriques (``Support``) composées

    * de coordonnées (``Value``)


Les valeurs (``Value``) se composent

* de données (``Data``)


Structure d'un afficheur
------------------------

Un ``Viewer`` contient

* une ou plusieurs pages ``Page`` qui se composent

    * d'un ``Text``
    * d'une ``Legend``
    * d'une ``Scene`` qui contient

        * une ``Collection`` composée

            * d'un ou plusieurs ``Graphic``


Spécialisation des termes génériques
====================================

``Data``
--------

C'est une matrice de valeurs. La matrice est de dimension *L* et contient des vecteurs de dimension *C* (*L* : dimension logique, *C* : nombre de composants).

* ``ListFloatData``
* ``ArrayFloatData``
* ``ListIntegerData``
* ``ArrayIntegerData``
* ``StringData``


``Value``
---------

Données physiques ou coordonnées.

* ``FloatValue``
* ``IntegerValue``
* ``StringValue``


``Support``
-----------

Contient un champs de coordinateValues (liste de `Value -> Data`)

* ``ScatteredSupport``
* ``StructuredSupport``
* ``UnstructuredSupport``


``Representation``
------------------

* ``StandardCurve``
* ``SetOfCurve``
* ``FlatMap``
* ``RaiseMap``
* ``PointField``
* ``VectorField``
* ``FlatHistogram``
* ``DepthHistogram``
* ``Image``
* ...

