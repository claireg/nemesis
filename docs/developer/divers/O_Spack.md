# Spack


Dans mon espace de travail : `~/Devel/Products/Spack/spack` (clone de `~maison/BARES`).

## Installation et configuration

```bash
    cd ~/Devel/Products/Spack
    ~maison/SPACK/scripts/get_spack_cea [-d spack3]
```

## Activation de l'environnement

```bash
    cd ~/Devel/Products/Spack/spack3
    . ./share/spack/setup-env.sh
```

## Les compilateurs

Liste des compilateurs installés

```bash
    spack compilers
```

Ajout d'un nouveau compilateur déjà installé dont on connait le chemin

```bash
    spack compiler find /path/vers/gcc49/gcc/6.4/bin
```

Suppression d'un compilateur

```bash
    spack compiler remove gcc@7.2.0
```

Si la suppression ne fonctionne pas, éditer le fichier

```bash
    gvim .spack/linux/compilers.yaml
    # supprimer les compilateurs dont on veut pas
```


## Informations sur les packages

Sur la liste des packages installables

```bash
    spack list
```

Sur un package

```bash
    spack info python
```

## Informations sur les dépendances

En général utilisé avant l'installation d'un package

Sur la version d'un package

```bash
    spack spec python@3.7.0
```

Installation pour une dépendance donnée

```bash
    # -I donne les dépendances déjà installées
    spack spec -I py-matplotlib@2.2.2 ^python@3.7.0
```

## Divers

Trouver un package installés dans son espace `spack`

```bash
    spack find
```

Sur les compilateurs installés

```bash
    spack compilers
```

## Installations

**Dans la mesure où mon environnement spack ne contient plus qu'un seul compilateur, je n'ai plus besoin de le spécifier.**

Les options de `spack spec` sont valables pour `spack install`.

Installation classique

```bash
    # -n dit «pas de définition de checksum dans les recettes»
    spack install [-n] the_package
```

Installation pour un compilateur donné, en debug

```bash
    spack install [%gcc@7.3_abi0] [+debug] the_package
```

Installation pour une dépendance donnée

```bash    
    spack install py-matplotlib@2.2.2 ^python@3.7.0
```
### Téléchargement des tarballs

La commande suivante créee une arborescence avec les tarballs des packages demandés

    spack mirror create [-D] -d /path/vers/repertoire/au/choix --file specs.txt
    # ou
    spack mirror create -D -d /path/vers/repertoire/au/choix py-numpy py-matplotlib py-sphinx ...
    # -D pour récupérer les dépendances

Pour ne prendre que les nouveautés :
    
    tar cf my_mirror.tgz --after-date="06/30/20 13:57:35" my_mirror

### Mise à jour des mirroirs

* Récupérer le chemin du mirroir à mettre à jour (via `spack mirror list`).
* Exécuter la command suivante (sans l'option `dry-run` et sûrement sans le groupe sur portable) :

    rsync --dry-run -i --chmod=a+rX,g+ws --groupmap=*:nec --archive --update --omit-dir-times --omit-link-times --checksum --progress my_mirror /mirror/to/update/offline_mirror

### Divers

```bash
    spack find -v glib
    spack find -L
    spack spec [-I] _hash_
    spack location -i _hash_
    # avec les dépendances installées
    spack spec -I sabre_test_b
```

## Initialisation d'un package spack


Équivalent à un `module load …`.

```bash
    spack load the_package
    # ou
    spack load /le_sha256_version_courte_ou_longue
```

Ensuite, on peut utiliser la commande `module`

```bash
    module avail
    module show
```

## Spack Organizer

### Création environnement python pour nemesis

On utilise maintenant un fichier d'environnement `spack.yaml`.
C'est un environnement spack avec une vue (remplace le script `create.env.py3.bash`
du repo `spack_env_python`.

Le fichier se trouve dans `<workspack_spacko>/deploy/<os>/nem_python_env`.

Pour l'installer :
    cd <workspace_spacko>/deploy/<os>/nem_python_env
    spack env deactivate
    spack env activate .
    # génère un fichier `spack.lock`
    spack concretize [-f]
    spack install --verbose

Si problème de concrétisation (ne voit pas tous les packages à installer) :
    rm spack.lock 
    spack concretize

Si modification de spack.yaml
    spack concretize -f

### Tips

* La commande `spack env list` ne voit pas les environnements qui ne sont pas 
lister dans son répertoire environments.
* Pour connaître les environnements dans `spack organizer` : 
    
    cd <workspace_spacko>
    tree deploy/

### Upstreams

Spack organizer utilise des upstreams pour lier les espaces d'installation.
Une *upstream* est reconnaissable par un `[^...` quand une spec est donnée.

## Spack dans `nemesis`

### Principe de la recette

* nemesiscppconfig.h.in :
    * `NEMESIS_SITE_PACKAGES` est renseigné lors de l'installation
    * L'environnement python est fixé lors de l'installation. En mode développement on peut le fixer soit même
    (voir les fichiers `configure` des exemples et tests C++).
* `SITE_PACKAGES` pointe sur (invariant suivant le mode d'utilisation : mode développement ou déploiement)
    * une installation python classique
    * un environnement spack activé
    * un environnement virtuel python
* Pour le mode *developpement*, il suffit d'avoir un python complet 
    * `export PATH=path/vers/mon/python/complet`
    * activation d'un environnement spack
    * activation d'un environnement virtuel python

### Environnemnt Python

Pour fonctionner `nemesis` se lie à un environnement Python contenant un ensemble de packages prédéfinis.

L'environnement python peut être :
* un environnement spack contenant un environnemnt python : activation via `spack env activate ...`
* une vue spack contenant un environnement python : il faut alors faire un script activate (eq. à 
celui créé par `python -m venv ...`)
* un environnement virtuel python (`python -m venv ...`)
* une installation python complète

**Pour le moment**, l'installation de nemesis ne demande rien, c'est à l'utilisateur de choisir / activer
un environnement python.

### Installation `nemesis` i.e `py-nemesis`

1. Création de l'archive

        git archive --format=tar.gz -v -o ./py-nemesis-0.1.2.tar.gz --prefix=py-nemesis-0.1.2/ HEAD

2. Copie de l'archive dans le mirror spack qui va bien
3. Installation via Spack

        spack install [-v -v -v] py-nemesis

*Nota Bene* pour tester à la main l'installation de `nemesis` (*i.e.* le `setup.py`)

    python setup.py install
    python setup.py install_headers

