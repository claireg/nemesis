.. _catch:

Catch
=====

.. toctree::
    :maxdepth: 2

    ./Catch2/README
    ./Catch2/docs/Readme
    ./Catch2/docs/assertions
    ./Catch2/docs/benchmarks.md
    ./Catch2/docs/ci-and-misc.md
    ./Catch2/docs/cmake-integration.md
    ./Catch2/docs/command-line
    ./Catch2/docs/commercial-users
    ./Catch2/docs/configuration
    ./Catch2/docs/contributing
    ./Catch2/docs/deprecations.md
    ./Catch2/docs/event-listeners
    ./Catch2/docs/generators.md
    ./Catch2/docs/limitations
    ./Catch2/docs/list-of-examples
    ./Catch2/docs/logging
    ./Catch2/docs/matchers
    ./Catch2/docs/opensource-users
    ./Catch2/docs/other-macros.md
    ./Catch2/docs/own-main
    ./Catch2/docs/release-notes
    ./Catch2/docs/release-process
    ./Catch2/docs/reporters
    ./Catch2/docs/slow-compiles
    ./Catch2/docs/test-cases-and-sections
    ./Catch2/docs/test-fixtures
    ./Catch2/docs/tostring
    ./Catch2/docs/tutorial
    ./Catch2/docs/why-catch
