.. _nem_sphinx:

Utilisation Sphinx
==================

Génération Makefile et conf.py de docs
--------------------------------------

À ne faire que la première, car après ``conf.py`` est adapté au projet::

    . /path/vers/gcc/python/3.6.4-ABI0/Nemesis_0.0/bin/activate
    sphinx-quickstart
    # répondre aux questions (voir ``sqs-questions.txt``)

.. literalinclude:: sqs-questions.txt


Génération de la documentation
------------------------------

.. warning::

    Si latex se plaint de l'absence du package ``anyfontsize.sty``, en cas d'utilisation de la directive ::

        .. math::

    #. Créer un réperoire ``texmf/tex/latex/perso``::

        cd $HOME
        mkdir mkdir -p texmf/tex/latex/perso

    #. Positionner la variable d'environnement ``TEXINPUTS`` ::

        export TEXINPUTS=$HOME/texmf//:

    #. Copier le fichier ``anyfontsize.sty`` dans ce répertoire ::

        cd <workdir>/docs/_static
        cp anyfontsize.sty ~/texmf/tex/latex/perso

.. warning::

    Si la génération de la documentation se plante, vérifier qu'on a pas mis des caractères exotiques dans un fichier ``markdown``.
    *E.g.* les 3 points de suspension sur un seul caractère : … – Ça fonctionne en ``reStructuredText`` !

La documentation se génère en une seule commande ::

    make html

Si réorganisation de la documentation ::

    make clean


Génération des présentations
----------------------------

Marche à suivre ::

    cd <workdir>/docs/presentations
    cd <ma_presentation>
    make slides


Génération de la documentation packages Python
----------------------------------------------

Pour tous les packages, vérifier qu'il n'y a pas de référence ``intersphinx`` pointant vers des paths en *https*.
Il convient de remplacer ces chemins comme suit::

    intersphinx_mapping = {
        'python': ('/path/vers/share/python/Python-3.7.0', None),
        'numpy': ('/path/vers/share/python/numpy-1.14.5', None),
        'matplotlib': ('/path/vers/share/python/matplotlib-2.2.2', None),
    }

matplotlib
~~~~~~~~~~

Si la documentation de matplotlib se plante avec des messages types::

    Inline strong start-string without end-string.

Cela provient des références à ``**kwargs`` dans des docstrings. Pour l'instant (15 janvier 2019), le seul
contournement que j'ai trouvé consiste à supprimer le ``**`` !

.. warning::

    Le plus simple est d'enlever l'option ``-W`` à la commande ``sphinx-build`` dans le Makefile

Tips sphinx
-----------

Dans un fichier .rst

* Référence à une page matplotlib (ici la liste des colormaps)

.. code::

        :doc:`colormap reference <matplotlib:gallery/color/colormap_reference>`


* Les «remontrances» (_admonition_). Il existe (les styles sont entre-parenthèses et à utiliser dans le modifieur ``:class:``:
        * ``attention`` (``admonition attention``),
        * ``caution`` (``admonition caution``),
        * ``danger`` (``admonition danger``),
        * ``error`` (``admonition error``),
        * ``hint`` (``admonition hint``),
        * ``important`` (``admonition important``),
        * ``note`` (``admonition note``),
        * ``tip`` (``admonition tip``),
        * ``warning`` (``admonition warning``),
        * ``admonition`` – générique, il faut mettre un titre – (``admonition-titre admonition``)

* Pour personnaliser son admonition (jamais testé) ::

    .. admonition:: My title goes here
        :class: myOwnStyle

        This is admonition text

    # Puis dans le répertoire _static, dans conf.py, ajouter ::

        def setup(app):
            app.add_stylesheet('custom.css')

    # Puis remplir son css


* Si une référence croisée n'est pas trouvé (alors qu'on est sûr que la page html dans le projet existe)

    * Activation de l'environnement virtuel qui contient ``Sphinx``, puis exécution de cette commande::

        python -m sphinx.ext.intersphinx /path/vers/objects.inv > /tmp/MyProducts/objects.txt


    * Chercher dans objects.txt, la référence que l'on cherche, puis utiliser la catégorie de cette référence
    comme point d'entrée de la référence: Exemple pour les méthodes `set_zlim`::

        :py:meth:`mpl_toolkits.mplot3d.axes3d.Axes3D.set_zlim`
