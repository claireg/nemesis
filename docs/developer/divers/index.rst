.. _dev_misc:

Divers
======

.. toctree::
    :maxdepth: 2

    O_Sphinx
    O_Psyche
    O_Matplotlib-cpp-master
    O_Matplotlib-cpp-contrib
    O_Catch2-index
    O_Spack
    O_CMake_Tips

