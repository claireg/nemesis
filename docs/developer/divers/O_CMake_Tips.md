# Notes sur Modern CMake

![camke_tips](./O_CMake_Tips.png "tips CMake")

page 20 de modern-cmake.pdf

**Targets are your friend**.

* Focus on using targets everywhere, and keywords everywhere, and you'll be fine
* Targets can have include directories, linked libraries (or linked targets), compile options, 
compile definitions, compile features (see theC++11 chapter), and more

## Bonnes pratiques

* Do not use global functions: link_directories, include_libraries, ...
* Don't add unneeded PUBLIC requirements
* Don't GLOB files
* Link to built files directly: Always link to targets if available.
* Never skip PUBLIC/PRIVATE when linking:

* Treat CMake as code
* Think in targets
* Make an (IMPORTED) INTERFACE target for anything that should stay together and link to that.
* Export your interface: You should be able to run from build or install.
* Write a Config.cmake file
* Make ALIAS targets to keep usage consistent: Using add_subdirectory and find_package should provide the same targets and namespaces.
* Combine common functionality into clearly documented functions or macros: Functions are better usually.
* Use lowercase function names: Uppercase is for variables.
* Use cmake_policy and/or range of versions: Policies change for a reason

## Les variables en «cache»

Quand on dit qu'une variable est en cache, ça veut dire qu'on peut y accéder depuis la ligne de commande.

    set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING "Build configuration type" FORCE)

**Ça ne remplace pas les valeurs existantes** sauf si on force la valeur et que la variable est ajoutée comme *avancée* :

    set(MY_CACHE_VARIABLE "VALUE" CACHE STRING"" FORCE)
    mark_as_advanced(MY_CACHE_VARIABLE)

Pour une variable booléenne :
    
    option(MY_OPTION "This is settable from the command line"OFF)

Pour connaître la list des variables modifiables (depuis un répertoire de build) :

    cmake -L -S path_vers_source

## Les fichiers de configuration

Beaucoup de modules / packages CMake ont un fichier de configuration dans le répertoire d'installation de `CMake`.
Sur le portable : `/home/guilbaudc/Devel/products/python_and_co/3.7.3/doris-gcc830/share/cmake-3.15/Modules`

## Personnalisation des *targets*

### Généralités

* Si notre target est de type `IMPORTED`, alors lors de la configuration des propriétés, il faut utiliser `INTERFACE`. 
Si il n'y'avait rien, on pourrait mettre `PUBLIC` ou `INTERFACE`.

### `target_compile_features`

Il existe une liste et `cxx_std_17` et `-fconcepts` ne sont pas dedans
* `fconcepts` : notion C++20 pour pouvoir utiliser `auto` comme type pour un paramètre dans la déclaration d'une fonction / méthode 

## Les variables d'environnement

Il est préférable de ne pas s'en servir.

    set(ENV{variable_name} value) 
    get $ENV{variable_name}
 
## Les propriétés (d'une cible)

CMake stocke des informations dans les variables *cache* et dans les *propriétés*.
C'est comme une variable mais est attachée à un autre item (répertoire ou cible).
 
Une propriété globale peut être une variable globale utile non mise en cache.
Beaucoup des propriétés des *cibles* sont initialisées à partir d'une variable commençant par `CMAKE_`.
Par exemple, si `CMAKE_CXX_STANDARD` est initialisée, cela veut dir que toutes les nouvelles *cibles* auront `CXX_STANDARD` 
initialisé à la valeur de cette dernière lors de leurs créations.

Forme générique :

    set_property(TARGET TargetName PROPERTY 
        CXX_STANDARD 11)

Forme simplifiée :

    set_target_properties(TargetName PROPERTIES 
        CXX_STANDARD 11)

Récupération :

    get_property(ResultVariable TARGET TargetName PROPERTY CXX_STANDARD)

Pour connaître la liste des propriétés disponibles : lire dans la doc de cmake `cmake-properties`.

## Control-flow (le `if`)

Mettre des quotes pour éviter qu'une variable soit étendue (*expand*) :

    if("${variable}")
        # True if variable is not false-like
    else()
        # Note that undefined variables would be `""` thus false
    endif()

Il existe un certain nombres de mots clés pour spécifier un test (`NOT`, `TARGET`, ...).

Résolu au moment de la phase «configuration».

### Tester la valeur d'une string

Ici `CMAKE_BUILD_TYPE` peut valoir `Debug`, `DEbug`, `DEBUG` ...

    string(TOLOWER "${CMAKE_BUILD_TYPE}" _type)
    if(_type STREQUAL debug)
        add_compile_definitions(${TU_EXE} PRIVATE DEBUG)
    endif()

## Generator Expressions

Logique pour les phases de «construction», et d'«installation». Ces *statements* sont évalués dans les propriétés des *cibles*.

Il en existe différentes formes :

* *informational expressions* : `$<KEYWORD>`. Ça définit une information pertinente pour la configuration courante.
* L'autre forme est `$<KEYWORD:value>`, où `KEYWORD` est un mot clé qui contrôle l'évaluation, et `value` est l'item à 
évaluer (une *informational expression* est aussi permis ici). Si `KEYWORD` est une *generator expression* ou une
variable qui vaut 0 ou 1, alors `value` est substituée seulement si 1.

En résumé
* L'expression `$<CONFIG:Debug>` vaut 1 si la config est Debug
* `$<1:DEBUG>` : ajoute **toujours** `-DDEBUG` à la ligne de compilation

Certaines expressions autorisent des valeurs multiples, séparés par des virgules.

Pour mettre un flag seulement pour la configuration Debug :

    target_compile_options(MyTarget PRIVATE "$<$<CONFIG:Debug>:--my-flag>")
    
Autres utilisations des *generator expressions*
* Limité un item à un certain language seulement, tel que `CXX` pour éviter d'être mélangé avec qqch comme `CUDA`, ou
envelopper pour qu'il soit différent selon la langue cible.
* Accéder aux propriétés dépendant de la configuration, comme le chemin d'un fichier.
* Donner un différent chemin pour le *build* ou l'*install*.

    
## Macros et Fonctions

Les variables définies à l'intérieur d'une macro sont «perdues» une fois sortie de la macro (elles ne sont plus accessibles).
Les variables définies à l'intérieur d'une fonction peuvent être récupéréers dans le scope au dessus si le mot clé 
`PARENT_SCOPE` est indiquée lors de la définition.

    function(SIMPLE REQUIRED_ARG)
        message(STATUS "Simple arguments: ${REQUIRED_ARG}, followed by ${ARGV}")
        set(${REQUIRED_ARG}"From SIMPLE" PARENT_SCOPE)
    endfunction()
    
    simple(This)
    message("Output: ${This}")


## Arguments

CMake a un système de variables nommées. On peut l'utiliser via la fonction `cmake_parse_arguments

    function(COMPLEX)    
        cmake_parse_arguments(        
            COMPLEX_PREFIX
            "SINGLE;ANOTHER"
            "ONE_VALUE;ALSO_ONE_VALUE"
            "MULTI_VALUES"
            ${ARGN}    
    )
    endfunction()

    complex(SINGLE ONE_VALUE value MULTI_VALUES some other values)

Après l'appel de la fonction, il y aura : 

    COMPLEX_PREFIX_SINGLE = TRUE
    COMPLEX_PREFIX_ANOTHER = FALSE
    COMPLEX_PREFIX_ONE_VALUE = "value"
    COMPLEX_PREFIX_ALSO_ONE_VALUE = <UNDEFINED>
    COMPLEX_PREFIX_MULTI_VALUES = "some;other;values"

Les arguments restant sont dans `OMPLEX_PREFIX_UNPARSED_ARGUMENTS`.


## Communication avec son code

### Configure un fichier

Génération d'un fichier à partir de variables `CMake` : commande `configure_file`.

Remplacement des variables définies dans CMAKE par leurs valeurs dans un fichier *.in :

CMakeLists.txt :

    set(TOTO "toto")

cfg.h.in 

    #define TATA "@TOTO@"

Fichier cfg.h
    


### Lecture depuis un fichier

Inverse de la commande `configure_file`.

    # Assuming the canonical version is listed in a single line
    # This would be in several parts if picking up from MAJOR, MINOR, etc.
    set(VERSION_REGEX "#define MY_VERSION[ \t]+\"(.+)\"")
    
    # Read in the line containing the version
    file(STRINGS "${CMAKE_CURRENT_SOURCE_DIR}/include/My/Version.hpp"    
        VERSION_STRING REGEX ${VERSION_REGEX})
    
    # Pick out just the version
    string(REGEX REPLACE ${VERSION_REGEX}"\\1" 
        VERSION_STRING "${VERSION_STRING}")
    
    # Automatically getting PROJECT_VERSION_MAJOR, My_VERSION_MAJOR, etc.
    project(My LANGUAGES CXX VERSION ${VERSION_STRING})


## Structuration du projet

Structure typique :

    - project  
        - .gitignore  
        - README.md  
        - LICENCE.md  
        - CMakeLists.txt  
        - cmake    
            - FindSomeLib.cmake    
            - something_else.cmake  
        - include    
            - project      
            - lib.hpp  
        - src    
            - CMakeLists.txt    
            - lib.cpp  
        - apps    
            - CMakeLists.txt    
            - app.cpp  
        - tests    
            - CMakeLists.txt    
            - testlib.cpp  
        - docs    
            - CMakeLists.txt  
        - extern    
            - googletest  
        - scripts    
            - helper.py




## Vocabulaire

* taking over : prendre le comtrôle
* make-shift : improvisé
* biased : tendancieux, déformé
