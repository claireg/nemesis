.. _nemesis_glossary:

===================================
Glossaire des termes de ``nemesis``
===================================

.. glossary::

    scatter
        littéralement «dispersion». Permet l'affichage de nuages de points

        .. image:: /_static/scatter.png
            :scale: 50

    stem
        littéralement «tige». Espèce de rateaux : Une ligne horizontale, et de part et d'autres de cette ligne, des
        lignes verticales.

        .. image:: /_static/stem.png
            :scale: 50

    contourf
        carte 3D Plan colorée (dans la terminologie CEA)

    contour
        isolignes

    cmap
        *colormap*, *i.e.* palette de couleurs. Voir
        :doc:`colormap reference <matplotlib:gallery/color/colormap_reference>` pour avoir la liste.

    figure
        Fenêtre graphique (ou image) dans laquelle ``matplotlib`` va dessiner. Elle est composée :

        * d'un titre
        * des légendes,
        * d'un ou plusieurs systèmes d'axes
        * de lignes, rectangles, ... représentant les données (courbes, histogrammes, ...)
        * d'une grille
        * ...

        .. image:: /_static/sphx_glr_anatomy_001.png
            :scale: 50

    Axes
        Boîte d'axes au sens ``matplotlib``

    backend
        Ensemble de primitives pour dessiner, afficher les données. Dans les exemples C++ de ``nemesis``,
        c'est le *backend* ``Qt5Agg`` qui est utilisé.

    marqueurs
        La liste est là :py:mod:`matplotlib:matplotlib.markers`.

    projection
        Projection utilisée pour afficher les données. Détermine le type de la boîte d'axes associée.
        Les projections possibles (avec la boîte d'eaxes correspondantes) :

        * "rectilinear" (défaut)
            .. image:: /_static/proj_rectilinear.png
                :scale: 50
        * "polar"
            .. image:: /_static/proj_polar.png
                :scale: 50
        * "3d"
            N'apparaît que si le module ``mplot3d`` a été importé (en général via ``from mpl_toolkits.mplot3d import Axes3D``)

            .. image:: /_static/proj_3d.png
                :scale: 50
        * "aitoff"
            .. image:: /_static/proj_aitoff.png
                :scale: 50
        * "hammer"
            .. image:: /_static/proj_hammer.png
                :scale: 50
        * "mollweide"
            .. image:: /_static/proj_mollweide.png
                :scale: 50
        * "lambert"
            .. image:: /_static/proj_lambert.png
                :scale: 50
