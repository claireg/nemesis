=====
To Do
=====

.. todo::

    * Calendrier (par ordre de priorité)
        * Appliquer les patches (travail réalisé durant le confinement) en interne
        * Appeler du C++ depuis le fortran ...
            * Faire une api C, puis Fortan de `matplotlibcpp_cea` et `matplotlibcpp_opensource`
        * Affichage de maillages non-structurés (2D, sans fourniture de la topologie (juste un nuage de points))
            * il y a déjà des choses pour la 2D dans ``nemesis`` (il faudrait des données pour finaliser)
            * regarder ce qu'on peut faire pour la 3D
        * Refactoring code Python ``pyplotcea.py``
            * Revoir paramètres ``networks_legend`` pour se rapprocher de ``pyplot.legend``.
        * Visu intéressantes
            * `errorbar <https://matplotlib.org/gallery/statistics/errorbar_limits.html#sphx-glr-gallery-statistics-errorbar-limits-py>`
            * `spectogrogrammes <https://matplotlib.org/gallery/images_contours_and_fields/specgram_demo.html#sphx-glr-gallery-images-contours-and-fields-specgram-demo-py>`
            * `box plot ou violin plot <https://matplotlib.org/gallery/statistics/boxplot_vs_violin.html#sphx-glr-gallery-statistics-boxplot-vs-violin-py>`
                * `papier box plot <http://vita.had.co.nz/papers/boxplots.pdf>`
            * `déviations standards <https://matplotlib.org/gallery/statistics/confidence_ellipse.html#sphx-glr-gallery-statistics-confidence-ellipse-py>`
            * `histogrammes et nuages de points <https://matplotlib.org/gallery/lines_bars_and_markers/scatter_hist.html#sphx-glr-gallery-lines-bars-and-markers-scatter-hist-py>`
            * `corrélations <https://matplotlib.org/gallery/lines_bars_and_markers/xcorr_acorr_demo.html#sphx-glr-gallery-lines-bars-and-markers-xcorr-acorr-demo-py>`
        * Utiles ?
            * un éditeur de formules mathématiques (ou en tout cas de quoi pouvoir en passer facilement)
        * Logs
            * Mettre les fichiers de logs dans un home utilisateur ... comment faire quand la config est dans un ``json`` ?
            * logging C++ (cf doc Genie Logiciel, rubrique «Logs du projet»)
        * Pour Gaia et Chrono
            * Problème coquille plus interne que d'autres ?
                * tester avec zorder ? : **change rien**
                * utiliser des ombres (voir exm python ``shadows.py``) ? : **change rien**
                * voir fichier ``coquilles*.psy`` (existe des données similaires sans passer par ``Psyché``)
                * voir si un export en SVG, et un affichage dans ``inkscape`` résoud le problème : **données 2D dans le SVG ...**
                * ``plotly`` devrait faire l'affaire : **oui, mais rendu dans une fenêtre de navigateur**
                    * test simple fait dans ``examples/python/developer/render_3d_sphere_with_plotly.py`` (utilisé l'env virtuel ``cersei``)
        * **@LATER** Faut-il remplacer tous les PyList_New et PyTuple_New par des Py_BuildValue comme le préconise la doc (python 2 ou python 3) ?
                * Si oui : permettrait de résoudre les pbms de *leaks* d'objets C Python
                * Si non : on reste cohérent avec la master de matplotlibcpp
                * Informations sur le sujet :doc:`c-api intro <python:c-api/intro>`, rubrique *Reference Count Details* :
                    * Peu de fct volent des références. Deux notables : ``PyList_SetItem``, ``PyTuple_SetItem``. Attention, seule la référence sur l'item est volé, la liste ou le tuple ne le sont pas.
                * À noter que ``Py_BuildValue`` est plus lent qu'un appel à ``PyTuple_New`` et ``PyList_New`` et insertion des items, mais c'est beaucoup plus lisible.
        * ``nemesis.readdata`` : Psyche : Faire une fonction générique de lecture
            * Revoir les maps pour faire comme pour ``nemesis.readdata.psy_hedgehog`` (``psy_utilities.psy_map_data``)
            * Revoir les autres méthodes d'extraction de données pour les rendre générique (pour les courbes, réseaux, voir si ``ReadPsyFile.get_curves_from_psy_file`` fait le boulot
            * NE PAS OUBLIER LES TESTS
        * Architecture / génie logiciel
            * **Existe un fichier de suppression des warnings Python pour valgrind dans les sources Python** (copié dans ``/Devel/products/python_and_co/3.7.4`` sur le portable)
            * Faire un environnement python en debug (voir ce qui a été fait sur le portable) pour comptage de référence
                -> PySide2 commo PyQt5 ne fonctionne pas avec un python en debug 3.7.4 ou 3.8.0 (`undefined symbol: PyModule_Create2`)
                -> Utilisation du backend `TkAgg`
            * Refactoring matplotlibcpp suite au découpage
                * voir si on peut créer une classe qui hérite de _interpreter et qui comprend tous les appels CEA
                    ATTENTION le ``get`` doit retourner ``_interpreter``
        * Iso-lignes : si besoins
            * Ne stocker qu'une seule fois les données pour les iso et les maps (ce sont les mêmes données)
            * **EST-IL BESOIN DE STOCKER LES DONNÉES DES CARTES SI ON NE PEUT PAS LES MODIFIER PAR L'IHM MATPLOTLIB ?**
                * Les premiers tests dans `eastworld` montre que c'est inutile de stocker les données des maps (sauf si iso+map).
        * Axes3D : ``raised_tricontourf``
            * dans ``nemesis``
            * dans ``Gaia``
        * Profiling et Performances
            * Si lenteurs à l'affichage de n courbes, utiliser des ``LinesCollection`` (voir ``run_perf.py:plot_collections``).
            * le gain est important (en python pur : 0.135 — plot(x,y, x,y, ...) vs 0.014
            * Si lenteurs affichages avec beaucoup de courbes (et lors des interactions) : régler le nombre de chunks des courbes.
        * Supprimer les warnings dûs au changement de version de matplotlib

    * Peu important
        * Tests
            * Écrire des tests pour les scripts de déploiement et configuration
            * Mettre tous les tests dans gitlab CI pour l'intégration continue (le jour où on en aura un ...)
            * ``psydata`` : créer un objet PsyDict pour récupérer les données, avec la méthode ``__eq__`` de ``testing``


=====
Notes
=====

.. note::

    * **PYCHARM** PyCharm 2019.1.1 plante lors de la prévisualisation d'un fichier markdown qui contient une image
        * Pour contourner : aller dans le ``workspace/.idea``, et éditer workspace.xml, chercher l'entrée pour le fichier markdown et
        remplacer ``SPLIT`` par ``FIRST`` dans ``<state split-layout="SPLIT">``
        * Si ça ne fonctionne toujours pas enlever les ``<file ...>`` correspondant à des fichiers markdown ou
        les ``<entry ...>``.
    * **Tests C++** : utilisation du backend ``Qt5Agg``.
        C'est celui le plus utilisé dans nemesis (seul ou couplé à un autre logiciel). En backend ``Agg``,
        la méthode ``show`` ne peut être utilisée (juste un warning à l'exécution), mais certaines méthodes
        (``[named_]semilog[x|y]``, ``loglog``, ``contourf`` finissent en erreurs.
        Le même code en python pur ne plante pas. Est-ce la surcouche ``Catch`` qui pose soucis ?
    * **NUMPY** Pour que l'import de numpy dans un logiciel CEA fonctionne, les sources de ``numpy.core.getlimits.py``
        ont été modifiées pour le calcul de ``_huge_f128``. Le système d'exception mis en place par ``numpy`` pour ignorer
        les erreurs lors du runtime ne fonctionne pas, si le logiciel inclut ``matplotlibcpp.hpp`` dans un lib statique,
        elle-même inclut dans un *exe*. On sait que ça fonctionne si ``matplotlibcpp.hpp`` est inclut directement dans
        un exécutable ou si il est inclut dans des sources qui forme un lib dynamique.
        Modification ligne 166::

            # CEA : modif pour que l'import fonctionne dans une lib statique en I4R4 ou I4R8
            #_tiny_f128 = exp2(_ld(-16382))
            _tiny_f128 = exp2(_ld(-16372))

    * Génération de la documentation depuis ``pycharm``
        * Avec un ``extern tools`` (menu ``Tools``)
            Ce tool est à créer via le menu ``Settings/Tools/External Tools``. Il se positionne dans le répertoire
            ``docs``, puis exécute ``make html``.
        * Avec une config ``sphinx`` (nommée ``sphinx nem``)
            Le fichier ``<pycharm_install>/helpers/rest_runners/sphinx_runner.py`` doit être
            modifié pour ne pas prendre en compte l'exe python. La ligne correcte est::

                cmdline.main(sys.argv[1:])

    * **[Optionnel]**
            * Installation de pycharm dans ``/tmp``, attention à la modification à faire dans ``sphinx_runner.py``.

    * Pourquoi la première ligne de NemesisDevice.cpp dans Gaia est ``_USE_MATH_DEFINES`` : pour pouvoir avoir les constantes mathématiques de ``<cstdint>`` type ``M_PI``.


====================================
Tags utilisés dans le code ou la doc
====================================

Il ne s'agit pas de tags ``git`` mais de mots écrits en majuscules commençant par **@**.

* ``@TODO`` : doit être fait. Répérable par un petite barre bleue dans ``CLion`` et ``Pycharm``.
* ``@BCKG`` : tâche en *background*.
* ``@FIXME`` : bug connu qui doit être corrigé.
* ``@WARNING`` : remarque importante à prendre en compte (tips sur des actions qui ont pris du temps à faire, corriger, …)
* ``@LATER`` : non urgent. À faire plus tard.
* ``@DONE`` : pour les actions faites mais pour lesquelles on souhaite garder une trace (comme dans le CDC des besoins pour Gaïa).
* ``@GRAMAT`` : fonctionnalités indispensables pour installation à Gramat.
* ``@SUSPEND`` : en attente de poser un question à qqn, ou d'un besoin informatique ...
* ``@INTERN`` : pour ce qui n'est valable qu'en interne et qui est donc en commentaire ailleurs qu'en interne..

