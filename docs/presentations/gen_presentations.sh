#!/usr/bin/env bash

for pres in $(find . -maxdepth 2 -type d)
do
    if [[ "${pres}" != "." ]] ; then
        cd ${pres}
        if [[ -f ./Makefile ]] ; then
            echo "## Presentation pour ${pres}"
            make slides
        fi
        cd -
    fi
done