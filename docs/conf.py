# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/stable/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('./developer/docstrings'))
sys.path.insert(0, os.path.abspath('../extern/matplotlib-cpp-master'))
sys.path.insert(0, os.path.abspath('../extern/Catch2-master'))
sys.path.insert(0, os.path.abspath('../export'))
sys.path.insert(0, os.path.abspath('..'))

import nemesis

os.environ['MPLBACKEND'] = 'Agg'  # avoid tkinter import errors on rtfd.io

# -- Project information -----------------------------------------------------

project = nemesis.__title__
nem_copyright = nemesis.__copyright__
author = nemesis.__author__

# The short X.Y version
version = nemesis.__version__
# The full version, including alpha/beta/rc tags
release = version


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'numpydoc',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.graphviz',
    'sphinx.ext.napoleon',
    'sphinx.ext.imgmath',
    'sphinxcontrib.mermaid',
    'recommonmark',
]

autosummary_generate = True

autodoc_docstring_signature = True
autodoc_default_flags = ['members', 'undoc-members']

"""
explicit_order_folders = [
                          '../examples/api',
                          '../examples/pyplots',
                          '../examples/subplots_axes_and_figures',
                          '../examples/color',
                          '../examples/statistics',
                          '../examples/lines_bars_and_markers',
                          '../examples/images_contours_and_fields',
                          '../examples/shapes_and_collections',
                          '../examples/text_labels_and_annotations',
                          '../examples/pie_and_polar_charts',
                          '../examples/style_sheets',
                          '../examples/axes_grid',
                          '../examples/showcase',
                          '../tutorials/introductory',
                          '../tutorials/intermediate',
                          '../tutorials/advanced']
for folder in sorted(glob('../examples/*') + glob('../tutorials/*')):
    if not os.path.isdir(folder) or folder in explicit_order_folders:
        continue
    explicit_order_folders.append(folder)

# Sphinx gallery configuration
sphinx_gallery_conf = {
    'examples_dirs': ['../examples', '../tutorials'],
    'filename_pattern': '^((?!sgskip).)*$',
    'gallery_dirs': ['gallery', 'tutorials'],
    'doc_module': ('matplotlib', 'mpl_toolkits'),
    'reference_url': {
        'matplotlib': None,
        'numpy': 'https://docs.scipy.org/doc/numpy',
        'scipy': 'https://docs.scipy.org/doc/scipy/reference',
    },
    'backreferences_dir': 'api/_as_gen',
    'subsection_order': ExplicitOrder(explicit_order_folders)
}
"""

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

source_parsers = {
   '.md': 'recommonmark.parser.CommonMarkParser',
}

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
source_suffix = ['.rst', '.md']
# source_suffix = '.rst'


# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'fr'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path .
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'presentations']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
# html_theme = 'nature'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {}

html_context = {
    'extra_css_files': [
        '_static/extra.css',
        '_static/admonition.css'
        '_static/codehilite.css',
        '_static/impression.css',
        '_static/light_print_style.css',
        '_static/light_style.css'
    ],
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static/extra.css']
html_static_path = ['_static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}
html_title = project

html_favicon = './_static/nemesis_small.png'

# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'nemesisdoc'


# -- Options for LaTeX output ------------------------------------------------
# Exemple de ce que peux contenir latex_elements
# The paper size ('letterpaper' or 'a4paper').
# 'papersize': 'a4paper',
#
# The font size ('10pt', '11pt' or '12pt').
# 'pointsize': '11pt',
#
# Additional stuff for the LaTeX preamble.
# preamble': '',
#
# Latex figure (float) alignment
# 'figure_align': 'htbp',

latex_elements = dict()

# Additional stuff for the LaTeX preamble.
latex_elements['preamble'] = r"""
   % In the parameters section, place a newline after the Parameters
   % header.  (This is stolen directly from Numpy's conf.py, since it
   % affects Numpy-style docstrings).
   \usepackage{expdlist}
   \let\latexdescription=\description
   \def\description{\latexdescription{}{} \breaklabel}

   \usepackage{amsmath}
   \usepackage{amsfonts}
   \usepackage{amssymb}
   \usepackage{txfonts}

   % The enumitem package provides unlimited nesting of lists and
   % enums.  Sphinx may use this in the future, in which case this can
   % be removed.  See
   % https://bitbucket.org/birkenfeld/sphinx/issue/777/latex-output-too-deeply-nested
   \usepackage{enumitem}
   \setlistdepth{2048}
"""

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'nemesis.tex', 'nemesis Documentation',
     'Claire Guilbaud', 'manual'),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'nemesis', 'nemesis Documentation',
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'nemesis', 'nemesis Documentation',
     author, 'nemesis', 'One line description of project.',
     'Miscellaneous'),
]


# -- Extension configuration -------------------------------------------------

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
# intersphinx_mapping = {
#     'python': ('/path/vers/share/python/Python-3.7.0',
#                '/path/vers/share/python/Python-3.7.0/objects.inv'),
#     'numpy': ('/path/vers/share/python/numpy-1.15.4',
#               '/path/vers/share/python/numpy-1.15.4/objects.inv'),
#     'matplotlib': ('/path/vers/share/python/matplotlib-3.0.2',
#                    '/path/vers/share/python/matplotlib-3.0.2/objects.inv'),
# }
intersphinx_mapping = {
    'python': ('/home/guilbaudc/Documents/Documentation/Python-3.7.3',
               '/home/guilbaudc/Documents/Documentation/Python-3.7.3/objects.inv'),
    'numpy': ('/home/guilbaudc/Documents/Documentation/numpy-1.16.4',
              '/home/guilbaudc/Documents/Documentation/numpy-1.16.4/objects.inv'),
    'matplotlib': ('/home/guilbaudc/Documents/Documentation/matplotlib-3.1.1',
                   '/home/guilbaudc/Documents/Documentation/matplotlib-3.1.1/objects.inv'),
}

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
