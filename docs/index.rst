.. title:: Aperçu

.. _index:

Welcome to nemesis's documentation!
===================================

.. image:: ./_static/nemesis.png

Documentation du projet nemesis.

.. only:: builder_html

    :Release: |version|
    :Date: |today|




Documentation
=============

.. toctree::
    :hidden:
    :titlesonly:
    :maxdepth: 2

    api/index
    nemesis_glossary
    developer/index


.. only:: builder_html

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
    * :ref:`nemesis_glossary`

.. only:: not builder_html

    * :ref:`modindex`
    * :ref:`nemesis_glossary`


Zen of Python
=============
*(Résultat de la commande ``import this``*)


| The Zen of Python, by Tim Peters
|
| Beautiful is better than ugly.
| Explicit is better than implicit.
| Simple is better than complex.
| Complex is better than complicated.
| Flat is better than nested.
| Sparse is better than dense.
| Readability counts.
| Special cases aren't special enough to break the rules.
| Although practicality beats purity.
| Errors should never pass silently.
| Unless explicitly silenced.
| In the face of ambiguity, refuse the temptation to guess.
| There should be one-- and preferably only one --obvious way to do it.
| Although that way may not be obvious at first unless you're Dutch.
| Now is better than never.
| Although never is often better than *right* now.
| If the implementation is hard to explain, it's a bad idea.
| If the implementation is easy to explain, it may be a good idea.
| Namespaces are one honking great idea -- let's do more of those!



Notes
=====

Documentation
-------------

Pour lancer la génération de la documentation depuis ``pycharm`` avec une config ``sphinx``,
le fichier ``/tmp/MyProducts/pycharm-community-2018.1/helpers/rest_runners/sphinx_runner.py`` a été modifiée
pour ne pas prendre en compte l'exe python python. La ligne correcte est::

    cmdline.main(sys.argv[1:])

Lundi matin
-----------

**[optionnel]** installation de pycharm dans `/tmp`, attention à la modification à faire dans ``sphinx_runner.py``.

