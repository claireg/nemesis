#define _USE_MATH_DEFINES
#include "matplotlibcpp_apic.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

static bool use_ion = true;

void curve(const unsigned int n, double* x, double* y) {
    apic_clf();

    apic_named_plot("Curve", n, x, y);

    if(use_ion) {
        apic_show();
        apic_pause(2);
    }
    else {
        apic_show();
    }
}

double* x_data(const unsigned int n) {
    double* x = (double *) malloc(n * sizeof(double));
    if(x == NULL) {
        printf("ERREUR mémoire non allouée : tableau des X (taille : %d)", n);
        exit(0);
    }
    for(unsigned int i = 0; i < n; ++i) {
        x[i] = i * i;
    }
    return x;
}

double* y_data(const unsigned int n) {
    double* y = (double *) malloc(n * sizeof(double));
    for(unsigned int i = 0 ; i < n ; ++i) {
        y[i] = sin(2 * M_PI * i / 360.0) + 2.;
    }
    if(y == NULL) {
        printf("ERREUR mémoire non allouée : tableau des Y (taille : %d)", n);
        exit(0);
    }
    return y;
}

int main(int argc, char** argv) {
    unsigned int n = 500;

    double* x = x_data(n);
    double* y = y_data(n);

    apic_backend("Qt5Agg");

    if(use_ion) {
        apic_ion();
    }

    curve(n, x, y);

    if(use_ion) {
        apic_ioff();
    }
    apic_close();

    free(x);
    free(y);

    return 0;
}

