//
// \brief   Exemple simple d'utilisation de matplotlibcpp-cea
// \date    4 octobre 2019
// \author  Claire Guilbaud

#include <vector>
#include <string>
#include <map>
#include <cmath>

#include "matplotlibcpp_cea.hpp"

// Indispensable pour avoir le namespack nem_utils
#include "utilities.h"

using namespace std;

namespace plt = matplotlibcpp;
namespace nem = nem_utils;

int main()
{
    // Some data
    unsigned long n = 5000;
    std::vector<double> x(n), y(n), z(n), w(n, 2);
    for (unsigned long i = 0; i < n; ++i) {
        auto j = i + 1;
        x.at(i) = j * j;
        y.at(i) = 1 + sin(2 * M_PI * j / 360.0);
        z.at(i) = log(j);
    }
    std::map<std::string, std::string> keywords {{"label", "Jolie courbe"}};

    plt::backend("Qt5Agg");
    // en matplotlib 3.1.1 : permet de faire le plt::clf() sans planter mais ...
    // plt::ion();
    // plante si il n'y a pas de figure !
    // plt::clf();
    plt::plot(x, w, "r--");
    plt::plot(y, "b+");
    plt::plot(x, y, "g+", x, w, "b+", x, z, "r--");
    plt::pause(4);

    plt::clf();
    plt::plot(w);
    plt::show();

    return 0;
}
