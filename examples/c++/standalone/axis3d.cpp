#define _USE_MATH_DEFINES
#include "matplotlibcpp.hpp"

#include <cmath>
// @FIXME : Ne règle pas le problème pour les compilateurs < 201402L
#if __cplusplus >= 201402L && __cplusplus < 201703L
#include <experimental/filesystem>
#else
#include <filesystem>
#endif
#include <iostream>

// Déjà inclut dans matplotlibcpp.hpp
// #include "nemesiscppconfig.h"
#include "utilities.h"

namespace plt = matplotlibcpp;
namespace nem = nem_utils;

int check_3d()
{
    plt::clf();
    // obligatoire sinon pas de projection 3d
    plt::load_3d_modules();
    plt::switch_axes("3d");
    auto i3d = plt::is_3d();
    if(i3d) {
        std::cout << "Pas besoin de changer de boîtes d'axes\n";
    }
    else {
        std::cout << "INATTENDU : Besoin de changer de boîtes d'axes\n";
    }

    std::vector<double> x, y, z;
    auto v=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x.push_back(v);
        y.push_back(v);
        z.push_back(std::sin(std::hypot(v, v)));
        v+=0.25;
    }
    plt::plot3D(x, y, z);
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int plot3d_cst()
{
    std::vector<double> x, y, z;
    auto w=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x.push_back(w);
        y.push_back(std::sin(std::hypot(w, w)));
        w+=0.25;
    }
    z.push_back(2.);

    plt::clf();
    plt::plot3D(x, y, z);
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int plot3d()
{
    plt::clf();
    std::vector<double> x, y, z;
    auto v=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x.push_back(v);
        y.push_back(v);
        z.push_back(std::sin(std::hypot(v, v)));
        v+=0.25;
    }
    plt::plot3D(x, y, z);
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int several_plots3d() {
    std::vector<double> x1, y1, z1;
    auto w=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x1.push_back(w);
        y1.push_back(std::sin(std::hypot(w, w)));
        w+=0.25;
    }
    z1.push_back(2.);

    plt::clf();
    plt::plot3D(x1, y1, z1);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<double> x2, y2, z2;
    auto v=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x2.push_back(v);
        y2.push_back(v);
        z2.push_back(std::sin(std::hypot(v, v)));
        v+=0.25;
    }

    // Attention : plt::clf appelle pyplot.clf, donc supprime toutes les boîtes d'axes de la figure, plus tout ce
    //  qui peut être mis dans une figurae.
    //  Donc la boîtes d'axes par défaut étant rectilinear, il faut reconstruire la boite 3D après chaque appel à clf
//    plt::clf();
    plt::plot3D(x2, y2, z2);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<std::vector<double>> x3, y3, z3;
    auto i=-5.0;
    for (auto ci = 0; ci <= 40;  ++ci) {
        std::vector<double> x_row, y_row, z_row;
        auto j=-5.0;
        for (auto cj = 0; cj <= 40; ++cj) {
            x_row.push_back(i);
            y_row.push_back(j);
            z_row.push_back(std::sin(std::hypot(i, j)));
            i+=0.25;
            j+=0.25;
        }
        x3.push_back(x_row);
        y3.push_back(y_row);
        z3.push_back(z_row);
    }

//    plt::clf();
    plt::plot_surface(x3, y3, z3);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    plt::close();
    return 0;
}

int several_plots3d_with_clf() {
    std::vector<double> x1, y1, z1;
    auto w=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x1.push_back(w);
        y1.push_back(std::sin(std::hypot(w, w)));
        w+=0.25;
    }
    z1.push_back(2.);

    plt::clf();
    plt::plot3D(x1, y1, z1);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<double> x2, y2, z2;
    auto v=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x2.push_back(v);
        y2.push_back(v);
        z2.push_back(std::sin(std::hypot(v, v)));
        v+=0.25;
    }

    // Attention : plt::clf appelle pyplot.clf, donc une boîte d'axes tout sauf 3d, du coup si on est en 3D, ça détruit
    //  la boîte courant pour en recréer une linéaire, d'où les switch_axes rectilinear to 3d
    // Si pas d'appel à plt::clf() : comportement attendu
    plt::clf();
    plt::plot3D(x2, y2, z2);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<std::vector<double>> x3, y3, z3;
    auto i=-5.0;
    for (auto ci = 0; ci <= 40;  ++ci) {
        std::vector<double> x_row, y_row, z_row;
        auto j=-5.0;
        for (auto cj = 0; cj <= 40; ++cj) {
            x_row.push_back(i);
            y_row.push_back(j);
            z_row.push_back(std::sin(std::hypot(i, j)));
            i+=0.25;
            j+=0.25;
        }
        x3.push_back(x_row);
        y3.push_back(y_row);
        z3.push_back(z_row);
    }

    plt::clf();
    plt::plot_surface(x3, y3, z3);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    plt::close();
    return 0;
}

int several_plots3d_with_cla() {
    std::vector<double> x1, y1, z1;
    auto w=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x1.push_back(w);
        y1.push_back(std::sin(std::hypot(w, w)));
        w+=0.25;
    }
    z1.push_back(2.);

    plt::cla();
    plt::plot3D(x1, y1, z1);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<double> x2, y2, z2;
    auto v=-5.0;
    for (auto ci=0; ci<=40; ++ci) {
        x2.push_back(v);
        y2.push_back(v);
        z2.push_back(std::sin(std::hypot(v, v)));
        v+=0.25;
    }

    // Attention : plt::clf appelle pyplot.clf, donc une boîte d'axes tout sauf 3d, du coup si on est en 3D, ça détruit
    //  la boîte courant pour en recréer une linéaire, d'où les switch_axes rectilinear to 3d
    // Si pas d'appel à plt::clf() : comportement attendu
    plt::cla();
    plt::plot3D(x2, y2, z2);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<std::vector<double>> x3, y3, z3;
    auto i=-5.0;
    for (auto ci = 0; ci <= 40;  ++ci) {
        std::vector<double> x_row, y_row, z_row;
        auto j=-5.0;
        for (auto cj = 0; cj <= 40; ++cj) {
            x_row.push_back(i);
            y_row.push_back(j);
            z_row.push_back(std::sin(std::hypot(i, j)));
            i+=0.25;
            j+=0.25;
        }
        x3.push_back(x_row);
        y3.push_back(y_row);
        z3.push_back(z_row);
    }

    plt::cla();
    plt::plot_surface(x3, y3, z3);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    plt::close();
    return 0;
}

int several_plots() {
    std::vector<double> x1, y1, z1;
    auto i = -5.0;
    for (auto ci = 0; ci <= 40;  ++ci) {
        x1.push_back(i);
        y1.push_back(std::sin(std::hypot(i, i)));
        i+= 0.25;
    }
    z1.push_back(2.);

    plt::clf();
    plt::plot3D(x1, y1, z1);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    std::vector<double> x2, y2;

    i = -5.0;
    for (auto ci = 0; ci <= 40;  ++ci) {
        x2.push_back(i);
        y2.push_back(std::sin(std::hypot(i, i)));
        i+= 0.25;
    }
    // La boîte d'axes rectilinear n'est pas recréée, sauf si on fait appel à plt::clf() ou switch_axes
    plt::switch_axes("rectilinear");
    plt::plot(x2, y2);
    plt::grid(true);
    plt::draw();
    plt::pause(2);

    plt::close();
    return 0;
}

int plot_surface()
{
    std::vector<std::vector<double>> x, y, z;
    auto i = -5.0;
    for(auto ci=0; ci<=40; ++ci) {
        std::vector<double> x_row, y_row, z_row;
        auto j=-5;
        for(auto cj=0; cj<=40; ++cj) {
            x_row.push_back(i);
            y_row.push_back(j);
            z_row.push_back(std::sin(std::hypot(i, j)));
            i+=0.25;
            j+=0.25;
        }
        x.push_back(x_row);
        y.push_back(y_row);
        z.push_back(z_row);
    }

    plt::clf();
    plt::plot_surface(x, y, z);
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int main()
{
    plt::backend("Qt5Agg");
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    plot_surface();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    plot3d();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    plot3d_cst();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    check_3d();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    several_plots3d();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    several_plots3d_with_clf();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    several_plots3d_with_cla();
    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
    // boîte d'axes rectilinear non recréée automatiquement
//    several_plots();

    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
    for(auto i=0; i<4; i++) {
        std::cout << "\n### " << i << '\n';
        nem::logger_timer("several_plots3d", several_plots3d)();
        nem::logger_timer("several_plots3d_with_clf", several_plots3d_with_clf)();
        nem::logger_timer("several_plots3d_with_cla", several_plots3d_with_cla)();
    }
}
