# Issue de https://gitlab.kitware.com/cmake/community/wikis/doc/cmake/CrossCompiling
include(CMakeForceCompiler)

# this one is important
set(CMAKE_SYSTEM_NAME gnu_83)

set(GNU_VERSION 8.3)
set(GNU_ROOT /usr)

# specify the cross compiler
CMAKE_FORCE_C_COMPILER("${GNU_ROOT}/bin/gcc" GNU)
CMAKE_FORCE_CXX_COMPILER(${GNU_ROOT}/bin/g++ GNU)

# where is the target environment
#set(CMAKE_FIND_ROOT_PATH  /home/alex/src/ecos/install )
set(CMAKE_FIND_ROOT_PATH "${GNU_ROOT}")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
