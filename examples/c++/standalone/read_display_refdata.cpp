#define _USE_MATH_DEFINES
#include "matplotlibcpp.hpp"

#include <cmath>
#if __cplusplus >= 201402L && __cplusplus < 201703L
#include <experimental/filesystem>
#else
#include <filesystem>
#endif
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <typeinfo>

#ifdef CATCH_UNIT_TESTS
#include "catch.hpp"
#endif

// Déjà inclut dans matplotlibcpp.hpp
// #include "nemesiscppconfig.h"
#include "utilities.h"

namespace plt = matplotlibcpp;
#if __cplusplus >= 201402L
namespace fs = std::experimental::filesystem;
#endif
namespace nem = nem_utils;

auto read_file(const std::string& filepath)
{
    std::wifstream flux(filepath.c_str(), std::ios_base::in);

    std::vector<std::vector<double>> results;
    std::vector<std::wstring> vec_n_items(1);
    std::wstring item_type;
    int i_tuple = 0;
    int n_items = -1;
    while (!flux.eof() && !flux.fail()) {
        std::wstring header;
        flux >> header;
        if (header == L"tuple") {
            flux >> n_items;
            results.resize(n_items);
        } else if (header == L"ndarray") {
            if (n_items == -1) {
                // pas de ligne tuple dans le fichier lu => un seul tableau
                results.resize(1);
            }
            flux >> item_type;

            int n_dim = 1;
            std::wstring line;
            do {
                std::getline(flux, line);
            } while (!flux.eof() && flux.good() && line.empty());
            nem::split_string(vec_n_items, line.c_str());
            if (vec_n_items.size() == 1) {
                results[i_tuple].resize(stoi(vec_n_items[0]));
                auto data = &results[i_tuple];
                for (double & i : *data) {
                    flux >> i;
                }
                i_tuple++;
            } else if (vec_n_items.size() == 2) {
                // @TODO : pas sûr que matplotlibcpp sache les prendre en compte.
                auto new_size = stoi(vec_n_items[0]) * stoi(vec_n_items[1]);
                results[i_tuple].reserve(new_size);
                results[i_tuple].resize(new_size);
                auto data = &results[i_tuple];
                for (double & i : *data) {
                    flux >> i;
                }
                i_tuple++;
            }
        } else {
            if (!header.empty()) {
                std::wcout << "header lu = " << header << std::endl;
                throw std::runtime_error(
                    "Type non pris en compte lors de la lecture d'un fichier.");
            }
        }
    }

    return results;
}

#ifdef CATCH_UNIT_TESTS
TEST_CASE("read_file", "[read_display_refdata]")
{
    auto filename = "small_curve.txt";
    auto filepath = nem::create_reference_path(filename);

    REQUIRE_NOTHROW(read_file(filepath.native()));

    auto results = read_file(filepath.native());
    auto x = results[0];
    auto y = results[1];

    REQUIRE(x.size() == 2);
    REQUIRE(x.size() == y.size());

    REQUIRE(x[0] == 1);
    REQUIRE(x[1] == 3);

    REQUIRE(y[0] == 10);
    REQUIRE(y[1] == 5);
}
#endif

#if __cplusplus >= 201402L
auto go(const fs::path& filepath_, const bool print_ = false)
{
    // vérification existence du fichier à lire
    nem::exists(filepath_);

    std::cout << "-------------------------" << std::endl;
    std::cout << "Lecture de " << filepath_.native() << "\n";

    std::vector<std::vector<double>> results = read_file(filepath_.native());
    std::cout << "Nombre de tableau lus : " << results.size() << "\n";
    if (print_ == true) {
        nem::print_vector_vector(results);
    }
    std::cout << std::endl;

    return results;
}

// @FIXME ne fonctionne plus : problème de lecture
int ex_1(int pause_)
{
#ifdef FIXME_DONE
    auto filename = "curves_rx_data.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    auto x = results[0];
    auto y = results[1];
    plt::clf();
    plt::semilogx(x, y);
    plt::xlim(0.1, 100.);
    plt::ylim(0.1, 10.);
    plt::title(filename);
    // plt::pause(pause_);
#endif
    return 0;
}

int ex_2(int pause_)
{
#ifdef CEA_INTERN
    auto filename = "gaia_curve_data.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    auto y = results[0];
    plt::clf();
    plt::named_plot("ma courbe", y, "o-");
    plt::legend();
    plt::title(filename);
    plt::pause(pause_);
#endif
    return 0;
}

int ex_3()
{
#ifdef CEA_INTERN
    auto filename = "gaia_stairs_curve_data.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    auto x = results[0];
    auto y = results[1];
    if (x.size() > y.size()) {
        x.pop_back();
    } else if (y.size() > x.size()) {
        y.pop_back();
    }
    plt::clf();
    plt::plot(x, y, "r-", x, [](double d) { return 12.5 + abs(sin(d)); }, "k-");
    plt::title(filename);
    plt::pause(3);
#endif
    return 0;
}

int ex_4()
{
#ifdef CEA_INTERN
    auto filename = "gaia_network_0.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    // 3 tableaux - 2 d'1 dimension, le dernier a 2 dimensions.
    auto x = results[0];
    auto y = results[1];
    auto z = results[2];
    plt::clf();
    plt::plot(z, "+-");
    plt::title(filename);
    plt::pause(3);
#endif
    return 0;
}

int ex_5(int pause_)
{
#ifdef CEA_INTERN
    auto filename = "gaia_network_1.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    // 5 tableaux
    auto z = results[4];
    plt::clf();
    plt::plot(z, ".-");
    plt::title(filename);
    plt::pause(pause_);
#endif
    return 0;
}

int ex_6(int pause_)
{
#ifdef CEA_INTERN
    auto filename = "gaia_network_2.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    // 3 tableaux - 2 d'1 dimension, le dernier a 2 dimensions.
    auto z = results[2];
    std::map<std::string, std::string> keywords;
    keywords["label"] = "line 1";
    keywords["linewidth"] = "2";
    plt::clf();
    plt::plot(z, "g-^");
    plt::title(filename);
    plt::pause(pause_);
#endif
    return 0;
}

#ifdef CEA_INTERN
auto read_network_2()
{
    auto filename = "gaia_network_2.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = go(filepath);
    // 3 tableaux - 2 d'1 dimension, le dernier a 2 dimensions.
    // dimensions de tableaux
    //  results[0] : 54
    //  results[1] : 12277
    //  results[25 : 54 x 12277
    return std::move(results);
}
#endif

std::vector<std::vector<double>> prepare_data(std::vector<std::vector<double>>& results)
{
    auto z = results[2]; // std::vector<std::vector<double>>
    std::vector<std::vector<double>> split_z(results[0].size());
    auto bigvec = results[2];
    auto extern_size = results[0].size();
    auto intern_size = results[1].size();
    auto it_start = bigvec.begin();
    auto start = 0;
    for (auto & i : split_z) {
        auto current_vec = &i;
        current_vec->resize(intern_size);
        start += intern_size;
        std::copy_n(it_start, intern_size, current_vec->begin());
        std::advance(it_start, intern_size);
    }
    return std::move(split_z);
}
#endif

int cpp_py_cmp_network_2_loglog_loop(const std::vector<std::vector<double>>& results, const std::vector<std::vector<double>>& split_z)
{
    plt::clf();
    for (auto& subvector : split_z) {
        plt::loglog(results[1], subvector);
    }
    auto x_minmax = std::minmax_element(results[1].begin(), results[1].end());
    plt::xlim(*(x_minmax.first), *(x_minmax.second));
    return 0;
}

#if __cplusplus >= 201402L
void ex_small()
{
#ifdef CEA_INTERN
    auto filename = "small_curve.txt";
    auto filepath = nem::create_reference_path(filename);
    auto results = read_file(filepath.native());
    nem::print_vector_vector(results);
    auto x = results[0];
    auto y = results[1];
    plt::clf();
    plt::title(filename);
    plt::semilogx(x, y);
    plt::show();
#endif
}
#endif

#ifndef CATCH_UNIT_TESTS
int main(int argc, char** argv)
{
    // Sur calculateur, avec le backend TkAgg (défaut), l'appel à plt::pause plante
    // Traceback (most recent call last):
    //      File "/ccc/products/opendist/gcc6/python/3.7.0/Nemesis_0.1_gcc_64/lib/python3.7/site-packages/matplotlib/pyplot.py", line 296, in pause
    //          canvas.start_event_loop(interval)
    //      File "/ccc/products/opendist/gcc6/python/3.7.0/Nemesis_0.1_gcc_64/lib/python3.7/site-packages/matplotlib/backend_bases.py", line 2460, in start_event_loop
    //          self.flush_events()
    //      File "/ccc/products/opendist/gcc6/python/3.7.0/Nemesis_0.1_gcc_64/lib/python3.7/site-packages/matplotlib/backends/_backend_tk.py", line 452, in flush_events
    //          self._master.update()
    //      File "/ccc/products/opendist/gcc6/python/3.7.0/python/lib/python3.7/tkinter/__init__.py", line 1174, in update
    //          self.tk.call('update')
    // _tkinter.TclError: can't invoke "update" command:  application has been destroyed
    // terminate called after throwing an instance of 'std::runtime_error'
    //  what():  Call to pause() failed.

    plt::backend("Qt5Agg");

    nem::cartridge();

    auto pause = 3;

    nem::logger_timer("gaia_section_efficace", ex_1)(pause);
    nem::logger_timer("gaia_curve_data", ex_2)(pause);
    nem::logger_timer("gaia_stairs_curve_data", ex_3)();
    nem::logger_timer("gaia_network_0", ex_4)();
    nem::logger_timer("gaia_network_1", ex_5)(pause);
    nem::logger_timer("gaia_network_2", ex_6)(pause);

#if __cplusplus >= 201402L
#ifdef CEA_INTERN
    auto results = read_network_2();
        auto split_z = prepare_data(results);
        // auto r_7 = nem::logger_timer("real_gaia_network_2", cpp_py_cmp_network_2_loglog_loop)(results, split_z);
        // nem::logger_timer("loop", cpp_py_cmp_network_2_loglog_one_loop)(results, split_z);
        // nem::logger_timer("unpack", cpp_py_cmp_network_2_loglog_one)(results, split_z);
        nem::logger_timer("loglog_loop", cpp_py_cmp_network_2_loglog_loop)(results, split_z);
#endif
#endif

#ifdef CEA_INTERN
    // si rien d'afficher, interpreter non initialisé, donc pause plante : @FIXME
    // ex_small();
    plt::pause(pause);
#endif
}
#endif
