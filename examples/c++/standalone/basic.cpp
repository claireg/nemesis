#define _USE_MAT()H_DEFINES
#include "matplotlibcpp.hpp"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <tuple>
#include <typeinfo>

// Déjà inclut dans matplotlibcpp.hpp
// #include "nemesiscppconfig.h"
#include "utilities.h"

namespace plt = matplotlibcpp;
namespace nem = nem_utils;

static const int WIDTH = 1600;
static const int HEIGHT = 1200;
static const double LINE_WIDTH = 1.5;

static std::string BBL() { return std::string("OPALV_4_ponc._(Sigles:ACE)"); }
static std::string TITLE() { return std::string("Temperature(keV) Min=1.0000E-03 Max=1.00000E+03"); }
static std::string MAT() { return std::string("U"); }
static std::string XLABEL() { return std::string("Densité (keV)"); }
static std::string YLABEL() { return std::string("Libre parcours moyen de Rosseland (cm)"); }

void simple()
{
    int width = 500;
    int height = 400;

    // Avec matplotlib 3.1.1, indispensable à avoir sinon segv
    plt::ion();

    plt::set_figure_size(width, height);
    plt::nemesis_params();

    // Prepare data.
    int n = 5000;
    std::vector<double> x(n), y(n), z(n), w(n, 2);
    for (int i = 0; i < n; ++i) {
        x.at(i) = i * i;
        y.at(i) = sin(2 * M_PI * i / 360.0);
        z.at(i) = log(i);
    }

    // Plot line from given x and y data. Color is selected automatically.
    plt::plot(x, y);
    // Plot a red dashed line from given x and y data.
    plt::plot(x, w, "r--");
    // Plot a line whose name will show up as "log(x)" in the legend.
    plt::named_plot("log(x)", x, z);

    // Add some titles
    plt::set_titles("Gros titre", "Sous-Titre", "Mat");

    // Set x-axis to interval [0,1000000]
    plt::xlim(0, 1000 * 1000);
    // Enable legend.
    plt::legend();
    // Use log scale
    plt::set_log_scale(false, true);
    // Use grid
    plt::grid(true);

    // save figure
    const char* filename = "./basic.png";
    std::cout << "Saving result to " << filename << std::endl;

    plt::save(filename);
    plt::close();
}

auto read_file(const std::string& filepath)
{
    std::wifstream flux(filepath.c_str(), std::ios_base::in);

    std::vector<std::vector<double>> results;
    std::vector<std::tuple<int, int>> results_size;
    std::vector<std::wstring> vec_n_items(1);
    std::wstring item_type;
    int i_tuple = 0;
    int n_items = -1;
    while (!flux.eof() && !flux.fail()) {
        std::wstring header;
        flux >> header;
        if (header == L"tuple") {
            flux >> n_items;
            results.resize(n_items);
            results_size.resize(n_items);
        } else if (header == L"ndarray") {
            if (n_items == -1) {
                // pas de ligne tuple dans le fichier lu => un seul tableau
                results.resize(1);
                results_size.resize(1);
            }
            flux >> item_type;

            std::wstring line;
            do {
                std::getline(flux, line);
            } while (!flux.eof() && flux.good() && line.empty());
            nem::split_string(vec_n_items, line.c_str());
            if (vec_n_items.size() == 1) {
                results[i_tuple].resize(stoi(vec_n_items[0]));
                results_size[i_tuple] = std::make_tuple(stoi(vec_n_items[0]), 0);
                auto data = &results[i_tuple];
                //                std::cout << "Lecture tuple " << i_tuple << " : " << data->size() << '\n';
                for (double & i : *data) {
                    flux >> i;
                }
                i_tuple++;
            } else if (vec_n_items.size() == 2) {
                // @TODO : pas sûr que matplotlibcpp sache les prendre en compte.
                auto new_size = stoi(vec_n_items[0]) * stoi(vec_n_items[1]);
                results_size[i_tuple] = std::make_tuple(stoi(vec_n_items[0]), stoi(vec_n_items[1]));
                results[i_tuple].reserve(new_size);
                results[i_tuple].resize(new_size);
                auto data = &results[i_tuple];
                //                std::cout << "Lecture tuple " << i_tuple << " : " << data->size() << '\n';
                for (double & i : *data) {
                    flux >> i;
                }
                i_tuple++;
            }
        } else {
            if (!header.empty()) {
                std::wcout << "header lu = " << header << std::endl;
                throw std::runtime_error(
                    "Type non pris en compte lors de la lecture d'un fichier.");
            }
        }
    }

    return std::make_tuple(results, results_size);
}

void debug_write_data(std::ofstream& ofs, const std::string& title, const std::vector<double>& data)
{
    ofs << "# " << title << " ";
    std::copy(data.begin(), data.end(), std::ostream_iterator<double>(ofs, " "));
    ofs << '\n';
}

auto read_curves_rx_data()
{
    auto filename = "curves_rx_data.txt";
    auto filepath = nem::create_reference_path(filename);
    std::vector<std::vector<double>> results;
    std::vector<std::tuple<int, int>> results_size;
    std::tie(results, results_size) = read_file(filepath.native());
    // 1 tableaux - 1 d'1 dimension, le dernier a 2 dimensions.
    auto x = results[0];
    auto z = results[1];

    //    std::string output_file = "/tmp/cpp_data.txt";
    //    std::ofstream ofs(output_file);
    //    debug_write_data(ofs, std::string("X"), x1);
    auto n_curves = std::get<0>(results_size[1]);
    auto n_points = std::get<1>(results_size[1]);
    auto start = z.begin();
    std::vector<std::vector<double>> ly;
    ly.resize(n_curves);
    for (auto c = 0; c < n_curves; ++c) {
        ly[c].resize(n_points);
        std::copy(start, start + n_points, ly[c].begin());
        start += n_points;
        //        std::ostringstream title;
        //        title << "Y[" << c << "]";
        //        debug_write_data(ofs, title.str(), tmp);
    }
    return std::make_tuple(x, ly);
}

int run_mplconfig_loop()
{
    std::vector<std::vector<double>> ly;
    std::vector<double> x;

    plt::ion();

    plt::set_figure_size(WIDTH, HEIGHT);
    plt::nemesis_params();
    plt::set_linewidth(LINE_WIDTH);
    std::tie(x, ly) = read_curves_rx_data();
    for (const auto & c : ly) {
        plt::plot(x, c);
    }
    plt::set_log_scale(true, true);
    plt::set_titles(BBL(), TITLE(), MAT());
    plt::xlabel(XLABEL());
    plt::ylabel(YLABEL());
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int run_plt_loop()
{
    std::vector<std::vector<double>> ly;
    std::vector<double> x;

    //    Appel direct à matplotlib, n'implique aucune autre modification
    //    Important pour comparaison avec run_mplconfig_loop
    plt::set_figure_size(WIDTH, HEIGHT);
    plt::nemesis_params();
    plt::set_linewidth(LINE_WIDTH);
    std::tie(x, ly) = read_curves_rx_data();
    for (const auto & c : ly) {
        plt::loglog(x, c);
    }
    //     N'ajoute que des textes, n'implique pas d'autres changements
    plt::set_titles(BBL(), TITLE(), MAT());
    plt::xlabel(XLABEL());
    plt::ylabel(YLABEL());
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int run_mplconfig_loop_no_data(const std::vector<double>& x, const std::vector<std::vector<double>>& ly)
{
    plt::set_figure_size(WIDTH, HEIGHT);
    plt::nemesis_params();
    plt::set_linewidth(LINE_WIDTH);
    for (const auto & c : ly) {
        plt::plot(x, c);
    }
    plt::set_log_scale(true, true);
    plt::set_titles(BBL(), TITLE(), MAT());
    plt::xlabel(XLABEL());
    plt::ylabel(YLABEL());
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int run_plt_loop_no_data(const std::vector<double>& x, const std::vector<std::vector<double>>& ly)
{
    //    Appel direct à matplotlib, n'implique aucune autre modification
    //    Important pour comparaison avec run_mplconfig_loop
    plt::set_figure_size(WIDTH, HEIGHT);
    plt::nemesis_params();
    plt::set_linewidth(LINE_WIDTH);
    for (const auto & c : ly) {
        plt::loglog(x, c);
    }
    //     N'ajoute que des textes, n'implique pas d'autres changements
    plt::set_titles(BBL(), TITLE(), MAT());
    plt::xlabel(XLABEL());
    plt::ylabel(YLABEL());
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

int run_stem()
{
    // x = np.linspace(0.1, 2 * np.pi, 10)
    std::vector<double> x_data = {0.1, 0.78702059, 1.47404118, 2.16106177, 2.84808236, 3.53510295, \
        4.22212354, 4.90914413, 5.59616472, 6.28318531};
    std::vector<double> y_data = {0.99500417, 0.70595862, 0.09660425, -0.55658157, -0.9572342, \
        -0.92356879, -0.47086008, 0.19548812, 0.77313909, 1.};
    std::map<std::string, std::string> keywords_stem {{"markerfmt", "C0o"}};

    // pas de plt::clf() si pas de figure
    plt::stem(x_data);
    plt::grid(true);
    plt::draw();
    plt::pause(2);
    plt::close();

    return 0;
}

double compute_mean(const std::vector<double>& array)
{
    auto mean = 0.;
    for (auto e : array) {
        mean += e;
    }
    return mean / array.size();
}

void elapsed_time()
{
    std::vector<double> y_plt = {2801., 2851., 2853., 2823., 2795., 2872., 2698.};
    std::vector<double> y_mplconfig = {3677., 3712., 3768., 3668., 3662., 3705., 3787.};
    std::vector<double> moy_plt(y_plt.size());
    auto mean_plt = compute_mean(y_plt);
    for (auto&& e : moy_plt) {
        e = mean_plt;
    }
    std::vector<double> moy_mplconfig(y_mplconfig.size());
    auto mean_mplconfig = compute_mean(y_mplconfig);
    for (auto&& e : moy_mplconfig) {
        e = mean_mplconfig;
    }

    plt::plot(y_plt, "r-");
    plt::plot(y_mplconfig, "k-");
    plt::plot(moy_plt, "r--");
    plt::plot(moy_mplconfig, "k--");
    plt::ylim(0., 3800.);
    plt::grid(true);
    plt::show();
}

int main()
{
    // Indispensable car absence de Tkinter sur le portable, et apparement va pas lire
    // le maplotlibrc de <env_spack>/site-packages/matplotlib/mpl_data
    plt::backend("Qt5Agg");

    // en matplotlib 3.1.1 : permet de faire le plt::clf() sans planter mais ...
    // plt::ion();
    // plante si il n'y a pas de figure !
    // plt::clf();

    // ---> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
    //      Utilise nemesis, donc besoin de plt::ion() pour que le premier appel
    //       nemesis ne plante pas (pbm à cause de l'absence de Tkinter sur le portable ?)
    simple();

    // --> PLOT
    //      /!\ Repose sur des données qui n'existent pas en externe !
//    run_mplconfig_loop();
//    run_plt_loop();
//
//    // Attention si on inverse les exécutions, les temps d'exécution diffèrent
//    nem::logger_timer("run_plt_loop", run_plt_loop)();
//    nem::logger_timer("run_mplconfig_loop", run_mplconfig_loop)();

    // --> PLOT
    //      /!\ Repose sur des données qui n'existent pas en externe !
//    std::vector<std::vector<double>> ly;
//    std::vector<double> x;
//    std::tie(x, ly) = read_curves_rx_data();
//
//    run_mplconfig_loop_no_data(x, ly);
//    run_plt_loop_no_data(x, ly);
//
//    for(auto i=0; i<4; i++) {
//        std::cout << "### " << i << '\n';
//        nem::logger_timer("run_mplconfig_loop_no_data", run_mplconfig_loop_no_data)(x, ly);
//        nem::logger_timer("run_plt_loop_no_data", run_plt_loop_no_data)(x, ly);
//    }

    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
//    elapsed_time();

    // --> PLOT
    //      OK : mpl 3.1.1 avec env doris-gcc830
    run_stem();
}
