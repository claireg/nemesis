#include <utility>

#define _USE_MATH_DEFINES
#include <cmath>

// WARNING : à mettre avant les include Qt !
#include "matplotlibcpp.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iterator>
#include <stdexcept>
#include <string>
#include <typeinfo>

#include <QtWidgets>
#include <QObject>          // core
#include <QAction>
#include <QMenu>    // widgets
#include <QApplication>
#include <QComboBox>
#include <QMenu>   // widgets
#include <QObject> // core
#include <QtWidgets>
#include <include/mainwindow.h>

#include "mainwindow.h"

#include "utilities.h"

using namespace std;
namespace plt = matplotlibcpp;
namespace nem = nem_utils;

vector<string> MainWindow::_cmaps = {
    "viridis", "plasma",   "inferno",  "magma",           "cividis",
    "winter",  "cool",     "Wistia",   "gist_heat",       "bone",
    "copper",  "coolwarm", "twilight", "twilight_shifted"};

unsigned int MainWindow::_figure_id = 1;

MainWindow::MainWindow() {
  _data_store.resize(0);
  _current_cmap = _cmaps[0];
  _clear_before_drawing = true;
  _linewidth = 0.9;
  _n_levels = 33;
  _scatter_spec_marker = "o";
  _scatter_spec_color = "black";
  _scatter_spec_size = 36.;
  _scatter_alpha = 1.0;

  _default_vec_stepx = 0.2; // 1.2
  _default_vec_stepy = 0.5; // 1.5

  // ATTENTION _store_map est le contraire de la checkbox correspondante
  _store_map = true;
  _last_action = &MainWindow::about;
  _use_scatter_spec = false;
  _default_subplots_params["left"] = _subplots_params["left"] = 0.125;
  _default_subplots_params["right"] = _subplots_params["right"] = 0.9;
  _default_subplots_params["bottom"] = _subplots_params["bottom"] = 0.1;
  _default_subplots_params["top"] = _subplots_params["top"] = 0.9;
  _default_subplots_params["hspace"] = _subplots_params["hspace"] = 0.2;
  _default_subplots_params["wspace"] = _subplots_params["wspace"] = 0.2;
  _execute_subplots = true;

  _central_widget = new QWidget;
  _central_widget->setObjectName("central_widget");
  setCentralWidget(_central_widget);
  _layout = new QVBoxLayout;
  _layout->setMargin(5);
  _central_widget->setLayout(_layout);

  createActions();
  createStatusBar();
  createMiscWidgets();
  createMapWidgets();
  createScatterWidgets();
  create3DWidgets();
  createSubplotsWidgets();

  setWindowTitle(tr("Eastworld"));

  _data.init(_default_n_points, _default_n_curves);
  _data3d.init();

  // pour positionner la fenêtre matplotlib au bon endroit
  this->show();

  this->init_device();

  // L'objet récupéré n'est pas nul, mais dès qu'on essaie d'appeler une de ces
  // fonctions -> SEGV
  //    QWidget* canvas = plt::get_canvas();
  //    canvas->setWindowTitle("YOUPI");
  //    canvas->show();
  // Peut-être quand récupérant la MainWindow qui ne descend que du QMainWindow
  // ça fonctionnerait ...
}

void MainWindow::closeEvent(QCloseEvent *event) {
  plt::close("all");
  event->accept();
}


void MainWindow::about() {
  QMessageBox::about(
      this, tr("About Application"),
      tr("The <b>Application</b> example demonstrates how to "
         "write modern GUI applications using Qt, with a menu bar, "
         "toolbars, and a status bar."));
}

void MainWindow::redo_last_action() { (this->*_last_action)(); }

void MainWindow::new_figure() {
  _last_action = &MainWindow::new_figure;

  _figure_id += 1;
  _figure_names.push_front("EAST " + std::to_string(_figure_id));
  plt::figure(_figure_names.front());
}

void MainWindow::close_figure() {
  _last_action = &MainWindow::close_figure;

  if (_figure_names.empty()) {
    return;
  }
  plt::close(_figure_names.back());
  //    plt::close("all");
  _figure_names.pop_back();
}

void MainWindow::init_device() {
  plt::backend("Qt5Agg");
  plt::ion();
  plt::nemesis_params();
  plt::set_figure_xy(this->x() + this->frameGeometry().width() + 42,
                     this->y() + 91, 800, 600);
  plt::set_window_title("Eastworld");
  plt::set_linewidth(1.);
  plt::ion();

  plt::plot();
  plt::show(true);

#ifdef DEBUG
  this->print_geometry();
#endif
}

void MainWindow::plot_all() {
  _last_action = &MainWindow::plot_all;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  unsigned long n_points = 0;
  unsigned long n_curves = 0;

  try {
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  } catch (const out_of_range &oor) {
    _data.init(_default_n_points, _default_n_curves);
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  }

  // Stockage des données
  // --------------------
  // Les abscisses sont les mêmes pour toutes les courbes -> ils ne sont stockés
  // qu'une seule fois
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  std::tie(idx_x, idx_y) = store_network(n_points, n_curves, x, y);

  auto labels = _data.get_labels();
  // Affichage avec les données stockées sinon par de modification possible des
  // courbes via l'ihm par défaut
  for (unsigned long c = 0; c < n_curves; ++c) {
    ostringstream oss;
    oss << c << ". f(x) = " << labels.at(c);
    plt::named_plot(oss.str(), _data_store[idx_x], _data_store[idx_y + c]);
  }

  if (n_curves > 1) {
    plt::legend();
  }
}

void MainWindow::plot_all_and_delete() {
  _last_action = &MainWindow::plot_all_and_delete;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  unsigned long n_points = 0;
  unsigned long n_curves = 0;

  try {
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  } catch (const out_of_range &oor) {
    _data.init(_default_n_points, _default_n_curves);
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  }

  // Les abscisses sont les mêmes pour toutes les courbes -> ils ne sont stockés
  // qu'une seule fois
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  std::tie(idx_x, idx_y) = store_network(n_points, n_curves, x, y);

  auto labels = _data.get_labels();
  for (unsigned long c = 0; c < n_curves; ++c) {
    ostringstream oss;
    oss << c << " — f(x) = " << labels.at(c);
    plt::named_plot(oss.str(), _data_store[idx_x], _data_store[idx_y + c]);
  }

  if (n_curves > 1) {
    plt::legend();
  }

  _data.purge();
}

void MainWindow::plot_all_and_delete_one_call() {
  _last_action = &MainWindow::plot_all_and_delete_one_call;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  unsigned long n_points = 0;
  unsigned long n_curves = 0;

  // --- Obtention de données
  try {
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  } catch (const out_of_range &oor) {
    _data.init(_default_n_points, _default_n_curves);
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  }

  // Les abscisses sont les mêmes pour toutes les courbes -> ils ne sont stockés
  // qu'une seule fois
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  std::tie(idx_x, idx_y) = store_network(n_points, n_curves, x, y);

  // --- Affichage
  // Construction des labels
  auto labels = _data.get_labels();
  vector<string> useful_labels(labels);
  // mutable indispensable pour l'initialisation d'une variable capturée dans
  // une generalized lambda capture
  transform(useful_labels.begin(), useful_labels.end(), useful_labels.begin(),
            [idx = -1](const string &s) mutable {
              ++idx;
              return to_string(idx) + " — f(x) = " + s;
            });

  for_each(useful_labels.begin(), useful_labels.end(),
           [xx = _data_store[idx_x],
            it_yy = _data_store.begin()](const string &s) mutable {
             ++it_yy;
             plt::named_plot(s, xx, *it_yy);
           });

  if (n_curves > 1) {
    auto step = 2;
    unsigned long n = n_curves / step;
    vector<string> net_labels(n, "");
    vector<int> net_idx(n, step);
    auto it = 0;
    for (auto &&e : net_labels) {
      for (auto i = 0; i < step; ++i) {
        e += labels[it++] + " / ";
      }
    }
    transform(net_idx.begin(), net_idx.end(), net_idx.begin(),
              [idx = -1](const int step) mutable {
                ++idx;
                return idx * step;
              });
    plt::networks_legend(net_labels, net_idx);
  }

  _data.purge();
}

void MainWindow::plot_nan_points() {
  _last_action = &MainWindow::plot_nan_points;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  unsigned long n_points = 0;
  unsigned long n_curves = 0;

  try {
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  } catch (const out_of_range &oor) {
    _data.init(_default_n_points, _default_n_curves);
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_points, x) = _data.get_x();
    std::tie(n_points, n_curves, y) = _data.get_y();
  }
  // Add NaN points
  auto middle = static_cast<int>(floor(n_points * 0.5));
  auto iy = 0;
  for (unsigned long c = 0; c < n_curves; ++c) {
    auto w = iy + middle;
    y[w - 1] = std::nan("");
    y[w] = std::nan("");
    y[w + 1] = std::nan("");
    iy += n_points;
  }
  // Stockage des données
  // --------------------
  // Les abscisses sont les mêmes pour toutes les courbes -> ils ne sont stockés
  // qu'une seule fois
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  std::tie(idx_x, idx_y) = store_network(n_points, n_curves, x, y);

  auto labels = _data.get_labels();
  // Affichage avec les données stockées sinon par de modification possible des
  // courbes via l'ihm par défaut
  for (unsigned long c = 0; c < n_curves; ++c) {
    ostringstream oss;
    oss << c << ". f(x) = " << labels.at(c);
    plt::named_plot(oss.str(), _data_store[idx_x], _data_store[idx_y + c],
                    "+-");
  }

  if (n_curves > 1) {
    plt::legend();
  }
}

void MainWindow::contourf_lin() {
  _last_action = &MainWindow::contourf_lin;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  auto idx_data = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_x * n_y, z);

  auto label = _data3d.get_labels(0);
  plt::named_linear_contourf(label, _data_store[idx_x], _data_store[idx_y],
                             _data_store[idx_z],
                             make_tuple("n_levels", _n_levels, "draw_iso", true,
                                        "cmap", _current_cmap));

  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

    plt::set_titles(_data3d.get_title(), _data3d.get_sub_title(), _data3d.get_title_mat());

}

void MainWindow::contourf_log() {
  _last_action = &MainWindow::contourf_log;
  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  auto idx_data = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_x * n_y, z);

  auto label = _data3d.get_labels(0);
  plt::named_logarithmic_contourf(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("masked_value", 0., "cmap", _current_cmap));

  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

  set_titles_for_map(_data3d.get_title(), _data3d.get_sub_title(),
                     _data3d.get_title_mat());
}

void MainWindow::contour_lin() {
  _last_action = &MainWindow::contour_lin;
  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  auto idx_data = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_x * n_y, z);

  auto label = _data3d.get_labels(0);
  //    plt::named_linear_contour(label, _data_store[idx_x], _data_store[idx_y],
  //    _data_store[idx_z], 30);
  plt::named_linear_contour(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("n_levels", _n_levels, "cmap", _current_cmap, "linewidths",
                 _linewidth));
  plt::set_titles(_data3d.get_title(), _data3d.get_sub_title(),
                  _data3d.get_title_mat());
  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }
}

void MainWindow::contour_log() {
  _last_action = &MainWindow::contour_log;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  auto idx_data = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_x * n_y, z);

  auto label = _data3d.get_labels(0);
  plt::named_logarithmic_contour(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("n_levels", _n_levels, "cmap", _current_cmap, "linewidths",
                 _linewidth, "masked_value", 0.));
  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

  set_titles_for_map(_data3d.get_title(), _data3d.get_sub_title(),
                     _data3d.get_title_mat());
}

void MainWindow::tricontourf_lin() {
  _last_action = &MainWindow::tricontourf_lin;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_z, z);

  auto label = _data3d.get_labels(0);
  plt::named_linear_tricontourf(label, _data_store[idx_x], _data_store[idx_y],
                                _data_store[idx_z],
                                make_tuple("n_levels", _n_levels, "draw_iso",
                                           true, "cmap", _current_cmap));

  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

  plt::set_titles(_data3d.get_title(), _data3d.get_sub_title(),
                  _data3d.get_title_mat());
}

void MainWindow::tricontourf_log() {
  _last_action = &MainWindow::tricontourf_log;
  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_z, z);

  auto label = _data3d.get_labels(0);
  plt::named_logarithmic_tricontourf(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("masked_value", 0., "cmap", _current_cmap));

  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

  set_titles_for_map(_data3d.get_title(), _data3d.get_sub_title(),
                     _data3d.get_title_mat());
}

void MainWindow::tricontour_lin() {
  _last_action = &MainWindow::tricontour_lin;
  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_z, z);

  auto label = _data3d.get_labels(0);
  plt::named_linear_tricontour(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("n_levels", _n_levels, "cmap", _current_cmap, "linewidths",
                 _linewidth));
  plt::set_titles(_data3d.get_title(), _data3d.get_sub_title(),
                  _data3d.get_title_mat());
  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }
}

void MainWindow::tricontour_log() {
  _last_action = &MainWindow::tricontour_log;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_z, z);

  auto label = _data3d.get_labels(0);
  plt::named_logarithmic_tricontour(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("n_levels", _n_levels, "cmap", _current_cmap, "linewidths",
                 _linewidth));
  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

  set_titles_for_map(_data3d.get_title(), _data3d.get_sub_title(),
                     _data3d.get_title_mat());
}

void MainWindow::load_image() {
  _last_action = &MainWindow::load_image;

  if (_clear_before_drawing) {
    clear();
  }

  string this_file = string(__FILE__);
  string this_directory = this_file.substr(0, this_file.find_last_of('/'));
  string image_name =
      this_directory + "/../resources/images/mprof_run_perf.png";
  auto last = image_name.find_last_of('/');
  auto title = image_name.substr(last + 1);
  plt::load(image_name);

  plt::title(title);
}

void MainWindow::scatter() {
  _last_action = &MainWindow::scatter;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_trix();
    std::tie(n_y, y) = _data3d.get_triy();
    std::tie(n_z, z) = _data3d.get_triz();
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_z, z);

  if (_use_scatter_spec) {
    plt::scatter(_data_store[idx_x], _data_store[idx_y], _scatter_spec_marker,
                 _scatter_spec_color, _scatter_spec_size);
  } else {
    //        plt::scatter(_data_store[idx_x], _data_store[idx_y],
    //                     make_tuple(
    //                             "marker", _scatter_spec_marker,
    //                             "c", _data_store[idx_z],
    //                             "s", _data_store[idx_z],
    //                             "cmap", _current_cmap,
    //                             "alpha", _scatter_alpha
    //                     ));
    plt::scatter(_data_store[idx_x], _data_store[idx_y],
                 make_tuple("marker", _scatter_spec_marker, "c",
                            _data_store[idx_z], "s", _scatter_spec_size, "cmap",
                            _current_cmap, "alpha", _scatter_alpha));
    //        plt::scatter(_data_store[idx_x], _data_store[idx_y],
    //             make_tuple(
    //                     "marker", _scatter_spec_marker,
    //                     "c", _scatter_spec_color,
    //                     "s", _data_store[idx_z],
    //                     "cmap", _current_cmap,
    //                     "alpha", _scatter_alpha
    //             ));
  }
}

void MainWindow::plot_surface() {
  _last_action = &MainWindow::plot_surface;

  if (_clear_before_drawing) {
    clear();
  }

  static std::vector<std::vector<double>> x, y, z;
  if (x.empty()) {
    for (double i = -5; i <= 5; i += 0.25) {
      std::vector<double> x_row, y_row, z_row;
      for (double j = -5; j <= 5; j += 0.25) {
        x_row.push_back(i * 2);
        y_row.push_back(j);
        z_row.push_back(std::sin(std::hypot(i, j)));
      }
      x.push_back(x_row);
      y.push_back(y_row);
      z.push_back(z_row);
    }
  }

  plt::plot_surface(x, y, z, {{"cmap", _current_cmap}});
}

void MainWindow::raised_contourf() {
  _last_action = &MainWindow::plot_surface;

  if (_clear_before_drawing) {
    clear();
  }

  double *x = nullptr;
  double *y = nullptr;
  double *z = nullptr;
  auto n_x = static_cast<unsigned long>(0);
  auto n_y = static_cast<unsigned long>(0);
  auto n_z = static_cast<unsigned long>(0);

  auto idx_data = static_cast<unsigned long>(0);

  try {
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  } catch (const out_of_range &oor) {
    _data3d.init();
    // les données existent, elles viennent d'être créées -> pas besoin de try /
    // catch
    std::tie(n_x, x) = _data3d.get_x();
    std::tie(n_y, y) = _data3d.get_y();
    std::tie(n_z, z) = _data3d.get_z(idx_data);
  }

  // Stockage des données
  // --------------------
  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_z = 0;
  std::tie(idx_x, idx_y, idx_z) = store_map(n_x, x, n_y, y, n_x * n_y, z);

  auto label = _data3d.get_labels(0);
  plt::named_raised_contourf(
      label, _data_store[idx_x], _data_store[idx_y], _data_store[idx_z],
      make_tuple("xscale", "log", "yscale", "log", "zscale", "lin", "cmap",
                 _current_cmap));

  if (_store_map) {
    _data_store.erase(_data_store.begin() + idx_x, _data_store.end());
  }

  plt::xlabel("XXXXX");
  plt::ylabel("YYYYY");
  plt::zlabel(label);
}

void MainWindow::plot_surface_curve() {
  _last_action = &MainWindow::plot_surface_curve;

  if (_clear_before_drawing) {
    clear();
  }

  static std::vector<std::vector<double>> x, y, z;
  if (x.empty()) {
    for (double i = -5; i <= 5; i += 0.25) {
      std::vector<double> x_row, y_row, z_row;
      for (double j = -5; j <= 5; j += 0.25) {
        x_row.push_back(i * 2);
        y_row.push_back(j);
        z_row.push_back(std::sin(std::hypot(i, j)));
      }
      x.push_back(x_row);
      y.push_back(y_row);
      z.push_back(z_row);
    }
  }

  plt::plot_surface(x, y, z,
                    make_tuple("alpha", _scatter_alpha, "cmap", _current_cmap));

  static std::vector<double> xp, yp, zp;
  auto middle = static_cast<unsigned long>(x[0].size() / 2);
  if (xp.empty()) {
    for (auto i = 0; i <= x[0].size(); ++i) {
      xp.push_back(x[middle][i]);
      yp.push_back(y[middle][i]);
      zp.push_back(z[middle][i]);
    }
  }
  plt::plot3D(xp, yp, zp, "r-", make_tuple("linewidth", _linewidth));

  plt::set_alpha(0.7);
}

void MainWindow::plot3d() {
  _last_action = &MainWindow::plot3d;

  if (_clear_before_drawing) {
    clear();
  }

  static std::vector<double> x, y, z;
  if (x.size() == 0) {
    for (double i = -5; i <= 5; i += 0.25) {
      x.push_back(i);
      y.push_back(i);
      z.push_back(std::sin(std::hypot(i, i)));
    }
  }
  plt::plot3D(x, y, z, "r-");
}

void MainWindow::plot3d_cst() {
  _last_action = &MainWindow::plot3d_cst;

  if (_clear_before_drawing) {
    clear();
  }

  static std::vector<double> x, y, z;
  if (x.empty()) {
    for (double i = -5; i <= 5; i += 0.25) {
      x.push_back(i);
      y.push_back(std::sin(std::hypot(i, i)));
    }
    z.push_back(2.);
  }
  plt::plot3D(x, y, z);
}

void MainWindow::quiver() {
  _last_action = &MainWindow::quiver;
  this->generic_quiver(false);
}

void MainWindow::colored_quiver() {
  _last_action = &MainWindow::colored_quiver;
  this->generic_quiver(true);
}

void MainWindow::generic_quiver(const bool color) {
  if (_clear_before_drawing) {
    clear();
  }

  vector<double> x;
  vector<double> y;
  vector<double> u;
  vector<double> v;
  vector<double> mag;

  x = _dataVec.get_x();
  if (x.empty()) {
    _dataVec.init(_default_vec_stepx, _default_vec_stepy);
    x = _dataVec.get_x();
  }
  y = _dataVec.get_y();
  u = _dataVec.get_u();
  v = _dataVec.get_v();
  mag = _dataVec.get_mag();

  unsigned long idx_x = 0;
  unsigned long idx_y = 0;
  unsigned long idx_u = 0;
  unsigned long idx_v = 0;
  unsigned long idx_m = 0;
  std::tie(idx_x, idx_y, idx_u, idx_v, idx_m) = store_vec(x, y, u, v, mag);

  if (!color) {
    plt::quiver(_data_store[idx_x], _data_store[idx_y], _data_store[idx_u],
                _data_store[idx_v]);
  } else {
    plt::quiver(_data_store[idx_x], _data_store[idx_y], _data_store[idx_u],
                _data_store[idx_v], _data_store[idx_m], make_tuple("cmap", _current_cmap));
  }
}

void MainWindow::nquiver_1d() {
  _last_action = &MainWindow::nquiver_1d;

  if (_clear_before_drawing) {
    clear();
  }

  auto x = _dataVec.get_x();
  if (x.empty()) {
    _dataVec.init(_default_vec_stepx, _default_vec_stepy);
    x = _dataVec.get_x();
  }
  auto y = _dataVec.get_y();
  auto u = _dataVec.get_u();
  auto v = _dataVec.get_v();

  plt::nquiver(x, y, u, v, make_tuple("draw_colorbar", false, "cmap", _current_cmap));
}

void MainWindow::nquiver_2d() {
  _last_action = &MainWindow::nquiver_2d;

  if (_clear_before_drawing) {
    clear();
  }

  auto x = _dataVec.get_x_as_2d();
  if (x.empty()) {
    _dataVec.init(_default_vec_stepx, _default_vec_stepy);
    x = _dataVec.get_x_as_2d();
  }
  auto y = _dataVec.get_y_as_2d();
  auto u = _dataVec.get_u_as_2d();
  auto v = _dataVec.get_v_as_2d();

  plt::nquiver(x, y, u, v, make_tuple("cmap", _current_cmap));
}

void MainWindow::streamplot() {
  _last_action = &MainWindow::streamplot;
  this->generic_streamplot(false);
}

void MainWindow::colored_streamplot() {
  _last_action = &MainWindow::streamplot;
  this->generic_streamplot(true);
}

void MainWindow::generic_streamplot(const bool color) {

  if (_clear_before_drawing) {
    clear();
  }

  auto x = _dataVec.get_bx();
  if (x.empty()) {
    _dataVec.init(_default_vec_stepx, _default_vec_stepy);
    x = _dataVec.get_bx();
  }
  auto y = _dataVec.get_by();
  auto u = _dataVec.get_u_as_2d();
  auto v = _dataVec.get_v_as_2d();
  auto mag = _dataVec.get_mag_as_2d();

  vector<vector<double>> lw = mag;
  auto max_mag = _dataVec.get_max_mag();
  for (auto &&ey : lw) {
    for (auto &&ex : ey) {
      ex = (5 * ex / max_mag);
    }
  }

  if (!color) {
    plt::streamplot(x, y, u, v);
  } else {
    plt::streamplot(x, y, u, v, make_tuple("color", mag, "linewidth", lw));
  }

  //  plt::scatter(_dataVec.get_u(), _dataVec.get_v(), '+');
}

void MainWindow::nstreamplot_1d() {
  _last_action = &MainWindow::nstreamplot_1d;

  if (_clear_before_drawing) {
    clear();
  }

  auto x = _dataVec.get_x();
  if (x.empty()) {
    _dataVec.init(_default_vec_stepx, _default_vec_stepy);
    x = _dataVec.get_x();
  }
  auto y = _dataVec.get_y();
  auto u = _dataVec.get_u();
  auto v = _dataVec.get_v();

  plt::nstreamplot(x, y, u, v, _dataVec.get_nx(), _dataVec.get_ny(),
                   make_tuple("lw_mag", true, "cmap", _current_cmap));
}

void MainWindow::nstreamplot_2d() {
  _last_action = &MainWindow::nstreamplot_2d;

  if (_clear_before_drawing) {
    clear();
  }

  auto x = _dataVec.get_x_as_2d();
  if (x.empty()) {
    _dataVec.init(_default_vec_stepx, _default_vec_stepy);
    x = _dataVec.get_x_as_2d();
  }
  auto y = _dataVec.get_y_as_2d();
  auto u = _dataVec.get_u_as_2d();
  auto v = _dataVec.get_v_as_2d();

  plt::nstreamplot(x, y, u, v, make_tuple("cmap", _current_cmap));
}

void MainWindow::clear() {
  update_data_storage();
  plt::clf();
}

void MainWindow::subplots_adjust() {
  _last_action = &MainWindow::subplots_adjust;
  plt::subplots_adjust(_subplots_params);
}

void MainWindow::subplots_reset() {
  _last_action = &MainWindow::subplots_adjust;

  _subplots_params = _default_subplots_params;
  _execute_subplots = false;
  for (auto entry : _subplots_spinbox) {
    entry.second->setValue(_subplots_params[entry.first]);
  }
  _execute_subplots = true;
  plt::subplots_adjust(_subplots_params);
}

void MainWindow::change_cmap(const QString &text) {
  _current_cmap = text.toStdString();
  redo_last_action();
}

void MainWindow::clear_before_drawing(int state) {
  _clear_before_drawing = (state == Qt::Checked);
}

void MainWindow::change_store_map(int state) {
  _store_map = (state == Qt::Checked);
}

void MainWindow::change_linewidth_for_map(double value) {
  _linewidth = value;
  redo_last_action();
}

void MainWindow::change_n_levels_for_map(int value) {
  _n_levels = value;
  redo_last_action();
}

void MainWindow::change_scatter_spec_marker(const QString &text) {
  _scatter_spec_marker = text.toStdString();
  redo_last_action();
}

void MainWindow::change_scatter_spec_color(const QString &text) {
  _scatter_spec_color = text.toStdString();
  redo_last_action();
}

void MainWindow::change_scatter_spec_size(double value) {
  _scatter_spec_size = value;
  redo_last_action();
}

void MainWindow::change_scatter_gen_alpha(double value) {
  _scatter_alpha = value;
  redo_last_action();
}

void MainWindow::toggle_scatter_spec(bool checked) {
  _use_scatter_spec = checked;
  redo_last_action();
}

void MainWindow::toggle_scatter_gen(bool checked) {
  _use_scatter_spec = checked;
  redo_last_action();
}

void MainWindow::change_subplots_adjust_left(double value) {
  generic_change_subplots_adjust("left", value);
}

void MainWindow::change_subplots_adjust_right(double value) {
  generic_change_subplots_adjust("right", value);
}

void MainWindow::change_subplots_adjust_top(double value) {
  generic_change_subplots_adjust("top", value);
}

void MainWindow::change_subplots_adjust_bottom(double value) {
  generic_change_subplots_adjust("bottom", value);
}

void MainWindow::change_subplots_adjust_wspace(double value) {
  generic_change_subplots_adjust("wspace", value);
}

void MainWindow::change_subplots_adjust_hspace(double value) {
  generic_change_subplots_adjust("hspace", value);
}

void MainWindow::generic_change_subplots_adjust(const string &side,
                                                double value) {
  _subplots_params[side] = value;
  if (_execute_subplots) {
    redo_last_action();
  }
}

void MainWindow::createActions() {

  QMenu *file_menu = menuBar()->addMenu(tr("&File"));
  auto file_tool_bar = new QToolBar(tr("File"));
  addToolBar(Qt::TopToolBarArea, file_tool_bar);

  auto exit_icon = QIcon::fromTheme("application-exit");
  auto exit_act = new QAction(exit_icon, tr("Exit"), this);
  exit_act->setShortcuts(QKeySequence::Quit);
  exit_act->setStatusTip(tr("Exit the application"));
  connect(exit_act, &QAction::triggered, this, &QWidget::close);
  file_menu->addAction(exit_act);
  file_tool_bar->addAction(exit_act);

  auto clf_icon = QIcon::fromTheme("plt", QIcon(":/images/plt_clear.png"));
  auto clf_act = new QAction(clf_icon, tr("Clear"), this);
  clf_act->setStatusTip(tr("Clear Figure"));
  connect(clf_act, &QAction::triggered, this, &MainWindow::clear);
  file_menu->addAction(clf_act);
  file_tool_bar->addAction(clf_act);

  auto redo_icon = QIcon::fromTheme(
      "plt", QIcon(":/images/edit-redo-symbolic-rtl.symbolic.png"));
  auto redo_act = new QAction(redo_icon, tr("Redo"), this);
  redo_act->setStatusTip(tr("Exécution de la dernière action"));
  connect(redo_act, &QAction::triggered, this, &MainWindow::redo_last_action);
  file_menu->addAction(redo_act);
  file_tool_bar->addAction(redo_act);

  auto new_figure_icon =
      QIcon::fromTheme("plt", QIcon(":/images/new_figure.png"));
  auto new_figure_act = new QAction(new_figure_icon, tr("New Fig"), this);
  new_figure_act->setStatusTip(tr("Création d'une nouvelle figure"));
  connect(new_figure_act, &QAction::triggered, this, &MainWindow::new_figure);
  file_menu->addAction(new_figure_act);
  file_tool_bar->addAction(new_figure_act);

  auto close_figure_icon =
      QIcon::fromTheme("plt", QIcon(":/images/close_figure.png"));
  auto close_figure_act = new QAction(close_figure_icon, tr("Close Fig"), this);
  close_figure_act->setStatusTip(tr("Destruction de la première figure créée"));
  connect(close_figure_act, &QAction::triggered, this,
          &MainWindow::close_figure);
  file_menu->addAction(close_figure_act);
  file_tool_bar->addAction(close_figure_act);

  auto subplots_icon = QIcon::fromTheme("plt", QIcon(":/images/subplots.png"));
  auto subplots_act = new QAction(subplots_icon, tr("Ajustements"), this);
  subplots_act->setStatusTip(tr("Configure subplots"));
  connect(subplots_act, &QAction::triggered, this,
          &MainWindow::subplots_adjust);
  file_menu->addAction(subplots_act);
  file_tool_bar->addAction(subplots_act);

  // ------------------------------------------
  QMenu *plot_menu = menuBar()->addMenu(tr("plt"));
  auto plot_tool_bar = new QToolBar(tr("plt"));
  addToolBar(Qt::RightToolBarArea, plot_tool_bar);

  auto all_act = new QAction(tr("f(x)"), this);
  all_act->setStatusTip(tr("Plot all graphs f(x) = ..."));
  connect(all_act, &QAction::triggered, this, &MainWindow::plot_all);
  plot_menu->addAction(all_act);
  plot_tool_bar->addAction(all_act);

  auto all_and_destroy_act = new QAction(tr("n : f(x) & ~"), this);
  all_and_destroy_act->setStatusTip(
      tr("Plot all graphs and delete data using n calls of named_plot"));
  connect(all_and_destroy_act, &QAction::triggered, this,
          &MainWindow::plot_all_and_delete);
  plot_menu->addAction(all_and_destroy_act);
  plot_tool_bar->addAction(all_and_destroy_act);

  auto all_and_destroy_one_act = new QAction(tr("1 : f(x) & ~"), this);
  all_and_destroy_one_act->setStatusTip(
      tr("Plot all graphs and delete data using 1 call of named_plot"));
  connect(all_and_destroy_one_act, &QAction::triggered, this,
          &MainWindow::plot_all_and_delete_one_call);
  plot_menu->addAction(all_and_destroy_one_act);
  plot_tool_bar->addAction(all_and_destroy_one_act);

  auto nan_points_function_act = new QAction(tr("f(x) with NaN"), this);
  nan_points_function_act->setStatusTip(tr("Plot functions with Nan values"));
  connect(nan_points_function_act, &QAction::triggered, this,
          &MainWindow::plot_nan_points);
  plot_menu->addAction(nan_points_function_act);
  plot_tool_bar->addAction(nan_points_function_act);

  // ------------------------------------------
  QMenu *misc_menu = menuBar()->addMenu(tr("misc"));
  auto misc_tool_bar = new QToolBar(tr("misc"));
  addToolBar(Qt::RightToolBarArea, misc_tool_bar);

  auto load_image_act = new QAction(tr("Img"), this);
  load_image_act->setStatusTip(tr("Load a png image"));
  connect(load_image_act, &QAction::triggered, this, &MainWindow::load_image);
  misc_menu->addAction(load_image_act);
  misc_tool_bar->addAction(load_image_act);

  auto scatter_icon = QIcon::fromTheme("plt", QIcon(":/images/scatter.png"));
  auto scatter_act = new QAction(scatter_icon, tr("Scatter"), this);
  scatter_act->setStatusTip(tr("Display points"));
  connect(scatter_act, &QAction::triggered, this, &MainWindow::scatter);
  misc_menu->addAction(scatter_act);
  misc_tool_bar->addAction(scatter_act);

  auto quiver_act = new QAction(tr("quiver"), this);
  quiver_act->setStatusTip(tr("Display quiver"));
  connect(quiver_act, &QAction::triggered, this, &MainWindow::quiver);
  misc_menu->addAction(quiver_act);
  misc_tool_bar->addAction(quiver_act);
  auto cquiver_act = new QAction(tr("cquiver"), this);
  cquiver_act->setStatusTip(tr("Display colored quiver"));
  connect(cquiver_act, &QAction::triggered, this, &MainWindow::colored_quiver);
  misc_menu->addAction(cquiver_act);
  misc_tool_bar->addAction(cquiver_act);
  auto nquiver_1d_act = new QAction(tr("nquiver 1D"), this);
  nquiver_1d_act->setStatusTip(tr("Display nquiver with 1D data"));
  connect(nquiver_1d_act, &QAction::triggered, this, &MainWindow::nquiver_1d);
  misc_menu->addAction(nquiver_1d_act);
  misc_tool_bar->addAction(nquiver_1d_act);
  auto nquiver_2d_act = new QAction(tr("nquiver 2D"), this);
  nquiver_2d_act->setStatusTip(tr("Display nquiver with 2D data"));
  connect(nquiver_2d_act, &QAction::triggered, this, &MainWindow::nquiver_2d);
  misc_menu->addAction(nquiver_2d_act);
  misc_tool_bar->addAction(nquiver_2d_act);

  auto streamplot_act = new QAction(tr("streamplot"), this);
  streamplot_act->setStatusTip(tr("Display streamlines"));
  connect(streamplot_act, &QAction::triggered, this, &MainWindow::streamplot);
  misc_menu->addAction(streamplot_act);
  misc_tool_bar->addAction(streamplot_act);
  auto cstreamplot_act = new QAction(tr("cstreamplot"), this);
  cstreamplot_act->setStatusTip(tr("Display colored streamlines"));
  connect(cstreamplot_act, &QAction::triggered, this,
          &MainWindow::colored_streamplot);
  misc_menu->addAction(cstreamplot_act);
  misc_tool_bar->addAction(cstreamplot_act);
  auto nstreamplot_1d_act = new QAction(tr("nstreamplot 1D"), this);
  nstreamplot_1d_act->setStatusTip(tr("Display nstreamplot with 1D data"));
  connect(nstreamplot_1d_act, &QAction::triggered, this,
          &MainWindow::nstreamplot_1d);
  misc_menu->addAction(nstreamplot_1d_act);
  misc_tool_bar->addAction(nstreamplot_1d_act);
  auto nstreamplot_2d_act = new QAction(tr("nstreamplot 2D"), this);
  nstreamplot_2d_act->setStatusTip(tr("Display nstreamplot with 2D data"));
  connect(nstreamplot_2d_act, &QAction::triggered, this,
          &MainWindow::nstreamplot_2d);
  misc_menu->addAction(nstreamplot_2d_act);
  misc_tool_bar->addAction(nstreamplot_2d_act);

  // ------------------------------------------
  QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

  auto about_act = new QAction(tr("Help"), this);
  about_act->setStatusTip(tr("Show the application's About box"));
  connect(about_act, &QAction::triggered, this, &MainWindow::about);
  helpMenu->addAction(about_act);

  auto about_qt_act = new QAction(tr("About &Qt"), this);
  about_qt_act->setStatusTip(tr("Show the Qt library's About box"));
  connect(about_qt_act, &QAction::triggered, qApp, &QApplication::aboutQt);
  helpMenu->addAction(about_qt_act);
}

void MainWindow::createStatusBar() { statusBar()->showMessage(tr("Ready")); }

void MainWindow::createMiscWidgets() {
  // --- Pour tout le monde
  auto misc_gb = new QGroupBox(QString::fromUtf8("Divers"));
  misc_gb->setObjectName("miscellanous_groupbox");
  auto always_clear_cb = new QCheckBox(QString::fromUtf8("Clear before drawing"));
  always_clear_cb->setObjectName("clear_before_drawing_checkbox");
  always_clear_cb->setChecked(true);
  connect(always_clear_cb, &QCheckBox::stateChanged, this,
          &MainWindow::clear_before_drawing);

  auto misc_layout = new QGridLayout;
  misc_layout->addWidget(always_clear_cb, 1, 0);
  misc_gb->setLayout(misc_layout);

  _layout->addWidget(misc_gb);
}

void MainWindow::create3DWidgets() {
  // ------------------------------------------
  // Plot 3D
  QMenu *p3d_menu = menuBar()->addMenu(QString::fromUtf8("plot_3d"));
  auto p3d_tool_bar = new QToolBar(QString::fromUtf8("plot_3d"));
  addToolBar(Qt::RightToolBarArea, p3d_tool_bar);

  auto surface_act = new QAction(QString::fromUtf8("Surf"), this);
  surface_act->setObjectName("p3d_surface_action");
  surface_act->setStatusTip(tr("Plot a 3D surface"));
  connect(surface_act, &QAction::triggered, this, &MainWindow::plot_surface);
  p3d_menu->addAction(surface_act);
  p3d_tool_bar->addAction(surface_act);

  auto surface_curve_act = new QAction(QString::fromUtf8("SurCur"), this);
  surface_curve_act->setObjectName("p3d_surface_curve_action");
  surface_curve_act->setStatusTip(tr("Plot a 3D surface"));
  connect(surface_curve_act, &QAction::triggered, this,
          &MainWindow::plot_surface_curve);
  p3d_menu->addAction(surface_curve_act);
  p3d_tool_bar->addAction(surface_curve_act);

  auto raised_contourf_act = new QAction(QString::fromUtf8("Raised"), this);
  raised_contourf_act->setObjectName("rcontour_action");
  raised_contourf_act->setStatusTip(tr("Raised Contour"));
  connect(raised_contourf_act, &QAction::triggered, this,
          &MainWindow::raised_contourf);
  p3d_menu->addAction(raised_contourf_act);
  p3d_tool_bar->addAction(raised_contourf_act);

  auto plot_3d_act = new QAction(QString::fromUtf8("P3D"), this);
  plot_3d_act->setObjectName("p3d_curve_action");
  plot_3d_act->setStatusTip(tr("Plot a 3D curve"));
  connect(plot_3d_act, &QAction::triggered, this, &MainWindow::plot3d);
  p3d_menu->addAction(plot_3d_act);
  p3d_tool_bar->addAction(plot_3d_act);

  auto plot_3d_cst_act = new QAction(QString::fromUtf8("P3D Cst"), this);
  plot_3d_cst_act->setObjectName("p3d_cst_curve_action");
  plot_3d_cst_act->setStatusTip(tr("Plot a 3D constant curve"));
  connect(plot_3d_cst_act, &QAction::triggered, this, &MainWindow::plot3d_cst);
  p3d_menu->addAction(plot_3d_cst_act);
  p3d_tool_bar->addAction(plot_3d_cst_act);
}

void MainWindow::createMapWidgets() {

  // ------------------------------------------
  // Map
  QMenu *map_menu = menuBar()->addMenu(QString::fromUtf8("map"));
  auto map_tool_bar = new QToolBar(QString::fromUtf8("map"));
  addToolBar(Qt::RightToolBarArea, map_tool_bar);

  auto contourf_lin_act = new QAction(QString::fromUtf8("2D Lin"), this);
  contourf_lin_act->setObjectName("lin_contourf_action");
  contourf_lin_act->setStatusTip(
      tr("Plot a contourf and contour using linear mapping"));
  connect(contourf_lin_act, &QAction::triggered, this,
          &MainWindow::contourf_lin);
  map_menu->addAction(contourf_lin_act);
  map_tool_bar->addAction(contourf_lin_act);

  auto contourf_log_act = new QAction(QString::fromUtf8("2D Log"), this);
  contourf_log_act->setObjectName("log_contourf_action");
  contourf_log_act->setStatusTip(
      tr("Plot a contourf and contour using logarithmic mapping"));
  connect(contourf_log_act, &QAction::triggered, this,
          &MainWindow::contourf_log);
  map_menu->addAction(contourf_log_act);
  map_tool_bar->addAction(contourf_log_act);

  auto contour_lin_act = new QAction(QString::fromUtf8("Iso 2D Lin"), this);
  contour_lin_act->setObjectName("lin_contour_action");
  contour_lin_act->setStatusTip(tr("Plot a contour using linear mapping"));
  connect(contour_lin_act, &QAction::triggered, this, &MainWindow::contour_lin);
  map_menu->addAction(contour_lin_act);
  map_tool_bar->addAction(contour_lin_act);

  auto contour_log_act = new QAction(QString::fromUtf8("Iso 2D Log"), this);
  contour_log_act->setObjectName("log_contour_action");
  contourf_log_act->setStatusTip(
      tr("Plot a contour using logarithmic mapping"));
  connect(contour_log_act, &QAction::triggered, this, &MainWindow::contour_log);
  map_menu->addAction(contour_log_act);
  map_tool_bar->addAction(contour_log_act);

  auto tricontourf_lin_act = new QAction(QString::fromUtf8("Tri 2D Lin"), this);
  tricontourf_lin_act->setObjectName("lin_contourf_action");
  tricontourf_lin_act->setStatusTip(
      tr("Plot a tricontourf and tricontour using linear mapping"));
  connect(tricontourf_lin_act, &QAction::triggered, this,
          &MainWindow::tricontourf_lin);
  map_menu->addAction(tricontourf_lin_act);
  map_tool_bar->addAction(tricontourf_lin_act);

  auto tricontourf_log_act = new QAction(QString::fromUtf8("Tri 2D Log"), this);
  tricontourf_log_act->setObjectName("log_contourf_action");
  tricontourf_log_act->setStatusTip(
      tr("Plot a tricontourf and tricontour using logarithmic mapping"));
  connect(tricontourf_log_act, &QAction::triggered, this,
          &MainWindow::tricontourf_log);
  map_menu->addAction(tricontourf_log_act);
  map_tool_bar->addAction(tricontourf_log_act);

  auto tricontour_lin_act = new QAction(QString::fromUtf8("TriIso 2D Lin"), this);
  tricontour_lin_act->setObjectName("lin_contour_action");
  tricontour_lin_act->setStatusTip(
      tr("Plot a tricontour using linear mapping"));
  connect(tricontour_lin_act, &QAction::triggered, this,
          &MainWindow::tricontour_lin);
  map_menu->addAction(tricontour_lin_act);
  map_tool_bar->addAction(tricontour_lin_act);

  auto tricontour_log_act = new QAction(QString::fromUtf8("TriIso 2D Log"), this);
  tricontour_log_act->setObjectName("log_contour_action");
  tricontourf_log_act->setStatusTip(
      tr("Plot a tricontour using logarithmic mapping"));
  connect(tricontour_log_act, &QAction::triggered, this,
          &MainWindow::tricontour_log);
  map_menu->addAction(tricontour_log_act);
  map_tool_bar->addAction(tricontour_log_act);

  // --- 3D Plan et Iso
  auto map_gb = new QGroupBox(tr("3D Plan et Iso"));
  map_gb->setObjectName("map_groupbox");

  auto no_storage_cb = new QCheckBox(QString::fromUtf8("Pas de stockage"));
  no_storage_cb->setObjectName("no_storage_checkbox");
  connect(no_storage_cb, &QCheckBox::stateChanged, this,
          &MainWindow::change_store_map);

  auto linewidth_lb = new QLabel(QString::fromUtf8("Épaisseur ligne pour iso : "));
  auto linewidth_sb = new QDoubleSpinBox;
  linewidth_sb->setObjectName("linewidth_doublespinbox");
  linewidth_sb->setRange(0, 40);
  linewidth_sb->setValue(_linewidth);
  linewidth_sb->setSingleStep(0.2);
  connect(linewidth_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_linewidth_for_map);

  auto n_levels_lb = new QLabel(QString::fromUtf8("Nombre de niveaux de couleurs : "));
  auto n_levels_sb = new QSpinBox;
  n_levels_sb->setObjectName("n_levels_spinbox");
  n_levels_sb->setRange(1, 200);
  n_levels_sb->setValue(_n_levels);
  n_levels_sb->setSingleStep(1);
  connect(n_levels_sb,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &MainWindow::change_n_levels_for_map);

  auto cmap_lb = new QLabel(QString::fromUtf8("Palette : "));
  auto cmap_combo = new QComboBox;
  cmap_combo->setObjectName("cmaps_combobox");
  for (auto &item : _cmaps) {
    cmap_combo->addItem(QString::fromStdString(item));
  }
  connect(
      cmap_combo,
      static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::activated),
      this, &MainWindow::change_cmap);
  //    connect(cmap_combo, static_cast<void(QComboBox::*)(const
  //    QString&)>(&QComboBox::activated),
  //            [=](const QString& text) {
  //        _current_cmap=text.toStdString();
  //    });

  // -----
  auto map_layout = new QGridLayout;
  map_layout->addWidget(cmap_lb, 0, 0);
  map_layout->addWidget(cmap_combo, 0, 1);
  map_layout->addWidget(no_storage_cb, 1, 0, 1, 2);
  map_layout->addWidget(linewidth_lb, 2, 0);
  map_layout->addWidget(linewidth_sb, 2, 1);
  map_layout->addWidget(n_levels_lb, 3, 0);
  map_layout->addWidget(n_levels_sb, 3, 1);
  map_gb->setLayout(map_layout);

  // -----
  _layout->addWidget(map_gb);
}

void MainWindow::createScatterWidgets() {
  // ------------------------------------------
  // Nuage de points
  auto scatter_gb = new QGroupBox(tr("Nuage de points"));
  scatter_gb->setObjectName("scatter_groupbox");

  auto scatter_type_cb = new QGroupBox;
  scatter_type_cb->setObjectName("scatter_type_groupe_box");
  auto spec_rb = new QRadioButton(tr("Spécialisation"));
  auto gen_rb = new QRadioButton(tr("Générique"));
  spec_rb->setChecked(true);
  _use_scatter_spec = true;
  connect(spec_rb,
          static_cast<void (QRadioButton::*)(bool)>(&QRadioButton::toggled),
          this, &MainWindow::toggle_scatter_spec);
  connect(gen_rb,
          static_cast<void (QRadioButton::*)(bool)>(&QRadioButton::toggled),
          this, &MainWindow::toggle_scatter_gen);

  vector<string> _str_markers = {".", "1", "2", "3", "4", "+", "x", "|",
                                 "_", "o", "v", "^", "<", ">", "8", "s",
                                 "X", "P", "d", "D", "H", "h", "*", "p"};
  vector<int> _int_markers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
  vector<string> _tableau_colors = {"black", "blue",   "orange", "green",
                                    "red",   "purple", "brown",  "pink",
                                    "gray",  "olive",  "cyan"};

  // Marker
  auto marker_lb = new QLabel(tr("Marqueur :"));
  auto marker_cbox = new QComboBox;
  for (auto item : _str_markers) {
    marker_cbox->addItem(QString::fromStdString(item));
  }
  marker_cbox->setCurrentIndex(
      marker_cbox->findText(QString::fromStdString(_scatter_spec_marker)));
  connect(
      marker_cbox,
      static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::activated),
      this, &MainWindow::change_scatter_spec_marker);
  // Color
  auto color_lb = new QLabel(tr("Couleur :"));
  auto color_cbox = new QComboBox;
  for (auto item : _tableau_colors) {
    color_cbox->addItem(QString::fromStdString(item));
  }
  color_cbox->setCurrentIndex(
      color_cbox->findText(QString::fromStdString(_scatter_spec_color)));
  connect(
      color_cbox,
      static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::activated),
      this, &MainWindow::change_scatter_spec_color);
  // Size
  auto size_sb_lb = new QLabel(tr("Taille :"));
  auto size_sb = new QDoubleSpinBox;
  size_sb->setRange(0., 100.);
  size_sb->setSingleStep(2.);
  size_sb->setValue(_scatter_spec_size);
  connect(size_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_scatter_spec_size);

  auto gen_marker = new QLabel(tr("Marqueur : Voir Spécialisation"));
  auto gen_color = new QLabel(tr("Couleur : Utilisation de z"));
  auto gen_size = new QLabel(tr("Taille : Voir Spécialisation"));
  auto gen_cmap = new QLabel(tr("Palette : Voir Map"));
  auto alpha_sb_lb = new QLabel(tr("Transparence :"));
  auto alpha_sb = new QDoubleSpinBox;
  alpha_sb->setRange(0., 1.);
  alpha_sb->setSingleStep(0.1);
  alpha_sb->setValue(_scatter_alpha);
  connect(alpha_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_scatter_gen_alpha);

  // -----
  auto scatter_type_layout = new QHBoxLayout;
  scatter_type_layout->addWidget(spec_rb);
  scatter_type_layout->addWidget(gen_rb);

  auto size_layout = new QHBoxLayout;
  size_layout->addWidget(size_sb_lb);
  size_layout->addWidget(size_sb);
  auto color_layout = new QHBoxLayout;
  color_layout->addWidget(color_lb);
  color_layout->addWidget(color_cbox);
  auto marker_layout = new QHBoxLayout;
  marker_layout->addWidget(marker_lb);
  marker_layout->addWidget(marker_cbox);
  auto alpha_layout = new QHBoxLayout;
  alpha_layout->addWidget(alpha_sb_lb);
  alpha_layout->addWidget(alpha_sb);

  auto scatter_layout = new QGridLayout;
  scatter_layout->addLayout(scatter_type_layout, 0, 0, 1, 2);
  scatter_layout->addLayout(marker_layout, 1, 0);
  scatter_layout->addWidget(gen_marker, 1, 1);
  scatter_layout->addLayout(color_layout, 2, 0);
  scatter_layout->addWidget(gen_color, 2, 1);
  scatter_layout->addLayout(size_layout, 3, 0);
  scatter_layout->addWidget(gen_size, 3, 1);
  scatter_layout->addLayout(alpha_layout, 4, 1);
  scatter_gb->setLayout(scatter_layout);

  // -----
  _layout->addWidget(scatter_gb);
}

void MainWindow::print_geometry() {
  cout << "————————————————————"
       << "\n";
  cout << "X, Y = " << this->x() << ", " << this->y() << "\n";
  cout << "W, H = " << this->width() << ", " << this->height() << "\n";
  QRect frame = this->frameGeometry();
  cout << "Frame : " << frame.left() << ", " << frame.right() << "\t"
       << frame.top() << ", " << frame.bottom() << "\n";
  QPoint pos = this->pos();
  cout << "Pos : " << pos.x() << ", " << pos.y() << "\n";
  cout << "————————————————————"
       << "\n";
  cout << "\n";
}

void MainWindow::createSubplotsWidgets() {
  // ------------------------------------------
  // Ajustements des subplots dans une figure
  auto subplots_gb = new QGroupBox(tr("Ajustement des subplots"));
  subplots_gb->setObjectName("subplots_groupbox");

  auto subplots_borders_gb = new QGroupBox(tr("Bordures"));
  // Borders
  auto left_sb_lb = new QLabel(tr("Gauche :"));
  auto left_sb = _subplots_spinbox["left"] = new QDoubleSpinBox;
  left_sb->setRange(0., 1.);
  left_sb->setSingleStep(0.005);
  left_sb->setDecimals(3);
  left_sb->setValue(_subplots_params["left"]);
  connect(left_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_subplots_adjust_left);

  auto right_sb_lb = new QLabel(tr("Droite :"));
  auto right_sb = _subplots_spinbox["right"] = new QDoubleSpinBox;
  right_sb->setRange(0., 1.);
  right_sb->setSingleStep(0.005);
  right_sb->setDecimals(3);
  right_sb->setValue(_subplots_params["right"]);
  connect(right_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_subplots_adjust_right);

  auto top_sb_lb = new QLabel(tr("Haut :"));
  auto top_sb = _subplots_spinbox["top"] = new QDoubleSpinBox;
  top_sb->setRange(0., 1.);
  top_sb->setSingleStep(0.005);
  top_sb->setDecimals(3);
  top_sb->setValue(_subplots_params["top"]);
  connect(top_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_subplots_adjust_top);

  auto bottom_sb_lb = new QLabel(tr("Bas :"));
  auto bottom_sb = _subplots_spinbox["bottom"] = new QDoubleSpinBox;
  bottom_sb->setRange(0., 1.);
  bottom_sb->setSingleStep(0.005);
  bottom_sb->setDecimals(3);
  bottom_sb->setValue(_subplots_params["bottom"]);
  connect(bottom_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_subplots_adjust_bottom);

  auto subplots_spacings_gb = new QGroupBox(tr("Espacements"));
  // Spacings
  auto wspace_sb_lb = new QLabel(tr("Horizontal :"));
  auto wspace_sb = _subplots_spinbox["wspace"] = new QDoubleSpinBox;
  wspace_sb->setRange(0., 1.);
  wspace_sb->setSingleStep(0.05);
  wspace_sb->setValue(_subplots_params["wspace"]);
  connect(wspace_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_subplots_adjust_wspace);

  auto hspace_sb_lb = new QLabel(tr("Vertical :"));
  auto hspace_sb = _subplots_spinbox["hspace"] = new QDoubleSpinBox;
  hspace_sb->setRange(0., 1.);
  hspace_sb->setSingleStep(0.05);
  hspace_sb->setValue(_subplots_params["hspace"]);
  connect(hspace_sb,
          static_cast<void (QDoubleSpinBox::*)(double)>(
              &QDoubleSpinBox::valueChanged),
          this, &MainWindow::change_subplots_adjust_hspace);

  auto reset_pb = new QPushButton("Reset");
  connect(reset_pb,
          static_cast<void (QPushButton::*)(bool)>(&QPushButton::clicked), this,
          &MainWindow::subplots_reset);

  // -----
  auto left_layout = new QHBoxLayout;
  left_layout->addWidget(left_sb_lb);
  left_layout->addWidget(left_sb);
  auto right_layout = new QHBoxLayout;
  right_layout->addWidget(right_sb_lb);
  right_layout->addWidget(right_sb);
  auto top_layout = new QHBoxLayout;
  top_layout->addWidget(top_sb_lb);
  top_layout->addWidget(top_sb);
  auto bottom_layout = new QHBoxLayout;
  bottom_layout->addWidget(bottom_sb_lb);
  bottom_layout->addWidget(bottom_sb);

  auto hspace_layout = new QHBoxLayout;
  hspace_layout->addWidget(hspace_sb_lb);
  hspace_layout->addWidget(hspace_sb);
  auto wspace_layout = new QHBoxLayout;
  wspace_layout->addWidget(wspace_sb_lb);
  wspace_layout->addWidget(wspace_sb);

  auto borders_layout = new QVBoxLayout;
  borders_layout->addLayout(left_layout);
  borders_layout->addLayout(right_layout);
  borders_layout->addLayout(top_layout);
  borders_layout->addLayout(bottom_layout);
  subplots_borders_gb->setLayout(borders_layout);

  auto spacings_layout = new QVBoxLayout;
  spacings_layout->addLayout(hspace_layout);
  spacings_layout->addLayout(wspace_layout);
  spacings_layout->addWidget(reset_pb);
  subplots_spacings_gb->setLayout(spacings_layout);

  auto subplots_layout = new QHBoxLayout;
  subplots_layout->addWidget(subplots_borders_gb);
  subplots_layout->addWidget(subplots_spacings_gb);
  subplots_gb->setLayout(subplots_layout);

  // -----
  _layout->addWidget(subplots_gb);
}

void MainWindow::set_titles_for_map(const std::string &t, const std::string &st,
                                    const std::string &tm) {
  plt::set_titles(std::move(t), 0.0, 0.95, 20, std::move(st), 0.1, 0.9, 12,
                  std::move(tm), -1.0, 0.9, 16);
}


// ----------------------------------------------------------------------------
void MainWindow::update_data_storage() {
  if (!_data_store.empty()) {
    for (auto &&curve : _data_store) {
      curve.resize(0);
    }
    _data_store.resize(0);
  }

  _data_store.reserve(_default_n_curves);
}

tuple<unsigned long, unsigned long>
MainWindow::store_network(const unsigned long n_points,
                          const unsigned long n_curves, double *x, double *y) {
  // @WARNING : cast implicit double en double
  auto idx_x = _data_store.size();
  _data_store.resize(idx_x + 1 + n_curves);

  // stockage des abscisses
  _data_store[idx_x].resize(n_points);
  copy(x, x + n_points, begin(_data_store[idx_x]));

  // stockage des ordonnées
  auto start = 0;
  for (auto idx_y = idx_x + 1; idx_y < _data_store.size(); ++idx_y) {
    _data_store[idx_y].resize(n_points);
    copy(y + start, y + start + n_points, begin(_data_store[idx_y]));
    start += n_points;
  }

  return make_tuple(idx_x, idx_x + 1);
}

std::tuple<unsigned long, unsigned long, unsigned long>
MainWindow::store_map(const unsigned long n_x, double *x,
                      const unsigned long n_y, double *y,
                      const unsigned long n_z, double *z) {

  // @WARNING : cast implicit double en double
  auto idx_x = _data_store.size();
  _data_store.resize(idx_x + 3);

  // stockage des abscisses
  _data_store[idx_x].resize(n_x);
  copy(x, x + n_x, begin(_data_store[idx_x]));

  // stockage des ordonnées
  auto idx_y = idx_x + 1;
  _data_store[idx_y].resize(n_y);
  copy(y, y + n_y, begin(_data_store[idx_y]));

  // stockage des ???
  auto idx_z = idx_y + 1;
  _data_store[idx_z].resize(n_z);
  copy(z, z + n_z, begin(_data_store[idx_z]));

  return make_tuple(idx_x, idx_y, idx_z);
}

std::tuple<unsigned long, unsigned long, unsigned long, unsigned long>
MainWindow::store_vec(vector<double> &x, vector<double> &y, vector<double> &u,
                      vector<double> &v) {

  auto idx_x = _data_store.size();
  _data_store.resize(idx_x + 4);

  // Tous les tableaux ont la même taille
  auto total = x.size();

  // Coordonnées
  _data_store[idx_x].resize(total);
  copy(x.begin(), x.end(), begin(_data_store[idx_x]));
  auto idx_y = idx_x + 1;
  _data_store[idx_y].resize(total);
  copy(y.begin(), y.end(), begin(_data_store[idx_y]));
  // Directions vecteurs
  auto idx_u = idx_y + 1;
  _data_store[idx_u].resize(total);
  copy(u.begin(), u.end(), begin(_data_store[idx_u]));
  auto idx_v = idx_u + 1;
  _data_store[idx_v].resize(total);
  copy(v.begin(), v.end(), begin(_data_store[idx_v]));

  return make_tuple(idx_x, idx_y, idx_u, idx_v);
}

std::tuple<unsigned long, unsigned long, unsigned long, unsigned long,
           unsigned long>
MainWindow::store_vec(vector<double> &x, vector<double> &y, vector<double> &u,
                      vector<double> &v, vector<double> &c) {

  auto idx_x = _data_store.size();
  _data_store.resize(idx_x + 5);

  // Tous les tableaux ont la même taille
  auto total = x.size();

  // Coordonnées
  _data_store[idx_x].resize(total);
  copy(x.begin(), x.end(), begin(_data_store[idx_x]));
  auto idx_y = idx_x + 1;
  _data_store[idx_y].resize(total);
  copy(y.begin(), y.end(), begin(_data_store[idx_y]));
  // Directions vecteurs
  auto idx_u = idx_y + 1;
  _data_store[idx_u].resize(total);
  copy(u.begin(), u.end(), begin(_data_store[idx_u]));
  auto idx_v = idx_u + 1;
  _data_store[idx_v].resize(total);
  copy(v.begin(), v.end(), begin(_data_store[idx_v]));
  auto idx_c = idx_v + 1;
  _data_store[idx_c].resize(total);
  copy(c.begin(), c.end(), begin(_data_store[idx_c]));
  return make_tuple(idx_x, idx_y, idx_u, idx_v, idx_c);
}
