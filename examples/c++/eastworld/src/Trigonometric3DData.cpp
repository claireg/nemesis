//
// Created by guilbaud on 09/10/18.
//
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

#include "../include/Trigonometric3DData.h"

using namespace std;

constexpr double pi() { return acos(-1); }

Trigonometric3DData::Trigonometric3DData(): _x(nullptr), _y(nullptr), _z(nullptr), _trix(nullptr),
_triy(nullptr), _triz(nullptr), _title("TITRE"),
    _sub_title("... SOUS TITRE..."), _title_mat("MAT"), _n_tri(0) {
}

Trigonometric3DData::~Trigonometric3DData() {
    purge();
}


void Trigonometric3DData::init() {
    _n_z = _n_x * _n_y;
    _labels.resize(0);

    this->set_x();
    this->set_y();
    this->set_z();

    this->set_tri();
}

void Trigonometric3DData::purge()
{
    delete [] _x;
    _x = nullptr;
    delete [] _y;
    _y = nullptr;
    delete [] _z;
    _z = nullptr;
}

void Trigonometric3DData::set_x() {
    _x = new double[_n_x];
    auto x_min = 10.;
    auto x_max = 45.;
    auto step = (x_max - x_min) / (_n_x-1);
    _x[0] = x_min;
    _x[_n_x-1] = x_max;
    for (unsigned long i = 1; i < _n_x-1; ++i) {
        _x[i] = _x[i-1] + step;
    }

#ifdef DATA_TRACE
    cout << __PRETTY_FUNCTION__ << '\n';
    print_x();
#endif
}

void Trigonometric3DData::set_y() {
    _y = new double[_n_y];
    auto y_min = 10.;
    auto y_max = 30.;
    auto step = (y_max - y_min) / (_n_y-1);
    _y[0] = y_min;
    _y[_n_y-1] = y_max;
    for (unsigned long i = 1; i < _n_y-1; ++i) {
        _y[i] = _y[i-1] + step;
    }
#ifdef DATA_TRACE
    cout << __PRETTY_FUNCTION__ << '\n';
    print_y();
#endif
}

void Trigonometric3DData::set_z() {

    unsigned long n = _n_z * _n_data;
    _z = new double[n];

    for(unsigned long c = 0; c < _n_data; ++c) {
        for (auto idy = 0; idy < _n_y; ++idy) {
            auto step = idy * _n_x;
            for (auto idx = 0; idx < _n_x; ++idx) {
                auto x = _x[idx];
                auto y = _y[idy];
                auto idf = idx + step;
//                double z1 = exp(pow(x, 2.) - pow(y, 2.));
//                double z2 = exp(-(pow(10 * x, 2.) - pow(10 * y, 2.)));
//                _z[idf] = fabs(2 * (z1 - z2));

//                    _z[idf] = x*x*cos(x) + y*y*sin(y);
                _z[idf] = x*x*cos(y) + y*y*sin(x);
            }
        }
    }
//    _labels.emplace_back("exp(pow(x, 2) - pow(y, 2)");
//    _labels.emplace_back("x*x*cos(y) + y*y*sin(x)");
    _labels.emplace_back("$x^2 * cos(y) + y^2*sin(x)$");

//    auto minimum = std::numeric_limits<double>::max();
//    for(auto i=0; i<_n_z; ++i) {
//        if(_z[i] < minimum) {
//            minimum = _z[i];
//        }
//    }
//    auto maximum = std::numeric_limits<double>::min();
//    for(auto i=0; i<_n_z; ++i) {
//        if(_z[i] > maximum) {
//            maximum = _z[i];
//        }
//    }
//    auto delta = maximum - minimum;
//    auto abs_min = fabs(minimum);
//    transform(_z, _z+_n_z, _z, [abs_min, delta](double v) {
//        return ((v+abs_min) / delta);
//    });
//    minima = fabs(minima);
//    transform(_z, _z+_n_z, _z, [minima](double v) {
//        return v+=minima;
//    });


#ifdef DATA_TRACE
    cout << __PRETTY_FUNCTION__ << '\n';
    print_z();
#endif
}

void Trigonometric3DData::set_tri() {
    auto n_angles = 48;
    auto n_radii = 8;
    auto min_radius = 0.25;

    vector<double> radii(n_radii);
    auto step_radii = (0.95 - min_radius) / n_radii;
    for(unsigned long i = 0; i < n_radii; i++) {
        radii[i] = min_radius + static_cast<double>(i)*step_radii;
                // np.linspace(min_radius, 0.95, n_radii);
    }
    vector<double> angles(n_angles * n_radii);
    auto step_angles = (2 * pi()) / n_angles;
    auto delta = pi() / n_angles;
    for(unsigned long i = 0; i < n_radii; i++) {
        auto current_value = step_angles*static_cast<double>(i);
        auto n_i = i*n_radii;
        for(unsigned long j=0; j<n_angles; j++) {
            auto ij = n_i + j;
            angles[ij] = current_value;
            if(static_cast<double>(j+1)*0.5 == 0.) {
                angles[ij] += delta;
            }
        }
    }

    _n_tri = angles.size();
    _trix = new double[angles.size()];
    _triy = new double[angles.size()];
    _triz = new double[angles.size()];
    auto i_radii = 0;
    for(unsigned long i=0; i<angles.size(); i++) {
        auto v_radii = radii[i_radii];
        auto v_angles = angles[i];
        _trix[i] = v_radii * cos(v_angles);
        _triy[i] = v_radii * sin(v_angles);
        _triz[i] = fabs(cos(v_radii) * cos(3*v_angles));
        if(i_radii+1 == radii.size()) {
            i_radii = 0;
        }
        else {
            i_radii++;
        }
    }
}

std::tuple<unsigned long, double*> Trigonometric3DData::get_x()
{
    if(_n_z == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(get_n_x(), _x);
}

std::tuple<unsigned long, double*> Trigonometric3DData::get_y()
{
    if(_n_z == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(get_n_y(), _y);
}

std::tuple<unsigned long, double*> Trigonometric3DData::get_z(unsigned long idx)
{
    if(_n_z == 0) {
        throw out_of_range("Aucune donnée construite. init() doit être appelée.");
    }
    if(idx < 0 || idx >= get_n_data()) {
        throw out_of_range("Il n'y a pas de données correspondant à cet indice.");
    }
    // Comme ça n'a pas de sens d'afficher plusieurs cartes dans la même boîte d'axes, on ne retourne qu'une partie
    // de _z (correspondant à un type de coloration)
    auto nz = get_n_z();
    auto * smallest_z = new double[nz];
    auto start = idx * nz;
    auto end = start + nz;
    copy(_z+start, _z+end, smallest_z);
    return std::make_tuple(nz, smallest_z);
}

std::tuple<unsigned long, double*> Trigonometric3DData::get_trix()
{
    if(_n_tri == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(_n_tri, _trix);
}

std::tuple<unsigned long, double*> Trigonometric3DData::get_triy()
{
    if(_n_tri == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(_n_tri, _triy);
}

std::tuple<unsigned long, double*> Trigonometric3DData::get_triz()
{
    if(_n_tri == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(_n_tri, _triz);
}

string& Trigonometric3DData::get_labels(unsigned long idx) {
    if(idx < 0 || idx >= _labels.size()) {
        throw out_of_range("Il n'y a pas de données correspondant à cet indice.");
    }
    return _labels[idx];
}

void Trigonometric3DData::print_x() {
    print_c_array(_n_x, _x);
}

void Trigonometric3DData::print_y() {
    print_c_array(_n_y, _y);
}

void Trigonometric3DData::print_z() {
    print_c_array(_n_z * _n_data, _z);
}


void Trigonometric3DData::print_c_array(unsigned long n, double *array_) {
    cout << "———————————————————— " << '\n';
    std::for_each(array_, array_+n, [](double v){
        cout << v << ' ';
    });
    cout << '\n';
}

