//
// Created by guilbaud on 09/10/18.
//
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

#include "../include/TrigonometricData.h"

using namespace std;

constexpr double pi() { return acos(-1); }

TrigonometricData::TrigonometricData(): _n_curves(0), _n_points(0), _x(nullptr), _y(nullptr) {
}

TrigonometricData::~TrigonometricData() {
    purge();
}


void TrigonometricData::init(const unsigned long n_points, const unsigned long n_curves) {
    _n_curves = n_curves;
    _n_points = n_points;

    if(_n_curves > _max_n_curves) {
        cerr << "/!\\ Nombre de courbes limité à " << _max_n_curves << "." << "\n";
        _n_curves = _max_n_curves;
    }

    _labels.resize(_n_curves);

    this->set_x();
    this->set_y();
}

void TrigonometricData::purge()
{
    delete [] _x;
    _x = nullptr;
    delete [] _y;
    _y = nullptr;
    _n_curves = 0;
    _n_points = 0;
}

void TrigonometricData::set_x() {
    _x = new double[_n_points];
    for (unsigned long i = 0; i < _n_points; ++i) {
        _x[i] = static_cast<double>(i);
    }
    std::transform(_x, _x+_n_points, _x, [](double v) { return 2*pi()*v / 360.; });

#ifdef DATA_TRACE
    cout << __PRETTY_FUNCTION__ << '\n';
    print_x();
#endif
}

void TrigonometricData::set_y() {

    unsigned long n = _n_curves * _n_points;
    _y = new double[n];

    for(unsigned long c = 0; c < _n_curves; ++c) {
        auto start = c * _n_points;
        auto end = start + _n_points;
        copy(_x, _x+_n_points, _y+start);
        switch(c) {
            case 0:
                _labels.at(c) = "cos(x)";
                transform(_y+start, _y+end, _y+start, [](double v) { return cos(v); });
                break;
            case 1:
                _labels.at(c) = "sin(x)";
                transform(_y+start, _y+end, _y+start, [](double v) { return sin(v); });
                break;
            case 2:
                _labels.at(c) = "acos(x)";
                transform(_y+start, _y+end, _y+start, [](double v) { return acos(v); });
                break;
            case 3:
                _labels.at(c) = "asin(x)";
                transform(_y+start, _y+end, _y+start, [](double v) { return asin(v); });
                break;
            case 4:
                _labels.at(c) = "atan(x)";
                transform(_y+start, _y+end, _y+start, [](double v) { return atan(v); });
                break;
            case 5:
                _labels.at(c) = "tanh(x)";
                transform(_y+start, _y+end, _y+start, [](double v) { return tanh(v); });
                break;
            default:
                throw runtime_error("TrigonometricData::set_y: cas par défaut. Ne devrait jamais arriver.");
        }
    }

#ifdef DATA_TRACE
    cout << __PRETTY_FUNCTION__ << '\n';
    print_x();
    print_y();
#endif
}

std::tuple<unsigned long, double*> TrigonometricData::get_x()
{
    if(_n_points == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(_n_points, _x);
}

std::tuple<unsigned long, unsigned long, double*> TrigonometricData::get_y()
{
    if(_n_points == 0) {
        throw out_of_range("Aucune donnée construite. init(np, nc) doit être appelée.");
    }
    return std::make_tuple(_n_points, _n_curves, _y);
}

vector<string>& TrigonometricData::get_labels() {
    return _labels;
}

void TrigonometricData::print_x() {
    print_c_array(_n_points, _x);
}

void TrigonometricData::print_y() {
    print_c_array(_n_points*_n_curves, _y);
}

void TrigonometricData::print_c_array(unsigned long n, double *array_) {
    cout << "———————————————————— " << '\n';
    std::for_each(array_, array_+n, [](double v){
        cout << v << ' ';
    });
    cout << '\n';
}