//
// Created by guilbaudc on 5/18/20.
//
#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>

#include "../include/VectorsData.h"

using namespace std;

constexpr double pi() { return acos(-1); }
const char *VectorsData::_sep = ", ";

VectorsData::VectorsData() : _nx(0), _ny(0), _stepx(nan("")), _stepy(nan("")) {}

void VectorsData::init(double sx, double sy) {
  _stepx = sx;
  _stepy = sy;

  this->set_data();

  _labels.resize(2);
  _labels[0] = "vecteurs";
  _labels[1] = "u=cos(x);v=sin(y)";
}

void VectorsData::set_data() {
  double end = 2 * pi();

  _nx = static_cast<unsigned long>(end / _stepx);
  _ny = static_cast<unsigned long>(end / _stepx);
  auto total = _nx * _ny;
  _x.resize(total);
  _y.resize(total);
  _u.resize(total);
  _v.resize(total);
  _mag.resize(total);

  _base_x.resize(_nx);
  for (unsigned long i = 0; i < _nx; ++i) {
    _base_x[i] = _base_x[i - 1] + _stepx;
  }
  auto posx = _x.begin();
  for (unsigned long i = 0; i < _ny; ++i) {
    std::copy(_base_x.begin(), _base_x.end(), posx);
    posx += _nx;
  }
  transform_2d(_x, _x2d);

  _base_y.resize(_ny);
  for (unsigned long i = 0; i < _nx; ++i) {
    _base_y[i] = _base_y[i - 1] + _stepy;
  }
  auto posy = _y.begin();
  for (unsigned long i = 0; i < _nx; i++) {
    auto vy = _base_y[i];
    transform(posy, posy + _nx, posy, [vy](double v) { return vy; });
    posy += _ny;
  }
  transform_2d(_y, _y2d);

  transform(_x.begin(), _x.end(), _u.begin(), [](double v) { return cos(v); });
  transform_2d(_u, _u2d);

  transform(_y.begin(), _y.end(), _v.begin(), [](double v) { return sin(v); });
  transform_2d(_v, _v2d);

  auto itx = _x.begin();
  auto ity = _y.begin();
  for (auto ith = _mag.begin(); ith != _mag.end(); ++ith, ++itx, ++ity) {
    (*ith) = hypot(*itx, *ity);
  }
  transform_2d(_mag, _mag2d);
  auto it_max = max_element(_mag.begin(), _mag.end());
  _max_mag = *it_max;
}

void VectorsData::transform_2d(const vector<double> &input,
                               vector<vector<double>> &output) const {
  unsigned long cpt_x = 0, cpt_y = 0;
  output.resize(_nx);
  for (auto &&e : output) {
    e.resize(_ny);
  }

  for (auto &e : input) {
    output[cpt_x][cpt_y] = e;
    cpt_y += 1;
    if (cpt_y == _ny) {
      cpt_x += 1;
      cpt_y = 0;
    }
  }
}

void VectorsData::print_x() {
  cout << "———————————————————— X (" << _x.size() << ") : "
       << "nx = " << _nx << " ny = " << _ny << '\n';
  std::copy(_x.begin(), _x.end(),
            std::ostream_iterator<double>(std::cout, _sep));
  cout << '\n';
}

void VectorsData::print_y() {
  cout << "———————————————————— Y (" << _y.size() << ") : "
       << "nx = " << _nx << " ny = " << _ny << '\n';
  std::copy(_y.begin(), _y.end(),
            std::ostream_iterator<double>(std::cout, _sep));
  cout << '\n';
}

void VectorsData::print_u() {
  cout << "———————————————————— U (" << _u.size() << ") : "
       << "nx = " << _nx << " ny = " << _ny << '\n';
  std::copy(_u.begin(), _u.end(),
            std::ostream_iterator<double>(std::cout, _sep));
  cout << '\n';
}

void VectorsData::print_v() {
  cout << "———————————————————— V (" << _v.size() << ") : "
       << "nx = " << _nx << " ny = " << _ny << '\n';
  std::copy(_v.begin(), _v.end(),
            std::ostream_iterator<double>(std::cout, _sep));
  cout << '\n';
}

void VectorsData::print_mag() {
  cout << "———————————————————— Mag (" << _mag.size() << ") : "
       << "nx = " << _nx << " ny = " << _ny << '\n';
  std::copy(_mag.begin(), _mag.end(),
            std::ostream_iterator<double>(std::cout, _sep));
  cout << '\n';
}