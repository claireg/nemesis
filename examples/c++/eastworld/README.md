# Eastworld

# Pré-requis

* Python 3.7
* Environnement ``Nemesis_0.1`` (avec ``PySide2``)

## But

La mainloop Qt est en C++, et appelle ``matplotlib`` avec le backend Qt5Agg.

Il existe un équivalent en python pure (``withmainloop/smallworldqt5.py``)

