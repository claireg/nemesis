//
// Created by guilbaud on 09/10/18.
//

#ifndef EASTWORLD_TRIGONOMETRIC_3D_DATA_H
#define EASTWORLD_TRIGONOMETRIC_3D_DATA_H

#include <tuple>
#include <cmath>
#include <vector>
#include <string>

class Trigonometric3DData {
public:
    Trigonometric3DData();
    ~Trigonometric3DData();

    void init();
    void purge();
    std::tuple<unsigned long, double*> get_x();
    std::tuple<unsigned long, double*> get_y();
    std::tuple<unsigned long, double*> get_z(unsigned long idx);
    std::tuple<unsigned long, double*> get_trix();
    std::tuple<unsigned long, double*> get_triy();
    std::tuple<unsigned long, double*> get_triz();
    std::string& get_labels(unsigned long idx);

    inline unsigned long get_n_x() { return _n_x; }
    inline unsigned long get_n_y() { return _n_y; }
    inline unsigned long get_n_z() { return _n_z; }
    inline unsigned long get_n_data() { return _n_data; }

    inline std::string get_title() { return _title; }
    inline std::string get_sub_title() { return _sub_title; }
    inline std::string get_title_mat() { return _title_mat; }

private:
    void set_x();
    void set_y();
    void set_z();
    void set_tri();

    void print_x();
    void print_y();
    void print_z();
    static void print_c_array(unsigned long n, double* array_);

    double* _x;
    double* _y;
    double* _z;
    double* _trix{};
    double* _triy{};
    double* _triz{};
    static constexpr unsigned long _n_data = 1;
    static constexpr unsigned long _n_x = 50;
    static constexpr unsigned long _n_y = 50;

    // Pourrait être une constexpr, mais on s'en sert pour savoir si les données ont été construites
    unsigned long _n_z{};
    unsigned long _n_tri;

    std::vector<std::string> _labels;
    std::string _title;
    std::string _sub_title;
    std::string _title_mat;
};

#endif //EASTWORLD_TRIGONOMETRIC_3D_DATA_H
