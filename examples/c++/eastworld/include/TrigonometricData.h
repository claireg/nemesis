//
// Created by guilbaud on 09/10/18.
//

#ifndef EASTWORLD_TRIGONOMETRICDATA_H
#define EASTWORLD_TRIGONOMETRICDATA_H

#include <tuple>
#include <cmath>
#include <vector>
#include <string>


class TrigonometricData {
public:
    TrigonometricData();
    ~TrigonometricData();

    void init(unsigned long n_points, unsigned long n_curves);
    void purge();
    std::tuple<unsigned long, double*> get_x();
    std::tuple<unsigned long, unsigned long, double*> get_y();
    std::vector<std::string>& get_labels();

private:
    void set_x();
    void set_y();

    void print_x();
    void print_y();
    static void print_c_array(unsigned long n, double* array_);

    double* _x;
    double* _y;
    // absisses communs à toutes les courbes
    unsigned long _n_curves;
    unsigned long _n_points;

    std::vector<std::string> _labels;
    static constexpr int _max_n_curves = 6;
};

#endif //EASTWORLD_TRIGONOMETRICDATA_H
