//
// Created by guilbaudc on 5/18/20.
//

#ifndef STARTREK_VECTORSDATA_H
#define STARTREK_VECTORSDATA_H

#include <streambuf>
#include <vector>

class VectorsData {
public:
  VectorsData();

  void init(double sx, double sy);

  std::vector<double> &get_x() { return _x; }
  std::vector<double> &get_y() { return _y; }
  std::vector<double> &get_u() { return _u; }
  std::vector<double> &get_v() { return _v; }
  std::vector<double> &get_mag() { return _mag; }
  std::vector<std::string> &get_labels() { return _labels; }
  std::vector<double> &get_bx() { return _base_x; }
  std::vector<double> &get_by() { return _base_y; };
  unsigned long get_nx() { return _base_x.size(); }
  unsigned long get_ny() { return _base_y.size(); }
  std::vector<std::vector<double>> &get_x_as_2d() { return _x2d; }
  std::vector<std::vector<double>> &get_y_as_2d() { return _y2d; }
  std::vector<std::vector<double>> &get_u_as_2d() { return _u2d; }
  std::vector<std::vector<double>> &get_v_as_2d() { return _v2d; }
  std::vector<std::vector<double>> &get_mag_as_2d() { return _mag2d; }

  double get_max_mag() const { return _max_mag; }

  void print_x();
  void print_y();
  void print_u();
  void print_v();
  void print_mag();

private:
  void set_data();
  void transform_2d(const std::vector<double> &input,
                    std::vector<std::vector<double>> &output) const;

  std::vector<std::string> _labels;
  std::vector<double> _x;
  std::vector<double> _y;
  std::vector<double> _u;
  std::vector<double> _v;
  std::vector<double> _mag;
  double _max_mag;
  double _stepx;
  double _stepy;
  unsigned long _nx;
  unsigned long _ny;
  std::vector<double> _base_x;
  std::vector<double> _base_y;
  std::vector<std::vector<double>> _x2d;
  std::vector<std::vector<double>> _y2d;
  std::vector<std::vector<double>> _u2d;
  std::vector<std::vector<double>> _v2d;
  std::vector<std::vector<double>> _mag2d;

  static const char *_sep;
};

#endif // STARTREK_VECTORSDATA_H
