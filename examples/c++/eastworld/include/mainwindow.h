#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Trigonometric3DData.h"
#include "TrigonometricData.h"
#include "VectorsData.h"
#include <QMainWindow>
#include "TrigonometricData.h"
#include "Trigonometric3DData.h"

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QVBoxLayout;
class QGroupBox;
class QDoubleSpinBox;
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow();

protected:
  void closeEvent(QCloseEvent *event) override;

private slots:
  void about();
  void plot_all();
  void plot_all_and_delete();
  void plot_all_and_delete_one_call();
  void plot_nan_points();
  void contourf_lin();
  void contourf_log();
  void contour_lin();
  void contour_log();
  void tricontourf_lin();
  void tricontourf_log();
  void tricontour_lin();
  void tricontour_log();
  void load_image();
  void scatter();
  void clear();
  void plot_surface();
  void plot_surface_curve();
  void raised_contourf();
  void plot3d();
  void plot3d_cst();
  void subplots_adjust();
  void subplots_reset();
  void quiver();
  void streamplot();
  void colored_quiver();
  void colored_streamplot();
  void nquiver_1d();
  void nquiver_2d();
  void nstreamplot_1d();
  void nstreamplot_2d();
  void redo_last_action();

  void new_figure();
  void close_figure();

  void change_cmap(const QString &text);
  void clear_before_drawing(int state);
  void change_store_map(int state);
  void change_linewidth_for_map(double value);
  void change_n_levels_for_map(int value);
  void change_scatter_spec_marker(const QString &text);
  void change_scatter_spec_color(const QString &text);
  void change_scatter_spec_size(double value);
  void change_scatter_gen_alpha(double value);
  void toggle_scatter_spec(bool checked);
  void toggle_scatter_gen(bool checked);
  void change_subplots_adjust_left(double value);
  void change_subplots_adjust_right(double value);
  void change_subplots_adjust_top(double value);
  void change_subplots_adjust_bottom(double value);
  void change_subplots_adjust_wspace(double value);
  void change_subplots_adjust_hspace(double value);

private:
  void createActions();
  void createStatusBar();
  void createMiscWidgets();
  void createMapWidgets();
  void createScatterWidgets();
  void create3DWidgets();
  void createSubplotsWidgets();

    void generic_change_subplots_adjust(const std::string& side, double value);

  void generic_change_subplots_adjust(const std::string &side, double value);

  void print_geometry();
  //! Initialisation environnement nemesis
  void init_device();
  //! Titre pour les map et iso
  static void set_titles_for_map(const std::string &, const std::string &,
                                 const std::string &);

  TrigonometricData _data;
  Trigonometric3DData _data3d;
  VectorsData _dataVec;
  static constexpr int _default_n_points = 200;
  static constexpr int _default_n_curves = 6;

  typedef void (MainWindow::*LastMemberActionPtr)();
  // avec paramètre de retour et arguments double toto(int a, chr b)
  // typedef double (MainWindow::*NomTypePtrSurCeTypeDeFunctionMembre)(int,
  // char);
  LastMemberActionPtr _last_action;

  // -- Variables liées à des widgets Qt
  std::string _current_cmap;
  bool _clear_before_drawing;
  bool _store_map;
  double _linewidth;
  int _n_levels;
  std::string _scatter_spec_marker;
  std::string _scatter_spec_color;
  double _scatter_spec_size;
  double _scatter_alpha;
  bool _use_scatter_spec;
  std::map<std::string, double> _subplots_params;
  std::map<std::string, double> _default_subplots_params;
  bool _execute_subplots;
  std::map<std::string, QDoubleSpinBox *> _subplots_spinbox;
  double _default_vec_stepx;
  double _default_vec_stepy;

  // 3D Plan et Iso
  static std::vector<std::string> _cmaps;

  std::vector<std::vector<double>> _data_store;
  //! Mise à jour du stockage des données
  void update_data_storage();
  //! Stockage d'un réseau
  std::tuple<unsigned long, unsigned long> store_network(unsigned long n_points,
                                                         unsigned long n_curves,
                                                         double *x, double *y);
  //! Stockage d'une carte (appelée 3D plan dans Gaia)
  std::tuple<unsigned long, unsigned long, unsigned long>
  store_map(unsigned long n_x, double *x, unsigned long n_y, double *y,
            unsigned long n_z, double *z);

  //! Stocakge des vecteurs
  std::tuple<unsigned long, unsigned long, unsigned long, unsigned long>
  store_vec(std::vector<double> &x, std::vector<double> &y,
            std::vector<double> &u, std::vector<double> &v);
  std::tuple<unsigned long, unsigned long, unsigned long, unsigned long,
             unsigned long>
  store_vec(std::vector<double> &x, std::vector<double> &y,
            std::vector<double> &u, std::vector<double> &v,
            std::vector<double> &c);

  // Liste des noms des figures créées
  std::list<std::string> _figure_names;
  static unsigned int _figure_id;

  // Widget centrale
  QWidget *_central_widget;
  QVBoxLayout *_layout;
};

#endif
