#define _USE_MATH_DEFINES
#include <Python.h>

#include <cmath>
#if __cplusplus >= 201402L
#include <experimental/filesystem>
#endif
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <typeinfo>
#include <stdexcept>

#include "nemesiscppconfig.h"

#if PY_MAJOR_VERSION >= 3
#define PyString_FromString PyUnicode_FromString
#define PyString_AsString PyUnicode_AsUTF8
#endif

#ifdef CATCH_UNIT_TESTS
#include "catch.hpp"
#endif

#ifndef CATCH_UNIT_TESTS
void current_cartridge()
{
    auto line_length = 50;
    std::ostringstream oss_line;
    oss_line << std::setw(line_length) << std::setfill('#') << "#"
             << "\n";
    std::ostringstream oss_empty_line;
    oss_empty_line << "#" << std::setw(line_length - 2) << " "
                   << "#\n";
    std::string prog_name("INTEL_API_PYTHON compatibility");

    std::cout << oss_line.str() << oss_empty_line.str();
    std::cout << "# " << prog_name << "\n";
    std::cout << oss_empty_line.str() << oss_line.str();
}

static bool exists(const std::string& pathname)
{
    struct stat info;
    if (stat(pathname.c_str(), &info) == 0) {
        return true;
    }
    return false;
}

static void add_site(const std::string& new_path)
{
    if (!exists(new_path)) {
        throw std::runtime_error("Directory doesn't exist : " + new_path);
    }
    PyObject* site = PyImport_ImportModule("site");
    if (!site) {
        PyErr_Print();
        throw std::runtime_error("Error loading module site!");
    }
    PyObject* fct = PyString_FromString("addsitedir");
    PyObject* args = PyString_FromString(new_path.c_str());
    PyObject_CallMethodObjArgs(site, fct, args, NULL);
    Py_DECREF(site);
}

void call_python()
{
#if PY_MAJOR_VERSION >= 3
    wchar_t name[] = L"plotting";
#else
    char name[] = "plotting";
#endif
    Py_SetProgramName(name);
    Py_Initialize();
    // ajout du path vers l'environnement virtuel
    std::string site_path = std::string(VIRTUAL_ENV_SITE_PACKAGES);
    add_site(site_path);

    std::cout << "Python initialisé !" << "\n";

    PyObject* matplotlibname = PyString_FromString("matplotlib");
    PyObject* pyplotname = PyString_FromString("matplotlib.pyplot");
    PyObject* pylabname = PyString_FromString("pylab");
    if (!pyplotname || !pylabname || !matplotlibname) {
       throw std::runtime_error("couldnt create string");
    }
    else {
        std::cout << "Création noms des modules OK" << "\n";
    }

    PyObject* matplotlib = PyImport_Import(matplotlibname);
    Py_DECREF(matplotlibname);
    if (!matplotlib) {
        PyErr_Print();
        fprintf(stderr, "Failed to load matplotlib\n");
        throw std::runtime_error("Error loading module matplotlib!");
    }
    else {
        std::cout << "Import matplotlib OK\n";
    }

    PyObject* pymod = PyImport_Import(pyplotname);
    Py_DECREF(pyplotname);
    if (!pymod) {
        PyErr_Print();
        throw std::runtime_error("Error loading module matplotlib.pyplot!");
    }
    else {
        std::cout << "Import matplotlib.pyplot OK\n";
    }


//    PyObject* pylabmod = PyImport_Import(pylabname);
//    Py_DECREF(pylabname);
//    if (!pylabmod) {
//        PyErr_Print();
//        throw std::runtime_error("Error loading module pylab!");
//    }
//    else {
//        std::cout << "Import pylab OK\n";
//    }

    Py_Finalize();
}

int main(int argc, char** argv)
{
    current_cartridge();
    call_python();
}
#endif
