#define _USE_MATH_DEFINES
#include "matplotlibcpp.hpp"

#include <cmath>

#include <iostream>
#include <sstream>
#include <tuple>
#include <exception>
#include <string>

using namespace std;

namespace plt = matplotlibcpp;

static bool use_ion = true;

void networks(const unsigned int num, const unsigned int n, double* x, double* y) {
    plt::clf();

    std::ostringstream oss;
    oss << num << " : " << n << " points";
    plt::set_titles("Networks", oss.str(), "C++ standalone");
    PyObject* xx = plt::detail::get_array(n, x);
    for(unsigned int i=0; i<num; ++i) {
        auto start = i * n;
        PyObject* yy = plt::detail::get_array(n, y + start);
        std::ostringstream name;
        switch(i) {
            case 0:
                name << i << " : f(x) = sin(2*pi*i/360) + 2";
                break;
            case 1:
                name << i << " : f(x) = 2";
                break;
            case 2:
                name << i << " : f(x) = log(i+1) + 2.";
                break;
            default:
                throw std::domain_error("i inexistant : " + to_string(i));
        }
        // Incrémentation du compteur pour que le Py_DECREF sur les arguments de plot ne supprime pas les données
        Py_INCREF(xx);
        plt::named_plot(name.str(), xx, yy);
    }

    plt::legend();
    if(use_ion) {
        plt::show(true);
        plt::pause(2);
    }
    else {
        plt::show();
    }
}

double* x_data(const unsigned int n) {
    auto* x = new double[n];
    for(unsigned int i = 0; i < n; ++i) {
        x[i] = i * i;
    }
    return x;
}

double* all_data(const unsigned int n) {
    auto n_curves = 3;
    auto* all = new double[n_curves*n];
    for(auto num=0; num < n_curves; ++num) {
        unsigned int start = n * num;
        for (unsigned int i = start; i < start+n; ++i) {
            auto one_step = i - start;
            switch(num) {
                case 0:
                    all[i] = sin(2 * M_PI * i / 360.0) + 2.;
                    break;
                case 1:
                    all[i] = cos(2 * M_PI * i / 360.0) + 2.;
                    break;
                case 2:
                    all[i] = log(one_step+1) - 2.;
                    break;
                default:
                    throw std::runtime_error("Cas non prévu.");
            }
        }
    }
    return all;
}

int main(int argc, char** argv) {
    unsigned int n = 500;

    double* x = x_data(n);
    double* all = all_data(n);

    plt::backend("Qt5Agg");

    if(use_ion) {
        plt::ion();
    }

    networks(1, n, x, all);
    networks(3, n, x, all);

    if(use_ion) {
        plt::ioff();
    }
    plt::close();

//    delete [] x;
//    delete [] all;
}

