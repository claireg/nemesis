set(GNU_VERSION 6.4 CACHE STRING "Version gcc" FORCE)

set(GNU_ROOT /ccc/products/opendist/gcc49/gcc/${GNU_VERSION} CACHE PATH "chemin vers installation gnu" FORCE)
message (STATUS "GNU_ROOT = ${GNU_ROOT}")

set(CMAKE_C_COMPILER ${GNU_ROOT}/bin/gcc CACHE FILEPATH "C Compiler" FORCE)
set(CMAKE_CXX_COMPILER ${GNU_ROOT}/bin/g++ CACHE FILEPATH "CXX Compiler" FORCE)
set(CMAKE_CXX_FLAGS "-D_GLIBCXX_USE_CXX11_ABI=0 -std=c++1z" CACHE STRING "Flags C++" FORCE)

message (STATUS "CMAKE_CXX_COMPILER = ${CMAKE_CXX_COMPILER}")

set(COMPILER_LIBRARIES ${GNU_ROOT}/lib64 CACHE PATH "chemin vers lib compilateur choisi" FORCE)
set(FS_LIBS "-lstdc++fs" CACHE STRING "lib pour filesystem C++" FORCE) 
