
# Affichage de toutes les variables (dans le cache cmake) avec leurs valeurs
function(show_all_variables)
    message(STATUS "\n*** dump start cmake variables ***")
    get_cmake_property (_all_var_names VARIABLES)
    foreach(_var_name ${_all_var_names})
        message(STATUS "${_var_name}=${${_var_name}}")
    endforeach()
    message(STATUS "*** dump end ***\n")
endfunction(show_all_variables)