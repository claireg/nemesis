set(INTEL_VERSION 17.0.4.196 CACHE STRING "Version intel" FORCE)

execute_process("module load intel/${INTEL_VERSION}")

set(INTEL_ROOT $ENV{CXX_INTEL_ROOT})

set(CMAKE_C_COMPILER $ENV{CCC_C} CACHE FILEPATH "C Compiler" FORCE)
set(CMAKE_CXX_COMPILER $ENV{CCC_CXX} CACHE FILEPATH "CXX Compiler" FORCE)
set(CMAKE_CXX_FLAGS "-D_GLIBCXX_USE_CXX11_ABI=0 -std=c++14" CACHE STRING "Flags C++" FORCE)

set(COMPILER_LIBRARIES $ENV{CXX_INTEL_LIBDIR} CACHE PATH "chemin vers lib compilateur choisi" FORCE)
set(FS_LIBS "" CACHE STRING "lib pour filesystem C++" FORCE)