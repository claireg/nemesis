///
/// utilities.h
///
/// \brief	Utilitaires pour la composante nemesis-cpp.
/// \date	20/04/2018
/// \author	Claire Guilbaud
///
/// Notes
/// -----
/// * Pour faire des traces explicites de manière simple, ajouter dans la fonction à tracer :
///        std::cout << __PRETTY_FUNCTION__ << '\n';

#ifndef NEMESIS_CPP_UTILITIES_H_
#define NEMESIS_CPP_UTILITIES_H_

#if __cplusplus >= 201402L
#include <chrono>
#include <experimental/filesystem>
#endif
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <type_traits>

#include "nemesiscppconfig.h"

#if __cplusplus >= 201402L
namespace fs = std::experimental::filesystem;
#endif

//-----------------------------------------------------------------------------
//
// Namespace : utilities
//
//-----------------------------------------------------------------------------
namespace nem_utils {

template <typename WStrContainer>
static inline void split_string(WStrContainer& results_, const wchar_t* tosplit_)
{
    results_.clear();
    std::wistringstream sin(tosplit_);
    while (!sin.eof()) {
        std::wstring t;
        sin >> t;
        if (!t.empty()) {
            results_.push_back(t);
        }
    }
}

// dans la STL, il existe des méthodes pour ajuster la sortie à gauche, à droite mais pas au centre …
template <typename charT, typename traits = std::char_traits<charT>>
class center_helper {
    std::basic_string<charT, traits> str_;

public:
    center_helper(std::basic_string<charT, traits> str)
        : str_(str)
    {
    }
    template <typename a, typename b>
    friend std::basic_ostream<a, b>& operator<<(std::basic_ostream<a, b>& s, const center_helper<a, b>& c);
};

template <typename charT, typename traits = std::char_traits<charT>>
center_helper<charT, traits> centered(std::basic_string<charT, traits> str)
{
    return center_helper<charT, traits>(str);
}

template <typename charT, typename traits>
std::basic_ostream<charT, traits>& operator<<(std::basic_ostream<charT, traits>& s, const center_helper<charT, traits>& c)
{
    std::streamsize w = s.width();
    if (w > c.str_.length()) {
        std::streamsize left = (w + c.str_.length()) / 2;
        s.width(left);
        s << c.str_;
        s.width(w - left);
    } else {
        s << c.str_;
    }
    return s;
}

template <typename T>
static inline void print_vector(std::vector<T>& tab_, const std::wstring& msg_ = L"")
{
    if (!msg_.empty()) {
        std::wcout << msg_ << std::endl;
    }
    std::cout << "## Tableau : " << tab_.size() << "\t(" << typeid(tab_).name() << ")\n";
    std::copy(tab_.begin(), tab_.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << "\n";
}

template <typename T>
static inline void print_vector_vector(std::vector<std::vector<T>>& results_)
{
    for (auto subvector : results_) {
        print_vector(subvector);
    }
}

#if __cplusplus >= 201402L
static inline auto reference_dir()
{
    const fs::path fp(__FILE__);
    auto rep = fp.parent_path();
    fs::path ppath = rep;
    ppath /= "..";
    ppath /= "..";
    ppath /= "data";
    ppath /= "reference_data";
    return ppath;
}

static inline auto create_reference_path(const fs::path& filename_)
{
    fs::path filepath(reference_dir());
    filepath /= filename_;
    return filepath;
}

static inline void exists(const fs::path& filepath_)
{
    if (!fs::exists(filepath_)) {
        std::ostringstream oss;
        oss << "File " << filepath_.native() << "doesn't exist.";
        throw std::runtime_error(oss.str());
    }
}

template <class TDuration = std::chrono::milliseconds>
static void logger_timer_message(const std::string& msg, auto interval)
{
    std::cout << "Temps d'exécution de " << msg << " : " << std::chrono::duration_cast<TDuration>(interval).count() << "\n";
}

// Attention ne traite que les fonctions T func(Args...). Le type de retour de la fonction ne peut être void.
//      Appel :
//          nem::logger_timer("gaia_section_efficace", ex_1)(pause); // int ex_1(int) { ... }
//          nem::logger_timer("gaia_section_efficace", ex_2)(); // int ex_2() {...}
template <class TFunc, class TDuration = std::chrono::milliseconds>
static auto logger_timer(const std::string& msg, TFunc&& func)
{
    auto new_function = [func = std::forward<TFunc>(func), msg](auto&&... args) {
        auto start = std::chrono::high_resolution_clock::now();
        auto result = func(std::forward<decltype(args)>(args)...);
        // auto result = func(args...);
        auto end = std::chrono::high_resolution_clock::now();
        logger_timer_message<>(msg, end - start);
        return result;
    };
    return new_function;
}

// Utilisable seulement si le type de retour de l'appel peut être construit
template <class TFunc, class TDuration = std::chrono::milliseconds, class... Args>
typename std::enable_if<std::is_constructible<TFunc, Args&&...>::value>::type
logger_timer_func_return_args(const std::string& msg, TFunc&& func, Args&&... args)
{
    auto start = std::chrono::high_resolution_clock::now();
    auto result = std::forward<TFunc>(func)(std::forward<decltype(args)>(args)...);
    // func(args...);
    auto end = std::chrono::high_resolution_clock::now();
    logger_timer_message<>(msg, end - start);
    return result;
}

// Traite tous les signatures de fonctions mais ne retourne jamais rien ! car quid du type de result dans le cas
// ou la fonction retourne void
//      Appel : nem::logger_timer_func("gaia_curve_data", ex_2, pause)
template <class TFunc, class TDuration = std::chrono::milliseconds, class... Args>
auto logger_timer_func(const std::string& msg, TFunc&& func, Args&&... args) -> decltype(func(args...))
{
    //std::cout << "###### logger_timer_func : VOID" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::forward<TFunc>(func)(std::forward<decltype(args)>(args)...);
    // func(args...);
    auto end = std::chrono::high_resolution_clock::now();
    logger_timer_message<>(msg, end - start);
}
#endif

constexpr std::tuple<int, int, int, int> version_info()
{
    return std::make_tuple(NEMESIS_VERSION_MAJOR, NEMESIS_VERSION_MINOR, NEMESIS_VERSION_PATCH, NEMESIS_VERSION_TWEAK);
}

static std::string version()
{
    std::ostringstream oss_version;
    int major, minor, patch, tweak;
    std::tie(major, minor, patch, tweak) = version_info();
    oss_version << major << "." << minor;
    oss_version << "." << patch << "." << tweak;
    return oss_version.str();
}

static void cartridge()
{
    auto line_length = 50;
    std::ostringstream oss_line;
    oss_line << std::setw(line_length) << std::setfill('#') << "#"
             << "\n";
    std::ostringstream oss_empty_line;
    oss_empty_line << "#" << std::setw(line_length - 2) << " "
                   << "#\n";
    std::string prog_name("Nemesis-cpp");

    std::cout << oss_line.str() << oss_empty_line.str();
    std::cout << "#" << std::setw(line_length) << nem_utils::centered(prog_name) << "#\n";
    std::cout << "#" << std::setw(line_length) << nem_utils::centered(version()) << "#\n";
    std::cout << oss_empty_line.str() << oss_line.str();
}
}

#endif // NEMESIS_CPP_UTILITIES_H_
