# Depuis ce répertoire :
# *ATTENTION* il faut avoir activer l'environnement virtuel désiré.
# ./configure et suivre les instructions
# cmake -S . -B /tmp --build doit aussi fonctionner

cmake_minimum_required(VERSION 3.15)

project(Westworld
        DESCRIPTION "Petit exemple simple pour tester matplotlibcpp et ses extensions CEA"
        LANGUAGES CXX)

# pour pip
if(${USE_VIRTUEL_ENV})
    set(ENV{PATH} "$ENV{VIRTUAL_ENV}/bin:$ENV{PATH}")
endif()
# pour spack sans vue avec option spack_env de configure

# if(DEFINED ENV{SPACK_SITE_PACKAGES})
#    message(STATUS "SPACK_SITE_PACKAGES environment variable defined")
#    set(SITE_PACKAGES "$ENV{SPACK_SITE_PACKAGES}")
#    message("SPACK_SITE_PACKAGES:" $ENV{SPACK_SITE_PACKAGES})
# endif()

# -----------------------------------------------
# Pour trouver NemesisConfig.cmake, fixer soit Nemesis_DIR
set(Nemesis_DIR ${PROJECT_SOURCE_DIR}/../../../lib/cmake)
message(STATUS "Nemesis_DIR = ${Nemesis_DIR}")
set(NemesisQt_DIR "${Nemesis_DIR}")

find_package(Nemesis REQUIRED CONFIG)
find_package(NemesisQt REQUIRED CONFIG)

set(PROJECT_VERSION_MAJOR  ${Nemesis_VERSION_MAJOR})
set(PROJECT_VERSION_MINOR ${Nemesis_VERSION_MINOR})
set(PROJECT_VERSION_PATCH ${Nemesis_VERSION_PATCH})
set(PROJECT_VERSION_TWEAK 99)
set(PROJECT_VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}.${PROJECT_VERSION_TWEAK})

# -----------------------------------------------
# -- CMake's variables
# ATTENTION les variables déjà existante dans le cache de CMake
#   ne sont pas modifiables depuis ici mais depuis la ligne de commande.
set(CMAKE_VERBOSE_MAKEFILE ON CACHE BOOL "Verbose Makefile" FORCE)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# -----------------------------------------------
# -- project configuration
set(CFG_NAME "nemesiscppconfig.h")
# set(CFG_DONE "${CMAKE_BINARY_DIR}/${CFG_NAME}")
set(CFG_IN_INSTALL_DIR "${Nemesis_DIR}/../../include/${CFG_NAME}")
set(CFG_IN_BUILD_DIR "${CMAKE_BINARY_DIR}/${CFG_NAME}")
# Si le fichier existe, c'est qu'on est dans un répertoire d'installation
# Sinon c'est qu'on est lors d'un build (dans un répertoire de développement)
if(NOT EXISTS ${CFG_IN_INSTALL_DIR})
    if(NOT EXISTS "${CFG_IN_BUILD_DIR}")
        message(STATUS "/!\\ Fichier ${CFG_NAME} inexistant dans \n"
                "\t* ${CFG_IN_BUILD_DIR} \n"
                "\t* ${CFG_IN_INSTALL_DIR}\n\t-> il faut en créer un.")
        set(NEMESIS_PKG_DIR "${PROJECT_SOURCE_DIR}/../../../")
        set(NEMESIS_SITE_PACKAGES "${NEMESIS_PKG_DIR}")
        # Remplacé lors de l'installation de nemesis. Inutile en dev
        set(SPACK_DEPENDENCIES_SITELIB "")
        # Utile si dans configure, utilisation de l'option --spack_env
        if(DEFINED ENV{SPACK_SITE_PACKAGES})
            set(SITE_PACKAGES "$ENV{SPACK_SITE_PACKAGES}")
        else()
            message(STATUS "Python3_SITELIB = ${Python3_SITELIB}")
            set(SITE_PACKAGES "${Python3_SITELIB}")
        endif()
        configure_file (
                "${NEMESIS_PKG_DIR}/include/${CFG_NAME}.in"
                "${CMAKE_BINARY_DIR}/${CFG_NAME}"
        )
    endif()
endif()

## -- Les fichiers
set(_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")
set(_SOURCE_DIR "${PROJECT_SOURCE_DIR}/src")
set(_RESOURCE_DIR "${PROJECT_SOURCE_DIR}/resources")

set(PROJECT_INCLUDE_DIRS "${_INCLUDE_DIR};${PROJECT_BINARY_DIR}/include")
set(PROJECT_QT_RESOURCES "${_RESOURCE_DIR}/application.qrc")

# INUTILE si CMAKE_AUTOMOC ON
qt5_wrap_cpp(PROJECT_QT_MOC "${_INCLUDE_DIR}/mainwindow.h")

set(PROJECT_FILES "${_SOURCE_DIR}/mainwindow.cpp;${_SOURCE_DIR}/main.cpp")

# -----------------------------------------------
# -- build executables
set(PROJECT_EXE ${PROJECT_NAME})
add_executable(${PROJECT_EXE}
        ${PROJECT_FILES}
        ${PROJECT_QT_RESOURCES}
        ${PROJECT_QT_MOC}
)
target_compile_definitions(${PROJECT_EXE}
        PRIVATE
        ${Nemesis_COMPILE_DEFINITIONS}
)
target_compile_features(${PROJECT_EXE}
        PRIVATE
        ${Nemesis_COMPILE_FEATURES}
        )
target_compile_options(${PROJECT_EXE}
        PRIVATE
        ${Nemesis_COMPILE_OPTIONS}
        )
target_include_directories(${PROJECT_EXE}
    PRIVATE
        ${Nemesis_INCLUDE_DIRS}
        ${CMAKE_BINARY_DIR}
        ${PROJECT_INCLUDE_DIRS}
        ${Qt5Widgets_INCLUDE_DIRS}
)
target_link_libraries(${PROJECT_EXE}
    PRIVATE
        ${Nemesis_LIBRARIES}
        ${QT_LIBRARIES}
)

