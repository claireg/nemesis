# Westworld

# Pré-requis

* Python 3.7
* Environnement ``Nemesis_0.2`` (avec ``PySide-1.2.4``)

## But

La mainloop Qt est en C++, et appelle ``matplotlib`` avec le backend Qt5Agg.

Il existe un équivalent en python pure (``withmainloop/smallworldqt5.py``)

Le fichier d'en-tête `matplotlibcpp.hpp` est en local de cet exemple.
Cela permet de tester les éventuels fuites mémoires au niveau de l'API C avec
un projet «léger».

**ATTENTION**, le fichier `matplotlibcpp.hpp` n'est pas à jour. Il ne reflète pas ce qui est
dans le package Python `nemesis`.