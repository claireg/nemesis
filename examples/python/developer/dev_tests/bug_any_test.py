# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd


def contour_data() -> (np.array, np.array, np.array):
    x, y, z = nem_rd.gaia_isolines_levels()
    return x, y, z


if __name__ == '__main__':
        ref_levels = np.array([1.e-322, 1.e-300, 1.e-278, 1.e-256, 1.e-234, 1.e-212, 1.e-190, 1.e-168, 1.e-146, 1.e-124,
                               1.e-102, 1.e-080, 1.e-058, 1.e-036, 1.e-014, 1.e+008, 1.e+030], dtype=float)
        ref_layers = np.array([5.e-301, 5.e-267, 5.e-233, 5.e-199, 5.e-165, 5.e-131, 5.e-097, 5.e-063, 5.e-029,
                               5.e+005, 5.e+039], dtype=float)
        ref_iso_levels = np.array([1.e-09, 1.e-07, 1.e-05, 1.e-03, 1.e-01, 1.e+01], dtype=float)
        x, y, z = contour_data()
        nem_plt.PyPlotCEA.logarithmic_contourf(x, y, z, masked_value=1.e-300)
        levels = nem_plt.PyPlotCEA.get_contourf_levels()
        layers = nem_plt.PyPlotCEA.get_contourf_layers()
        nem_plt.PyPlotCEA.logarithmic_contour(x, y, z, levels=ref_iso_levels)
        iso_levels = nem_plt.PyPlotCEA.get_contour_levels()
        assert ref_levels.all() == levels.all()
        assert ref_layers.all() == layers.all()
        assert ref_iso_levels.all() == iso_levels.all()
        plt.show()
