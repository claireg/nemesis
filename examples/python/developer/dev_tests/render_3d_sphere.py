# -*- coding: utf-8 -*-

import os

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

import nemesis.plotter as nem_plt


def plot_quart_sphere(ax, r, zo, color, surf=False):
    n_theta = 50
    n_phi = 100

    theta, phi = np.mgrid[0.:0.5*np.pi:n_theta*1j, 0.:np.pi:n_phi*1j]
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    if surf is True:
        ax.plot_surface(x, y, z, zorder=zo, color=color, linewidth=0., antialiased=False)
    else:
        ax.plot_wireframe(x, y, z, zorder=zo, color=color)


def set_axis_labels(ax, sx='X', sy='Y', sz='Z', fw='bold', fs=16):
    ax.set_xlabel(sx, fontsize=fs, fontweight=fw)
    ax.set_ylabel(sy, fontsize=fs, fontweight=fw)
    ax.set_zlabel(sz, fontsize=fs, fontweight=fw)


def quarts_spheres(radii, zorders, surf=False):
    ax = nem_plt.PyPlotCEA.switch_axes('3d')

    if isinstance(radii, int):
        radii = [radii]

    colors_base = [mcolors.to_rgba(c) for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]
    for radius, zo, color in zip(radii, zorders, colors_base):
        plot_quart_sphere(ax, radius, zo, color, surf)

    set_axis_labels(ax)
    # Si élevation = 0, alors rendu incorrect
    ax.view_init(azim=-90, elev=31.)


if __name__ == '__main__':
    # mpl.use('svg')
    nem_plt.PyPlotCEA.set_figure_size(800, 600)
    nem_plt.PyPlotCEA.nemesis_params()

    quarts_spheres(radii=(0.5, 1., 2.), zorders=[1, 2, 3], surf=False)

    plt.gcf().suptitle(f'Matplotlib {mpl.__version__}', fontweight='bold')

    plt.show()
    # chemin = os.path.join(os.path.expanduser('~'), 'tmp', 'spheres_bg_svg_2.svg')
    # print(chemin)
    # plt.savefig(chemin)
