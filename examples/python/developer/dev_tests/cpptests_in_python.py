# -*- coding: utf-8 -*-

import os

import numpy as np
# import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.colors as colors


def build_simple_data():
    x = np.array([-3., -1.5, 1.5, 3.])
    y = np.array([-2., -1., 0., 1., 2.])
    z = np.array([3, 0.1, 0.1, 3,  0.1, 0.2, 0.2, 0.1,  0.1, 3, 3, 0.1,  0.1, 0.2, 0.2, 0.1,  3, 0.1, 0.1, 3])
    return x, y, z


def build_data_with_zeros():
    x, y, z = build_simple_data()
    z[0] = z[3] = z[16] = z[19] = 0.
    z[9] = 0
    return x, y, z


def contour():
    x, y, z = build_simple_data()
    xi, yi = np.meshgrid(x, y, indexing='ij')
    zi = z.reshape(x.size, y.size)

    fig, axs = plt.subplots(1, 2)

    lin_ax = axs[0]
    lin_c = lin_ax.contourf(xi, yi, zi)
    fig.colorbar(lin_c, ax=lin_ax)
    lin_ax.grid(color='black', linestyle='-', alpha=0.3)

    log_ax = axs[1]
    log_c = log_ax.contourf(xi, yi, zi, norm=colors.LogNorm())
    fig.colorbar(log_c, ax=log_ax)
    log_ax.grid(color='black', linestyle='-', alpha=0.3)
    plt.show()


def masked_contour():
    x, y, z = build_data_with_zeros()
    xi, yi = np.meshgrid(x, y, indexing='ij')
    z = z.reshape(x.size, y.size)
    zi = np.ma.masked_where(z <= 0, z)

    fig, axs = plt.subplots(1, 2)

    lin_ax = axs[0]
    lin_c = lin_ax.contourf(xi, yi, zi)
    fig.colorbar(lin_c, ax=lin_ax)
    lin_ax.grid(color='black', linestyle='-', alpha=0.3)

    log_ax = axs[1]
    # log_c = log_ax.contourf(xi, yi, zi, locator=ticker.LogLocator())
    log_c = log_ax.contourf(xi, yi, zi, norm=colors.LogNorm())
    fig.colorbar(log_c, ax=log_ax)
    log_ax.grid(color='black', linestyle='-', alpha=0.3)

    plt.show()


def __display(interactive, name=None):
    if interactive is True:
        plt.show(True)
    else:
        plt.savefig(os.path.join('/tmp', name))


def bug_semilog_backend_agg(interactive=False):
    x = np.linspace(1, 5001, num=5000)
    y = np.sin(2 * np.pi * x / 360.0)
    x = x * x
    if interactive is True:
        plt.ion()
    plt.semilogx(x, y)
    __display(interactive, 'semilogx.png')

    plt.semilogy(x, y)
    __display(interactive, 'semilogy.png')


if __name__ == '__main__':
    # contour()
    bug_semilog_backend_agg()
