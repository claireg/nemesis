# -*- coding: utf-8 -*-

from typing import List

import numpy as np
from numpy.testing import (
    assert_array_equal
)
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import nemesis.readdata as nem_rd
import nemesis.plotter as nem_plt
import nemesis.testing as nem_tst


color_seg_base = [mcolors.to_rgba(c) for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]


def plot_hedgehogs(hh_data: List[nem_rd.PsyCurveData]):
    ax = nem_plt.PyPlotCEA.switch_axes('3d')
    for hh in hh_data:
        for arr, c in zip(hh.data, hh.colors):
            ax.plot3D(*arr, color=color_seg_base[c])
    ax.view_init(azim=165, elev=15)
    plt.show()


def save_hedgehogs(hh_data: List[nem_rd.PsyCurveData]):
    name = nem_tst.create_name_for_npz('/tmp', 'test_psy_hedgehog')
    to_save = [hh.data for hh in hh_data]
    np.savez_compressed(name, hedgehog=to_save)

    npz_file = np.load(name)
    hh = npz_file['hedgehog']

    for i, h in enumerate(hh):
        assert_array_equal(to_save[i], h)


if __name__ == '__main__':
    nem_plt.PyPlotCEA.nemesis_params()

    data = nem_rd.psy_hedgehog()
    plot_hedgehogs(data)

    # save_hedgehogs(data)
