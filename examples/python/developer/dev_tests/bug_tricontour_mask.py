# -*- coding: utf-8 -*-

"""
Reproducer pour les tests test_log_triangulation_1_0 et test_log_triangulation_1_1 :

def test_log_triangulation_1_0() -> None:
    x, y, z = tricontour_data()
    z = np.power(10, z * 16)
    nem_plt.PyPlotCEA.logarithmic_tricontourf(x, y, z, n_decades=2, max_decades=5)
    nem_plt.PyPlotCEA.logarithmic_tricontour(x, y, z, n_decades=2, max_decades=5)

Message d'erreurs des tests :
        # z values must be finite, only need to check points that are included
        # in the triangulation.
        z_check = z[np.unique(tri.get_masked_triangles())]
        if np.ma.is_masked(z_check):
>           raise ValueError('z must not contain masked points within the '
                             'triangulation')
E           ValueError: z must not contain masked points within the triangulation

Message d'erreur de ce script :

    z array must not contain non-finite values within the triangulation
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri

import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd


def contour_data() -> (np.array, np.array, np.array):
    return nem_rd.gaia_isolines_levels()


def tricontour_data() -> (np.array, np.array, np.array):
    n_angles = 48
    n_radii = 8
    min_radius = 0.25
    radii = np.linspace(min_radius, 0.95, n_radii)

    angles = np.linspace(0, 2 * np.pi, n_angles, endpoint=False)
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)
    angles[:, 1::2] += np.pi / n_angles

    return (radii * np.cos(angles)).flatten(), (radii * np.sin(angles)).flatten(), \
           np.fabs(np.cos(radii) * np.cos(3 * angles)).flatten()


def manual_logarithmic_tricontour(ax: np.array, ay: np.array, az: np.array):
    # si pas modification de z : là ça plante
    isbad = np.less(az, 0.2) | np.greater(az, 0.8)
    triang = tri.Triangulation(ax, ay)
    mask = np.all(np.where(isbad[triang.triangles], True, False), axis=1)
    # triang.set_mask(mask)
    # plt.tricontourf(triang, az)
    plt.tricontourf(triang, az, mask=mask)
    plt.colorbar()
    plt.show()


def auto_logarithmic_tricontour(ax: np.array, ay: np.array, az: np.array):
    new_z = np.power(10, az * 16)
    nem_plt.PyPlotCEA.logarithmic_tricontour(ax, ay, new_z, n_decades=2, max_decades=5)
    plt.show()


def auto_logarithmic_tricontourf(ax: np.array, ay: np.array, az: np.array):
    new_z = np.power(10, az * 16)
    nem_plt.PyPlotCEA.logarithmic_tricontourf(ax, ay, new_z, n_decades=2, max_decades=5)
    plt.show()


def auto_iso_log_mapping_0():
    ax, ay, az = contour_data()
    # new_z_0 = np.ma.masked_where(az <= 1e-300, az, copy=False)
    # new_z_1 = np.ma.masked_where(new_z_0 <= 0., new_z_0, copy=False)
    # plt.plot(new_z_1)
    # plt.show()
    nem_plt.PyPlotCEA.logarithmic_contourf(ax, ay, az, label='carte',
                                           draw_iso=False, masked_value=1e-300, n_decades=9, max_decades=100)
    plt.show()


def fluff(ax: np.array, ay: np.array, az: np.array):
    # si pas modification de z : là ça plante
    isbad = np.less(az, 0.2) | np.greater(az, 0.8)
    triang = tri.Triangulation(ax, ay)
    mask = np.all(np.where(isbad[triang.triangles], True, False), axis=1)
    # plt.tricontourf(triangles=triang, az)
    # plt.tricontourf(triang, az, mask=mask)
    plt.colorbar()
    plt.show()


def fluff_2(ax, ay, az):
    """
    Notes:
    -----
    * avec contourf, on masquait les données (on jouait sur z)
    * avec tricontourf, il faut masquer les triangles correspondant au critère SANS toucher à z
     """
    min_radius = 0.25
    # Create the Triangulation; no triangles so Delaunay triangulation created.
    triang = tri.Triangulation(ax, ay)

    # Mask off unwanted triangles : Ici ça fonctionne
    # -- Premier masque : les points  l'intérieur du cercle de rayon min_radius (et centré au milieu des données)
    # triang.set_mask(np.hypot(ax[triang.triangles].mean(axis=1),
    #                          ay[triang.triangles].mean(axis=1))
    #                 < min_radius)
    # -- Second masque :
    # * création d'un masque tableaux pour des z < 0.2 et > 0.8
    # * report du masques sur les triangles
    isbad = np.less(az, 0.2) | np.greater(az, 0.8)
    triang.set_mask(np.all(np.where(isbad[triang.triangles], True, False), axis=1))
    fig1, ax1 = plt.subplots()
    ax1.set_aspect('equal')
    tcf = ax1.tricontourf(triang, az)
    fig1.colorbar(tcf)
    ax1.tricontour(triang, az, colors='k')
    plt.show()


if __name__ == '__main__':
    x, y, z = tricontour_data()
    # z = np.power(10, z * 16)
    # OK
    # fluff()
    # OK
    # auto_iso_log_mapping_0()
    # OK
    # auto_logarithmic_tricontour(x, y, z)
    # OK
    # auto_logarithmic_tricontourf(x, y, z)
    # OK : on ne peut pas masquer des données même après avoir calculé la triangulation
    # fluff_2(x, y, z)
