# -*- coding: utf-8 -*-

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

import nemesis.plotter as nem_plt


def plot_quart_sphere(ax, r, zo, color):
    n_theta = 50
    n_phi = 100

    theta, phi = np.mgrid[0.:0.5*np.pi:n_theta*1j, 0.:np.pi:n_phi*1j]
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    ax.scatter3D(x, y, z, color=color)


def set_axis_labels(ax, sx='X', sy='Y', sz='Z', fw='bold', fs=16):
    ax.set_xlabel(sx, fontsize=fs, fontweight=fw)
    ax.set_ylabel(sy, fontsize=fs, fontweight=fw)
    ax.set_zlabel(sz, fontsize=fs, fontweight=fw)


def quarts_spheres(radii, zorders):
    ax = nem_plt.PyPlotCEA.switch_axes('3d')

    if isinstance(radii, int):
        radii = [radii]

    colors_base = [mcolors.to_rgba(c) for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]
    for radius, zo, color in zip(radii, zorders, colors_base):
        plot_quart_sphere(ax, radius, zo, color)

    set_axis_labels(ax)
    ax.view_init(azim=-90, elev=0.)


def make_ax(grid=False):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.grid(grid)
    return ax


def midpoints(x):
    sl = ()
    for i in range(x.ndim):
        x = (x[sl + np.index_exp[:-1]] + x[sl + np.index_exp[1:]]) / 2.0
        sl += np.index_exp[:]
    return x


def simple_voxels():
    from mpl_toolkits.mplot3d import Axes3D
    filled = np.array([
        [[1, 0, ], [0, 0]],
        [[0, 1, ], [1, 0]]
    ])
    colors = np.zeros(filled.shape + (3, ))

    r, g, b = np.indices((3, 3, 3)) / 5.0
    rc = midpoints(r)
    gc = midpoints(g)
    bc = midpoints(b)
    colors[..., 0] = rc
    colors[..., 1] = gc
    colors[..., 2] = bc

    ax = make_ax(True)
    ax.voxels(r, g, b, filled,
              facecolors=colors,
              edgecolors=np.clip(2*colors - 0.5, 0, 1),
              linewidth=0.5)
    plt.show()


def mpl_exm():
    # prepare some coordinates, and attach rgb values to each
    r, g, b = np.indices((17, 17, 17)) / 16.0
    rc = midpoints(r)
    gc = midpoints(g)
    bc = midpoints(b)

    # define a sphere about [0.5, 0.5, 0.5]
    sphere = (rc - 0.5)**2 + (gc - 0.5)**2 + (bc - 0.5)**2 < 0.5**2
    # print(sphere.shape)
    # print(sphere.shape + (3,))

    # combine the color components
    colors = np.zeros(sphere.shape + (3,))
    # print(colors.shape)
    colors[..., 0] = rc
    colors[..., 1] = gc
    colors[..., 2] = bc

    # and plot everything
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.voxels(r, g, b, sphere,
              facecolors=colors,
              edgecolors=np.clip(2*colors - 0.5, 0, 1),  # brighter
            linewidth=0.5)
    ax.set(xlabel='r', ylabel='g', zlabel='b')

    plt.show()


def spheres():
    nem_plt.PyPlotCEA.set_figure_size(800, 600)
    nem_plt.PyPlotCEA.nemesis_params()

    quarts_spheres(radii=(0.5, 1., 2.), zorders=[1, 2, 3])

    plt.gcf().suptitle(f'Matplotlib {mpl.__version__}', fontweight='bold')
    plt.show()


if __name__ == '__main__':
    # simple_voxels()
    # mpl_exm()
    spheres()
