# -*- coding: utf-8 -*-

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt


def data(n: int = 100, nc: int = 1) -> [np.array]:
    x = np.linspace(0, 5, n)
    return x, [1 + np.sin(x) + 0.1 * i for i in range(nc)]


if __name__ == '__main__':
    nem_plt.PyPlotCEA.nemesis_params()
    mpl.rc('axes.formatter', limits=(-5, 5), use_mathtext=True)
    # mpl.rc('axes.formatter', limits=(-5, 5))
    nem_plt.PyPlotCEA.set_figure_xy(618, 28, 816, 1020)

    x, yi = data(100, 2)
    for j, y in enumerate(yi):
        plt.plot(x, y * 1e5, label=f'Sinus {j}')

    plt.legend()
    plt.ylabel('1 + np.sin(x) + 0.1 * i')
    plt.xlabel('Angles')
    cax = plt.gca().yaxis
    # f = cax.get_major_formatter()
    # print(f'Formatter axe Y : {f}')
    # cax.set_major_formatter(mpl.ticker.ScalarFormatter(useMathText=True))
    plt.show()
