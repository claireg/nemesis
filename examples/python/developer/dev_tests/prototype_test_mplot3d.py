# -*- coding: utf-8 -*-

import warnings

import numpy as np
import matplotlib as mpl
# Pour le debug dans PyCharm (si sources matplotlib non modifiées)
# mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.projections as projections
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

from PySide2 import QtCore


def build_r_theta():
    r = np.arange(0, 2, 0.01)
    theta = 2 * np.pi * r
    return r, theta


def build_x_y():
    x = np.linspace(0, 1, 100)
    y = np.sin(x * 2 * np.pi) / 2
    return x * 8, y * 8


def build_mesh():
    x = np.arange(-5, 5, 0.25)
    y = np.arange(-5, 5, 0.25)
    x, y = np.meshgrid(x, y)
    r = np.sqrt(x ** 2 + y ** 2)
    z = np.sin(r)
    return x, y, z


def lorenz(x, y, z, s=10, r=28, b=2.667):
    """
    Given:
       x, y, z: a point of interest in three dimensional space
       s, r, b: parameters defining the lorenz attractor
    Returns:
       x_dot, y_dot, z_dot: values of the lorenz attractor's partial
           derivatives at the point x, y, z
    """
    x_tmp = s * (y - x)
    y_tmp = r * x - y - x * z
    z_tmp = x * y - b * z
    return x_tmp, y_tmp, z_tmp


def build_lorenz():
    dt = 0.01
    num_steps = 10000

    # Need one more for the initial values
    xs = np.empty((num_steps + 1,))
    ys = np.empty((num_steps + 1,))
    zs = np.empty((num_steps + 1,))

    # Set initial values
    xs[0], ys[0], zs[0] = (0., 1., 1.05)

    # Step through "time", calculating the partial derivatives at the current point
    # and using them to estimate the next point
    for i in range(num_steps):
        x_dot, y_dot, z_dot = lorenz(xs[i], ys[i], zs[i])
        xs[i + 1] = xs[i] + (x_dot * dt)
        ys[i + 1] = ys[i] + (y_dot * dt)
        zs[i + 1] = zs[i] + (z_dot * dt)

    return xs, ys, zs


def relax():
    plt.draw()
    plt.pause(2.)


def my_surface():
    ax = plt.gca()
    x, y, z = build_mesh()
    ax.plot_surface(x, y, z, rstride=1, cstride=1, linewidth=0, antialiased=False)


def my_contour():
    ax = plt.gca()
    x, y, z = build_mesh()
    cset = ax.contour(x, y, z, extend3d=False)
    ax.clabel(cset)


def my_curve():
    ax = plt.gca()
    x = np.linspace(0, 1, 100)
    y = np.sin(x * 2 * np.pi) / 2
    # ax.plot(x*8, y*8, zs=0, zdir='z')
    ax.plot(x * 8, y * 8, x)


def solution_1():
    # Nouvelle manière préconisée
    fig = plt.figure(figsize=plt.figaspect(6. / 9.))
    fig.gca(projection='3d')

    my_surface()
    my_curve()
    # animation()
    plt.show()


def solution_2():
    # Ancienne manière
    fig = plt.figure(figsize=plt.figaspect(6. / 9.))
    fig.add_subplot(111, projection='3d')

    my_contour()
    plt.show()


def build_3d_curve():
    x = np.arange(-5, 5, 0.25)
    y = np.arange(-5, 5, 0.25)
    r = np.sqrt(x ** 2 + y ** 2)
    z = np.sin(r)
    return x, y, z


def check_switch_axes():
    x, y, z = build_mesh()
    ax = switch_axes('3d')
    ax.contour3D(x, y, z, extend3d=False)
    relax()

    plt.clf()
    ax = switch_axes('3d')
    xs, ys, zs = build_lorenz()
    ax.plot3D(xs, ys, zs, linewidth=1.)
    relax()

    ax = switch_axes('polar')
    r, theta = build_r_theta()
    ax.plot(theta, r)
    ax.set_rmax(2)
    ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
    ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
    ax.grid(True)
    relax()

    ax = switch_axes('3d')
    x, y, z = build_3d_curve()
    print(f'X = {x}\nY = {y}\nZ = {z}')
    ax.plot3D(x, y, z, linewidth=1.)
    relax()


def clear_3d_axis():
    ax = switch_axes('3d')

    x, y, z = build_mesh()
    ax.contour3D(x, y, z, extend3d=False)
    relax()

    plt.cla()
    xs, ys, zs = build_lorenz()
    ax.plot3D(xs, ys, zs, linewidth=1.)
    relax()

    ax = switch_axes('polar')
    r, theta = build_r_theta()
    ax.plot(theta, r)
    ax.set_rmax(2)
    ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
    ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
    ax.grid(True)
    relax()

    ax = switch_axes('3d')
    x, y, z = build_3d_curve()
    ax.plot3D(x, y, z, linewidth=1.)
    relax()


def solution_retenue():
    x, y, z = build_mesh()
    ax = plt.gca(projection='3d')
    print(f'##### Projection : {ax.name}')
    ax.contour3D(x, y, z, extend3d=False)
    relax()

    ax = switch_axes('rectilinear')
    print(f'##### Projection : {ax.name}')
    x, y = build_x_y()
    plt.plot(x, y)
    relax()

    ax = switch_axes('3d')
    xs, ys, zs = build_lorenz()
    print(f'##### Projection : {ax.name}')
    ax.plot3D(xs, ys, zs, linewidth=1.)
    relax()

    ax = switch_axes('polar')
    r, theta = build_r_theta()
    print(f'##### Projection : {ax.name}')
    ax.plot(theta, r)
    ax.set_rmax(2)
    ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
    ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
    ax.grid(True)
    relax()

    ax = switch_axes('aitoff')
    print(f'##### Projection : {ax.name}')
    plt.title(f'{ax.name}')
    plt.grid(True)
    relax()

    ax = switch_axes('hammer')
    print(f'##### Projection : {ax.name}')
    plt.title(f'{ax.name}')
    plt.grid(True)
    relax()

    ax = switch_axes('mollweide')
    print(f'##### Projection : {ax.name}')
    plt.title(f'{ax.name}')
    plt.grid(True)
    relax()

    ax = switch_axes('lambert')
    print(f'##### Projection : {ax.name}')
    plt.title(f'{ax.name}')
    plt.grid(True)
    relax()

    plt.close()


def switch_axes(mode='rectilinear'):
    cax = plt.gca()
    names = projections.get_projection_names()
    if mode not in names:
        # Ou une exception ??
        warnings.warn(f'Type de projection inconnue ({mode}) : possibilité {names}')
        return cax
    if cax.name != mode:
        print(f'##### Switch axes {cax.name} to {mode}')
        fig = plt.gcf()
        fig.delaxes(cax)
        return plt.gca(projection=mode)
    return cax


def demo():
    x, y, z = build_mesh()
    ax = switch_axes('3d')
    toto = ax.plot_surface(x, y, z, rstride=1, cstride=1)
    print(f'Type retour ax.plotsurface : {type(toto)}')
    plt.show()

    ax = switch_axes('3d')
    ax.contour3D(x, y, z, extend3d=False)
    plt.show()

    plt.clf()
    ax = switch_axes('3d')
    xs, ys, zs = build_lorenz()
    ax.plot3D(xs, ys, zs, linewidth=1.)
    plt.show()


def animation():
    ax = plt.gca()
    for angle in range(0, 90):
        ax.view_init(30, angle)
        plt.draw()
        plt.pause(.001)


def search_for(obj: object, name: str = '', title: str = ''):
    if len(title) > 0:
        title += ' : '
    print(f'##### {title}{obj}')

    if len(name) == 0:
        print(dir(obj))
    else:
        name = name.lower()
        for e in dir(obj):
            if name in e.lower():
                print(f'* {e}')


def where_is_canvas() -> None:
    """
    Essai pour trouver le canvas Qt créer par matplotlib, pour voir si il est possible de l'intégrer dans une
    appli C++

    On le trouve à deux endroits :
    * plt.get_current_fig_manager().canvas
    * plt.get_current_fig_manager().window.centralWidget()

    C'est bien une widget au sens Qt, on peut modifier le cursor.

    Reste à savoir si cet object python qui descend des classes QtWidgets.QWidget et FigureCanvasBase peut être
    caster en QWidget* ?
    Si oui, il serait alors possible de faire un setCentralWidget(canvas_matplotlib) !
    """
    ax = switch_axes('3d')
    xs, ys, zs = build_lorenz()
    print(f'##### Projection : {ax.name}')
    ax.plot3D(xs, ys, zs, linewidth=0.5)
    plt.show(block=False)

    if mpl.get_backend() in 'Qt5Agg':
        fig_manager = plt.get_current_fig_manager()
        search_for(fig_manager, 'canvas', 'Figure Manager')
        canvas = fig_manager.canvas
        print(f'fig_manager.canvas : {canvas}')
        print(f'\t{dir(canvas)}')
        window = fig_manager.window
        search_for(window, title='Window')
        # for e in dir(window):
        #     if 'canvas' in e.lower() or 'widget' in e.lower():
        #         print(f'* {e}')
        central_widget = window.centralWidget()
        print(f'fig_manager.window.central_widget : {central_widget}')
        print(f'\t{dir(central_widget)}')
        # search_for(central_widget, title='Central Widget')
        # -- fig_manager.canvas is fig_manager.window.central_widget
        print(f'Identique : {canvas is central_widget}')
        # FigureCanvasQT descend de QtWidgets.QWidget et FigureCanvasBase (selon les sources)
        search_for(canvas, 'widget', 'Canvas')
        # Sur le papier, depuis le C++, il doit être possible de caster cette widget en QWidget pour faire
        # un setCentralWidget(canvas) dans une autre appli C++
        # /!\ Est-ce que le QWidget *canvas = reinterpret_cast<QWidget*>(py_canvas); va fonctionner ?
        #   Oui, ça fonctionne (voir code en commentaire dans l'exemple Eastworld), mais son utilisation finit en SEGV !
        canvas.setCursor(QtCore.Qt.OpenHandCursor)
        print(f'Widget au sens Qt ? {canvas.isWidgetType()}')

        ax.plot3D(xs, ys, zs, linewidth=0.5)
        plt.show()



if __name__ == '__main__':
    list_names = projections.get_projection_names()
    print(f'Les projections : {list_names}')
    # solution_retenue()
    # where_is_canvas()
    # check_switch_axes()
    # clear_3d_axis()
    demo()
