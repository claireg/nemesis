# -*- coding: utf-8 -*-

import os

import numpy as np

import nemesis.readdata as nem_rd

# psy_unstruct_map

if __name__ == '__main__':
    data_path = os.path.join(nem_rd.get_data_dir(), 'herisson.psy')
    psy_dict = nem_rd.ReadPsyFile.read_psy_file(data_path)

    # data_0 = nem_rd.ReadPsyFile.get_curves_from_psy_file(data_path)

    page_names = nem_rd.ReadPsyFile.psy_page_name(psy_dict)
    assert len(page_names) == 1
    assert page_names[0] == 'GEOMETRIE3D_4'

    object_names = nem_rd.ReadPsyFile.psy_page(psy_dict, 'Page', 'GEOMETRIE3D_4')
    assert str(object_names[0]) == '[GEOMETRIE3D_4_scene, GEOMETRIE3D_4_title, GEOMETRIE3D_4_comment]'
    assert object_names[1] == ['StandardText', 'Scene', 'ColorLegend']

    frame_names = nem_rd.ReadPsyFile.psy_page_frame_name(psy_dict, 'GEOMETRIE3D_4')
    assert str(frame_names) == '[GEOMETRIE3D_4_scene, GEOMETRIE3D_4_title, GEOMETRIE3D_4_comment]'

    collection_names = nem_rd.ReadPsyFile.psy_page_collection_name(psy_dict, 'GEOMETRIE3D_4')
    assert collection_names == 'GEOMETRIE3D_4'

    # @FIXME pour l'instant collection_names est une string et non une liste de string
    graphics, next_types = nem_rd.ReadPsyFile.psy_collection(psy_dict, 'Collection', collection_names)
    assert len(graphics) == 283
    assert nem_rd.PsyObject('GEOMETRIE3D_4_Crb3D_901') in graphics
    assert next_types == ['DepthCurve', 'StandardCurve', 'SetOfCurve', 'VectorField', 'RaiseMap', 'UnstructuredRaiseMap']

    rep_typ = nem_rd.ReadPsyFile.psy_representation_type(psy_dict, 'GEOMETRIE3D_4_Crb3D_902')
    assert rep_typ == 'DepthCurve'

    graphic_names = nem_rd.ReadPsyFile.psy_page_graphic_name(psy_dict, 'GEOMETRIE3D_4')
    assert graphic_names == ['GEOMETRIE3D_4_Crb3D_901']

    scene_type = nem_rd.ReadPsyFile.psy_scene_type(psy_dict, 'GEOMETRIE3D_4')
    assert scene_type == 'DEPTH'

    # comment on récupère le nom de la scène ?
    object_names_2 = nem_rd.ReadPsyFile.psy_scene(psy_dict, 'Scene', 'GEOMETRIE3D_4_scene')
    assert object_names_2[0] == nem_rd.PsyEnum('GEOMETRIE3D_4')

    texts = nem_rd.ReadPsyFile.psy_text(psy_dict, 'GEOMETRIE3D_4')
    assert len(texts) == 2
    assert texts[0] == nem_rd.PsyStdTextData(title='Géométrie3D', posx=0.5, posy=0.96, alignment='center',
                                             name=nem_rd.PsyObject('GEOMETRIE3D_4_title'))
    assert texts[1] == nem_rd.PsyStdTextData(title='herisson.exp', posx=0.5, posy=0.0, alignment='center',
                                             name=nem_rd.PsyObject('GEOMETRIE3D_4_comment'))

    depth_curve = nem_rd.ReadPsyFile.psy_depth_curve(psy_dict, 'DepthCurve', 'GEOMETRIE3D_4_Crb3D_901')
    assert len(depth_curve) == 6
    assert depth_curve[0:3] == (5, False, True)
    assert depth_curve[3].all() == np.array([0., 103.764]).all()
    assert depth_curve[4].all() == np.array([0., 0.13185]).all()
    assert depth_curve[5].all() == np.array([0., 50.3624]).all()

    function_1183 = nem_rd.ReadPsyFile.psy_function(psy_dict, 'Function', 'GEOMETRIE3D_4_Crb3D_1183')
    assert len(function_1183) == 3
    assert function_1183[0].all() == np.array([0., -24.4187]).all()
    assert function_1183[1].all() == np.array([0., 5.42157]).all()
    assert function_1183[2].all() == np.array([0., 1.15536]).all()

    psy_data_1183 = nem_rd.ReadPsyFile.psy_function_data(psy_dict, 'GEOMETRIE3D_4')
    # [PsyCurveData] : psy_data_1183
    # il faut faire un pickle de la liste de PsyCurveData et faire une fonction de comparaison

    scene_labels = nem_rd.ReadPsyFile.psy_axes_label(psy_dict, 'GEOMETRIE3D_4')
    assert scene_labels == ('X', 'mm', 'Y', 'mm', 'Z', 'mm')

    scene_scales = nem_rd.ReadPsyFile.psy_axes_scaling(psy_dict, 'GEOMETRIE3D_4')
    linear = nem_rd.PsyEnum('LIN')
    assert scene_scales == (linear, linear, linear)

    xlb, xu, fl, fu = nem_rd.ReadPsyFile.psy_curve_axes_label(psy_dict, 'GEOMETRIE3D_4_Crb3D_1183')
    assert xlb == ['X', 'Y']
    assert xu == ['mm', 'mm']
    assert fl == 'Z'
    assert fu == 'mm'

    # ----------
    data_path = os.path.join(nem_rd.get_data_dir(), 'plusieurs_reseaux.psy')

    psy_dict = nem_rd.ReadPsyFile.read_psy_file(data_path)
    page_names = nem_rd.ReadPsyFile.psy_page_name(psy_dict)

    legend = nem_rd.ReadPsyFile.psy_legend(psy_dict, 'pg5_a1')
    assert len(legend) == 2
    # les espaces après R1 sont dans le fichier .psy d'origine
    assert legend[0] == ['R1      ', 'R2', 'R3']

    graphics, next_psy_type = nem_rd.ReadPsyFile.psy_collection(psy_dict, 'Collection', 'col5_a1')
    networks = nem_rd.ReadPsyFile.get_psy_object(psy_dict, 'SetOfCurve', 'res5_R1')
    c, (x, y) = nem_rd.ReadPsyFile.psy_net_curve(psy_dict, 'SetOfCurve', networks)
    # c = indice de la couleur
    assert c == 1
    # Il faut sauvegarder tous les tableaux dans un npz, pour ensuite le relire
    #   voir ce qui a été fait dans les tests existants
    # print(f'E : {type(x)}\t{x}')
    # print(f'E : {type(y)}\t{y}')

    # ----------
    data_path = os.path.join(nem_rd.get_data_dir(), 'plusieurs_courbes.psy')

    psy_dict = nem_rd.ReadPsyFile.read_psy_file(data_path)
    page_names = nem_rd.ReadPsyFile.psy_page_name(psy_dict)

    legend = nem_rd.ReadPsyFile.psy_legend(psy_dict, page_names[0])
    assert len(legend) == 2
    # les espaces après R1 sont dans le fichier .psy d'origine
    assert legend[0] == ['C1      ', 'C2', 'C3']

    graphics, next_psy_type = nem_rd.ReadPsyFile.psy_collection(psy_dict, 'Collection', 'col4_a1')
    curves = nem_rd.ReadPsyFile.get_psy_object(psy_dict, 'StandardCurve', 'crb4_C1')
    c, m, (x, y) = nem_rd.ReadPsyFile.psy_std_curve(psy_dict, 'StandardCurve', curves)
    # c = indice de la couleur
    assert c == 1
    assert m is False
    # Il faut sauvegarder tous les tableaux dans un npz, pour ensuite le relire
    #   voir ce qui a été fait dans les tests existants
    # print(f'X : {type(x)}\t{x}')
    # print(f'Y : {type(y)}\t{y}')

    # ----------
    data_path = os.path.join(nem_rd.get_data_dir(), 'vecteurs.psy')

    psy_dict = nem_rd.ReadPsyFile.read_psy_file(data_path)
    page_names = nem_rd.ReadPsyFile.psy_page_name(psy_dict)

    colormap = nem_rd.ReadPsyFile.psy_colormap_name(psy_dict)
    assert colormap == 'DefaultColorMap'

    graphics, next_psy_type = nem_rd.ReadPsyFile.psy_collection(psy_dict, 'Collection', 'FLD_02')
    vecfield = nem_rd.ReadPsyFile.get_psy_object(psy_dict, 'VectorField', 'FLD_02')
    x, y, u, v, h, c = nem_rd.ReadPsyFile.psy_vector_field(psy_dict, 'VectorField', vecfield)
    # Il faut sauvegarder tous les tableaux dans un npz, pour ensuite le relire
    #   voir ce qui a été fait dans les tests existants
    # print(f'type(x) = {type(x)}\t{x}')
    # print(f'type(y) = {type(y)}\t{y}')
    # print(f'type(u) = {type(u)}\t{u}')
    # print(f'type(v) = {type(v)}\t{v}')
    # print(f'type(h) = {type(h)}\t{h}')
    # print(f'type(c) = {type(c)}\t{c}')

    # ----------
    data_path = os.path.join(nem_rd.get_data_dir(), 'onde.psy')

    psy_dict = nem_rd.ReadPsyFile.read_psy_file(data_path)

    raise_map_name = nem_rd.ReadPsyFile.psy_raisemap_name(psy_dict)
    assert raise_map_name == 'Onde'

    colormap_name = nem_rd.ReadPsyFile.psy_colormap_name(psy_dict)
    x_tab, y_tab, z_tab, nb_col = nem_rd.ReadPsyFile.psy_map_data(psy_dict, raise_map_name, colormap_name)
    assert nb_col == 240
    # Il faut sauvegarder tous les tableaux dans un npz, pour ensuite le relire
    #   voir ce qui a été fait dans les tests existants
    # print(f'X : {type(x_tab)}\t{x_tab}')
    # print(f'Y : {type(y_tab)}\t{y_tab}')
    # print(f'Z : {type(z_tab)}\t{z_tab}')

    # autre façon de faire
    graphics, next_psy_type = nem_rd.ReadPsyFile.psy_collection(psy_dict, 'Collection', 'Onde')
    rmap = nem_rd.ReadPsyFile.get_psy_object(psy_dict, 'RaiseMap', 'Onde')
    x, y, h, c = nem_rd.ReadPsyFile.psy_struct_map(psy_dict, 'RaiseMap', rmap)
    # Il faut sauvegarder tous les tableaux dans un npz, pour ensuite le relire
    #   voir ce qui a été fait dans les tests existants
    # print(f'type(x) = {type(x)}\t{x}')
    # print(f'type(y) = {type(y)}\t{y}')
    # print(f'type(h) = {type(h)}\t{h}')
    # print(f'type(c) = {type(c)}\t{c}')

    # ----------
    data_path = os.path.join(nem_rd.get_data_dir(), 'coquilles2_herissons.psy')

    psy_dict = nem_rd.ReadPsyFile.read_psy_file(data_path)
    rmap = nem_rd.ReadPsyFile.get_psy_object(psy_dict, 'UnstructuredRaiseMap', 'GEOMETRIE3D_3_SHAPE_1751')
    x, y, h, c, t = nem_rd.ReadPsyFile.psy_unstruct_map(psy_dict, 'UnstructuredRaiseMap', rmap)
    # Il faut sauvegarder tous les tableaux dans un npz, pour ensuite le relire
    #   voir ce qui a été fait dans les tests existants
    # print(f'type(x) = {type(x)}\t{x}')
    # print(f'type(y) = {type(y)}\t{y}')
    # print(f'type(h) = {type(h)}\t{h}')
    # print(f'type(c) = {type(c)}\t{c}')
    # print(f'type(t) = {type(t)}\t{t}')
