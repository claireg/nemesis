# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt


def parametric_curve(n_theta=100):
    theta = np.linspace(-4 * np.pi, 4 * np.pi, n_theta)
    z = np.linspace(-2, 2, n_theta)
    r = z**2 + 1
    x = r * np.sin(theta)
    y = r * np.cos(theta)
    return x, y, z


if __name__ == '__main__':
    x, y, z = parametric_curve()
    ax = nem_plt.PyPlotCEA.switch_axes('3d')
    ax.plot(x, y, z, label='$f(z) = sin(theta), cos(theta)$')
    print(f'X : {plt.gca().get_xlim()}')
    print(f'Y : {plt.gca().get_ylim()}')
    print(f'Z : {plt.gca().get_zlim()}')
    plt.xlim((-4., 4.))
    plt.xlabel('Abscisses')
    plt.ylim((-5, 6))
    plt.ylabel('Ordonnées')
    plt.gca().set_zlim((-2.5, 1.5))
    plt.show()
