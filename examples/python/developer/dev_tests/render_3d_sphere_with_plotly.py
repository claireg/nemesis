# -*- coding: utf-8 -*-

"""
Petit test pour vérifier que `plotly` pourrait résoudre nos problèmes de 3D.

Le package `plotly` est installé dans l'environnement virtuel cersei sur le portable pro.
J'ai pas essayé de personnaliser la figure, les couleurs.
Le rendu se fait dans un browser web. C'est intégrable dans `jupyter` / `jupyterlab`.

Installation simple (attention si besoins dans jypyter ou jypyterlab, fa
"""
import numpy as np
import plotly.graph_objects as go

n_theta = 50
n_phi = 100

def quart(r):
    theta, phi = np.mgrid[0.:0.5 * np.pi:n_theta * 1j, 0.:np.pi:n_phi * 1j]
    return r * np.sin(theta) * np.cos(phi),\
           r * np.sin(theta) * np.sin(phi), \
           r * np.cos(theta)


r = 0.5
x1, y1, z1 = quart(0.5)
x2, y2, z2 = quart(1.)
x3, y3, z3 = quart(2.)

fig = go.Figure(data=[go.Surface(z=z1, x=x1, y=y1), go.Surface(z=z2, x=x2, y=y2), go.Surface(z=z3, x=x3, y=y3)])
fig.update_layout(title='Spheres', autosize=False,
                  width=1200, height=800,
                  margin=dict(l=65, r=50, b=65, t=90))
fig.show()