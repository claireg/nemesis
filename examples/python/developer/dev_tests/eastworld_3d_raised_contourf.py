# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd


if __name__ == '__main__':
    x, y, z = nem_rd.waves(30, 30)
    print(f'{nem_rd.narray_prop(x)}, {nem_rd.narray_prop(y)}, {nem_rd.narray_prop(z)}')
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current',
                                      label='$x^2 * cos(y) + y^2*sin(x)$')
    plt.gca().view_init(25, -65)
    print(plt.gca().get_zlim())
    plt.gca().set_zlim((-1600, 1600))
    print(plt.gca().get_zlim())
    plt.show()
