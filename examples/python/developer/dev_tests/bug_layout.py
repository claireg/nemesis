# -*- coding: utf-8 -*-

from typing import List
import warnings

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import matplotlib.axes as maxes
import nemesis.plotter as nem_plt

xmin = None
xmax = None
ymin = None
ymax = None
cformat = 'r-'


def curves(format_):
    x = np.linspace(-np.pi, np.pi, 201)
    globals()['xmin'] = x.min()
    globals()['xmax'] = x.max()
    plt.plot(x, np.sin(x), format_, label='sinus')
    plt.plot(x, np.cos(x), format_, label='cosinus')
    globals()['ymin'] = -1
    globals()['ymax'] = 1
    # plt.legend()
    nem_plt.PyPlotCEA.networks_legend(['trigonometry'], [0, ])


def curve(format_):
    x = np.linspace(-np.pi, np.pi, 201)
    globals()['xmin'] = x.min()
    globals()['xmax'] = x.max()
    globals()['ymin'] = -1
    globals()['ymax'] = 1
    plt.plot(x, np.sin(x), format_, label='sinus')
    # plt.legend()
    nem_plt.PyPlotCEA.networks_legend(['trigonometry'], [0, ])


def curve2(format_):
    x = np.linspace(-np.pi, np.pi, 201)
    globals()['xmin'] = x.min()
    globals()['xmax'] = x.max()
    globals()['ymin'] = -1
    globals()['ymax'] = 1
    plt.plot(x, np.sin(x), format_, label='sinus')
    # plt.legend()
    bbx_2 = (0.002, 0.002, 0.995, 0.102)
    plt.legend(bbox_to_anchor=bbx_2, bbox_transform=plt.gcf().transFigure, ncol=2,
               mode="expand", borderaxespad=0., loc='lower center')


def cycle():
    x = np.linspace(0, 5, 100)
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color'][:5]
    styles = ['solid', 'solid', 'dashed', 'dashed', 'dashed']
    for i, i_color, style in zip(range(5), colors, styles):
        plt.plot(x, np.sin(x) - .1 * i, c=i_color, ls=style)

    for i, i_color, style in zip(range(5), colors, styles):
        plt.plot(x, np.cos(x) - .1 * i, c=i_color, ls=style)

    nem_plt.PyPlotCEA.networks_legend(['sinus 1', 'sinus 2', 'cosinus 1', 'cosinus 2'],
                                      [0, 2, 5, 7], loc='lower center')


def proto_0():
    plt.figure('Chrono')
    # nem_plt.PyPlotCEA.set_figure_xy(618, 28, 816, 1020)
    nem_plt.PyPlotCEA.set_linewidth(2.)
    plt.title('SubTitle')
    nem_plt.PyPlotCEA.set_titles('AAAAA', 'BBBBB', 'CCCCC')

    # curves(cformat)
    # curve(cformat)
    # curve2(cformat)
    cycle()

    plt.xlabel('Abscisses')
    plt.ylabel('Ordonnées')
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.pause(2)

    print('Bougé !')
    plt.tight_layout()
    plt.show()


def get_data(n_points: int) -> [np.array, List[np.array], List[str]]:
    arr = np.linspace(0, n_points, num=n_points)
    x = arr*2*np.pi / 360.
    yy = [np.cos(x), np.sin(x), np.arccos(x), np.arcsin(x), np.arctan(x), np.tanh(x)]
    labels = ['cos(x)', 'sin(x)', 'acos(x)', 'asin(x)', 'atan(x)', 'tanh(x)']
    return x, yy, labels


def local_move_axes_net_legend(parent, **kwargs):
    loc = kwargs.pop('loc', 'lower center')
    if loc not in ('upper right', 'upper left', 'upper center', 'lower right', 'lower left', 'lower center'):
        warnings.warn(f'{loc} : Unauthorized legend location for networks. \'lower\' center will be used.')
        loc = 'lower center'
    fraction = kwargs.pop('fraction', 0.1)
    shrink = kwargs.pop('shrink', 1.0)
    # aspect = kwargs.pop('aspect', 20)
    # pb = transforms.PBox(parent.get_position())
    pb = parent.get_position(original=True).frozen()
    print(f'# Parent position : {pb}')
    if 'lower' in loc:
        pad = kwargs.pop('pad', 0.1)  # 0.05 ok si pas de labels de tracer
        pbcb, pbx, pb1 = pb.splity(fraction, fraction + pad)
        pbcb = pbcb.shrunk(shrink, 1.0).anchored('C', pbcb)
        # aspect = 1.0 / aspect
        anchor = kwargs.pop('anchor', (0.25, 1.0))
        panchor = kwargs.pop('panchor', (0.25, 0.0))
    else:
        pad = kwargs.pop('pad', 0.05)
        x1 = 1.0 - fraction
        pb1, pbx, pbcb = pb.splity(x1 - pad, x1)
        pbcb = pbcb.shrunk(1.0, shrink).anchored('C', pbcb)
        anchor = kwargs.pop('anchor', (0.0, 0.25))
        panchor = kwargs.pop('panchor', (1.0, 0.25))
    parent.set_position(pb1)
    parent.set_anchor(panchor)
    return anchor, pbcb


def local_networks_legend(labels: List[str], idx0: List[int], loc: str = 'lower center') -> None:
    try:
        llc = nem_plt.misc.create_legend_line_collections(idx0)
    except RuntimeError as e:
        warnings.warn(f'Impossible de créer la légende de réseaux : {e}', RuntimeWarning)
        return None
    else:
        ax = plt.gca()
        # ax.set_ajustable('box')
        print(f'Window extent : {ax.get_window_extent()}')
        # Avec loc='lower center', il faut pad=0.1
        anchor, pbcb = local_move_axes_net_legend(plt.gca(), loc=loc)
        print(f'Anchor : {anchor}')
        print(f'pbcb : {pbcb}')
        print(f'Window extent : {ax.get_window_extent()}')
        # plt.legend(llc, labels, handler_map={type(llc[0]): nem_plt.misc.HandlerDashedLines()}, handleheight=3,
        #            bbox_transform=plt.gcf().transFigure, loc=loc,
        #            bbox_to_anchor=pbcb, ncol=len(idx0))
        # Plus joli si pour loc, on met 'best' (matplotlib la positionne mieux que nous, et comme on a modifié
        #  la bbox des axes, ça fonctionne bien
        plt.legend(llc, labels, handler_map={type(llc[0]): nem_plt.misc.HandlerDashedLines()}, handleheight=2,
                   bbox_transform=plt.gcf().transFigure, loc='best',
                   bbox_to_anchor=pbcb, ncol=len(idx0))


def proto_1():
    # Identique à MainWindow::plot_all_and_delete_one_call de l'exemple Eastworld
    x, yy, labels = get_data(200)
    for y, t in zip(yy, labels):
        plt.plot(x, y, label=t)
    plt.gca().set_xlabel('Abscisses')
    plt.gca().set_ylabel('Ordonnées')
    net_labels = [f'{labels[i]} / {labels[i+1]}' for i in range(0, len(labels), 2)]
    net_idx = [i for i in range(0, len(labels), 2)]
    local_networks_legend(net_labels, net_idx)
    plt.show()


if __name__ == '__main__':
    mpl.use('Qt5Agg')
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_cmap('rainbow')
    proto_0()
    proto_1()
