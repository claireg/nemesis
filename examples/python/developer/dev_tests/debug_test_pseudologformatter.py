# -*- coding: utf-8 -*-
"""
Module pour debugger les tests Python de nemesis.
"""

import nemesis.plotter as nem_plt


def pseudologformatter():
    value = 0.001
    ref_math = '$10^{0}$'
    ref_data = '1.01times10^1'
    ref_short = '1.00231     '
    pseudo = nem_plt.PseudoLogMathTextSciFormatter()
    assert pseudo(value) == ref_math
    assert pseudo.format_data(value) == ref_data
    assert pseudo.format_data_short(value) == ref_short


if __name__ == '__main__':
    pseudologformatter()
