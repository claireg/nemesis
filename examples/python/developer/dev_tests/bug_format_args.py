# -*- coding: utf-8 -*-

import os
import pickle

import numpy as np

import nemesis.plotter as nem_plt
import nemesis.tools as nem_tools

#
# Bugs
# * si type str : manque les '' ?


def load_data():
    import pickle
    dir_path = os.path.dirname(os.path.realpath(__file__))
    basename = os.path.basename(os.path.realpath(__file__))
    base_ = basename.split('.')
    filename_ = f'{base_[0]}_data.pickle'
    pick_file = os.path.join(dir_path, filename_)
    result = pickle.load(open(pick_file, 'rb'))
    return result['x'], result['y'], result['z']


def load_perf(filename_):
    if os.path.exists(filename_):
        b_data = pickle.load(open(filename_, 'rb'))
        return True, b_data
    return False, None


if __name__ == '__main__':
    here = os.path.dirname(__file__)
    base = os.path.join('..', 'tests_and_profiling')
    fn_no_autolog = os.path.join(base, 'no_autolog.pickle')
    fn_autolog = os.path.join(base, 'autolog.pickle')
    fn_uselog = os.path.join(base, 'uselog.pickle')
    fn_sessionlog = os.path.join(base, 'sessionlog.pickle')

    infolog = nem_tools.get_log_varenv()
    print(f'# Infolog : {infolog}')
    if not infolog.autolog:
        filename = fn_no_autolog
    else:
        if infolog.uselog and infolog.sessionlog:
            filename = fn_autolog
        elif infolog.uselog and not infolog.sessionlog:
            filename = fn_uselog
        else:
            # not infolog.uselog and infolog.sessionlog
            filename = fn_sessionlog

    if os.path.exists(filename):
        intervals = pickle.load(open(filename, 'rb'))
    else:
        intervals = []

    with nem_tools.Timer() as timer:
        x_array, y_array, z_array = load_data()
        nem_plt.PyPlotCEA.nemesis_params()
        nem_plt.PyPlotCEA.set_figure_xy(x=556, y=91, width=800, height=600)
        nem_plt.PyPlotCEA.set_window_title(title='Eastworld')
        nem_plt.PyPlotCEA.set_linewidth(width=1.0)
        nem_plt.PyPlotCEA.logarithmic_contourf(x=x_array, y=y_array, z=z_array,
                                                masked_value=0.0, cmap='viridis', label='$x^2 * cos(y) + y^2*sin(x)$')
        nem_plt.PyPlotCEA.set_titles(title_bbl='TITRE', sub_title='... SOUS TITRE...', title_mat='MAT',
                                     title_bbl_pos=(0.0, 0.95), title_bbl_font_size=20, sub_title_pos=(0.1, 0.9),
                                     sub_title_font_size=12, title_mat_pos=(-1.0, 0.9), title_mat_font_size=16)
    # Refaire idem sans autolog (commenté dans le code de pyplotcea)
    # Les premiers tests montrent une différence inférieur à 10-4 pour la fonction logarithmic_contourf
    print('Total in seconds : {}'.format(timer.interval))
    intervals.append(timer.interval)
    pickle.dump(intervals, open(filename, 'wb'), pickle.HIGHEST_PROTOCOL)

    avail_data = []
    avail_name = []
    ex_autolog, b_with_auto = load_perf(fn_autolog)
    if ex_autolog:
        avail_name.append('autolog')
        avail_data.append(b_with_auto)
    ex_no_autolgo, b_without_auto = load_perf(fn_no_autolog)
    if ex_no_autolgo:
        avail_name.append('no_autolog')
        avail_data.append(b_without_auto)
    ex_sessionlog, b_sessionlog = load_perf(fn_sessionlog)
    if ex_sessionlog:
        avail_name.append('sessionlog')
        avail_data.append(b_sessionlog)
    ex_uselog, b_uselog = load_perf(fn_uselog)
    if ex_uselog:
        avail_name.append('uselog')
        avail_data.append(b_uselog)

    avail_len = [ len(data) for data in avail_data ]
    print('# Bilan condensé de tous les run : ')
    for name, s in zip(avail_name, avail_len):
        print(f'\t* {name:15} : {s:03}')
    i_min = np.argmin(avail_len)
    nb_items = len(avail_data[i_min])
    np_data = [ np.array(data[:nb_items]) for data in avail_data ]
    print('Moyennes : ')
    for name, data in zip(avail_name, np_data):
        print(f'\t* {name}: {data.mean():.4f}')
