# -*- coding: utf-8 -*-
"""
Module pour debugger les tests Python de nemesis.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd


def contour_data():
    x, y, z = nem_rd.gaia_isolines_levels()
    return x, y, z


def get_contours_logarithmic_contourf():
    ref_levels = np.array([1.e-322, 1.e-300, 1.e-278, 1.e-256, 1.e-234, 1.e-212, 1.e-190, 1.e-168, 1.e-146, 1.e-124,
                           1.e-102, 1.e-080, 1.e-058, 1.e-036, 1.e-014, 1.e+008, 1.e+030], dtype=float)
    ref_layers = np.array([5.e-301, 5.e-267, 5.e-233, 5.e-199, 5.e-165, 5.e-131, 5.e-097, 5.e-063, 5.e-029,
                           5.e+005, 5.e+039], dtype=float)
    ref_iso_levels = np.array([1.e-266, 1.e-198, 1.e-130, 1.e-062, 1.e-028], dtype=float)
    x, y, z = contour_data()
    nem_plt.PyPlotCEA.logarithmic_contourf(x, y, z, masked_value=1.e-300)
    levels = nem_plt.PyPlotCEA.get_contourf_levels()
    layers = nem_plt.PyPlotCEA.get_contourf_layers()
    nem_plt.PyPlotCEA.logarithmic_contour(x, y, z, levels=ref_iso_levels)
    iso_levels = nem_plt.PyPlotCEA.get_contour_levels()
    assert ref_levels.all() == levels.all()
    assert ref_layers.all() == layers.all()
    assert ref_iso_levels.all() == iso_levels.all()


def isocolorbar_log():
    x, y, z = nem_rd.gaia_isolines_levels_2()
    levels = [1.72089643e-01, 1.92639705e+00, 2.15643750e+01, 2.41394821e+02, 2.70220952e+03, 3.02489351e+04,
              3.38611078e+05, 3.79046275e+06, 4.24310036e+07, 4.74978964e+08]
    cs = plt.contour(x, y, z, levels=levels)
    formatter = ticker.LogFormatterSciNotation(minor_thresholds=(np.inf, np.inf))
    current_axes = plt.gca()
    current_axes.clear()
    cb = nem_plt.PyPlotCEA.isocolorbar(cs, cax=current_axes, format=formatter, drawedges=False, )
    cb.set_label('No Labels !')
    plt.show()


if __name__ == '__main__':
    get_contours_logarithmic_contourf()
    isocolorbar_log()
