# -*- coding: utf-8 -*-

import nemesis.testing as nem_tst

TEST_DIR = nem_tst.get_objects_test_dir(__file__)


def read_psy_file_onde():
    tst_name = 'read_psy_file_onde'
    psy_name = 'onde.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


if __name__ == '__main__':
    read_psy_file_onde()
