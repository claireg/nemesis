# -*- coding: utf-8 -*-

"""
Module pour debugger test_log_triangulation_1_0 et test_log_triangulation_1_1
suite au passage en matplotlib 3.1.1

En fait, ces deux tests réduisent le nombre de décades des données en masquant les données sous un certain seuil.

Dans matplotlib 3.2.1 et 3.1.1, il n'est plus possible de masquer des données à trianguler car `matplotlib` dans
`matplotlib/tri/tricontour.py/_contour_args`, masque les z pour supprimer les NaNs et infs (et on ne peut pas masquer 2
fois dans un tableau ?)
En matplotlib 3.0.3, il n'y avait pas de vérifications du masquage des données ... d'ailleurs les valeurs sans «sens»
n'étaient pas masquées.

Pas de correction envisagées dans l'immédiat, car j'arrive plus trop à savoir pourquoi on avait mis ça en place
(sûrement pour réduire l'échelle de couleurs ...)

Les résultats obtenus en supprimant la modification des valeurs de z sont comparables
        * dans `nemesis/plotter/contour.py`, fct `log_transform_z` (mise en commentaire de `self._update_z_for_log()`)
        * par contre moins de niveaux de couleurs dans l'échelle de couleurs que précédemment
"""

import numpy as np
import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt


def tricontour_data() -> (np.array, np.array, np.array):
    n_angles = 48
    n_radii = 8
    min_radius = 0.25
    radii = np.linspace(min_radius, 0.95, n_radii)

    angles = np.linspace(0, 2 * np.pi, n_angles, endpoint=False)
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)
    angles[:, 1::2] += np.pi / n_angles

    x = (radii * np.cos(angles)).flatten()
    y = (radii * np.sin(angles)).flatten()
    z = np.fabs(np.cos(radii) * np.cos(3 * angles)).flatten()

    return x, y, z


def log_triangulation(withf: bool = True):
    x, y, z = tricontour_data()
    z = np.power(10, z * 16)
    fct = nem_plt.PyPlotCEA.logarithmic_tricontourf if withf is True else nem_plt.PyPlotCEA.logarithmic_tricontour
    fct(x, y, z, n_decades=2, max_decades=5)
    # fct = plt.tricontourf if withf is True else plt.tricontour
    # fct(x, y, z)
    plt.show()


if __name__ == '__main__':
    log_triangulation(withf=True)
    log_triangulation(withf=False)
