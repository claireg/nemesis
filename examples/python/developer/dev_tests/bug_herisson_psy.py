# -*- coding: utf-8 -*-

import os

import nemesis.readdata as nem_rd


if __name__ == '__main__':
    data_path = os.path.join(nem_rd.get_data_dir(), 'herisson.psy')
    data = nem_rd.get_curves_from_psy_file(data_path)
