# -*- coding: utf-8 -*-

import numpy as np
import matplotlib as mpl

mpl.use('Qt5Agg')
import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt

if __name__ == '__main__':
    x = np.linspace(0, 5000, 5000)
    y = np.linspace(0, 5000, 5000)
    y = np.sin(2 * np.pi * y / 360.)

    plt.ion()
    plt.plot(x, y, label='toto')
    plt.show(block=True)

    nem_plt.PyPlotCEA.set_window_title("Tests Unitaires avec Catch")
    plt.plot(x, y, label='toto')
    nem_plt.PyPlotCEA.set_label(0, "TOTO")
    plt.legend()
    plt.draw()
    plt.show(block=True)
    # dessine dans un certain style (valable pour ce qui est dessiné après)
    plt.xkcd()
    plt.clf()
    plt.plot(x, y, label='Courbe')
    plt.show(block=True)
    plt.close()
