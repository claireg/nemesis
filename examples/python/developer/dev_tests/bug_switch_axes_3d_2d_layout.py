# -*- coding: utf-8 -*-
from dataclasses import dataclass
from typing import Dict, Callable

from matplotlib import rcParams
import matplotlib.pyplot as plt
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import nemesis.readdata as nem_rd
import nemesis.plotter as nem_plt


@dataclass
class MyParams:
    left: float
    right: float
    bottom: float
    top: float
    wspace: float
    hspace: float


# mode: matplotlib.figure.SubplotParams
current_adj: Dict[str, MyParams] = {}
key_subplots = {'left': 'figure.subplot.left', 'right': 'figure.subplot.right',
                'bottom': 'figure.subplot.bottom', 'top': 'figure.subplot.top',
                'wspace': 'figure.subplot.wspace', 'hspace': 'figure.subplot.hspace'}


def repr_figure_params():
    global key_subplots
    s = [f'\t* {key}: {rcParams[value]}' for key, value in key_subplots.items()]
    return '\n'.join(s)


def repr_current_adj():
    global current_adj
    return '\n'.join([f'\t* {key.upper()}: {value}' for key, value in current_adj.items()])


def switch(mode):
    global current_adj
    cax = plt.gca()
    fig = plt.gcf()
    if cax.name != mode:
        fig.delaxes(cax)
        fig.clear()
        # Sauvegarde des anciennes valeurs
        if cax.name not in current_adj:
            print(f'Ajout de {cax.name}')
            current_adj[cax.name] = MyParams(left=fig.subplotpars.left, right=fig.subplotpars.right,
                                             bottom=fig.subplotpars.bottom, top=fig.subplotpars.top,
                                             wspace=fig.subplotpars.wspace, hspace=fig.subplotpars.hspace)
        cax = plt.gca(projection=mode)
        if mode == '3d':
            if mode not in current_adj:
                print(f'Ajout de {cax.name}')
                current_adj[mode] = MyParams(left=0.050, right=0.950, bottom=0.050, top=0.920,
                                             wspace=fig.subplotpars.wspace, hspace=fig.subplotpars.hspace)
        adj = current_adj[mode]
        print(f'Application mode {mode}')
        # print(adj)
        fig.subplots_adjust(left=adj.left, bottom=adj.bottom, right=adj.right, top=adj.top)
        # figure.subplot.* ne semblent jamais changer ... Pourquoi ?
        # print(repr_figure_params())
    return cax


def axis_text(cax):
    cax.set_xlabel('Abscisses')
    cax.set_ylabel('Ordonnées')
    if cax.name == '3d':
        cax.set_zlabel('Cotes')


def set_text(cax):
    axis_text(cax)
    plt.title(cax.name, fontsize=16, fontweight='bold')


def set_nem_text(cax):
    axis_text(cax)
    nem_plt.PyPlotCEA.set_titles('BLBLBL', cax.name, 'MATMATMAT')


fun_text = set_nem_text
# fun_text = set_text
t_pause = 4


def plot_2d(next_act: bool = True, switch_fun: Callable = switch):
    global t_pause
    cax = switch_fun('rectilinear')
    z = nem_rd.wavelets_2d()
    for zz in z:
        cax.plot(zz)
    fun_text(cax)
    step = len(z) // 2
    labels = []
    idx = []
    for i in range(0, len(z), step):
        labels.append(f'G {i}')
        idx.append(i)
    nem_plt.PyPlotCEA.networks_legend(labels=labels, idx0=idx)
    if next_act is True:
        plt.pause(t_pause)
    else:
        plt.show()


def plot_3d(next_act: bool = True, switch_fun: Callable = switch):
    global t_pause
    cax = switch_fun('3d')
    data = nem_rd.parametric()
    cax.plot3D(*data, 'o-')
    fun_text(cax)
    if next_act is True:
        plt.pause(t_pause)
    else:
        plt.show()


if __name__ == '__main__':
    nem_plt.PyPlotCEA.nemesis_params()
    fun = nem_plt.PyPlotCEA.switch_axes
    # fun = switch
    plot_2d(switch_fun=fun)
    plot_3d(switch_fun=fun)
    plot_2d(switch_fun=fun)
    print(f'\n\n# subplotparams stockés : \n{repr_current_adj()}')
