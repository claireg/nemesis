# -*- coding: utf-8 -*-

import itertools

from matplotlib import pyplot as plt
import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

waves_label = '$x^2 * cos(y) + y^2*sin(x)$'


def custom_print_dir(obj, wanted=None):
    for item in dir(obj):
        if wanted is None:
            print(item)
        else:
            ok = False
            for result in itertools.filterfalse(lambda key: key not in item, wanted):
                if ok or result:
                    ok = True
            if ok:
                print(item)


def _raised_contourf_text(scales):
    x, y, z = nem_rd.waves(10, 10)
    xscale, yscale, zscale = scales.split('_')
    fig = plt.figure()
    if zscale == 'log':
        add_kwargs = {'masked_value': 1.e-300}
    else:
        add_kwargs = {}
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, xscale=xscale, yscale=yscale, zscale=zscale,
                                      cmap='current', label=waves_label, **add_kwargs)
    cax = plt.gca()
    cax.view_init(25, -65)
    nem_plt.PyPlotCEA.zlabel('Cotes')
    lbl = nem_plt.PyPlotCEA.zlabel()
    print(f'##### Label après : {lbl}')
    plt.show()
    # custom_print_dir(cax, ['tick', 'label'])
    # print(f'##### {scales}')
    # print(cax.get_xticks())
    # print(cax.get_yticks())
    # print(cax.get_zticks())
    # print(type(cax.get_xticks()))


def _named_contourf(scales):
    print(f'##### {scales}')
    x, y, z = nem_rd.waves(20, 20)
    xscale, yscale = scales.split('_')
    xscale = True if xscale == 'log' else False
    yscale = True if yscale == 'log' else False
    print(xscale)
    nem_plt.PyPlotCEA.linear_contourf(x, y, z)
    nem_plt.PyPlotCEA.set_log_scale(xscale, yscale)
    plt.show()


if __name__ == '__main__':
    # _raised_contourf_text('lin_lin_lin')
    # _raised_contourf_text('log_lin_lin')
    # _raised_contourf_text('log_log_lin')
    _raised_contourf_text('log_log_log')
    # _named_contourf('lin_lin')
    # _named_contourf('log_lin')
    # _named_contourf('lin_log')
    # _named_contourf('log_log')
    # a = nem_plt.PseudoLogMathTextSciFormatter()
    # value = 0.001
    # math_text = a(value)
    # data = a.format_data(value)
    # short_data = a.format_data_short(value)
    # print(f'Value : #{value}#\nMath : #{math_text}#\nData : #{data}#\nShort = #{short_data}#')
