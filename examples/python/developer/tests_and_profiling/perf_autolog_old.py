# -*- coding: utf-8 -*-

"""
Pour avoir plus de donner, il faut exécuter ../dev_tests/bug_format_args.py avec ou sans la variable d'environnement
NEMESIS_NO_AUTOLOG
"""
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    base = os.path.dirname(__file__)
    fn_without = os.path.join(base, 'without_autolog.pickle')
    fn_with = os.path.join('with_autolog.pickle')

    if os.path.exists(fn_with) and os.path.exists(fn_without):
        b_with_auto = pickle.load(open(fn_with, 'rb'))
        b_without_auto = pickle.load(open(fn_without, 'rb'))
        nb_items = len(b_with_auto) if len(b_with_auto) < len(b_without_auto) else len(b_without_auto)
        with_auto = np.array(b_with_auto[:nb_items])
        without_auto = np.array(b_without_auto[:nb_items])
        mean_with = with_auto.mean()
        mean_without = without_auto.mean()
        diff_auto = with_auto - without_auto
        ecart = diff_auto * 0.5
        mean_ecart = ecart.mean()

        plt.plot(with_auto, '-', [mean_with, ] * nb_items, '--', label='avec autolog')
        plt.plot(without_auto, '-', [mean_without, ] * nb_items, '--', label='sans autolog')
        # plt.plot(ecart, '-', [mean_ecart, ] * nb_items, '--', label='écart')

        x = (nb_items - 1)
        deb = (x + 0.1, mean_without)
        fin = (x + 0.1, mean_with)
        y = mean_ecart + mean_without
        perc = (100 * mean_ecart) / mean_without

        plt.annotate('', xy=deb, xytext=fin, arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
        plt.text(x + 0.2, y, f'{mean_ecart:.2f}s')
        plt.annotate('', xy=(0, mean_without), xytext=(0, mean_with),
                     arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
        plt.text(0.2, y, f'{perc:.2f}%')
        yh = without_auto.min()
        plt.annotate('', xy=(16, yh), xytext=(30, yh), arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
        yh2 = yh + (yh * 0.01)
        plt.text(21, yh2, 'youtube')
        # plt.legend()
        plt.show()
