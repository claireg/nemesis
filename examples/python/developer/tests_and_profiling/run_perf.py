# -*- coding: utf-8 -*-

import itertools

import matplotlib.pyplot as plt
from matplotlib import collections, colors
from memory_profiler import profile

import nemesis.readdata as nem_rd
import nemesis.tools as spock


# @spock.LoggerTimer('create_np_array')
def create_np_array():
    i_x, i_ly = nem_rd.curves_rx_data()
    # print(nem_rd.narray_prop(i_ly))
    return i_x, i_ly


def create_np_array_network_2():
    i_x1, i_x2, i_ly = nem_rd.gaia_network_2()
    # print(nem_rd.narray_prop(i_ly))
    return i_x1, i_x2, i_ly


@spock.LoggerTimer('loglog_loop')
def loglog_loop(ax_, x_, ly_):
    """
    Boucle pour afficher pls courbes dans une seule figure.

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    ly_ : liste des ordonnées

    Notes
    -----
    Temps d'exécution : entre 0.18 et 0.21 secondes.
    """
    for y in ly_:
        ax_.loglog(x_, y)
    ax_.set_xlim(x_.min(), x_.max())


@spock.LoggerTimer('loglog_one')
def loglog_one(ax_, x_, plt_args_):
    """
    Affichage de plusieurs courbes dans une seule figure en un seul appel

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    plt_args_  : liste x, y0, x, y1, …

    Notes
    -----
    Temps d'exécution : entre 0.031 et 0.032 secondes, hors preparation des données.
    """
    ax_.loglog(*plt_args_)
    ax_.set_xlim(x_.min(), x_.max())


@spock.LoggerTimer('loglog_one_with_prepare')
def loglog_one_with_prepare(ax_, x_, ly_):
    """
    Affichage de plusieurs courbes dans une seule figure en un seul appel

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    ly_  : liste des ordonnées

    Notes
    -----
    Temps d'exécution : entre 0.031 et 0.032 secondes, avec preparation des données.
    """
    plt_args = prepare_one(x_, ly_)
    ax_.loglog(*plt_args)
    ax_.set_xlim(x_.min(), x_.max())


@spock.LoggerTimer('plot_loop')
def plot_loop(ax_, x_, ly_):
    """
    Boucle pour afficher pls courbes dans une seule figure.

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    ly_ : liste des ordonnées

    Notes
    -----
    Temps d'exécution : Entre 0.19 et 0.2 secondes.
    """
    for y in ly_:
        ax_.loglog(x_, y)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")


@spock.LoggerTimer('plot_one')
def plot_one(ax_, x_, plt_args_):
    """
    Affichage de plusieurs courbes dans une seule figure en un seul appel.
    plot(x0, y0, x1, y1, ...)

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    plt_args_  : liste x, y0, x, y1, …

    Notes
    -----
    Temps d'exécution : 0.032 et 0.034 secondes
    """
    ax_.plot(*plt_args_)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")


@spock.LoggerTimer('plot_collections')
def plot_collections(ax_, x_, ly_):
    """
    Affichage de plusieurs courbes dans une seule figure en un seul appel via la création d'une collections.
    plot(x, [y0, y1, ...])

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    ly_  : liste y0, y1, …

    Notes
    -----
    Temps d'exécution : 0.012 et 0.015 secondes
    Le cycle sur les couleurs ne fonctionne pas.
    """
    color_seg_base = [colors.to_rgba(c) for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]
    line_seg = collections.LineCollection([list(zip(x, y)) for y in ly_])
    line_seg.set_array(x)
    color_seg = [current_color for (current_color, i) in zip(itertools.cycle(color_seg_base), range(len(ly_)))]
    line_seg.set_color(color_seg)
    ax_.add_collection(line_seg)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")


@spock.LoggerTimer('plot_one_with_prepare')
def plot_one_with_prepare(ax_, x_, ly_):
    """
    Affichage de plusieurs courbes dans une seule figure en un seul appel

    Parameters
    ----------
    ax_ : sytème d'axes dans lequel dessiner
    x_ : abscisses communs
    ly_  : liste des ordonnées

    Notes
    -----
    Temps d'exécution :  0.032 et 0.034 secondes.
    """
    plt_args = prepare_one(x_, ly_)
    ax_.plot(*plt_args)
    ax_.set_xlim(x_.min(), x_.max())
    ax_.set_xscale("log")
    ax_.set_yscale("log")


def prepare_one(x_, ly_):
    args = []
    for y in ly_:
        args.append(x_)
        args.append(y)
    return args


def run_loglog(x_, ly_):
    fig, axs = plt.subplots(1, 3, figsize=(15, 12))
    loglog_loop(axs[0], x_, ly_)
    axs[0].set_title('plot_loop')
    plt_args = prepare_one(x_, ly_)
    loglog_one(axs[1], x_, plt_args)
    axs[1].set_title('plot_one')
    loglog_one_with_prepare(axs[2], x_, ly_)
    axs[1].set_title('plot_one_with_prepare')


@spock.LoggerTimer('cppy_cmp_network_2_loglog_loop')
def cpp_py_cmp_network_2_loglog_loop(x_, ly_):
    plt.clf()
    for y in ly_:
        plt.loglog(x_, y)
    plt.xlim(x_.min(), x_.max())


@spock.LoggerTimer('cpy_cmp_network_2_loglog_one')
def cpp_py_cmp_network_2_loglog_one(x_, ly_):
    plt.clf()
    plt_args = prepare_one(x_, ly_)
    plt.loglog(*plt_args)
    plt.xlim(x_.min(), x_.max())


@profile
def run_plot(x_, ly_):
    fig, axs = plt.subplots(1, 4, figsize=(15, 12))

    plot_loop(axs[0], x_, ly_)
    axs[0].set_title('plot_loop')

    plt_args = prepare_one(x_, ly_)
    plot_one(axs[1], x_, plt_args)
    axs[1].set_title('plot_one')

    plot_one_with_prepare(axs[2], x_, ly_)
    axs[2].set_title('plot_one_with_prepare')

    plot_collections(axs[3], x_, ly_)
    axs[3].set_title('plot_one_x_ny')


if __name__ == '__main__':
    x, ly = create_np_array()
    for i in range(10):
        run_plot(x, ly)

    # pour les tests de perfs (comparaison)
    # run_plot(x, ly)
    plt.show()
    # x1_net2, x2_net2, ly_net2 = create_np_array_network_2()
    # for i in range(5):
    #     cpp_py_cmp_network_2_loglog_loop(x2_net2, ly_net2)
    #     cpp_py_cmp_network_2_loglog_one(x2_net2, ly_net2)
