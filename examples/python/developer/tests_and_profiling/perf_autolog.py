# -*- coding: utf-8 -*-

"""
Pour avoir plus de donner, il faut exécuter ../dev_tests/bug_format_args.py avec ou sans la variable d'environnement
NEMESIS_NO_AUTOLOG
"""
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt


def load_perf(filename_):
    if os.path.exists(filename_):
        b_data = pickle.load(open(filename_, 'rb'))
        return True, b_data
    return False, None


if __name__ == '__main__':
    base = os.path.dirname(__file__)

    fn_no_autolog = os.path.join(base, 'no_autolog.pickle')
    fn_autolog = os.path.join(base, 'autolog.pickle')
    fn_uselog = os.path.join(base, 'uselog.pickle')
    fn_sessionlog = os.path.join(base, 'sessionlog.pickle')

    avail_data = []
    avail_name = []
    ex_autolog, b_with_auto = load_perf(fn_autolog)
    if ex_autolog:
        avail_name.append('autolog')
        avail_data.append(b_with_auto)
    ex_no_autolgo, b_without_auto = load_perf(fn_no_autolog)
    if ex_no_autolgo:
        avail_name.append('no_autolog')
        avail_data.append(b_without_auto)
    ex_sessionlog, b_sessionlog = load_perf(fn_sessionlog)
    if ex_sessionlog:
        avail_name.append('sessionlog')
        avail_data.append(b_sessionlog)
    ex_uselog, b_uselog = load_perf(fn_uselog)
    if ex_uselog:
        avail_name.append('uselog')
        avail_data.append(b_uselog)

    avail_data = []
    avail_name = []
    ex_autolog, b_with_auto = load_perf(fn_autolog)
    if ex_autolog:
        avail_name.append('autolog')
        avail_data.append(b_with_auto)
    ex_no_autolgo, b_without_auto = load_perf(fn_no_autolog)
    if ex_no_autolgo:
        avail_name.append('no_autolog')
        avail_data.append(b_without_auto)
    ex_sessionlog, b_sessionlog = load_perf(fn_sessionlog)
    if ex_sessionlog:
        avail_name.append('sessionlog')
        avail_data.append(b_sessionlog)
    ex_uselog, b_uselog = load_perf(fn_uselog)
    if ex_uselog:
        avail_name.append('uselog')
        avail_data.append(b_uselog)

    avail_len = [len(data) for data in avail_data]
    i_min = np.argmin(avail_len)
    print(type(i_min))
    nb_items = len(avail_data[i_min])
    np_data = [np.array(data[:nb_items]) for data in avail_data]

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    indices = []
    i_no_autolog = avail_name.index('no_autolog')
    mean_no_autolog = np_data[i_no_autolog].mean()
    final_labels = []
    for (i, name), data, ccolor in zip(enumerate(avail_name), np_data, colors):
        current_mean = data.mean()
        if i != i_no_autolog:
            diff = current_mean - mean_no_autolog
            perc = (100 * diff) / mean_no_autolog
            final_labels.append(f'{name} ({diff:.4f}, {perc:.3f}%)')
        else:
            final_labels.append(name)
        indices.append(2*len(indices))
        plt.plot(data, color=ccolor)
        plt.plot([current_mean, ] * nb_items, '--', label=final_labels[-1], color=ccolor)

    # import nemesis.plotter as nem_plt
    # nem_plt.PyPlotCEA.networks_legend(final_labels, indices)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='upper center', ncol=2)

    # (bottom, top) = plt.ylim()
    # plt.ylim(0.0, top)
    plt.show()

    #     mean_with = with_auto.mean()
    #     mean_without = without_auto.mean()
    #     diff_auto = with_auto - without_auto
    #     ecart = diff_auto * 0.5
    #     mean_ecart = ecart.mean()
    #
    #     plt.plot(with_auto, '-', [mean_with, ] * nb_items, '--', label='avec autolog')
    #     plt.plot(without_auto, '-', [mean_without, ] * nb_items, '--', label='sans autolog')
    #     # plt.plot(ecart, '-', [mean_ecart, ] * nb_items, '--', label='écart')
    #
    #     x = (nb_items - 1)
    #     deb = (x + 0.1, mean_without)
    #     fin = (x + 0.1, mean_with)
    #     y = mean_ecart + mean_without
    #     perc = (100 * mean_ecart) / mean_without
    #
    #     plt.annotate('', xy=deb, xytext=fin, arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
    #     plt.text(x + 0.2, y, f'{mean_ecart:.2f}s')
    #     plt.annotate('', xy=(0, mean_without), xytext=(0, mean_with),
    #                  arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
    #     plt.text(0.2, y, f'{perc:.2f}%')
    #     yh = without_auto.min()
    #     plt.annotate('', xy=(16, yh), xytext=(30, yh), arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
    #     yh2 = yh + (yh * 0.01)
    #     plt.text(21, yh2, 'youtube')
    #     # plt.legend()
    #     plt.show()
