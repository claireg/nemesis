# -*- coding: utf-8 -*-

import time
from memory_profiler import profile
import warnings

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd
import nemesis.tools as spock

BBL = 'BIB._(A:B)'
TITLE = 'GRANDEUR(Unité) Min=1.0000E-03 Max=1.00000E+03'
MAT = 'MAT'
XLABEL = 'Abscisses (Unité)'
YLABEL = 'Ordonnées (Unité)'
WIDTH = 1600
HEIGHT = 1200
LINE_WIDTH = 1.5
N = 40


def build_data_x_ny(n: int = 20):
    xx = np.linspace(0, 2 * np.pi)
    offsets = np.linspace(0, 2 * np.pi, n, endpoint=False)
    lyy = []
    for phi in offsets:
        lyy.append(1.05 + np.sin(xx + phi))
    return xx, lyy


def prepare_one(x_, ly_):
    args = []
    for y in ly_:
        args.append(x_)
        args.append(y)
    return args


def get_data():
    x_, ly_ = build_data_x_ny(n=N)
    plt_args = prepare_one(x_, ly_)
    return x_, plt_args


def get_data_loop():
    return build_data_x_ny(n=N)


def run_interactive(x_, ly_):
    nem_plt.PyPlotCEA.set_figure_size(WIDTH, HEIGHT)
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_linewidth(LINE_WIDTH)
    for y in ly_:
        plt.plot(x_, y)
    nem_plt.PyPlotCEA.set_log_scale(True, True)
    nem_plt.PyPlotCEA.set_titles(BBL, TITLE, MAT)
    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.grid(True)


@profile
def run_mplconfig_loop(x_, ly_):
    nem_plt.PyPlotCEA.set_figure_size(WIDTH, HEIGHT)
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_linewidth(LINE_WIDTH)
    for y in ly_:
        plt.plot(x_, y)
    nem_plt.PyPlotCEA.set_log_scale(True, True)
    nem_plt.PyPlotCEA.set_titles(BBL, TITLE, MAT)
    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.grid(True)
    plt.draw()
    plt.pause(2)
    plt.close()


@profile
def run_plt_loop(x_, ly_):
    # Appel direct à matplotlib, n'implique aucune autre modification
    #   Important pour comparaison avec run_mplconfig
    nem_plt.PyPlotCEA.set_figure_size(WIDTH, HEIGHT)
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_linewidth(LINE_WIDTH)
    for y in ly_:
        plt.plot(x_, y)
    plt.xscale('log')
    plt.yscale('log')
    # N'ajoute que des textes, n'implique pas d'autres changements
    nem_plt.PyPlotCEA.set_titles(BBL, TITLE, MAT)
    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.grid(True)
    plt.draw()
    plt.pause(2)
    plt.close()


# @profile
def run_mplconfig():
    nem_plt.PyPlotCEA.set_figure_size(WIDTH, HEIGHT)
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_linewidth(LINE_WIDTH)
    x_, plt_args = get_data()
    plt.plot(*plt_args)
    nem_plt.PyPlotCEA.set_log_scale(True, True)
    nem_plt.PyPlotCEA.set_titles(BBL, TITLE, MAT)
    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.grid(True)
    plt.draw()
    plt.pause(2)
    plt.close()


# @profile
def run_plt():
    # Appel direct à matplotlib, n'implique aucune autre modification
    #   Important pour comparaison avec run_mplconfig
    nem_plt.PyPlotCEA.set_figure_size(WIDTH, HEIGHT)
    nem_plt.PyPlotCEA.nemesis_params()
    nem_plt.PyPlotCEA.set_linewidth(LINE_WIDTH)
    x_, plt_args = get_data()
    plt.plot(*plt_args)
    plt.xscale('log')
    plt.yscale('log')
    # N'ajoute que des textes, n'implique pas d'autres changements
    nem_plt.PyPlotCEA.set_titles(BBL, TITLE, MAT)
    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.grid(True)
    plt.draw()
    plt.pause(2)
    plt.close()


def timer():
    with spock.LoggerTimer('AVEC customisation des axes log (run_mplconfig) : '):
        run_mplconfig()
    with spock.LoggerTimer('SANS customisation des axes log (run_plt) : '):
        run_plt()


def timer_loop(x_, ly_):
    with spock.LoggerTimer('SANS customisation des axes log (run_plt) : '):
        run_plt_loop(x_, ly_)
    with spock.LoggerTimer('AVEC customisation des axes log (run_mplconfig) : '):
        run_mplconfig_loop(x_, ly_)


def __results_elapsed_time(title, title_1, array_1, title_2, array_2):
    value_moy_array_1 = np.median(array_1)
    moy_array_1 = np.array([value_moy_array_1]*array_1.size)
    value_moy_array_2 = np.median(array_2)
    moy_array_2 = np.array([value_moy_array_2]*array_2.size)
    ecart = value_moy_array_2-value_moy_array_1

    plt.plot(array_1, 'r-', moy_array_1, 'r--', array_2, 'k-', moy_array_2, 'k--')
    plt.grid(True)
    plt.ylim(2.6, 3.8)
    plt.legend([title_1, 'moy {}'.format(title_1), title_2, 'moy {}'.format(title_2)])
    plt.title(title)
    plt.annotate('',
                 xy=(3, value_moy_array_1), xycoords='data',
                 xytext=(3, value_moy_array_2), textcoords='data',
                 arrowprops=dict(arrowstyle="<->"))
    yv = value_moy_array_1+ecart*0.5
    plt.annotate(str(ecart),
                 xy=(3, yv),
                 xytext=(2, yv), textcoords='data',
                 bbox=dict(boxstyle="round4,pad=.5", fc="0.8"),
                 arrowprops=dict(arrowstyle="-"))
    plt.pause(5)
    name = title.split(' ')[0]
    plt.savefig('./{}_results_elapsed_time.png'.format(name))
    plt.close()


def cpp_results_elapsed_time():
    # Temps en milliseconds
    y_plt = np.array([2801., 2851., 2853., 2823., 2795., 2872., 2698.])
    y_mplconfig = np.array([3677., 3712., 3768., 3668., 3662., 3705., 3787.])
    __results_elapsed_time('C++ (seconds)', 'pyplot', y_plt*0.001, 'mplconfig', y_mplconfig*0.001)


def python_results_elapsed_time():
    y_mplconfig = np.array([3.7032370567321777, 3.7621662616729736, 3.670691967010498,
                           3.670504093170166, 3.6837527751922607, 3.672002077102661, 3.6130475997924805])
    y_plt = np.array([2.8274035453796387, 2.7921509742736816, 2.857795000076294, 2.806091785430908,
                      2.8013715744018555, 2.8301265239715576, 2.860438108444214])
    __results_elapsed_time('Python (seconds)', 'pyplot', y_plt, 'mplconfig', y_mplconfig)


if __name__ == '__main__':
    # Ajouter le décorateur profile sur run_mplconfig
    # mprof run examples/python/developer/tests_and_profiling/profile_mplconfig.py
    # mprof plot
    # Commenter le décorateur profile sur run_mplconfig
    # python -m cProfile -o mplconfig.prof ./nemesis/for_intern_debug_purpose/profile_mplconfig.py
    # snakeviz mplconfig.prof

    # run_mplconfig()
    # run_plt()
    # timer()

    # --------------------

    # /!\ Finit en SEGV dans PyCharm – OK en debug ... @FIXME ?
    x, ly = get_data_loop()
    for i in range(4):
        timer_loop(x, ly)

    # Indépendant de la boucle ci-dessus
    # run_mplconfig_loop(x, ly)  # OK
    # run_plt_loop(x, ly)  # OK

    # --------------------

    # cpp_results_elapsed_time()
    # python_results_elapsed_time()

    # --------------------

    # x, ly = get_data()
    # run_interactive(x, ly)
    # plt.show()
