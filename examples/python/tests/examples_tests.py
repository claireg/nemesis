# -*- coding: utf-8 -*-

import os
import sys
import glob
import subprocess
import time
import tempfile

import matplotlib
matplotlib.use('Agg')

# actual physical directory for each dir
dirs = dict(nemesis=os.path.join('..', 'nemesis'),
            dev_dtests=os.path.join('..', 'developer', 'dev_tests'),
            dev_tprofiling=os.path.join('..', 'developer', 'tests_and_profiling'),)


# files in each dir
files = dict()

files['nemesis'] = [
    'isocolorbar_prototype.py',
    'legend_networks.py',
    'plots_over.py',
    'pres_v0.0.1.5_ex0.py',
    'run_stuff.py',
    ]

files['dev_dtests'] = [
    'bug_layout.py',
    'cpptests_in_python.py',
    'debug_tests_isocolorbar.py',
    'debug_tests_readdata.py',
    'debug_tests_zlim.py',
    'eastworld_3d_raised_contourf.py',
    'prototype_test_mplot3d.py',
    'raised_contourf_text.py',
    'tucpp_matplotlibending.py',
]

files['dev_tprofiling'] = [
    'profile_mplconfig.py',
    'run_perf.py',
]
# d
# ict from dir to files we know we don't want to test (e.g., examples
# not using pyplot, examples requiring user input, animation examples,
# examples that may only work in certain environs (usetex examples?),
# examples that generate multiple figures

excluded = {
}
# excluded = {
#     'units': ['__init__.py', 'date_support.py', ],
# }


def report_missing(one_dir, flist):
    """report the py files in dir that are not in flist"""
    globstr = os.path.join(one_dir, '*.py')
    fnames = glob.glob(globstr)

    pyfiles = {os.path.split(fullpath)[-1] for fullpath in fnames}

    exclude = set(excluded.get(one_dir, []))
    flist = set(flist)
    missing = list(pyfiles - flist - exclude)
    if missing:
        print('%s files not tested: %s' % (one_dir, ', '.join(sorted(missing))))


def report_all_missing(directories):
    for f in directories:
        report_missing(dirs[f], files[f])


def run(arglist):
    try:
        ret = subprocess.call(arglist)
    except KeyboardInterrupt:
        sys.exit()
    else:
        return ret


def drive(directory, exe_python='python'):
    fails = []

    # Clear the destination directory for the examples
    path = os.path.join('/tmp', 'nemesis_examples_tests', directory)
    nemesis_pkg_path = os.path.realpath(os.path.join('..', '..', '..', 'nemesis'))
    if not os.path.exists(path):
        os.makedirs(path)

    testcases = [os.path.join(dirs[directory], fname) for fname in files[directory]]

    for fullpath in testcases:
        print(f'\tdriving {fullpath}')
        sys.stdout.flush()

        tmpfile = tempfile.NamedTemporaryFile(mode='w', delete=False, dir=path)

        for line in open(fullpath):
            line_lstrip = line.lstrip()
            if line_lstrip.startswith("#"):
                tmpfile.write(line)

        tmpfile.writelines((
            'import sys\n',
            'sys.path.append("%s")\n' % nemesis_pkg_path.replace('\\', '\\\\'),
            'import matplotlib\n',
            'matplotlib.use("Agg")\n'
            'from pylab import savefig\n',
            'import numpy\n',
            'numpy.seterr(invalid="ignore")\n',
            ))
        for line in open(fullpath):
            if line.lstrip().startswith(('matplotlib.use', 'savefig', 'show')):
                continue
            tmpfile.write(line)

        tmpfile.close()

        program = [exe_python]
        start_time = time.time()
        ret = run(program + [tmpfile.name])
        end_time = time.time()
        print("%s %s" % ((end_time - start_time), ret))
        if ret:
            fails.append(fullpath)

        os.remove(tmpfile.name)

    os.rmdir(path)
    return fails


if __name__ == '__main__':
    times = {}
    failures = {}

    python = sys.executable

    current_dir = os.path.join(os.path.dirname(__file__), '..')
    report_all_missing(dirs)
    for rep in dirs:
        print(f'testing {rep}')
        t0 = time.time()
        failures[rep] = drive(rep, python)
        t1 = time.time()
        times[rep] = (t1 - t0) / 60

    for rep, elapsed in times.items():
        print(f'\n### Directory {rep} took {elapsed:1.2} minutes to complete')
        failed = failures[rep]
        if failed:
            print(f'    Failures: {failed}')
        if 'template' in times:
            first = elapsed/times['template']
            second = elapsed - times['template']
            print(f'\ttemplate ratio {first:1.3}, template residual {second:1.3}')
