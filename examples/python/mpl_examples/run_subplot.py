# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
    x = np.linspace(0, 2*np.pi, 400)
    y = np.sin(x**2)
    fig, axes = plt.subplots(2, 2, sharex='col')
    axes[0, 0].plot(x, y)
    axes[1, 1].scatter(x, y)
    plt.show()
