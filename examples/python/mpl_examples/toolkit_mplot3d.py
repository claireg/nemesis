# -*- coding: utf-8 -*-

"""
Exemples toolkits mplot3d
"""

import numpy as np
import matplotlib.pyplot as plt
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from numpy.core.multiarray import ndarray


def lorenz(x, y, z, s=10, r=28, b=2.667):
    """
    Given:
       x, y, z: a point of interest in three dimensional space
       s, r, b: parameters defining the lorenz attractor
    Returns:
       x_dot, y_dot, z_dot: values of the lorenz attractor's partial
           derivatives at the point x, y, z
    """
    x_tmp = s*(y - x)
    y_tmp = r*x - y - x*z
    z_tmp = x*y - b*z
    return x_tmp, y_tmp, z_tmp


def draw_lorenz():
    dt = 0.01
    num_steps = 10000

    # Need one more for the initial values
    xs = np.empty((num_steps + 1,))
    ys = np.empty((num_steps + 1,))
    zs = np.empty((num_steps + 1,))

    # Set initial values
    xs[0], ys[0], zs[0] = (0., 1., 1.05)

    # Step through "time", calculating the partial derivatives at the current point
    # and using them to estimate the next point
    for i in range(num_steps):
        x_dot, y_dot, z_dot = lorenz(xs[i], ys[i], zs[i])
        xs[i + 1] = xs[i] + (x_dot * dt)
        ys[i + 1] = ys[i] + (y_dot * dt)
        zs[i + 1] = zs[i] + (z_dot * dt)

    # Plot
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot(xs, ys, zs, lw=1.0)
    ax.set_xlabel("X Axis")
    ax.set_ylabel("Y Axis")
    ax.set_zlabel("Z Axis")
    ax.set_title("Lorenz Attractor")

    print(f'{fig.axes}')

    plt.show()


def draw_parametric_curve(animation=False):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    xx = 30*np.random.randn(10000)
    mu = xx.mean()
    median = np.median(xx)
    sigma = xx.std()
    textstr = '\n'.join((
        r'$\mu=%.2f$' % (mu, ),
        r'$\mathrm{median}=%.2f$' % (median, ),
        r'$\sigma=%.2f$' % (sigma, )))

    # Prepare arrays x, y, z
    theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
    z = np.linspace(-2, 2, 100)
    r = z**2 + 1
    x = r * np.sin(theta)
    y = r * np.cos(theta)

    ax.plot(x, y, z, label='parametric curve')
    ax.legend()
    ax.text2D(0.05, 0.95, textstr, transform=ax.transAxes)

    print(f'{fig.axes}')

    if not animation:
        plt.show()
    else:
        for angle in range(0, 360):
            ax.view_init(30, angle)
            plt.draw()
            plt.pause(.001)


if __name__ == '__main__':
    # draw_lorenz()
    draw_parametric_curve(True)
