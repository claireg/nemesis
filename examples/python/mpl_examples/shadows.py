import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LightSource


def data():
    x, y = np.mgrid[-3:3:150j, -3:3:150j]
    z = 3*(1 - x)**2 * np.exp(-x**2 - (y + 1)**2) \
        - 10*(x/5 - x**3 - y**5)*np.exp(-x**2 - y**2) \
        - 1./3*np.exp(-(x + 1)**2 - y**2)
    return x, y, z


if __name__ == "__main__":
    x, y, z = data()
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Create light source object.
    ls = LightSource(azdeg=0, altdeg=65)
    # Shade data, creating an rgb array.
    rgb = ls.shade(z, plt.cm.RdYlBu)
    surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, linewidth=0,
                           antialiased=False, facecolors=rgb)
    plt.show()
