# -*- coding: utf-8 -*-

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np


def f(t):
    s1 = np.cos(2*np.pi*t)
    e1 = np.exp(-t)
    return np.multiply(s1, e1)


def fig_2d_3d():
    # Set up a figure twice as tall as it is wide
    fig = plt.figure(figsize=plt.figaspect(2.))
    fig.suptitle('A tale of 2 subplots')

    # First subplot
    ax = fig.add_subplot(2, 1, 1)

    t1 = np.arange(0.0, 5.0, 0.1)
    t2 = np.arange(0.0, 5.0, 0.02)
    t3 = np.arange(0.0, 2.0, 0.01)

    ax.plot(t1, f(t1), 'bo',
            t2, f(t2), 'k--', markerfacecolor='green')
    ax.grid(True)
    ax.set_ylabel('Damped oscillation')

    # Second subplot
    ax = fig.add_subplot(2, 1, 2, projection='3d')

    X = np.arange(-5, 5, 0.25)
    Y = np.arange(-5, 5, 0.25)
    X, Y = np.meshgrid(X, Y)
    R = np.sqrt(X**2 + Y**2)
    Z = np.sin(R)

    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                           linewidth=0, antialiased=False)
    ax.set_zlim(-1, 1)

    print(f'{fig.axes}')

    plt.show()


def mesh():
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Make the grid
    x, y, z = np.meshgrid(np.arange(-0.8, 1, 0.2),
                          np.arange(-0.8, 1, 0.2),
                          np.arange(-0.8, 1, 0.8))

    # Make the direction data for the arrows
    # u = np.sin(np.pi * x) * np.cos(np.pi * y) * np.cos(np.pi * z)
    # v = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
    # w = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) *
    #      np.sin(np.pi * z))

    # print('\n'.join(dir(ax)))
    # ax.quiver(x, y, z, u, v, w, length=0.1, normalize=True)
    ax.scatter(x, y, z)
    plt.show()


if __name__ == '__main__':
    fig_2d_3d()
    mesh()
