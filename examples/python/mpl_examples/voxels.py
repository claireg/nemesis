"""
==========================
3D voxel / volumetric plot
==========================

Demonstrates plotting 3D volumetric objects with ``ax.voxels``
"""
from typing import Dict

import matplotlib.pyplot as plt
import numpy as np

# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import


# prepare some coordinates
b8: Dict[str, int] = {'n': 8, 'b_inf': 3, 'b_sup': 5, 'b_mid': 2}
b5: Dict[str, int] = {'n': 5, 'b_inf': 2, 'b_sup': 3, 'b_mid': 3}
b4: Dict[str, int] = {'n': 4, 'b_inf': 1, 'b_sup': 2, 'b_mid': 2}


def data_and_colors(current_b: Dict[str, int]):
    x, y, z = np.indices((current_b['n'], current_b['n'], current_b['n']))

    # draw cuboids in the top left and bottom right corners, and a link between them
    cube1 = (x < current_b['b_inf']) & (y < current_b['b_inf']) & (z < current_b['b_inf'])
    cube2 = (x >= current_b['b_sup']) & (y >= current_b['b_sup']) & (z >= current_b['b_sup'])
    link = abs(x - y) + abs(y - z) + abs(z - x) <= current_b['b_mid']

    # combine the objects into a single boolean array
    voxels = cube1 | cube2 | link

    # set the colors of each object
    colors = np.empty(voxels.shape, dtype=object)
    colors[link] = 'red'
    colors[cube1] = 'blue'
    colors[cube2] = 'green'
    edge_colors = np.empty(voxels.shape, dtype=object)
    edge_colors[link] = 'gray'
    edge_colors[cube1] = 'orange'
    edge_colors[cube2] = 'yellow'

    return voxels, colors, edge_colors


def draw_points(n):
    x = np.arange(n*n).reshape((n, n))
    diag = np.diag(np.diag(x))
    # print(diag)
    plt.matshow(diag)
    plt.show()


def draw_voxels(current_b):
    d_voxels, d_colors, d_edge_colors = data_and_colors(current_b)
    # and plot everything
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.voxels(d_voxels, facecolors=d_colors, edgecolors=d_edge_colors)
    plt.show()


if __name__ == '__main__':
    draw_voxels(b4)
    # draw_points(10)
