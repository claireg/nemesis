# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

step = 2


def data():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(0, 2 * np.pi, .2))
    ud = np.cos(xd)
    vd = np.sin(yd)
    return xd, yd, ud, vd


def my_quiver(ax_, x_, y_, u_, v_, with_scatter=False):
    mag = np.hypot(u_, v_)
    q = ax_.quiver(x_[::step, ::step], y_[::step, ::step], u_[::step, ::step], v_[::step, ::step], mag, headwidth=5)
    # une espèce de légende
    ax_.quiverkey(q, 0.9, 0.9, 1, r'$1 \frac{m}{s}$', labelpos='E', coordinates='figure')
    if with_scatter:
        ax_.scatter(x_[::step, ::step], y_[::step, ::step], color='r', s=5)


def my_streamlines(ax_, x_, y_, u_, v_):
    strm = None
    mag = np.hypot(u_, v_)
    # speed est mag sont quasiment identique ...
    speed = np.sqrt(u_ ** 2 + v_ ** 2)
    # lw = 5 * speed / speed.max()
    lw = 5 * speed / speed.max()
    # density = [0.5, 1], linewidth=lw
    strm = ax_.streamplot(x_, y_, u_, v_, color=mag, linewidth=lw)
    fig.colorbar(strm.lines)


if __name__ == '__main__':
    x, y, u, v = data()
    # x, y, u, v sont des tableaux 2D
    fig, axs = plt.subplots(1, 2, figsize=(15, 12))
    my_quiver(axs[0], x, y, u, v)
    my_streamlines(axs[1], x, y, u, v)
    plt.show()