# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri

if __name__ == "__main__":
    x = np.asarray([1.79970, 11.7995, 17.5000, 19.8997, 25.0997, 28.2001, 30.1000, 30.4997, 38.4000, 39.0997,
                    44.6997, 47.2997, 47.6002, 53.8998, 59.4002, 64.2997, 68.8003, 68.8995, 74.2999, 78.2001,
                    78.5995, 81.2997, 85.6003, 87.9004, 89.5998, 91.6998, 91.8994, 93.4008, 96.4994, 100.8,
                    100.9, 102.8])
    y = np.asarray([0.0, 0.0, 0.0, 0.0, 0.025, 0.025, 0.025, 0.025, 0.05, 0.05,
                    0.05, 0.05, 0.075, 0.075, 0.075, 0.075, 0.1, 0.1, 0.1, 0.1,
                    0.125, 0.125, 0.125, 0.125, 0.15, 0.15, 0.15, 0.15, 0.175, 0.175,
                    0.175, 0.175])
    z = np.asarray([0.0, 0.0, 0.0, 0.0, 0.0819769, 0.251633, 525.682, 1098.47, 1116.27, 1161.32,
                    1104.19, 785.840, 0.0, 0.0, 0.0, 0.0, 0.272969, 0.638116, 668.916, 1095.08,
                    1115.67, 1161.35, 1109.84, 935.146, 0.0, 0.0, 0.0, 0.0, 1.36715, 812.884,
                    1090.81, 1114.97])

    fig = plt.figure()
    ax = fig.gca()

    triang = tri.Triangulation(x, y)
    ax.triplot(triang, 'ko-', lw=0.5)
    ax.tricontourf(triang, z)

    plt.grid(True)
    plt.show()
