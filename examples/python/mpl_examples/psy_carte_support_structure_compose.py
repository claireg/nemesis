# -*- coding: utf-8 -*-

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.image import NonUniformImage
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


x_coord = np.array([1.79970, 11.7995, 17.5000, 19.8997, 25.0997,
                    28.2001, 30.1000, 30.4997, 38.4000, 39.0997,
                    44.6997, 47.2997, 47.6002, 53.8998, 59.4002,
                    64.2997, 68.8003, 68.8995, 74.2999, 78.2001,
                    78.5995, 81.2997, 85.6003, 87.9004, 89.5998,
                    91.6998, 91.8994, 93.4008, 96.4994, 100.800,
                    100.900, 102.800])
y_coord = np.array([0.00000, 0.00000, 0.00000, 0.00000,
                    0.0250000, 0.0250000, 0.0250000, 0.0250000,
                    0.0500000, 0.0500000, 0.0500000, 0.0500000,
                    0.0750000, 0.0750000, 0.0750000, 0.0750000,
                    0.100000, 0.100000, 0.100000, 0.100000,
                    0.125000, 0.125000, 0.125000, 0.125000,
                    0.150000, 0.150000, 0.150000, 0.150000,
                    0.175000, 0.175000, 0.175000, 0.175000])
z_coord = np.array([-0.00000, -0.00000, -0.00000, -0.00000,
                   0.0819769, 0.251633, 525.682, 1098.47,
                   1116.27, 1161.32, 1104.19, 785.840,
                   -0.00000, -0.00000, -0.00000, -0.00000,
                   0.272969, 0.638116, 668.916, 1095.08,
                   1115.67, 1161.35, 1109.84, 935.146,
                   -0.00000, -0.00000, -0.00000, -0.00000,
                   1.36715, 812.884, 1090.81, 1114.97])


def simple(nx, ny):
    fig, ax = plt.subplots()
    ax.scatter(x_coord, y_coord, c=z_coord)
    plt.show()


def grid(nx, ny):
    fig, ax = plt.subplots()
    ax.tricontourf(x_coord, y_coord, z_coord)
    plt.show()


def image(nx, ny):
    interp = 'bilinear'
    z = z_coord.reshape(nx, ny)
    fig, ax = plt.subplots()
    im = NonUniformImage(ax, interpolation=interp, extent=(x_coord.min(), x_coord.max(), y_coord.min(), y_coord.max()),
                         cmap=cm.Purples)
    im.set_data(x_coord, y_coord, z)
    ax.images.append(im)
    ax.set_title(interp)


def surface(nx, ny):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.scatter(x_coord, y_coord, z_coord)
    ax.plot(x_coord, y_coord, z_coord)
    # ax.plot_surface(x_coord, y_coord, z_coord)
    plt.show()


if __name__ == '__main__':
    # mpl.use('Qt5Agg')
    print('youpi')
    n1, n2 = 4, 8
    # simple(n2, n1)
    # grid(n2, n1)
    # image(n1, n2)
    surface(n1, n2)
