# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import nemesis.plotter as nem_plt

nem_plt.PyPlotCEA.set_figure_size(600, 870)
nem_plt.PyPlotCEA.nemesis_params()
x = np.linspace(-np.pi, np.pi, 201)
plt.plot(x, np.cos(x), label='cos(x)')
plt.plot(x, 0.5*np.cos(x), label='cos(x)/2')
plt.plot(x, np.sin(x), '--', label='sin(x)')
plt.plot(x, 0.5*np.sin(x), '--', label='sin(x)/2')
plt.xlabel('Àngles (€)')
plt.ylabel('Valeurs (æ_)')
nem_plt.PyPlotCEA.networks_legend(['cos', 'sin'], [0, 2])
plt.show()
