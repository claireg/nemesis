# -*- coding: utf-8 -*-
import warnings
import logging

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.cm as cm
import matplotlib.colorbar as cbar
from matplotlib.patches import Patch
from matplotlib.axes import SubplotBase
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import matplotlib.contour as contour
import matplotlib.axes as axes
import matplotlib.colorbar as colorbar

import nemesis.readdata as nem_rd

# Logger
logger = logging.getLogger('pyplotcea')
logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
ch_formatter = logging.Formatter('%(levelname)s - %(name)s - %(message)s')
ch.setFormatter(ch_formatter)
# add the handlers to the logger
logger.addHandler(ch)


def log_kwargs(**kwargs):
    if len(kwargs) > 0:
        logger.info(f'\t\tKWARGS')
        for (k, v) in kwargs.items():
            logger.info(f'\t\t* {k} = {v}')


def __check_coherence_tricontour(x_coord: np.array, y_coord: np.array, z_coord: np.array, **kwargs) -> None:
    set_aliases = {'function_name': ('fn', 'funcname')}
    g_kwargs = cbook.normalize_kwargs(kwargs, set_aliases)
    fn = g_kwargs.pop('function_name', __name__)
    if not((x_coord.ndim == y_coord.ndim == z_coord.ndim == 1) and (x_coord.size == y_coord.size == z_coord.size)):
        raise RuntimeError(f'{fn} : Données incohérentes. Les tableaux x_coord, y_coord et z_coord '
                           f'doivent répondre aux critères suivants\n\t'
                           f'* ils doivent être à une dimension\n\t'
                           f'* ils doivent avoir la même taille')


def __colors_cmap_process_args(**kwargs: dict) -> (dict, dict):
    local_kwargs = {}
    color_found = False
    if 'colors' in kwargs and 'cmap' in kwargs:
        raise ValueError("Il faut donner un couleur (colors=) OU une carte de couleurs (cmap=) mais PAS les deux.")
    elif 'colors' in kwargs:
        color_found = True
        local_kwargs['colors'] = kwargs.pop('colors')
    elif 'cmap' in kwargs:
        color_found = True
        local_kwargs['cmap'] = kwargs.pop('cmap')

    if color_found is False:
        local_kwargs['cmap'] = cm.get_cmap()
    return local_kwargs, kwargs


def __line_colors_cmap_process_args(**kwargs: dict) -> (dict, dict):
    cmap_colors_kwargs, kwargs = __colors_cmap_process_args(**kwargs)
    local_kwargs = {'linewidths': kwargs.pop('linewidths', 0.5), 'linestyles': 'solid', }
    local_kwargs.update(cmap_colors_kwargs)
    return local_kwargs, kwargs


def __levels_process_args(**kwargs: dict) -> (object, dict):
    if (('n_levels' in kwargs) and ('levels' in kwargs)) or ('levels' in kwargs):
        levels = kwargs.pop('levels')
    else:
        levels = kwargs.pop('n_levels', 10)

    return levels, kwargs


def isocolorbar(mappable, cax=None, ax=None, use_gridspec=True, **kw):
    """
    Create a colorbar for a ScalarMappable instance, *mappable*.

    Documentation for the pyplot thin wrapper:
    %(colorbar_doc)s
    """
    if ax is None:
        ax = plt.gca()

    if cax is None:
        if use_gridspec and isinstance(ax, SubplotBase)  \
                 and (not plt.gcf().get_constrained_layout()):
            cax, kw = cbar.make_axes_gridspec(ax, **kw)
        else:
            cax, kw = cbar.make_axes(ax, **kw)

    # need to remove kws that cannot be passed to Colorbar
    non_colorbar_keys = ['fraction', 'pad', 'shrink', 'aspect', 'anchor',
                         'panchor', ]
    cb_kw = {k: v for k, v in kw.items() if k not in non_colorbar_keys}

    iso_labels = [str(iso_value) for iso_value in mappable.levels]
    iso_colors = [e[0] for e in mappable.tcolors]
    legend_elements = [Patch(facecolor=iso_color, label=iso_label)
                       for iso_color, iso_label in zip(iso_colors, iso_labels)]
    cax.legend(handles=legend_elements, **cb_kw)


def my_isocolorbar(iso_lines: np.array, iso_colors: np.array, title: str) -> None:
    iso_labels = [str(iso_value) for iso_value in iso_lines]
    kwargs = dict(bbox_to_anchor=(1.01, 1), mode='expand', borderaxespad=0., loc='best', frameon=False,
                  title=title)
    # legend_elements = [Line2D([0], [0], color=iso_color, linewidth=5, label=iso_label)
    #                    for iso_color, iso_label in zip(iso_colors, iso_labels)]
    # plt.gca().legend(handles=legend_elements, **kwargs)
    legend_elements = [Patch(facecolor=iso_color, label=iso_label)
                       for iso_color, iso_label in zip(iso_colors, iso_labels)]
    plt.gca().legend(handles=legend_elements, **kwargs)


def move_axes(parent, **kw):
    orientation = kw.setdefault('orientation', 'vertical')
    fraction = kw.pop('fraction', 0.15)
    shrink = kw.pop('shrink', 1.0)
    aspect = kw.pop('aspect', 20)
    # pb = transforms.PBox(parent.get_position())
    pb = parent.get_position(original=True).frozen()
    if orientation == 'vertical':
        pad = kw.pop('pad', 0.05)
        x1 = 1.0-fraction
        pb1, pbx, pbcb = pb.splitx(x1-pad, x1)
        pbcb = pbcb.shrunk(1.0, shrink).anchored('C', pbcb)
        anchor = kw.pop('anchor', (0.0, 0.5))
        panchor = kw.pop('panchor', (1.0, 0.5))
    else:
        pad = kw.pop('pad', 0.15)
        pbcb, pbx, pb1 = pb.splity(fraction, fraction+pad)
        pbcb = pbcb.shrunk(shrink, 1.0).anchored('C', pbcb)
        aspect = 1.0/aspect
        anchor = kw.pop('anchor', (0.5, 1.0))
        panchor = kw.pop('panchor', (0.5, 0.0))
    parent.set_position(pb1)
    parent.set_anchor(panchor)
    # fig = parent.get_figure()
    # cax = fig.add_axes(pbcb)
    # cax.set_aspect(aspect, anchor=anchor, adjustable='box')
    # return cax, kw
    return anchor, pbcb


def get_formatter(mappable, format_=None):
    if format_ is None:
        if isinstance(mappable.norm, colors.LogNorm):
            formatter = ticker.LogFormatterMathtext()
        else:
            formatter = ticker.ScalarFormatter(useMathText=False)
            formatter.set_scientific(True)
    # elif cbook.is_string_like(format):
    elif isinstance(format_, (str,)):
        formatter = ticker.FormatStrFormatter(format_)
    else:
        formatter = format_  # Assume it is a Formatter
    return formatter


def my_isocolorbar2(mappable, title: str) -> None:
    iso_colors = [e[0] for e in mappable.tcolors]
    formatter = get_formatter(mappable, '%.2f')
    iso_labels = [formatter.format_data(value) for value in mappable.levels]
    print(f'iso_labels = {iso_labels}')
    # iso_labels = ['0', r'1.5{\\times}10^{−1}', r'3{\\times}10^{−1}', r'4.5{\\times}10^{−1}', r'6{\\times}10^{−1}',
    #               r'7.5{\\times}10^{−1}', r'9{\\times}10^{−1}', '1.05']
    legend_elements = [Patch(facecolor=iso_color, label=iso_label)
                       for iso_color, iso_label in zip(iso_colors, iso_labels)]

    anchor, pbcb = move_axes(plt.gca())
    # plt.gca().legend(handles=legend_elements, title=title, loc='center left', bbox_to_anchor=(1, 0, 0.2, 1))
    plt.gca().legend(handles=legend_elements, title=title, loc='center left', bbox_to_anchor=anchor)


def my_isocolorbar3(mappable, title: str) -> None:
    print(f'Avant iso_contours.tlinewidths = {mappable.tlinewidths}')
    print(f'Type = {type(mappable.tlinewidths)}')
    print(f'Type [0]= {type(mappable.tlinewidths[0])}')
    linewidths = []
    for e in mappable.tlinewidths:
        linewidths.append((10*e[0],))
    print(f'Après tlinewidths = {linewidths}')
    mappable.tlinewidths = linewidths

    cb = plt.colorbar(mappable, drawedges=False)
    cb.set_label(title)
    # Ne fait rien
    cb.ax.spines['right'].set_visible(False)
    cb.ax.spines['left'].set_visible(False)
    cb.ax.spines['top'].set_visible(False)
    cb.ax.spines['bottom'].set_visible(False)
    cb.ax.yaxis.set_ticks_position('right')


def __iso_lines_width(n_levels):
    tmp = 100 / n_levels
    if tmp < 1:
        return 1
    if tmp > 20:
        return 20
    return tmp


def __check_coherence_contour(x_coord: np.array, y_coord: np.array, z_coord: np.array,
                              **kwargs: object) -> tuple:
    # print(f'Dimension 2 ? {x.ndim == y.ndim == z.ndim == 2}')
    if not (x_coord.ndim == y_coord.ndim == z_coord.ndim == 2):
        if x_coord.size == y_coord.size == z_coord.size:
            # kwargs doit contenir nx et ny
            g_kwargs = cbook.normalize_kwargs(kwargs, required=('nx', 'ny'), allowed=('nx', 'ny'))
            nx, ny = g_kwargs['nx'], g_kwargs['ny']
            x = x_coord.reshape(nx, ny)
            y = y_coord.reshape(nx, ny)
            z = z_coord.reshape(nx, ny)
        elif x_coord.size * y_coord.size == z_coord.size:
            # Ne pas inverser dans z_coord.reshape les valeurs : meshgrid inverse (voir doc numpy.meshgrid)
            x, y = np.meshgrid(x_coord, y_coord)
            z = z_coord.reshape(y_coord.size, x_coord.size)
        else:
            raise RuntimeError('z.size != y.size * y.size : no contour can be built.')
        return x, y, z
    else:
        return x_coord, y_coord, z_coord


def set_log_scale(x_log: bool, y_log: bool) -> None:
    use_specific_subticks = ''
    if x_log is True:
        plt.xscale("log")
        use_specific_subticks += 'x'
    else:
        plt.xscale("linear")
    if y_log is True:
        plt.yscale("log")
        use_specific_subticks += 'y'
    else:
        plt.yscale("linear")


def my_tricontour(x: np.array, y: np.array, z: np.array, **kwargs) -> None:
    set_aliases = {'n_levels': ('nlevels', 'nl'), 'levels': ('l',),
                   'colors': ('c',), 'cmap': ('cm',), 'label': ('lb',),
                   'draw_colorbar': ('dcb', 'dc'), 'linewidths': ('lw',), }
    g_kwargs = cbook.normalize_kwargs(kwargs, set_aliases)

    levels, g_kwargs = __levels_process_args(g_kwargs)
    local_kwargs, g_kwargs = __line_colors_cmap_process_args(g_kwargs)
    # title = g_kwargs.pop('label', '')

    __check_coherence_tricontour(x, y, z, function_name='my_tricontour')

    current_axes = plt.gca()
    iso_contours = current_axes.tricontour(x, y, z, levels, **local_kwargs, )

    current_axes.grid(color='black', linestyle='-', alpha=0.3)

    # need to remove kws that cannot be passed to isocolorbar
    # non_iso_colorbar_keys = ['label', 'masked_value', 'orientation']
    # icb_kw = {k: v for k, v in g_kwargs.items() if k not in non_iso_colorbar_keys}
    # iso_colors = [e[0] for e in iso_contours.tcolors]
    # my_isocolorbar(iso_contours.levels, iso_colors, title)
    # isocolorbar(iso_contours, current_axes, **icb_kw)
    # my_isocolorbar2(iso_contours, title)
    # my_isocolorbar3(iso_contours, title)
    cea_isocolorbar(iso_contours, drawedges=False)


def __sanitizer_z(z: np.array, f_name: str = '') -> np.array:
    # On supprime les valeurs <= 0 pour être sûr d'afficher qqch
    z_min = float(z.min())
    if z_min <= 0.:
        ma_z = np.ma.masked_where(z <= 0., z, copy=False)
        warnings.warn(f'{f_name}: values of z <= 0 have been masked')
        return ma_z
    return z


def __remove_less_equal_zero(arr: np.array) -> np.array:
    nz = np.count_nonzero(arr <= 0.)
    if nz == len(arr):
        return arr

    if isinstance(arr, (list, tuple)):
        arr = np.array(arr)
    good_indices = arr > 0.
    return arr[np.nonzero(good_indices)]


def __compute_shrink(n: int) -> float:
    shrink = 1.0
    if n < 10:
        shrink = 0.3
    elif n < 20:
        shrink = 0.5
    elif n < 50:
        shrink = 0.75
    elif n < 75:
        shrink = 0.85
    return shrink


def cea_isocolorbar(mappable: contour.QuadContourSet = None, cax: axes.Axes = None, ax: axes.Axes = None, **kwargs) \
        -> colorbar.Colorbar:
    logger.info(f'cea_isocolorbar')
    log_kwargs(**kwargs)
    logger.info(f'\t\t* Levels : {mappable.levels}')

    if mappable is None:
        mappable = plt.gci()
        if mappable is None:
            raise RuntimeError('No mappable was found to use for colorbar '
                               'creation. First define a mappable such as '
                               'an image (with imshow) or a contour set ('
                               'with contourf).')
    if ax is None:
        ax = plt.gca()

    # Contournement «bug» (?) si aucun contour n'est trouvé par plt.contour, alors il peut y avoir quand
    #   même un niveau (et un layers) avec une valeur abérante (2e-315)
    if len(mappable.levels) == 0 or (len(mappable.levels) == 1 and mappable.levels[0] == mappable.layers[0]):
        warnings.warn('No levels found, so no colorbar needed.')
        return None

    ret = plt.gcf().colorbar(mappable, cax=cax, ax=ax, **kwargs)
    # logger.critical(f'cb ticks : {ret.get_ticks()}')
    # logger.critical(f'cb clim : {ret.get_clim()}')
    width = __iso_lines_width(len(mappable.levels))
    # changement largeur des traits des iso
    ret.lines[0].set_linewidth(width)
    # suppression des ticks
    ret.ax.tick_params(size=0)
    # changement largeur du rectangle qui entoure la cbar
    ret.outline.set_linewidth(0)
    return ret


def __post_contour_actions(mappable: contour.QuadContourSet = None, cax: axes.Axes = None,
                           ax: axes.Axes = None, draw_colorbar: bool = True,
                           **kwargs) -> None:
    logger.info(f'__post_contour_actions')
    log_kwargs(**kwargs)

    ax.grid(color='black', linestyle='-', alpha=0.3)

    label = kwargs.pop('label', None)
    c_shrink = __compute_shrink(len(mappable.levels))
    # cb = plt.gcf().colorbar(iso_contours, ax=current_axes, shrink=c_shrink, aspect=20)
    if draw_colorbar:
        cb = cea_isocolorbar(mappable, cax=cax, ax=ax, shrink=c_shrink, drawedges=False, **kwargs)
        if label is not None and len(label) != 0:
            cb.set_label(label)


def logarithmic_contour(x: np.array, y: np.array, z: np.array, **kwargs: object) -> None:
    set_aliases = {'n_levels': ('nlevels', 'nl'), 'levels': ('l',), 'nx': ('n_x',), 'ny': ('n_y',),
                   'masked_value': ('mv',), 'draw_colorbar': ('dcb', 'dc'),
                   'colors': ('c',), 'cmap': ('cm',), 'label': ('lb',), 'linewidths': ('lw',), }
    g_kwargs = cbook.normalize_kwargs(kwargs, set_aliases)

    logger.info(f'logarithmic_contour')
    log_kwargs(**g_kwargs)

    levels, g_kwargs = __levels_process_args(**g_kwargs)
    local_kwargs, g_kwargs = __line_colors_cmap_process_args(**g_kwargs)

    x, y, z = __check_coherence_contour(x, y, z, **kwargs)
    masked_value = g_kwargs.pop('masked_value', None)
    if masked_value is not None:
        z = np.ma.masked_where(z <= masked_value, z)
    z = __sanitizer_z(z, 'logarithmic_contour')
    current_axes = plt.gca()

    log_norm = colors.LogNorm()
    # Si l'utilisateur passe des valeurs erronnées, ``contour(…)`` s'en sort très bien, mais pas ``colorbar(…)``
    if isinstance(levels, (list, tuple, np.ndarray)) is True:
        if not isinstance(levels, np.ndarray):
            levels = np.array(levels)
        levels_min = float(levels.min())
        if levels_min <= 0.:
            levels = __remove_less_equal_zero(levels)
            warnings.warn('Log scale: values of levels <= 0 have been removed')
    cs = current_axes.contour(x, y, z, levels, norm=log_norm, **local_kwargs, )

    z_min = z.min()
    z_max = z.max()
    diff = z_max - z_min
    q = np.floor(z_max / z_min)

    # Gestion ticks et labels de la colorbar - Ne peux arriver que si l'utilisateur passe un flottant ou un
    # entier à levels (erreur de l'utilisateur ?)
    # ticks_labels = None if not isinstance(levels, (list, tuple, np.ndarray)) else cs.levels
    # logger.info(f'\t\t* ticks = {ticks_labels}')
    # minor_thresholds : To label all minor ticks when the view limits span up to 1.5 decades
    #   issue de la doc `~.matplotlib.ticker.LogFormatter`
    # Pas de labels
    # formatter = mpl.ticker.LogFormatterSciNotation(minor_thresholds=(1.5, 1.5))
    # formatter = mpl.ticker.LogFormatterSciNotation(minor_thresholds=(0, 0))

    # formatter = mpl.ticker.LogFormatterMathtext()
    # On a des labels
    # formatter = mpl.ticker.ScalarFormatter(useMathText=True)
    # formatter = mpl.ticker.StrMethodFormatter('{x}')
    # formatter = mpl.ticker.EngFormatter()
    # formatter = mpl.ticker.LogFormatterSciNotation(minor_thresholds=(np.inf, np.inf))
    formatter = mpl.ticker.LogFormatterSciNotation(minor_thresholds=(q, 2))
    __post_contour_actions(cs, ax=current_axes, format=formatter, **g_kwargs)

    # __post_contour_actions(cs, ax=current_axes, **g_kwargs)


def draw_iso_no_labels():
    x, y, z = nem_rd.gaia_isolines_levels_2()
    levels = [1.72089643e-01, 1.92639705e+00, 2.15643750e+01, 2.41394821e+02, 2.70220952e+03, 3.02489351e+04,
              3.38611078e+05, 3.79046275e+06, 4.24310036e+07, 4.74978964e+08]
    logarithmic_contour(x, y, z, levels=levels, masked_value=1e-300, label='No Labels !')
    set_log_scale(True, True)
    plt.show()


def draw_contour():
    n_angles = 48
    n_radii = 8
    min_radius = 0.25
    radii = np.linspace(min_radius, 0.95, n_radii)

    angles = np.linspace(0, 2 * np.pi, n_angles, endpoint=False)
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)
    angles[:, 1::2] += np.pi / n_angles

    x = (radii * np.cos(angles)).flatten()
    y = (radii * np.sin(angles)).flatten()
    z = np.fabs(np.cos(radii) * np.cos(3 * angles)).flatten()

    my_tricontour(x, y, z, n_levels=10, masked_value=0., label='IsoColorbar')
    plt.show()


if __name__ == '__main__':
    ms = mpl.rcParams['lines.markersize']
    print(f'lines.markersize : {ms}')
    print(f'lines.markersize : {ms ** 2}')
    # draw_contour()
    draw_iso_no_labels()
