# -*- coding: utf-8 -*-

"""

"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt


interval = 1
cpt = 0


def data_1():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(0, 2 * np.pi, .2))
    ud = np.cos(xd)
    vd = np.sin(yd)
    return "u=cos(x) ; y=sin(y)", xd, yd, ud, vd


def data_2():
    # xd, yd = np.meshgrid(np.arange(-0.8, 1, 0.2), np.arange(-0.8, 1, 0.2))
    xd, yd = np.meshgrid(np.arange(-0.8, 1, 0.02), np.arange(-0.8, 1, 0.02))
    ud = np.sin(np.pi * xd) * np.cos(np.pi * yd)
    vd = -np.cos(np.pi * xd) * np.sin(np.pi * yd)
    return "u=sin(pi*x) * cos(pi*y) ; v=-cos(pi*x) * sin(pi*y)", xd, yd, ud, vd


def data_3():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = np.power(xd, 2) * np.cos(yd)
    vd = np.power(yd, 2) * np.sin(xd)
    return "u=x^2*cos(y) ; v=y^2*sin(x)", xd, yd, ud, vd


def dipole_potential(xd, yd):
    """The electric dipole potential V, at position *x*, *y*."""
    r_sq = xd ** 2 + yd ** 2
    theta = np.arctan2(yd, xd)
    zd = np.cos(theta) / r_sq
    return (np.max(zd) - zd) / (np.max(zd) - np.min(zd))


def data_4():
    from matplotlib.tri import (
        Triangulation, CubicTriInterpolator)
    # First create the x and y coordinates of the points.
    n_angles = 30
    n_radii = 10
    min_radius = 0.2
    radii = np.linspace(min_radius, 0.95, n_radii)

    angles = np.linspace(0, 2 * np.pi, n_angles, endpoint=False)
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)
    angles[:, 1::2] += np.pi / n_angles

    xd = (radii * np.cos(angles)).flatten()
    yd = (radii * np.sin(angles)).flatten()
    vd = dipole_potential(xd, yd)

    # Create the Triangulation; no triangles specified so Delaunay triangulation
    # created.
    triang = Triangulation(xd, yd)
    # Mask off unwanted triangles.
    # triang.set_mask(np.hypot(xd[triang.triangles].mean(axis=1),
    #                          yd[triang.triangles].mean(axis=1))
    #                 < min_radius)
    # -- Computes the electrical field (Ex, Ey) as gradient of electrical potential
    tci = CubicTriInterpolator(triang, -vd)
    # Gradient requested here at the mesh nodes but could be anywhere else:
    (ex, ey) = tci.gradient(triang.x, triang.y)
    e_norm = np.sqrt(ex**2 + ey**2)
    return "u=[grad(tri.x, tri.y)].x / norm ; v = [grad(tri.x, tri.y)].y / norm", \
           triang.x, triang.y, ex / e_norm, ey / e_norm


def data_5():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = xd * (1-xd) * np.cos(4 * np.pi * yd)
    vd = np.sin(4 * np.pi * xd)
    return "u=x*(1-x)*cos(4*pi*y) ; v = sin(4*pi*x)", xd, yd, ud, vd


def data_6():
    xd, yd = np.meshgrid(np.arange(-4, 4, .2), np.arange(-4, 4, .2))
    # joli mais equation physiquement erronée
    # ex = (xd + 1) / ((xd + 1) ** 2 + yd ** 2) - (xd - 1) / ((xd - 1) ** 2 + xd ** 2)
    ex = (xd + 1) / ((xd + 1) ** 2 + yd ** 2) - (xd - 1) / ((xd - 1) ** 2 + yd ** 2)
    # joli mais equation physiquemnt erronée
    # ey = xd / ((xd + 1) ** 2 + yd ** 2) - xd / ((xd - 1) ** 2 + yd ** 2)
    ey = yd / ((xd + 1) ** 2 + yd ** 2) - yd / ((xd - 1) ** 2 + yd ** 2)
    # plt.streamplot(X,Y,Ex,Ey)
    # plt.quiver(X,Y,Ex,Ey,scale=50)
    return "dipôleX; dipôleY", xd, yd, ex, ey


def data_7():
    # return x * (1 - x) * np.cos(4 * np.pi * x) * np.sin(4 * np.pi * y ** 2) ** 2
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = np.cos(4 * np.pi * yd)
    vd = (1 - xd) / (xd + 1)
    return "u=cos(4*pi*y) ; v = (1-x)/(x+1)", xd, yd, ud, vd


def data_8():
    xd, yd = np.meshgrid(np.arange(-np.pi, np.pi, .2), np.arange(-np.pi, np.pi, .2))
    ud = -np.cos(xd) + np.sin(3 * yd)
    vd = xd**2
    return "u=-cos(x)+sin(3y) ; v=x^2", xd, yd, ud, vd


def data_9():
    xd, yd = np.meshgrid(np.arange(0.01, 2 * np.pi, .2), np.arange(0.2, np.pi, .2))
    ud = 1.5 * xd**2 * np.sin(yd)
    vd = 1.5 * yd**2 * np.cos(xd)
    return "u=1.5*x^2*sin(y) ; v=1.5*y^2*cos(x)", xd, yd, ud, vd


def data_10():
    xd, yd = np.meshgrid(np.arange(-1, 1, 0.05), np.arange(-1, 1, 0.05))
    expn_2_3 = np.full(xd.shape, 2/3)
    ud = np.exp(xd, expn_2_3) + np.cos(yd * np.pi)
    vd = yd ** 2 + np.sin(xd * np.pi)
    return "u=x^(2/3) + cos(y*pi) ; v=y^2 + sin(x * pi)", xd, yd, ud, vd


def get_info(arr: np.array) -> str:
    tot = f'total: {arr.shape[0] * arr.shape[1]}' if arr.ndim == 2 else ''
    return f'ndim: {arr.ndim}\tshape: {arr.shape}\t{tot}'


def save_data(bname, uvname, xd, yd, ud, vd):
    global cpt
    data = {'x': xd, 'y': yd, 'u': ud, 'v': vd}
    dir_name = os.path.join(os.path.expanduser('~'), 'tmp', bname)
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    where_base = os.path.join(os.path.expanduser('~'), 'tmp', bname, f'data_{bname}.')
    pickle.dump(data, open(where_base + 'pickle', 'wb'), pickle.HIGHEST_PROTOCOL)
    with open(where_base + 'txt', 'w') as f:
        f.writelines(f'# {bname} : {uvname}\n')
        lines = [ f'{name:4} : {get_info(arr)}' for name, arr in data.items() ]
        f.writelines('\n'.join(lines))


def next_draw(bname):
    global cpt
    plt.pause(interval)
    # save_data alreaady called, so directory exists
    where = os.path.join(os.path.expanduser('~'), 'tmp', bname, f'image_{bname}_{cpt}.png')
    plt.savefig(where)
    cpt += 1
    plt.clf()


def print_data_size(bname, named, xd, yd, ud, vd):
    print(f'\n# Données : {bname} : {named}')
    for i, t in enumerate([xd, yd, ud, vd]):
        info = get_info(t)
        print(f'\t* {i}. {info}')


def merge_metadata():
    # Find all data_*.txt files in os.path.join(os.path.expanduser('~'), 'tmp', 'data*')
    bw = os.path.join(os.path.expanduser('~'), 'tmp')
    import glob
    filenames = glob.glob(os.path.join(bw, 'data_*/data_*.txt'))
    filenames.sort(key=os.path.getmtime)
    # print("\n".join(filenames))

    with open(os.path.join(bw, 'data_all.txt'), 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.writelines('\n')


if __name__ == '__main__':
    # for fct in [data_10]:
    # for fct in [data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10]:
    #     d_name = fct.__name__
    #     name, x, y, u, v = fct()
    #     uname, vname = name.split(';')
    #     if '=' in uname:
    #         uname = uname.split('=')[1]
    #     if '=' in vname:
    #         vname = vname.split('=')[1]
    #     save_data(d_name, name, x, y, u, v)
    #     print_data_size(d_name, name, x, y, u, v)
    #
    #     plt.plot(u)
    #     u_tot = f'  Total = {u.shape[0] * u.shape[1]}' if u.ndim == 2 else ''
    #     v_tot = f'  Total = {v.shape[0] * v.shape[1]}' if v.ndim == 2 else ''
    #     plt.title(f'{d_name} : U : f(x) = {uname}\nshape = {u.shape}{u_tot}')
    #     next_draw(d_name)
    #     plt.plot(v)
    #     plt.title(f'{d_name} : V : f(x) = {vname}\nshape = {v.shape}{v_tot}')
    #     next_draw(d_name)
    #     plt.plot(u.flatten())
    #     plt.title(f'{d_name} : U flatten : f(x) = {uname}\nshape = {u.shape}{u_tot}')
    #     next_draw(d_name)
    #     plt.plot(v.flatten())
    #     plt.title(f'{d_name} : V flatten : f(x) = {vname}\nshape = {v.shape}{v_tot}')
    #     next_draw(d_name)
    #     mag_xy = np.hypot(x, y)
    #     mag_uv = np.hypot(u, v)
    #     plt.plot(mag_xy)
    #     plt.title(f'{d_name} : MAG_XY : f(x) = mag(xy)')
    #     next_draw(d_name)
    #     plt.plot(mag_uv)
    #     plt.title(f'{d_name} : MAG_UV : f(x) = mag(uv)')
    #     next_draw(d_name)
    #     plt.plot(mag_xy, mag_uv)
    #     plt.title(f'{d_name} : f(mag_xy) = mag_uv')
    #     next_draw(d_name)
    #     if mag_uv.ndim == 2:
    #         plt.contourf(x, y, mag_uv)
    #         plt.title(f'{d_name} : Contour np.hypot({uname}, {vname})')
    #         next_draw(d_name)
    #     plt.quiver(x, y, u, v)
    #     plt.title(f'{d_name} : quiver')
    #     next_draw(d_name)
    #     try:
    #         plt.streamplot(x, y, u, v)
    #         plt.title(f'{d_name} : streamplot')
    #         next_draw(d_name)
    #     except ValueError as e:
    #         # data_4 : 'x' values must be equally spaced
    #         print(f'ERREUR streamplot : {d_name}')
    #         pass
    #     cpt = 0
    merge_metadata()