# -*- coding: utf-8 -*-

import os
import glob

import nemesis.conversion as nem_cv

DATA_DIR = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'data')


def convert(extraction=False, drawing=True, conversion=False):
    """
    Returns
    -------
    None.

    """
    data_psy = glob.glob(f'{DATA_DIR}/*.psy')
    # data_psy = ['onde.psy']

    for fic in data_psy:
        if 'carte_support_structure_compose.psy' not in fic:
            print(f'\n# Analyse du fichier {fic} :\n'
                  f'\t* Affichage : {drawing}\n'
                  f'\t* Extraction : {extraction}\n'
                  f'\t* Conversion : {conversion}')
            if drawing is True:
                print('\tATTENTION il faut fermer le fenêtre pour continuer')
            filename = os.path.join(DATA_DIR, fic)
            if drawing is True and extraction is False and conversion is False:
                nem_cv.psy_draw(filename)
            elif drawing is False and extraction is True and conversion is False:
                data = nem_cv.psy_extract(filename)
                print(data)
            elif drawing is False and extraction is False and conversion is True:
                for f in nem_cv.output_formats:
                    nem_cv.psy_convert(filename, '/tmp/test_conversion', f)
            else:
                cv_args = {}
                if conversion is True:
                    cv_args['where'] = '/tmp/test_conversion_compose'
                    cv_args['to_format'] = 'png'
                nem_cv.psy_compose(filename, drawing, extraction, conversion, **cv_args)


if __name__ == '__main__':
    convert(True, False, False)
    convert(False, True, False)
    convert(False, False, True)
    convert(True, True, False)
    convert(True, False, True)
    convert(False, True, True)
    convert(True, True, True)
