# -*- coding: utf-8 -*-
import warnings

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.collections as mcol
from matplotlib.lines import Line2D
from matplotlib.legend_handler import HandlerLineCollection

import nemesis.plotter as nem_plt


class HandlerDashedLines(HandlerLineCollection):
    """
    Custom Handler for LineCollection instances.
    """

    def create_artists(self, legend, orig_handle,
                       xdescent, ydescent, width, height, fontsize, trans):
        # figure out how many lines there are
        numlines = len(orig_handle.get_segments())
        xdata, xdata_marker = self.get_xdata(legend, xdescent, ydescent,
                                             width, height, fontsize)
        leglines = []
        # divide the vertical space where the lines will go
        # into equal parts based on the number of lines
        ydata = (height / (numlines + 1)) * np.ones(xdata.shape, float)
        # for each line, create the line at the proper location
        # and set the dash pattern
        for i_line in range(numlines):
            legline = Line2D(xdata, ydata * (numlines - i_line) - ydescent)
            self.update_prop(legline, orig_handle, legend)
            # set color, dash pattern, and linewidth to that
            # of the lines in linecollection
            try:
                color = orig_handle.get_colors()[i_line]
            except IndexError:
                color = orig_handle.get_colors()[0]
            try:
                dashes = orig_handle.get_dashes()[i_line]
            except IndexError:
                dashes = orig_handle.get_dashes()[0]
            try:
                lw = orig_handle.get_linewidths()[i_line]
            except IndexError:
                lw = orig_handle.get_linewidths()[0]
            if dashes[0] is not None:
                legline.set_dashes(dashes[1])
            legline.set_color(color)
            legline.set_transform(trans)
            legline.set_linewidth(lw)
            leglines.append(legline)
        return leglines


def __create_legend_line_collections(idx0: list) -> list:
    # make list of one line -- doesn't matter what the coordinates are
    line = [[(0, 0)]]
    # set up the proxy artist

    # all_lines = plt.gca().lines
    all_lines = plt.gcf().gca().lines
    if len(all_lines) == 0:
        raise RuntimeError("Aucune courbe ou réseau affiché.")

    n_networks = len(idx0)
    line_collections = [None] * n_networks
    n_curves = [0] * n_networks
    end = len(idx0) - 1
    if n_networks == 1:
        n_curves[0] = len(all_lines)
    else:
        # calcul du nombre de courbes par réseau
        for i, e in enumerate(idx0):
            if i == 0:
                n_curves[i] = idx0[i + 1]
            elif i == end:
                n_curves[i] = len(all_lines) - e
            else:
                n_curves[i] = idx0[i + 1] - e
    # Pour chaque réseau, création d'une LineCollection représentant le réseau (style et couleurs)
    for net in range(n_networks):
        start = idx0[net]
        n_lines_in_col = 3 if n_curves[net] > 2 else n_curves[net]
        net_styles = [None] * n_lines_in_col
        net_colors = [None] * n_lines_in_col
        for i in range(n_lines_in_col):
            current_line = all_lines[start + i]
            net_styles[i] = current_line.get_linestyle()
            net_colors[i] = current_line.get_color()
        line_collections[net] = mcol.LineCollection(n_lines_in_col * line, linestyles=net_styles, colors=net_colors)
    return line_collections


def networks_legend(labels: list, idx0: list, loc: str = 'lower center') -> None:
    try:
        llc = __create_legend_line_collections(idx0)
    except RuntimeError as e:
        warnings.warn(f'Impossible de créer la légende de réseaux : {e}', RuntimeWarning)
        return None
    else:
        anchor, pbcb = move_axes_net_leg(plt.gca(), loc=loc)
        print(f'Anchor = {anchor}\t({type(anchor)}')
        print(f'pbcb = {pbcb}')
        # plt.legend(llc, labels, handler_map={type(llc[0]): HandlerDashedLines()}, handleheight=3,
        #            bbox_transform=plt.gcf().transFigure, loc='lower center',
        #            bbox_to_anchor=(0.002, 0.002, 0.995, 0.102), ncol=len(idx0))
        plt.legend(llc, labels, handler_map={type(llc[0]): HandlerDashedLines()}, handleheight=3,
                   bbox_transform=plt.gcf().transFigure, loc=loc,
                   bbox_to_anchor=pbcb, ncol=len(idx0))


def move_axes_net_leg(parent, **kw):
    loc = kw.pop('loc', 'lower center')
    if loc not in ('upper right', 'upper left', 'upper center', 'lower right', 'lower left', 'lower center'):
        warnings.warn(f'{loc} : Unauthorized legend location for networks. \'lower\' center will be used.')
        loc = 'lower center'
    fraction = kw.pop('fraction', 0.15)
    shrink = kw.pop('shrink', 1.0)
    aspect = kw.pop('aspect', 20)
    # pb = transforms.PBox(parent.get_position())
    pb = parent.get_position(original=True).frozen()
    if 'lower' in loc:
        pad = kw.pop('pad', 0.075)
        pbcb, pbx, pb1 = pb.splity(fraction, fraction + pad)
        pbcb = pbcb.shrunk(shrink, 1.0).anchored('C', pbcb)
        aspect = 1.0 / aspect
        anchor = kw.pop('anchor', (0.25, 1.0))
        panchor = kw.pop('panchor', (0.25, 0.0))
    else:
        pad = kw.pop('pad', 0.05)
        x1 = 1.0 - fraction
        pb1, pbx, pbcb = pb.splity(x1 - pad, x1)
        pbcb = pbcb.shrunk(1.0, shrink).anchored('C', pbcb)
        anchor = kw.pop('anchor', (0.0, 0.25))
        panchor = kw.pop('panchor', (1.0, 0.25))
    parent.set_position(pb1)
    parent.set_anchor(panchor)
    # fig = parent.get_figure()
    # cax = fig.add_axes(pbcb)
    # cax.set_aspect(aspect, anchor=anchor, adjustable='box')
    # return cax, kw
    return anchor, pbcb


def one_plot_move():
    # Création et affichage des données
    x = np.linspace(0, 5, 100)
    plt.figure()
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color'][:5]
    styles = ['solid', 'solid', 'dashed', 'dashed', 'dashed']
    for i, i_color, style in zip(range(5), colors, styles):
        plt.plot(x, np.sin(x) - .1 * i, c=i_color, ls=style)

    networks_legend(['groupe 1', 'groupe 2'], [0, 2], loc='lower center')
    # nem_plt.MatplotlibConfiguration.networks_legend(['groupe 1'], [0, ])

    # nem_plt.MatplotlibConfiguration.highlight_curve(2)
    # nem_plt.MatplotlibConfiguration.hide_curve(1)

    plt.show()


def one_plot():
    # Création et affichage des données
    x = np.linspace(0, 5, 100)
    plt.figure()
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color'][:5]
    styles = ['solid', 'solid', 'dashed', 'dashed', 'dashed']
    for i, i_color, style in zip(range(5), colors, styles):
        plt.plot(x, np.sin(x) - .1 * i, c=i_color, ls=style)

    nem_plt.PyPlotCEA.networks_legend(['groupe 1', 'groupe 2'], [0, 2])
    # nem_plt.MatplotlibConfiguration.networks_legend(['groupe 1'], [0, ])

    # nem_plt.MatplotlibConfiguration.highlight_curve(2)
    # nem_plt.MatplotlibConfiguration.hide_curve(1)

    plt.show()


def several_plots():
    x = np.linspace(0, 5, 100)
    plt.figure()
    n_p1 = 4
    for i in range(n_p1):
        plt.plot(x, np.sin(x) - .1 * i)

    nem_plt.PyPlotCEA.networks_legend(['groupe 1', 'groupe 2'], [0, 2])

    n_p2 = 2
    for i in range(n_p1, n_p1 + n_p2):
        plt.plot(x, np.sin(x) - .1 * (n_p1 + i))

    plt.draw()

    nem_plt.PyPlotCEA.networks_legend(['groupe 1', 'groupe 2', 'groupe 3'], [0, 3, n_p1])

    plt.show()


if __name__ == '__main__':
    nem_plt.PyPlotCEA.nemesis_params()

    # one_plot()
    # several_plots()
    one_plot_move()

    plt.show()
