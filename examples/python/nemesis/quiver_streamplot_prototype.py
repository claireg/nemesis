# -*- coding: utf-8 -*-

"""
Existe wrapping C++ de `quiver` et `streamplot`, mais pour les besoins maisons, on a besoin de :
* toujours colorer les vecteurs par leur magnitude
* re-former les tableaux 1D qui devraient être 2D
* normaliser les vecteurs
* afficher les valeurs min et max ayant servis à la normalisation
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

interval = 1
cpt = 0


def data():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(0, 2 * np.pi, .2))
    ud = np.cos(xd)
    vd = np.sin(yd)
    return "u=cos(x) ; y=sin(y)", xd, yd, ud, vd


def data_2():
    # xd, yd = np.meshgrid(np.arange(-0.8, 1, 0.2), np.arange(-0.8, 1, 0.2))
    xd, yd = np.meshgrid(np.arange(-0.8, 1, 0.02), np.arange(-0.8, 1, 0.02))
    ud = np.sin(np.pi * xd) * np.cos(np.pi * yd)
    vd = -np.cos(np.pi * xd) * np.sin(np.pi * yd)
    return "u=sin(pi*x) * cos(pi*y) ; v=-cos(pi*x) * sin(pi*y)", xd, yd, ud, vd


def data_3():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = np.power(xd, 2) * np.cos(yd)
    vd = np.power(yd, 2) * np.sin(xd)
    return "u=x^2*cos(y) ; v=y^2*sin(x)", xd, yd, ud, vd


def dipole_potential(xd, yd):
    """The electric dipole potential V, at position *x*, *y*."""
    r_sq = xd ** 2 + yd ** 2
    theta = np.arctan2(yd, xd)
    zd = np.cos(theta) / r_sq
    return (np.max(zd) - zd) / (np.max(zd) - np.min(zd))


def data_4():
    from matplotlib.tri import (
        Triangulation, CubicTriInterpolator)
    # First create the x and y coordinates of the points.
    n_angles = 30
    n_radii = 10
    min_radius = 0.2
    radii = np.linspace(min_radius, 0.95, n_radii)

    angles = np.linspace(0, 2 * np.pi, n_angles, endpoint=False)
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)
    angles[:, 1::2] += np.pi / n_angles

    xd = (radii * np.cos(angles)).flatten()
    yd = (radii * np.sin(angles)).flatten()
    vd = dipole_potential(xd, yd)

    # Create the Triangulation; no triangles specified so Delaunay triangulation
    # created.
    triang = Triangulation(xd, yd)
    # Mask off unwanted triangles.
    # triang.set_mask(np.hypot(xd[triang.triangles].mean(axis=1),
    #                          yd[triang.triangles].mean(axis=1))
    #                 < min_radius)
    # -- Computes the electrical field (Ex, Ey) as gradient of electrical potential
    tci = CubicTriInterpolator(triang, -vd)
    # Gradient requested here at the mesh nodes but could be anywhere else:
    (ex, ey) = tci.gradient(triang.x, triang.y)
    e_norm = np.sqrt(ex**2 + ey**2)
    return "u=[grad(tri.x, tri.y)].x / norm ; v = [grad(tri.x, tri.y)].y / norm", \
           triang.x, triang.y, ex / e_norm, ey / e_norm


def data_5():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = xd * (1-xd) * np.cos(4 * np.pi * yd)
    vd = np.sin(4 * np.pi * xd)
    return "u=x*(1-x)*cos(4*pi*y) ; v = sin(4*pi*x)", xd, yd, ud, vd


def data_6():
    xd, yd = np.meshgrid(np.arange(-4, 4, .2), np.arange(-4, 4, .2))
    # joli mais equation physiquement erronée
    # ex = (xd + 1) / ((xd + 1) ** 2 + yd ** 2) - (xd - 1) / ((xd - 1) ** 2 + xd ** 2)
    ex = (xd + 1) / ((xd + 1) ** 2 + yd ** 2) - (xd - 1) / ((xd - 1) ** 2 + yd ** 2)
    # joli mais equation physiquemnt erronée
    # ey = xd / ((xd + 1) ** 2 + yd ** 2) - xd / ((xd - 1) ** 2 + yd ** 2)
    ey = yd / ((xd + 1) ** 2 + yd ** 2) - yd / ((xd - 1) ** 2 + yd ** 2)
    # plt.streamplot(X,Y,Ex,Ey)
    # plt.quiver(X,Y,Ex,Ey,scale=50)
    return "dipôleX; dipôleY", xd, yd, ex, ey


def data_7():
    # return x * (1 - x) * np.cos(4 * np.pi * x) * np.sin(4 * np.pi * y ** 2) ** 2
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = np.cos(4 * np.pi * yd)
    vd = (1 - xd) / (xd + 1)
    return "u=cos(4*pi*y) ; v = (1-x)/(x+1)", xd, yd, ud, vd


def data_8():
    xd, yd = np.meshgrid(np.arange(-np.pi, np.pi, .2), np.arange(-np.pi, np.pi, .2))
    ud = -np.cos(xd) + np.sin(3 * yd)
    vd = xd**2
    return "u=-cos(x)+sin(3y) ; v=x^2", xd, yd, ud, vd


def data_9():
    xd, yd = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(-0.2, np.pi, .2))
    ud = 1.5 * xd**2 * np.sin(yd)
    vd = 1.5 * yd**2 * np.cos(xd)
    return "u=1.5*x^2*sin(y) ; v=1.5*y^2*cos(x)", xd, yd, ud, vd


def data_10():
    xd, yd = np.meshgrid(np.arange(-1, 1, 0.1), np.arange(-1, 1, 0.1))
    expn_2_3 = np.full(xd.shape, 2/3)
    ud = np.exp(xd, expn_2_3) + np.cos(yd * np.pi)
    vd = yd ** 2 + np.sin(xd * np.pi)
    return "u=bah ; v=beuh", xd, yd, ud, vd


def custom_streamplot(x: np.array, y: np.array, u: np.array, v: np.array, **kwargs):
    """
    pyplot.streamplot(x, y, u, v,
        density=1, linewidth=None,
        color=None, cmap=None,
        norm=None, arrowsize=1,
        arrowstyle='-|>',
        minlength=0.1, transform=None,
        zorder=None, start_points=None,
        maxlength=4.0, integration_direction='both')
    * x, y : 1D array
    * u, v : 2D array
    * density : float ou (float, float)
    * linewidth : float ou 2D array
    * color : matplotlib color code, or 2D array
    * cmap : objet de type colormap
    * norm : objet de normalisation par défaut (min, max) -> (0, 1)
    * arrowsize : float
    * arrowstyle : str
    * minlength : float
    * zorder : int
    * start_points : Nx2 array
    * maxlength : float
    * integration_direction : str dans {'forward', 'backward', 'both'}

    * title : str : Titre du graphique
    * draw_colorbar : bool : affichage ou non de la colorbar (défaut : True)

    :return: None
    """
    # vérification de kwargs : on enlève les arguments maison :
    # 'title', 'lw_mag' pour le moment
    common_aliases = {'density': ('d',), 'linewidth': ('lw', 'linewidths'), 'color': ('c',),
                      'cmap': ('cm',), 'norm': ('no',), 'arrowsize': ('asize', 'as',),
                      'arrowstyle': ('astyle', 'ast',), 'minlength': ('minlg', 'milg',),
                      'zorder': ('zo',), 'start_points': ('spoints', 'spts', ),
                      'maxlength': ('maxlg', 'malg',),
                      'integration_direction': ('i_dir', 'int_dir'),
                      'title': ('title',), 'lw_mag': ('lwm',), 'draw_colorbar': ('dcb', 'dc',)}

    # --- Vérification des dimensions des données et de leurs cohérences
    #   plt.streamplot est moins permissif que plt.quiver
    # * X et Y doivent être de même dimension : 1D ou 2D, et de même shape
    #   * 1D : soit base de la grille, soit tab 2D en mode flatten
    # * U et V doivent être de même dimension : 1D ou 2D, et de même forme
    # * Dans le cas où ce ne sont que des tableaux 1D, et qu'ils ont tous la même taille,
    #   nx et ny peuvent être préciser
    if x.ndim == y.ndim == u.ndim == v.ndim == 1 and x.size == y.size == u.size == v.size:
        common_aliases.update(nx=('n_x',), ny=('n_y',))

    # construction du dictionnaire
    strm_args = cbook.normalize_kwargs(kwargs, common_aliases)
    user_title = strm_args.pop('title', None)
    with_lw_mag = strm_args.pop('lw_mag', False)
    draw_colorbar = strm_args.pop('draw_colorbar', True)

    if x.ndim == y.ndim == 2:
        if u.ndim == v.ndim == 2:
            if not(x.shape == y.shape == u.shape == v.shape):
                msg = f'{x.shape}\t{y.shape}\t{u.shape}\t{v.shape}'
                raise RuntimeError(f"X, Y, U et V doivent avoir la même forme : {msg}")
        elif u.ndim == v.ndim == 1:
            # Cas X et Y 2D, U et V 1D
            if not (u.shape == v.shape and x.shape == y.shape and u.shape[0] == x.size):
                print(f'u.shape[0] == v.shape[0] : {u.shape[0] == v.shape[0]}')
                print(f'u.shape[0] == x.shape[0] : {u.shape[0] == x.size}')
                print(f'x.shape == y.shape : {x.shape == y.shape}')
                msg_uv = f'Forme de U ou V invalide : U:{u.shape} == V:{v.shape}' \
                    if not(u.shape == v.shape) else ''
                msg_xy = f'Forme de X ou Y invalide : X:{x.shape} == Y:{y.shape})' \
                    if not(x.shape == y.shape) else ''
                msg_xyuv = f'Forme de U ou V incompatible avec forme de X ou Y : ' \
                           f'U:{u.shape} == V:{v.shape} U0:{u.shape[0]} == X0:{x.size}' \
                    if not(u.shape[0] == x.size) else ''
                raise RuntimeError(f"Cas X et Y 2D, U et V 1D\n\tCombinaison des formes des tableaux invalide\n"
                                   f"{msg_xy} {msg_uv} {msg_xyuv}")
            else:
                u = u.reshape(x.shape)
                v = v.reshape(y.shape)
        else:
            raise RuntimeError(f"U et V doivent être de même dimension : 1 ou 2 : {u.ndim}, {v.ndim}")
    elif x.ndim == y.ndim == 1:
        if u.ndim == u.ndim == 2:
            if not(u.shape == v.shape and x.shape == y.shape):
                msg = f'{x.shape}\t{y.shape}\t{u.shape}\t{v.shape}'
                raise RuntimeError(f"X, Y, U et V doivent avoir la même forme : {msg}")
            else:
                # Cas X et Y 1D complet, U et V 2D
                if x.shape[0] == u.size:
                    x = x.reshape(u.shape)
                    y = y.reshape(u.shape)
                # Cas X et Y 1D base de la grille, U et V 2D -> rien à faire
                elif not (x.shape[0] == u.shape[0]):
                    msg = f'X0:{x.shape[0]} == U0:{u.shape[0]}'
                    raise RuntimeError(f"Combinaison des formes invalide : {msg}")
        elif u.ndim == v.ndim == 1:
            # Cas X et Y 1D base de la grille, U et V 1D
            if x.size * y.size == u.size and u.size == v.size:
                new_shape = (x.size, y.size)
                u = u.reshape(new_shape)
                v = v.reshape(new_shape)
            # Cas X et Y 1D complet, U et V 1D : nx et ny doivent exister
            elif x.size == y.size == u.size == v.size:
                nx, ny = strm_args.pop('nx', None), strm_args.pop('ny', None)
                if nx and ny and x.size == nx * ny:
                    new_shape = (nx, ny)
                    x = x.reshape(new_shape)
                    y = y.reshape(new_shape)
                    u = u.reshape(new_shape)
                    v = v.reshape(new_shape)
                else:
                    if not(nx and ny):
                        raise RuntimeError('X, Y, U et V 1D et de même taille : nx et ny doivent être préciser')
                    else:
                        raise RuntimeError(f"X, Y, U et V doivent être de taille nx*ny ({nx*ny} – X: {x.size})")
        else:
            raise RuntimeError(f"U et V doivent être de même dimension : 1 ou 2 : {u.ndim}, {v.ndim}")
    else:
        raise RuntimeError(f"X et Y doivent être de même dimension : 1 ou 2 : {x.ndim}, {y.ndim}")

    # calcul de la magnitude
    mag = np.hypot(u, v)
    # normalisation des composantes
    s_min = mag.min()
    s_max = mag.max()
    mag_norm = s_max - s_min
    u /= mag_norm
    v /= mag_norm
    # création tableaux des épaisseurs
    if with_lw_mag:
        lw = 5 * mag / mag.max()
        if 'linewidth' not in strm_args.keys():
            strm_args['linewidth'] = lw
    # affichage du streamplot
    if 'color' not in strm_args.keys():
        strm = plt.streamplot(x, y, u, v, color=mag, **strm_args)
    else:
        if strm_args['color'] is None:
            draw_colorbar = False
        strm = plt.streamplot(x, y, u, v, **strm_args)
    if draw_colorbar:
        plt.gcf().colorbar(strm.lines)

    # modification du titre
    minmax_title = f'Norme : [{s_min:.4e}, {s_max:.4e}]'
    if user_title:
        plt.suptitle(user_title)
        plt.title(minmax_title)
    else:
        plt.suptitle(minmax_title)
    return None


def custom_quiver(x: np.array, y: np.array, u: np.array, v: np.array, **kwargs):
    """
    pyplot.quiver([X, Y], U, V, [C], **kw)
    * x, y : 1D ou 2D array-like
    * u, v : 1D ou 2D array-like
    * c : 1D ou 2D array-like
    * units : str dans {'width', 'height', 'dots', 'inches', 'x', 'y' 'xy'}, default : width
    * angles : {'uv', 'xy'} or array-like, default 'uv'
    * scale : float
    * scale_units : str dans {'width', 'height', 'dots', 'inches', 'x', 'y', 'xy'}
    * width : float
    * headwidth : float (defaut : 3)
    * headlength : float, optional, default: 5
    * headaxislength : float, optional, default: 4.5
    * minshaft : float, optional, default: 1
    * minlength : float, optional, default: 1
    * pivot : {'tail', 'mid', 'middle', 'tip'}, optional, default: 'tail'
    * color : color or color sequence, optional. Si donné, draw_colorbar est mis à False.

    * title : str : Titre du graphique
    * draw_colorbar : bool : affichage ou non de la colorbar (défaut : True)
    * draw_quiverkey : bool : affichague ou non de la quiverkey (défaut : True)

    :return: None
    """
    # vérification de kwargs : on enlève les arguments maison :
    # 'title', 'lw_mag' pour le moment
    common_aliases = {'units': ('u',), 'angles': ('ang', 'ag',),
                      'scale': ('sc', ), 'scale_units': ('scu',), 'width': ('w',),
                      'headwidth': ('hw',), 'headlength': ('hl',), 'headaxislength': ('hal',),
                      'minshaft': ('minsh',), 'minlength': ('minlg', 'milg',), 'pivot': ('pv',),
                      'color': ('c',),
                      'title': ('title',), 'draw_colorbar': ('dcb', 'dc',),
                      'draw_quiverkey': ('dqk', 'dk',)}

    # vérification des dimensions des données
    # X et Y doivent être de même dimension : 1D ou 2D
    if not ((x.ndim == y.ndim == 1) or (x.ndim == y.ndim == 2)):
        raise RuntimeError('X et Y doivent être de même dimension (dim = 1 ou dim = 2)')
    # construction du dictionnaire
    quiver_args = cbook.normalize_kwargs(kwargs, common_aliases)
    user_title = quiver_args.pop('title', None)
    draw_colorbar = quiver_args.pop('draw_colorbar', True)
    draw_quiverkey = quiver_args.pop('draw_quiverkey', True)

    with_color_mag = True
    if 'color' in quiver_args.keys():
        with_color_mag = False
        draw_colorbar = False
    # calcul de la magnitude
    mag = np.hypot(u, v)
    # normalisation des composantes
    s_min = mag.min()
    s_max = mag.max()
    mag_norm = s_max - s_min
    u /= mag_norm
    v /= mag_norm
    # affichage du champs de vecteurs
    if with_color_mag:
        q = plt.quiver(x, y, u, v, mag, **quiver_args)
    else:
        q = plt.quiver(x, y, u, v, **quiver_args)
    # modification du titre
    minmax_title = f'Norme : [{s_min:.4e}, {s_max:.4e}]'
    if user_title:
        plt.suptitle(user_title)
        plt.title(minmax_title)
    else:
        plt.suptitle(minmax_title)
    if draw_colorbar:
        plt.gcf().colorbar(q)
    if draw_quiverkey:
        plt.gca().quiverkey(q, 0.8, 0.9, 1, 'unité', labelpos='E', coordinates='figure')

    return None


def print_data_size(name, x, y, u, v):
    print(f'\n# Données : {name}')
    for i, t in enumerate([x, y, u, v]):
        tot = f'{t.shape[0] * t.shape[1]}' if t.ndim == 2 else ''
        print(f'\t* {i}. {t.ndim}\t{t.shape}\t{tot}')


def next_draw():
    global cpt
    import os
    plt.pause(interval)
    where = os.path.join(os.path.expanduser('~'), 'tmp', f'image_{cpt}.png')
    plt.savefig(where)
    cpt += 1
    plt.clf()


def custom_suptitle(message):
    plt.suptitle(message, y=0.5, fontsize=20, fontweight='bold')


if __name__ == '__main__':
    name1, xd1, yd1, ud1, vd1 = data()
    name2, xd2, yd2, ud2, vd2 = data_2()
    name3, xd3, yd3, ud3, vd3 = data_3()
    name4, xd4, yd4, ud4, vd4 = data_4()
    name5, xd5, yd5, ud5, vd5 = data_5()
    name6, xd6, yd6, ud6, vd6 = data_6()
    name7, xd7, yd7, ud7, vd7 = data_7()
    name8, xd8, yd8, ud8, vd8 = data_8()
    name9, xd9, yd9, ud9, vd9 = data_9()
    name10, xd10, yd10, ud10, vd10 = data_10()
    txd8 = xd8[:ud9.shape[0], :]
    tyd8 = yd8[:ud9.shape[0], :]
    # print_data_size(name2, xd2, yd2, ud2, vd2)
    # print_data_size(name1, xd1, yd1, ud1, vd1)
    # print_data_size(name3, xd3, yd3, ud3, vd3)
    # print_data_size(name4, xd4, yd4, ud4, vd4)
    # print_data_size(name5, xd5, yd5, ud5, vd5)
    # print_data_size(name6, xd6, yd6, ud6, vd6)
    # print_data_size(name7, xd7, yd7, ud7, vd7)
    # print_data_size(name8, xd8, yd8, ud8, vd8)
    # print_data_size(name9, xd9, yd9, ud9, vd9)
    # print_data_size(name10, xd10, yd10, ud10, vd10)

    # custom_quiver(xd1, yd1, ud1, vd1, title=f'Name 1 :  {name1}')
    # next_draw()
    # custom_quiver(xd2, yd2, ud2, vd2, units='xy', title=name2)
    # next_draw()
    # custom_quiver(xd3, yd3, ud3, vd3, width=0.006, title=name3)
    # next_draw()
    # custom_quiver(xd4, yd4, ud4, vd4,
    #               title=name4,
    #               units='xy', zorder=3, color='blue', width=0.007, headwidth=3., headlength=4.)
    # next_draw()
    # # Pas le même rendu des couleurs avec et sans colorbar ...
    # custom_quiver(xd4, yd4, ud4, vd4,
    #               draw_colorbar=False, title=name4)
    # next_draw()
    # custom_quiver(xd5, yd5, ud5, vd5, title=name5)
    # next_draw()
    # custom_quiver(xd6, yd6, ud6, vd6, title=name6, scale=50)
    # next_draw()
    # custom_quiver(xd7, yd7, ud7, vd7, title=name7)
    # next_draw()
    # custom_quiver(xd8, yd8, ud8, vd8, title=name8)
    # next_draw()
    # custom_quiver(xd9, yd9, ud9, vd9, title=name9)
    # next_draw()
    # custom_quiver(xd8, yd8, ud9, vd9, title="mix d8 pour coord, d9 pour uv")
    # next_draw()
    # custom_quiver(xd10, yd10, ud10, vd10, title=name10)
    # next_draw()
    #
    # custom_streamplot(xd1, yd1, ud1, vd1, title=name1)
    # next_draw()
    # custom_streamplot(xd2, yd2, ud2, vd2, lw_mag=True, title=name2)
    # next_draw()
    # custom_streamplot(xd3, yd3, ud3, vd3, draw_colorbar=False, title=name3)
    # next_draw()
    # # Ne peut fonctionner car 'x' values must be equally spaced
    # # custom_streamplot(xd4, yd4, ud4, vd4, title=name4)
    # # plt.show()
    # custom_streamplot(xd5, yd5, ud5, vd5, title=name5)
    # next_draw()
    # custom_streamplot(xd6, yd6, ud6, vd6, title=name6)
    # next_draw()
    # custom_streamplot(xd7, yd7, ud7, vd7, title=name7)
    # next_draw()
    # custom_streamplot(xd8, yd8, ud8, vd8, title=name8)
    # next_draw()
    # custom_streamplot(xd9, yd9, ud9, vd9, lw_mag=True, title=name9)
    # next_draw()
    # custom_streamplot(txd8, tyd8, ud9, vd9, color=None, title="mix d8 pour coord, d9 pour uv")
    # next_draw()
    # custom_streamplot(txd8, tyd8, ud9, vd9, title="mix d8 pour coord, d9 pour uv")
    # next_draw()
    # custom_streamplot(xd10, yd10, ud10, vd10, title=name10, lw_mag=True)
    # next_draw()

    # Tests des combinaisons des formes de X, Y, U et V pour custom_quiver et custom_streamplot
    xd8_small = xd8[0]
    yd8_small = yd8[:, 0]
    try:
        custom_suptitle('# Quiver\nx & y 2D\nu & v 2D')
        next_draw()
        custom_quiver(xd8, yd8, ud8, vd8)
        next_draw()
        custom_suptitle('# Quiver\nx & y 1D flatten\nu & v 1D flatten')
        next_draw()
        custom_quiver(xd8.flatten(), yd8.flatten(), ud8.flatten(), vd8.flatten())
        next_draw()
        custom_suptitle('# Quiver\nx & y 1D flatten\nu & v 2D')
        next_draw()
        custom_quiver(xd8.flatten(), yd8.flatten(), ud8, vd8)
        next_draw()
        custom_suptitle('# Quiver\nx & y 2d\nu & v 1D flatten')
        next_draw()
        custom_quiver(xd8, yd8, ud8.flatten(), vd8.flatten())
        next_draw()
        custom_suptitle('# Quiver\nx & y 1D small\nu & v 2D')
        next_draw()
        custom_quiver(xd8_small, yd8_small, ud8, vd8)
        next_draw()
        # Ne se plante pas mais rendu incorrect
        custom_suptitle('# Quiver\nx & y 1D small\nu & v 1D flatten')
        next_draw()
        custom_quiver(xd8_small, yd8_small, ud8.flatten(), vd8.flatten())
        next_draw()

        custom_suptitle('# Streamplot\nx & y 2D\nu & v 2D')
        next_draw()
        custom_streamplot(xd8, yd8, ud8, vd8)
        next_draw()
        custom_suptitle('# Streamplot\nx & y 2D\nu & v 1D flatten')
        next_draw()
        custom_streamplot(xd8, yd8, ud8.flatten(), vd8.flatten())
        next_draw()
        custom_suptitle('# Streamplot\nx & y 1D small\nu & v 2D')
        next_draw()
        custom_streamplot(xd8_small, yd8_small, ud8, vd8)
        next_draw()
        custom_suptitle('# Streamplot\nx & y 1D small\nu & v 1D flatten')
        next_draw()
        custom_streamplot(xd8_small, yd8_small, ud8.flatten(), vd8.flatten())
        next_draw()
        custom_suptitle('# Streamplot\nx & y 1D flatten\nu & v 1D flatten\nnx & ny')
        next_draw()
        custom_streamplot(xd8.flatten(), yd8.flatten(), ud8.flatten(), vd8.flatten(), nx=xd8.shape[0], ny=xd8.shape[1])
        next_draw()
        custom_suptitle('# Streamplot\nx & y 1D flatten\nu & v 2D')
        next_draw()
        custom_streamplot(xd8.flatten(), yd8.flatten(), ud8, vd8)
        next_draw()
    except (RuntimeError, ValueError) as e:
        import traceback
        print(traceback.format_exc())
        print(e)
