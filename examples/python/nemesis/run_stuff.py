# -*- coding: utf-8 -*-

import os

import nemesis.testing as nem_tst


TEST_DIR = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'nemesis', 'tests', 'baseline_objects')


def psyche():
    tst_name = 'read_psy_file_vecteurs'
    psy_name = 'vecteurs.psy'
    nem_tst.run_test_read_psy_file_base(TEST_DIR, tst_name, psy_name)


def gaia():
    tst_name = 'read_gaia_file_curve'
    gaia_name = 'curve.xml'
    nem_tst.run_test_read_gaia_file_base(TEST_DIR, tst_name, gaia_name)
    print(tst_name)
    tst_name = 'read_gaia_file_courbe_en_escalier'
    gaia_name = 'courbe_en_escalier.xml'
    nem_tst.run_test_read_gaia_file_base(TEST_DIR, tst_name, gaia_name)
    print(gaia_name)


if __name__ == '__main__':
    gaia()
    psyche()
