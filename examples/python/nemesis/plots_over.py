# -*- coding: utf-8 -*-

from matplotlib import collections
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import nemesis.plotter as nem_plt
import nemesis.readdata as nem_rd

x, y, z = nem_rd.wavelet(30, 30)
px, py = x, y
pz = z[0::x.size]
elevation = 88
azimuth = -54


def essai_1():
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', linewidth=0, )
    nem_plt.PyPlotCEA.plot_over(x, y, pz, 'r-', linewidth=2)
    plt.gca().view_init(elevation, azimuth)
    plt.show()


def essai_2():
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', linewidth=0, )
    ax = plt.gca()
    ax.plot(px, py, pz, 'r-', zorder=3, linewidth=2)
    plt.gca().view_init(elevation, azimuth)
    plt.show()


def essai_3():
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', linewidth=0, zorder=-1)
    ax = plt.gca()
    ax.plot(px, py, pz, 'r-', zorder=0, linewidth=2)
    plt.gca().view_init(elevation, azimuth)
    plt.show()


def essai_4():
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', alpha=0.95, zorder=-1)
    ax = plt.gca()
    ax.plot(px, py, pz, 'r-', zorder=0)
    plt.gca().view_init(elevation, azimuth)
    plt.show()


def essai_5():
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', alpha=0.6)
    ax = plt.gca()
    ax.plot(px, py, pz, 'r-')
    plt.gca().view_init(elevation, azimuth)
    plt.show()


def essai_6():
    ax = nem_plt.PyPlotCEA.switch_axes('3d')
    ax.plot3D(px, py, pz, 'r-', alpha=0.95)
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', edgecolor='none')
    plt.gca().view_init(elevation, azimuth)
    plt.show()


def essai_7():
    ax = nem_plt.PyPlotCEA.switch_axes('3d')
    nem_plt.PyPlotCEA.raised_contourf(x, y, z, cmap='current', label='Wavelet', edgecolor='none')
    ax.plot3D(px, py, pz, 'r-')
    plt.gca().view_init(elevation, azimuth)
    nem_plt.PyPlotCEA.set_alpha(0.7)
    plt.show()


def essai_alpha_2d_0():
    nem_plt.PyPlotCEA.linear_contourf(x, y, z)
    nem_plt.PyPlotCEA.set_alpha(0.5, [], collections.PathCollection)
    plt.show()


def essai_alpha_2d_1():
    nem_plt.PyPlotCEA.linear_contourf(x, y, z)
    nem_plt.PyPlotCEA.set_alpha(0.5, [0, 2], 'collections.PathCollection')
    plt.show()


if __name__ == '__main__':
    # essai_1()
    # essai_2()
    # essai_3()
    # essai_4()
    # essai_5()  # Pas mal
    # essai_6()
    # essai_7()
    # essai_alpha_2d_0()
    essai_alpha_2d_1()
