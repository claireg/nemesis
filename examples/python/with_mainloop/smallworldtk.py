# -*- coding: utf-8 -*-

from plotter import Plotter

import tkinter as tk


class SmallWorldTk(object):
    def __init__(self, w=400, h=300, title=''):
        self.__root = tk.Tk()
        self.__width = w
        self.__height = h
        if len(title) == 0:
            self.__root.title("It's a small world")
        else:
            self.__root.title(title)
        self.__buttons = []
        self.__create_button('Quitter', self.cb_quit)

        self.__plotter = Plotter(1600, 1200, n=1000)
        self.__create_button('Show', self.__plotter.show)
        self.__create_button('Clear', self.__plotter.clear)
        self.__create_button('Légende', self.__plotter.legend)
        self.__create_button('XW', self.__plotter.plot_xw)
        self.__create_button('XY', self.__plotter.plot_xy)
        self.__create_button('XY', self.__plotter.plot_xz)

        self.__plotter.plot_init()
        SmallWorldTk.__start()

    def __create_button(self, text, func):
        self.__buttons.append(tk.Button(master=self.__root, text=text, command=func))
        self.__buttons[-1].pack(side=tk.BOTTOM)

    def cb_quit(self):
        # stops  mainloop
        self.__plotter.close()
        self.__root.quit()
        self.__root.destroy()

    def cb_show(self):
        self.__plotter.show()

    def cb_plot_xy(self):
        self.__plotter.plot_xy()

    def cb_plot_xz(self):
        self.__plotter.plot_xz()

    @staticmethod
    def __start():
        tk.mainloop()

