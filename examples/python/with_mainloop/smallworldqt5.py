# -*- coding: utf-8 -*-
"""
@TODO: À passer en PySide2 i.e. en Qt5
"""

import matplotlib.pyplot as plt
try:
    from PySide2 import QtWidgets, QtCore
except ModuleNotFoundError as e:
    from PyQt5 import QtWidgets, QtCore

from plotter import Plotter



class SmallWorldQt5(QtWidgets.QWidget):
    def __init__(self, w=400, h=300, title='', parent=None):
        super().__init__(parent)

        self.__width = w
        self.__height = h

        self.__layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.__layout)
        self.resize(self.__width, self.__height)

        if len(title) == 0:
            self.setWindowTitle("It's a small world")
        else:
            self.setWindowTitle(title)

        self.__gp = QtWidgets.QGroupBox()

        self.__buttons = dict()
        self.__buttons['quit'] = QtWidgets.QPushButton("Quitter")
        self.__layout.addWidget(self.__buttons['quit'])
        self.connect(self.__buttons['quit'], QtCore.SIGNAL("clicked()"), QtWidgets.qApp, QtCore.SLOT("quit()"))

        self.__buttons['connect'] = QtWidgets.QPushButton("Connect")
        self.__layout.addWidget(self.__buttons['connect'])
        self.connect(self.__buttons['connect'], QtCore.SIGNAL("clicked()"), self.__connect)

        self.__create_button('Show')
        self.__create_button('Clear')
        self.__create_button('Légende')
        self.__create_button('Move')
        self.__create_button('XW')
        self.__create_button('XY')
        self.__create_button('XZ')

        self.__layout.addWidget(self.__gp, 1, 1)

    def __create_button(self, text):
        self.__buttons[text] = QtWidgets.QPushButton(text)
        self.__layout.addWidget(self.__buttons[text])

    def __connect(self):
        self.__plotter = Plotter(self.__width, self.__height, n=1000)
        self.connect(self.__buttons['Show'], QtCore.SIGNAL("clicked()"), self.__plotter.show)
        self.connect(self.__buttons['Clear'], QtCore.SIGNAL("clicked()"), self.__plotter.clear)
        self.connect(self.__buttons['Légende'], QtCore.SIGNAL("clicked()"), self.__plotter.legend)
        self.connect(self.__buttons['Move'], QtCore.SIGNAL("clicked()"), self.__plotter.move)
        self.connect(self.__buttons['XW'], QtCore.SIGNAL("clicked()"), self.__plotter.plot_xw)
        self.connect(self.__buttons['XY'], QtCore.SIGNAL("clicked()"), self.__plotter.plot_xy)
        self.connect(self.__buttons['XZ'], QtCore.SIGNAL("clicked()"), self.__plotter.plot_xz)
        self.__plotter.plot_init()


    def cb_show(self):
        self.__plotter.show()

    def cb_plot_xy(self):
        self.__plotter.plot_xy()

    def cb_plot_xz(self):
        self.__plotter.plot_xz()

