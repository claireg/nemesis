# -*- coding: utf-8 -*-

import numpy as np
import matplotlib as mpl
# Fonctionne avec ion et Qt5Agg, si on ne fait pas appel à show, fonctionne à une bonne vitesse
#   (à vérifier)
# Ne fonctionne pas avec TkAgg (même avec mpl.rc('tk', window_focus=True))
import matplotlib.pyplot as plt

"""
Paramètres de configuration matplotlib
--------------------------------------
#axes.autolimit_mode : data # How to scale axes limits to the data.
                            # Use "data" to use data limits, plus some margin
                            # Use "round_number" move to the nearest "round" number
#axes.xmargin        : .05  # x margin.  See `axes.Axes.margins`
#axes.ymargin        : .05  # y margin See `axes.Axes.margins`
# tk backend params
#tk.window_focus   : False    # Maintain shell focus for TkAgg
"""


class Plotter(object):
    def __init__(self, w, h, n=5):
        linear = np.arange(1, n+1)
        self.__x = linear * linear
        self.__y = np.sin(2*np.pi*linear / 360.)
        self.__z = np.log(linear)
        self.__w = linear * 2
        self.__width = w
        self.__height = h
        self.__pos_x = 400
        self.__pos_y = 200
        self.__delta = 50
        self.__n_call = 0
        # mpl.rc('tk', window_focus=True)
        mpl.rc('axes', autolimit_mode='round_numbers', xmargin=0, ymargin=0)
        mpl.rc('axes', grid=True)
        plt.ion()

    @classmethod
    def plot_init(cls):
        plt.plot()
        plt.show(block=True)

    def plot_xy(self):
        plt.plot(self.__x, self.__y, label='$sin(2*\pi*x / 360.)$')
        self.show()

    def plot_xz(self):
        plt.plot(self.__x, self.__z, label='log(x)')
        self.show()

    def plot_xw(self):
        plt.plot(self.__x, self.__w, label='linear * 2')
        self.show()

    @staticmethod
    def legend():
        plt.legend()

    @staticmethod
    def show():
        if mpl.get_backend() != 'Qt5Agg':
            plt.show(block=True)
        else:
            plt.draw()

    @classmethod
    def clear(cls):
        plt.clf()
        cls.show()

    @staticmethod
    def close():
        plt.close()

    def move(self):
        # Le réglage de la position de la fenêtre est dépendant du backend utilisé (et du window manager)
        window = plt.get_current_fig_manager().window
        # Pour mémoire
        if mpl.get_backend() in ('Qt5Agg', 'Qt4Agg'):
            if self.__n_call % 2 == 0:
                self.__pos_x += self.__delta
                self.__pos_y += self.__delta
            else:
                self.__pos_x -= self.__delta
                self.__pos_y -= self.__delta
            window.setGeometry(self.__pos_x, self.__pos_y, self.__width, self.__height)
        self.__n_call += 1

