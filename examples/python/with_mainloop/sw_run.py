# -*- coding: utf-8 -*-

import sys
import matplotlib as mpl


def run_tk():
    from smallworldtk import SmallWorldTk
    SmallWorldTk()


def run_qt():
    # Oubli d'un rpath lors de la compilation de PySide2, donc ajout à la main
    # ld_path_name = 'LD_LIBRARY_PATH'
    # old_ld_path = os.environ[ld_path_name]
    # gcc_64 = os.path.join('/path', 'vers', 'install', 'gcc49', 'gcc', '6.4')
    # ld_path = old_ld_path + ':' + os.path.join(gcc_64, 'lib') + ':' + os.path.join(gcc_64, 'lib64')
    # os.environ[ld_path_name] = ld_path
    import matplotlib as mpl
    mpl.use('Qt5Agg')
    from smallworldqt5 import SmallWorldQt5
    try:
        from PySide2 import QtWidgets
    except ModuleNotFoundError as e:
        from PyQt5 import QtWidgets
    qapp = QtWidgets.QApplication(sys.argv)
    sw = SmallWorldQt5()
    sw.show()
    sys.exit(qapp.exec_())


def rc_defaults():
    print(mpl._get_configdir())


if __name__ == '__main__':
    if '-qt' in sys.argv[1:]:
        run_qt()
    else:
        run_tk()
